import { Routes } from '@angular/router';
import { LayoutComponent } from './layout/layout.component';
import { AuthGuard } from './shared/service/auth.guard';

export const AppRoutingModule: Routes = [

  { path: 'login', loadChildren: () => import('./pages/login/login.module').then(m => m.LoginModule) },
  { path: 'register', loadChildren: () => import('./pages/register/register.module').then(m => m.RegisterModule) },
  { path: 'forgot-password', loadChildren: () => import('./pages/forgot-password/forgot-password.module').then(m => m.ForgotPasswordModule) },
  { path: '', component: LayoutComponent, loadChildren: () => import('./pages/dashboard/dashboard.module').then(m => m.DashboardModule), canActivate: [AuthGuard] },
  { path: 'adm', component: LayoutComponent, loadChildren: () => import('./pages/adm/adm.module').then(m => m.AdmModule), canActivate: [AuthGuard] },
  { path: 'stu', component: LayoutComponent, loadChildren: () => import('./pages/stu/stu.module').then(m => m.StuModule), canActivate: [AuthGuard] },
  { path: 'aca', component: LayoutComponent, loadChildren: () => import('./pages/aca/aca.module').then(m => m.AcaModule), canActivate: [AuthGuard] },
  { path: 'gra', component: LayoutComponent, loadChildren: () => import('./pages/gra/gra.module').then(m => m.GraModule), canActivate: [AuthGuard] },
  { path: 'dashboard', component: LayoutComponent, loadChildren: () => import('./pages/dashboard/dashboard.module').then(m => m.DashboardModule), canActivate: [AuthGuard] },
  { path: 'mst', component: LayoutComponent, loadChildren: () => import('./pages/mst/mst.module').then(m => m.MstModule), canActivate: [AuthGuard] },
  { path: 'rep', component: LayoutComponent, loadChildren: () => import('./pages/rep/rep.module').then(m => m.RepModule), canActivate: [AuthGuard] },
  { path: 'tec', component: LayoutComponent, loadChildren: () => import('./pages/tec/tec.module').then(m => m.TecModule), canActivate: [AuthGuard] },
  { path: 'admrp', component: LayoutComponent, loadChildren: () => import('./pages/admrp/admrp.module').then(m => m.AdmrpModule), canActivate: [AuthGuard] },
  { path: 'graph', component: LayoutComponent, loadChildren: () => import('./pages/graph/graph.module').then(m => m.GraphModule), canActivate: [AuthGuard] },
  { path: 'information', component: LayoutComponent, loadChildren: () => import('./pages/information/information.module').then(m => m.InformationModule), canActivate: [AuthGuard] },
];
 