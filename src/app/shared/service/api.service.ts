import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams} from '@angular/common/http';
import { Router } from '@angular/router';
//import { constants } from 'http2';
import * as constants from '../../constants';

export interface IResponse {
  responseCode: any;
  responseMsg: any;
  responseData: any;
  responseSize: any;
}
export enum EHttp{
  POST
  , GET
  , PUT
  , DELETTE
  , LOGIN
  , REGISTER
  , FORGOT_PASSWORD
  , FORGOT_PASSWORD_CHECK
  , PREVIEW_PDF
  , REPORT_PDF
  , REPORT_XLS
  , REPORT_DOCX
}

@Injectable({
  providedIn: 'root'
})
export class ApiService {
  //private apiURL: string = 'http://localhost:8089/PSMS';
  private apiURL: string = constants.SERVICE_API; 
  constructor(private httpClient: HttpClient,private router: Router) {
  }
 
  private convertToParam(json:string){
    if(json == null){
      return '';
    }
    return '?'+Object.entries(json).map(e => e.join('=')).join('&');
  }
 
  public async login(json:any):Promise<any>{
    return new Promise((resolve,reject)=>{
      return resolve(this.sendHttp(EHttp.LOGIN,null,json,null));
    });
  }

  public async register(json:any):Promise<any>{
    return new Promise((resolve,reject)=>{
      return resolve(this.sendHttp(EHttp.REGISTER,null,json,null));
    });
  }

  public async forgotpassword(json:any):Promise<any>{ 
    return new Promise((resolve,reject)=>{
      return resolve(this.sendHttp(EHttp.FORGOT_PASSWORD,null,json,null));
    });
  }

  public async forgotpasswordcheck(json:any):Promise<any>{
    return new Promise((resolve,reject)=>{
      return resolve(this.sendHttp(EHttp.FORGOT_PASSWORD_CHECK,null,json,null));
    });
  }

  public async httpGet(path:string,json:any):Promise<any>{
    return new Promise((resolve,reject)=>{
      return resolve(this.sendHttp(EHttp.GET,path,json,null));
    });
  }

  public async httpPost(path:string,json:any):Promise<any>{
    return new Promise((resolve,reject)=>{
      return resolve(this.sendHttp(EHttp.POST,path,json,null));
    });
  }

  public async httpPut(path:string,json:any):Promise<any>{
    return new Promise((resolve,reject)=>{
      return resolve(this.sendHttp(EHttp.PUT,path,json,null));
    });
  }

  public async httpDelete(path:string,json:any):Promise<any>{
    return new Promise((resolve,reject)=>{
      return resolve(this.sendHttp(EHttp.DELETTE,path,json,null));
    });
  }

  public async httpPreviewPDF(path:string,json:any):Promise<any>{
    return new Promise((resolve,reject)=>{
      return resolve(this.sendHttp(EHttp.PREVIEW_PDF,path,json,null));
    });
  }

  public async httpReportPDF(path:string,fileName:any,json:any):Promise<any>{
    return new Promise((resolve,reject)=>{
      return resolve(this.sendHttp(EHttp.REPORT_PDF,path,json,fileName));
    });
  }

  public async httpReportXLS(path:string,fileName:any,json:any):Promise<any>{
    return new Promise((resolve,reject)=>{
      return resolve(this.sendHttp(EHttp.REPORT_XLS,path,json,fileName));
    });
  }

  public async httpReportDOCX(path:string,fileName:any,json:any):Promise<any>{
    return new Promise((resolve,reject)=>{
      return resolve(this.sendHttp(EHttp.REPORT_DOCX,path,json,fileName));
    });
  }

  public async sendHttp(http:EHttp,path:string,json:any,fileName:string): Promise<any>{
    let headers = new HttpHeaders();
    return new Promise((resolve, reject) => {
      if(http == EHttp.LOGIN){
        this.httpClient.post(this.apiURL+"/auth",json).subscribe((s)=>{
          resolve(s);
        },e=>{
          this.error(e);
        });
      }else if(http == EHttp.REGISTER){
        this.httpClient.post(this.apiURL+"/register",json).subscribe((s)=>{
          resolve(s);
        },e=>{
          this.error(e);
        });
      }else if(http == EHttp.FORGOT_PASSWORD){
        this.httpClient.put(this.apiURL+"/forgot-password",json).subscribe((s)=>{
          resolve(s);
        },e=>{
          this.error(e);
        });
      }else if(http == EHttp.FORGOT_PASSWORD_CHECK){
        this.httpClient.get(this.apiURL+"/forgot-password-check"+this.convertToParam(json)).subscribe((s)=>{
          resolve(s);
        },e=>{
          this.error(e);
        });
      }else if(http == EHttp.GET){
        headers = headers.set('Authorization',localStorage.getItem('tokenNo'));
        this.httpClient.get(this.apiURL+path+this.convertToParam(json),{headers:headers}).subscribe((s)=>{
          resolve(s);
        },e=>{
          this.error(e);
        });
      }else if(http == EHttp.POST){
        headers = headers.set('Authorization',localStorage.getItem('tokenNo'));
        json.userName = localStorage.getItem('userName')||'';
        this.httpClient.post(this.apiURL+path,json, {headers:headers}).subscribe((s)=>{
          resolve(s);
        },e=>{
          this.error(e);
        });
      }else if(http == EHttp.PUT){
        headers = headers.set('Authorization',localStorage.getItem('tokenNo'));
        json.userName = localStorage.getItem('userName')||'';
        this.httpClient.put(this.apiURL+path,json,{headers:headers}).subscribe((s)=>{
          resolve(s);
        },e=>{
          this.error(e);
        });
      }else if(http == EHttp.DELETTE){
        headers = headers.set('Authorization',localStorage.getItem('tokenNo'));
        json.userName = localStorage.getItem('userName')||'';
        this.httpClient.delete(this.apiURL+path+this.convertToParam(json),{headers:headers}).subscribe((s)=>{
          resolve(s);
        },e=>{
          this.error(e);
        });
      }else if(http == EHttp.PREVIEW_PDF){
        headers = headers.set('Authorization',localStorage.getItem('tokenNo'));
        this.httpClient.get(this.apiURL+path+this.convertToParam(json),{headers:headers, responseType: 'arraybuffer'}).subscribe((s)=>{
          var file = new Blob([s], {type: 'application/pdf'});
          //resolve(URL.createObjectURL(file));
         window.open(URL.createObjectURL(file));
         
        },e=>{
          this.error(e);
        });
      }else if(http == EHttp.REPORT_PDF){
        headers = headers.set('Authorization',localStorage.getItem('tokenNo'));
        this.httpClient.get(this.apiURL+path+this.convertToParam(json),{headers:headers, responseType: 'arraybuffer'}).subscribe((s)=>{
          const downloadLink = document.createElement('a');
          downloadLink.href = URL.createObjectURL(new Blob([s], {type: 'application/octet-stream'}));
          downloadLink.setAttribute('download',fileName+".pdf");
          downloadLink.click();
          resolve();
        },e=>{
          this.error(e);
        });
      }else if(http == EHttp.REPORT_XLS){
        headers = headers.set('Authorization',localStorage.getItem('tokenNo'));
        this.httpClient.get(this.apiURL+path+this.convertToParam(json),{headers:headers, responseType: 'arraybuffer'}).subscribe((s)=>{
          const downloadLink = document.createElement('a');
          downloadLink.href = URL.createObjectURL(new Blob([s], {type: 'application/octet-stream'}));
          downloadLink.setAttribute('download',fileName+".xlsx");
          downloadLink.click();
          resolve();
        },e=>{
          this.error(e);
        });
      }else if(http == EHttp.REPORT_DOCX){
        headers = headers.set('Authorization',localStorage.getItem('tokenNo'));
        this.httpClient.get(this.apiURL+path+this.convertToParam(json),{headers:headers, responseType: 'arraybuffer'}).subscribe((s)=>{
          const downloadLink = document.createElement('a');
          downloadLink.href = URL.createObjectURL(new Blob([s], {type: 'application/octet-stream'}));
          downloadLink.setAttribute('download',fileName+".docx");
          downloadLink.click();
          resolve();
        },e=>{
          this.error(e);
        });
      }
    });
  }

  private error(e:any){
    if(e.status == 502 || e.status == 417){
      alert(e.error.message||e.error);
      localStorage.clear();
      this.router.navigate(['/login']);
      return;
    }
    alert(e.error.message||e.error);
  }
}
