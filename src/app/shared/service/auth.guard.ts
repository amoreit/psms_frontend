import { Injectable } from '@angular/core';
import { CanActivate, CanActivateChild, Router, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';

@Injectable({
    providedIn: 'root'
  })
export class AuthGuard implements CanActivate {

    constructor(private router : Router){}

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
        let url: string = state.url;  
        return this.verifyLogin(url);
    }

    verifyLogin(url) : boolean{
      if((localStorage.getItem("tokenNo")||'') == ''){
        this.router.navigate(['/login']);
        return false;
      }

      if (url == '/dashboard' || url == '/adm/profile-user' || url == '/adm/adm007') {
        return true;
      }

      let url_link = JSON.parse(localStorage.getItem("menu")).filter(o => { return (o.routerLink == url.substring(0, 11) || o.routerLink == url.substring(0, 15) || o.routerLink == url.substring(0, 17) || o.routerLink == url.substring(0, 20)); })
      if (url_link.length == 0) {
        this.router.navigate(['/dashboard']);
        return false;
      }
      //   if(JSON.parse(localStorage.getItem("page")).filter(o=>{return ('/'+o.pageUrl == url);}).length == 0){
      //     this.router.navigate(['/']);
      //     return false;
      //   }
      return true;
    }
}