import { Injectable } from '@angular/core';
import { $ } from 'protractor';

@Injectable({
  providedIn: 'root'
})
export class UtilService {

  constructor() { }

  dateyearplus(dateStr: string, formatBC: boolean, splitter: string = '/', num:number){
    if (dateStr != null && dateStr != '') {
      dateStr = dateStr.substring(0, 10);
      if (dateStr.length == 10) {
        var arr = dateStr.split('-');
        if (arr.length == 3 && arr[0].length == 4 && arr[1].length == 2 && arr[2].length == 2) {
          var year = parseInt(arr[0]);
          if (!isNaN(parseInt(arr[2])) && !isNaN(parseInt(arr[1])) && !isNaN(year)){
              if (formatBC) {
                if (year < 2362) {
                  year += 543;
                }
              } else {
                if (year > 2219) {
                  year -= 543;
                }
              }
              year+=num;
              return this.twoDigit(arr[2]) + splitter + this.twoDigit(arr[1]) + splitter + year;
          }
        }
      }
    }
  
    return '';

  }

  formatDateForSQL(date: Date) {
    return date.getFullYear() + "-" + this.twoDigit(date.getMonth() + 1) +"-" + this.twoDigit(date.getDate());
  }

  formatDateSQLToDate(sqlDate: string) {
    return new Date(sqlDate);
  }

  twoDigit(digit: any) {
    digit = parseInt(digit);
    return digit < 10 ? '0' + digit : new String(digit);
  }

  stringNotNull(str: string) {
    return str == null ? '' : str;
  }

  activeMenu(menuId: string) {
    var menuItems = document.getElementsByClassName('rp-menu-item');
    for (var i = 0; i < menuItems.length; i++) {
      menuItems[i].classList.remove('active');
    }
    var menuItem = document.getElementById(menuId);
    if (menuItem) {
      document.getElementById(menuId).classList.add('active');
    }
  }

  encodeString(str: string) {
    return btoa(encodeURIComponent(str));
  }

  //Format: dd/mm/yyyy - Ex. 31/01/2019 or 31/01/2562
  formatDateStringForSQL(dateStr: string) {
    if (dateStr != null && dateStr != '') {
      if (dateStr.length == 10) {
        var arr = dateStr.split('/');
        if (arr.length == 3 && arr[0].length == 2 && arr[1].length == 2 && arr[2].length == 4) {
          var year = parseInt(arr[2]);
          if (!isNaN(parseInt(arr[0])) && !isNaN(parseInt(arr[1])) && !isNaN(year)){
              if (year > 2219) {
                year -= 543;
              }
              return year + '-' + this.twoDigit(arr[1]) + '-' + this.twoDigit(arr[0]);
          }
        }
      }
    }

    return '';
  }

 //Format: yyyy-mm-dd - Ex. 2019-01-31
 formatDateSQLToString(dateStr: string, formatBC: boolean, splitter: string = '/') {
  if (dateStr != null && dateStr != '') {
    dateStr = dateStr.substring(0, 10);
    if (dateStr.length == 10) {
      var arr = dateStr.split('-');
      if (arr.length == 3 && arr[0].length == 4 && arr[1].length == 2 && arr[2].length == 2) {
        var year = parseInt(arr[0]);
        if (!isNaN(parseInt(arr[2])) && !isNaN(parseInt(arr[1])) && !isNaN(year)){
            if (formatBC) {
              if (year < 2362) {
                year += 543;
              }
            } else {
              if (year > 2219) {
                year -= 543;
              }
            }
            return this.twoDigit(arr[2]) + splitter + this.twoDigit(arr[1]) + splitter + year;
        }
      }
    }
  }

  return '';
}
  //Format: dd.mm.yyyy - Ex. 31.01.2019
  formatDateTableToString(dateStr: string, formatBC: boolean) {
    if (dateStr != null && dateStr != '') {
      if (dateStr.length == 10) {
        var arr = dateStr.split('.');
        if (arr.length == 3 && arr[0].length == 2 && arr[1].length == 2 && arr[2].length == 4) {
          var year = parseInt(arr[2]);
          if (!isNaN(parseInt(arr[0])) && !isNaN(parseInt(arr[1])) && !isNaN(year)){
              if (formatBC) {
                if (year < 2362) {
                  year += 543;
                }
              } else {
                if (year > 2219) {
                  year -= 543;
                }
              }
              return this.twoDigit(arr[0]) + '/' + this.twoDigit(arr[1]) + '/' + year;
          }
        }
      }
    }

    return '';
  }

  formatFloat(txt,num){
    if(txt == '' || txt == null){
      return;
    }else{
      return parseFloat(txt).toFixed(num)
    }
  }
  
  transform(date: Date) {
    let ThaiDay = ['อาทิตย์','จันทร์','อังคาร','พุธ','พฤหัสบดี','ศุกร์','เสาร์']
    let shortThaiMonth = [
        '01','02','03','04','05','06',
        '07','08','09','10','11','12'
        ];  
    let longThaiMonth = [
        '01','02','03','04','05','06',
        '07','08','09','10','11','12'
        ];   
  
    let inputDate=new Date(date);
    let dataDate = [
        inputDate.getDay(),inputDate.getDate(),inputDate.getMonth(),inputDate.getFullYear()
        ];
    let outputDateFull = [
        'วัน '+ThaiDay[dataDate[0]],
        'ที่ '+dataDate[1],
        'เดือน '+longThaiMonth[dataDate[2]],
        'พ.ศ. '+(dataDate[3]+543)
    ];
    let outputDateShort = [
      this.twoDigit(dataDate[1]),
        shortThaiMonth[dataDate[2]],
        dataDate[3]+543
    ];
    let outputDateMedium = [
        dataDate[1],
        longThaiMonth[dataDate[2]],
        dataDate[3]+543
    ];    
    let returnDate:string;
    returnDate = outputDateMedium.join("/");
    // if(format=='full'){
    //     returnDate = outputDateFull.join(" ");
    // }    
    // if(format=='medium'){
    //     returnDate = outputDateMedium.join(" ");
    // }      
    // if(format=='short'){
    //     returnDate = outputDateShort.join("/");
    // }    
    return returnDate;
  }

  transformEng(date: Date, format: string): string {
    let EngDay = ['sunday', 'monday', 'tuesday', 'wednesday', 'thursday', 'friday', 'saturday']
    let shortThaiMonth = [
      '01', '02', '03', '04', '05', '06',
      '07', '08', '09', '10', '11', '12'
    ];
    let longEngMonth = [
      'January', 'February', 'March', 'April', 'May', 'June',
      '่July', 'August', 'September', 'October', 'November', 'December'
    ];

    let inputDate = new Date(date);
    let dataDate = [
      inputDate.getDay(), inputDate.getDate(), inputDate.getMonth(), inputDate.getFullYear()
    ];
    let outputDateFull = [
      'day ' + EngDay[dataDate[0]],
      'at ' + dataDate[1],
      'month ' + longEngMonth[dataDate[2]],
      'year ' + (dataDate[3] + 543)
    ];
    let outputDateShort = [
      this.twoDigit(dataDate[1]),
      shortThaiMonth[dataDate[2]],
      dataDate[3] - 543
    ];
    let outputDateMedium = [
      dataDate[1],
      longEngMonth[dataDate[2]],
      dataDate[3] - 543
    ];
    let outputDateYear = [
      longEngMonth[dataDate[2]],
      dataDate[1] + ',',
      +(dataDate[3])
    ];
    let returnDate: string;
    returnDate = outputDateYear.join(" ");
    if (format == 'full') {
      returnDate = outputDateFull.join(" ");
    }
    if (format == 'medium') {
      returnDate = outputDateMedium.join(" ");
    }
    if (format == 'short') {
      returnDate = outputDateShort.join(" ");
    }
    return returnDate;
  }

  transformThai(date: Date, format: string): string {
    let ThaiDay = ['อาทิตย์', 'จันทร์', 'อังคาร', 'พุธ', 'พฤหัสบดี', 'ศุกร์', 'เสาร์']
    let shortThaiMonth = [
      '01', '02', '03', '04', '05', '06',
      '07', '08', '09', '10', '11', '12'
    ];
    let longThaiMonth = [
      'มกราคม', 'กุมภาพันธ์', 'มีนาคม', 'เมษายน', 'พฤษภาคม', 'มิถุนายน',
      'กรกฎาคม', 'สิงหาคม', 'กันยายน', 'ตุลาคม', 'พฤศจิกายน', 'ธันวาคม'
    ];

    let inputDate = new Date(date);
    let dataDate = [
      inputDate.getDay(), inputDate.getDate(), inputDate.getMonth(), inputDate.getFullYear()
    ];
    let outputDateFull = [
      'วัน ' + ThaiDay[dataDate[0]],
      'ที่ ' + dataDate[1],
      'เดือน ' + longThaiMonth[dataDate[2]],
      'พ.ศ. ' + (dataDate[3] + 543)
    ];
    let outputDateShort = [
      this.twoDigit(dataDate[1]),
      shortThaiMonth[dataDate[2]],
      dataDate[3] + 543
    ];
    let outputDateMedium = [
      dataDate[1],
      longThaiMonth[dataDate[2]],
      dataDate[3] + 543
    ];
    let outputDateYear = [
      dataDate[1],
      longThaiMonth[dataDate[2]],
      'พ.ศ. ' + (dataDate[3] + 543)
    ];

    let returnDate: string;
    returnDate = outputDateYear.join(" ");
    if (format == 'full') {
      returnDate = outputDateFull.join(" ");
    }
    if (format == 'medium') {
      returnDate = outputDateMedium.join(" ");
    }
    if (format == 'short') {
      returnDate = outputDateShort.join(" ");
    }
    return returnDate;
  }
  
}
