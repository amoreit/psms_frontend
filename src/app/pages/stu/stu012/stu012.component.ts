import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormControl } from '@angular/forms';
import { ApiService, IResponse } from 'src/app/shared/service/api.service';
import { Router } from '@angular/router';
import { MatTableDataSource, MatPaginator } from '@angular/material';
import { UtilService } from 'src/app/_util/util.service';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-stu012',
  templateUrl: './stu012.component.html',
  styleUrls: ['./stu012.component.scss']
})
export class Stu012Component implements OnInit {

  isProcess = false;
  searchForm = this.fb.group({
    yearGroupId: [''],
    yearName: [''],
    classId: [''],
    className: [''],
    classRoomId: [''],
    roomName: [''],
    condition: [''],
    startDate: [''],
    endDate: [''],
    p: [''], 
    result: [10]
  }); 

  pageSize: number;
  displayedColumns: string[] = [
    'no',
    'stuCode',
    'firstNameTh',
    'time'
  ];

  pageSizeAll: number;
  displayedColumnsAll: string[] = [
    'no',
    'stuCode',
    'firstNameTh',
    // 'doorNo',
    'firstTime',
    'lastTime'
  ];

  dataSource = new MatTableDataSource();
  dataSourceAll = new MatTableDataSource();

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;

  yearList: [];
  classList: [];
  classRoomList: [];
  classRoomMemberId: [];

  constructor(
    private fb: FormBuilder,
    private service: ApiService,
    private router: Router,
    private translate: TranslateService,
    public utilService: UtilService
  ) { }

  ngOnInit() {
    this.onSearch(0);
    this.ddlYear();
    this.ddlClass();
    this.ddlClassRoom();

  }

  onSearch(e) {
    if (this.searchForm.value.yearGroupId != '') {
      if (this.searchForm.value.startDate == '' && this.searchForm.value.endDate == '') {
        this.searchForm.controls.startDate.setValue(this.utilService.formatDateForSQL(new Date()));
        this.searchForm.controls.endDate.setValue(this.utilService.formatDateForSQL(new Date()));
        this.searchForm.value.p = (this.paginator.pageIndex = e.pageIndex || 0) + 1;
        this.service
          .httpGet('/api/v1/tbstutimeattendance', this.searchForm.value)
          .then((res: IResponse) => {
            this.dataSource.data = res.responseData || [];
            this.pageSize = res.responseSize || 0;

          });

        this.service
          .httpGet('/api/v1/tbstutimeattendance', this.searchForm.value)
          .then((res: IResponse) => {
            this.dataSourceAll.data = res.responseData || [];
            this.pageSizeAll = res.responseSize || 0;

          });

      } else if (this.searchForm.value.startDate != '' && this.searchForm.value.endDate == '') {
        this.searchForm.controls.startDate.setValue(this.utilService.formatDateForSQL(new Date(this.searchForm.value.startDate)));
        this.searchForm.controls.endDate.setValue(this.utilService.formatDateForSQL(new Date()));
        this.searchForm.value.p = (this.paginator.pageIndex = e.pageIndex || 0) + 1;
        this.service
          .httpGet('/api/v1/tbstutimeattendance', this.searchForm.value)
          .then((res: IResponse) => {
            this.dataSource.data = res.responseData || [];
            this.pageSize = res.responseSize || 0;

          });

        this.service
          .httpGet('/api/v1/tbstutimeattendance', this.searchForm.value)
          .then((res: IResponse) => {
            this.dataSourceAll.data = res.responseData || [];
            this.pageSizeAll = res.responseSize || 0;

          });

      } else if (this.searchForm.value.startDate == '' && this.searchForm.value.endDate != '') {
        this.searchForm.controls.startDate.setValue(this.utilService.formatDateForSQL(new Date()));
        this.searchForm.controls.endDate.setValue(this.utilService.formatDateForSQL(new Date(this.searchForm.value.endDate)));
        this.searchForm.value.p = (this.paginator.pageIndex = e.pageIndex || 0) + 1;
        this.service
          .httpGet('/api/v1/tbstutimeattendance', this.searchForm.value)
          .then((res: IResponse) => {
            this.dataSource.data = res.responseData || [];
            this.pageSize = res.responseSize || 0;

          });

        this.service
          .httpGet('/api/v1/tbstutimeattendance', this.searchForm.value)
          .then((res: IResponse) => {
            this.dataSourceAll.data = res.responseData || [];
            this.pageSizeAll = res.responseSize || 0;

          });

      } else if (this.searchForm.value.startDate != '' && this.searchForm.value.endDate != '') {
        this.searchForm.controls.startDate.setValue(this.utilService.formatDateForSQL(new Date(this.searchForm.value.startDate)));
        this.searchForm.controls.endDate.setValue(this.utilService.formatDateForSQL(new Date(this.searchForm.value.endDate)));
        this.searchForm.value.p = (this.paginator.pageIndex = e.pageIndex || 0) + 1;
        this.service
          .httpGet('/api/v1/tbstutimeattendance', this.searchForm.value)
          .then((res: IResponse) => {
            this.dataSource.data = res.responseData || [];
            this.pageSize = res.responseSize || 0;

          });

        this.service
          .httpGet('/api/v1/tbstutimeattendance', this.searchForm.value)
          .then((res: IResponse) => {
            this.dataSourceAll.data = res.responseData || [];
            this.pageSizeAll = res.responseSize || 0;

          });
      }
    }
  }

  onReport() {
    this.isProcess = true;

    if (this.searchForm.value.condition == '' || this.searchForm.value.condition == '2') {

      if (this.searchForm.value.yearGroupId == '') {
        if (this.searchForm.value.classId == '') {
          if (this.searchForm.value.classRoomId == '') {

          } else if (this.searchForm.value.classRoomId != '') {
            const jsonRoom = this.classRoomList.filter((o) => {
              return o['classRoomId'] == this.searchForm.value.classRoomId;
            });
            this.searchForm.value.roomName = jsonRoom[0]['classRoomSnameTh'];

          }
        } else if (this.searchForm.value.classId != '') {
          if (this.searchForm.value.classRoomId == '') {
            const jsonClass = this.classList.filter((o) => {
              return o['classId'] == this.searchForm.value.classId;
            });
            this.searchForm.value.className = jsonClass[0]['classSnameTh'];

          } else if (this.searchForm.value.classRoomId != '') {
            const jsonClass = this.classList.filter((o) => {
              return o['classId'] == this.searchForm.value.classId;
            });
            this.searchForm.value.className = jsonClass[0]['classSnameTh'];

            const jsonRoom = this.classRoomList.filter((o) => {
              return o['classRoomId'] == this.searchForm.value.classRoomId;
            });
            this.searchForm.value.roomName = jsonRoom[0]['classRoomSnameTh'];

          }
        }
      } else if (this.searchForm.value.yearGroupId != '') {
        if (this.searchForm.value.classId == '') {
          if (this.searchForm.value.classRoomId == '') {
            const jsonYear = this.yearList.filter((o) => {
              return o['yearGroupId'] == this.searchForm.value.yearGroupId;
            });
            this.searchForm.value.yearName = jsonYear[0]['name'];

          } else if (this.searchForm.value.classRoomId != '') {
            const jsonYear = this.yearList.filter((o) => {
              return o['yearGroupId'] == this.searchForm.value.yearGroupId;
            });
            this.searchForm.value.yearName = jsonYear[0]['name'];

            const jsonRoom = this.classRoomList.filter((o) => {
              return o['classRoomId'] == this.searchForm.value.classRoomId;
            });
            this.searchForm.value.roomName = jsonRoom[0]['classRoomSnameTh'];

          }
        } else if (this.searchForm.value.classId != '') {
          if (this.searchForm.value.classRoomId == '') {
            const jsonYear = this.yearList.filter((o) => {
              return o['yearGroupId'] == this.searchForm.value.yearGroupId;
            });
            this.searchForm.value.yearName = jsonYear[0]['name'];

            const jsonClass = this.classList.filter((o) => {
              return o['classId'] == this.searchForm.value.classId;
            });
            this.searchForm.value.className = jsonClass[0]['classSnameTh'];

          } else if (this.searchForm.value.classRoomId != '') {
            const jsonYear = this.yearList.filter((o) => {
              return o['yearGroupId'] == this.searchForm.value.yearGroupId;
            });
            this.searchForm.value.yearName = jsonYear[0]['name'];

            const jsonClass = this.classList.filter((o) => {
              return o['classId'] == this.searchForm.value.classId;
            });
            this.searchForm.value.className = jsonClass[0]['classSnameTh'];

            const jsonRoom = this.classRoomList.filter((o) => {
              return o['classRoomId'] == this.searchForm.value.classRoomId;
            });
            this.searchForm.value.roomName = jsonRoom[0]['classRoomSnameTh'];

          }
        }
      }

      this.service
        .httpPreviewPDF('/api/v1/stu012/report/stu012_2/pdf', this.searchForm.value)
        .then(res => {
          this.isProcess = false;
          window.location.href = res;
        });

    } else if (this.searchForm.value.condition == '1') {
      if (this.searchForm.value.yearGroupId == '') {
        if (this.searchForm.value.classId == '') {
          if (this.searchForm.value.classRoomId == '') {

          } else if (this.searchForm.value.classRoomId != '') {
            const jsonRoom = this.classRoomList.filter((o) => {
              return o['classRoomId'] == this.searchForm.value.classRoomId;
            });
            this.searchForm.value.roomName = jsonRoom[0]['classRoomSnameTh'];

          }
        } else if (this.searchForm.value.classId != '') {
          if (this.searchForm.value.classRoomId == '') {
            const jsonClass = this.classList.filter((o) => {
              return o['classId'] == this.searchForm.value.classId;
            });
            this.searchForm.value.className = jsonClass[0]['classSnameTh'];

          } else if (this.searchForm.value.classRoomId != '') {
            const jsonClass = this.classList.filter((o) => {
              return o['classId'] == this.searchForm.value.classId;
            });
            this.searchForm.value.className = jsonClass[0]['classSnameTh'];


            const jsonRoom = this.classRoomList.filter((o) => {
              return o['classRoomId'] == this.searchForm.value.classRoomId;
            });
            this.searchForm.value.roomName = jsonRoom[0]['classRoomSnameTh'];

          }
        }
      } else if (this.searchForm.value.yearGroupId != '') {
        if (this.searchForm.value.classId == '') {
          if (this.searchForm.value.classRoomId == '') {
            const jsonYear = this.yearList.filter((o) => {
              return o['yearGroupId'] == this.searchForm.value.yearGroupId;
            });
            this.searchForm.value.yearName = jsonYear[0]['name'];

          } else if (this.searchForm.value.classRoomId != '') {
            const jsonYear = this.yearList.filter((o) => {
              return o['yearGroupId'] == this.searchForm.value.yearGroupId;
            });
            this.searchForm.value.yearName = jsonYear[0]['name'];

            const jsonRoom = this.classRoomList.filter((o) => {
              return o['classRoomId'] == this.searchForm.value.classRoomId;
            });
            this.searchForm.value.roomName = jsonRoom[0]['classRoomSnameTh'];

          }
        } else if (this.searchForm.value.classId != '') {
          if (this.searchForm.value.classRoomId == '') {
            const jsonYear = this.yearList.filter((o) => {
              return o['yearGroupId'] == this.searchForm.value.yearGroupId;
            });
            this.searchForm.value.yearName = jsonYear[0]['name'];


            const jsonClass = this.classList.filter((o) => {
              return o['classId'] == this.searchForm.value.classId;
            });
            this.searchForm.value.className = jsonClass[0]['classSnameTh'];

          } else if (this.searchForm.value.classRoomId != '') {
            const jsonYear = this.yearList.filter((o) => {
              return o['yearGroupId'] == this.searchForm.value.yearGroupId;
            });
            this.searchForm.value.yearName = jsonYear[0]['name'];


            const jsonClass = this.classList.filter((o) => {
              return o['classId'] == this.searchForm.value.classId;
            });
            this.searchForm.value.className = jsonClass[0]['classSnameTh'];


            const jsonRoom = this.classRoomList.filter((o) => {
              return o['classRoomId'] == this.searchForm.value.classRoomId;
            });
            this.searchForm.value.roomName = jsonRoom[0]['classRoomSnameTh'];

          }
        }
      }

      this.service
        .httpPreviewPDF('/api/v1/stu012/report/stu012_1/pdf', this.searchForm.value)
        .then(res => {
          this.isProcess = false;
          
        });
    }
  }

  onExportPDF() {
    this.isProcess = true;

    if (this.searchForm.value.condition == '' || this.searchForm.value.condition == '2') {

      if (this.searchForm.value.yearGroupId == '') {
        if (this.searchForm.value.classId == '') {
          if (this.searchForm.value.classRoomId == '') {

          } else if (this.searchForm.value.classRoomId != '') {
            const jsonRoom = this.classRoomList.filter((o) => {
              return o['classRoomId'] == this.searchForm.value.classRoomId;
            });
            this.searchForm.value.roomName = jsonRoom[0]['classRoomSnameTh'];

          }
        } else if (this.searchForm.value.classId != '') {
          if (this.searchForm.value.classRoomId == '') {
            const jsonClass = this.classList.filter((o) => {
              return o['classId'] == this.searchForm.value.classId;
            });
            this.searchForm.value.className = jsonClass[0]['classSnameTh'];

          } else if (this.searchForm.value.classRoomId != '') {
            const jsonClass = this.classList.filter((o) => {
              return o['classId'] == this.searchForm.value.classId;
            });
            this.searchForm.value.className = jsonClass[0]['classSnameTh'];


            const jsonRoom = this.classRoomList.filter((o) => {
              return o['classRoomId'] == this.searchForm.value.classRoomId;
            });
            this.searchForm.value.roomName = jsonRoom[0]['classRoomSnameTh'];

          }
        }
      } else if (this.searchForm.value.yearGroupId != '') {
        if (this.searchForm.value.classId == '') {
          if (this.searchForm.value.classRoomId == '') {
            const jsonYear = this.yearList.filter((o) => {
              return o['yearGroupId'] == this.searchForm.value.yearGroupId;
            });
            this.searchForm.value.yearName = jsonYear[0]['name'];

          } else if (this.searchForm.value.classRoomId != '') {
            const jsonYear = this.yearList.filter((o) => {
              return o['yearGroupId'] == this.searchForm.value.yearGroupId;
            });
            this.searchForm.value.yearName = jsonYear[0]['name'];

            const jsonRoom = this.classRoomList.filter((o) => {
              return o['classRoomId'] == this.searchForm.value.classRoomId;
            });
            this.searchForm.value.roomName = jsonRoom[0]['classRoomSnameTh'];

          }
        } else if (this.searchForm.value.classId != '') {
          if (this.searchForm.value.classRoomId == '') {
            const jsonYear = this.yearList.filter((o) => {
              return o['yearGroupId'] == this.searchForm.value.yearGroupId;
            });
            this.searchForm.value.yearName = jsonYear[0]['name'];


            const jsonClass = this.classList.filter((o) => {
              return o['classId'] == this.searchForm.value.classId;
            });
            this.searchForm.value.className = jsonClass[0]['classSnameTh'];

          } else if (this.searchForm.value.classRoomId != '') {
            const jsonYear = this.yearList.filter((o) => {
              return o['yearGroupId'] == this.searchForm.value.yearGroupId;
            });
            this.searchForm.value.yearName = jsonYear[0]['name'];


            const jsonClass = this.classList.filter((o) => {
              return o['classId'] == this.searchForm.value.classId;
            });
            this.searchForm.value.className = jsonClass[0]['classSnameTh'];


            const jsonRoom = this.classRoomList.filter((o) => {
              return o['classRoomId'] == this.searchForm.value.classRoomId;
            });
            this.searchForm.value.roomName = jsonRoom[0]['classRoomSnameTh'];

          }
        }
      }

      this.service
        .httpReportPDF('/api/v1/stu012/report/stu012_2/pdf', 'รายงานเวลาเข้า-ออก โรงเรียนนักเรียน เฉพาะ First/Last', this.searchForm.value)
        .then(res => {
          this.isProcess = false;

        });

    } else if (this.searchForm.value.condition == '1') {
      if (this.searchForm.value.yearGroupId == '') {
        if (this.searchForm.value.classId == '') {
          if (this.searchForm.value.classRoomId == '') {

          } else if (this.searchForm.value.classRoomId != '') {
            const jsonRoom = this.classRoomList.filter((o) => {
              return o['classRoomId'] == this.searchForm.value.classRoomId;
            });
            this.searchForm.value.roomName = jsonRoom[0]['classRoomSnameTh'];

          }
        } else if (this.searchForm.value.classId != '') {
          if (this.searchForm.value.classRoomId == '') {
            const jsonClass = this.classList.filter((o) => {
              return o['classId'] == this.searchForm.value.classId;
            });
            this.searchForm.value.className = jsonClass[0]['classSnameTh'];

          } else if (this.searchForm.value.classRoomId != '') {
            const jsonClass = this.classList.filter((o) => {
              return o['classId'] == this.searchForm.value.classId;
            });
            this.searchForm.value.className = jsonClass[0]['classSnameTh'];


            const jsonRoom = this.classRoomList.filter((o) => {
              return o['classRoomId'] == this.searchForm.value.classRoomId;
            });
            this.searchForm.value.roomName = jsonRoom[0]['classRoomSnameTh'];

          }
        }
      } else if (this.searchForm.value.yearGroupId != '') {
        if (this.searchForm.value.classId == '') {
          if (this.searchForm.value.classRoomId == '') {
            const jsonYear = this.yearList.filter((o) => {
              return o['yearGroupId'] == this.searchForm.value.yearGroupId;
            });
            this.searchForm.value.yearName = jsonYear[0]['name'];

          } else if (this.searchForm.value.classRoomId != '') {
            const jsonYear = this.yearList.filter((o) => {
              return o['yearGroupId'] == this.searchForm.value.yearGroupId;
            });
            this.searchForm.value.yearName = jsonYear[0]['name'];

            const jsonRoom = this.classRoomList.filter((o) => {
              return o['classRoomId'] == this.searchForm.value.classRoomId;
            });
            this.searchForm.value.roomName = jsonRoom[0]['classRoomSnameTh'];

          }
        } else if (this.searchForm.value.classId != '') {
          if (this.searchForm.value.classRoomId == '') {
            const jsonYear = this.yearList.filter((o) => {
              return o['yearGroupId'] == this.searchForm.value.yearGroupId;
            });
            this.searchForm.value.yearName = jsonYear[0]['name'];


            const jsonClass = this.classList.filter((o) => {
              return o['classId'] == this.searchForm.value.classId;
            });
            this.searchForm.value.className = jsonClass[0]['classSnameTh'];

          } else if (this.searchForm.value.classRoomId != '') {
            const jsonYear = this.yearList.filter((o) => {
              return o['yearGroupId'] == this.searchForm.value.yearGroupId;
            });
            this.searchForm.value.yearName = jsonYear[0]['name'];


            const jsonClass = this.classList.filter((o) => {
              return o['classId'] == this.searchForm.value.classId;
            });
            this.searchForm.value.className = jsonClass[0]['classSnameTh'];


            const jsonRoom = this.classRoomList.filter((o) => {
              return o['classRoomId'] == this.searchForm.value.classRoomId;
            });
            this.searchForm.value.roomName = jsonRoom[0]['classRoomSnameTh'];

          }
        }
      }

      this.service
        .httpReportPDF('/api/v1/stu012/report/stu012_1/pdf', 'รายงานเวลาเข้า-ออก โรงเรียนนักเรียน ทุก Transection', this.searchForm.value)
        .then(res => {
          this.isProcess = false;
         
        });
    }

  }

  onExportXLS() {
    this.isProcess = true;

    if (this.searchForm.value.condition == '' || this.searchForm.value.condition == '2') {

      if (this.searchForm.value.yearGroupId == '') {
        if (this.searchForm.value.classId == '') {
          if (this.searchForm.value.classRoomId == '') {

          } else if (this.searchForm.value.classRoomId != '') {
            const jsonRoom = this.classRoomList.filter((o) => {
              return o['classRoomId'] == this.searchForm.value.classRoomId;
            });
            this.searchForm.value.roomName = jsonRoom[0]['classRoomSnameTh'];

          }
        } else if (this.searchForm.value.classId != '') {
          if (this.searchForm.value.classRoomId == '') {
            const jsonClass = this.classList.filter((o) => {
              return o['classId'] == this.searchForm.value.classId;
            });
            this.searchForm.value.className = jsonClass[0]['classSnameTh'];

          } else if (this.searchForm.value.classRoomId != '') {
            const jsonClass = this.classList.filter((o) => {
              return o['classId'] == this.searchForm.value.classId;
            });
            this.searchForm.value.className = jsonClass[0]['classSnameTh'];


            const jsonRoom = this.classRoomList.filter((o) => {
              return o['classRoomId'] == this.searchForm.value.classRoomId;
            });
            this.searchForm.value.roomName = jsonRoom[0]['classRoomSnameTh'];

          }
        }
      } else if (this.searchForm.value.yearGroupId != '') {
        if (this.searchForm.value.classId == '') {
          if (this.searchForm.value.classRoomId == '') {
            const jsonYear = this.yearList.filter((o) => {
              return o['yearGroupId'] == this.searchForm.value.yearGroupId;
            });
            this.searchForm.value.yearName = jsonYear[0]['name'];

          } else if (this.searchForm.value.classRoomId != '') {
            const jsonYear = this.yearList.filter((o) => {
              return o['yearGroupId'] == this.searchForm.value.yearGroupId;
            });
            this.searchForm.value.yearName = jsonYear[0]['name'];

            const jsonRoom = this.classRoomList.filter((o) => {
              return o['classRoomId'] == this.searchForm.value.classRoomId;
            });
            this.searchForm.value.roomName = jsonRoom[0]['classRoomSnameTh'];

          }
        } else if (this.searchForm.value.classId != '') {
          if (this.searchForm.value.classRoomId == '') {
            const jsonYear = this.yearList.filter((o) => {
              return o['yearGroupId'] == this.searchForm.value.yearGroupId;
            });
            this.searchForm.value.yearName = jsonYear[0]['name'];


            const jsonClass = this.classList.filter((o) => {
              return o['classId'] == this.searchForm.value.classId;
            });
            this.searchForm.value.className = jsonClass[0]['classSnameTh'];

          } else if (this.searchForm.value.classRoomId != '') {
            const jsonYear = this.yearList.filter((o) => {
              return o['yearGroupId'] == this.searchForm.value.yearGroupId;
            });
            this.searchForm.value.yearName = jsonYear[0]['name'];


            const jsonClass = this.classList.filter((o) => {
              return o['classId'] == this.searchForm.value.classId;
            });
            this.searchForm.value.className = jsonClass[0]['classSnameTh'];


            const jsonRoom = this.classRoomList.filter((o) => {
              return o['classRoomId'] == this.searchForm.value.classRoomId;
            });
            this.searchForm.value.roomName = jsonRoom[0]['classRoomSnameTh'];

          }
        }
      }

      this.service
        .httpReportXLS('/api/v1/stu012/report/stu012_2/xls', 'รายงานเวลาเข้า-ออก โรงเรียนนักเรียน เฉพาะ First/Last', this.searchForm.value)
        .then(res => {
          this.isProcess = false;

        });

    } else if (this.searchForm.value.condition == '1') {
      if (this.searchForm.value.yearGroupId == '') {
        if (this.searchForm.value.classId == '') {
          if (this.searchForm.value.classRoomId == '') {

          } else if (this.searchForm.value.classRoomId != '') {
            const jsonRoom = this.classRoomList.filter((o) => {
              return o['classRoomId'] == this.searchForm.value.classRoomId;
            });
            this.searchForm.value.roomName = jsonRoom[0]['classRoomSnameTh'];

          }
        } else if (this.searchForm.value.classId != '') {
          if (this.searchForm.value.classRoomId == '') {
            const jsonClass = this.classList.filter((o) => {
              return o['classId'] == this.searchForm.value.classId;
            });
            this.searchForm.value.className = jsonClass[0]['classSnameTh'];

          } else if (this.searchForm.value.classRoomId != '') {
            const jsonClass = this.classList.filter((o) => {
              return o['classId'] == this.searchForm.value.classId;
            });
            this.searchForm.value.className = jsonClass[0]['classSnameTh'];


            const jsonRoom = this.classRoomList.filter((o) => {
              return o['classRoomId'] == this.searchForm.value.classRoomId;
            });
            this.searchForm.value.roomName = jsonRoom[0]['classRoomSnameTh'];

          }
        }
      } else if (this.searchForm.value.yearGroupId != '') {
        if (this.searchForm.value.classId == '') {
          if (this.searchForm.value.classRoomId == '') {
            const jsonYear = this.yearList.filter((o) => {
              return o['yearGroupId'] == this.searchForm.value.yearGroupId;
            });
            this.searchForm.value.yearName = jsonYear[0]['name'];

          } else if (this.searchForm.value.classRoomId != '') {
            const jsonYear = this.yearList.filter((o) => {
              return o['yearGroupId'] == this.searchForm.value.yearGroupId;
            });
            this.searchForm.value.yearName = jsonYear[0]['name'];

            const jsonRoom = this.classRoomList.filter((o) => {
              return o['classRoomId'] == this.searchForm.value.classRoomId;
            });
            this.searchForm.value.roomName = jsonRoom[0]['classRoomSnameTh'];

          }
        } else if (this.searchForm.value.classId != '') {
          if (this.searchForm.value.classRoomId == '') {
            const jsonYear = this.yearList.filter((o) => {
              return o['yearGroupId'] == this.searchForm.value.yearGroupId;
            });
            this.searchForm.value.yearName = jsonYear[0]['name'];


            const jsonClass = this.classList.filter((o) => {
              return o['classId'] == this.searchForm.value.classId;
            });
            this.searchForm.value.className = jsonClass[0]['classSnameTh'];

          } else if (this.searchForm.value.classRoomId != '') {
            const jsonYear = this.yearList.filter((o) => {
              return o['yearGroupId'] == this.searchForm.value.yearGroupId;
            });
            this.searchForm.value.yearName = jsonYear[0]['name'];


            const jsonClass = this.classList.filter((o) => {
              return o['classId'] == this.searchForm.value.classId;
            });
            this.searchForm.value.className = jsonClass[0]['classSnameTh'];


            const jsonRoom = this.classRoomList.filter((o) => {
              return o['classRoomId'] == this.searchForm.value.classRoomId;
            });
            this.searchForm.value.roomName = jsonRoom[0]['classRoomSnameTh'];

          }
        }
      }

      this.service
        .httpReportXLS('/api/v1/stu012/report/stu012_1/xls', 'รายงานเวลาเข้า-ออก โรงเรียนนักเรียน ทุก Transection', this.searchForm.value)
        .then(res => {
          this.isProcess = false;

        });
    }

  }

  onCancel() {
    this.searchForm = this.fb.group({
      yearGroupId: [this.ddlYear()],
      classId: [''],
      classRoomId: [''],
      condition: [''],
      startDate: [''],
      endDate: [''],
      p: [''],
      result: [10]
    });
    this.onSearch(0);

  }


  /* DROP DOWN ปีการศึกษา */
  ddlYear() {
    this.service.httpGet('/api/v1/0/year-group/ddl', null).then(res => {
      this.yearList = res || [];
      this.searchForm.controls.yearGroupId.setValue(res[0].yearGroupId);
    });
  }

  /* DROP DOWN ชั้นเรียน */
  ddlClass() {
    this.service.httpGet('/api/v1/0/class/classddl', null).then(res => {
      this.classList = res || [];
    });
  }

  /* DROP DOWN ห้องเรียน */
  ddlClassRoom() {
    const json = { classId: this.searchForm.value.classId };
    this.service
      .httpGet('/api/v1/0/classroom-list/ddlFilter', json)
      .then(res => {
        this.classRoomList = res || [];
      });
  }

}
