import { Component, OnInit, ViewChild, Inject } from '@angular/core';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { MatPaginator, MatTableDataSource, MAT_DIALOG_DATA, MatDialog } from '@angular/material';
import { ApiService,IResponse,EHttp } from '../../../shared/service/api.service';
import { Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-stu008',
  templateUrl: './stu008.component.html',
  styleUrls: ['./stu008.component.scss']
})
export class Stu008Component implements OnInit {
  isProcess:boolean = false;
  searchForm = this.fb.group({
      'yearGroupId': ['']
      ,'classId': ['']
      ,'religion': ['']
      ,'studentSummary':['']
      ,'chkReligion':['']
    , 'p':['']
    , 'result':[10]
  });
 
  report = this.fb.group({
    'yearGroupId':['']
    ,'religion': ['']
    ,'classRoomId': ['']
  });

  report2 = this.fb.group({
    'yearGroupId':['']
    ,'classRoomId': ['']
  });

  pageSize:number;
  displayedColumns: string[] = ['no', 'classRoom', 'Status'];
  dataSource = new MatTableDataSource();

  studentSummary = false;
  yearGroupList: [];
  classList: [];
  religionList: [];
  chkReligion:Boolean;

  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  constructor(
    private translate: TranslateService,
    private fb: FormBuilder,
    private service:ApiService,
    public dialog: MatDialog,
    private router: Router
  ) { }

  ngOnInit() {
    this.ddlYearGroup();
    this.ddlClass();
    this.ddlTbMstReligion();  
    this.onSearch(0);
  }

  onSearch(e){
    this.searchForm.value.p = (this.paginator.pageIndex = e.pageIndex||0)+1;
    this.service.httpGet('/api/v1/classroom/searchClassroomReligion',this.searchForm.value).then((res:IResponse)=>{
      this.dataSource.data = res.responseData||[];
      this.pageSize = res.responseSize||0;
    });
  }

  /* DROP DOWN ปีการศึกษา */
  ddlYearGroup() {
    this.service.httpGet('/api/v1/0/year-group/ddl', null).then((res) => {
      this.yearGroupList = res || [];
      let ele = this.yearGroupList.filter((o) => {
        return o['iscurrent'] == "true";
      });
      this.searchForm.controls.yearGroupId.setValue(ele[0]['yearGroupId'])
    });
  }
  /* DROP DOWN ชั้นเรียน */
  ddlClass() {
    this.service.httpGet('/api/v1/0/tbMstClass/ddl', null).then((res) => {
      this.classList = res || [];
    });
  }
  /* DROP DOWN ศาสนา */
  ddlTbMstReligion(){
    this.service.httpGet('/api/v1/0/tbMstReligion/ddl',null).then((res)=>{
        this.religionList = res||[];
    });
  }

  onReport(classRoomId){
    this.isProcess = true;
    if(this.searchForm.value.chkReligion == true){ 
      if(this.searchForm.value.religion == ''){
        this.report2.controls.classRoomId.setValue(classRoomId);
        this.report2.controls.yearGroupId.setValue(this.searchForm.value.yearGroupId);
        this.service.httpReportPDF('/api/v1/studentList/report/pdf','รายชื่อนักเรียน',this.report2.value).then((res)=>{
          this.isProcess = false;
        });
      }
      else{
        this.report.controls.classRoomId.setValue(classRoomId);
        this.report.controls.yearGroupId.setValue(this.searchForm.value.yearGroupId);
        this.report.controls.religion.setValue(this.searchForm.value.religion);
        this.service.httpReportPDF('/api/v1/studentList/report/pdf2','รายชื่อนักเรียน',this.report.value).then((res)=>{
          this.isProcess = false;
        });
      }
    }else{
      this.report2.controls.classRoomId.setValue(classRoomId);
      this.report2.controls.yearGroupId.setValue(this.searchForm.value.yearGroupId);
      this.service.httpReportPDF('/api/v1/studentList/report/pdf3','รายชื่อนักเรียน',this.report2.value).then((res)=>{
        this.isProcess = false;
      });
    }
  };

  onCancel(){
    this.searchForm.controls.yearGroupId.setValue('');
    this.searchForm.controls.classId.setValue('');
    this.searchForm.controls.religion.setValue('');
    this.onSearch(0);
    this.ddlYearGroup();
  }
}
