import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder } from '@angular/forms';
import { ApiService, IResponse } from 'src/app/shared/service/api.service';
import { MatDialog, MatTableDataSource, MatPaginator } from '@angular/material';
import { UtilService } from 'src/app/_util/util.service';
import Swal from 'sweetalert2/dist/sweetalert2.js';
import 'sweetalert2/src/sweetalert2.scss';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-stu007',
  templateUrl: './stu007.component.html',
  styleUrls: ['./stu007.component.scss']
})
export class Stu007Component implements OnInit {


  isProcess: boolean = false;
  searchForm = this.fb.group({
    'yearGroupId': ['']
    , 'classId': ['']
    , 'classRoomId': ['']
  });

  upgradeForm = this.fb.group({
    'yearGroupId': ['']
    , 'classId': ['']
    , 'classRoomId': ['']
    , 'copyDataActivity': [false]
  });
 
  pageSize: number;
  displayedColumns: string[] = ['no', 'stuCode', 'fullname', 'choose'];
  dataSource = new MatTableDataSource();
  classList: [];
  classroomnoList: [];
  yearGroupList: [];
  classUpgradeList: [];
  classroomnoUpgradeList: [];
  yearGroupUpgradeList: [];
  _allChecklist = [];
  checkdata = false;
  year_group_name: any;
  class_name: any;
  class_room_name: any;
  headerSelected: boolean;
  year_group_search: any;
  key_class: any;
  is_ep: any;

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  constructor(private translate: TranslateService,private fb: FormBuilder, private service: ApiService, public dialog: MatDialog, private router: Router, public _util: UtilService) {

  }

  ngOnInit() {
    this.ddlClass();
    this.ddlYearGroup();
    this.onSearch(0);

  }

  onSearch(e) {
    this.headerSelected = false;
    this.service.httpGet('/api/v1/trn-classroom-member/stuforstu007', this.searchForm.value).then((res: IResponse) => {
      if (res.responseData.length === 0) {
        this.dataSource.data = []
        this.checkdata = true;
      } else {
        this.dataSource.data = res.responseData || [];
        this.checkdata = false;
      }
    });
  }


  onCancel() {
    this.searchForm = this.fb.group({
      'yearGroupId': ['']
      , 'classId': ['']
    });
    this.onSearch(0);
  }


  ddlClass() {
    this.service.httpGet('/api/v1/0/class/ddlfunction', null).then((res) => {
      this.classList = res || [];
    });
  }


  ddlClassRoomNo() {
    let json = { 'classId': this.searchForm.value.classId };
    this.searchForm.controls.classRoomId.setValue('');
    this.service.httpGet('/api/v1/0/classroom/ddlByClassId', json).then((res) => {
      this.classroomnoList = res || [];
    });
  }

  ddlYearGroup() {
    this.service.httpGet('/api/v1/0/year-group/ddl', null).then((res) => {
      this.yearGroupList = res || [];
      let ele = this.yearGroupList.filter((o) => {
        return o['iscurrent'] == "true";
      });
      this.searchForm.controls.yearGroupId.setValue(ele[0]['yearGroupId'])
      this.checkYearGroup(this.searchForm.value.yearGroupId);
    });
  }

  checkYearGroup(txt) {
    let ele = this.yearGroupList.filter((o) => {
      return o['yearGroupId'] == txt;
    });
    this.year_group_name = ele[0]['name']
    this.year_group_search = this.year_group_name.substring(2) + this.year_group_name.substring(0, 1)

    this.service.httpGet('/api/v1/0/year-group/ddlupperyeargroup', { 'yearGroupSearch': this.year_group_search }).then((res) => {
      this.yearGroupUpgradeList = res || [];
    });
  }

  onCheckYearGroupUpgrade() {
    let ele = this.yearGroupUpgradeList.filter((o) => {
      return o['year_group_id'] == this.upgradeForm.value.yearGroupId;
    });
    this.year_group_name = ele[0]['name']
  }

  onCheckClassId() {
    let ele = this.classUpgradeList.filter((o) => {
      return o['class_id'] == this.upgradeForm.value.classId;
    });
    this.class_name = ele[0]['class_sname_th']
  }

  onCheckClassRoomId() {
    let ele = this.classroomnoUpgradeList.filter((o) => {
      return o['class_room_id'] == this.upgradeForm.value.classRoomId;
    });
    this.class_room_name = ele[0]['class_room_sname_th']
    this.is_ep = ele[0]['is_ep'];
  }

  onCheckClass(txt) {
    let ele = this.classList.filter((o) => {
      return o['class_id'] == txt;
    });
    this.key_class = ele[0]['key_class']
    this.service.httpGet('/api/v1/0/class/ddlfunctionbykey', { 'keyClass': this.key_class }).then((res) => {
      this.classUpgradeList = res || [];
    });
  }

  ddlClassRoomNoUpgrade() {
    let json = { 'classId': this.upgradeForm.value.classId };
    this.upgradeForm.controls.classRoomId.setValue('');
    this.service.httpGet('/api/v1/0/classroom/ddlByClassId', json).then((res) => {
      this.classroomnoUpgradeList = res || [];
    });
  }

  isCancelCheck() {
    for (var i = 0; i < this.dataSource.data.length; i++) {
      this.dataSource.data[i]['isSelected'] = false;
    }
    this.headerSelected = false;
    this._allChecklist = [];
  }

  checkAll() {
    for (var i = 0; i < this.dataSource.data.length; i++) {
      this.dataSource.data[i]['isSelected'] = this.headerSelected;
    }
    this.getCheckedItemList();
  }

  isAllSelected() {
    this.dataSource.data.every((item: any) => {
      return item.isSelected === true;
    })
    this.headerSelected = false;
    this.getCheckedItemList();
  }

  getCheckedItemList() {
    this._allChecklist = [];
    if (this.upgradeForm.value.copyDataActivity == false) {
      for (var i = 0; i < this.dataSource.data.length; i++) {
        if (this.dataSource.data[i]['isSelected']) {
          let itemData = {
            'scCode': this.dataSource.data[i]['sc_code'], 'yearGroupId': '',
            'yearGroupName': '', 'memberId': this.dataSource.data[i]['member_id'], 'stuCode': this.dataSource.data[i]['stu_code'],
            'fullname': this.dataSource.data[i]['fullname'], 'fullnameEn': this.dataSource.data[i]['fullname_en'], 'classId': '', 'className': '',
            'classRoomId': '', 'classRoom': '', 'role': this.dataSource.data[i]['role'], 'classRoomNo': null,
            'activity1id': null, 'activity1name': null, 'activity1lesson': null,
            'activity2id': null, 'activity2name': null, 'activity2lesson': null,
            'activity3id': null, 'activity3name': null, 'activity3lesson': null,
            'activity4id': null, 'activity4name': null, 'activity4lesson': null,
            'activity5id': null, 'activity5name': null, 'activity5lesson': null
          };
          this._allChecklist.push(itemData);
        }
      }
    } else {
      for (var i = 0; i < this.dataSource.data.length; i++) {
        if (this.dataSource.data[i]['isSelected']) {
          let itemData = {
            'scCode': this.dataSource.data[i]['sc_code'], 'yearGroupId': '',
            'yearGroupName': '', 'memberId': this.dataSource.data[i]['member_id'], 'stuCode': this.dataSource.data[i]['stu_code'],
            'fullname': this.dataSource.data[i]['fullname'], 'fullnameEn': this.dataSource.data[i]['fullname_en'], 'classId': '', 'className': '',
            'classRoomId': '', 'classRoom': '', 'role': this.dataSource.data[i]['role'], 'classRoomNo': null,
            'activity1id': this.dataSource.data[i]['activity1id'], 'activity1name': this.dataSource.data[i]['activity1name'], 'activity1lesson': this.dataSource.data[i]['activity1lesson'],
            'activity2id': this.dataSource.data[i]['activity2id'], 'activity2name': this.dataSource.data[i]['activity2name'], 'activity2lesson': this.dataSource.data[i]['activity2lesson'],
            'activity3id': this.dataSource.data[i]['activity3id'], 'activity3name': this.dataSource.data[i]['activity3name'], 'activity3lesson': this.dataSource.data[i]['activity3lesson'],
            'activity4id': this.dataSource.data[i]['activity4id'], 'activity4name': this.dataSource.data[i]['activity4name'], 'activity4lesson': this.dataSource.data[i]['activity4lesson'],
            'activity5id': this.dataSource.data[i]['activity5id'], 'activity5name': this.dataSource.data[i]['activity5name'], 'activity5lesson': this.dataSource.data[i]['activity5lesson']
          };
          this._allChecklist.push(itemData);
        }
      }
    }


  }

  onSave2() {
    this._allChecklist.map(e => {
      e['yearGroupId'] = this.upgradeForm.value.yearGroupId;
      e['yearGroupName'] = this.year_group_name;
      e['classId'] = this.upgradeForm.value.classId;
      e['className'] = this.class_name;
      e['classRoomId'] = this.upgradeForm.value.classRoomId;
      e['classRoom'] = this.class_room_name;
      e['isEp'] = this.is_ep;
      return e;
    });

    Swal.fire({
      title: this.translate.instant('alert.approve_class'),
      icon: 'info',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: this.translate.instant('psms.DL0008'),
      cancelButtonText: this.translate.instant('psms.DL0009')
    }).then((result) => {
      if (result.dismiss == 'cancel') {
        return;
      }
      let json = this._allChecklist;
      let user = localStorage.getItem('userName')
      if (json.length === 0 || this.upgradeForm.value.yearGroupId == '' || this.upgradeForm.value.classId == '' || this.upgradeForm.value.classRoomId == '') {
        Swal.fire(
          this.translate.instant('alert.approve_class_error'),
          this.translate.instant('alert.approve_class_selected'),
          'error'
        )
      } else {
        this.service.httpPost('/api/v1/trn-classroom-member/copy/' + user, json).then((res: IResponse) => {
          if ((res.responseCode || 500) != 200) {
            Swal.fire(
              this.translate.instant('alert.approve_class_error'),
              res.responseMsg,
              'error'
            )
            return;
          }
          Swal.fire(
            this.translate.instant('alert.approve_class_header'),
            this.translate.instant('alert.approve_class_success'),
            'success'
          ).then(() => { this.onSearch(0) })
        });
      }
      return;
    });
  }


}
