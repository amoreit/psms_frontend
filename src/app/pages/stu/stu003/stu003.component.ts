import { Component, OnInit, ViewChild, Inject } from '@angular/core';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { MatPaginator, MatTableDataSource, MAT_DIALOG_DATA, MatDialog } from '@angular/material';
import { ApiService, IResponse, EHttp } from '../../../shared/service/api.service';
import { Router } from '@angular/router';
import { UtilService } from 'src/app/_util/util.service';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-stu003',
  templateUrl: './stu003.component.html',
  styleUrls: ['./stu003.component.scss']
})
export class Stu003Component implements OnInit {

  isProcess: boolean = false;
  searchForm = this.fb.group({
    'toYearGroup': ['']
    , 'firstnameTh': ['']
    , 'lastnameTh': ['']
    , 'citizenId': [''] 
    , 'stuCode': ['']
    , 'toClass': ['']
    , 'stuStatusId': [''] 
    , 'p': ['']
    , 'result': [10]
  });

  report = this.fb.group({
    'stuProfileId': ['']
  });

  pageSize: number;
  displayedColumns: string[] = ['no', 'citizenNO', 'stuCode', 'title', 'firstNameTh', 'lastNameTh', 'class', 'stuStatus', 'Status'];
  dataSource = new MatTableDataSource();

  classList: [];
  yearGroupList: [];

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  constructor(
    private fb: FormBuilder,
    private service: ApiService,
    public dialog: MatDialog,
    private router: Router,
    private translate: TranslateService,
    public utilService: UtilService
  ) { }

  ngOnInit() {
    this.ddlClass();
    this.ddlYearGroup();
  }
  /* DROP DOWN ปีการศึกษา */
  ddlYearGroup() {
    this.service.httpGet('/api/v1/0/year-group/ddl', null).then((res) => {
      this.yearGroupList = res || [];
      let ele = this.yearGroupList.filter((o) => {
        return o['iscurrent'] == "true";
      });
      this.searchForm.controls.toYearGroup.setValue(ele[0]['name'])
      this.searchForm.controls.stuStatusId.setValue(0);
      this.onSearch(0);
    });
  }
  /* DROP DOWN ชั้นเรียน */
  ddlClass() {
    this.service.httpGet('/api/v1/0/tbMstClass/ddl', null).then((res) => {
      this.classList = res || [];
    });
  } 

  onSearch(e) {
    this.searchForm.value.p = (this.paginator.pageIndex = e.pageIndex || 0) + 1;
    this.service.httpGet('/api/v1/tbStuProfile', this.searchForm.value).then((res: IResponse) => {
      this.dataSource.data = res.responseData || [];
      this.pageSize = res.responseSize || 0;
    });
  }

  onCancel() {
    this.searchForm.controls.firstnameTh.setValue('');
    this.searchForm.controls.lastnameTh.setValue('');
    this.searchForm.controls.stuCode.setValue('');
    this.searchForm.controls.citizenId.setValue('');
    this.searchForm.controls.toClass.setValue('');
    this.searchForm.controls.stuStatusId.setValue(0);
    this.onSearch(0);
  } 

  onForm(id) {
    this.router.navigate(['/stu/stu003/form'], { queryParams: { id: id } });
  }

  onReport(stuProfileId) {
    this.isProcess = true;
    this.report.controls.stuProfileId.setValue(stuProfileId);
    this.service.httpReportPDF('/api/v1/tbStuProfile/report/pdf002', 'ใบมอบตัวนักเรียน', this.report.value).then((res) => {
      this.isProcess = false;
    });
  };

  onPreviewPDF() {
    this.isProcess = true;
    if (this.searchForm.value.stuStatusId == '') {
      this.service.httpPreviewPDF('/api/v1/tbStuProfile/report/pdf003_1', this.searchForm.value).then((res) => {
        this.isProcess = false;
        window.location.href = res;
      });
    } if (this.searchForm.value.stuStatusId != '') {
      this.service.httpPreviewPDF('/api/v1/tbStuProfile/report/pdf003_2', this.searchForm.value).then((res) => {
        this.isProcess = false;
        window.location.href = res;
      });
    }
  }

  onExportPDF() {
    this.isProcess = true;
    if (this.searchForm.value.stuStatusId == '') {
      this.service.httpReportPDF('/api/v1/tbStuProfile/report/pdf003_1', "ข้อมูลค้นหารายชื่อมอบตัว", this.searchForm.value).then((res) => {
        this.isProcess = false;
      });
    }
    if (this.searchForm.value.stuStatusId != '') {
      this.service.httpReportPDF('/api/v1/tbStuProfile/report/pdf003_2', "ข้อมูลค้นหารายชื่อมอบตัว", this.searchForm.value).then((res) => {
        this.isProcess = false;
      });
    }
  }

  onExportExcel() {
    this.isProcess = true;
    if (this.searchForm.value.stuStatusId == '') {
      this.service.httpReportXLS('/api/v1/tbStuProfile/report/xls003_1', "ข้อมูลค้นหารายชื่อมอบตัว", this.searchForm.value).then((res) => {
        this.isProcess = false;
      });
    }
    if (this.searchForm.value.stuStatusId != '') {
      this.service.httpReportXLS('/api/v1/tbStuProfile/report/xls003_2', "ข้อมูลค้นหารายชื่อมอบตัว", this.searchForm.value).then((res) => {
        this.isProcess = false;
      });
    }
  }

}

