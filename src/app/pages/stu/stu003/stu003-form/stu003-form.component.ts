import { Component, OnInit } from '@angular/core'; 
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { ApiService, IResponse } from 'src/app/shared/service/api.service';
import Swal from 'sweetalert2/dist/sweetalert2.js';
import 'sweetalert2/src/sweetalert2.scss';
import { UtilService } from 'src/app/_util/util.service';
import { TranslateService } from '@ngx-translate/core';


@Component({
  selector: 'app-stu003-form',
  templateUrl: './stu003-form.component.html',
  styleUrls: ['./stu003-form.component.scss']
}) 
export class Stu003FormComponent implements OnInit {

  isProcess: boolean = false;
  form = this.fb.group({
    'citizenId': ['']
    , 'stuProfileId': ['']
    , 'scCode': ['']
    , 'stuCode': ['']
    , 'lastClass': ['']
    , 'title': ['']
    , 'gender': ['']
    , 'genderId': ['']
    , 'firstnameTh': ['']
    , 'lastnameTh': ['']
    , 'firstnameEn': ['']
    , 'lastnameEn': ['']
    , 'fullname': ['']
    , 'familyStatus': ['']
    , 'nickname': ['']
    , 'ethnicity': ['']
    , 'citizenship': ['']
    , 'religion': ['']
    , 'saint': ['']
    , 'church': ['']
    , 'dob': ['']
    , 'bloodType': ['']
    , 'weight': ['']
    , 'height': ['']
    , 'tel1': ['']
    , 'tel2': ['']
    , 'email': ['']
    , 'specialSkill': ['']
    , 'toClass': ['']
    , 'toYearGroup': ['']
    , 'exSchool': ['']
    , 'exGpa': ['']
    , 'regVillage': ['']
    , 'regHouseNo': ['']
    , 'regAddrNo': ['']
    , 'regVillageNo': ['']
    , 'regRoad': ['']
    , 'regAlley': ['']
    , 'regCountry': ['']
    , 'regProvince': ['']
    , 'regDistrict': ['']
    , 'regSubDistrict': ['']
    , 'regPostCode': ['']
    , 'curVillage': ['']
    , 'curAddrNo': ['']
    , 'curVillageNo': ['']
    , 'curRoad': ['']
    , 'curAlley': ['']
    , 'curCountry': ['']
    , 'curProvince': ['']
    , 'curDistrict': ['']
    , 'curSubDistrict': ['']
    , 'curPostCode': ['']
    , 'pob': ['']
    , 'pobProvince': ['']
    , 'pobDistrict': ['']
    , 'pobSubDistrict': ['']
    , 'noOfRelatives': ['']
    , 'relativesStuCode1': ['']
    , 'relativesStuCode1Name': ['']
    , 'relativesStuCode1Class': ['']
    , 'relativesStuCode2': ['']
    , 'relativesStuCode2Name': ['']
    , 'relativesStuCode2Class': ['']
    , 'relativesStuCode3': ['']
    , 'relativesStuCode3Name': ['']
    , 'relativesStuCode3Class': ['']
    , 'medicalInfo': ['']
    , 'isDisabled': ['']
    , 'disability': ['']
    , 'isactive': ['']
    , 'fatherCitizenId': ['']
    , 'fatherTitle': ['']
    , 'fatherFirstnameTh': ['']
    , 'fatherLastnameTh': ['']
    , 'fatherFirstnameEn': ['']
    , 'fatherLastnameEn': ['']
    , 'fatherEthnicity': ['']
    , 'fatherCitizenship': ['']
    , 'fatherReligion': ['']
    , 'fatherSaint': ['']
    , 'fatherEducation': ['']
    , 'fatherOccupation': ['']
    , 'fatherAnnualIncome': ['']
    , 'fatherOffice': ['']
    , 'fatherVillage': ['']
    , 'fatherAddrNo': ['']
    , 'fatherVillageNo': ['']
    , 'fatherRoad': ['']
    , 'fatherAlley': ['']
    , 'fatherCountry': ['']
    , 'fatherProvince': ['']
    , 'fatherDistrict': ['']
    , 'fatherSubDistrict': ['']
    , 'fatherPostCode': ['']
    , 'fatherTel1': ['']
    , 'fatherTel2': ['']
    , 'fatherEmail': ['']
    , 'fatherAlive': ['']

    , 'motherCitizenId': ['']
    , 'motherTitle': ['']
    , 'motherFirstnameTh': ['']
    , 'motherLastnameTh': ['']
    , 'motherFirstnameEn': ['']
    , 'motherLastnameEn': ['']
    , 'motherEthnicity': ['']
    , 'motherCitizenship': ['']
    , 'motherReligion': ['']
    , 'motherSaint': ['']
    , 'motherEducation': ['']
    , 'motherOccupation': ['']
    , 'motherAnnualIncome': ['']
    , 'motherOffice': ['']
    , 'motherVillage': ['']
    , 'motherAddrNo': ['']
    , 'motherVillageNo': ['']
    , 'motherRoad': ['']
    , 'motherAlley': ['']
    , 'motherCountry': ['']
    , 'motherProvince': ['']
    , 'motherDistrict': ['']
    , 'motherSubDistrict': ['']
    , 'motherPostCode': ['']
    , 'motherTel1': ['']
    , 'motherTel2': ['']
    , 'motherEmail': ['']
    , 'motherAlive': ['']

    , 'parentCitizenId': ['']
    , 'parentTitle': ['']
    , 'parentFirstnameTh': ['']
    , 'parentLastnameTh': ['']
    , 'parentFirstnameEn': ['']
    , 'parentLastnameEn': ['']
    , 'parentEthnicity': ['']
    , 'parentCitizenship': ['']
    , 'parentReligion': ['']
    , 'parentSaint': ['']
    , 'parentEducation': ['']
    , 'parentOccupation': ['']
    , 'parentAnnualIncome': ['']
    , 'parentOffice': ['']
    , 'parentStatus': ['']
    , 'parentVillage': ['']
    , 'parentAddrNo': ['']
    , 'parentVillageNo': ['']
    , 'parentRoad': ['']
    , 'parentAlley': ['']
    , 'parentCountry': ['']
    , 'parentProvince': ['']
    , 'parentDistrict': ['']
    , 'parentSubDistrict': ['']
    , 'parentPostCode': ['']
    , 'parentTel1': ['']
    , 'parentTel2': ['']
    , 'parentEmail': ['']
    , 'parentFullname': ['']
    , 'studentFullname': ['']
    , 'memberId': ['']
    , 'role': ['']
    , 'yearGroupName': ['', Validators.required]
    , 'yearGroupId': ['']
    , 'className': ['', Validators.required]
    , 'classRoom': ['']
    , 'classId': ['']
    , 'classRoomId': ['', Validators.required]
    , 'isEp': ['']
    , 'iscurrent': ['']
    , 'stuStatusId': ['']
    , 'stuStatus': ['']

    , 'stuProfileDocId': ['']
    , 'dataPathImage': ['']
    , 'dataPathBirth': ['']
    , 'dataPathRegister': ['']
    , 'dataPathReligion': ['']
    , 'dataPathChangeName': ['']
    , 'dataPathTranscript7': ['']
    , 'dataPathTranscript1': ['']
    , 'dataPathRecord8': ['']
    , 'dataPathTransfer': ['']
    , 'dataPathFatherRegister': ['']
    , 'dataPathFatherCitizenId': ['']
    , 'dataPathMotherRegister': ['']
    , 'dataPathMotherCitizenId': ['']
    , 'dataPathParentRegister': ['']
    , 'dataPathParentCitizenId': ['']
  });

  id = '';
  reg_province: String;
  reg_district: String;
  reg_sub_district: String;
  reg_post_code: String;
  citizen_id: String;
  father_citizen_id: String;
  mother_citizen_id: String;
  parent_citizen_id: String;
  religion: String;
  fatherReligion: String;
  motherReligion: String;
  parentReligion: String;

  isLinear = false;
  parentFullname: String;
  studentFullname: String;

  firstFormGroup: FormGroup;
  secondFormGroup: FormGroup;
  thirdFormGroup: FormGroup;
  fourthFormGroup: FormGroup;

  data_path_birth: any;
  data_path_register: any;
  data_path_religion: any;
  data_path_change_name: any;
  data_path_transcript7: any;
  data_path_transcript1: any;
  data_path_record8: any;
  data_path_transfer: any;
  data_path_father_register: any;
  data_path_father_citizen_id: any;
  data_path_mother_register: any;
  data_path_mother_citizen_id: any;
  data_path_parent_register: any;
  data_path_parent_citizen_id: any;

  titleList: [];
  titleFatherList: [];
  titleMotherList: [];
  titleParentList: [];
  family_statusList: [];
  bloodTypeList: [];
  lastClassList: [];
  toYearGroupList: [];
  toClassList: [];

  citizenshipList: [];
  citizenshipFatherList: [];
  citizenshipMotherList: [];
  citizenshipParentList: [];

  religionList: [];
  religionFatherList: [];
  religionMotherList: [];
  religionParentList: [];

  provinceRegList: [];
  districtRegList: [];
  subDistrictRegList: [];

  provinceCurList: [];
  districtCurList: [];
  subDistrictCurList: [];

  provincePobList: [];
  districtPobList: [];
  subDistrictPobList: [];

  provinceParentList: [];
  districtParentList: [];
  subDistrictParentList: [];

  provinceFatherList: [];
  districtFatherList: [];
  subDistrictFatherList: [];

  provinceMotherList: [];
  districtMotherList: [];
  subDistrictMotherList: [];

  genderList: [];
  yearGroupList: [];
  classList: [];
  classRoomList: [];

  stuCode1List: [];
  stuCode2List: [];
  stuCode3List: [];

  day_bob: any;
  day_date_now: any
  count_dob: any

  constructor(
    private ar: ActivatedRoute,
    private fb: FormBuilder,
    private service: ApiService,
    private router: Router,
    private translate: TranslateService,
    public utilService: UtilService
  ) {
  }

  ngOnInit() {
    this.id = this.ar.snapshot.queryParamMap.get('id') || '';

    this.loadData();
    this.loadDataDoc();
    this.ddlTbMstTitle();
    this.ddlTbMstTitleFather();
    this.ddlTbMstTitleMother();
    this.ddlTbMstTitleParent();

    this.ddlTbMstGender();
    this.ddlTbMstBloodType();
    this.ddlLastClass();
    this.ddlTbMstFamilyStatus();

    this.ddlTbMstReligion();
    this.ddlFatherTbMstReligion();
    this.ddlMotherTbMstReligion();
    this.ddlParentTbMstReligion();

    this.ddlCitizenship();
    this.ddlFatherCitizenship();
    this.ddlMotherCitizenship();
    this.ddlParentCitizenship();

    this.ddlYearGroup();
    this.ddlClass();


    this.ddltoYearGroup();
    this.ddltoClass();
  }

  loadData() {
    this.isProcess = true;
    this.service.httpGet('/api/v1/tbStuProfile/' + this.id, null).then((res: IResponse) => {
      this.isProcess = false;
      let data = res.responseData;

      this.form.controls.stuProfileId.setValue(data.stuProfileId || '');
      this.form.controls.citizenId.setValue(data.citizenId || '');
      this.form.controls.scCode.setValue(data.scCode || '');
      this.form.controls.stuCode.setValue(data.stuCode || '');
      this.form.controls.lastClass.setValue(data.lastClass || '');
      this.form.controls.title.setValue(data.title || '');
      this.form.controls.genderId.setValue(data.genderJoin.genderId || '');
      this.form.controls.firstnameTh.setValue(data.firstnameTh || '');
      this.form.controls.lastnameTh.setValue(data.lastnameTh || '');
      this.form.controls.firstnameEn.setValue(data.firstnameEn || '');
      this.form.controls.lastnameEn.setValue(data.lastnameEn || '');
      this.form.controls.familyStatus.setValue(data.familyStatus || '');
      this.form.controls.nickname.setValue(data.nickname || '');
      this.form.controls.ethnicity.setValue(data.ethnicity || '');
      this.form.controls.citizenship.setValue(data.citizenship || '');
      this.form.controls.religion.setValue(data.religion || '');
      this.form.controls.saint.setValue(data.saint || '');
      this.form.controls.church.setValue(data.church || '');
      this.form.controls.dob.setValue(this.utilService.formatDateSQLToDate(data.dob || ''));
      this.countDob(this.form.value.dob)
      this.form.controls.bloodType.setValue(data.bloodType || '');
      this.form.controls.weight.setValue(data.weight || '');
      this.form.controls.height.setValue(data.height || '');
      this.form.controls.tel1.setValue(data.tel1 || '');
      this.form.controls.tel2.setValue(data.tel2 || '');
      this.form.controls.email.setValue(data.email || '');
      this.form.controls.specialSkill.setValue(data.specialSkill || '');
      this.form.controls.isactive.setValue(data.isactive || '');
      this.form.controls.toClass.setValue(data.toClass || '');
      this.form.controls.toYearGroup.setValue(data.toYearGroup || '');
      this.form.controls.exSchool.setValue(data.exSchool || '');
      this.form.controls.exGpa.setValue(data.exGpa || '');

      this.form.controls.yearGroupName.setValue(data.toYearGroup || '');

      if (data.toClass == null) { }
      else { this.form.controls.className.setValue(data.toClass || ''); }
      if (data.classRoomJoin == null) { }
      else { this.form.controls.classRoomId.setValue(data.classRoomJoin.classRoomId || ''); }

      this.form.controls.regVillage.setValue(data.regVillage || '');
      this.form.controls.regHouseNo.setValue(data.regHouseNo || '');
      this.form.controls.regAddrNo.setValue(data.regAddrNo || '');
      this.form.controls.regVillageNo.setValue(data.regVillageNo || '');
      this.form.controls.regRoad.setValue(data.regRoad || '');
      this.form.controls.regAlley.setValue(data.regAlley || '');
      this.form.controls.regCountry.setValue(data.regCountry || '');
      this.form.controls.regProvince.setValue(data.regProvince || '');
      this.form.controls.regDistrict.setValue(data.regDistrict || '');
      this.form.controls.regSubDistrict.setValue(data.regSubDistrict || '');
      this.form.controls.regPostCode.setValue(data.regPostCode || '');

      this.form.controls.curVillage.setValue(data.curVillage || '');
      this.form.controls.curAddrNo.setValue(data.curAddrNo || '');
      this.form.controls.curVillageNo.setValue(data.curVillageNo || '');
      this.form.controls.curRoad.setValue(data.curRoad || '');
      this.form.controls.curAlley.setValue(data.curAlley || '');
      this.form.controls.curCountry.setValue(data.curCountry || '');
      this.form.controls.curProvince.setValue(data.curProvince || '');
      this.form.controls.curDistrict.setValue(data.curDistrict || '');
      this.form.controls.curSubDistrict.setValue(data.curSubDistrict || '');
      this.form.controls.curPostCode.setValue(data.curPostCode || '');

      this.form.controls.relativesStuCode1.setValue(data.relativesStuCode1 || '')
      this.form.controls.relativesStuCode2.setValue(data.relativesStuCode2 || '')
      this.form.controls.relativesStuCode3.setValue(data.relativesStuCode3 || '')

      this.form.controls.pob.setValue(data.pob || '');
      this.form.controls.pobProvince.setValue(data.pobProvince || '');
      this.form.controls.pobDistrict.setValue(data.pobDistrict || '');
      this.form.controls.pobSubDistrict.setValue(data.pobSubDistrict || '');
      this.form.controls.noOfRelatives.setValue(data.noOfRelatives || '');
      this.form.controls.medicalInfo.setValue(data.medicalInfo || '');
      this.form.controls.isDisabled.setValue(data.isDisabled || '');
      this.form.controls.disability.setValue(data.disability || '');

      this.form.controls.fatherCitizenId.setValue(data.fatherCitizenId || '');
      this.form.controls.fatherTitle.setValue(data.fatherTitle || '');
      this.form.controls.fatherFirstnameTh.setValue(data.fatherFirstnameTh || '');
      this.form.controls.fatherLastnameTh.setValue(data.fatherLastnameTh || '');
      this.form.controls.fatherFirstnameEn.setValue(data.fatherFirstnameEn || '');
      this.form.controls.fatherLastnameEn.setValue(data.fatherLastnameEn || '');
      this.form.controls.fatherEthnicity.setValue(data.fatherEthnicity || '');
      this.form.controls.fatherCitizenship.setValue(data.fatherCitizenship || '');
      this.form.controls.fatherReligion.setValue(data.fatherReligion || '');
      this.form.controls.fatherSaint.setValue(data.fatherSaint || '');
      this.form.controls.fatherEducation.setValue(data.fatherEducation || '');
      this.form.controls.fatherOccupation.setValue(data.fatherOccupation || '');
      this.form.controls.fatherAnnualIncome.setValue(data.fatherAnnualIncome || '');
      this.form.controls.fatherOffice.setValue(data.fatherOffice || '');
      this.form.controls.fatherAddrNo.setValue(data.fatherAddrNo || '');
      this.form.controls.fatherVillage.setValue(data.fatherVillage || '');
      this.form.controls.fatherVillageNo.setValue(data.fatherVillageNo || '');
      this.form.controls.fatherRoad.setValue(data.fatherRoad || '');
      this.form.controls.fatherAlley.setValue(data.fatherAlley || '');
      this.form.controls.fatherCountry.setValue(data.fatherCountry || '');
      this.form.controls.fatherProvince.setValue(data.fatherProvince || '');
      this.form.controls.fatherDistrict.setValue(data.fatherDistrict || '');
      this.form.controls.fatherSubDistrict.setValue(data.fatherSubDistrict || '');
      this.form.controls.fatherPostCode.setValue(data.fatherPostCode || '');
      this.form.controls.fatherTel1.setValue(data.fatherTel1 || '');
      this.form.controls.fatherTel2.setValue(data.fatherTel2 || '');
      this.form.controls.fatherEmail.setValue(data.fatherEmail || '');
      this.form.controls.fatherAlive.setValue(data.fatherAlive || '');

      this.form.controls.motherCitizenId.setValue(data.motherCitizenId || '');
      this.form.controls.motherTitle.setValue(data.motherTitle || '');
      this.form.controls.motherFirstnameTh.setValue(data.motherFirstnameTh || '');
      this.form.controls.motherLastnameTh.setValue(data.motherLastnameTh || '');
      this.form.controls.motherFirstnameEn.setValue(data.motherFirstnameEn || '');
      this.form.controls.motherLastnameEn.setValue(data.motherLastnameEn || '');
      this.form.controls.motherEthnicity.setValue(data.motherEthnicity || '');
      this.form.controls.motherCitizenship.setValue(data.motherCitizenship || '');
      this.form.controls.motherReligion.setValue(data.motherReligion || '');
      this.form.controls.motherSaint.setValue(data.motherSaint || '');
      this.form.controls.motherEducation.setValue(data.motherEducation || '');
      this.form.controls.motherOccupation.setValue(data.motherOccupation || '');
      this.form.controls.motherAnnualIncome.setValue(data.motherAnnualIncome || '');
      this.form.controls.motherOffice.setValue(data.motherOffice || '');
      this.form.controls.motherAddrNo.setValue(data.motherAddrNo || '');
      this.form.controls.motherVillage.setValue(data.motherVillage || '');
      this.form.controls.motherVillageNo.setValue(data.motherVillageNo || '');
      this.form.controls.motherRoad.setValue(data.motherRoad || '');
      this.form.controls.motherCountry.setValue(data.motherCountry || '');
      this.form.controls.motherProvince.setValue(data.motherProvince || '');
      this.form.controls.motherDistrict.setValue(data.motherDistrict || '');
      this.form.controls.motherSubDistrict.setValue(data.motherSubDistrict || '');
      this.form.controls.motherPostCode.setValue(data.motherPostCode || '');
      this.form.controls.motherTel1.setValue(data.motherTel1 || '');
      this.form.controls.motherTel2.setValue(data.motherTel2 || '');
      this.form.controls.motherEmail.setValue(data.motherEmail || '');
      this.form.controls.motherAlive.setValue(data.motherAlive || '');

      this.form.controls.parentCitizenId.setValue(data.parentCitizenId || '');
      this.form.controls.parentTitle.setValue(data.parentTitle || '');
      this.form.controls.parentFirstnameTh.setValue(data.parentFirstnameTh || '');
      this.form.controls.parentLastnameTh.setValue(data.parentLastnameTh || '');
      this.form.controls.parentFirstnameEn.setValue(data.parentFirstnameEn || '');
      this.form.controls.parentLastnameEn.setValue(data.parentLastnameEn || '');
      this.form.controls.parentEthnicity.setValue(data.parentEthnicity || '');
      this.form.controls.parentCitizenship.setValue(data.parentCitizenship || '');
      this.form.controls.parentReligion.setValue(data.parentReligion || '');
      this.form.controls.parentSaint.setValue(data.parentSaint || '');
      this.form.controls.parentEducation.setValue(data.parentEducation || '');
      this.form.controls.parentOccupation.setValue(data.parentOccupation || '');
      this.form.controls.parentAnnualIncome.setValue(data.parentAnnualIncome || '');
      this.form.controls.parentOffice.setValue(data.parentOffice || '');
      this.form.controls.parentAddrNo.setValue(data.parentAddrNo || '');
      this.form.controls.parentVillage.setValue(data.parentVillage || '');
      this.form.controls.parentVillageNo.setValue(data.parentVillageNo || '');
      this.form.controls.parentRoad.setValue(data.parentRoad || '');
      this.form.controls.parentCountry.setValue(data.parentCountry || '');
      this.form.controls.parentProvince.setValue(data.parentProvince || '');
      this.form.controls.parentDistrict.setValue(data.parentDistrict || '');
      this.form.controls.parentSubDistrict.setValue(data.parentSubDistrict || '');
      this.form.controls.parentPostCode.setValue(data.parentPostCode || '');
      this.form.controls.parentTel1.setValue(data.parentTel1 || '');
      this.form.controls.parentTel2.setValue(data.parentTel2 || '');
      this.form.controls.parentEmail.setValue(data.parentEmail || '');
      this.form.controls.parentStatus.setValue(data.parentStatus || '');
      //ชื่อผู้ปกครอง
      this.parentFullname = (data.parentTitle || '') + (data.parentFirstnameTh || '') + ' ' + (data.parentLastnameTh || '');
      this.form.controls.parentFullname.setValue(this.parentFullname);
      //ชื่อนักเรียน
      this.studentFullname = (data.firstnameTh || '') + ' ' + (data.lastnameTh || '');
      this.form.controls.studentFullname.setValue(this.studentFullname);

      // this.form.controls.fullname.setValue(data.fullname||'');
      this.form.controls.stuStatusId.setValue(data.stuStatusId || '');
      this.form.controls.stuStatus.setValue(data.stuStatus || '');

      this.ddlClassRoom();

      this.ddlRegTbMstProvince();
      this.ddlRegTbMstDistrict();
      this.ddlRegTbMstSubDistrict();

      this.ddlCurTbMstProvince();
      this.ddlCurTbMstDistrict();
      this.ddlCurTbMstSubDistrict();

      this.ddlPobTbMstProvince();
      this.ddlPobTbMstDistrict();
      this.ddlPobTbMstSubDistrict();

      this.ddlFatherTbMstProvince();
      this.ddlFatherTbMstDistrict();
      this.ddlFatherTbMstSubDistrict();

      this.ddlMotherTbMstProvince();
      this.ddlMotherTbMstDistrict();
      this.ddlMotherTbMstSubDistrict();

      this.ddlParentTbMstProvince();
      this.ddlParentTbMstDistrict();
      this.ddlParentTbMstSubDistrict();

      this.getFullnameClass1()
      this.getFullnameClass2()
      this.getFullnameClass3()

      this.firstFormGroup = this.fb.group({
        firstCtrl: ['', Validators.required]
      });
      this.secondFormGroup = this.fb.group({
        secondCtrl: ['', Validators.required]
      });
      this.thirdFormGroup = this.fb.group({
        thirdCtrl: ['', Validators.required]
      });
      this.fourthFormGroup = this.fb.group({
        fourthCtrl: ['', Validators.required]
      });
    });
  }

  loadDataDoc() {
    this.isProcess = true;
    this.service.httpGet('/api/v1/stu-profile-doc/' + this.id, null).then((res: IResponse) => {
      this.isProcess = false;
      let data = res.responseData;
      if (data == null) { return; }
      else {
        this.data_path_birth = data.dataPathBirth || '';
        this.data_path_register = data.dataPathRegister || '';
        this.data_path_religion = data.dataPathReligion || '';
        this.data_path_change_name = data.dataPathChangeName || '';
        this.data_path_transcript7 = data.dataPathTranscript7 || '';
        this.data_path_transcript1 = data.dataPathTranscript1 || '';
        this.data_path_record8 = data.dataPathRecord8 || '';
        this.data_path_transfer = data.dataPathTransfer || '';
        this.data_path_father_register = data.dataPathFatherRegister || '';
        this.data_path_father_citizen_id = data.dataPathFatherCitizenId || '';
        this.data_path_mother_register = data.dataPathMotherRegister || '';
        this.data_path_mother_citizen_id = data.dataPathMotherCitizenId || '';
        this.data_path_parent_register = data.dataPathParentRegister || '';
        this.data_path_parent_citizen_id = data.dataPathParentCitizenId || '';

        this.form.controls.stuProfileDocId.setValue(data.stuProfileDocId || '');
        this.form.controls.dataPathBirth.setValue(data.dataPathBirth || '');
        this.form.controls.dataPathRegister.setValue(data.dataPathRegister || '');
        this.form.controls.dataPathReligion.setValue(data.dataPathReligion || '');
        this.form.controls.dataPathChangeName.setValue(data.dataPathChangeName || '');
        this.form.controls.dataPathTranscript7.setValue(data.dataPathTranscript7 || '');
        this.form.controls.dataPathTranscript1.setValue(data.dataPathTranscript1 || '');
        this.form.controls.dataPathRecord8.setValue(data.dataPathRecord8 || '');
        this.form.controls.dataPathTransfer.setValue(data.dataPathTransfer || '');
        this.form.controls.dataPathFatherRegister.setValue(data.dataPathFatherRegister || '');
        this.form.controls.dataPathFatherCitizenId.setValue(data.dataPathFatherCitizenId || '');
        this.form.controls.dataPathMotherRegister.setValue(data.dataPathMotherRegister || '');
        this.form.controls.dataPathMotherCitizenId.setValue(data.dataPathMotherCitizenId || '');
        this.form.controls.dataPathParentRegister.setValue(data.dataPathParentRegister || '');
        this.form.controls.dataPathParentCitizenId.setValue(data.dataPathParentCitizenId || '');
      }
    });
  }

  onBirth(event) {
    const data = event.target.files[0];
    this.data_path_birth = data.name;
    const uploadData = new FormData();
    uploadData.append('file', data, this.data_path_birth);
    this.service.httpPost("/api/v1/attachment/uploaddoc/" + this.form.value.citizenId, uploadData).then((res: IResponse) => {
      if (res.responseCode != 200) {
        Swal.fire(
          this.translate.instant('alert.error_pic'),
          res.responseMsg,
          'error'
        )
        return;
      }
      this.form.get('dataPathBirth').setValue(res.responseData.attachmentCode + "/" + res.responseData.attachmentName);
    });
  }

  onRegister(event) {
    const data = event.target.files[0];
    this.data_path_register = data.name;
    const uploadData = new FormData();
    uploadData.append('file', data, this.data_path_register);
    this.service.httpPost("/api/v1/attachment/uploaddoc/" + this.form.value.citizenId, uploadData).then((res: IResponse) => {
      if (res.responseCode != 200) {
        Swal.fire(
          this.translate.instant('alert.error_pic'),
          res.responseMsg,
          'error'
        )
        return;
      }
      this.form.get('dataPathRegister').setValue(res.responseData.attachmentCode + "/" + res.responseData.attachmentName);
    });
  }

  onReligion(event) {
    const data = event.target.files[0];
    this.data_path_religion = data.name;
    const uploadData = new FormData();
    uploadData.append('file', data, this.data_path_religion);
    this.service.httpPost("/api/v1/attachment/uploaddoc/" + this.form.value.citizenId, uploadData).then((res: IResponse) => {
      if (res.responseCode != 200) {
        Swal.fire(
          this.translate.instant('alert.error_pic'),
          res.responseMsg,
          'error'
        )
        return;
      }
      this.form.get('dataPathReligion').setValue(res.responseData.attachmentCode + "/" + res.responseData.attachmentName);
    });
  }

  onChangeName(event) {
    const data = event.target.files[0];
    this.data_path_change_name = data.name;
    const uploadData = new FormData();
    uploadData.append('file', data, this.data_path_change_name);
    this.service.httpPost("/api/v1/attachment/uploaddoc/" + this.form.value.citizenId, uploadData).then((res: IResponse) => {
      if (res.responseCode != 200) {
        Swal.fire(
          this.translate.instant('alert.error_pic'),
          res.responseMsg,
          'error'
        )
        return;
      }
      this.form.get('dataPathChangeName').setValue(res.responseData.attachmentCode + "/" + res.responseData.attachmentName);
    });
  }

  onTranscript7(event) {
    const data = event.target.files[0];
    this.data_path_transcript7 = data.name;
    const uploadData = new FormData();
    uploadData.append('file', data, this.data_path_transcript7);
    this.service.httpPost("/api/v1/attachment/uploaddoc/" + this.form.value.citizenId, uploadData).then((res: IResponse) => {
      if (res.responseCode != 200) {
        Swal.fire(
          this.translate.instant('alert.error_pic'),
          res.responseMsg,
          'error'
        )
        return;
      }
      this.form.get('dataPathTranscript7').setValue(res.responseData.attachmentCode + "/" + res.responseData.attachmentName);
    });
  }

  onTranscript1(event) {
    const data = event.target.files[0];
    this.data_path_transcript1 = data.name;
    const uploadData = new FormData();
    uploadData.append('file', data, this.data_path_transcript1);
    this.service.httpPost("/api/v1/attachment/uploaddoc/" + this.form.value.citizenId, uploadData).then((res: IResponse) => {
      if (res.responseCode != 200) {
        Swal.fire(
          this.translate.instant('alert.error_pic'),
          res.responseMsg,
          'error'
        )
        return;
      }
      this.form.get('dataPathTranscript1').setValue(res.responseData.attachmentCode + "/" + res.responseData.attachmentName);
    });
  }

  onRecord8(event) {
    const data = event.target.files[0];
    this.data_path_record8 = data.name;
    const uploadData = new FormData();
    uploadData.append('file', data, this.data_path_record8);
    this.service.httpPost("/api/v1/attachment/uploaddoc/" + this.form.value.citizenId, uploadData).then((res: IResponse) => {
      if (res.responseCode != 200) {
        Swal.fire(
          this.translate.instant('alert.error_pic'),
          res.responseMsg,
          'error'
        )
        return;
      }
      this.form.get('dataPathRecord8').setValue(res.responseData.attachmentCode + "/" + res.responseData.attachmentName);
    });
  }

  onTransfer(event) {
    const data = event.target.files[0];
    this.data_path_transfer = data.name;
    const uploadData = new FormData();
    uploadData.append('file', data, this.data_path_transfer);
    this.service.httpPost("/api/v1/attachment/uploaddoc/" + this.form.value.citizenId, uploadData).then((res: IResponse) => {
      if (res.responseCode != 200) {
        Swal.fire(
          this.translate.instant('alert.error_pic'),
          res.responseMsg,
          'error'
        )
        return;
      }
      this.form.get('dataPathTransfer').setValue(res.responseData.attachmentCode + "/" + res.responseData.attachmentName);
    });
  }

  onFatherRegister(event) {
    const data = event.target.files[0];
    this.data_path_father_register = data.name;
    const uploadData = new FormData();
    uploadData.append('file', data, this.data_path_father_register);
    this.service.httpPost("/api/v1/attachment/uploaddoc/" + this.form.value.citizenId, uploadData).then((res: IResponse) => {
      if (res.responseCode != 200) {
        Swal.fire(
          this.translate.instant('alert.error_pic'),
          res.responseMsg,
          'error'
        )
        return;
      }
      this.form.get('dataPathFatherRegister').setValue(res.responseData.attachmentCode + "/" + res.responseData.attachmentName);
    });
  }

  onFatherCitizenId(event) {
    const data = event.target.files[0];
    this.data_path_father_citizen_id = data.name;
    const uploadData = new FormData();
    uploadData.append('file', data, this.data_path_father_citizen_id);
    this.service.httpPost("/api/v1/attachment/uploaddoc/" + this.form.value.citizenId, uploadData).then((res: IResponse) => {
      if (res.responseCode != 200) {
        Swal.fire(
          this.translate.instant('alert.error_pic'),
          res.responseMsg,
          'error'
        )
        return;
      }
      this.form.get('dataPathFatherCitizenId').setValue(res.responseData.attachmentCode + "/" + res.responseData.attachmentName);
    });
  }

  onMotherRegister(event) {
    const data = event.target.files[0];
    this.data_path_mother_register = data.name;
    const uploadData = new FormData();
    uploadData.append('file', data, this.data_path_mother_register);
    this.service.httpPost("/api/v1/attachment/uploaddoc/" + this.form.value.citizenId, uploadData).then((res: IResponse) => {
      if (res.responseCode != 200) {
        Swal.fire(
          this.translate.instant('alert.error_pic'),
          res.responseMsg,
          'error'
        )
        return;
      }
      this.form.get('dataPathMotherRegister').setValue(res.responseData.attachmentCode + "/" + res.responseData.attachmentName);
    });
  }

  onMotherCitizenId(event) {
    const data = event.target.files[0];
    this.data_path_mother_citizen_id = data.name;
    const uploadData = new FormData();
    uploadData.append('file', data, this.data_path_mother_citizen_id);
    this.service.httpPost("/api/v1/attachment/uploaddoc/" + this.form.value.citizenId, uploadData).then((res: IResponse) => {
      if (res.responseCode != 200) {
        Swal.fire(
          this.translate.instant('alert.error_pic'),
          res.responseMsg,
          'error'
        )
        return;
      }
      this.form.get('dataPathMotherCitizenId').setValue(res.responseData.attachmentCode + "/" + res.responseData.attachmentName);
    });
  }

  onParentRegister(event) {
    const data = event.target.files[0];
    this.data_path_parent_register = data.name;
    const uploadData = new FormData();
    uploadData.append('file', data, this.data_path_parent_register);
    this.service.httpPost("/api/v1/attachment/uploaddoc/" + this.form.value.citizenId, uploadData).then((res: IResponse) => {
      if (res.responseCode != 200) {
        Swal.fire(
          this.translate.instant('alert.error_pic'),
          res.responseMsg,
          'error'
        )
        return;
      }
      this.form.get('dataPathParentRegister').setValue(res.responseData.attachmentCode + "/" + res.responseData.attachmentName);
    });
  }

  onParentCitizenId(event) {
    const data = event.target.files[0];
    this.data_path_parent_citizen_id = data.name;
    const uploadData = new FormData();
    uploadData.append('file', data, this.data_path_parent_citizen_id);
    this.service.httpPost("/api/v1/attachment/uploaddoc/" + this.form.value.citizenId, uploadData).then((res: IResponse) => {
      if (res.responseCode != 200) {
        Swal.fire(
          this.translate.instant('alert.error_pic'),
          res.responseMsg,
          'error'
        )
        return;
      }
      this.form.get('dataPathParentCitizenId').setValue(res.responseData.attachmentCode + "/" + res.responseData.attachmentName);
    });
  }

  /* GET ชื่อนักเรียน/ระดับชั้น */
  getFullnameClass1() {
    let json = { 'stuCode': this.form.value.relativesStuCode1 };
    this.service.httpGet('/api/v1/0/stu-profile/ddl', json).then((res) => {
      this.stuCode1List = res || [];
      if (this.form.value.relativesStuCode1.length == 5) {
        for (let data of this.stuCode1List) {
          this.form.controls.relativesStuCode1Name.setValue(data['fullname']);
          this.form.controls.relativesStuCode1Class.setValue(data['classRoom']);
        }
      } else {
        this.clearGetFullnameClass1();
      }
    });
  }

  getFullnameClass2() {
    let json = { 'stuCode': this.form.value.relativesStuCode2 };
    this.service.httpGet('/api/v1/0/stu-profile/ddl', json).then((res) => {
      this.stuCode2List = res || [];
      if (this.form.value.relativesStuCode2.length == 5) {
        for (let data of this.stuCode2List) {
          this.form.controls.relativesStuCode2Name.setValue(data['fullname']);
          this.form.controls.relativesStuCode2Class.setValue(data['classRoom']);
        }
      } else {
        this.clearGetFullnameClass2();
      }
    });
  }

  getFullnameClass3() {
    let json = { 'stuCode': this.form.value.relativesStuCode3 };
    this.service.httpGet('/api/v1/0/stu-profile/ddl', json).then((res) => {
      this.stuCode3List = res || [];
      if (this.form.value.relativesStuCode3.length == 5) {
        for (let data of this.stuCode3List) {
          this.form.controls.relativesStuCode3Name.setValue(data['fullname']);
          this.form.controls.relativesStuCode3Class.setValue(data['classRoom']);
        }
      } else {
        this.clearGetFullnameClass3();
      }
    });
  }

  clearGetFullnameClass1() {
    this.form.controls.relativesStuCode1Name.setValue('');
    this.form.controls.relativesStuCode1Class.setValue('');
  }

  clearGetFullnameClass2() {
    this.form.controls.relativesStuCode2Name.setValue('');
    this.form.controls.relativesStuCode2Class.setValue('');
  }

  clearGetFullnameClass3() {
    this.form.controls.relativesStuCode3Name.setValue('');
    this.form.controls.relativesStuCode3Class.setValue('');
  }

  countDob(dob) {
    let convert_dob = this.utilService.transform(dob);
    let mounth_dob = parseInt(convert_dob.substr(-7, 2));
    let year_dob = parseInt(convert_dob.substr(-4)) - 543;

    let date_now = new Date();
    let convert_date_now = this.utilService.transform(date_now);
    let mounth_date_now = parseInt(convert_date_now.substr(-7, 2));
    let year_date_now = parseInt(convert_date_now.substr(-4)) - 543;

    if (convert_dob.length == 9) {
      this.day_bob = parseInt(convert_dob.substr(-9, 1));
    } else {
      this.day_bob = parseInt(convert_dob.substr(-10, 2));
    }

    if (convert_date_now.length == 9) {
      this.day_date_now = parseInt(convert_date_now.substr(-9, 1));
    } else {
      this.day_date_now = parseInt(convert_date_now.substr(-10, 2));
    }

    let cal_mounth = ((year_date_now * 12) + mounth_date_now) - ((year_dob * 12) + mounth_dob);
    let cal_day = this.day_date_now - this.day_bob;
    let convert_cal_day = cal_day.toString();
    if (convert_cal_day.substr(0, 1) == '-') {
      this.count_dob = 'อายุ : ' + Math.floor((cal_mounth - 1) / 12) + '  ปี  ' + ((cal_mounth - 1) % 12) + '  เดือน';
    } else {
      this.count_dob = 'อายุ : ' + Math.floor(cal_mounth / 12) + '  ปี  ' + (cal_mounth % 12) + '  เดือน';
    }
  }

  /* CHEK เลขบัตรประชาชน */
  checkCitizenId() {
    if (this.citizen_id.length == 13) {
      let index0 = parseFloat(this.citizen_id[0]);
      let index1 = parseFloat(this.citizen_id[1]);
      let index2 = parseFloat(this.citizen_id[2]);
      let index3 = parseFloat(this.citizen_id[3]);
      let index4 = parseFloat(this.citizen_id[4]);
      let index5 = parseFloat(this.citizen_id[5]);
      let index6 = parseFloat(this.citizen_id[6]);
      let index7 = parseFloat(this.citizen_id[7]);
      let index8 = parseFloat(this.citizen_id[8]);
      let index9 = parseFloat(this.citizen_id[9]);
      let index10 = parseFloat(this.citizen_id[10]);
      let index11 = parseFloat(this.citizen_id[11]);
      let index12 = parseFloat(this.citizen_id[12]);
      let sum = (index0 * 13) + (index1 * 12) + (index2 * 11) + (index3 * 10) + (index4 * 9) + (index5 * 8) + (index6 * 7) + (index7 * 6) + (index8 * 5) + (index9 * 4) + (index10 * 3) + (index11 * 2);
      let modSum = sum % 11;
      let checkDidit = 11 - modSum;
      if (index12 == checkDidit) {
        Swal.fire(
          this.translate.instant('alert.welcome'),
          this.translate.instant('alert.citizen_correct'),
          'success'
        )
      } else {
        Swal.fire(
          this.translate.instant('alert.error'),
          this.translate.instant('alert.citizen_incorrect'),
          'error'
        )
      }
    }
  }

  checkFatherCitizenId() {
    if (this.father_citizen_id.length == 13) {
      let index0 = parseFloat(this.father_citizen_id[0]);
      let index1 = parseFloat(this.father_citizen_id[1]);
      let index2 = parseFloat(this.father_citizen_id[2]);
      let index3 = parseFloat(this.father_citizen_id[3]);
      let index4 = parseFloat(this.father_citizen_id[4]);
      let index5 = parseFloat(this.father_citizen_id[5]);
      let index6 = parseFloat(this.father_citizen_id[6]);
      let index7 = parseFloat(this.father_citizen_id[7]);
      let index8 = parseFloat(this.father_citizen_id[8]);
      let index9 = parseFloat(this.father_citizen_id[9]);
      let index10 = parseFloat(this.father_citizen_id[10]);
      let index11 = parseFloat(this.father_citizen_id[11]);
      let index12 = parseFloat(this.father_citizen_id[12]);
      let sum = (index0 * 13) + (index1 * 12) + (index2 * 11) + (index3 * 10) + (index4 * 9) + (index5 * 8) + (index6 * 7) + (index7 * 6) + (index8 * 5) + (index9 * 4) + (index10 * 3) + (index11 * 2);
      let modSum = sum % 11;
      let checkDidit = 11 - modSum;
      if (index12 == checkDidit) {
        Swal.fire(
          this.translate.instant('alert.welcome'),
          this.translate.instant('alert.citizen_correct'),
          'success'
        )
      } else {
        Swal.fire(
          this.translate.instant('alert.error'),
          this.translate.instant('alert.citizen_incorrect'),
          'error'
        )
      }
    }
  }

  checkMotherCitizenId() {
    if (this.mother_citizen_id.length == 13) {
      let index0 = parseFloat(this.mother_citizen_id[0]);
      let index1 = parseFloat(this.mother_citizen_id[1]);
      let index2 = parseFloat(this.mother_citizen_id[2]);
      let index3 = parseFloat(this.mother_citizen_id[3]);
      let index4 = parseFloat(this.mother_citizen_id[4]);
      let index5 = parseFloat(this.mother_citizen_id[5]);
      let index6 = parseFloat(this.mother_citizen_id[6]);
      let index7 = parseFloat(this.mother_citizen_id[7]);
      let index8 = parseFloat(this.mother_citizen_id[8]);
      let index9 = parseFloat(this.mother_citizen_id[9]);
      let index10 = parseFloat(this.mother_citizen_id[10]);
      let index11 = parseFloat(this.mother_citizen_id[11]);
      let index12 = parseFloat(this.mother_citizen_id[12]);
      let sum = (index0 * 13) + (index1 * 12) + (index2 * 11) + (index3 * 10) + (index4 * 9) + (index5 * 8) + (index6 * 7) + (index7 * 6) + (index8 * 5) + (index9 * 4) + (index10 * 3) + (index11 * 2);
      let modSum = sum % 11;
      let checkDidit = 11 - modSum;
      if (index12 == checkDidit) {
        Swal.fire(
          this.translate.instant('alert.welcome'),
          this.translate.instant('alert.citizen_correct'),
          'success'
        )
      } else {
        Swal.fire(
          this.translate.instant('alert.error'),
          this.translate.instant('alert.citizen_incorrect'),
          'error'
        )
      }
    }
  }

  checkParentCitizenId() {
    if (this.parent_citizen_id.length == 13) {
      let index0 = parseFloat(this.parent_citizen_id[0]);
      let index1 = parseFloat(this.parent_citizen_id[1]);
      let index2 = parseFloat(this.parent_citizen_id[2]);
      let index3 = parseFloat(this.parent_citizen_id[3]);
      let index4 = parseFloat(this.parent_citizen_id[4]);
      let index5 = parseFloat(this.parent_citizen_id[5]);
      let index6 = parseFloat(this.parent_citizen_id[6]);
      let index7 = parseFloat(this.parent_citizen_id[7]);
      let index8 = parseFloat(this.parent_citizen_id[8]);
      let index9 = parseFloat(this.parent_citizen_id[9]);
      let index10 = parseFloat(this.parent_citizen_id[10]);
      let index11 = parseFloat(this.parent_citizen_id[11]);
      let index12 = parseFloat(this.parent_citizen_id[12]);
      let sum = (index0 * 13) + (index1 * 12) + (index2 * 11) + (index3 * 10) + (index4 * 9) + (index5 * 8) + (index6 * 7) + (index7 * 6) + (index8 * 5) + (index9 * 4) + (index10 * 3) + (index11 * 2);
      let modSum = sum % 11;
      let checkDidit = 11 - modSum;
      if (index12 == checkDidit) {
        Swal.fire(
          this.translate.instant('alert.welcome'),
          this.translate.instant('alert.citizen_correct'),
          'success'
        )
      } else {
        Swal.fire(
          this.translate.instant('alert.error'),
          this.translate.instant('alert.citizen_incorrect'),
          'error'
        )
      }
    }
  }

  /* DROP DOWN สถานะครอบครัว */
  ddlTbMstFamilyStatus() {
    this.service.httpGet('/api/v1/0/tbMstFamilyStatus/ddl', null).then((res) => {
      this.family_statusList = res || [];
    });
  }

  /* DROP DOWN คำนำหน้าชื่อนักเรียน พ่อ แม่ ผู้ปกครอง */
  ddlTbMstTitle() {
    this.service.httpGet('/api/v1/0/tbMstTitle/ddl', null).then((res) => {
      this.titleList = res || [];
    });
  }
  ddlTbMstTitleFather() {
    this.service.httpGet('/api/v1/0/tbMstTitle/ddlFather', null).then((res) => {
      this.titleFatherList = res || [];
    });
  }
  ddlTbMstTitleMother() {
    this.service.httpGet('/api/v1/0/tbMstTitle/ddlMother', null).then((res) => {
      this.titleMotherList = res || [];
    });
  }
  ddlTbMstTitleParent() {
    this.service.httpGet('/api/v1/0/tbMstTitle/ddlParent', null).then((res) => {
      this.titleParentList = res || [];
    });
  }

  /* DROP DOWN เพศ */
  ddlTbMstGender() {
    this.service.httpGet('/api/v1/0/tbMstGender/ddl', null).then((res) => {
      this.genderList = res || [];
    });
  }

  /* DROP DOWN ศาสนา */
  ddlTbMstReligion() {
    this.service.httpGet('/api/v1/0/tbMstReligion/ddl', null).then((res) => {
      this.religionList = res || [];
    });
  }
  ddlFatherTbMstReligion() {
    this.service.httpGet('/api/v1/0/tbMstReligion/ddlFather', null).then((res) => {
      this.religionFatherList = res || [];
    });
  }
  ddlMotherTbMstReligion() {
    this.service.httpGet('/api/v1/0/tbMstReligion/ddlMother', null).then((res) => {
      this.religionMotherList = res || [];
    });
  }
  ddlParentTbMstReligion() {
    this.service.httpGet('/api/v1/0/tbMstReligion/ddlParent', null).then((res) => {
      this.religionParentList = res || [];
    });
  }

  /* DROP DOWN กรุ๊ปเลือด */
  ddlTbMstBloodType() {
    this.service.httpGet('/api/v1/0/tbMstBloodType/ddl', null).then((res) => {
      this.bloodTypeList = res || [];
    });
  }

  /* DROP DOWN สัญชาติ */
  ddlCitizenship() {
    this.service.httpGet('/api/v1/0/tbMstCountry/ddl', null).then((res) => {
      this.citizenshipList = res || [];
    });
  }
  ddlFatherCitizenship() {
    this.service.httpGet('/api/v1/0/tbMstCountry/ddlFather', null).then((res) => {
      this.citizenshipFatherList = res || [];
    });
  }
  ddlMotherCitizenship() {
    this.service.httpGet('/api/v1/0/tbMstCountry/ddlMother', null).then((res) => {
      this.citizenshipMotherList = res || [];
    });
  }
  ddlParentCitizenship() {
    this.service.httpGet('/api/v1/0/tbMstCountry/ddlParent', null).then((res) => {
      this.citizenshipParentList = res || [];
    });
  }

  /* DROP DOWN ชั้นเรียนสุดท้าย */
  ddlLastClass() {
    this.service.httpGet('/api/v1/0/tbMstClass/ddl', null).then((res) => {
      this.lastClassList = res || [];
    });
  }

  /* DROP DOWN จังหวัด อำเภอ ตำบล ตามทะเบียนบ้าน */
  ddlRegTbMstProvince() {
    this.service.httpGet('/api/v1/0/tbMstProvince/ddlReg', null).then((res) => {
      this.provinceRegList = res || [];
    });
  }
  ddlRegTbMstDistrict() {
    let json = { 'provinceThReg': this.form.value.regProvince };
    this.service.httpGet('/api/v1/0/tbMstDistrict/ddlReg', json).then((res) => {
      this.districtRegList = res || [];
    });

  }
  ddlRegTbMstSubDistrict() {
    let json = { 'districtThReg': this.form.value.regDistrict };
    this.service.httpGet('/api/v1/0/tbMstSubDistrict/ddlReg', json).then((res) => {
      this.subDistrictRegList = res || [];
    });
  }
  changeRegPostCode() {
    for (let ele of this.subDistrictRegList) {
      this.form.controls.regPostCode.setValue(ele['postCode']);
    }
  }

  /* DROP DOWN จังหวัด (ที่อยู่ปัจจุบัน)*/
  ddlCurTbMstProvince() {
    this.service.httpGet('/api/v1/0/tbMstProvince/ddlCur', null).then((res) => {
      this.provinceCurList = res || [];
    });
  }
  ddlCurTbMstDistrict() {
    let json = { 'provinceThCur': this.form.value.curProvince };
    this.service.httpGet('/api/v1/0/tbMstDistrict/ddlCur', json).then((res) => {
      this.districtCurList = res || [];
    });
  }
  ddlCurTbMstSubDistrict() {
    let json = { 'districtThCur': this.form.value.curDistrict };
    this.service.httpGet('/api/v1/0/tbMstSubDistrict/ddlCur', json).then((res) => {
      this.subDistrictCurList = res || [];
    });
  }
  changeCurPostCode() {
    for (let ele of this.subDistrictCurList) {
      this.form.controls.curPostCode.setValue(ele['postCode']);
    }
  }

  /* DROP DOWN จังหวัด อำเภอ ตำบล ตามสถานที่เกิด */
  ddlPobTbMstProvince() {
    this.service.httpGet('/api/v1/0/tbMstProvince/ddlPob', null).then((res) => {
      this.provincePobList = res || [];
    });
  }

  ddlPobTbMstDistrict() {
    let json = { 'provinceThPob': this.form.value.pobProvince };
    this.service.httpGet('/api/v1/0/tbMstDistrict/ddlPob', json).then((res) => {
      this.districtPobList = res || [];
    });
  }

  ddlPobTbMstSubDistrict() {
    let json = { 'districtThPob': this.form.value.pobDistrict };
    this.service.httpGet('/api/v1/0/tbMstSubDistrict/ddlPob', json).then((res) => {
      this.subDistrictPobList = res || [];
    });
  }

  /* DROP DOWN จังหวัด อำเภอ ตำบล ของพ่อ */
  ddlFatherTbMstProvince() {
    this.service.httpGet('/api/v1/0/tbMstProvince/ddlFather', null).then((res) => {
      this.provinceFatherList = res || [];
    });
  }

  ddlFatherTbMstDistrict() {
    let json = { 'provinceThFather': this.form.value.fatherProvince };
    this.service.httpGet('/api/v1/0/tbMstDistrict/ddlFather', json).then((res) => {
      this.districtFatherList = res || [];
    });
  }

  ddlFatherTbMstSubDistrict() {
    let json = { 'districtThFather': this.form.value.fatherDistrict };
    this.service.httpGet('/api/v1/0/tbMstSubDistrict/ddlFather', json).then((res) => {
      this.subDistrictFatherList = res || [];
    });
  }

  changeFatherPostCode() {
    for (let ele of this.subDistrictFatherList) {
      this.form.controls.fatherPostCode.setValue(ele['postCode']);
    }
  }

  /* DROP DOWN จังหวัด อำเภอ ตำบล ของแม่ */
  ddlMotherTbMstProvince() {
    this.service.httpGet('/api/v1/0/tbMstProvince/ddlMother', null).then((res) => {
      this.provinceMotherList = res || [];
    });
  }

  ddlMotherTbMstDistrict() {
    let json = { 'provinceThMother': this.form.value.motherProvince };
    this.service.httpGet('/api/v1/0/tbMstDistrict/ddlMother', json).then((res) => {
      this.districtMotherList = res || [];
    });
  }

  ddlMotherTbMstSubDistrict() {
    let json = { 'districtThMother': this.form.value.motherDistrict };
    this.service.httpGet('/api/v1/0/tbMstSubDistrict/ddlMother', json).then((res) => {
      this.subDistrictMotherList = res || [];
    });
  }

  changeMotherPostCode() {
    for (let ele of this.form.value.subDistrictMotherList) {
      this.form.controls.motherPostCode.setValue(ele['postCode']);
    }
  }

  /* DROP DOWN จังหวัด อำเภอ ตำบล ของผู้ปกครอง */
  ddlParentTbMstProvince() {
    this.service.httpGet('/api/v1/0/tbMstProvince/ddlParent', null).then((res) => {
      this.provinceParentList = res || [];
    });
  }

  ddlParentTbMstDistrict() {
    let json = { 'provinceThParent': this.form.value.parentProvince };
    this.service.httpGet('/api/v1/0/tbMstDistrict/ddlParent', json).then((res) => {
      this.districtParentList = res || [];
    });
  }

  ddlParentTbMstSubDistrict() {
    let json = { 'districtThParent': this.form.value.parentDistrict };
    this.service.httpGet('/api/v1/0/tbMstSubDistrict/ddlParent', json).then((res) => {
      this.subDistrictParentList = res || [];
    });
  }

  changeParentPostCode() {
    for (let ele of this.subDistrictParentList) {
      this.form.value.parentPostCode.setValue(ele['postCode']);
    }
  }

  /* DROP DOWN ชั้นเรียน */
  ddlClass() {
    this.service.httpGet('/api/v1/0/tbMstClass/ddl', null).then((res) => {
      this.classList = res || [];
    });
  }
  ddltoClass() {
    this.service.httpGet('/api/v1/0/tbMstClass/ddl', null).then((res) => {
      this.toClassList = res || [];
    });
  }
  /* DROP DOWN ห้องเรียน */
  ddlClassRoom() {
    let json = {
      'classSnameTh': this.form.value.className
    };
    this.service.httpGet('/api/v1/0/classroom-list/className', json).then((res) => {
      this.classRoomList = res || [];
    });
  }
  /* DROP DOWN ปีการศึกษา */
  ddlYearGroup() {
    this.service.httpGet('/api/v1/0/year-group/ddl', null).then((res) => {
      this.yearGroupList = res || [];
    });
  }
  ddltoYearGroup() {
    this.service.httpGet('/api/v1/0/year-group/ddl', null).then((res) => {
      this.toYearGroupList = res || [];
    });
  }
  backPage() {
    this.router.navigate(['/stu/stu003']);
  }

  onSave() {

    if (this.form.invalid) {
      Swal.fire(
        this.translate.instant('message.edit_error'),
        this.translate.instant('alert.validate'),
        'error'
      )
      return;
    }

    Swal.fire({
      title: this.translate.instant('psms.DL0001'),
      icon: 'info',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: this.translate.instant('psms.DL0008'),
      cancelButtonText: this.translate.instant('psms.DL0009')
    }).then((result) => {
      if (result.dismiss == 'cancel') {
        return;
      }

      let jsonClass = this.classList.filter((o) => {
        return o['classSnameTh'] == this.form.value.className;
      });
      let jsonClassRoom = this.classRoomList.filter((o) => {
        return o['classRoomId'] == this.form.value.classRoomId;
      });
      let jsonGender = this.genderList.filter((o) => {
        return o['genderId'] == this.form.value.genderId;
      });
      let jsonyearGroup = this.yearGroupList.filter((o) => {
        return o['name'] == this.form.value.yearGroupName;
      });
      this.form.controls.classId.setValue(jsonClass[0]['classId']);
      this.form.controls.toClass.setValue(this.form.value.className);
      this.form.controls.classRoom.setValue(jsonClassRoom[0]['classRoomSnameTh']);
      this.form.controls.isEp.setValue(jsonClassRoom[0]['isEp']);
      this.form.controls.gender.setValue(jsonGender[0]['genderDescTh']);
      this.form.controls.yearGroupId.setValue(jsonyearGroup[0]['yearGroupId']);
      this.form.controls.stuStatusId.setValue(2);
      this.form.controls.stuStatus.setValue("มอบตัวแล้ว");
      this.form.controls.role.setValue("student");
      this.form.controls.memberId.setValue(this.form.value.stuProfileId);

      let json = this.form.value;
      let user = localStorage.getItem('userName');

      if (json.stuProfileDocId != '') {
        this.service.httpPut('/api/v1/tbStuProfile/updateUploadFile/' + user, json).then((res: IResponse) => {
          if ((res.responseCode || 500) != 200) {
            Swal.fire(
              this.translate.instant('message.edit_error'),
              res.responseMsg,
              'error'
            )
            return;
          } else {
            this.onSaveClassRoom();
          }
        })
      } else {
        this.service.httpPut('/api/v1/tbStuProfile/' + user, json).then((res: IResponse) => {
          if ((res.responseCode || 500) != 200) {
            Swal.fire(
              this.translate.instant('message.edit_error'),
              res.responseMsg,
              'error'
            )
            return;
          } else {
            this.onSaveClassRoom();
          }
        });
      }
    });
  }
  onSaveClassRoom() {
    let json = this.form.value;
    let user = localStorage.getItem('userName');
    this.service.httpPost('/api/v1/classRoom-member/' + user, json).then((res: IResponse) => {
      if ((res.responseCode || 500) != 200) {
        Swal.fire(
          this.translate.instant('message.edit_error'),
          res.responseMsg,
          'error'
        )
        return;
      }
      Swal.fire(
        this.translate.instant('psms.DL0006'),
        this.translate.instant('message.edit'),
        'success'
      ).then(() => { this.backPage(); })
    });
  }
}