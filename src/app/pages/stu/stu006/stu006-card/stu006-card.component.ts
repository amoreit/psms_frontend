import { Component, OnInit } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { FormBuilder, Validators } from "@angular/forms";
import { ApiService, IResponse } from "src/app/shared/service/api.service";
import * as constants from "../../../../constants";
import Swal from "sweetalert2/dist/sweetalert2.js";
import "sweetalert2/src/sweetalert2.scss";
import { TranslateService } from '@ngx-translate/core';
@Component({
  selector: "app-stu006-card",
  templateUrl: "./stu006-card.component.html",
  styleUrls: ["./stu006-card.component.scss"]
})
export class Stu006CardComponent implements OnInit {
  id = "";
  stu_profile_id = "";
  smart_purse_card_no = "";
  item_no:any;
  isProcess: boolean = false;
  citizen_id: String;
  title: String;
  firstname_th: String;
  lastname_th: String;
  dob: any;
  stu_code: any;
  date_start = new Date();
  open_card: any;
  exit_card: any;
  max_itemno: any;
  stu_image: any;
  urls = new Array<string>();
  formStuPrintCard = this.fb.group({
    stuProfileId: [""],
    stuCode: [""],
    startDate: [""],
    expiredDate: [""],
    scCode: [""],
    itemno:[""]
  });
  formStuProfile = this.fb.group({
    stuProfileId: [""],
    smartPurseCardNo: ["", Validators.required],
    smartPurseId: ["",Validators.required],
    startDate: [""],
    expiredDate: [""]
  });
  report = this.fb.group({
    stuProfileId: [""],
    signatureImage : [""]
  });
  stuCode1List:[];
  constructor(
    private translate: TranslateService,
    private fb: FormBuilder,
    public router: Router,
    private service: ApiService,
    private ar: ActivatedRoute
  ) {}
  ngOnInit() {
    this.id = this.ar.snapshot.queryParamMap.get("id") || "";
    this.loadData();
    this.open_card = this.formatDateForSQL(this.date_start);
    this.formStuProfile.controls.expiredDate.setValue(
      this.formatDateSQLToDate(this.dateyearplus(this.open_card, true, "/", 5))
    );
    this.signatureImagePath();
  }
  onBack() {
    this.router.navigate(["stu/stu006"]);
  }
  signatureImagePath(){
    this.isProcess = true;
    this.service.httpGet("/api/v1/0/ddlAutho/ddlSignaturePath" ,null).then(res=> {
      let data = res || [];
      this.report.controls.signatureImage.setValue(data[0].signature_image_path);
      });
  }
  loadData() {
    this.isProcess = true;
    this.service
      .httpGet("/api/v1/stu-profile/" + this.id, null)
      .then((res: IResponse) => {
        this.isProcess = false;
        let data = res.responseData;
        this.stu_profile_id = data.stuProfileId;
        this.citizen_id = data.citizenId;
        this.title = data.title || "";
        this.firstname_th = data.firstnameTh;
        this.lastname_th = data.lastnameTh;
        this.formStuProfile.controls.smartPurseCardNo.setValue(data.smartPurseCardNo);
        this.formStuProfile.controls.smartPurseId.setValue(data.smartPurseId);
        this.dob = data.dob;
        this.stu_code = data.stuCode;
        this.stu_image = data.stuImage;
        if (this.stu_image == null || this.stu_image == "") {
          this.urls = [
            constants.SERVICE_API + "/api/v1/attachment/view/user-no-img.jpg"
          ];
        } else {
          this.urls = [
            constants.SERVICE_API + "/api/v1/attachment/view/" + this.stu_image
          ];
        }
      });
  }
  formatDateSQLToDate(sqlDate: string) {
    return new Date(sqlDate);
  }
  formatDateForSQL(date: Date) {
    return (
      date.getFullYear() +
      "-" +
      this.twoDigit(date.getMonth() + 1) +
      "-" +
      this.twoDigit(date.getDate())
    );
  }
  formatDateSQLToString(
    dateStr: string,
    formatBC: boolean,
    splitter: string = "/"
  ) {
    if (dateStr != null && dateStr != "") {
      dateStr = dateStr.substring(0, 10);
      if (dateStr.length == 10) {
        var arr = dateStr.split("-");
        if (
          arr.length == 3 &&
          arr[0].length == 4 &&
          arr[1].length == 2 &&
          arr[2].length == 2
        ) {
          var year = parseInt(arr[0]);
          if (
            !isNaN(parseInt(arr[2])) &&
            !isNaN(parseInt(arr[1])) &&
            !isNaN(year)
          ) {
            if (formatBC) {
              if (year < 2362) {
                year += 543;
              }
            } else {
              if (year > 2219) {
                year -= 543;
              }
            }
            return (
              this.twoDigit(arr[2]) +
              splitter +
              this.twoDigit(arr[1]) +
              splitter +
              year
            );
          }
        }
      }
    }
    return "";
  }
  dateyearplus(
    dateStr: string,
    formatBC: boolean,
    splitter: string = "/",
    num: number
  ) {
    if (dateStr != null && dateStr != "") {
      dateStr = dateStr.substring(0, 10);
      if (dateStr.length == 10) {
        var arr = dateStr.split("-");
        if (
          arr.length == 3 &&
          arr[0].length == 4 &&
          arr[1].length == 2 &&
          arr[2].length == 2
        ) {
          var year = parseInt(arr[0]);
          if (
            !isNaN(parseInt(arr[2])) &&
            !isNaN(parseInt(arr[1])) &&
            !isNaN(year)
          ) {
            if (formatBC) {
              // if (year < 2362) {
              //   year += 543;
              // }
            } else {
              if (year > 2219) {
                year -= 543;
              }
            }
            year += 5;
            return (
              year +
              splitter +
              this.twoDigit(arr[1]) +
              splitter +
              this.twoDigit(arr[2])
            );
          }
        }
      }
    }
    return "";
  }
  twoDigit(digit: any) {
    digit = parseInt(digit);
    return digit < 10 ? "0" + digit : new String(digit);
  }

  onSave_stuProfile() {
    this.formStuProfile.controls.stuProfileId.setValue(this.id);
    if (this.formStuProfile.invalid) {
      Swal.fire(
        this.translate.instant('message.add_error'),//เพิ่มข้อมูลผิดพลาด
        this.translate.instant('alert.validate'),//กรุณาระบุข้อมูล ให้ครบ.
          "error");
      return;
    }
    Swal.fire({
      title: this.translate.instant('alert.save'),//ต้องการบันทึกข้อมูล ใช่หรือไม่
      icon: "info",
      showCancelButton: true,
      confirmButtonColor: "#3085d6",
      cancelButtonColor: "#d33",
      confirmButtonText: this.translate.instant('psms.DL0008'),//ใช่
      cancelButtonText: this.translate.instant('psms.DL0009')//ไม่
    }).then(result => {
      if (result.dismiss == "cancel") {
        return;
      }
      let json = this.formStuProfile.value;
      this.service.httpPut('/api/v1/stu-profile/smartPurse',json).then((res:IResponse)=>{
          this.isProcess = false;
          if((res.responseCode||500)!=200){
              Swal.fire(
                this.translate.instant('message.add_error'),//เพิ่มข้อมูลผิดพลาด
                res.responseMsg,
                'error' 
              )
            return;
          }
          Swal.fire(
            this.translate.instant('alert.add_header'),//เพิ่มข้อมูล
            this.translate.instant('message.add'),//เพิ่มข้อมูลสำเร็จ
              "success").then(() => {
            // this.onBack();
          });
        });
    });
  }
  onSave_stuProfile_report() {
    this.formStuProfile.controls.stuProfileId.setValue(this.id);
    if (this.formStuProfile.invalid) {
      Swal.fire(
        this.translate.instant('message.add_error'),//เพิ่มข้อมูลผิดพลาด
        this.translate.instant('alert.validate'),//กรุณาระบุข้อมูล ให้ครบ.
        "error");
      return;
    }
    	Swal.fire({
    	  title: this.translate.instant('alert.print'),//ต้องการพิมพ์ข้อมูล ใช่หรือไม่
    	  icon: "info",
    	  showCancelButton: true,
    	  confirmButtonColor: "#3085d6",
    	  cancelButtonColor: "#d33",
    	  confirmButtonText: this.translate.instant('psms.DL0008'),//ใช่
        cancelButtonText: this.translate.instant('psms.DL0009')//ไม่
    	}).then(result => {
    	  if (result.dismiss == "cancel") {
    	    return;
    	  }
    	  let json = this.formStuProfile.value; 
    	  this.service.httpPut('/api/v1/stu-profile/smartPurse',json).then((res:IResponse)=>{
    	      this.isProcess = false;
    	      if((res.responseCode||500)!=200){
    	          Swal.fire(
    	            this.translate.instant('message.add_error'),//เพิ่มข้อมูลผิดพลาด
    	            res.responseMsg,
    	            'error' 
    	          )
    	        return;
    	      }
    	      this.stuCodeCheck()
    	    });
    	});
  }
  stuCodeCheck(){
    let stuCode = {'stuCode':this.stu_code};
    this.service.httpGet('/api/v1/0/stu-printcardlog/stuCode-check',stuCode).then((res)=>{
        this.stuCode1List = res||[];
        this.item_no = this.stuCode1List.length;
    }).then(()=> this.onSave_stuPrintLog());
  }
  onSave_stuPrintLog() {
    this.formStuPrintCard.controls.itemno.setValue(this.item_no+1);
    this.formStuPrintCard.controls.stuProfileId.setValue(this.formStuProfile.value.stuProfileId);
    this.formStuPrintCard.controls.stuCode.setValue( this.stu_code);
    this.formStuPrintCard.controls.expiredDate.setValue(this.formStuProfile.value.expiredDate);
    this.formStuPrintCard.controls.startDate.setValue( this.date_start);
    let printCard = this.formStuPrintCard.value;
    this.service.httpPost("/api/v1/PrintCardLog", printCard).then((res: IResponse) => {
        if ((res.responseCode || 500) != 200) {
          Swal.fire(
            this.translate.instant('message.add_error'),//เพิ่มข้อมูลผิดพลาด
            res.responseMsg,
            "error");
          return;
        }
          this.onReport();
      });
    return;
  }
  onReport() {
    this.isProcess = true;
    this.report.controls.stuProfileId.setValue(this.stu_profile_id);
    console.log(this.report.value);
    this.service
      .httpPreviewPDF("/api/v1/stuCard/report/pdf", this.report.value)
      .then(res => {
        this.isProcess = false;
        window.location.href = res;
      });
  }
  onSave_stuProfile_reportPDF() {
    this.formStuProfile.controls.stuProfileId.setValue(this.id);
    if (this.formStuProfile.invalid) {
      Swal.fire(
        this.translate.instant('message.add_error'),//เพิ่มข้อมูลผิดพลาด
        this.translate.instant('alert.validate'),//กรุณาระบุข้อมูล ให้ครบ.
        "error");
      return;
    }
    Swal.fire({
      title: this.translate.instant('alert.save'),//ต้องการบันทึกข้อมูล ใช่หรือไม่
      icon: "info",
      showCancelButton: true,
      confirmButtonColor: "#3085d6",
      cancelButtonColor: "#d33",
      confirmButtonText: this.translate.instant('psms.DL0008'),//ใช่
      cancelButtonText: this.translate.instant('psms.DL0009')//ไม่
    }).then(result => {
      if (result.dismiss == "cancel") {
        return;
      }
      let json = this.formStuProfile.value;
      this.service.httpPut('/api/v1/stu-profile/smartPurse',json).then((res:IResponse)=>{
          this.isProcess = false;
          if((res.responseCode||500)!=200){
              Swal.fire(
                this.translate.instant('message.add_error'),//เพิ่มข้อมูลผิดพลาด
                res.responseMsg,
                'error' 
              )
            return;
          }
          this.stuCodeCheckPDF()
        });
    });
  }
  stuCodeCheckPDF(){
    let stuCode = {'stuCode':this.stu_code};
    this.service.httpGet('/api/v1/0/stu-printcardlog/stuCode-check',stuCode).then((res)=>{
        this.stuCode1List = res||[];
        this.item_no = this.stuCode1List.length;
    }).then(()=> this.onSave_stuPrintLogPDF());
  }
  onSave_stuPrintLogPDF() {
    this.formStuPrintCard.controls.itemno.setValue(this.item_no+1);
    this.formStuPrintCard.controls.stuProfileId.setValue(this.formStuProfile.value.stuProfileId);
    this.formStuPrintCard.controls.stuCode.setValue( this.stu_code);
    this.formStuPrintCard.controls.expiredDate.setValue(this.formStuProfile.value.expiredDate);
    this.formStuPrintCard.controls.startDate.setValue( this.date_start);
    let printCard = this.formStuPrintCard.value;
    this.service.httpPost("/api/v1/PrintCardLog", printCard).then((res: IResponse) => {
        if ((res.responseCode || 500) != 200) {
          Swal.fire(this.translate.instant('message.add_error'),//เพิ่มข้อมูลผิดพลาด
          res.responseMsg, "error");
          return;
        }
          this.onReportPDF();
      });
    return;
  }
  onReportPDF() {
    this.isProcess = true;
    this.report.controls.stuProfileId.setValue(this.stu_profile_id);
    this.service
      .httpReportPDF("/api/v1/stuCard/report/pdf","StuCard", this.report.value)
      .then(res => {
        this.isProcess = false;
      });
  }
}
