import { Component, OnInit, ViewChild, Inject } from "@angular/core";
import { FormBuilder, Validators, FormGroup } from "@angular/forms";
import {
  MatPaginator,
  MatTableDataSource,
  MAT_DIALOG_DATA,
  MatDialog
} from "@angular/material";
import {
  ApiService,
  IResponse,
  EHttp
} from "../../../shared/service/api.service";
import { Router } from "@angular/router";
import Swal from "sweetalert2/dist/sweetalert2.js";
import "sweetalert2/src/sweetalert2.scss";
import { TranslateService } from '@ngx-translate/core';
@Component({
  selector: "app-stu006",
  templateUrl: "./stu006.component.html",
  styleUrls: ["./stu006.component.scss"]
})
export class Stu006Component implements OnInit {

  isProcess: boolean = false;
  searchForm = this.fb.group({
    p: [""],
    classId: [""],
    classRoomId: [""],
    stuCode: [""],
    FirstnameTh: [""],
    LastnameTh: [""],
    result: [10]
  });

  pageSize: number;
  displayedColumns: string[] = [
    "stuNo",
    "stuCode",
    "fullname",
    "classRoom",
    "status",
    "yearStatus"
  ];

  dataSource = new MatTableDataSource();
  yearList: [];
  classList: [];
  classRoomList: [];

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  constructor(
    private translate: TranslateService,
    private fb: FormBuilder,
    private service: ApiService,
    public dialog: MatDialog,
    private router: Router
  ) {}

  ngOnInit() {
    this.ddlClass();
    //this.ddlClassRoom();
    this.onSearch(0);
  }

  onSearch(e) {
    this.searchForm.value.p = (this.paginator.pageIndex = e.pageIndex || 0) + 1;
    this.service
      .httpGet("/api/v1/stu-profile", this.searchForm.value)
      .then((res: IResponse) => {
        this.dataSource.data = res.responseData || [];
        this.pageSize = res.responseSize || 0;
      });
  }

  ddlClass() {
    this.service.httpGet("/api/v1/0/class/ddl", null).then(res => {
      this.classList = res || [];
    });
  }

  ddlYear() {
    this.service.httpGet("/api/v1/0/year-group/ddl", null).then(res => {
      this.yearList = res || [];
    });
  } 

  ddlClassRoom() {
    let json = {
      classId: this.searchForm.value.classId
    };
    this.service
      .httpGet("/api/v1/0/classroom/ddlByCondition", json)
      .then(res => {
        this.classRoomList = res || [];
      });
  }

  onForm(id) {
    this.router.navigate(["/stu/stu006/card"], { queryParams: { id: id } });
  }

  onCancel(){
    this.searchForm.controls.classId.setValue('');
    this.searchForm.controls.classRoomId.setValue('');
    this.searchForm.controls.stuCode.setValue('');
    this.searchForm.controls.FirstnameTh.setValue('');
    this.searchForm.controls.LastnameTh.setValue('');
    this.onSearch(0);
  }
}
