import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { MatPaginator, MatTableDataSource, MatDialog } from '@angular/material';
import { ApiService, IResponse } from '../../../shared/service/api.service';
import { Router } from '@angular/router';
import { Stu004DialogComponent } from 'src/app/dialog/stu004-dialog/stu004-dialog.component';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-stu004',
  templateUrl: './stu004.component.html',
  styleUrls: ['./stu004.component.scss']
})
export class Stu004Component implements OnInit {

  isProcess: boolean = false;
  searchForm = this.fb.group({
    'firstnameTh': ['']
    , 'lastnameTh': ['']
    , 'toYearGroup': ['']
    , 'toClass': ['']
    , 'p': ['']
    , 'result': [10]
  });

  pageSize: number;
  displayedColumns: string[] = ['no', 'citizenNO', 'title', 'firstNameTh', 'lastNameTh', 'classRoom', 'Status'];
  dataSource = new MatTableDataSource();

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  constructor(
    private fb: FormBuilder,
    private service: ApiService,
    public dialog: MatDialog,
    private translate: TranslateService,
    private router: Router
  ) { }

  yearGroupList: [];
  classList: [];

  ngOnInit() {
    this.ddlYearGroup();
    this.ddlClass();

  }

  onSearch(e) {
    this.searchForm.value.p = (this.paginator.pageIndex = e.pageIndex || 0) + 1;
    this.service.httpGet('/api/v1/tbStuProfile/searchCode', this.searchForm.value).then((res: IResponse) => {
      this.dataSource.data = res.responseData || [];
      this.pageSize = res.responseSize || 0;
    });
  }

  onCancel() {
    this.searchForm.controls.firstnameTh.setValue('');
    this.searchForm.controls.lastnameTh.setValue('');
    this.searchForm.controls.toClass.setValue('');
    this.searchForm.controls.toYearGroup.setValue('');
  }
  /* DROP DOWN ปีการศึกษา */
  ddlYearGroup() {
    this.service.httpGet('/api/v1/0/year-group/ddl', null).then((res) => {
      this.yearGroupList = res || [];
      let ele = this.yearGroupList.filter((o) => {
        return o['iscurrent'] == "true";
      });
      this.searchForm.controls.toYearGroup.setValue(ele[0]['name'])
      this.onSearch(0);
    });
  }
  /* DROP DOWN ชั้นเรียน */
  ddlClass() {
    this.service.httpGet('/api/v1/0/tbMstClass/ddl', null).then((res) => {
      this.classList = res || [];
    });
  }

  onPreviewPDF() {
    this.isProcess = true;
    this.service.httpPreviewPDF('/api/v1/tbStuProfile/report/pdf004_1', this.searchForm.value).then((res) => {
      this.isProcess = false;
      window.location.href = res;
    });
  }

  onExportPDF() {
    this.isProcess = true;
    this.service.httpReportPDF('/api/v1/tbStuProfile/report/pdf004_1', "ข้อมูลค้นหารายชื่อรหัสนักเรียน", this.searchForm.value).then((res) => {
      this.isProcess = false;
    });
  }

  onExportExcel() {
    this.isProcess = true;
    this.service.httpReportXLS('/api/v1/tbStuProfile/report/xls004_1', "ข้อมูลค้นหารายชื่อรหัสนักเรียน", this.searchForm.value).then((res) => {
      this.isProcess = false;
    });
  }

  onForm(data): void {
    const dialogRef = this.dialog.open(Stu004DialogComponent, {
      width: '60%'
      , 'autoFocus': true
      , 'disableClose': true
      , 'data': data || {}
    });
    dialogRef.afterClosed().subscribe(result => {
      if ((result || '') == '') {
        this.onSearch(0);
      }
    });
  }

}
