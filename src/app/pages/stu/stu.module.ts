import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FlexLayoutModule } from '@angular/flex-layout';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';  
import { MatDatepickerModule} from '@angular/material/datepicker'; 
import { TranslateModule } from '@ngx-translate/core';

/*** Material ***/
import {
  MatExpansionModule,
  MatButtonModule,
  MatIconModule,
  MatInputModule,
  MatTableModule, 
  MatCardModule,
  MatFormFieldModule,
  MatPaginatorModule,
  MatDialogModule,
  MatSlideToggleModule,
  MatTabsModule
} from '@angular/material';

/*** Routing ***/
import {StuRoutes} from './stu.routing';

/*** Page ***/
import { Stu001Component } from './stu001/stu001.component';
import { Stu002Component } from './stu002/stu002.component';
import { Stu003Component } from './stu003/stu003.component';
import { Stu003FormComponent } from './stu003/stu003-form/stu003-form.component';
import { Stu004Component } from './stu004/stu004.component';
import { Stu005Component } from './stu005/stu005.component';
import { Stu005FormComponent } from './stu005/stu005-form/stu005-form.component';
import { Stu005FormMiniComponent } from './stu005/stu005-form-mini/stu005-form-mini.component';
import { Stu006Component } from './stu006/stu006.component';
import { Stu007Component } from './stu007/stu007.component';
import { Stu008Component } from './stu008/stu008.component';
import { Stu009Component } from './stu009/stu009.component';
import { Stu010Component } from './stu010/stu010.component';
import { Stu011Component } from './stu011/stu011.component';

import { SharedModule } from 'src/app/shared/shared.module';
import { Stu012Component } from './stu012/stu012.component';
import { Stu006CardComponent } from "./stu006/stu006-card/stu006-card.component";
import { TestPrintPicComponent } from './stu010/dialog-stu010/test-print-pic/test-print-pic.component';
import { DialogStu010Component } from './stu010/dialog-stu010/dialog-stu010.component';

@NgModule({
  imports: [ 
    CommonModule,
    TranslateModule,
    FlexLayoutModule,
    RouterModule.forChild(StuRoutes),
    MatExpansionModule,
    MatButtonModule,
    MatIconModule,
    MatInputModule,
    MatTableModule,
    MatDatepickerModule,
    MatCardModule,
    MatFormFieldModule,
    ReactiveFormsModule,
    MatPaginatorModule,
    MatDialogModule,
    MatSlideToggleModule,
    MatTabsModule,
    CommonModule,

    FormsModule, 
    ReactiveFormsModule, 
    SharedModule,
  ],
    providers: [],
    exports:[RouterModule],
  declarations: [
    Stu001Component, 
    Stu002Component, 
    Stu003Component, 
    Stu003FormComponent,
    Stu004Component, 
    Stu005Component, 
    Stu005FormComponent,
    Stu005FormMiniComponent,
    Stu006Component, 
    Stu007Component, 
    Stu008Component, 
    Stu009Component, 
    Stu010Component, 
    Stu011Component,
    Stu012Component,
    Stu006CardComponent,
    TestPrintPicComponent,
    DialogStu010Component
  ],
  entryComponents:[
    TestPrintPicComponent,
    DialogStu010Component
  ]
})
export class StuModule { }
