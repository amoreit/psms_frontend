import { Routes } from '@angular/router';
import { AuthGuard } from '../../shared/service/auth.guard';

/*** Page ***/
import { Stu001Component } from './stu001/stu001.component';
import { Stu002Component } from './stu002/stu002.component';
import { Stu003Component } from './stu003/stu003.component';
import { Stu003FormComponent } from './stu003/stu003-form/stu003-form.component';
import { Stu004Component } from './stu004/stu004.component';
import { Stu005Component } from './stu005/stu005.component';
import { Stu005FormComponent } from './stu005/stu005-form/stu005-form.component';
import { Stu005FormMiniComponent } from './stu005/stu005-form-mini/stu005-form-mini.component';
import { Stu006Component } from './stu006/stu006.component';
import { Stu007Component } from './stu007/stu007.component';
import { Stu008Component } from './stu008/stu008.component';
import { Stu009Component } from './stu009/stu009.component';
import { Stu010Component } from './stu010/stu010.component';
import { Stu011Component } from './stu011/stu011.component';
import { Stu012Component } from './stu012/stu012.component';
import { Stu006CardComponent } from "./stu006/stu006-card/stu006-card.component";
import { TestPrintPicComponent } from './stu010/dialog-stu010/test-print-pic/test-print-pic.component';

export const StuRoutes: Routes = [ 
  {
    path:'', 
    children:[
      {path: 'stu001',component: Stu001Component,canActivate:[AuthGuard]},
      {path: 'stu002',component: Stu002Component,canActivate:[AuthGuard]},
      {path: 'stu003',component: Stu003Component,canActivate:[AuthGuard]},
      {path: 'stu003/form',component: Stu003FormComponent,canActivate:[AuthGuard]},
      {path: 'stu004',component: Stu004Component,canActivate:[AuthGuard]},
      {path: 'stu005',component: Stu005Component,canActivate:[AuthGuard]},
      {path: 'stu005/form',component: Stu005FormComponent,canActivate:[AuthGuard]},
      {path: 'stu005/form-mini',component: Stu005FormMiniComponent,canActivate:[AuthGuard]},
      {path: 'stu006',component: Stu006Component,canActivate:[AuthGuard]},
      {path: 'stu007',component: Stu007Component,canActivate:[AuthGuard]},
      {path: 'stu008',component: Stu008Component,canActivate:[AuthGuard]},
      {path: 'stu009',component: Stu009Component,canActivate:[AuthGuard]}, 
      {path: 'stu010',component: Stu010Component,canActivate:[AuthGuard]},
      {path: 'stu011',component: Stu011Component,canActivate:[AuthGuard]},
      {path: 'stu012',component: Stu012Component,canActivate:[AuthGuard]},
      {path: 'stu006/card',component: Stu006CardComponent,canActivate:[AuthGuard]},
      {path: 'stu0101/TestPrintPicComponent',component: TestPrintPicComponent,canActivate:[AuthGuard]},
    ]
  }
];