import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { UtilService } from 'src/app/_util/util.service';
import { ApiService, IResponse } from 'src/app/shared/service/api.service';
import { Router, ActivatedRoute } from '@angular/router';
import * as constants from '../../../constants';

// sweetalert2
import Swal from 'sweetalert2/dist/sweetalert2.js';
import 'sweetalert2/src/sweetalert2.scss';
// sweetalert2

import { of } from 'rxjs';
import { delay } from 'rxjs/operators';

@Component({
  selector: 'app-stu011',
  templateUrl: './stu011.component.html',
  styleUrls: ['./stu011.component.scss']
})
export class Stu011Component implements OnInit {

  isProcess:boolean = false;
  FormGroup = this.fb.group({
      'cardId':['']
     ,'checkTime':['']
     ,'stuCode':['']
     ,'yearGroupId':['']
     ,'scCode':['']
     ,'fullName':['']
     ,'doorName':['']
     ,'smartPurseId':['']
  });

  date = new Date();
  time = new Date();
  timer;
  today = new Date();
  check_checkTime : boolean;
  urls = new Array<string>();

  smartPurseId = '';
  stuProfileList:[];
  tbStuTimeAttendanceList:[];
  yearGruopList:[];

  fullname = '';
  className = '';
  classRoom = '';
  stuCode = '';
  yearGroupId = '';
  cardId = '';
  checkTime ='';
  stu_image:any;
  stuname = '';

  dataSucess = false;
  show: boolean = true;

  constructor(private ar:ActivatedRoute,
    private router: Router ,
    private _formBuilder: FormBuilder,
    private service:ApiService,
    public util:UtilService,
    private fb: FormBuilder,
  ) { }

  ngOnInit() {
    this.timer = setInterval(() => {
      this.time = new Date();
    }, 1000);

    document.getElementById("stu_profile_id").focus();
    this.urls = ['../assets/img/profile/user-no-img.jpg'];
  }

  detectFiles(event) {
    this.urls = [];
    let files = event.target.files;
    if (files) {
      for (let file of files) { 
        let reader = new FileReader();
        reader.onload = (e: any) => {
          this.urls.push(e.target.result);
        }
        reader.readAsDataURL(file);
      }
    }
  }

  ddlTbMstCatClass(){
    if(this.FormGroup.value.smartPurseId.length == 8){
      let json = {'smartPurseId':this.FormGroup.value.smartPurseId};
      this.service.httpGet('/api/v1/0/stu-profile/ddlSm',json).then((res)=>{
        this.stuProfileList = res||[];
        for(let data of this.stuProfileList){
          this.fullname =  data['fullname'];
          this.className = data['className'];
          this.classRoom = data['classRoom'];
          this.stuCode = data['stuCode'];
          this.stu_image = data['stuImage']
          if(this.stu_image == null || this.stu_image == ''){
            this.urls = [constants.SERVICE_API + '/api/v1/attachment/view/user-no-img.jpg'];
          }else {
            this.urls = [constants.SERVICE_API + '/api/v1/attachment/view/' + this.stu_image];
          }
        }
        this.onYear();
      });
    }
  }

  onYear(){
    this.service.httpGet('/api/v1/0/year-group/ddlYear',null).then((res)=>{
      this.yearGruopList = res||[];
      for(let data of this.yearGruopList){
        this.yearGroupId =  data['yearGroupId'];  
      }
      this.onSave();
      setTimeout(() => this.onClearData(),3000);
    });
  }

  onClearData(){
    this.dataSucess = false;
    this.FormGroup.controls.smartPurseId.setValue('');
  }

  onSave(){
    if(this.fullname != '' && this.className != '' && this.classRoom != '' && this.stuCode != '' ){
      this.FormGroup.controls.cardId.setValue(this.FormGroup.value.smartPurseId.toUpperCase()); 
      this.FormGroup.controls.stuCode.setValue(this.stuCode);
      this.FormGroup.controls.yearGroupId.setValue(this.yearGroupId);
      this.FormGroup.controls.fullName.setValue(this.fullname);
      this.FormGroup.value.doorName ='1'; 
      this.FormGroup.value.scCode ='PRAMANDA';
      this.service.httpPost('/api/v1/tbstutimeattendance',this.FormGroup.value).then((res:IResponse)=>{ 
        this.ddlTbStuTimeAttendance();   
      });
    }else {
      Swal.fire(
        'เกิดข้อผิดพลาด',
        'เนื่องจากไม่พบข้อมูลของท่านในระบบ',
        'error'
      )
    }
  }

  ddlTbStuTimeAttendance(){
    let json = {'cardId': this.FormGroup.value.cardId};
    this.service.httpGet('/api/v1/0/tbStuTimeAttendance/ddl',json).then((res)=>{
      this.tbStuTimeAttendanceList = res||[];
      for(let data of this.tbStuTimeAttendanceList){
        this.checkTime =  data['checkTime'];
      }
      this.dataSucess = true;
    });
  }

}
