import { Component, OnInit, ViewChild } from '@angular/core';
import { MatTableDataSource, MatPaginator, MatDialog } from '@angular/material';
import { FormBuilder } from '@angular/forms';
import { ApiService, IResponse } from 'src/app/shared/service/api.service';
import { Router } from '@angular/router';
import { DialogStu010Component } from './dialog-stu010/dialog-stu010.component';
import { TranslateService } from '@ngx-translate/core';


@Component({
  selector: 'app-stu010',
  templateUrl: './stu010.component.html',
  styleUrls: ['./stu010.component.scss']
})
export class Stu010Component implements OnInit {

  isProcess = false;
  searchForm = this.fb.group({
    p: [''],
    classId: [''],
    classRoomId: [''],
    stuCode: [''],
    FirstnameTh: [''],
    LastnameTh: [''],
    result: [10] 
  });

  reportForm = this.fb.group({
    stuProfileId: ['']
  });

  pageSize: number;
  displayedColumns: string[] = [
    'all',
    'stuNo',
    'stuCode',
    'fullname',
    'classRoom'
  ];

  dataSource = new MatTableDataSource();
  yearList: [];
  classList: [];
  classRoomList: [];
  headerSelected: boolean;
  isSelected: any;
  checklist: any;
  allChecklist=[];
  ClassName=[];
  checkChoose = false;  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;

  constructor(
    private translate: TranslateService,
    private fb: FormBuilder,
    private service: ApiService,
    public dialog: MatDialog,
    private router: Router
  ) { }

  ngOnInit() {
    this.ddlClass();
    this.ddlClassRoom();
    this.onSearch(0);
  }

  onSearch(e) {
    this.searchForm.value.p = (this.paginator.pageIndex = e.pageIndex || 0) + 1;
    this.service
      .httpGet('/api/v1/stu-profile/stu010', this.searchForm.value)
      .then((res: IResponse) => {
        this.dataSource.data = res.responseData || [];
        this.checklist = this.dataSource.data;
        this.pageSize = res.responseSize || 0;
      });
  }

  onForm(){
    let Code={Code:this.allChecklist,Scale:[],Num:'',Stu:[],Class:this.ClassName};
    localStorage.setItem("CodePic",JSON.stringify(Code));
    const dialogRef = this.dialog.open(DialogStu010Component, {width: '800px','disableClose':false});
      dialogRef.afterClosed().subscribe(result => {
      if((result||'') == ''){
        this.onSearch(0);
        return;
        }
      });
      this.router.navigate(["stu/stu010"]);
  }

  ddlClass() {
    this.service.httpGet('/api/v1/0/class/classddl', null).then(res => {
      this.classList = res || [];
    });
  }

  ddlClassRoom() {
    const json = {
      classId: this.searchForm.value.classId
    };
    this.service
      .httpGet('/api/v1/0/classroom-list/ddlFilter', json)
      .then(res => {
        this.classRoomList = res || [];
      });
  }

  onCancel() {
    this.searchForm.controls.classId.setValue('');
    this.searchForm.controls.classRoomId.setValue('');
    this.searchForm.controls.stuCode.setValue('');
    this.searchForm.controls.FirstnameTh.setValue('');
    this.searchForm.controls.LastnameTh.setValue('');
    this.ngOnInit();
  }

  checkAll() {
    for (const i in this.checklist) {
      this.checklist[i].isSelected = this.headerSelected;

    }
    this.getCheckedItemList();
  }

  isAllSelected() {
    this.checklist.every((item: any) => {
      return item.isSelected === true;
    });
    this.headerSelected = false;
    this.getCheckedItemList();
  }

  getCheckedItemList() {
    this.allChecklist = [];

    for (const i in this.checklist) {
      if (this.checklist[i].isSelected === true) {
        const itemData = this.checklist[i].stu_code;
        const className = this.checklist[i].class_name;
        this.allChecklist.push(itemData);
        this.ClassName.push(className);
        this.checkChoose = true;
      }
    }
  }
}
