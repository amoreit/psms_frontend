import { Component, OnInit } from '@angular/core';
import { ApiService, IResponse } from 'src/app/shared/service/api.service';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, Validators, FormGroup } from "@angular/forms";
import {MatDialog} from "@angular/material";
import * as constants from '../../../../../../app/constants';
import { TranslateService } from '@ngx-translate/core';


@Component({
  selector: 'app-test-print-pic',
  templateUrl: './test-print-pic.component.html',
  styleUrls: ['./test-print-pic.component.css']
})
export class TestPrintPicComponent implements OnInit {

  data:{};
  Scale=[];
  local:any;
  constructor(
  private translate: TranslateService,
  private fb: FormBuilder,
  private service: ApiService,
  public dialog: MatDialog,
  private ar: ActivatedRoute,
  private router: Router
  ) { }

  ngOnInit() {
    this.data=JSON.parse(localStorage.getItem("CodePic"));
    this.data['Num'] = parseInt(this.data['Num']);
    this.stuProfile();
  }
  stuProfile(){
    for (let index = 0; index < this.data['Code'].length; index++) {
      let stuCode = {'stuCode':this.data['Code'][index]};
      this.service.httpGet("/api/v1/stu-profile/stuPic",stuCode).then((res: IResponse) => {
        this.local = res.responseData[0];
        let j:string = this.local.class_name;
          if(j.charAt(0)=="อ"){
            this.Scale.push("1");
          }
          else{
            this.Scale.push(this.data['Scale'][0]);
          } 
        this.local.stu_image = constants.SERVICE_API+'/api/v1/attachment/view/'+this.local.stu_image
        this.data['Stu'].push(this.local);
      })
    }
  }
  NumArray(length: any): Array<any> {
    if (length >= 0) {
      return new Array(length);
    }
  }
  printComponent(cmpName) {
    let printContents = document.getElementById(cmpName).innerHTML;
    let originalContents = document.body.innerHTML;
    document.body.innerHTML = printContents;
    window.print();
    document.body.innerHTML = originalContents;
    location.reload();
  }
  goBack(){
    window.history.back();
  }
}