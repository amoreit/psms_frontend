import { Component, OnInit, Inject } from '@angular/core';
import { FormBuilder, Validators} from '@angular/forms';
import { ApiService, IResponse } from 'src/app/shared/service/api.service';
import { MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import 'sweetalert2/src/sweetalert2.scss';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-dialog-stu010',
  templateUrl: './dialog-stu010.component.html',
  styleUrls: ['./dialog-stu010.component.scss']
})
export class DialogStu010Component implements OnInit {

  allchecklistlocalstore:any;
  stuCode=[];
  dataunsave:any;
  class:any;
  printform = this.fb.group({
    Scale:["1"],
    picNum:[""],
  })
  

  constructor(
    private translate: TranslateService,
    private service:ApiService,
    @Inject(MAT_DIALOG_DATA) data:any,
    public dialogRef: MatDialogRef<DialogStu010Component>, 
    private fb: FormBuilder,
    private ar: ActivatedRoute,
    private router: Router) { }

  ngOnInit() {
    this.dataunsave = JSON.parse(localStorage.getItem("CodePic"));
    for (let index = 0; index < this.dataunsave['Class'].length; index++) {
      if(this.dataunsave['Class'][index].charAt(0)!="อ"){
        this.class = 0;
      }
      else{
        this.class = 1;
      }
    }
  }
  close() { 
    this.dialogRef.close(); 
  }
  onPrint(){
    this.dataunsave.Scale = this.printform.value.Scale;
    this.dataunsave.Num = this.printform.value.picNum;
    localStorage.setItem('CodePic',JSON.stringify(this.dataunsave));
    this.router.navigate(["../stu/stu0101/TestPrintPicComponent"]).then(() => {this.close();});
  }
}
