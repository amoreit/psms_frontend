import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder } from '@angular/forms';
import { ApiService, IResponse } from 'src/app/shared/service/api.service';
import { MatDialog, MatTableDataSource, MatPaginator } from '@angular/material';
import { UtilService } from 'src/app/_util/util.service';
import Swal from 'sweetalert2/dist/sweetalert2.js';
import 'sweetalert2/src/sweetalert2.scss';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-stu009',
  templateUrl: './stu009.component.html',
  styleUrls: ['./stu009.component.scss']
})
export class Stu009Component implements OnInit {


  isProcess: boolean = false;
  searchForm = this.fb.group({
    'yearGroupId': ['']
    , 'classId': ['']
    , 'classRoomId': ['']
  });

  graduateForm = this.fb.group({ 
    'graduateReason': ['']
    , 'classOf': ['']
    , 'graduateDate': ['']
  });

  pageSize: number;
  displayedColumns: string[] = ['no', 'stuCode', 'fullname', 'choose'];
  dataSource = new MatTableDataSource();
  classList: [];
  classroomnoList: [];
  yearGroupList: [];
  graduateReasonList: [];
  _allChecklist = [];
  checkdata = false;
  year_group_name: any;
  class_name: any;
  class_room_name: any;
  headerSelected: boolean;
  graduate_date = new Date();

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  constructor(private translate: TranslateService,
    private fb: FormBuilder, private service: ApiService, public dialog: MatDialog, private router: Router, public _util: UtilService) {

  }

  ngOnInit() {
    this.ddlClass();
    this.ddlYearGroup();
    this.ddlGraduateReason();
    this.onSearch(0);

  }

  onSearch(e) {
    this.headerSelected = false;
    this.service.httpGet('/api/v1/trn-classroom-member/stuforstu009', this.searchForm.value).then((res: IResponse) => {
      if (res.responseData.length === 0) {
        this.dataSource.data = []
        this.checkdata = true;
      } else {
        this.dataSource.data = res.responseData || [];
        this.checkdata = false;
      }
    });
  }


  onCancel() {
    this.searchForm = this.fb.group({
      'yearGroupId': ['']
      , 'classId': ['']
    });
    this.onSearch(0);
  }


  ddlClass() {
    this.service.httpGet('/api/v1/0/class/ddl', null).then((res) => {
      this.classList = res || [];
    });
  }

  ddlClassRoomNo() {
    let json = { 'classId': this.searchForm.value.classId };
    this.searchForm.controls.classRoomId.setValue('');
    this.service.httpGet('/api/v1/0/classroom/ddlByClassId', json).then((res) => {
      this.classroomnoList = res || [];
    });
  }

  ddlYearGroup() {
    this.service.httpGet('/api/v1/0/year-group/ddl', null).then((res) => {
      this.yearGroupList = res || [];
      let ele = this.yearGroupList.filter((o) => {
        return o['iscurrent'] == "true";
      });
      this.searchForm.controls.yearGroupId.setValue(ele[0]['yearGroupId'])
    });
  }

  ddlGraduateReason() {
    this.service.httpGet('/api/v1/0/graduatereason/ddl', null).then((res) => {
      this.graduateReasonList = res || [];
    });
  }

  checkAll() {
    for (var i = 0; i < this.dataSource.data.length; i++) {
      this.dataSource.data[i]['isSelected'] = this.headerSelected;
    }
    this.getCheckedItemList();
  }

  isAllSelected() {
    this.dataSource.data.every((item: any) => {
      return item.isSelected === true;
    })
    this.headerSelected = false;
    this.getCheckedItemList();
  }

  getCheckedItemList() {
    this._allChecklist = [];
    for (var i = 0; i < this.dataSource.data.length; i++) {
      if (this.dataSource.data[i]['isSelected']) {
        let itemData = {
          'scCode': this.dataSource.data[i]['scCode'], 'yearGroupId': this.dataSource.data[i]['yearGroupId'],
          'yearGroupName': this.dataSource.data[i]['yearGroupName'], 'stuProfileId': this.dataSource.data[i]['stuProfileId'], 'stuCode': this.dataSource.data[i]['stuCode'],
          'fullname': this.dataSource.data[i]['fullname'], 'lastClassRoom': this.dataSource.data[i]['lastClassRoom'], 'graduateReason': '', 'classOf': '',
          'graduateDate': '', 'stuStatusId': this.dataSource.data[i]['stuStatusId'], 'stuStatus': this.dataSource.data[i]['stuStatus']
        };
        this._allChecklist.push(itemData);
      }
    }

  }

  onSave2() {
    this._allChecklist.map(e => {
      e['graduateReason'] = this.graduateForm.value.graduateReason;
      e['classOf'] = this.graduateForm.value.classOf;
      e['graduateDate'] = this.graduateForm.value.graduateDate;
      return e;
    });

    Swal.fire({
      title: this.translate.instant('alert.approve_final'),
      icon: 'info',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: this.translate.instant('psms.DL0008'),//ใช่
      cancelButtonText: this.translate.instant('psms.DL0009')//ไม่
    }).then((result) => {
      if (result.dismiss == 'cancel') {
        return;
      }
      let json = this._allChecklist;
      let user = localStorage.getItem('userName')
      if(json.length === 0 || this.graduateForm.value.graduateReason == '' || this.graduateForm.value.graduateDate == ''){
        Swal.fire(
          this.translate.instant('alert.approve_final_error'),
          this.translate.instant('alert.validate'),//กรุณาระบุข้อมูล ให้ครบ.
          'error' 
        )
      }else{
        this.service.httpPost('/api/v1/graduate-log/'+user,json).then((res:IResponse)=>{
            if((res.responseCode||500)!=200){
                Swal.fire(
                  this.translate.instant('alert.approve_final_error'),
                  res.responseMsg,
                  'error' 
                )
              return;
            }
          }).then(()=> this.service.httpPut('/api/v1/stu-profile/' + user, json).then((res: IResponse) => {
            if ((res.responseCode || 500) != 200) {
              Swal.fire(
                this.translate.instant('alert.approve_final_error'),
                res.responseMsg,
                'error'
              )
              return;
            }
            Swal.fire(
              this.translate.instant('alert.approve_final_header'),
              this.translate.instant('alert.approve_final_success'),
              'success'
            ).then(() => {})
          }));
        }
      return;
    });
  }



}
