import { Component, OnInit} from '@angular/core'; 
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ApiService, IResponse } from 'src/app/shared/service/api.service';
import Swal from 'sweetalert2/dist/sweetalert2.js';
import 'sweetalert2/src/sweetalert2.scss';
import { TranslateService } from '@ngx-translate/core';
import { UtilService } from 'src/app/_util/util.service';

@Component({ 
  selector: 'app-stu001',
  templateUrl: './stu001.component.html',  
  styleUrls: ['./stu001.component.scss']  
})
export class Stu001Component implements OnInit {

  form = this._formBuilder.group({ 
      'stuProfileId':['']  
      , 'citizenId':['']  
      , 'requestId':['']
      , 'scCode':['']
      , 'stuCode':['']
      , 'stuImage':['']
      , 'lastClass':[''] 
      , 'title':['',Validators.required]  
      , 'titleEn':['']
      , 'gender':['']
      , 'genderId':['']
      , 'firstnameTh':['',Validators.required] 
      , 'lastnameTh':['',Validators.required]
      , 'firstnameEn':['']
      , 'lastnameEn':['']
      , 'familyStatus':['']
      , 'familyStatusDetail':['']
      , 'nickname':['']
      , 'ethnicity':['']
      , 'citizenship':['']
      , 'religion':['']
      , 'saint':['']
      , 'church':['']
      , 'dob':['',Validators.required]
      , 'bloodType':['']
      , 'weight':['']
      , 'height':['']
      , 'tel1':['']
      , 'tel2':['']
      , 'email':['']
      , 'specialSkill':['']
      , 'toClass':['']
      , 'toYearGroup':['']
      , 'exSchool':['']
      , 'exGpa':['']
      , 'regVillage':['']
      , 'regHouseNo':['']
      , 'regAddrNo':['']
      , 'regVillageNo':['']
      , 'regAlley':['']
      , 'regRoad':['']
      , 'regCountry':['']
      , 'regProvince':['']
      , 'regDistrict':['']
      , 'regSubDistrict':['']
      , 'regPostCode':['']
      , 'curVillage':['']
      , 'curAddrNo':['']
      , 'curVillageNo':['']
      , 'curAlley':['']
      , 'curRoad':['']
      , 'curCountry':['']
      , 'curProvince':['']
      , 'curDistrict':['']
      , 'curSubDistrict':['']
      , 'curPostCode':['']
      , 'pob':['']
      , 'pobProvince':['']
      , 'pobDistrict':['']
      , 'pobSubDistrict':['']
      , 'noOfRelatives':['']
      , 'relativesStuCode1':['']
      , 'relativesStuCode2':['']
      , 'relativesStuCode3':['']
      , 'medicalInfo':['']
      , 'isDisabled':['']
      , 'disability':['']
      , 'fatherCitizenId':['']
      , 'fatherTitle':['']
      , 'fatherTitleEn':['']
      , 'fatherFirstnameTh':['']
      , 'fatherLastnameTh':['']
      , 'fatherFirstnameEn':['']
      , 'fatherLastnameEn':['']
      , 'fatherEthnicity':['']
      , 'fatherCitizenship':['']
      , 'fatherReligion':['']
      , 'fatherSaint':['']
      , 'fatherEducation':['']
      , 'fatherAnnualIncome':['']
      , 'fatherOccupation':['']
      , 'fatherOffice':['']
      , 'fatherVillage':['']
      , 'fatherAddrNo':['']
      , 'fatherVillageNo':['']
      , 'fatherAlley':['']
      , 'fatherRoad':['']
      , 'fatherCountry':['']
      , 'fatherProvince':['']
      , 'fatherDistrict':['']
      , 'fatherSubDistrict':['']
      , 'fatherPostCode':['']
      , 'fatherTel1':['']
      , 'fatherTel2':['']
      , 'fatherEmail':['']
      , 'fatherAlive':['']
      , 'motherCitizenId':['']
      , 'motherTitle':['']
      , 'motherTitleEn':['']
      , 'motherFirstnameTh':['']
      , 'motherLastnameTh':['']
      , 'motherFirstnameEn':['']
      , 'motherLastnameEn':['']
      , 'motherEthnicity':['']
      , 'motherCitizenship':['']
      , 'motherReligion':['']
      , 'motherSaint':['']
      , 'motherEducation':['']
      , 'motherAnnualIncome':['']
      , 'motherOccupation':['']
      , 'motherOffice':['']
      , 'motherVillage':['']
      , 'motherAddrNo':['']
      , 'motherVillageNo':['']
      , 'motherAlley':['']
      , 'motherRoad':['']
      , 'motherCountry':['']
      , 'motherProvince':['']
      , 'motherDistrict':['']
      , 'motherSubDistrict':['']
      , 'motherPostCode':['']
      , 'motherTel1':['']
      , 'motherTel2':['']
      , 'motherEmail':['']
      , 'motherAlive':['']
      , 'parentCitizenId':['']
      , 'parentTitle':['']
      , 'parentTitleEn':['']
      , 'parentFirstnameTh':['']
      , 'parentLastnameTh':['']
      , 'parentFirstnameEn':['']
      , 'parentLastnameEn':['']
      , 'parentEthnicity':['']
      , 'parentCitizenship':['']
      , 'parentReligion':['']
      , 'parentSaint':['']
      , 'parentEducation':['']
      , 'parentAnnualIncome':['']
      , 'parentOccupation':['']
      , 'parentOffice':['']
      , 'parentStatus':['']
      , 'parentVillage':['']
      , 'parentAddrNo':['']
      , 'parentVillageNo':['']
      , 'parentAlley':['']
      , 'parentRoad':['']
      , 'parentCountry':['']
      , 'parentProvince':['']
      , 'parentDistrict':['']
      , 'parentSubDistrict':['']
      , 'parentPostCode':['']
      , 'parentTel1':['']
      , 'parentTel2':['']
      , 'parentEmail':['']
      , 'stuStatusId':['']
      , 'stuStatus':[''] 
  });

  formDoc = this._formBuilder.group({ 
    'stuProfileDocId':[''] 
      , 'stuProfileId':['']  
      , 'scCode':['']
      , 'stuCode':['']
      , 'dataPathImage':['']
      , 'dataPathBirth':['']
      , 'dataPathRegister':['']
      , 'dataPathChangeName':['']
      , 'dataPathTranscript7':[''] 
      , 'dataPathTranscript1':[''] 
      , 'dataPathRecord8':['']
      , 'dataPathTransfer':['']
      , 'dataPathFatherRegister':['']
      , 'dataPathFatherCitizenId':['']
      , 'dataPathMotherRegister':['']
      , 'dataPathMotherCitizenId':['']
      , 'dataPathParentRegister':['']
      , 'dataPathParentCitizenId':['']
      , 'dataPathReligion':['']
  });

  formReport = this._formBuilder.group({ 
      'citizenId':['']  
  });

  isLinear = false;
  firstFormGroup: FormGroup;
  secondFormGroup: FormGroup;
  thirdFormGroup: FormGroup; 
  fourthFormGroup: FormGroup;  
  fifthFormGroup: FormGroup; 
  sixFormGroup: FormGroup; 
   
  request_id: String;
  checked_address = false; // checkbox ที่อยู่
  citizen_id : String; //เลขที่บัตรประชาชน
  stu_code : String; //เลขที่บัตรประชาชน
  title_id: String; //คำนำหน้าชื่อ id
  title_name: String; //คำนำหน้าชื่อย่อ
  title_desc: String; //คำนำหน้าชื่อเต็ม
  title_en: String; //คำนำหน้าชื่อ EN
  title_en_name: String; //คำนำหน้าชื่อ EN
  gender: String; //เพศ
  gender_id: String; //รหัสเพศ
  firstname_th: String; //ชื่อ TH
  lastname_th: String; //นามสกุล TH
  firstname_en: String; //ชื่อ EN
  lastname_en: String; //นามสกุล EN
  nickname: String; //ชื่อเล่น
  ethnicity: String; //เชื้อชาติ 
  citizenship: String; //สัญชาติ
  religion: String; //ศาสนา
  saint: String; //ชื่อนักบุญ
  church: String; //สัตบุรุษวัต
  dob:Date; //วัน/เดือน/ปีเกิด
  count_dob:any;
  blood_type: String; //หมู่เลือด
  weight: any; //น้ำหนัก
  height: any; //ส่วนสูง
  tel1: String; //โทรศัพท์มือถือ(1)
  tel2: String; //โทรศัพท์มือถือ(2)
  email: String; //อีเมล์
  special_skill: String; //ความสามารถพิเศษ
  to_class: String; //เข้าศึกษาระดับชั้น
  to_year_group: String; //ปีการศึกษา
  ex_school: String; //สถานศึกษาเดิม
  ex_gpa: any; //เกรดสถานศึกษาเดิม
  last_class: String; //ชั้นเรียนสุดท้าย

  /* ที่อยู่ตามทะเบียนบ้านนักเรียน */
  reg_village: String; //ชื่อหมู่บ้าน (ตามทะเบัยนบ้าน)
  reg_house_no: String; //เลขประจำบ้าน (ตามทะเบัยนบ้าน)
  reg_addr_no: String; //เลขที่ (ตามทะเบัยนบ้าน)
  reg_village_no: String; //หมู่ที่ (ตามทะเบัยนบ้าน)
  reg_alley: String; //ซอย (ตามทะเบัยนบ้าน)
  reg_road: String; //ถนน (ตามทะเบัยนบ้าน)
  reg_country: String; //ประเทศ
  reg_province: String; //จังหวัด  (ตามทะเบัยนบ้าน)
  reg_district: String; //อำเภอ (ตามทะเบัยนบ้าน)
  reg_sub_district: String; //ตำบล/แขวง (ตามทะเบัยนบ้าน)
  reg_post_code: String; //รหัสไปรษณีย์ (ตามทะเบัยนบ้าน)

  /* ที่อยู่ปัจจุบันนักเรียน */
  cur_village: String; //ชื่อหมู่บ้าน (ตามที่อยู่ปัจจุบัน)
  cur_addr_no: String; //เลขที่ (ตามที่อยู่ปัจจุบัน)
  cur_village_no: String; //หมู่ที่ (ตามที่อยู่ปัจจุบัน)
  cur_alley: String; //ซอย (ตามที่อยู่ปัจจุบัน)
  cur_road: String; //ถนน (ตามที่อยู่ปัจจุบัน)
  cur_country: String; //ประเทศ
  cur_province: String; //จังหวัด  (ตามที่อยู่ปัจจุบัน)
  cur_district: String; //อำเภอ (ตามที่อยู่ปัจจุบัน)
  cur_sub_district: String; //ตำบล/แขวง (ตามที่อยู่ปัจจุบัน)
  cur_post_code: String; //รหัสไปรษณีย์ (ตามที่อยู่ปัจจุบัน)

  /* สถานที่เกิดนักเรียน */
  pob: String;//สถานที่เกิด
  pob_province: String; //จังหวัด
  pob_district: String; //อำเภอ
  pob_sub_district: String; //ตำบล/แขวง
  no_of_relatives: any; //มีพี่หรือน้องจำนวน
  relatives_stu_code1: String; //มีพี่หรือน้องที่เรียน รร.นี้ คนที่ 1
  relatives_stu_code2: String; //มีพี่หรือน้องที่เรียน รร.นี้ คนที่ 2
  relatives_stu_code3: String; //มีพี่หรือน้องที่เรียน รร.นี้ คนที่ 3
  relatives_stu_code1_name: String;
  relatives_stu_code1_class: String;
  relatives_stu_code2_name: String;
  relatives_stu_code2_class: String;
  relatives_stu_code3_name: String;
  relatives_stu_code3_class: String;
  medical_info: String; //ข้อมูลทางการแพทย์
  is_disabled = false; // checkbox เลือกความพิการ
  disability: String; //input ความพิการ

  /* ข้อมูลบิดา */
  father_title: String; //คำนำหน้าชื่อ
  father_title_en: String; //คำนำหน้าชื่อ EN
  father_firstname_th: String; //ชื่อจริง TH
  father_lastname_th: String; //นามสกุล TH
  father_firstname_en: String; //ชื่อจริง EN
  father_lastname_en: String; // นามสกุล EN
  father_religion: String; //ศาสนา
  father_saint: String; //ชื่อนักบุญ
  father_ethnicity: String; //เชื้อชาติ
  father_citizenship: String; //สัญชาติ
  father_citizen_id: String; //เลข ปปช
  father_occupation: String; //อาชีพ
  father_office: String; //สถานที่ทำงาน
  father_education: String; //การศึกษา
  father_annual_income: String; //รายได้ประจำปี
  father_addr_no: String; //บ้านเลขที่
  father_village: String; //ชื่อหมู่บ้าน
  father_village_no: String; //หมู่ที่
  father_alley: String; //ซอย
  father_road: String; //ถนน
  father_sub_district: String; //ตำบล
  father_district: String; //อำเภอ
  father_province: String; //จังหวัด
  father_country: String; //ประเทศ
  father_post_code: String; //รหัสไปรษณีย์
  father_email: String; //email
  father_tel1: String; //เบอร์โทร1
  father_tel2: String; //เบอร์โทร2
  father_stu_image: String; //รูปภาพ
  father_alive: Boolean; //สถานะภาพชีวิต

  /* ข้อมูลมารดา */
  mother_title: String; //คำนำหน้าชื่อ
  mother_title_en: String; //คำนำหน้าชื่อ EN
  mother_firstname_th: String; //ชื่อจริง TH
  mother_lastname_th: String; //นามสกุล TH
  mother_firstname_en: String; //ชื่อจริง EN
  mother_lastname_en: String; // นามสกุล EN
  mother_religion: String; //ศาสนา
  mother_saint: String; //ชื่อนักบุญ
  mother_ethnicity: String; //เชื้อชาติ
  mother_citizenship: String; //สัญชาติ
  mother_citizen_id: String; //เลข ปปช
  mother_occupation: String; //อาชีพ
  mother_office: String; //สถานที่ทำงาน
  mother_education: String; //การศึกษา
  mother_annual_income: String; //รายได้ประจำปี
  mother_addr_no: String; //บ้านเลขที่
  mother_village: String; //ชื่อหมู่บ้าน
  mother_village_no: String; //หมู่ที่
  mother_alley: String; //ซอย
  mother_road: String; //ถนน
  mother_sub_district: String; //ตำบล
  mother_district: String; //อำเภอ
  mother_province: String; //จังหวัด
  mother_country: String; //ประเทศ
  mother_post_code: String; //รหัสไปรษณีย์
  mother_email: String; //email
  mother_tel1: String; //เบอร์โทร1
  mother_tel2: String; //เบอร์โทร2
  mother_stu_image: String; //รูปภาพ
  mother_alive: Boolean; //สถานะภาพชีวิต

  /* ข้อมูลปกครอง */
  parent_title: String; //คำนำหน้าชื่อ
  parent_title_en: String; //คำนำหน้าชื่อ EN
  parent_firstname_th: String; //ชื่อจริง TH
  parent_lastname_th: String; //นามสกุล TH
  parent_firstname_en: String; //ชื่อจริง EN
  parent_lastname_en: String; // นามสกุล EN
  parent_religion: String; //ศาสนา
  parent_saint: String; //ชื่อนักบุญ
  parent_ethnicity: String; //เชื้อชาติ
  parent_citizenship: String; //สัญชาติ
  parent_citizen_id: String; //เลข ปปช
  parent_occupation: String; //อาชีพ
  parent_office: String; //สถานที่ทำงาน
  parent_education: String; //การศึกษา
  parent_annual_income: String; //รายได้ประจำปี
  parent_addr_no: String; //บ้านเลขที่
  parent_village: String; //ชื่อหมู่บ้าน
  parent_village_no: String; //หมู่ที่
  parent_alley: String; //ซอย
  parent_road: String; //ถนน
  parent_sub_district: String; //ตำบล
  parent_district: String; //อำเภอ
  parent_province: String; //จังหวัด
  parent_country: String; //ประเทศ
  parent_post_code: String; //รหัสไปรษณีย์
  parent_email: String; //email
  parent_tel1: String; //เบอร์โทร1
  parent_tel2: String; //เบอร์โทร2
  parent_stu_image: String; //รูปภาพ
  parent_status: String; //ความสัมพันธ์กับนักเรียน
  family_status: String; //สถานะครอบครัว
  stu_status_id: Number; //สถานะนักเรียน ID
  stu_status: String; //สถานะนักเรียน "สมัครเข้าเรียน"


  /**** PATH IMAGE ******/
  data_path1: String;
  data_path_image: String;
  data_path2: String;
  data_path_birth: String;
  data_path3: String;
  data_path_register: String;
  data_path4: String;
  data_path_change_name: String;
  data_path5: String;
  data_path_transcript7: String;
  data_path6: String;
  data_path_transcript1: String;
  data_path7: String;
  data_path_record8: String;
  data_path8: String;
  data_path_transfer: String;
  data_path9: String;
  data_path_father_register: String;
  data_path10: String;
  data_path_father_citizen_id: String;
  data_path11: String;
  data_path_mother_register: String;
  data_path12: String;
  data_path_mother_citizen_id: String;
  data_path13: String;
  data_path_parent_register: String;
  data_path14: String;
  data_path_parent_citizen_id: String;
  data_path15: String;
  data_path_religion: String;

  checkRoleId: Number;
  checkRoleIdStatus = false;
  isProcess = false;
  check_citizen_id: Boolean;
  check_father_citizen_id: Boolean;
  check_mother_citizen_id: Boolean;
  check_parent_citizen_id: Boolean;
  validateValue = false;
  radio_parent: String;
  checked_stu_old = false; 

  day_bob:any;
  day_date_now:any;
  family_status_detail = '';

  /* LIST DROP DOWN */
  titleList:[];
  titleEnList:[]; 
  titleFatherList:[];
  titleEnFatherList:[];
  titleMotherList:[];
  titleEnMotherList:[];
  titleParentList:[];
  titleEnParentList:[];

  religionList:[];
  religionFatherList:[];
  religionMotherList:[];
  religionParentList:[];
  
  citizenshipList:[];
  citizenshipFatherList:[];
  citizenshipMotherList:[];
  citizenshipParentList:[];

  bloodTypeList:[];
  lastClassList:[];
  genderList:[];
  family_statusList:[];
  educationList:[];
  registerList:[];
  yearGroupList:[];
  yearGroupFilterList:[];
  
  stuCode1List:[];
  stuCode2List:[];
  stuCode3List:[];

  stuProfileDocList:[];

  provinceRegList:[];
  districtRegList:[];
  subDistrictRegList:[];

  provinceCurList:[];
  districtCurList:[];
  subDistrictCurList:[];

  provincePobList:[];
  districtPobList:[];
  subDistrictPobList:[];

  provinceFatherList:[];
  districtFatherList:[];
  subDistrictFatherList:[];

  provinceMotherList:[];
  districtMotherList:[];
  subDistrictMotherList:[];

  provinceParentList:[];
  districtParentList:[];
  subDistrictParentList:[];
  postCodeList:[];
  refNoList:[];

  constructor(
      private _formBuilder: FormBuilder,
      public router:Router,
      private service:ApiService, 
      private util: UtilService,
      private translate: TranslateService
  ) {
    this.ddlRegTbMstProvince();
    this.ddlRegTbMstDistrict();
    this.ddlRegTbMstSubDistrict();

    this.ddlCurTbMstProvince();
    this.ddlCurTbMstDistrict();
    this.ddlCurTbMstSubDistrict();
    
    this.ddlPobTbMstProvince();
    this.ddlPobTbMstDistrict();
    this.ddlPobTbMstSubDistrict();

    this.ddlFatherTbMstProvince();
    this.ddlFatherTbMstDistrict();
    this.ddlFatherTbMstSubDistrict();

    this.ddlMotherTbMstProvince();
    this.ddlMotherTbMstDistrict();
    this.ddlMotherTbMstSubDistrict();
    
    this.ddlParentTbMstProvince();
    this.ddlParentTbMstDistrict();
    this.ddlParentTbMstSubDistrict();

    this.ddlTbMstTitle();
    this.ddlTbMstTitleEn()
    this.ddlTbMstTitleFather();
    this.ddlTbMstTitleMother();
    this.ddlTbMstTitleParent();

    this.ddlTbMstBloodType();
    this.ddlLastClass();
    this.ddlTbMstFamilyStatus();
    this.ddlTbMstEducation();
    this.ddlYearGroup();

    this.ddlTbMstReligion();
    this.ddlFatherTbMstReligion();
    this.ddlMotherTbMstReligion();
    this.ddlParentTbMstReligion();

    this.ddlCitizenship();
    this.ddlFatherCitizenship();
    this.ddlMotherCitizenship(); 
    this.ddlParentCitizenship();
  } 

  ngOnInit() {  
    this.firstname_en = '-';
    this.lastname_en = '-';
    this.nickname = '-';
    this.firstname_en = '-';
    this.weight = '0';
    this.height = '0';
    this.tel1 = '-';
    this.tel2 = '-';
    this.ex_school = '-';
    this.ex_gpa = '0.00';
    this.no_of_relatives = 0;

    this.reg_addr_no = '-';
    this.reg_village_no = '-';
    this.cur_addr_no = '-';
    this.cur_village_no = '-';

    this.father_citizen_id = '-';
    this.father_firstname_th = '-';
    this.father_lastname_th = '-';
    this.father_firstname_en = '-';
    this.father_lastname_en = '-';
    this.father_addr_no = '-';
    this.father_village_no = '-';
    this.father_tel1 = '-';
    this.father_tel2 = '-';

    this.mother_citizen_id = '-';
    this.mother_firstname_th = '-';
    this.mother_lastname_th = '-';
    this.mother_firstname_en = '-';
    this.mother_lastname_en = '-';
    this.mother_addr_no = '-';
    this.mother_village_no = '-';
    this.mother_tel1 = '-';
    this.mother_tel2 = '-';

    this.parent_citizen_id = '-';
    this.parent_firstname_th = '-';
    this.parent_lastname_th = '-';
    this.parent_firstname_en = '-';
    this.parent_lastname_en = '-';
    this.parent_addr_no = '-';
    this.parent_village_no = '-';
    this.parent_tel1 = '-';
    this.parent_tel2 = '-';

    this.email = localStorage.getItem('email')||'';  
      let json = {'email':this.email};
      this.service.httpGet('/api/v1/0/registerList/ddl',json).then((res)=>{
        this.registerList = res||[]; 
            for (let data of this.registerList){
              this.checkRoleId = data['roleId'];
              if(this.checkRoleId == 777){
                this.checkRoleIdStatus = true;
                let citizenId:any = data['citizenId'];
                this.citizen_id = citizenId.toString().substring(0,1)+"-"+citizenId.toString().substring(1,5)+"-"+citizenId.toString().substring(5,10)+"-"+citizenId.toString().substring(10,12)+"-"+citizenId.toString().substring(12);
                this.firstname_th = data['firstName'];
                this.lastname_th = data['lastName'];
                this.email = data['email'];
              }else{
                this.checkRoleIdStatus = false;
                this.citizen_id = '';
                this.firstname_th = '';
                this.lastname_th = '';
                this.email = '-';
              }
            }
      });

      this.firstFormGroup = this._formBuilder.group({
        firstCtrl: ['', Validators.required]
      });
      this.secondFormGroup = this._formBuilder.group({
        secondCtrl: ['', Validators.required]
      });
      this.thirdFormGroup = this._formBuilder.group({
        thirdCtrl: ['', Validators.required]
      });
      this.fourthFormGroup = this._formBuilder.group({
        fourthCtrl: ['', Validators.required]
      });
      this.fifthFormGroup = this._formBuilder.group({
        fifthCtrl: ['', Validators.required] 
      });
      this.sixFormGroup = this._formBuilder.group({
        sixCtrl: ['', Validators.required]
      });

      this.father_alive = false;
      this.mother_alive = false;
      this.radio_parent = "N"; 
  } 

  numberOnly(event): boolean {
    const charCode = (event.which) ? event.which : event.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      return false;
    }
    return true;
  }  

  onGrade(event): boolean {
    const chaCode = (event.which) ? event.which : event.keyCode;
    if(chaCode > 31 && (chaCode < 48 || chaCode > 57) && chaCode !=46){
      return false;
    }
    return true; 
  }

  autoFormat(str,typeCheck){
    if(typeCheck==1){
        var pattern = new String("_-____-_____-__-_"); 
        var pattern_ex = new String("-");  
        var returnText = new String("");
        var str_length = str.length;
        var str_length_total = str_length-1;
        for(let i = 0; i < pattern.length; i++){           
            if(str_length_total == i && pattern.charAt(i+1) == pattern_ex){
                returnText += str+pattern_ex;
                this.citizen_id = returnText;
            } 
        }
    }else if(typeCheck==2){
        var pattern = new String("__-____-____"); 
        var pattern_ex = new String("-");       
        var returnText = new String("");
        var str_length = str.length;
        var str_length_total = str_length-1;
        for(let i = 0; i < pattern.length; i++){  
            if(str_length_total == i && pattern.charAt(i+1) == pattern_ex){
                returnText += str+pattern_ex;
                this.tel1 = returnText;
            }
        }  
    }else if(typeCheck==3){
        var pattern = new String("__-____-____"); 
        var pattern_ex = new String("-");       
        var returnText = new String("");
        var str_length = str.length;
        var str_length_total = str_length-1;
        for(let i = 0; i < pattern.length; i++){  
            if(str_length_total == i && pattern.charAt(i+1) == pattern_ex){
                returnText += str+pattern_ex;
                this.tel2 = returnText;
            }
        }
    }else if(typeCheck==4){
        var pattern = new String("_-____-_____-__-_"); 
        var pattern_ex = new String("-");  
        var returnText = new String("");
        var str_length = str.length;
        var str_length_total = str_length-1;
        for(let i = 0; i < pattern.length; i++){           
            if(str_length_total == i && pattern.charAt(i+1) == pattern_ex){
                returnText += str+pattern_ex;
                this.father_citizen_id = returnText;
            } 
        }
    }else if(typeCheck==5){
        var pattern = new String("__-____-____"); 
        var pattern_ex = new String("-");       
        var returnText = new String("");
        var str_length = str.length;
        var str_length_total = str_length-1;
        for(let i = 0; i < pattern.length; i++){  
            if(str_length_total == i && pattern.charAt(i+1) == pattern_ex){
                returnText += str+pattern_ex;
                this.father_tel1 = returnText;
            }
        }
    }else if(typeCheck==6){
        var pattern = new String("__-____-____"); 
        var pattern_ex = new String("-");       
        var returnText = new String("");
        var str_length = str.length;
        var str_length_total = str_length-1;
        for(let i = 0; i < pattern.length; i++){  
            if(str_length_total == i && pattern.charAt(i+1) == pattern_ex){
                returnText += str+pattern_ex;
                this.father_tel2 = returnText;
            }
        }
    }else if(typeCheck==7){
        var pattern = new String("_-____-_____-__-_"); 
        var pattern_ex = new String("-");  
        var returnText = new String("");
        var str_length = str.length;
        var str_length_total = str_length-1;
        for(let i = 0; i < pattern.length; i++){           
            if(str_length_total == i && pattern.charAt(i+1) == pattern_ex){
                returnText += str+pattern_ex;
                this.mother_citizen_id = returnText;
            } 
        }
    }else if(typeCheck==8){
        var pattern = new String("__-____-____"); 
        var pattern_ex = new String("-");       
        var returnText = new String("");
        var str_length = str.length;
        var str_length_total = str_length-1;
        for(let i = 0; i < pattern.length; i++){  
            if(str_length_total == i && pattern.charAt(i+1) == pattern_ex){
                returnText += str+pattern_ex;
                this.mother_tel1 = returnText;
            }
        }
    }else if(typeCheck==9){
        var pattern = new String("__-____-____"); 
        var pattern_ex = new String("-");       
        var returnText = new String("");
        var str_length = str.length;
        var str_length_total = str_length-1;
        for(let i = 0; i < pattern.length; i++){  
            if(str_length_total == i && pattern.charAt(i+1) == pattern_ex){
                returnText += str+pattern_ex;
                this.mother_tel2 = returnText;
            }
        }
    }else if(typeCheck==10){
        var pattern = new String("_-____-_____-__-_"); 
        var pattern_ex = new String("-");  
        var returnText = new String("");
        var str_length = str.length;
        var str_length_total = str_length-1;
        for(let i = 0; i < pattern.length; i++){           
            if(str_length_total == i && pattern.charAt(i+1) == pattern_ex){
                returnText += str+pattern_ex;
                this.parent_citizen_id = returnText;
            } 
        }
    }else if(typeCheck==11){
        var pattern = new String("__-____-____"); 
        var pattern_ex = new String("-");       
        var returnText = new String("");
        var str_length = str.length;
        var str_length_total = str_length-1;
        for(let i = 0; i < pattern.length; i++){  
            if(str_length_total == i && pattern.charAt(i+1) == pattern_ex){
                returnText += str+pattern_ex;
                this.parent_tel1 = returnText;
            }
        }
    }else{
        var pattern = new String("__-____-____"); 
        var pattern_ex = new String("-");       
        var returnText = new String("");
        var str_length = str.length;
        var str_length_total = str_length-1;
        for(let i = 0; i < pattern.length; i++){  
            if(str_length_total == i && pattern.charAt(i+1) == pattern_ex){
                returnText += str+pattern_ex;
                this.parent_tel2 = returnText;
            }
        }
    }
}
 
  /***************************** ตรวจสอบข้อมูล CITIZEN ID **********************************/
  ckeckStuOld(e){
    this.checked_stu_old = e.target.checked; 
    if(this.checked_stu_old == true){
        if(this.citizen_id.length == 17){ 
            let concat_str = this.citizen_id.toString().substring(0,1)+this.citizen_id.toString().substring(2,6)+this.citizen_id.toString().substring(7,12)+this.citizen_id.toString().substring(13,15)+this.citizen_id.toString().substring(16);
            let json = {'citizenId':concat_str};
            this.service.httpGet('/api/v1/0/stu-profile/ddlStuOld',json).then((res)=>{
                this.stuCode1List = res||[];
                if(this.stuCode1List.length != 0){
                    for (let data of this.stuCode1List){
                        /** start tab 1 **/
                        this.stu_code = data['stuCode'];
                        this.firstname_th = data['firstnameTh'];
                        this.lastname_th = data['lastnameTh'];
                        this.title_desc = data['title'];
                        this.ddlTbMstGender();
                        this.gender = data['gender'];
                        this.title_en = data['titleEn'];
                        this.firstname_en = data['firstnameEn'];
                        this.lastname_en = data['lastnameEn'];
                        this.form.controls.stuProfileId.setValue(data['stuProfileId']);
                        this.nickname = data['nickname'];
                        this.ethnicity = data['ethnicity'];
                        this.citizenship = data['citizenship'];
                        this.religion = data['religion'];
                        this.church = data['church'];
                        let dob = data['dob']; 
                        this.countDob(dob);
                        this.dob = this.util.formatDateSQLToDate(dob);
                        this.blood_type = data['bloodType']; 
                        this.weight = data['weight']; 
                        this.height = data['height']; 

                        let tel1:any = data['tel1']; 
                        if(tel1 != null){
                          this.tel1 = tel1.toString().substring(0,2)+"-"+tel1.toString().substring(2,6)+"-"+tel1.toString().substring(6);
                        }else{
                          this.tel1 = '-';
                        }

                        let tel2:any = data['tel2'];
                        if(tel2 != null){
                          this.tel2 = tel2.toString().substring(0,2)+"-"+tel2.toString().substring(2,6)+"-"+tel2.toString().substring(6);
                        }else{
                          this.tel2 = '-';
                        }

                        this.email = data['email']; 
                        this.special_skill = data['specialSkill']; 
                        this.to_class = data['toClass']; 
                        this.to_year_group = data['toYearGroup']; 
                        this.ex_school = data['exSchool']; 
                        this.last_class = data['lastClass']; 
                        this.ex_gpa = data['exGpa']; 
                        /** end tab 1 **/
                        
                        /** start tab 2 **/
                        this.reg_village = data['regVillage']; 
                        this.reg_addr_no = data['regAddrNo']; 
                        this.reg_village_no = data['regVillageNo'];
                        this.reg_alley = data['regAlley']; 
                        this.reg_road = data['regRoad']; 
                        this.reg_country = data['regCountry'];
                        this.reg_province = data['regProvince']; 
                        this.ddlRegTbMstDistrict();
                        this.reg_district = data['regDistrict'];
                        this.ddlRegTbMstSubDistrict();
                        this.reg_sub_district = data['regSubDistrict'];
                        this.reg_post_code = data['regPostCode']; 
                        this.cur_village = data['curVillage']; 
                        this.cur_addr_no = data['curAddrNo']; 
                        this.cur_village_no = data['curVillageNo'];
                        this.cur_alley = data['curAlley']; 
                        this.cur_road = data['curRoad']; 
                        this.cur_country = data['curCountry'];
                        this.cur_province = data['curProvince']; 
                        this.ddlCurTbMstDistrict();
                        this.cur_district = data['curDistrict'];
                        this.ddlCurTbMstSubDistrict();
                        this.cur_sub_district = data['curSubDistrict'];
                        this.cur_post_code = data['curPostCode']; 
                        this.pob = data['pob']; 
                        this.pob_province = data['pobProvince']; 
                        this.pob_district = data['pobDistrict']; 
                        this.pob_sub_district = data['pobSubDistrict']; 
                        this.no_of_relatives = data['noOfRelatives']; 
                        /** end tab 2 **/

                        /** start tab 3 **/
                        this.medical_info = data['medicalInfo'];  
                        this.disability = data['disability']; 
                        if(this.disability == '' || this.disability == null){
                          this.is_disabled = false;
                        }else{
                          this.is_disabled = true;
                        }
                        /** end tab 3 **/

                        /** start tab 4 **/
                        this.family_status = data['familyStatus'];
                        this.family_status_detail = data['familyStatusDetail'];

                        let father_citizen_id:any = data['fatherCitizenId'];  
                        if(father_citizen_id != null){
                          this.father_citizen_id = father_citizen_id.toString().substring(0,1)+"-"+father_citizen_id.toString().substring(1,5)+"-"+father_citizen_id.toString().substring(5,10)+"-"+father_citizen_id.toString().substring(10,12)+"-"+father_citizen_id.toString().substring(12);
                        }else{
                          this.father_citizen_id = '-';
                        }
 
                        this.father_title = data['fatherTitle']; 
                        this.father_firstname_th = data['fatherFirstnameTh']; 
                        this.father_lastname_th = data['fatherLastnameTh']; 
                        this.father_title_en = data['fatherTitleEn']; 
                        this.father_firstname_en = data['fatherFirstnameEn']; 
                        this.father_lastname_en = data['fatherLastnameEn']; 
                        this.father_ethnicity = data['fatherEthnicity']; 
                        this.father_citizenship = data['fatherCitizenship']; 
                        this.father_religion = data['fatherReligion']; 
                        this.father_saint = data['fatherSaint']; 
                        this.father_education = data['fatherEducation']; 
                        this.father_occupation = data['fatherOccupation']; 
                        this.father_annual_income = data['fatherAnnualIncome']; 
                        this.father_office = data['fatherOffice']; 
                        this.father_village = data['fatherVillage']; 
                        this.father_addr_no = data['fatherAddrNo']; 
                        this.father_village_no = data['fatherVillageNo'];
                        this.father_road = data['fatherRoad']; 
                        this.father_alley = data['fatherAlley']; 
                        this.father_country = data['fatherCountry']; 
                        this.father_province = data['fatherProvince']; 
                        this.ddlFatherTbMstDistrict();
                        this.father_district = data['fatherDistrict']; 
                        this.ddlFatherTbMstSubDistrict();
                        this.father_sub_district = data['fatherSubDistrict']; 
                        this.father_post_code = data['fatherPostCode']; 

                        let father_tel1:any = data['fatherTel1']; 
                        if(father_tel1 != null){
                          this.father_tel1 = father_tel1.toString().substring(0,2)+"-"+father_tel1.toString().substring(2,6)+"-"+father_tel1.toString().substring(6);
                        }else{
                          this.father_tel1 = '-';
                        }

                        let father_tel2:any = data['fatherTel2']; 
                        if(father_tel2 != null){
                          this.father_tel2 = father_tel2.toString().substring(0,2)+"-"+father_tel2.toString().substring(2,6)+"-"+father_tel2.toString().substring(6);
                        }else{
                          this.father_tel2 = '-';
                        }

                        this.father_email = data['fatherEmail']; 

                        let mother_citizen_id:any = data['motherCitizenId']; 
                        if(mother_citizen_id != null){
                          this.mother_citizen_id = mother_citizen_id.toString().substring(0,1)+"-"+mother_citizen_id.toString().substring(1,5)+"-"+mother_citizen_id.toString().substring(5,10)+"-"+mother_citizen_id.toString().substring(10,12)+"-"+mother_citizen_id.toString().substring(12);
                        }else{
                          this.mother_citizen_id = '-';
                        }

                        this.mother_title = data['motherTitle']; 
                        this.mother_firstname_th = data['motherFirstnameTh']; 
                        this.mother_lastname_th = data['motherLastnameTh']; 
                        this.mother_title_en = data['motherTitleEn']; 
                        this.mother_firstname_en = data['motherFirstnameEn']; 
                        this.mother_lastname_en = data['motherLastnameEn']; 
                        this.mother_ethnicity = data['motherEthnicity']; 
                        this.mother_citizenship = data['motherCitizenship']; 
                        this.mother_religion = data['motherReligion']; 
                        this.mother_saint = data['motherSaint']; 
                        this.mother_education = data['motherEducation']; 
                        this.mother_occupation = data['motherOccupation']; 
                        this.mother_annual_income = data['motherAnnualIncome']; 
                        this.mother_office = data['motherOffice']; 
                        this.mother_village = data['motherVillage']; 
                        this.mother_addr_no = data['motherAddrNo']; 
                        this.mother_village_no = data['motherVillageNo'];
                        this.mother_road = data['motherRoad']; 
                        this.mother_alley = data['motherAlley']; 
                        this.mother_country = data['motherCountry']; 
                        this.mother_province = data['motherProvince']; 
                        this.ddlMotherTbMstDistrict();
                        this.mother_district = data['motherDistrict']; 
                        this.ddlMotherTbMstSubDistrict();
                        this.mother_sub_district = data['motherSubDistrict']; 
                        this.mother_post_code = data['motherPostCode']; 

                        let mother_tel1:any = data['motherTel1']; 
                        if(mother_tel1 != null){
                          this.mother_tel1 = mother_tel1.toString().substring(0,2)+"-"+mother_tel1.toString().substring(2,6)+"-"+mother_tel1.toString().substring(6);
                        }else{
                          this.mother_tel1 = '-';
                        }

                        let mother_tel2:any = data['motherTel2']; 
                        if(mother_tel2 != null){
                          this.mother_tel2 = mother_tel2.toString().substring(0,2)+"-"+mother_tel2.toString().substring(2,6)+"-"+mother_tel2.toString().substring(6);
                        }else{
                          this.mother_tel2 = '-';
                        }

                        this.mother_email = data['motherEmail']; 

                        let parent_citizen_id:any = data['parentCitizenId']; 
                        if(parent_citizen_id != null){
                          this.parent_citizen_id = parent_citizen_id.toString().substring(0,1)+"-"+parent_citizen_id.toString().substring(1,5)+"-"+parent_citizen_id.toString().substring(5,10)+"-"+parent_citizen_id.toString().substring(10,12)+"-"+parent_citizen_id.toString().substring(12);
                        }else{
                          this.parent_citizen_id = '-';
                        }

                        this.parent_title = data['parentTitle']; 
                        this.parent_firstname_th = data['parentFirstnameTh']; 
                        this.parent_lastname_th = data['parentLastnameTh']; 
                        this.parent_title_en = data['parentTitleEn']; 
                        this.parent_firstname_en = data['parentFirstnameEn']; 
                        this.parent_lastname_en = data['parentLastnameEn']; 
                        this.parent_ethnicity = data['parentEthnicity']; 
                        this.parent_citizenship = data['parentCitizenship']; 
                        this.parent_religion = data['parentReligion']; 
                        this.parent_saint = data['parentSaint']; 
                        this.parent_education = data['parentEducation']; 
                        this.parent_occupation = data['parentOccupation']; 
                        this.parent_annual_income = data['parentAnnualIncome']; 
                        this.parent_office = data['parentOffice']; 
                        this.parent_status = data['parentStatus']; 
                        this.parent_village = data['parentVillage']; 
                        this.parent_addr_no = data['parentAddrNo']; 
                        this.parent_village_no = data['parentVillageNo'];
                        this.parent_road = data['parentRoad']; 
                        this.parent_alley = data['parentAlley']; 
                        this.parent_country = data['parentCountry']; 
                        this.parent_province = data['parentProvince']; 
                        this.ddlParentTbMstDistrict();
                        this.parent_district = data['parentDistrict']; 
                        this.ddlParentTbMstSubDistrict();
                        this.parent_sub_district = data['parentSubDistrict']; 
                        this.parent_post_code = data['parentPostCode']; 

                        let parent_tel1:any = data['parentTel1']; 
                        if(parent_tel1 != null){
                          this.parent_tel1 = parent_tel1.toString().substring(0,2)+"-"+parent_tel1.toString().substring(2,6)+"-"+parent_tel1.toString().substring(6);
                        }else{
                          this.parent_tel1 = '-';
                        }

                        let parent_tel2:any = data['parentTel2']; 
                        if(parent_tel2 != null){
                          this.parent_tel2 = parent_tel2.toString().substring(0,2)+"-"+parent_tel2.toString().substring(2,6)+"-"+parent_tel2.toString().substring(6);
                        }else{
                          this.parent_tel2 = '-';
                        }

                        this.parent_email = data['parentEmail']; 
                        /** end tab 4 **/
                    } 
                    let jsonDoc = {'stuProfileId':this.form.value.stuProfileId};
                    this.service.httpGet('/api/v1/0/stu-profile-doc/ddl',jsonDoc).then((res)=>{
                        this.stuProfileDocList = res||[];
                        for (let doc of this.stuProfileDocList){
                            /** start tab 5 **/
                            this.formDoc.controls.stuProfileDocId.setValue(doc['stuProfileDocId']);
                            this.formDoc.controls.stuProfileId.setValue(doc['stuProfileId']);

                            this.data_path_image = doc['dataPathImage'];
                            if(this.data_path_image == '' || this.data_path_image == null){
                              this.data_path1 = '';
                            }else{
                              this.data_path1 = this.data_path_image.substring(14);
                            }

                            this.data_path_birth = doc['dataPathBirth'];
                            if(this.data_path_birth == '' || this.data_path_birth == null){
                              this.data_path2 = '';
                            }else{
                              this.data_path2 = this.data_path_birth.substring(14);
                            }

                            this.data_path_register = doc['dataPathRegister'];
                            if(this.data_path_register == '' || this.data_path_register == null){
                              this.data_path3 = '';
                            }else{
                              this.data_path3 = this.data_path_register.substring(14);
                            }

                            this.data_path_change_name = doc['dataPathChangeName'];
                            if(this.data_path_change_name == '' || this.data_path_change_name == null){
                              this.data_path4 = '';
                            }else{
                              this.data_path4 = this.data_path_change_name.substring(14);
                            }

                            this.data_path_transcript7= doc['dataPathTranscript7'];
                            if(this.data_path_transcript7 == '' || this.data_path_transcript7 == null){
                              this.data_path5 = '';
                            }else{
                              this.data_path5 = this.data_path_transcript7.substring(14);
                            }

                            this.data_path_transcript1 = doc['dataPathTranscript1'];
                            if(this.data_path_transcript1 == '' || this.data_path_transcript1 == null){
                              this.data_path6 = '';
                            }else{
                              this.data_path6 = this.data_path_transcript1.substring(14);
                            }

                            this.data_path_record8 = doc['dataPathRecord8'];
                            if(this.data_path_record8 == '' || this.data_path_record8 == null){
                              this.data_path7 = '';
                            }else{
                              this.data_path7 = this.data_path_record8.substring(14);
                            }

                            this.data_path_transfer = doc['dataPathTransfer'];
                            if(this.data_path_transfer == '' || this.data_path_transfer == null){
                              this.data_path8 = '';
                            }else{
                              this.data_path8 = this.data_path_transfer.substring(14);
                            }

                            this.data_path_father_register = doc['dataPathFatherRegister'];
                            if(this.data_path_father_register == '' || this.data_path_father_register == null){
                              this.data_path9 = '';
                            }else{
                              this.data_path9 = this.data_path_father_register.substring(14);
                            }

                            this.data_path_father_citizen_id = doc['dataPathFatherCitizenId'];
                            if(this.data_path_father_citizen_id == '' || this.data_path_father_citizen_id == null){
                              this.data_path10 = '';
                            }else{
                              this.data_path10 = this.data_path_father_citizen_id.substring(14); 
                            }

                            this.data_path_mother_register = doc['dataPathMotherRegister'];
                            if(this.data_path_mother_register == '' || this.data_path_mother_register == null){
                              this.data_path11 = ''; 
                            }else{
                              this.data_path11 = this.data_path_mother_register.substring(14);
                            }

                            this.data_path_mother_citizen_id = doc['dataPathMotherCitizenId'];
                            if(this.data_path_mother_citizen_id == '' || this.data_path_mother_citizen_id == null){
                              this.data_path12 = '';
                            }else{
                              this.data_path12 = this.data_path_mother_citizen_id.substring(14);
                            }

                            this.data_path_parent_register = doc['dataPathParentRegister'];
                            if(this.data_path_parent_register == '' || this.data_path_parent_register == null){
                              this.data_path13 = '';
                            }else{
                              this.data_path13 = this.data_path_parent_register.substring(14);
                            }

                            this.data_path_parent_citizen_id = doc['dataPathParentCitizenId'];
                            if(this.data_path_parent_citizen_id == '' || this.data_path_parent_citizen_id == null){
                              this.data_path14 = '';
                            }else{
                              this.data_path14 = this.data_path_parent_citizen_id.substring(14);
                            }

                            this.data_path_religion = doc['dataPathReligion'];
                            if(this.data_path_religion == '' || this.data_path_religion == null){
                              this.data_path15 = '';
                            }else{
                              this.data_path15 = this.data_path_religion.substring(14);
                            }
                            /** end tab 5 **/
                        }
                    });
                }else{
                    Swal.fire(
                        this.translate.instant('alert.apply'),
                        this.translate.instant('alert.apply_detail1')+' '+'<b>'+this.citizen_id+'</b>'+' '+this.translate.instant('alert.apply_detail2'),
                        'info' 
                    ).then(() => {this.checked_stu_old = false;})
                } 
            });
        }else{
            Swal.fire(
                this.translate.instant('alert.error'),
                this.translate.instant('alert.error_detail1'),
                'error' 
            ).then(() => {this.checked_stu_old = false;}) 
        }
    }else{
        this.onClear();
        this.formDoc.controls.stuProfileDocId.setValue('');
        this.form.controls.stuProfileId.setValue('');
        this.checked_address = false;
    }
  }

  /***************************** DROP DOWN YEAR GROUP ************************************/
  ddlYearGroup(){
    let yearname:any;
    this.service.httpGet('/api/v1/0/year-group/ddl',null).then((res)=>{
      this.yearGroupList = res||[];
      let ele = this.yearGroupList.filter((o) => {
        return o['iscurrent'] == "true";
      });
      yearname = ele[0]['name'];
    }).then(()=> this.checkYearGroup(yearname)); 
  } 

  checkYearGroup(str) {
    let year_group_search = str.substring(2) + str.substring(0, 1);
    this.service.httpGet('/api/v1/0/year-group/ddlcheckyeargroup', { 'yearGroupSearch': year_group_search }).then((res) => {
      this.yearGroupFilterList = res||[];
      this.to_year_group = res[0]['name'];
    });
  }

  /************************************ COUNT DOB ****************************************/
  countDob(dob){
      let convert_dob = this.util.transform(dob); 
      let mounth_dob = parseInt(convert_dob.substr(-7,2));
      let year_dob = parseInt(convert_dob.substr(-4))-543;

      let date_now = new Date();
      let convert_date_now = this.util.transform(date_now);
      let mounth_date_now  = parseInt(convert_date_now.substr(-7,2));
      let year_date_now  = parseInt(convert_date_now.substr(-4))-543;

      if(convert_dob.length == 9){
          this.day_bob = parseInt(convert_dob.substr(-9,1));
      }else{
          this.day_bob = parseInt(convert_dob.substr(-10,2));
      }
       
      if(convert_date_now.length == 9){
          this.day_date_now = parseInt(convert_date_now.substr(-9,1));
      }else{
          this.day_date_now = parseInt(convert_date_now.substr(-10,2));
      }
      
      let cal_mounth = ((year_date_now*12)+mounth_date_now)-((year_dob*12)+mounth_dob);
      let cal_day = this.day_date_now-this.day_bob;
      let convert_cal_day = cal_day.toString();
      if(convert_cal_day.substr(0,1) == '-'){
          this.count_dob = 'อายุ : '+Math.floor((cal_mounth-1)/12)+'  ปี  '+((cal_mounth-1)%12)+'  เดือน';
      }else{
          this.count_dob = 'อายุ : '+Math.floor(cal_mounth/12)+'  ปี  '+(cal_mounth%12)+'  เดือน';
      }
  }

  /***************************** UPLOAD FILE *********************************************/
  onPathfile(event,pathType){
    if(this.citizen_id.length != 17){
        Swal.fire(
          this.translate.instant('alert.error'), 
          this.translate.instant('alert.error_upload_doc'),
          'error' 
        )
    }else{
      let concat_str = this.citizen_id.toString().substring(0,1)+
                       this.citizen_id.toString().substring(2,6)+
                       this.citizen_id.toString().substring(7,12)+
                       this.citizen_id.toString().substring(13,15)+
                       this.citizen_id.toString().substring(16);
      const selectedFile = event.target.files[0];
      const sizeFile = selectedFile.size;
      const covert_sizeFile_mb = sizeFile / (1024*1024);
      if(covert_sizeFile_mb <= 5.0){
          const uploadData = new FormData();
          uploadData.append('file',selectedFile,selectedFile.name);
          this.service.httpPost("/api/v1/attachment/uploaddoc/"+concat_str,uploadData).then((res:IResponse)=>{ 
            if(res.responseCode != 200){
              alert(res.responseMsg);
              return;
            }

            let json = res.responseData;
            if(pathType == 1){
              this.data_path1 = selectedFile.name;
              this.data_path_image = json.attachmentCode+'/'+json.attachmentName;
            }else if(pathType == 2){
              this.data_path2 = selectedFile.name;
              this.data_path_birth = json.attachmentCode+'/'+json.attachmentName;
            }else if(pathType == 3){
              this.data_path3 = selectedFile.name;
              this.data_path_register = json.attachmentCode+'/'+json.attachmentName;
            }else if(pathType == 4){
              this.data_path4 = selectedFile.name;
              this.data_path_change_name = json.attachmentCode+'/'+json.attachmentName;
            }else if(pathType == 5){
              this.data_path5 = selectedFile.name;
              this.data_path_transcript7 = json.attachmentCode+'/'+json.attachmentName;
            }else if(pathType == 6){
              this.data_path6 = selectedFile.name;
              this.data_path_transcript1 = json.attachmentCode+'/'+json.attachmentName;
            }else if(pathType == 7){
              this.data_path7 = selectedFile.name;
              this.data_path_record8 = json.attachmentCode+'/'+json.attachmentName;
            }else if(pathType == 8){
              this.data_path8 = selectedFile.name;
              this.data_path_transfer = json.attachmentCode+'/'+json.attachmentName;
            }else if(pathType == 9){
              this.data_path9= selectedFile.name;
              this.data_path_father_register = json.attachmentCode+'/'+json.attachmentName;
            }else if(pathType == 10){
              this.data_path10= selectedFile.name;
              this.data_path_father_citizen_id = json.attachmentCode+'/'+json.attachmentName;
            }else if(pathType == 11){
              this.data_path11= selectedFile.name;
              this.data_path_mother_register = json.attachmentCode+'/'+json.attachmentName;
            }else if(pathType == 12){
              this.data_path12= selectedFile.name;
              this.data_path_mother_citizen_id = json.attachmentCode+'/'+json.attachmentName;
            }else if(pathType == 13){
              this.data_path13= selectedFile.name;
              this.data_path_parent_register = json.attachmentCode+'/'+json.attachmentName;
            }else if(pathType == 14){
              this.data_path14= selectedFile.name;
              this.data_path_parent_citizen_id = json.attachmentCode+'/'+json.attachmentName;
            }else{
              this.data_path15 = selectedFile.name;
              this.data_path_religion = json.attachmentCode+'/'+json.attachmentName;
            }
          });
      }else{
        Swal.fire(
          this.translate.instant('alert.error_doc'), 
          this.translate.instant('alert.error_doc_detail1')+' '+'<b style="color:red;">'+'5 mb'+'</b>'+' '+this.translate.instant('alert.error_doc_detail2'),
          'error' 
        )
      }
    }
  }

  /******************************* COPY ADDRESS ******************************************/
  dataAddressReg(e){
      this.checked_address = e.target.checked;
      if(this.checked_address == true){
          this.cur_village = this.reg_village;
          this.cur_addr_no = this.reg_addr_no;
          this.cur_alley = this.reg_alley;
          this.cur_road = this.reg_road;
          this.cur_country = this.reg_country;
          this.cur_village_no = this.reg_village_no;
          this.cur_province = this.reg_province;
          this.districtCurList = this.districtRegList;
          this.subDistrictCurList = this.subDistrictRegList;
          this.cur_district = this.reg_district;
          this.cur_sub_district = this.reg_sub_district;
          this.cur_post_code = this.reg_post_code;
      }else{ 
          this.clearDataAddressReg(); 
      }
  }

  dataAddressFather(e){
      this.radio_parent = e.target.value;
      if(this.radio_parent == "F"){
          this.parent_citizen_id = this.father_citizen_id;
          this.parent_title = this.father_title;
          this.ddlTbMstTitleEnParent(); 
          this.parent_title_en =  this.father_title_en;
          this.parent_firstname_th = this.father_firstname_th;
          this.parent_lastname_th = this.father_lastname_th;
          this.parent_firstname_en = this.father_firstname_en;
          this.parent_lastname_en = this.father_lastname_en;
          this.parent_ethnicity = this.father_ethnicity;
          this.parent_citizenship = this.father_citizenship;
          this.parent_religion = this.father_religion;
          this.parent_saint = this.father_saint;
          this.parent_education = this.father_education;
          this.parent_annual_income = this.father_annual_income;
          this.parent_occupation = this.father_occupation;
          this.parent_office = this.father_office;
          this.parent_status = "บิดา";
          this.parent_village = this.father_village;
          this.parent_addr_no = this.father_addr_no;
          this.parent_village_no = this.father_village_no;
          this.parent_alley = this.father_alley;
          this.parent_road = this.father_road;
          this.parent_country = this.father_country;
          this.parent_province = this.father_province;
          this.districtParentList = this.districtFatherList;
          this.subDistrictParentList = this.subDistrictFatherList;
          this.parent_district = this.father_district;
          this.parent_sub_district = this.father_sub_district;
          this.parent_post_code = this.father_post_code;
          this.parent_tel1 = this.father_tel1;
          this.parent_tel2 = this.father_tel2;
          this.parent_email = this.father_email;
      }
  }

  dataAddressMother(e){
      this.radio_parent = e.target.value;
        if(this.radio_parent == "M"){
            this.parent_citizen_id = this.mother_citizen_id;
            this.parent_title = this.mother_title;
            this.ddlTbMstTitleEnParent(); 
            this.parent_title_en =  this.mother_title_en;
            this.parent_firstname_th = this.mother_firstname_th;
            this.parent_lastname_th = this.mother_lastname_th;
            this.parent_firstname_en = this.mother_firstname_en;
            this.parent_lastname_en = this.mother_lastname_en;
            this.parent_ethnicity = this.mother_ethnicity;
            this.parent_citizenship = this.mother_citizenship;
            this.parent_religion = this.mother_religion;
            this.parent_saint = this.mother_saint;
            this.parent_education = this.mother_education;
            this.parent_annual_income = this.mother_annual_income;
            this.parent_occupation = this.mother_occupation;
            this.parent_office = this.mother_office;
            this.parent_status = "มารดา";
            this.parent_village = this.mother_village;
            this.parent_addr_no = this.mother_addr_no;
            this.parent_village_no = this.mother_village_no;
            this.parent_alley = this.mother_alley;
            this.parent_road = this.mother_road;
            this.parent_country = this.mother_country;
            this.parent_province = this.mother_province;
            this.districtParentList = this.districtMotherList;
            this.subDistrictParentList = this.subDistrictMotherList;
            this.parent_district = this.mother_district;
            this.parent_sub_district = this.mother_sub_district;
            this.parent_post_code = this.mother_post_code;
            this.parent_tel1 = this.mother_tel1;
            this.parent_tel2 = this.mother_tel2;
            this.parent_email = this.mother_email;
        }
  }

  clearDataAddressReg(){
      this.cur_village = "";
      this.cur_addr_no = "";
      this.cur_village_no = "";
      this.cur_alley = "";
      this.cur_road = "";
      this.cur_country = undefined;
      this.cur_province = undefined;
      this.cur_district = undefined; 
      this.cur_sub_district = undefined;
      this.cur_post_code = "";
  }

  clearDataAddressParent(e){
      this.radio_parent = e.target.value;
      if(this.radio_parent == 'N'){
          this.parent_citizen_id = '';
          this.parent_title = undefined;
          this.parent_title_en = undefined;
          this.parent_firstname_th = '';
          this.parent_lastname_th = '';
          this.parent_firstname_en = '';
          this.parent_lastname_en = '';
          this.parent_ethnicity = undefined;
          this.parent_citizenship = undefined;
          this.parent_religion = undefined;
          this.parent_saint = '';
          this.parent_education = undefined;
          this.parent_annual_income = '';
          this.parent_occupation = '';
          this.parent_office = '';
          this.parent_status = '';
          this.parent_village = '';
          this.parent_addr_no = '';
          this.parent_village_no = '';
          this.parent_alley = '';
          this.parent_road = '';
          this.parent_country = undefined;
          this.parent_province = undefined;
          this.parent_district = undefined;
          this.parent_sub_district = undefined;
          this.parent_post_code = '';
          this.parent_tel1 = '';
          this.parent_tel2 = '';
          this.parent_email = '';
      }
  }

  /***************************** GET ชื่อนักเรียน/ระดับชั้น ****************************************/
  getFullnameClass1(){
      let json = {'stuCode': this.relatives_stu_code1};
      this.service.httpGet('/api/v1/0/stu-profile/ddl',json).then((res)=>{
          this.stuCode1List = res||[]; 
          if(this.relatives_stu_code1.length == 5){
              for (let data of this.stuCode1List){
                  this.relatives_stu_code1_name = data['fullname'];
                  this.relatives_stu_code1_class = data['classRoom'];
              } 
          }else{
              this.clearGetFullnameClass1();
          }
      });
  }

  getFullnameClass2(){
    let json = {'stuCode': this.relatives_stu_code2};
      this.service.httpGet('/api/v1/0/stu-profile/ddl',json).then((res)=>{
          this.stuCode2List = res||[]; 
          if(this.relatives_stu_code2.length == 5){
              for (let data of this.stuCode2List){
                this.relatives_stu_code2_name = data['fullname'];
                this.relatives_stu_code2_class = data['classRoom'];
              }  
            }else{
              this.clearGetFullnameClass2();
            }  
      });
  }

  getFullnameClass3(){
    let json = {'stuCode': this.relatives_stu_code3};
      this.service.httpGet('/api/v1/0/stu-profile/ddl',json).then((res)=>{
          this.stuCode3List = res||[]; 
          if(this.relatives_stu_code3.length == 5){
              for (let data of this.stuCode3List){
                this.relatives_stu_code3_name = data['fullname'];
                this.relatives_stu_code3_class = data['classRoom'];
              } 
          }else{
              this.clearGetFullnameClass3();
          }
      });
  }

  clearGetFullnameClass1(){
      this.relatives_stu_code1_name = '';
      this.relatives_stu_code1_class = '';
  }

  clearGetFullnameClass2(){
      this.relatives_stu_code2_name = '';
      this.relatives_stu_code2_class = '';
  }

  clearGetFullnameClass3(){
      this.relatives_stu_code3_name = '';
      this.relatives_stu_code3_class = '';
  }

  /***************************** CHECK เลขบัตรประชาชน **************************************/
  checkCitizenId(){ 
      if(this.citizen_id.length == 17){
        let index0 = parseFloat(this.citizen_id[0]);
        let index1 = parseFloat(this.citizen_id[2]);
        let index2 = parseFloat(this.citizen_id[3]);
        let index3 = parseFloat(this.citizen_id[4]);
        let index4= parseFloat(this.citizen_id[5]);
        let index5 = parseFloat(this.citizen_id[7]);
        let index6= parseFloat(this.citizen_id[8]);
        let index7 = parseFloat(this.citizen_id[9]);
        let index8= parseFloat(this.citizen_id[10]);
        let index9 = parseFloat(this.citizen_id[11]);
        let index10 = parseFloat(this.citizen_id[13]);
        let index11 = parseFloat(this.citizen_id[14]);
        let index12 = parseFloat(this.citizen_id[16]);
        let data_index12 = index12.toString();
        let sum = (index0*13)+(index1*12)+(index2*11)+(index3*10)+(index4*9)+(index5*8)+(index6*7)+(index7*6)+(index8*5)+(index9*4)+(index10*3)+(index11*2);
        let modSum = sum%11; 
        let checkDigit = 11-modSum;
        let data_checkDigit = checkDigit.toString();
        if(data_checkDigit.length == 2){
          if(data_index12 == data_checkDigit[1]){
              this.check_citizen_id = true;
          }else{
              this.check_citizen_id = false;
              this.citizen_id = '';
          }
        }else{
          if(data_index12 == data_checkDigit){
            this.check_citizen_id = true;
          }else{
              this.check_citizen_id = false;
              this.citizen_id = '';
          }
        }
      }else{
        this.checked_stu_old = false;
      }
  }
 
  checkFatherCitizenId(){ 
      if(this.father_citizen_id.length == 17){
        let index0 = parseFloat(this.father_citizen_id[0]);
        let index1 = parseFloat(this.father_citizen_id[2]);
        let index2 = parseFloat(this.father_citizen_id[3]);
        let index3 = parseFloat(this.father_citizen_id[4]);
        let index4= parseFloat(this.father_citizen_id[5]);
        let index5 = parseFloat(this.father_citizen_id[7]);
        let index6= parseFloat(this.father_citizen_id[8]);
        let index7 = parseFloat(this.father_citizen_id[9]);
        let index8= parseFloat(this.father_citizen_id[10]);
        let index9 = parseFloat(this.father_citizen_id[11]);
        let index10 = parseFloat(this.father_citizen_id[13]);
        let index11 = parseFloat(this.father_citizen_id[14]);
        let index12 = parseFloat(this.father_citizen_id[16]);
        let data_index12 = index12.toString();
        let sum = (index0*13)+(index1*12)+(index2*11)+(index3*10)+(index4*9)+(index5*8)+(index6*7)+(index7*6)+(index8*5)+(index9*4)+(index10*3)+(index11*2);
        let modSum = sum%11; 
        let checkDigit = 11-modSum;
        let data_checkDigit = checkDigit.toString();
        if(data_checkDigit.length == 2){
          if(data_index12 == data_checkDigit[1]){
              this.check_father_citizen_id = true;
          }else{
              this.check_father_citizen_id = false;
              this.father_citizen_id = '-';
          }
        }else{
          if(data_index12 == data_checkDigit){
              this.check_father_citizen_id = true;
          }else{
              this.check_father_citizen_id = false;
              this.father_citizen_id = '-';
          }
        }
      }
  }

  checkMotherCitizenId(){ 
      if(this.mother_citizen_id.length == 17){
        let index0 = parseFloat(this.mother_citizen_id[0]);
        let index1 = parseFloat(this.mother_citizen_id[2]);
        let index2 = parseFloat(this.mother_citizen_id[3]);
        let index3 = parseFloat(this.mother_citizen_id[4]);
        let index4= parseFloat(this.mother_citizen_id[5]);
        let index5 = parseFloat(this.mother_citizen_id[7]);
        let index6= parseFloat(this.mother_citizen_id[8]);
        let index7 = parseFloat(this.mother_citizen_id[9]);
        let index8= parseFloat(this.mother_citizen_id[10]);
        let index9 = parseFloat(this.mother_citizen_id[11]);
        let index10 = parseFloat(this.mother_citizen_id[13]);
        let index11 = parseFloat(this.mother_citizen_id[14]);
        let index12 = parseFloat(this.mother_citizen_id[16]);
        let data_index12 = index12.toString();
        let sum = (index0*13)+(index1*12)+(index2*11)+(index3*10)+(index4*9)+(index5*8)+(index6*7)+(index7*6)+(index8*5)+(index9*4)+(index10*3)+(index11*2);
        let modSum = sum%11; 
        let checkDigit = 11-modSum;
        let data_checkDigit = checkDigit.toString();
        if(data_checkDigit.length == 2){
          if(data_index12 == data_checkDigit[1]){
              this.check_mother_citizen_id = true;
          }else{
              this.check_mother_citizen_id = false;
              this.mother_citizen_id = '-';
          }
        }else{
          if(data_index12 == data_checkDigit){
              this.check_mother_citizen_id = true;
          }else{
              this.check_mother_citizen_id = false;
              this.mother_citizen_id = '-';
          }
        }
      }
  }
 
  checkParentCitizenId(){ 
      if(this.parent_citizen_id.length == 17){
        let index0 = parseFloat(this.parent_citizen_id[0]);
        let index1 = parseFloat(this.parent_citizen_id[2]);
        let index2 = parseFloat(this.parent_citizen_id[3]);
        let index3 = parseFloat(this.parent_citizen_id[4]);
        let index4= parseFloat(this.parent_citizen_id[5]);
        let index5 = parseFloat(this.parent_citizen_id[7]);
        let index6= parseFloat(this.parent_citizen_id[8]);
        let index7 = parseFloat(this.parent_citizen_id[9]);
        let index8= parseFloat(this.parent_citizen_id[10]);
        let index9 = parseFloat(this.parent_citizen_id[11]);
        let index10 = parseFloat(this.parent_citizen_id[13]);
        let index11 = parseFloat(this.parent_citizen_id[14]);
        let index12 = parseFloat(this.parent_citizen_id[16]);
        let data_index12 = index12.toString();
        let sum = (index0*13)+(index1*12)+(index2*11)+(index3*10)+(index4*9)+(index5*8)+(index6*7)+(index7*6)+(index8*5)+(index9*4)+(index10*3)+(index11*2);
        let modSum = sum%11; 
        let checkDigit = 11-modSum;
        let data_checkDigit = checkDigit.toString();
        if(data_checkDigit.length == 2){
          if(data_index12 == data_checkDigit[1]){
              this.check_parent_citizen_id = true;
          }else{
              this.check_parent_citizen_id = false;
              this.parent_citizen_id = '-';
          }
        }else{
          if(data_index12 == data_checkDigit){
              this.check_parent_citizen_id = true;
          }else{
              this.check_parent_citizen_id = false;
              this.parent_citizen_id = '-';
          }
        }
      }
  }

  /***************************** CHECK BOX เลือกกรอกความพิการ *********************************/
  onCheck(e){
      this.is_disabled = e.target.checked;
  }

  /******************************* DROP DOWN สถานะครอบครัว ***********************************/
  ddlTbMstFamilyStatus(){
    this.service.httpGet('/api/v1/0/tbMstFamilyStatus/ddl',null).then((res)=>{
        this.family_statusList = res||[];
    });
  }

  /***************************** DROP DOWN ข้อมูลการศึกษา พ่อ แม่ *******************************/
  ddlTbMstEducation(){
    this.service.httpGet('/api/v1/0/tbMstEducation/ddl',null).then((res)=>{
        this.educationList = res||[];
    });
  }

  /********************** DROP DOWN คำนำหน้าชื่อนักเรียน พ่อ แม่ ผู้ปกครอง **************************/
  ddlTbMstTitle(){
    this.service.httpGet('/api/v1/0/tbMstTitle/ddlStu',null).then((res)=>{
        this.titleList = res||[];
    });
  }

  ddlTbMstTitleEn(){
    if(this.title_desc == 'ด.ช.'){
      this.service.httpGet('/api/v1/0/tbMstTitle/ddlEn',null).then((res)=>{
          this.titleEnList = res||[];
          this.title_en = res[0]['name'];
      });
    }else if(this.title_desc == 'ด.ญ.'){
      this.service.httpGet('/api/v1/0/tbMstTitle/ddlEn',null).then((res)=>{
        this.titleEnList = res||[];
        this.title_en = res[1]['name'];
      });
    }else if(this.title_desc == 'นาย'){
      this.service.httpGet('/api/v1/0/tbMstTitle/ddlEn',null).then((res)=>{
        this.titleEnList = res||[];
        this.title_en = res[2]['name'];
      });
    }else if(this.title_desc == 'น.ส.'){
      this.service.httpGet('/api/v1/0/tbMstTitle/ddlEn',null).then((res)=>{
        this.titleEnList = res||[];
        this.title_en = res[1]['name'];
      });
    }else{
      this.service.httpGet('/api/v1/0/tbMstTitle/ddlEn',null).then((res)=>{
        this.titleEnList = res||[];
      });
    }
  }

  ddlTbMstTitleFather(){
    this.service.httpGet('/api/v1/0/tbMstTitle/ddlFather',null).then((res)=>{
        this.titleFatherList = res||[];
    });
  }

  ddlTbMstTitleEnFather(){
    if(this.father_title == 'นาย'){
      this.service.httpGet('/api/v1/0/tbMstTitle/ddlEnFather',null).then((res)=>{
          this.titleEnFatherList = res||[];
          this.father_title_en = res[0]['descTh'];
      });
    }else{
      this.titleEnFatherList = [];
    }
  }
  
  ddlTbMstTitleMother(){
    this.service.httpGet('/api/v1/0/tbMstTitle/ddlMother',null).then((res)=>{
        this.titleMotherList = res||[];
    });
  }

  ddlTbMstTitleEnMother(){
    if(this.mother_title == 'น.ส.'){
      this.service.httpGet('/api/v1/0/tbMstTitle/ddlEnMother',null).then((res)=>{
          this.titleEnMotherList = res||[];
          this.mother_title_en = res[0]['descTh'];
      });
    }else if(this.mother_title == 'นาง'){
      this.service.httpGet('/api/v1/0/tbMstTitle/ddlEnMother',null).then((res)=>{
        this.titleEnMotherList = res||[];
        this.mother_title_en = res[1]['descTh'];
      });
    }else{
      this.titleEnMotherList = [];
    }
  }

  ddlTbMstTitleParent(){
    this.service.httpGet('/api/v1/0/tbMstTitle/ddlParent',null).then((res)=>{
        this.titleParentList = res||[];
    });
  }

  ddlTbMstTitleEnParent(){
    if(this.parent_title == 'นาย'){
      this.service.httpGet('/api/v1/0/tbMstTitle/ddlEnParent',null).then((res)=>{
          this.titleEnParentList = res||[];
          this.parent_title_en = res[1]['descTh'];
      });
    }else if(this.parent_title == 'นาง'){
      this.service.httpGet('/api/v1/0/tbMstTitle/ddlEnParent',null).then((res)=>{
        this.titleEnParentList = res||[];
        this.parent_title_en = res[2]['descTh'];
      });
    }else if(this.parent_title == 'น.ส.'){
      this.service.httpGet('/api/v1/0/tbMstTitle/ddlEnParent',null).then((res)=>{
        this.titleEnParentList = res||[];
        this.parent_title_en = res[0]['descTh'];
      });
    }else{
      this.titleEnParentList = [];
    }
  }

  /*********************************** DROP DOWN เพศ ***************************************/
  ddlTbMstGender(){
    if(this.title_desc == 'ด.ช.' || this.title_desc == 'นาย'){
      this.service.httpGet('/api/v1/0/tbMstGender/titleBoy',null).then((res)=>{
          this.genderList = res||[];
          this.gender = res[0]['genderDescTh']
      });
    }else{
      this.service.httpGet('/api/v1/0/tbMstGender/titleGirl',null).then((res)=>{
          this.genderList = res||[];
          this.gender = res[0]['genderDescTh']
      });
    }
  }

  /********************************** DROP DOWN ศาสนา **************************************/
  ddlTbMstReligion(){
    this.service.httpGet('/api/v1/0/tbMstReligion/ddl',null).then((res)=>{
        this.religionList = res||[];
    });
  }

  ddlFatherTbMstReligion(){
    this.service.httpGet('/api/v1/0/tbMstReligion/ddlFather',null).then((res)=>{
        this.religionFatherList = res||[];
    });
  }

  ddlMotherTbMstReligion(){
    this.service.httpGet('/api/v1/0/tbMstReligion/ddlMother',null).then((res)=>{
        this.religionMotherList = res||[];
    });
  }

  ddlParentTbMstReligion(){
    this.service.httpGet('/api/v1/0/tbMstReligion/ddlParent',null).then((res)=>{
        this.religionParentList = res||[];
    });
  }

  /********************************* DROP DOWN กรุ๊ปเลือด *************************************/
  ddlTbMstBloodType(){
    this.service.httpGet('/api/v1/0/tbMstBloodType/ddl',null).then((res)=>{
        this.bloodTypeList = res||[];
    });
  }

  /********************************* DROP DOWN สัญชาติ **************************************/
  ddlCitizenship(){
    this.service.httpGet('/api/v1/0/tbMstCountry/ddl',null).then((res)=>{
        this.citizenshipList = res||[];
        this.ethnicity = res[147]['countryEn'];
        this.citizenship = res[147]['countryEn'];
        this.cur_country = res[147]['countryEn'];
        this.reg_country = res[147]['countryEn'];
    });
  }

  ddlFatherCitizenship(){
    this.service.httpGet('/api/v1/0/tbMstCountry/ddlFather',null).then((res)=>{
        this.citizenshipFatherList = res||[];
        this.father_ethnicity = res[147]['countryEn'];
        this.father_country = res[147]['countryEn'];
        this.father_citizenship = res[147]['countryEn'];
    });
  }
 
  ddlMotherCitizenship(){
    this.service.httpGet('/api/v1/0/tbMstCountry/ddlMother',null).then((res)=>{
        this.citizenshipMotherList = res||[];
        this.mother_ethnicity = res[147]['countryEn'];
        this.mother_country = res[147]['countryEn'];
        this.mother_citizenship = res[147]['countryEn'];
    });
  } 

  ddlParentCitizenship(){
    this.service.httpGet('/api/v1/0/tbMstCountry/ddlParent',null).then((res)=>{
        this.citizenshipParentList = res||[];
        this.parent_ethnicity = res[147]['countryEn'];
        this.parent_country = res[147]['countryEn'];
        this.parent_citizenship = res[147]['countryEn'];
    });
  }

  /******************************* DROP DOWN ชั้นเรียนสุดท้าย ***********************************/
  ddlLastClass(){
    this.service.httpGet('/api/v1/0/tbMstClass/ddl',null).then((res)=>{
      this.lastClassList = res||[];
    });
  }

  /********************* DROP DOWN จังหวัด อำเภอ ตำบล ตามทะเบียนบ้าน ***************************/
  ddlRegTbMstProvince(){
    this.service.httpGet('/api/v1/0/tbMstProvince/ddlReg',null).then((res)=>{
        this.provinceRegList = res||[];
    });
  }

  ddlRegTbMstDistrict(){
    let json = {'provinceThReg':this.reg_province};
    this.service.httpGet('/api/v1/0/tbMstDistrict/ddlReg',json).then((res)=>{
        this.districtRegList = res||[];
    });
  }

  ddlRegTbMstSubDistrict(){
    let json = {'districtThReg':this.reg_district};
    this.service.httpGet('/api/v1/0/tbMstSubDistrict/ddlReg',json).then((res)=>{
        this.subDistrictRegList = res||[];
    });
  }

  changeRegPostCode(){
    let json = {'districtThReg':this.reg_district,'subdistrictReg':this.reg_sub_district}
    this.service.httpGet('/api/v1/0/tbMstSubDistrict/ddlRegPostcode',json).then((res)=>{
        this.postCodeList = res||[];
        for(let ele of this.postCodeList){
          this.reg_post_code = ele['postCode'];
        }
    });
  }

  /********************* DROP DOWN จังหวัด อำเภอ ตำบล ตามที่อยู่ปัจจุบัน ***************************/
  ddlCurTbMstProvince(){
    this.service.httpGet('/api/v1/0/tbMstProvince/ddlCur',null).then((res)=>{
        this.provinceCurList = res||[];
    }); 
  }

  ddlCurTbMstDistrict(){
    let json = {'provinceThCur':this.cur_province};
    this.service.httpGet('/api/v1/0/tbMstDistrict/ddlCur',json).then((res)=>{
        this.districtCurList = res||[];
    });
  }

  ddlCurTbMstSubDistrict(){
    let json = {'districtThCur':this.cur_district,'subdistrictCur':this.cur_sub_district};
    this.service.httpGet('/api/v1/0/tbMstSubDistrict/ddlCur',json).then((res)=>{
        this.subDistrictCurList = res||[];
    });
  }

  changeCurPostCode(){
    let json = {'districtThCur':this.cur_district,'subdistrictCur':this.cur_sub_district}
    this.service.httpGet('/api/v1/0/tbMstSubDistrict/ddlCurPostcode',json).then((res)=>{
        this.postCodeList = res||[];
        for(let ele of this.postCodeList){
          this.cur_post_code = ele['postCode'];
        }
    });
  }

  /********************* DROP DOWN จังหวัด อำเภอ ตำบล ตามสถานที่เกิด ***************************/
  ddlPobTbMstProvince(){
    this.service.httpGet('/api/v1/0/tbMstProvince/ddlPob',null).then((res)=>{
        this.provincePobList = res||[];
    });
  }

  ddlPobTbMstDistrict(){
    let json = {'provinceThPob':this.pob_province};
    this.service.httpGet('/api/v1/0/tbMstDistrict/ddlPob',json).then((res)=>{
        this.districtPobList = res||[];
    });
  }

  ddlPobTbMstSubDistrict(){
    let json = {'districtThPob':this.pob_district};
    this.service.httpGet('/api/v1/0/tbMstSubDistrict/ddlPob',json).then((res)=>{
        this.subDistrictPobList = res||[];
    });
  }

  /************************ DROP DOWN จังหวัด อำเภอ ตำบล ของพ่อ *****************************/
  ddlFatherTbMstProvince(){
    this.service.httpGet('/api/v1/0/tbMstProvince/ddlFather',null).then((res)=>{
        this.provinceFatherList = res||[];
    });
  }

  ddlFatherTbMstDistrict(){
    let json = {'provinceThFather':this.father_province};
    this.service.httpGet('/api/v1/0/tbMstDistrict/ddlFather',json).then((res)=>{
        this.districtFatherList = res||[];
    });
  }

  ddlFatherTbMstSubDistrict(){
    let json = {'districtThFather':this.father_district};
    this.service.httpGet('/api/v1/0/tbMstSubDistrict/ddlFather',json).then((res)=>{
        this.subDistrictFatherList = res||[];
    });
  }

  changeFatherPostCode(){
    let json = {'districtThCur':this.father_district,'subdistrictCur':this.father_sub_district}
    this.service.httpGet('/api/v1/0/tbMstSubDistrict/ddlCurPostcode',json).then((res)=>{
        this.postCodeList = res||[];
        for(let ele of this.postCodeList){
          this.father_post_code = ele['postCode'];
        }
    });
  }

  /************************ DROP DOWN จังหวัด อำเภอ ตำบล ของแม่ *****************************/
  ddlMotherTbMstProvince(){
    this.service.httpGet('/api/v1/0/tbMstProvince/ddlMother',null).then((res)=>{
        this.provinceMotherList = res||[];
    });
  }

  ddlMotherTbMstDistrict(){
    let json = {'provinceThMother':this.mother_province};
    this.service.httpGet('/api/v1/0/tbMstDistrict/ddlMother',json).then((res)=>{
        this.districtMotherList = res||[];
    });
  }

  ddlMotherTbMstSubDistrict(){
    let json = {'districtThMother':this.mother_district};
    this.service.httpGet('/api/v1/0/tbMstSubDistrict/ddlMother',json).then((res)=>{
        this.subDistrictMotherList = res||[];
    });
  }

  changeMotherPostCode(){
    let json = {'districtThCur':this.mother_district,'subdistrictCur':this.mother_sub_district}
    this.service.httpGet('/api/v1/0/tbMstSubDistrict/ddlCurPostcode',json).then((res)=>{
        this.postCodeList = res||[];
        for(let ele of this.postCodeList){
          this.mother_post_code = ele['postCode'];
        }
    });
  }

  /********************** DROP DOWN จังหวัด อำเภอ ตำบล ของผู้ปกครอง ***************************/
  ddlParentTbMstProvince(){
    this.service.httpGet('/api/v1/0/tbMstProvince/ddlParent',null).then((res)=>{
        this.provinceParentList = res||[];
    });
  }

  ddlParentTbMstDistrict(){
    let json = {'provinceThParent':this.parent_province};
    this.service.httpGet('/api/v1/0/tbMstDistrict/ddlParent',json).then((res)=>{
        this.districtParentList = res||[];
    });
  }

  ddlParentTbMstSubDistrict(){
    let json = {'districtThParent':this.parent_district};
    this.service.httpGet('/api/v1/0/tbMstSubDistrict/ddlParent',json).then((res)=>{
        this.subDistrictParentList = res||[];
    }); 
  }
 
  changeParentPostCode(){
    let json = {'districtThCur':this.parent_district,'subdistrictCur':this.parent_sub_district}
    this.service.httpGet('/api/v1/0/tbMstSubDistrict/ddlCurPostcode',json).then((res)=>{
        this.postCodeList = res||[]; 
        for(let ele of this.postCodeList){ 
          this.parent_post_code = ele['postCode'];
        }
    });
  } 

  /************************************** CLEAR DATA ***************************************/
  onClear(){ 
      this.last_class = undefined;
      this.title_desc = undefined;
      this.title_en = undefined;
      this.gender = undefined;
      this.request_id = '';
      this.gender_id = '';
      this.nickname = '-';
      this.ethnicity = 'Thailand';
      this.citizenship = 'Thailand';
      this.religion = undefined;
      this.saint = '';
      this.church = '';
      this.dob = null;
      this.count_dob = '';
      this.blood_type = undefined;
      this.weight = '0'; 
      this.height = '0';
      this.tel1 = '-';
      this.tel2 = '-';
      this.special_skill = '';
      this.to_class = undefined;
      this.ex_school = '-';
      this.ex_gpa = '0.00';
      this.reg_village = '';
      this.reg_house_no = '';
      this.reg_addr_no = '-';
      this.reg_village_no = '-';
      this.reg_alley = '';
      this.reg_road = '';
      this.reg_country = 'Thailand';
      this.reg_province = undefined;
      this.reg_district = undefined;
      this.reg_sub_district = undefined;
      this.reg_post_code = '';
      this.cur_village = '';
      this.cur_addr_no = '-';
      this.cur_village_no = '-';
      this.cur_alley = '';
      this.cur_road = '';
      this.cur_country = 'Thailand';
      this.cur_province = undefined;
      this.cur_district = undefined;
      this.cur_sub_district = undefined;
      this.cur_post_code = '';
      this.pob = '';
      this.pob_province = undefined;
      this.pob_district = undefined;
      this.pob_sub_district = undefined;
      this.no_of_relatives = 0;
      this.relatives_stu_code1 = '';
      this.relatives_stu_code1_name = '';
      this.relatives_stu_code1_class = '';
      this.relatives_stu_code2 = '';
      this.relatives_stu_code2_name = '';
      this.relatives_stu_code2_class = '';
      this.relatives_stu_code3 = '';
      this.relatives_stu_code3_name = '';
      this.relatives_stu_code3_class = '';
      this.medical_info = '';
      this.is_disabled = null;
      this.disability = '';
      this.family_status = undefined;
      this.father_citizen_id = '-';
      this.father_title = undefined;
      this.father_title_en = undefined;
      this.father_firstname_th = '-';
      this.father_lastname_th = '-';
      this.father_firstname_en = '-';
      this.father_lastname_en = '-';
      this.father_ethnicity = 'Thailand';
      this.father_citizenship = 'Thailand';
      this.father_religion = undefined;
      this.father_saint = '';
      this.father_education = undefined;
      this.father_annual_income = '';
      this.father_occupation = '';
      this.father_office = '';
      this.father_village = '';
      this.father_addr_no = '-';
      this.father_village_no = '-';
      this.father_alley = '';
      this.father_road = '';
      this.father_country = 'Thailand';
      this.father_province = undefined;
      this.father_district = undefined;
      this.father_sub_district = undefined;
      this.father_post_code = '';
      this.father_tel1 = '-';
      this.father_tel2 = '-';
      this.father_email = '';
      this.mother_citizen_id = '-';
      this.mother_title = undefined;
      this.mother_title_en = undefined;
      this.mother_firstname_th = '-';
      this.mother_lastname_th = '-'; 
      this.mother_firstname_en = '-'; 
      this.mother_lastname_en = '-';
      this.mother_ethnicity = 'Thailand'; 
      this.mother_citizenship = 'Thailand';
      this.mother_religion = undefined;
      this.mother_saint = '';
      this.mother_education = undefined;
      this.mother_annual_income = '';
      this.mother_occupation = '';
      this.mother_office = '';
      this.mother_village = '';
      this.mother_addr_no = '-';
      this.mother_village_no = '-';
      this.mother_alley = '';
      this.mother_road = '';
      this.mother_country = 'Thailand';
      this.mother_province = undefined;
      this.mother_district = undefined;
      this.mother_sub_district = undefined;
      this.mother_post_code = '';
      this.mother_tel1 = '-';
      this.mother_tel2 = '-';
      this.mother_email = '';
      this.parent_citizen_id = '-';
      this.parent_title = undefined;
      this.parent_title_en = undefined;
      this.parent_firstname_th = '-';
      this.parent_lastname_th = '-';
      this.parent_firstname_en = '-';
      this.parent_lastname_en = '-';
      this.parent_ethnicity = 'Thailand';
      this.parent_citizenship = 'Thailand';
      this.parent_religion = undefined;
      this.parent_saint = '';
      this.parent_education = undefined;
      this.parent_annual_income = '';
      this.parent_occupation = '';
      this.parent_office = '';
      this.parent_status = '';
      this.parent_village = '';
      this.parent_addr_no = '-';
      this.parent_village_no = '-';
      this.parent_alley = '';
      this.parent_road = '';
      this.parent_country = 'Thailand';
      this.parent_province = undefined;
      this.parent_district = undefined;
      this.parent_sub_district = undefined;
      this.parent_post_code = '';
      this.parent_tel1 = '-';
      this.parent_tel2 = '-';
      this.parent_email = '';
      this.form.controls.stuStatusId.setValue('');
      this.form.controls.stuStatus.setValue('');
      this.checked_stu_old = false;
      this.radio_parent = "N";
      this.validateValue = false;

      this.data_path1 = '';
      this.data_path2 = '';
      this.data_path3 = '';
      this.data_path4 = '';
      this.data_path5 = '';
      this.data_path6 = '';
      this.data_path7 = ''; 
      this.data_path8 = '';
      this.data_path9 = '';
      this.data_path10 = '';
      this.data_path11 = '';
      this.data_path12 = '';
      this.data_path13 = '';
      this.data_path14 = '';
      this.data_path15 = '';
  }
  
  /********************************* SAVE ก่อน exportPDF *************************************/
  onSaveBeforePDF(){ 
    if(this.citizen_id.length == 17){
      this.service.httpGet('/api/v1/tbStuProfileSingup',null).then((res: IResponse)=>{
          this.refNoList = res.responseData||[];
          let refno_prefix = this.to_year_group.substr(2);
          let db_maxref_no = res.responseData[0]['max_ref_no'];
          let db_year = db_maxref_no.toString().substr(0,4);
          if(db_maxref_no == 0){
              this.request_id = refno_prefix+'0001';
          }else{
              if(refno_prefix != db_year){
                  this.request_id = refno_prefix+'0001';
              }else{
                  if(refno_prefix != db_year){
                      this.request_id = refno_prefix+'0001';
                  }else{
                      let substr_db_maxref_no = db_maxref_no.toString().substr(4);
                      let convert_db_maxref_no = Number(substr_db_maxref_no);
                      let sum_refno = convert_db_maxref_no+1;
                      if(sum_refno.toString().length == 1){
                        this.request_id = refno_prefix+'000'+sum_refno;
                      }else if(sum_refno.toString().length == 2){
                        this.request_id = refno_prefix+'00'+sum_refno;
                      }else if(sum_refno.toString().length == 3){
                        this.request_id = refno_prefix+'0'+sum_refno;
                      }else{
                        this.request_id = refno_prefix+sum_refno;
                      }
                  }
              }
          }
          let sc_code = "PRAMANDA"; 
          this.stu_status_id = 1;
          this.stu_status = "สมัครเรียน";

          let concat_str = this.citizen_id.toString().substring(0,1)+this.citizen_id.toString().substring(2,6)+this.citizen_id.toString().substring(7,12)+this.citizen_id.toString().substring(13,15)+this.citizen_id.toString().substring(16);
          this.form.controls.citizenId.setValue(concat_str);

          this.form.controls.requestId.setValue(this.request_id);
          this.form.controls.stuCode.setValue(this.stu_code);
          this.form.controls.scCode.setValue(sc_code);
          this.form.controls.lastClass.setValue(this.last_class);
          this.form.controls.title.setValue(this.title_desc);
          this.form.controls.titleEn.setValue(this.title_en);
          this.form.controls.gender.setValue(this.gender);
          this.form.controls.firstnameTh.setValue(this.firstname_th);
          this.form.controls.lastnameTh.setValue(this.lastname_th);
          this.form.controls.firstnameEn.setValue(this.firstname_en);
          this.form.controls.lastnameEn.setValue(this.lastname_en);
          this.form.controls.familyStatus.setValue(this.family_status);
          this.form.controls.familyStatusDetail.setValue(this.family_status_detail);
          this.form.controls.nickname.setValue(this.nickname);
          this.form.controls.ethnicity.setValue(this.ethnicity);
          this.form.controls.citizenship.setValue(this.citizenship);
          this.form.controls.religion.setValue(this.religion);
          this.form.controls.saint.setValue(this.saint);
          this.form.controls.church.setValue(this.church);
          this.form.controls.dob.setValue(this.dob);
          this.form.controls.bloodType.setValue(this.blood_type);
          this.form.controls.weight.setValue(this.weight);
          this.form.controls.height.setValue(this.height);

          if(this.tel1.length == 12){
            let concat_tel1 = this.tel1.toString().substring(0,2)+this.tel1.toString().substring(3,7)+this.tel1.toString().substring(8);
            this.tel1 = concat_tel1;
          }else{
            this.tel1 = '-';
          }
          this.form.controls.tel1.setValue(this.tel1);

          if(this.tel2.length == 12){
            let concat_tel2 = this.tel2.toString().substring(0,2)+this.tel2.toString().substring(3,7)+this.tel2.toString().substring(8);
            this.tel2 = concat_tel2;
          }else{
            this.tel2 = '-';
          }
          this.form.controls.tel2.setValue(this.tel2);

          this.form.controls.email.setValue(this.email);
          this.form.controls.specialSkill.setValue(this.special_skill);

          if(this.to_class == undefined || this.to_class == null){
            this.to_class = '';
          }
          this.form.controls.toClass.setValue(this.to_class);

          this.form.controls.toYearGroup.setValue(this.to_year_group);
          this.form.controls.exSchool.setValue(this.ex_school);
          this.form.controls.exGpa.setValue(this.ex_gpa);
          this.form.controls.regVillage.setValue(this.reg_village);
          this.form.controls.regHouseNo.setValue(this.reg_house_no);
          this.form.controls.regAddrNo.setValue(this.reg_addr_no);
          this.form.controls.regVillageNo.setValue(this.reg_village_no);
          this.form.controls.regAlley.setValue(this.reg_alley);
          this.form.controls.regRoad.setValue(this.reg_road);
          this.form.controls.regCountry.setValue(this.reg_country);
          this.form.controls.regProvince.setValue(this.reg_province);
          this.form.controls.regDistrict.setValue(this.reg_district);
          this.form.controls.regSubDistrict.setValue(this.reg_sub_district);
          this.form.controls.regPostCode.setValue(this.reg_post_code);
          this.form.controls.curVillage.setValue(this.cur_village);
          this.form.controls.curAddrNo.setValue(this.cur_addr_no);
          this.form.controls.curVillageNo.setValue(this.cur_village_no);
          this.form.controls.curAlley.setValue(this.cur_alley);
          this.form.controls.curRoad.setValue(this.cur_road);
          this.form.controls.curCountry.setValue(this.cur_country);
          this.form.controls.curProvince.setValue(this.cur_province);
          this.form.controls.curDistrict.setValue(this.cur_district);
          this.form.controls.curSubDistrict.setValue(this.cur_sub_district);;
          this.form.controls.curPostCode.setValue(this.cur_post_code);
          this.form.controls.pob.setValue(this.pob);
          this.form.controls.pobProvince.setValue(this.pob_province);
          this.form.controls.pobDistrict.setValue(this.pob_district);
          this.form.controls.pobSubDistrict.setValue(this.pob_sub_district);
          this.form.controls.noOfRelatives.setValue(this.no_of_relatives);
          this.form.controls.relativesStuCode1.setValue(this.relatives_stu_code1);
          this.form.controls.relativesStuCode2.setValue(this.relatives_stu_code2);
          this.form.controls.relativesStuCode3.setValue(this.relatives_stu_code3);
          this.form.controls.medicalInfo.setValue(this.medical_info);
          this.form.controls.isDisabled.setValue(this.is_disabled);
          this.form.controls.disability.setValue(this.disability);

          if(this.father_citizen_id.length == 17){
            let concat_father_citizen = this.father_citizen_id.toString().substring(0,1)+this.father_citizen_id.toString().substring(2,6)+this.father_citizen_id.toString().substring(7,12)+this.father_citizen_id.toString().substring(13,15)+this.father_citizen_id.toString().substring(16);
            this.father_citizen_id = concat_father_citizen;
          }else{
            this.father_citizen_id = '-';
          }
          this.form.controls.fatherCitizenId.setValue(this.father_citizen_id);

          this.form.controls.fatherTitle.setValue(this.father_title);
          this.form.controls.fatherTitleEn.setValue(this.father_title_en);
          this.form.controls.fatherFirstnameTh.setValue(this.father_firstname_th);
          this.form.controls.fatherLastnameTh.setValue(this.father_lastname_th);
          this.form.controls.fatherFirstnameEn.setValue(this.father_firstname_en);
          this.form.controls.fatherLastnameEn.setValue(this.father_lastname_en);
          this.form.controls.fatherEthnicity.setValue(this.father_ethnicity);
          this.form.controls.fatherCitizenship.setValue(this.father_citizenship);
          this.form.controls.fatherReligion.setValue(this.father_religion);
          this.form.controls.fatherSaint.setValue(this.father_saint);
          this.form.controls.fatherEducation.setValue(this.father_education);
          this.form.controls.fatherAnnualIncome.setValue(this.father_annual_income);
          this.form.controls.fatherOccupation.setValue(this.father_occupation);
          this.form.controls.fatherOffice.setValue(this.father_office);
          this.form.controls.fatherVillage.setValue(this.father_village);
          this.form.controls.fatherAddrNo.setValue(this.father_addr_no);
          this.form.controls.fatherVillageNo.setValue(this.father_village_no);
          this.form.controls.fatherAlley.setValue(this.father_alley);
          this.form.controls.fatherRoad.setValue(this.father_road);
          this.form.controls.fatherCountry.setValue(this.father_country);
          this.form.controls.fatherProvince.setValue(this.father_province);
          this.form.controls.fatherDistrict.setValue(this.father_district);
          this.form.controls.fatherSubDistrict.setValue(this.father_sub_district);
          this.form.controls.fatherPostCode.setValue(this.father_post_code);

          if(this.father_tel1.length == 12){
            let concat_father_tel1 = this.father_tel1.toString().substring(0,2)+this.father_tel1.toString().substring(3,7)+this.father_tel1.toString().substring(8);
            this.father_tel1 = concat_father_tel1;
          }else{
            this.father_tel1 = '-';
          }
          this.form.controls.fatherTel1.setValue(this.father_tel1);

          if(this.father_tel2.length == 12){
            let concat_father_tel2 = this.father_tel2.toString().substring(0,2)+this.father_tel2.toString().substring(3,7)+this.father_tel2.toString().substring(8);
            this.father_tel2 = concat_father_tel2;
          }else{
            this.father_tel2 = '-';
          }
          this.form.controls.fatherTel2.setValue(this.father_tel2);

          this.form.controls.fatherEmail.setValue(this.father_email);
          this.form.controls.fatherAlive.setValue(this.father_alive);

          if(this.mother_citizen_id.length == 17){
            let concat_mother_citizen = this.mother_citizen_id.toString().substring(0,1)+this.mother_citizen_id.toString().substring(2,6)+this.mother_citizen_id.toString().substring(7,12)+this.mother_citizen_id.toString().substring(13,15)+this.mother_citizen_id.toString().substring(16);
            this.mother_citizen_id = concat_mother_citizen;
          }else{
            this.mother_citizen_id = '-';
          }
          this.form.controls.motherCitizenId.setValue(this.mother_citizen_id);

          this.form.controls.motherTitle.setValue(this.mother_title);
          this.form.controls.motherTitleEn.setValue(this.mother_title_en);
          this.form.controls.motherFirstnameTh.setValue(this.mother_firstname_th);
          this.form.controls.motherLastnameTh.setValue(this.mother_lastname_th);
          this.form.controls.motherFirstnameEn.setValue(this.mother_firstname_en);
          this.form.controls.motherLastnameEn.setValue(this.mother_lastname_en);
          this.form.controls.motherEthnicity.setValue(this.mother_ethnicity);
          this.form.controls.motherCitizenship.setValue(this.mother_citizenship);
          this.form.controls.motherReligion.setValue(this.mother_religion);
          this.form.controls.motherSaint.setValue(this.mother_saint);
          this.form.controls.motherEducation.setValue(this.mother_education);
          this.form.controls.motherAnnualIncome.setValue(this.mother_annual_income);
          this.form.controls.motherOccupation.setValue(this.mother_occupation);
          this.form.controls.motherOffice.setValue(this.mother_office);
          this.form.controls.motherVillage.setValue(this.mother_village);
          this.form.controls.motherAddrNo.setValue(this.mother_addr_no);
          this.form.controls.motherVillageNo.setValue(this.mother_village_no);
          this.form.controls.motherAlley.setValue(this.mother_alley);
          this.form.controls.motherRoad.setValue(this.mother_road);
          this.form.controls.motherCountry.setValue(this.mother_country);
          this.form.controls.motherProvince.setValue(this.mother_province);
          this.form.controls.motherDistrict.setValue(this.mother_district);
          this.form.controls.motherSubDistrict.setValue(this.mother_sub_district);
          this.form.controls.motherPostCode.setValue(this.mother_post_code);

          if(this.mother_tel1.length == 12){
            let concat_mother_tel1 = this.mother_tel1.toString().substring(0,2)+this.mother_tel1.toString().substring(3,7)+this.mother_tel1.toString().substring(8);
            this.mother_tel1 = concat_mother_tel1;
          }else{
            this.mother_tel1 = '-';
          }
          this.form.controls.motherTel1.setValue(this.mother_tel1);

          if(this.mother_tel2.length == 12){
            let concat_mother_tel2 = this.mother_tel2.toString().substring(0,2)+this.mother_tel2.toString().substring(3,7)+this.mother_tel2.toString().substring(8);
            this.mother_tel2 = concat_mother_tel2;
          }else{
            this.mother_tel2 = '-';
          }
          this.form.controls.motherTel2.setValue(this.mother_tel2);

          this.form.controls.motherEmail.setValue(this.mother_email);
          this.form.controls.motherAlive.setValue(this.mother_alive);

          if(this.parent_citizen_id.length == 17){
            let concat_parent_citizen = this.parent_citizen_id.toString().substring(0,1)+this.parent_citizen_id.toString().substring(2,6)+this.parent_citizen_id.toString().substring(7,12)+this.parent_citizen_id.toString().substring(13,15)+this.parent_citizen_id.toString().substring(16);
            this.parent_citizen_id = concat_parent_citizen;
          }else{
            this.parent_citizen_id = '-';
          }
          this.form.controls.parentCitizenId.setValue(this.parent_citizen_id);

          this.form.controls.parentTitle.setValue(this.parent_title);
          this.form.controls.parentTitleEn.setValue(this.parent_title_en);
          this.form.controls.parentFirstnameTh.setValue(this.parent_firstname_th);
          this.form.controls.parentLastnameTh.setValue(this.parent_lastname_th);
          this.form.controls.parentFirstnameEn.setValue(this.parent_firstname_en);
          this.form.controls.parentLastnameEn.setValue(this.parent_lastname_en);
          this.form.controls.parentEthnicity.setValue(this.parent_ethnicity);
          this.form.controls.parentCitizenship.setValue(this.parent_citizenship);
          this.form.controls.parentReligion.setValue(this.parent_religion);
          this.form.controls.parentSaint.setValue(this.parent_saint);
          this.form.controls.parentEducation.setValue(this.parent_education);
          this.form.controls.parentAnnualIncome.setValue(this.parent_annual_income);
          this.form.controls.parentOccupation.setValue(this.parent_occupation);
          this.form.controls.parentOffice.setValue(this.parent_office);
          this.form.controls.parentStatus.setValue(this.parent_status);
          this.form.controls.parentVillage.setValue(this.parent_village);
          this.form.controls.parentAddrNo.setValue(this.parent_addr_no);
          this.form.controls.parentVillageNo.setValue(this.parent_village_no);
          this.form.controls.parentAlley.setValue(this.parent_alley);
          this.form.controls.parentRoad.setValue(this.parent_road);
          this.form.controls.parentCountry.setValue(this.parent_country);
          this.form.controls.parentProvince.setValue(this.parent_province);
          this.form.controls.parentDistrict.setValue(this.parent_district);
          this.form.controls.parentSubDistrict.setValue(this.parent_sub_district);
          this.form.controls.parentPostCode.setValue(this.parent_post_code);

          if(this.parent_tel1.length == 12){
            let concat_parent_tel1 = this.parent_tel1.toString().substring(0,2)+this.parent_tel1.toString().substring(3,7)+this.parent_tel1.toString().substring(8);
            this.parent_tel1 = concat_parent_tel1;
          }else{
            this.parent_tel1 = '-';
          }
          this.form.controls.parentTel1.setValue(this.parent_tel1);

          if(this.parent_tel2.length == 12){
            let concat_parent_tel2 = this.parent_tel2.toString().substring(0,2)+this.parent_tel2.toString().substring(3,7)+this.parent_tel2.toString().substring(8);
            this.parent_tel2 = concat_parent_tel2;
          }else{
            this.parent_tel2 = '-';
          }
          this.form.controls.parentTel2.setValue(this.parent_tel2);

          this.form.controls.parentEmail.setValue(this.parent_email);
          this.form.controls.stuStatusId.setValue(this.stu_status_id);
          this.form.controls.stuStatus.setValue(this.stu_status);
          this.form.controls.stuImage.setValue(null);
          
          if (this.form.invalid) { 
              this.validateValue = true;
              Swal.fire(
                this.translate.instant('alert.error'),
                this.translate.instant('alert.validate'),
                'error' 
              )
            return;
          }
          this.validateValue = false; 
          Swal.fire({
              title: this.translate.instant('alert.report'),
              icon : 'info',
              showCancelButton: true,
              confirmButtonColor: '#3085d6',
              confirmButtonText: this.translate.instant('psms.DL0008'),
              cancelButtonText: this.translate.instant('psms.DL0009')
          }).then((result) => {
            if(result.dismiss == 'cancel'){
                return;
            } 

            let ele = this.genderList.filter((o)=>{ 
              return o['genderDescTh'] == this.gender;
            });
            this.gender_id =  ele[0]['genderId'];
            this.form.controls.genderId.setValue(this.gender_id);

            let json = this.form.value;
            if(json.stuProfileId == ''){
                this.service.httpPost('/api/v1/tbStuProfileSingup',json).then((res:IResponse)=>{
                  if((res.responseCode||500)!=200){
                      Swal.fire(
                        this.translate.instant('message.save_error'),
                        res.responseMsg,
                        'error' 
                      )
                    return;
                  }
                  this.onSaveBeforeStuDocToPDF();
                });
                return;
            }
            this.service.httpPut('/api/v1/tbStuProfileSingup',json).then((res:IResponse)=>{
              this.isProcess = false;
              if((res.responseCode||500)!=200){
                  Swal.fire(
                    this.translate.instant('message.edit_error'),
                    res.responseMsg,
                    'error' 
                  )
                return;
              }
              this.onSaveBeforeStuDocToPDF();
            });
          });
      });
    }else{
      Swal.fire(
        this.translate.instant('message.save_error'),
        this.translate.instant('alert.error_detail1'),
        'error' 
      )
    }
  }
 
  onSaveBeforeStuDocToPDF(){
      let concat_str = this.citizen_id.toString().substring(0,1)+this.citizen_id.toString().substring(2,6)+this.citizen_id.toString().substring(7,12)+this.citizen_id.toString().substring(13,15)+this.citizen_id.toString().substring(16);
      let json = {'citizenId':concat_str};
      this.service.httpGet('/api/v1/0/stu-profile/ddlStuOld',json).then((res)=>{
          this.stuCode1List = res||[];
          for (let data of this.stuCode1List){
              this.formDoc.controls.stuProfileId.setValue(data['stuProfileId']);
          }
          this.formDoc.controls.scCode.setValue("PRAMANDA");
          this.formDoc.controls.dataPathImage.setValue(this.data_path_image);
          this.formDoc.controls.dataPathBirth.setValue(this.data_path_birth);
          this.formDoc.controls.dataPathRegister.setValue(this.data_path_register);
          this.formDoc.controls.dataPathChangeName.setValue(this.data_path_change_name);
          this.formDoc.controls.dataPathTranscript7.setValue(this.data_path_transcript7);
          this.formDoc.controls.dataPathTranscript1.setValue(this.data_path_transcript7);
          this.formDoc.controls.dataPathRecord8.setValue(this.data_path_record8);
          this.formDoc.controls.dataPathTransfer.setValue(this.data_path_transfer);
          this.formDoc.controls.dataPathFatherRegister.setValue(this.data_path_father_register);
          this.formDoc.controls.dataPathFatherCitizenId.setValue(this.data_path_father_citizen_id);
          this.formDoc.controls.dataPathMotherRegister.setValue(this.data_path_mother_register);
          this.formDoc.controls.dataPathMotherCitizenId.setValue(this.data_path_mother_citizen_id);
          this.formDoc.controls.dataPathParentRegister.setValue(this.data_path_parent_register);
          this.formDoc.controls.dataPathParentCitizenId.setValue(this.data_path_parent_citizen_id);
          this.formDoc.controls.dataPathReligion.setValue(this.data_path_religion);

          if(this.data_path_image == undefined && this.data_path_birth == undefined && this.data_path_register == undefined && this.data_path_change_name == undefined
            && this.data_path_transcript7 == undefined && this.data_path_transcript1 == undefined && this.data_path_record8 == undefined && this.data_path_father_citizen_id == undefined
            && this.data_path_father_register == undefined && this.data_path_mother_register == undefined && this.data_path_mother_citizen_id == undefined 
            && this.data_path_parent_register == undefined && this.data_path_parent_citizen_id == undefined && this.data_path_religion == undefined){
            Swal.fire(
              this.translate.instant('alert.save_header'),
              this.translate.instant('message.save')+' '+this.translate.instant('message.wait_report'),
              'success'
            ).then(() => {this.onReportPDF();})
            
          }else{
            let stuDoc = this.formDoc.value;
            /*** insert ***/
            if(this.formDoc.value.stuProfileDocId == ''){
                this.service.httpPost('/api/v1/tbStuProfileDoc',stuDoc).then((res:IResponse)=>{
                    if((res.responseCode||500)!=200){
                        Swal.fire(
                          this.translate.instant('message.save_error_doc'),
                          res.responseMsg,
                          'error' 
                        )
                      return;
                    }
                    this.onReportPDF();
                });
                return;
            }
            /*** update  ***/
            this.service.httpPut('/api/v1/tbStuProfileDoc',stuDoc).then((res:IResponse)=>{
              this.isProcess = false;
              if((res.responseCode||500)!=200){
                  Swal.fire(
                    this.translate.instant('message.edit_error_doc'),
                    res.responseMsg,
                    'error' 
                  )
                return;
              }
              this.onReportPDF();
            });
          }
      });
  }
 
  onReportPDF(){
    let concat_str = this.citizen_id.toString().substring(0,1)+this.citizen_id.toString().substring(2,6)+this.citizen_id.toString().substring(7,12)+this.citizen_id.toString().substring(13,15)+this.citizen_id.toString().substring(16);
    this.formReport.controls.citizenId.setValue(concat_str);
    this.service.httpReportPDF('/api/v1/tbStuProfile/report/pdf','stu-profile',this.formReport.value).then((res)=>{
    }).then(() => {this.onClear();});
  }
 
  /*********** SAVE ข้อมูลโดยตรง ก่อน GET STUPROFILE ID and INSERT TO STUdoc *******************/
  onSave(){
      if(this.citizen_id.length == 17){
        this.service.httpGet('/api/v1/tbStuProfileSingup',null).then((res: IResponse)=>{
            this.refNoList = res.responseData||[];
            let refno_prefix = this.to_year_group.substr(2);
            let db_maxref_no = res.responseData[0]['max_ref_no'];
            let db_year = db_maxref_no.toString().substr(0,4);
            if(db_maxref_no == 0){
                this.request_id = refno_prefix+'0001';
            }else{
                if(refno_prefix != db_year){
                    this.request_id = refno_prefix+'0001';
                }else{
                    let substr_db_maxref_no = db_maxref_no.toString().substr(4);
                    let convert_db_maxref_no = Number(substr_db_maxref_no);
                    let sum_refno = convert_db_maxref_no+1;
                    if(sum_refno.toString().length == 1){
                      this.request_id = refno_prefix+'000'+sum_refno;
                    }else if(sum_refno.toString().length == 2){
                      this.request_id = refno_prefix+'00'+sum_refno;
                    }else if(sum_refno.toString().length == 3){
                      this.request_id = refno_prefix+'0'+sum_refno;
                    }else{
                      this.request_id = refno_prefix+sum_refno;
                    }
                }
            }
            let sc_code = "PRAMANDA"; 
            this.stu_status_id = 1;
            this.stu_status = "สมัครเรียน";

            let concat_str = this.citizen_id.toString().substring(0,1)+this.citizen_id.toString().substring(2,6)+this.citizen_id.toString().substring(7,12)+this.citizen_id.toString().substring(13,15)+this.citizen_id.toString().substring(16);
            this.form.controls.citizenId.setValue(concat_str);

            this.form.controls.requestId.setValue(this.request_id);
            this.form.controls.stuCode.setValue(this.stu_code);
            this.form.controls.scCode.setValue(sc_code);
            this.form.controls.lastClass.setValue(this.last_class);
            this.form.controls.title.setValue(this.title_desc);
            this.form.controls.titleEn.setValue(this.title_en);
            this.form.controls.gender.setValue(this.gender);
            this.form.controls.firstnameTh.setValue(this.firstname_th);
            this.form.controls.lastnameTh.setValue(this.lastname_th);
            this.form.controls.firstnameEn.setValue(this.firstname_en);
            this.form.controls.lastnameEn.setValue(this.lastname_en);
            this.form.controls.familyStatus.setValue(this.family_status);
            this.form.controls.familyStatusDetail.setValue(this.family_status_detail);
            this.form.controls.nickname.setValue(this.nickname);
            this.form.controls.ethnicity.setValue(this.ethnicity);
            this.form.controls.citizenship.setValue(this.citizenship);
            this.form.controls.religion.setValue(this.religion);
            this.form.controls.saint.setValue(this.saint);
            this.form.controls.church.setValue(this.church);
            this.form.controls.dob.setValue(this.dob);
            this.form.controls.bloodType.setValue(this.blood_type);
            this.form.controls.weight.setValue(this.weight);
            this.form.controls.height.setValue(this.height);

            if(this.tel1.length == 12){
              let concat_tel1 = this.tel1.toString().substring(0,2)+this.tel1.toString().substring(3,7)+this.tel1.toString().substring(8);
              this.tel1 = concat_tel1;
            }else{
              this.tel1 = '-';
            }
            this.form.controls.tel1.setValue(this.tel1);

            if(this.tel2.length == 12){
              let concat_tel2 = this.tel2.toString().substring(0,2)+this.tel2.toString().substring(3,7)+this.tel2.toString().substring(8);
              this.tel2 = concat_tel2;
            }else{
              this.tel2 = '-';
            }
            this.form.controls.tel2.setValue(this.tel2);

            this.form.controls.email.setValue(this.email);
            this.form.controls.specialSkill.setValue(this.special_skill);

            if(this.to_class == undefined || this.to_class == null){
              this.to_class = '';
            }
            this.form.controls.toClass.setValue(this.to_class);

            this.form.controls.toYearGroup.setValue(this.to_year_group);
            this.form.controls.exSchool.setValue(this.ex_school);
            this.form.controls.exGpa.setValue(this.ex_gpa);
            this.form.controls.regVillage.setValue(this.reg_village);
            this.form.controls.regHouseNo.setValue(this.reg_house_no);
            this.form.controls.regAddrNo.setValue(this.reg_addr_no);
            this.form.controls.regVillageNo.setValue(this.reg_village_no);
            this.form.controls.regAlley.setValue(this.reg_alley);
            this.form.controls.regRoad.setValue(this.reg_road);
            this.form.controls.regCountry.setValue(this.reg_country);
            this.form.controls.regProvince.setValue(this.reg_province);
            this.form.controls.regDistrict.setValue(this.reg_district);
            this.form.controls.regSubDistrict.setValue(this.reg_sub_district);
            this.form.controls.regPostCode.setValue(this.reg_post_code);
            this.form.controls.curVillage.setValue(this.cur_village);
            this.form.controls.curAddrNo.setValue(this.cur_addr_no);
            this.form.controls.curVillageNo.setValue(this.cur_village_no);
            this.form.controls.curAlley.setValue(this.cur_alley);
            this.form.controls.curRoad.setValue(this.cur_road);
            this.form.controls.curCountry.setValue(this.cur_country);
            this.form.controls.curProvince.setValue(this.cur_province);
            this.form.controls.curDistrict.setValue(this.cur_district);
            this.form.controls.curSubDistrict.setValue(this.cur_sub_district);;
            this.form.controls.curPostCode.setValue(this.cur_post_code);
            this.form.controls.pob.setValue(this.pob);
            this.form.controls.pobProvince.setValue(this.pob_province);
            this.form.controls.pobDistrict.setValue(this.pob_district);
            this.form.controls.pobSubDistrict.setValue(this.pob_sub_district);
            this.form.controls.noOfRelatives.setValue(this.no_of_relatives);
            this.form.controls.relativesStuCode1.setValue(this.relatives_stu_code1);
            this.form.controls.relativesStuCode2.setValue(this.relatives_stu_code2);
            this.form.controls.relativesStuCode3.setValue(this.relatives_stu_code3);
            this.form.controls.medicalInfo.setValue(this.medical_info);
            this.form.controls.isDisabled.setValue(this.is_disabled);
            this.form.controls.disability.setValue(this.disability);

            if(this.father_citizen_id.length == 17){
              let concat_father_citizen = this.father_citizen_id.toString().substring(0,1)+this.father_citizen_id.toString().substring(2,6)+this.father_citizen_id.toString().substring(7,12)+this.father_citizen_id.toString().substring(13,15)+this.father_citizen_id.toString().substring(16);
              this.father_citizen_id = concat_father_citizen;
            }else{
              this.father_citizen_id = '-';
            }
            this.form.controls.fatherCitizenId.setValue(this.father_citizen_id);

            this.form.controls.fatherTitle.setValue(this.father_title);
            this.form.controls.fatherTitleEn.setValue(this.father_title_en);
            this.form.controls.fatherFirstnameTh.setValue(this.father_firstname_th);
            this.form.controls.fatherLastnameTh.setValue(this.father_lastname_th);
            this.form.controls.fatherFirstnameEn.setValue(this.father_firstname_en);
            this.form.controls.fatherLastnameEn.setValue(this.father_lastname_en);
            this.form.controls.fatherEthnicity.setValue(this.father_ethnicity);
            this.form.controls.fatherCitizenship.setValue(this.father_citizenship);
            this.form.controls.fatherReligion.setValue(this.father_religion);
            this.form.controls.fatherSaint.setValue(this.father_saint);
            this.form.controls.fatherEducation.setValue(this.father_education);
            this.form.controls.fatherAnnualIncome.setValue(this.father_annual_income);
            this.form.controls.fatherOccupation.setValue(this.father_occupation);
            this.form.controls.fatherOffice.setValue(this.father_office);
            this.form.controls.fatherVillage.setValue(this.father_village);
            this.form.controls.fatherAddrNo.setValue(this.father_addr_no);
            this.form.controls.fatherVillageNo.setValue(this.father_village_no);
            this.form.controls.fatherAlley.setValue(this.father_alley);
            this.form.controls.fatherRoad.setValue(this.father_road);
            this.form.controls.fatherCountry.setValue(this.father_country);
            this.form.controls.fatherProvince.setValue(this.father_province);
            this.form.controls.fatherDistrict.setValue(this.father_district);
            this.form.controls.fatherSubDistrict.setValue(this.father_sub_district);
            this.form.controls.fatherPostCode.setValue(this.father_post_code);

            if(this.father_tel1.length == 12){
              let concat_father_tel1 = this.father_tel1.toString().substring(0,2)+this.father_tel1.toString().substring(3,7)+this.father_tel1.toString().substring(8);
              this.father_tel1 = concat_father_tel1;
            }else{
              this.father_tel1 = '-';
            }
            this.form.controls.fatherTel1.setValue(this.father_tel1);

            if(this.father_tel2.length == 12){
              let concat_father_tel2 = this.father_tel2.toString().substring(0,2)+this.father_tel2.toString().substring(3,7)+this.father_tel2.toString().substring(8);
              this.father_tel2 = concat_father_tel2;
            }else{
              this.father_tel2 = '-';
            }
            this.form.controls.fatherTel2.setValue(this.father_tel2);

            this.form.controls.fatherEmail.setValue(this.father_email);
            this.form.controls.fatherAlive.setValue(this.father_alive);

            if(this.mother_citizen_id.length == 17){
              let concat_mother_citizen = this.mother_citizen_id.toString().substring(0,1)+this.mother_citizen_id.toString().substring(2,6)+this.mother_citizen_id.toString().substring(7,12)+this.mother_citizen_id.toString().substring(13,15)+this.mother_citizen_id.toString().substring(16);
              this.mother_citizen_id = concat_mother_citizen;
            }else{
              this.mother_citizen_id = '-';
            }
            this.form.controls.motherCitizenId.setValue(this.mother_citizen_id);

            this.form.controls.motherTitle.setValue(this.mother_title);
            this.form.controls.motherTitleEn.setValue(this.mother_title_en);
            this.form.controls.motherFirstnameTh.setValue(this.mother_firstname_th);
            this.form.controls.motherLastnameTh.setValue(this.mother_lastname_th);
            this.form.controls.motherFirstnameEn.setValue(this.mother_firstname_en);
            this.form.controls.motherLastnameEn.setValue(this.mother_lastname_en);
            this.form.controls.motherEthnicity.setValue(this.mother_ethnicity);
            this.form.controls.motherCitizenship.setValue(this.mother_citizenship);
            this.form.controls.motherReligion.setValue(this.mother_religion);
            this.form.controls.motherSaint.setValue(this.mother_saint);
            this.form.controls.motherEducation.setValue(this.mother_education);
            this.form.controls.motherAnnualIncome.setValue(this.mother_annual_income);
            this.form.controls.motherOccupation.setValue(this.mother_occupation);
            this.form.controls.motherOffice.setValue(this.mother_office);
            this.form.controls.motherVillage.setValue(this.mother_village);
            this.form.controls.motherAddrNo.setValue(this.mother_addr_no);
            this.form.controls.motherVillageNo.setValue(this.mother_village_no);
            this.form.controls.motherAlley.setValue(this.mother_alley);
            this.form.controls.motherRoad.setValue(this.mother_road);
            this.form.controls.motherCountry.setValue(this.mother_country);
            this.form.controls.motherProvince.setValue(this.mother_province);
            this.form.controls.motherDistrict.setValue(this.mother_district);
            this.form.controls.motherSubDistrict.setValue(this.mother_sub_district);
            this.form.controls.motherPostCode.setValue(this.mother_post_code);

            if(this.mother_tel1.length == 12){
              let concat_mother_tel1 = this.mother_tel1.toString().substring(0,2)+this.mother_tel1.toString().substring(3,7)+this.mother_tel1.toString().substring(8);
              this.mother_tel1 = concat_mother_tel1;
            }else{
              this.mother_tel1 = '-';
            }
            this.form.controls.motherTel1.setValue(this.mother_tel1);

            if(this.mother_tel2.length == 12){
              let concat_mother_tel2 = this.mother_tel2.toString().substring(0,2)+this.mother_tel2.toString().substring(3,7)+this.mother_tel2.toString().substring(8);
              this.mother_tel2 = concat_mother_tel2;
            }else{
              this.mother_tel2 = '-';
            }
            this.form.controls.motherTel2.setValue(this.mother_tel2);

            this.form.controls.motherEmail.setValue(this.mother_email);
            this.form.controls.motherAlive.setValue(this.mother_alive);

            if(this.parent_citizen_id.length == 17){
              let concat_parent_citizen = this.parent_citizen_id.toString().substring(0,1)+this.parent_citizen_id.toString().substring(2,6)+this.parent_citizen_id.toString().substring(7,12)+this.parent_citizen_id.toString().substring(13,15)+this.parent_citizen_id.toString().substring(16);
              this.parent_citizen_id = concat_parent_citizen;
            }else{
              this.parent_citizen_id = '-';
            }
            this.form.controls.parentCitizenId.setValue(this.parent_citizen_id);

            this.form.controls.parentTitle.setValue(this.parent_title);
            this.form.controls.parentTitleEn.setValue(this.parent_title_en);
            this.form.controls.parentFirstnameTh.setValue(this.parent_firstname_th);
            this.form.controls.parentLastnameTh.setValue(this.parent_lastname_th);
            this.form.controls.parentFirstnameEn.setValue(this.parent_firstname_en);
            this.form.controls.parentLastnameEn.setValue(this.parent_lastname_en);
            this.form.controls.parentEthnicity.setValue(this.parent_ethnicity);
            this.form.controls.parentCitizenship.setValue(this.parent_citizenship);
            this.form.controls.parentReligion.setValue(this.parent_religion);
            this.form.controls.parentSaint.setValue(this.parent_saint);
            this.form.controls.parentEducation.setValue(this.parent_education);
            this.form.controls.parentAnnualIncome.setValue(this.parent_annual_income);
            this.form.controls.parentOccupation.setValue(this.parent_occupation);
            this.form.controls.parentOffice.setValue(this.parent_office);
            this.form.controls.parentStatus.setValue(this.parent_status);
            this.form.controls.parentVillage.setValue(this.parent_village);
            this.form.controls.parentAddrNo.setValue(this.parent_addr_no);
            this.form.controls.parentVillageNo.setValue(this.parent_village_no);
            this.form.controls.parentAlley.setValue(this.parent_alley);
            this.form.controls.parentRoad.setValue(this.parent_road);
            this.form.controls.parentCountry.setValue(this.parent_country);
            this.form.controls.parentProvince.setValue(this.parent_province);
            this.form.controls.parentDistrict.setValue(this.parent_district);
            this.form.controls.parentSubDistrict.setValue(this.parent_sub_district);
            this.form.controls.parentPostCode.setValue(this.parent_post_code);

            if(this.parent_tel1.length == 12){
              let concat_parent_tel1 = this.parent_tel1.toString().substring(0,2)+this.parent_tel1.toString().substring(3,7)+this.parent_tel1.toString().substring(8);
              this.parent_tel1 = concat_parent_tel1;
            }else{
              this.parent_tel1 = '-';
            }
            this.form.controls.parentTel1.setValue(this.parent_tel1);

            if(this.parent_tel2.length == 12){
              let concat_parent_tel2 = this.parent_tel2.toString().substring(0,2)+this.parent_tel2.toString().substring(3,7)+this.parent_tel2.toString().substring(8);
              this.parent_tel2 = concat_parent_tel2;
            }else{
              this.parent_tel2 = '-';
            }
            this.form.controls.parentTel2.setValue(this.parent_tel2);

            this.form.controls.parentEmail.setValue(this.parent_email);
            this.form.controls.stuStatusId.setValue(this.stu_status_id);
            this.form.controls.stuStatus.setValue(this.stu_status);
            this.form.controls.stuImage.setValue(null);
            
            if(this.form.invalid) { 
                this.validateValue = true; 
                Swal.fire( 
                  this.translate.instant('alert.error'),
                  this.translate.instant('alert.validate'),
                  'error' 
                )
              return;
            }
            this.validateValue = false; 
            Swal.fire({
                title: this.translate.instant('alert.save'),
                icon : 'info',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                confirmButtonText: this.translate.instant('psms.DL0008'),
                cancelButtonText: this.translate.instant('psms.DL0009')
            }).then((result) => { 
              if(result.dismiss == 'cancel'){
                  return;
              } 

              let ele = this.genderList.filter((o)=>{ 
                return o['genderDescTh'] == this.gender;
              });
              this.gender_id =  ele[0]['genderId'];
              this.form.controls.genderId.setValue(this.gender_id);

              let json = this.form.value;
              if(json.stuProfileId == ''){
                  this.service.httpPost('/api/v1/tbStuProfileSingup',json).then((res:IResponse)=>{
                    if((res.responseCode||500)!=200){
                        Swal.fire(
                          this.translate.instant('message.save_error'),
                          res.responseMsg,
                          'error' 
                        )
                      return;
                    }
                    this.onGetStuProfileId();
                  });
                  return;
              }
              this.service.httpPut('/api/v1/tbStuProfileSingup',json).then((res:IResponse)=>{
                this.isProcess = false;
                if((res.responseCode||500)!=200){
                    Swal.fire(
                      this.translate.instant('message.edit_error'),
                      res.responseMsg,
                      'error' 
                    )
                  return;
                }
                this.onGetStuProfileId();
              });
            });
        });
      }else{
        Swal.fire(
          this.translate.instant('message.save_error'),
          this.translate.instant('alert.error_detail1'),
          'error' 
        )
      }
  } 
  
  onGetStuProfileId(){
    let concat_str = this.citizen_id.toString().substring(0,1)+this.citizen_id.toString().substring(2,6)+this.citizen_id.toString().substring(7,12)+this.citizen_id.toString().substring(13,15)+this.citizen_id.toString().substring(16);
      let json = {'citizenId':concat_str};
      this.service.httpGet('/api/v1/0/stu-profile/ddlStuOld',json).then((res)=>{
          this.stuCode1List = res||[];
          for (let data of this.stuCode1List){
              this.formDoc.controls.stuProfileId.setValue(data['stuProfileId']);
          }
          this.formDoc.controls.scCode.setValue("PRAMANDA");
          this.formDoc.controls.dataPathImage.setValue(this.data_path_image);
          this.formDoc.controls.dataPathBirth.setValue(this.data_path_birth);
          this.formDoc.controls.dataPathRegister.setValue(this.data_path_register);
          this.formDoc.controls.dataPathChangeName.setValue(this.data_path_change_name);
          this.formDoc.controls.dataPathTranscript7.setValue(this.data_path_transcript7);
          this.formDoc.controls.dataPathTranscript1.setValue(this.data_path_transcript1);
          this.formDoc.controls.dataPathRecord8.setValue(this.data_path_record8);
          this.formDoc.controls.dataPathTransfer.setValue(this.data_path_transfer);
          this.formDoc.controls.dataPathFatherRegister.setValue(this.data_path_father_register);
          this.formDoc.controls.dataPathFatherCitizenId.setValue(this.data_path_father_citizen_id);
          this.formDoc.controls.dataPathMotherRegister.setValue(this.data_path_mother_register);
          this.formDoc.controls.dataPathMotherCitizenId.setValue(this.data_path_mother_citizen_id);
          this.formDoc.controls.dataPathParentRegister.setValue(this.data_path_parent_register);
          this.formDoc.controls.dataPathParentCitizenId.setValue(this.data_path_parent_citizen_id);
          this.formDoc.controls.dataPathReligion.setValue(this.data_path_religion);

          if(this.data_path_image == undefined && this.data_path_birth == undefined && this.data_path_register == undefined && this.data_path_change_name == undefined
            && this.data_path_transcript7 == undefined && this.data_path_transcript1 == undefined && this.data_path_record8 == undefined && this.data_path_father_citizen_id == undefined
            && this.data_path_father_register == undefined && this.data_path_mother_register == undefined && this.data_path_mother_citizen_id == undefined 
            && this.data_path_parent_register == undefined && this.data_path_parent_citizen_id == undefined && this.data_path_religion == undefined){
            Swal.fire(
              this.translate.instant('alert.save_header'),
              this.translate.instant('message.save'),
              'success'
            ).then(() => {this.onClear();})
          }else{
            let stuDoc = this.formDoc.value;
            /*** insert ***/
            if(this.formDoc.value.stuProfileDocId == ''){
                this.service.httpPost('/api/v1/tbStuProfileDoc',stuDoc).then((res:IResponse)=>{
                    if((res.responseCode||500)!=200){
                        Swal.fire(
                          this.translate.instant('message.save_error_doc'),
                          res.responseMsg,
                          'error' 
                        )
                      return;
                    }
                    Swal.fire(
                      this.translate.instant('alert.save_header'),
                      this.translate.instant('message.save'),
                      'success' 
                    ).then(() => {this.onClear();})
                });
                return;
            }
            /*** update  ***/
            this.service.httpPut('/api/v1/tbStuProfileDoc',stuDoc).then((res:IResponse)=>{
              this.isProcess = false;
              if((res.responseCode||500)!=200){
                  Swal.fire(
                    this.translate.instant('message.edit_error_doc'),
                    res.responseMsg,
                    'error' 
                  )
                return;
              }Swal.fire(
                this.translate.instant('alert.save_header'),
                this.translate.instant('message.save'),
                'success'
              ).then(() => {this.onClear();})
            });
          }

      });
  }

} 