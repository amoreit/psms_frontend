import { Component, OnInit, ViewChild } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { ApiService, IResponse } from 'src/app/shared/service/api.service';
import Swal from 'sweetalert2/dist/sweetalert2.js';
import 'sweetalert2/src/sweetalert2.scss';
import * as constants from '../../../../constants';
import { UtilService } from 'src/app/_util/util.service';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-stu005-form',
  templateUrl: './stu005-form.component.html',
  styleUrls: ['./stu005-form.component.scss']
})
export class Stu005FormComponent implements OnInit {

  isProcess: boolean = false;
  form = this.fb.group({ 
    'stuProfileId': ['']
    , 'citizenId': ['']
    , 'title': ['']
    , 'gender': ['']
    , 'genderId': ['']
    , 'firstnameTh': ['']
    , 'lastnameTh': ['']
    , 'titleEn': ['']
    , 'firstnameEn': ['']
    , 'lastnameEn': ['']
    , 'familyStatus': ['']
    , 'nickname': ['']
    , 'ethnicity': ['']
    , 'citizenship': ['']
    , 'religion': ['']
    , 'saint': ['']
    , 'church': ['']
    , 'dob': ['']
    , 'bloodType': ['']
    , 'weight': ['']
    , 'height': ['']
    , 'tel1': ['']
    , 'tel2': ['']
    , 'email': ['']
    , 'specialSkill': ['']
    , 'toClass': ['']
    , 'toYearGroup': ['']
    , 'exSchool': ['']
    , 'lastClass': ['']
    , 'exGpa': ['']
    , 'regHouseNo': ['']
    , 'regAlley': ['']
    , 'regVillage': ['']
    , 'regAddrNo': ['']
    , 'regVillageNo': ['']
    , 'regRoad': ['']
    , 'regCountry': ['']
    , 'regProvince': ['']
    , 'regDistrict': ['']
    , 'regSubDistrict': ['']
    , 'regPostCode': ['']
    , 'curAlley': ['']
    , 'curVillage': ['']
    , 'curAddrNo': ['']
    , 'curVillageNo': ['']
    , 'curRoad': ['']
    , 'curCountry': ['']
    , 'curProvince': ['']
    , 'curDistrict': ['']
    , 'curSubDistrict': ['']
    , 'curPostCode': ['']
    , 'pob': ['']
    , 'pobProvince': ['']
    , 'pobDistrict': ['']
    , 'pobSubDistrict': ['']
    , 'noOfRelatives': ['']
    , 'relativesStuCode1': ['']
    , 'relativesStuCode2': ['']
    , 'relativesStuCode3': ['']
    , 'medicalInfo': ['']
    , 'isDisabled': ['']
    , 'disability': ['']
    , 'fatherCitizenId': ['']
    , 'fatherTitle': ['']
    , 'fatherTitleEn': ['']
    , 'fatherFirstnameTh': ['']
    , 'fatherLastnameTh': ['']
    , 'fatherFirstnameEn': ['']
    , 'fatherLastnameEn': ['']
    , 'fatherEthnicity': ['']
    , 'fatherCitizenship': ['']
    , 'fatherReligion': ['']
    , 'fatherSaint': ['']
    , 'fatherEducation': ['']
    , 'fatherOccupation': ['']
    , 'fatherOffice': ['']
    , 'fatherAnnualIncome': ['']
    , 'fatherVillage': ['']
    , 'fatherAddrNo': ['']
    , 'fatherVillageNo': ['']
    , 'fatherRoad': ['']
    , 'fatherAlley': ['']
    , 'fatherCountry': ['']
    , 'fatherProvince': ['']
    , 'fatherDistrict': ['']
    , 'fatherSubDistrict': ['']
    , 'fatherPostCode': ['']
    , 'fatherTel1': ['']
    , 'fatherTel2': ['']
    , 'fatherEmail': ['']
    , 'fatherAlive': ['']
    , 'motherCitizenId': ['']
    , 'motherTitle': ['']
    , 'motherTitleEn': ['']
    , 'motherFirstnameTh': ['']
    , 'motherLastnameTh': ['']
    , 'motherFirstnameEn': ['']
    , 'motherLastnameEn': ['']
    , 'motherEthnicity': ['']
    , 'motherCitizenship': ['']
    , 'motherReligion': ['']
    , 'motherSaint': ['']
    , 'motherEducation': ['']
    , 'motherOccupation': ['']
    , 'motherOffice': ['']
    , 'motherAnnualIncome': ['']
    , 'motherVillage': ['']
    , 'motherAddrNo': ['']
    , 'motherVillageNo': ['']
    , 'motherRoad': ['']
    , 'motherAlley': ['']
    , 'motherCountry': ['']
    , 'motherProvince': ['']
    , 'motherDistrict': ['']
    , 'motherSubDistrict': ['']
    , 'motherPostCode': ['']
    , 'motherTel1': ['']
    , 'motherTel2': ['']
    , 'motherEmail': ['']
    , 'motherAlive': ['']
    , 'parentCitizenId': ['']
    , 'parentTitle': ['']
    , 'parentTitleEn': ['']
    , 'parentFirstnameTh': ['']
    , 'parentLastnameTh': ['']
    , 'parentFirstnameEn': ['']
    , 'parentLastnameEn': ['']
    , 'parentEthnicity': ['']
    , 'parentCitizenship': ['']
    , 'parentReligion': ['']
    , 'parentSaint': ['']
    , 'parentEducation': ['']
    , 'parentOccupation': ['']
    , 'parentOffice': ['']
    , 'parentAnnualIncome': ['']
    , 'parentStatus': ['']
    , 'parentVillage': ['']
    , 'parentAddrNo': ['']
    , 'parentVillageNo': ['']
    , 'parentRoad': ['']
    , 'parentAlley': ['']
    , 'parentCountry': ['']
    , 'parentProvince': ['']
    , 'parentDistrict': ['']
    , 'parentSubDistrict': ['']
    , 'parentPostCode': ['']
    , 'parentTel1': ['']
    , 'parentTel2': ['']
    , 'parentEmail': ['']
    , 'scCode': ['']

    , 'stuProfileDocId': ['']
    , 'stuCode': ['']
    , 'dataPathImage': ['']
    , 'dataPathBirth': ['']
    , 'dataPathRegister': ['']
    , 'dataPathReligion': ['']
    , 'dataPathChangeName': ['']
    , 'dataPathTranscript7': ['']
    , 'dataPathTranscript1': ['']
    , 'dataPathRecord8': ['']
    , 'dataPathTransfer': ['']
    , 'dataPathFatherRegister': ['']
    , 'dataPathFatherCitizenId': ['']
    , 'dataPathMotherRegister': ['']
    , 'dataPathMotherCitizenId': ['']
    , 'dataPathParentRegister': ['']
    , 'dataPathParentCitizenId': ['']
  });

  id = '';
  isLinear = false;
  firstFormGroup: FormGroup;
  secondFormGroup: FormGroup;
  thirdFormGroup: FormGroup;
  fourthFormGroup: FormGroup;
  fifthFormGroup: FormGroup;

  show_fullname1: String;
  show_class_room1: String;
  show_fullname2: String;
  show_class_room2: String;
  show_fullname3: String;
  show_class_room3: String;
  day_bob: any;
  day_date_now: any
  count_dob: any

  titleList: [];
  titleEnList: [];
  titleFatherList: [];
  titleMotherList: [];
  titleParentList: [];
  religionList: [];
  bloodTypeList: [];
  classList: [];
  yearGroupList: [];
  genderList: [];
  familystatusList: [];

  provinceList: [];
  districtList: [];
  subdistrictList: [];
  educationList: [];
  citizenshipandcountryList: [];

  regdistrictList: [];
  regsubdistrictList: [];

  curdistrictList: [];
  cursubdistrictList: [];

  pobdistrictList: [];
  pobsubdistrictList: [];

  fatherdistrictList: [];
  fathersubdistrictList: [];

  motherdistrictList: [];
  mothersubdistrictList: [];

  parentdistrictList: [];
  parentsubdistrictList: [];

  stuprofile1List: [];
  stuprofile2List: [];
  stuprofile3List: [];

  regpostcodeList: [];
  curpostcodeList: [];
  fatherpostcodeList: [];
  motherpostcodeList: [];
  parentpostcodeList: [];

  constructor(
    private fb: FormBuilder,
    private router: Router,
    private service: ApiService,
    private ar: ActivatedRoute,
    public _util: UtilService,
    private translate: TranslateService 
  ) { }

  ngOnInit() {
    this.id = this.ar.snapshot.queryParamMap.get('id') || '';
    this.loadData()
    this.loadDataDoc()
    this.ddlTitle()
    this.ddlTitleFather()
    this.ddlTitleMother()
    this.ddlTitleParent()
    this.ddlTitleEn()
    this.ddlGender()
    this.ddlBloodType()
    this.ddlFamilyStatus()
    this.ddlReligion()
    this.ddlCitizenShipAndCountry()
    this.ddlEducation()
    this.ddlClass()
    this.ddlYearGroup()
  }

  loadData() {
    this.isProcess = true;
    this.service.httpGet('/api/v1/stu-profile/' + this.id, null).then((res: IResponse) => {
      this.isProcess = false;
      let data = res.responseData;

      this.form.controls.stuProfileId.setValue(data.stuProfileId || '');
      this.form.controls.stuCode.setValue(data.stuCode || null);
      this.form.controls.citizenId.setValue(data.citizenId || '');
      this.form.controls.firstnameTh.setValue(data.firstnameTh || '');
      this.form.controls.lastnameTh.setValue(data.lastnameTh || '');
      this.form.controls.firstnameEn.setValue(data.firstnameEn || '');
      this.form.controls.lastnameEn.setValue(data.lastnameEn || '');
      this.form.controls.title.setValue(data.title || '');
      this.form.controls.titleEn.setValue(data.titleEn || '');
      this.form.controls.nickname.setValue(data.nickname || '');
      this.form.controls.gender.setValue(data.gender || '');
      this.form.controls.genderId.setValue(data.genderId || '');
      this.form.controls.dob.setValue(this._util.formatDateSQLToDate(data.dob || ''));
      this.countDob(this.form.value.dob)
      this.form.controls.ethnicity.setValue(data.ethnicity || '');
      this.form.controls.citizenship.setValue(data.citizenship || '');
      this.form.controls.religion.setValue(data.religion || '');
      this.form.controls.saint.setValue(data.saint || '');
      this.form.controls.church.setValue(data.church || '');
      this.form.controls.bloodType.setValue(data.bloodType || '');
      this.form.controls.weight.setValue(data.weight || '');
      this.form.controls.height.setValue(data.height || '');
      this.form.controls.tel1.setValue(data.tel1 || '');
      this.form.controls.tel2.setValue(data.tel2 || '');
      this.form.controls.email.setValue(data.email || '');
      this.form.controls.specialSkill.setValue(data.specialSkill || '')
      this.form.controls.toClass.setValue(data.toClass || '');
      this.form.controls.toYearGroup.setValue(data.toYearGroup || '');
      this.form.controls.exSchool.setValue(data.exSchool || '');
      this.form.controls.lastClass.setValue(data.lastClass || '');
      this.form.controls.exGpa.setValue(this._util.formatFloat(data.exGpa, 2));
      this.form.controls.familyStatus.setValue(data.familyStatus || '');

      this.form.controls.regCountry.setValue(data.regCountry || '');
      this.form.controls.regProvince.setValue(data.regProvince || '');
      this.form.controls.regDistrict.setValue(data.regDistrict || '');
      this.form.controls.regSubDistrict.setValue(data.regSubDistrict || '');
      this.form.controls.regPostCode.setValue(data.regPostCode || '');
      this.form.controls.regVillage.setValue(data.regVillage || '');
      this.form.controls.regAddrNo.setValue(data.regAddrNo || '');
      this.form.controls.regVillageNo.setValue(data.regVillageNo || '');
      this.form.controls.regRoad.setValue(data.regRoad || '');
      this.form.controls.regHouseNo.setValue(data.regHouseNo || '');
      this.form.controls.regAlley.setValue(data.regAlley || '');

      this.form.controls.curCountry.setValue(data.curCountry || '');
      this.form.controls.curProvince.setValue(data.curProvince || '');
      this.form.controls.curDistrict.setValue(data.curDistrict || '');
      this.form.controls.curSubDistrict.setValue(data.curSubDistrict || '');
      this.form.controls.curPostCode.setValue(data.curPostCode || '');
      this.form.controls.curVillage.setValue(data.curVillage || '');
      this.form.controls.curAddrNo.setValue(data.curAddrNo || '');
      this.form.controls.curVillageNo.setValue(data.curVillageNo || '');
      this.form.controls.curRoad.setValue(data.curRoad || '');
      this.form.controls.curAlley.setValue(data.curAlley || '');

      this.form.controls.pob.setValue(data.pob || '');
      this.form.controls.pobProvince.setValue(data.pobProvince || '');
      this.form.controls.pobDistrict.setValue(data.pobDistrict || '');
      this.form.controls.pobSubDistrict.setValue(data.pobSubDistrict || '');
      this.form.controls.noOfRelatives.setValue(data.noOfRelatives || '');
      this.form.controls.relativesStuCode1.setValue(data.relativesStuCode1 || '');
      this.form.controls.relativesStuCode2.setValue(data.relativesStuCode2 || '');
      this.form.controls.relativesStuCode3.setValue(data.relativesStuCode3 || '');

      this.form.controls.medicalInfo.setValue(data.medicalInfo || '');
      this.form.controls.isDisabled.setValue(data.isDisabled || '');
      this.form.controls.disability.setValue(data.disability || '');

      this.form.controls.fatherTitle.setValue(data.fatherTitle || '')
      this.form.controls.fatherTitleEn.setValue(data.fatherTitleEn || '')
      this.form.controls.fatherFirstnameTh.setValue(data.fatherFirstnameTh || '')
      this.form.controls.fatherLastnameTh.setValue(data.fatherLastnameTh || '')
      this.form.controls.fatherFirstnameEn.setValue(data.fatherFirstnameEn || '')
      this.form.controls.fatherLastnameEn.setValue(data.fatherLastnameEn || '')
      this.form.controls.fatherReligion.setValue(data.fatherReligion || '')
      this.form.controls.fatherSaint.setValue(data.fatherSaint || '')
      this.form.controls.fatherEthnicity.setValue(data.fatherEthnicity || '')
      this.form.controls.fatherCitizenship.setValue(data.fatherCitizenship || '')
      this.form.controls.fatherCitizenId.setValue(data.fatherCitizenId || '')
      this.form.controls.fatherOccupation.setValue(data.fatherOccupation || '')
      this.form.controls.fatherOffice.setValue(data.fatherOffice || '')
      this.form.controls.fatherAnnualIncome.setValue(data.fatherAnnualIncome || '');
      this.form.controls.fatherEducation.setValue(data.fatherEducation || '')
      this.form.controls.fatherAddrNo.setValue(data.fatherAddrNo || '')
      this.form.controls.fatherVillage.setValue(data.fatherVillage || '')
      this.form.controls.fatherVillageNo.setValue(data.fatherVillageNo || '')
      this.form.controls.fatherRoad.setValue(data.fatherRoad || '')
      this.form.controls.fatherAlley.setValue(data.fatherAlley || '')
      this.form.controls.fatherSubDistrict.setValue(data.fatherSubDistrict || '')
      this.form.controls.fatherDistrict.setValue(data.fatherDistrict || '')
      this.form.controls.fatherProvince.setValue(data.fatherProvince || '')
      this.form.controls.fatherCountry.setValue(data.fatherCountry || '')
      this.form.controls.fatherPostCode.setValue(data.fatherPostCode || '')
      this.form.controls.fatherEmail.setValue(data.fatherEmail || '')
      this.form.controls.fatherTel1.setValue(data.fatherTel1 || '')
      this.form.controls.fatherTel2.setValue(data.fatherTel2 || '')
      this.form.controls.fatherAlive.setValue(data.fatherAlive || '')

      this.form.controls.motherTitle.setValue(data.motherTitle || '')
      this.form.controls.motherTitleEn.setValue(data.motherTitleEn || '')
      this.form.controls.motherFirstnameTh.setValue(data.motherFirstnameTh || '')
      this.form.controls.motherLastnameTh.setValue(data.motherLastnameTh || '')
      this.form.controls.motherFirstnameEn.setValue(data.motherFirstnameEn || '')
      this.form.controls.motherLastnameEn.setValue(data.motherLastnameEn || '')
      this.form.controls.motherReligion.setValue(data.motherReligion || '')
      this.form.controls.motherSaint.setValue(data.motherSaint || '')
      this.form.controls.motherEthnicity.setValue(data.motherEthnicity || '')
      this.form.controls.motherCitizenship.setValue(data.motherCitizenship || '')
      this.form.controls.motherCitizenId.setValue(data.motherCitizenId || '')
      this.form.controls.motherOccupation.setValue(data.motherOccupation || '')
      this.form.controls.motherOffice.setValue(data.motherOffice || '')
      this.form.controls.motherAnnualIncome.setValue(data.motherAnnualIncome || '');
      this.form.controls.motherEducation.setValue(data.motherEducation || '')
      this.form.controls.motherAddrNo.setValue(data.motherAddrNo || '')
      this.form.controls.motherVillage.setValue(data.motherVillage || '')
      this.form.controls.motherVillageNo.setValue(data.motherVillageNo || '')
      this.form.controls.motherRoad.setValue(data.motherRoad || '')
      this.form.controls.motherAlley.setValue(data.motherAlley || '')
      this.form.controls.motherSubDistrict.setValue(data.motherSubDistrict || '')
      this.form.controls.motherDistrict.setValue(data.motherDistrict || '')
      this.form.controls.motherProvince.setValue(data.motherProvince || '')
      this.form.controls.motherCountry.setValue(data.motherCountry || '')
      this.form.controls.motherPostCode.setValue(data.motherPostCode || '')
      this.form.controls.motherEmail.setValue(data.motherEmail || '')
      this.form.controls.motherTel1.setValue(data.motherTel1 || '')
      this.form.controls.motherTel2.setValue(data.motherTel2 || '')
      this.form.controls.motherAlive.setValue(data.motherAlive || '')

      this.form.controls.parentTitle.setValue(data.parentTitle || '')
      this.form.controls.parentTitleEn.setValue(data.parentTitleEn || '')
      this.form.controls.parentFirstnameTh.setValue(data.parentFirstnameTh || '')
      this.form.controls.parentLastnameTh.setValue(data.parentLastnameTh || '')
      this.form.controls.parentFirstnameEn.setValue(data.parentFirstnameEn || '')
      this.form.controls.parentLastnameEn.setValue(data.parentLastnameEn || '')
      this.form.controls.parentReligion.setValue(data.parentReligion || '')
      this.form.controls.parentSaint.setValue(data.parentSaint || '')
      this.form.controls.parentEthnicity.setValue(data.parentEthnicity || '')
      this.form.controls.parentCitizenship.setValue(data.parentCitizenship || '')
      this.form.controls.parentCitizenId.setValue(data.parentCitizenId || '')
      this.form.controls.parentOccupation.setValue(data.parentOccupation || '')
      this.form.controls.parentOffice.setValue(data.parentOffice || '')
      this.form.controls.parentAnnualIncome.setValue(data.parentAnnualIncome || '');
      this.form.controls.parentEducation.setValue(data.parentEducation || '')
      this.form.controls.parentAddrNo.setValue(data.parentAddrNo || '')
      this.form.controls.parentVillage.setValue(data.parentVillage || '')
      this.form.controls.parentVillageNo.setValue(data.parentVillageNo || '')
      this.form.controls.parentRoad.setValue(data.parentRoad || '')
      this.form.controls.parentAlley.setValue(data.parentAlley || '')
      this.form.controls.parentSubDistrict.setValue(data.parentSubDistrict || '')
      this.form.controls.parentDistrict.setValue(data.parentDistrict || '')
      this.form.controls.parentProvince.setValue(data.parentProvince || '')
      this.form.controls.parentCountry.setValue(data.parentCountry || '')
      this.form.controls.parentPostCode.setValue(data.parentPostCode || '')
      this.form.controls.parentEmail.setValue(data.parentEmail || '')
      this.form.controls.parentTel1.setValue(data.parentTel1 || '')
      this.form.controls.parentTel2.setValue(data.parentTel2 || '')
      this.form.controls.parentStatus.setValue(data.parentStatus || '');

      this.ddlProvince()

      this.ddlRegDistrict()
      this.ddlRegSubDistrict()

      this.ddlCurDistrict()
      this.ddlCurSubDistrict()

      this.ddlPobDistrict()
      this.ddlPobSubDistrict()

      this.ddlFatherDistrict()
      this.ddlFatherSubDistrict()

      this.ddlMotherDistrict()
      this.ddlMotherSubDistrict()

      this.ddlParentDistrict()
      this.ddlParentSubDistrict()

      this.ddlStuProfile1();
      this.ddlStuProfile2();
      this.ddlStuProfile3();

      this.firstFormGroup = this.fb.group({
        firstCtrl: ['', Validators.required]
      });
      this.secondFormGroup = this.fb.group({
        secondCtrl: ['', Validators.required]
      });
      this.thirdFormGroup = this.fb.group({
        thirdCtrl: ['', Validators.required]
      });
      this.fourthFormGroup = this.fb.group({
        fourthCtrl: ['', Validators.required]
      });
      this.fifthFormGroup = this.fb.group({
        fifthCtrl: ['', Validators.required]
      });
    })
  }

  loadDataDoc() {
    this.isProcess = true;
    this.service.httpGet('/api/v1/stu-profile-doc/' + this.id, null).then((res: IResponse) => {
      this.isProcess = false;
      let data = res.responseData;
      if (data == null) {
        return;
      } else {
        this.form.controls.stuProfileDocId.setValue(data.stuProfileDocId || '');
        this.form.controls.dataPathBirth.setValue(data.dataPathBirth || '');
        this.form.controls.dataPathRegister.setValue(data.dataPathRegister || '');
        this.form.controls.dataPathReligion.setValue(data.dataPathReligion || '');
        this.form.controls.dataPathChangeName.setValue(data.dataPathChangeName || '');
        this.form.controls.dataPathTranscript7.setValue(data.dataPathTranscript7 || '');
        this.form.controls.dataPathTranscript1.setValue(data.dataPathTranscript1 || '');
        this.form.controls.dataPathRecord8.setValue(data.dataPathRecord8 || '');
        this.form.controls.dataPathTransfer.setValue(data.dataPathTransfer || '');
        this.form.controls.dataPathFatherRegister.setValue(data.dataPathFatherRegister || '');
        this.form.controls.dataPathFatherCitizenId.setValue(data.dataPathFatherCitizenId || '');
        this.form.controls.dataPathMotherRegister.setValue(data.dataPathMotherRegister || '');
        this.form.controls.dataPathMotherCitizenId.setValue(data.dataPathMotherCitizenId || '');
        this.form.controls.dataPathParentRegister.setValue(data.dataPathParentRegister || '');
        this.form.controls.dataPathParentCitizenId.setValue(data.dataPathParentCitizenId || '');
      }
    });
  }

  onUpload(event, txt) {
    const data = event.target.files[0];
    const name_data = data.name;
    const uploadData = new FormData();
    uploadData.append('file', data, name_data);
    this.service.httpPost("/api/v1/attachment/uploaddoc/" + this.form.value.citizenId, uploadData).then((res: IResponse) => {
      if (res.responseCode != 200) {
        Swal.fire(
          this.translate.instant('alert.error_doc'),
          res.responseMsg,
          'error'
        )
        return;
      }
      if (txt == '1') {
        this.form.get('dataPathBirth').setValue(res.responseData.attachmentCode + "/" + res.responseData.attachmentName);
      }
      if (txt == '2') {
        this.form.get('dataPathRegister').setValue(res.responseData.attachmentCode + "/" + res.responseData.attachmentName);
      }
      if (txt == '3') {
        this.form.get('dataPathReligion').setValue(res.responseData.attachmentCode + "/" + res.responseData.attachmentName);
      }
      if (txt == '4') {
        this.form.get('dataPathChangeName').setValue(res.responseData.attachmentCode + "/" + res.responseData.attachmentName);
      }
      if (txt == '5') {
        this.form.get('dataPathTranscript7').setValue(res.responseData.attachmentCode + "/" + res.responseData.attachmentName);
      }
      if (txt == '6') {
        this.form.get('dataPathTranscript1').setValue(res.responseData.attachmentCode + "/" + res.responseData.attachmentName);
      }
      if (txt == '7') {
        this.form.get('dataPathRecord8').setValue(res.responseData.attachmentCode + "/" + res.responseData.attachmentName);
      }
      if (txt == '8') {
        this.form.get('dataPathTransfer').setValue(res.responseData.attachmentCode + "/" + res.responseData.attachmentName);
      }
      if (txt == '9') {
        this.form.get('dataPathFatherRegister').setValue(res.responseData.attachmentCode + "/" + res.responseData.attachmentName);
      }
      if (txt == '10') {
        this.form.get('dataPathFatherCitizenId').setValue(res.responseData.attachmentCode + "/" + res.responseData.attachmentName);
      }
      if (txt == '11') {
        this.form.get('dataPathMotherRegister').setValue(res.responseData.attachmentCode + "/" + res.responseData.attachmentName);
      }
      if (txt == '12') {
        this.form.get('dataPathMotherCitizenId').setValue(res.responseData.attachmentCode + "/" + res.responseData.attachmentName);
      }
      if (txt == '13') {
        this.form.get('dataPathParentRegister').setValue(res.responseData.attachmentCode + "/" + res.responseData.attachmentName);
      }
      if (txt == '14') {
        this.form.get('dataPathParentCitizenId').setValue(res.responseData.attachmentCode + "/" + res.responseData.attachmentName);
      }

    });
  }

  onSave() {
    if (this.form.invalid) {
      Swal.fire(
        this.translate.instant('alert.error'),
        this.translate.instant('alert.validate'),
        'error'
      )
      return;
    }

    Swal.fire({
      title: this.translate.instant('psms.DL0001'),
      icon: 'info',
      showCancelButton: true,
      confirmButtonColor: '#2085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: this.translate.instant('psms.DL0008'),
      cancelButtonText: this.translate.instant('psms.DL0009')
    }).then((result) => {
      if (result.dismiss == 'cancel') {
        return;
      }
      this.form.value.scCode = 'PRAMANDA';
      let json = this.form.value;
      if (json.stuProfileDocId == null || json.stuProfileDocId == undefined || json.stuProfileDocId == '') {
        this.service.httpPut('/api/v1/stu-profile/updateandinsert', json).then((res: IResponse) => {
          if ((res.responseCode || 500) != 200) {
            Swal.fire(
              this.translate.instant('message.edit_error'),
              res.responseMsg,
              'error'
            )
            return;
          }
          Swal.fire(
            this.translate.instant('psms.DL0006'),
            this.translate.instant('message.edit'),
            'success'
          ).then(() => {
            this.onBackFirst();
          })

        })
        return;
      }
      this.service.httpPut('/api/v1/stu-profile', json).then((res: IResponse) => {
        if ((res.responseCode || 500) != 200) {
          Swal.fire(
            this.translate.instant('message.edit_error'),
            res.responseMsg,
            'error'
          )
          return;
        }
        Swal.fire(
          this.translate.instant('psms.DL0006'),
          this.translate.instant('message.edit'),
          'success'
        ).then(() => {
          this.onBackFirst();
        })
      });
    });
  }

  onCancel() {
    this.loadData();
  }


  onCheckReligion() {
    let ele = this.religionList.filter((o) => {
      return o['religionNameTh'] == this.form.value.religion;
    });
    this.form.value.religion = ele[0]['religionNameTh']
    if (this.form.value.religion != 'คาทอลิก') {
      this.form.value.saint = '';
      this.form.value.church = '';
    }
  }

  onCheckReligionFather() {
    let ele = this.religionList.filter((o) => {
      return o['religionNameTh'] == this.form.value.fatherReligion;
    });
    this.form.value.fatherReligion = ele[0]['religionNameTh']
    if (this.form.value.fatherReligion != 'คาทอลิก') {
      this.form.value.fatherSaint = '';
    }
  }

  onCheckReligionMother() {
    let ele = this.religionList.filter((o) => {
      return o['religionNameTh'] == this.form.value.motherReligion;
    });
    this.form.value.motherReligion = ele[0]['religionNameTh']
    if (this.form.value.motherReligion != 'คาทอลิก') {
      this.form.value.motherSaint = '';
    }
  }

  onCheckReligionParent() {
    let ele = this.religionList.filter((o) => {
      return o['religionNameTh'] == this.form.value.parentReligion;
    });
    this.form.value.parentReligion = ele[0]['religionNameTh']
    if (this.form.value.parentReligion != 'คาทอลิก') {
      this.form.value.parentSaint = '';
    }
  }

  onCheck(e) {
    this.form.value.isDisabled = e.target.checked;
    if (this.form.value.isDisabled == false) {
      this.form.value.disability = ''
    }
  }

  ddlStuProfile1() {
    this.service.httpGet('/api/v1/0/stu-profile/ddl', { 'stuCode': this.form.value.relativesStuCode1 }).then((res) => {
      this.stuprofile1List = res || [];
      for (let item of this.stuprofile1List) {
        this.show_fullname1 = item['fullname'];
        this.show_class_room1 = item['classRoom'];
      }
    });
  }

  clearData1() {
    this.show_fullname1 = '';
    this.show_class_room1 = '';
  }

  ddlStuProfile2() {
    this.service.httpGet('/api/v1/0/stu-profile/ddl', { 'stuCode': this.form.value.relativesStuCode2 }).then((res) => {
      this.stuprofile2List = res || [];
      for (let item of this.stuprofile2List) {
        this.show_fullname2 = item['fullname'];
        this.show_class_room2 = item['classRoom'];
      }
    });
  }

  clearData2() {
    this.show_fullname2 = '';
    this.show_class_room2 = '';
  }

  ddlStuProfile3() {
    this.service.httpGet('/api/v1/0/stu-profile/ddl', { 'stuCode': this.form.value.relativesStuCode3 }).then((res) => {
      this.stuprofile3List = res || [];
      for (let item of this.stuprofile3List) {
        this.show_fullname3 = item['fullname'];
        this.show_class_room3 = item['classRoom'];
      }
    });
  }

  clearData3() {
    this.show_fullname3 = '';
    this.show_class_room3 = '';
  }

  ddlTitle() {
    this.service.httpGet('/api/v1/0/title/ddl', { 'langCode': 'th' }).then((res) => {
      this.titleList = res || [];
    });
  }

  onCheckTitle() {
    let ele = this.titleList.filter((o) => {
      return o['name'] == this.form.value.title;
    })
    this.form.controls.genderId.setValue(ele[0]['gender'])

    let _ele = this.genderList.filter((o) => {
      return o['genderId'] == this.form.value.genderId;
    });
    this.form.controls.gender.setValue(_ele[0]['genderDescTh'])

    if (this.form.value.title == 'ด.ช.') {
      this.form.controls.titleEn.setValue('M.')
    }
    if (this.form.value.title == 'นาย') {
      this.form.controls.titleEn.setValue('MR.')
    }
    if (this.form.value.title == 'ด.ญ.' || this.form.value.title == 'น.ส.') {
      this.form.controls.titleEn.setValue('MS.')
    }
  }

  ddlTitleFather() {
    this.service.httpGet('/api/v1/0/title/ddlfiltergender', { 'genderId': '1', 'titleId': '1' }).then((res) => {
      this.titleFatherList = res || [];
    });
  }

  onCheckTitleFather() {
    if (this.form.value.fatherTitle == 'นาย') {
      this.form.controls.fatherTitleEn.setValue('MR.')
    } else {
      this.form.controls.fatherTitleEn.setValue('')
    }
  }

  ddlTitleMother() {
    this.service.httpGet('/api/v1/0/title/ddlfiltergender', { 'genderId': '2', 'titleId': '2' }).then((res) => {
      this.titleMotherList = res || [];

    });
  }

  onCheckTitleMother() {
    if (this.form.value.motherTitle == 'น.ส.') {
      this.form.controls.motherTitleEn.setValue('MS.')
    }
    else if (this.form.value.motherTitle == 'นาง') {
      this.form.controls.motherTitleEn.setValue('MRS.')
    }
    else {
      this.form.controls.motherTitleEn.setValue('')
    }
  }

  ddlTitleParent() {
    this.service.httpGet('/api/v1/0/title/ddlparent', null).then((res) => {
      this.titleParentList = res || [];
    });
  }

  onCheckTitleParent() {
    if (this.form.value.parentTitle == 'นาย') {
      this.form.controls.parentTitleEn.setValue('MR.')
    }
    else if (this.form.value.parentTitle == 'น.ส.') {
      this.form.controls.parentTitleEn.setValue('MS.')
    }
    else if (this.form.value.parentTitle == 'นาง') {
      this.form.controls.parentTitleEn.setValue('MRS.')
    }
    else {
      this.form.controls.parentTitleEn.setValue('')
    }
  }

  ddlTitleEn() {
    this.service.httpGet('/api/v1/0/title/ddl', { 'langCode': 'en' }).then((res) => {
      this.titleEnList = res || [];
    });
  }

  ddlFamilyStatus() {
    this.service.httpGet('/api/v1/0/family-status/ddl', null).then((res) => {
      this.familystatusList = res || [];
    });
  }

  ddlReligion() {
    this.service.httpGet('/api/v1/0/religion/ddl', null).then((res) => {
      this.religionList = res || [];
    });
  }

  ddlBloodType() {
    this.service.httpGet('/api/v1/0/blood-type/ddl', null).then((res) => {
      this.bloodTypeList = res || [];
    });
  }

  ddlCitizenShipAndCountry() {
    this.service.httpGet('/api/v1/0/country/ddl', null).then((res) => {
      this.citizenshipandcountryList = res || [];
    });
  }

  ddlClass() {
    this.service.httpGet('/api/v1/0/class/ddl', null).then((res) => {
      this.classList = res || [];
    });
  }

  ddlYearGroup() {
    this.service.httpGet('/api/v1/0/year-group/ddl', null).then((res) => {
      this.yearGroupList = res || [];
    });
  }

  ddlGender() {
    this.service.httpGet('/api/v1/0/gender/ddl', null).then((res) => {
      this.genderList = res || [];
    });
  }

  ddlEducation() {
    this.service.httpGet('/api/v1/0/education/ddl', { 'langCode': 'th' }).then((res) => {
      this.educationList = res || [];
    });
  }

  ddlProvince() {
    this.service.httpGet('/api/v1/0/province/ddl', null).then((res) => {
      this.provinceList = res || [];
    });
  }

  ddlRegDistrict() {
    this.service.httpGet('/api/v1/0/district/ddlByCondition', { 'province': this.form.value.regProvince }).then((res) => {
      this.regdistrictList = res || [];
    });
  }

  ddlRegSubDistrict() {
    this.service.httpGet('/api/v1/0/sub-district/ddlByCondition', { 'district': this.form.value.regDistrict }).then((res) => {
      this.regsubdistrictList = res || [];
    });
  }

  ddlRegPostCode() {
    this.service.httpGet('/api/v1/0/sub-district/ddlByPostcode', { 'district': this.form.value.regDistrict, 'subDistrict': this.form.value.regSubDistrict }).then((res) => {
      this.regpostcodeList = res || [];
      for (let item of this.regpostcodeList) {
        this.form.controls.regPostCode.setValue(item['postCode'])
      }
    });
  }

  ddlCurDistrict() {
    this.service.httpGet('/api/v1/0/district/ddlByCondition', { 'province': this.form.value.curProvince }).then((res) => {
      this.curdistrictList = res || [];
    });
  }

  ddlCurSubDistrict() {
    this.service.httpGet('/api/v1/0/sub-district/ddlByCondition', { 'district': this.form.value.curDistrict }).then((res) => {
      this.cursubdistrictList = res || [];
    });
  }

  ddlCurPostCode() {
    this.service.httpGet('/api/v1/0/sub-district/ddlByPostcode', { 'district': this.form.value.curDistrict, 'subDistrict': this.form.value.curSubDistrict }).then((res) => {
      this.curpostcodeList = res || [];
      for (let item of this.curpostcodeList) {
        this.form.controls.curPostCode.setValue(item['postCode'])
      }
    });
  }

  ddlPobDistrict() {
    this.service.httpGet('/api/v1/0/district/ddlByCondition', { 'province': this.form.value.pobProvince }).then((res) => {
      this.pobdistrictList = res || [];
    });
  }

  ddlPobSubDistrict() {
    this.service.httpGet('/api/v1/0/sub-district/ddlByCondition', { 'district': this.form.value.pobDistrict }).then((res) => {
      this.pobsubdistrictList = res || [];
    });
  }

  ddlFatherDistrict() {
    this.service.httpGet('/api/v1/0/district/ddlByCondition', { 'province': this.form.value.fatherProvince }).then((res) => {
      this.fatherdistrictList = res || [];
    });
  }

  ddlFatherSubDistrict() {
    this.service.httpGet('/api/v1/0/sub-district/ddlByCondition', { 'district': this.form.value.fatherDistrict }).then((res) => {
      this.fathersubdistrictList = res || [];
    });
  }

  ddlFatherPostCode() {
    this.service.httpGet('/api/v1/0/sub-district/ddlByPostcode', { 'district': this.form.value.fatherDistrict, 'subDistrict': this.form.value.fatherSubDistrict }).then((res) => {
      this.fatherpostcodeList = res || [];
      for (let item of this.fatherpostcodeList) {
        this.form.controls.fatherPostCode.setValue(item['postCode'])
      }
    });
  }

  ddlMotherDistrict() {
    this.service.httpGet('/api/v1/0/district/ddlByCondition', { 'province': this.form.value.motherProvince }).then((res) => {
      this.motherdistrictList = res || [];
    });
  }

  ddlMotherSubDistrict() {
    this.service.httpGet('/api/v1/0/sub-district/ddlByCondition', { 'district': this.form.value.motherDistrict }).then((res) => {
      this.mothersubdistrictList = res || [];
    });
  }

  ddlMotherPostCode() {
    this.service.httpGet('/api/v1/0/sub-district/ddlByPostcode', { 'district': this.form.value.motherDistrict, 'subDistrict': this.form.value.motherSubDistrict }).then((res) => {
      this.motherpostcodeList = res || [];
      for (let item of this.motherpostcodeList) {
        this.form.controls.motherPostCode.setValue(item['postCode'])
      }
    });
  }

  ddlParentDistrict() {
    this.service.httpGet('/api/v1/0/district/ddlByCondition', { 'province': this.form.value.parentProvince }).then((res) => {
      this.parentdistrictList = res || [];
    });
  }

  ddlParentSubDistrict() {
    this.service.httpGet('/api/v1/0/sub-district/ddlByCondition', { 'district': this.form.value.parentDistrict }).then((res) => {
      this.parentsubdistrictList = res || [];
    });
  }

  ddlParentPostCode() {
    this.service.httpGet('/api/v1/0/sub-district/ddlByPostcode', { 'district': this.form.value.parentDistrict, 'subDistrict': this.form.value.parentSubDistrict }).then((res) => {
      this.parentpostcodeList = res || [];
      for (let item of this.parentpostcodeList) {
        this.form.controls.parentPostCode.setValue(item['postCode'])
      }
    });
  }
  onDownload(txt) {
    window.open(constants.SERVICE_API + '/api/v1/attachment/download/' + txt, '_self')
  }

  onBack(id) {
    this.router.navigate(['/stu/stu005/form-mini'], { queryParams: { id: id } });
  }

  onBackFirst() {
    this.router.navigate(['stu/stu005']);
  }

  countDob(dob) {
    let convert_dob = this._util.transform(dob);
    let mounth_dob = parseInt(convert_dob.substr(-7, 2));
    let year_dob = parseInt(convert_dob.substr(-4)) - 543;

    let date_now = new Date();
    let convert_date_now = this._util.transform(date_now);
    let mounth_date_now = parseInt(convert_date_now.substr(-7, 2));
    let year_date_now = parseInt(convert_date_now.substr(-4)) - 543;

    if (convert_dob.length == 9) {
      this.day_bob = parseInt(convert_dob.substr(-9, 1));
    } else {
      this.day_bob = parseInt(convert_dob.substr(-10, 2));
    }

    if (convert_date_now.length == 9) {
      this.day_date_now = parseInt(convert_date_now.substr(-9, 1));
    } else {
      this.day_date_now = parseInt(convert_date_now.substr(-10, 2));
    }

    let cal_mounth = ((year_date_now * 12) + mounth_date_now) - ((year_dob * 12) + mounth_dob);
    let cal_day = this.day_date_now - this.day_bob;
    let convert_cal_day = cal_day.toString();
    if (convert_cal_day.substr(0, 1) == '-') {
      this.count_dob = 'อายุ : ' + Math.floor((cal_mounth - 1) / 12) + '  ปี  ' + ((cal_mounth - 1) % 12) + '  เดือน';
    } else {
      this.count_dob = 'อายุ : ' + Math.floor(cal_mounth / 12) + '  ปี  ' + (cal_mounth % 12) + '  เดือน';
    }
  }

}
