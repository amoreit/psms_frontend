import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder } from '@angular/forms';
import { ApiService, IResponse } from 'src/app/shared/service/api.service';
import { MatDialog, MatTableDataSource, MatPaginator } from '@angular/material';
import Swal from 'sweetalert2/dist/sweetalert2.js';
import 'sweetalert2/src/sweetalert2.scss';
import { TranslateService } from '@ngx-translate/core';


@Component({
  selector: 'app-stu005',
  templateUrl: './stu005.component.html',
  styleUrls: ['./stu005.component.scss']
})
export class Stu005Component implements OnInit {

  isProcess: boolean = false;
  searchForm = this.fb.group({
    'stuCode': ['']
    , 'citizenId': ['']
    , 'firstnameTh': ['']
    , 'lastnameTh': ['']
    , 'classId': ['']
    , 'classRoomId': ['']
    , 'stuStatus': ['']
    , 'p': ['']
    , 'result': [10]
  });

  pageSize: number;
  checkbutton = false;
  displayedColumns: string[] = ['no', 'stuCode', 'title', 'firstnameTh', 'lastnameTh', 'classRoom', 'citizenId', 'stuStatus', 'stuProfileStatus'];
  dataSource = new MatTableDataSource();
  stustatusList: [];
  classList: [];
  classroomnoList: [];

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  constructor(private fb: FormBuilder,
    private service: ApiService,
    public dialog: MatDialog,
    private router: Router,
    private translate: TranslateService
  ) {}

  ngOnInit() {
    this.ddlStuStatus();
    this.ddlClass();
    this.onSearch(0);
  }

  onSearch(e) {
    this.searchForm.value.p = (this.paginator.pageIndex = e.pageIndex || 0) + 1;
    this.service.httpGet('/api/v1/stu-profile', this.searchForm.value).then((res: IResponse) => {
      this.dataSource.data = res.responseData || [];
      this.pageSize = res.responseSize || 0;
      if(this.pageSize == 0){
        this.checkbutton = true;
      }else{
        this.checkbutton = false;
      }
    });
  }

  onDelete(e) {
    Swal.fire({
      title: this.translate.instant('alert.delete'),
      icon: 'info',
      showCancelButton: true,
      confirmButtonColor: '#2085d6',
      cancelButtonVolor: '#d33',
      confirmButtonText: this.translate.instant('psms.DL0008'),
      cancelButtonText: this.translate.instant('psms.DL0009')
    }).then((result) => {
      if (result.dismiss == 'cancel') {
        return;
      }
      this.isProcess = true;
      this.service.httpDelete('/api/v1/stu-profile', { 'stuProfileId': e.stu_profile_id }).then((res: IResponse) => {
        this.isProcess = false;
        if ((res.responseCode || 500) != 200) {
          Swal.fire(
            this.translate.instant('message.delete_error'),
            res.responseMsg,
            'error'
          )
          return;
        }
        Swal.fire(
          this.translate.instant('alert.delete_header'),
          this.translate.instant('message.delete'),
          'success'
        )
        this.onSearch(0);
      });
    });
  }

  onCancel() {
    this.searchForm = this.fb.group({
      'stuCode': ['']
      , 'citizenId': ['']
      , 'firstnameTh': ['']
      , 'lastnameTh': ['']
      , 'classId': ['']
      , 'classRoomId': ['']
      , 'stuStatus': ['']
      , 'p': ['']
      , 'result': [10]
    });
    this.onSearch(0);
    this.searchForm.controls.classRoomId.setValue('');
  }

  onForm(id) {
    this.router.navigate(['/stu/stu005/form-mini'], { queryParams: { id: id } });
  }

  ddlStuStatus() {
    this.service.httpGet('/api/v1/0/stu-status/ddl', null).then((res) => {
      this.stustatusList = res || [];
    });
  }

  ddlClass() {
    this.service.httpGet('/api/v1/0/class/ddl', null).then((res) => {
      this.classList = res || [];
    });
  }

  ddlClassRoomNo() {
    let json = { 'classId': this.searchForm.value.classId };
    this.searchForm.controls.classRoomId.setValue('');
    this.service.httpGet('/api/v1/0/classroom/ddlByCondition', json).then((res) => {
      this.classroomnoList = res || [];
    });
  }

  onPreviewPDF() {
    this.isProcess = true;

    if(this.searchForm.value.stuCode == '' && this.searchForm.value.classId == '' && this.searchForm.value.classRoomId == ''){
      this.service.httpPreviewPDF('/api/v1/stu-profile/report/pdfall', this.searchForm.value).then((res) => {
        this.isProcess = false;
        window.location.href = res;
      });
    }
    if(this.searchForm.value.stuCode != '' && this.searchForm.value.classId == '' && this.searchForm.value.classRoomId == ''){
      this.service.httpPreviewPDF('/api/v1/stu-profile/report/pdfstucode', this.searchForm.value).then((res) => {
        this.isProcess = false;
        window.location.href = res;
      });
    }
    if(this.searchForm.value.classId != '' && this.searchForm.value.classRoomId == '' && this.searchForm.value.stuCode == ''){
      this.service.httpPreviewPDF('/api/v1/stu-profile/report/pdfclassid', this.searchForm.value).then((res) => {
        this.isProcess = false;
        window.location.href = res;
      });
    }
    if(this.searchForm.value.classId != '' && this.searchForm.value.classRoomId != '' && this.searchForm.value.stuCode == ''){
      this.service.httpPreviewPDF('/api/v1/stu-profile/report/pdfclassidandclassroomid', this.searchForm.value).then((res) => {
        this.isProcess = false;
        window.location.href = res;
      });
    }
    if(this.searchForm.value.stuCode != '' && this.searchForm.value.classId != '' && this.searchForm.value.classRoomId != ''){
      this.service.httpPreviewPDF('/api/v1/stu-profile/report/pdfstucodeandclassidandclassroomid', this.searchForm.value).then((res) => {
        this.isProcess = false;
        window.location.href = res;
      });
    }
    if(this.searchForm.value.stuCode != '' && this.searchForm.value.classId != '' && this.searchForm.value.classRoomId == ''){
      this.service.httpPreviewPDF('/api/v1/stu-profile/report/pdfstucodeandclassid', this.searchForm.value).then((res) => {
        this.isProcess = false;
        window.location.href = res;
      });
    }
  }

  onExportPDF() {
    this.isProcess = true;

    if(this.searchForm.value.stuCode == '' && this.searchForm.value.classId == '' && this.searchForm.value.classRoomId == ''){
      this.service.httpReportPDF('/api/v1/stu-profile/report/pdfall', "ข้อมูลทะเบียนประวัตินักเรียน", this.searchForm.value).then((res) => {
        this.isProcess = false;
      });
    }
    if(this.searchForm.value.stuCode != '' && this.searchForm.value.classId == '' && this.searchForm.value.classRoomId == ''){
      this.service.httpReportPDF('/api/v1/stu-profile/report/pdfstucode', "ข้อมูลทะเบียนประวัตินักเรียน", this.searchForm.value).then((res) => {
        this.isProcess = false;
      });
    }
    if(this.searchForm.value.classId != '' && this.searchForm.value.classRoomId == '' && this.searchForm.value.stuCode == ''){
      this.service.httpReportPDF('/api/v1/stu-profile/report/pdfclassid', "ข้อมูลทะเบียนประวัตินักเรียน", this.searchForm.value).then((res) => {
        this.isProcess = false;
      });
    }
    if(this.searchForm.value.classId != '' && this.searchForm.value.classRoomId != '' && this.searchForm.value.stuCode == ''){
      this.service.httpReportPDF('/api/v1/stu-profile/report/pdfclassidandclassroomid', "ข้อมูลทะเบียนประวัตินักเรียน", this.searchForm.value).then((res) => {
        this.isProcess = false;
      });
    }
    if(this.searchForm.value.stuCode != '' && this.searchForm.value.classId != '' && this.searchForm.value.classRoomId != ''){
      this.service.httpReportPDF('/api/v1/stu-profile/report/pdfstucodeandclassidandclassroomid', "ข้อมูลทะเบียนประวัตินักเรียน", this.searchForm.value).then((res) => {
        this.isProcess = false;
      });
    }
    if(this.searchForm.value.stuCode != '' && this.searchForm.value.classId != '' && this.searchForm.value.classRoomId == ''){
      this.service.httpReportPDF('/api/v1/stu-profile/report/pdfstucodeandclassid', "ข้อมูลทะเบียนประวัตินักเรียน", this.searchForm.value).then((res) => {
        this.isProcess = false;
      });
    }
  }

  onExportExcel() {
    this.isProcess = true;

    if(this.searchForm.value.stuCode == '' && this.searchForm.value.classId == '' && this.searchForm.value.classRoomId == ''){
      this.service.httpReportXLS('/api/v1/stu-profile/report/xlsall', "ข้อมูลทะเบียนประวัตินักเรียน", this.searchForm.value).then((res) => {
        this.isProcess = false;
      });
    }
    if(this.searchForm.value.stuCode != '' && this.searchForm.value.classId == '' && this.searchForm.value.classRoomId == ''){
      this.service.httpReportXLS('/api/v1/stu-profile/report/xlsstucode', "ข้อมูลทะเบียนประวัตินักเรียน", this.searchForm.value).then((res) => {
        this.isProcess = false;
      });
    }
    if(this.searchForm.value.classId != '' && this.searchForm.value.classRoomId == '' && this.searchForm.value.stuCode == ''){
      this.service.httpReportXLS('/api/v1/stu-profile/report/xlsclassid', "ข้อมูลทะเบียนประวัตินักเรียน", this.searchForm.value).then((res) => {
        this.isProcess = false;
      });
    }
    if(this.searchForm.value.classId != '' && this.searchForm.value.classRoomId != '' && this.searchForm.value.stuCode == ''){
      this.service.httpReportXLS('/api/v1/stu-profile/report/xlsclassidandclassroomid', "ข้อมูลทะเบียนประวัตินักเรียน", this.searchForm.value).then((res) => {
        this.isProcess = false;
      });
    }
    if(this.searchForm.value.stuCode != '' && this.searchForm.value.classId != '' && this.searchForm.value.classRoomId != ''){
      this.service.httpReportXLS('/api/v1/stu-profile/report/xlsstucodeandclassidandclassroomid', "ข้อมูลทะเบียนประวัตินักเรียน", this.searchForm.value).then((res) => {
        this.isProcess = false;
      });
    }
    if(this.searchForm.value.stuCode != '' && this.searchForm.value.classId != '' && this.searchForm.value.classRoomId == ''){
      this.service.httpReportXLS('/api/v1/stu-profile/report/xlsstucodeandclassid', "ข้อมูลทะเบียนประวัตินักเรียน", this.searchForm.value).then((res) => {
        this.isProcess = false;
      });
    }
  }

  

}
