import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { ApiService, IResponse } from 'src/app/shared/service/api.service';
import * as constants from '../../../../constants'; 
import Swal from 'sweetalert2/dist/sweetalert2.js';
import 'sweetalert2/src/sweetalert2.scss'; 
import { UtilService } from 'src/app/_util/util.service';
import { Stu005IsleftDialogComponent } from 'src/app/dialog/stu005-isleft-dialog/stu005-isleft-dialog.component';
import { MatDialog } from '@angular/material';
import { TranslateService } from '@ngx-translate/core';

@Component({ 
  selector: 'app-stu005-form-mini',
  templateUrl: './stu005-form-mini.component.html',
  styleUrls: ['./stu005-form-mini.component.scss']
})
export class Stu005FormMiniComponent implements OnInit {

  form = this.fb.group({
    'stuProfileId': [''] 
    , 'stuImage': ['']
    , 'title': ['']
    , 'titleEn': ['']
    , 'firstnameTh': ['']
    , 'lastnameTh': ['']
    , 'nickname': ['']
    , 'gender': ['']
    , 'genderId': ['']
    , 'dob': ['']
    , 'pob': ['']
    , 'citizenId': ['']
    , 'pobProvince': ['']
    , 'pobDistrict': ['']
    , 'pobSubDistrict': ['']
    , 'stuStatusId': ['']
    , 'scCode': ['']
  });

  formReport = this.fb.group({
    'stuProfileId': ['']
  });


  id: any;
  isProcess: boolean = false;

  fullname: String; // ชื่อเต็ม
  class_room: String // ห้องเรียน
  stu_code: String // รหัสนักเรียน
  smart_purse_id: String;
  selectedFile: any;
  show_name_file: any;
  day_bob: any;
  day_date_now: any
  count_dob: any
  is_left: any

  urls = new Array<string>();

  genderList: [];
  titleList: [];
  stustatusList: [];
  provinceList: [];
  pobdistrictList: [];
  pobsubdistrictList: [];

  constructor(
    private fb: FormBuilder,
    private router: Router,
    private service: ApiService,
    private ar: ActivatedRoute, 
    public _util: UtilService,
    public dialog: MatDialog,
    private translate: TranslateService
  ) { }

  ngOnInit() {
    this.id = this.ar.snapshot.queryParamMap.get('id') || '';
    this.loadData();
    this.ddlGender();
    this.ddlTitle();
    this.ddlStuStatus();

  }

  detectFiles(event) {
    this.urls = [];
    let files = event.target.files;
    if (files) {
      for (let file of files) {
        let reader = new FileReader();
        reader.onload = (e: any) => {
          this.urls.push(e.target.result);
        }
        reader.readAsDataURL(file);
      }
    }
    this.selectedFile = event.target.files[0];
    this.show_name_file = this.selectedFile.name;

  }

  onReport() {
    this.isProcess = true;
    this.service.httpPreviewPDF('/api/v1/stu-profile-register/report/pdf', this.formReport.value).then((res) => {
      this.isProcess = false;
      window.location.href = res;
    });
  }

  loadData() {
    this.isProcess = true;
    this.service.httpGet('/api/v1/stu-profile/' + this.id, null).then((res: IResponse) => {
      this.isProcess = false;
      let data = res.responseData;

      this.form.controls.stuProfileId.setValue(data.stuProfileId || '');
      this.form.controls.citizenId.setValue(data.citizenId || '');
      this.form.controls.title.setValue(data.title || '')
      this.form.controls.titleEn.setValue(data.titleEn || '')
      this.form.controls.firstnameTh.setValue(data.firstnameTh || '')
      this.form.controls.lastnameTh.setValue(data.lastnameTh || '')
      this.form.controls.nickname.setValue(data.nickname || '')
      this.form.controls.genderId.setValue(data.genderId || '')
      this.form.controls.gender.setValue(data.gender || '')
      this.form.controls.dob.setValue(this._util.formatDateSQLToDate(data.dob || ''))
      this.countDob(this.form.value.dob)
      this.form.controls.pob.setValue(data.pob || '')
      this.form.controls.pobProvince.setValue(data.pobProvince || '');
      this.form.controls.pobDistrict.setValue(data.pobDistrict || '');
      this.form.controls.pobSubDistrict.setValue(data.pobSubDistrict || '');
      this.form.controls.stuStatusId.setValue(data.joinStuStatus.stuStatusId || '');
      this.form.controls.stuImage.setValue(data.stuImage || '');
      this.formReport.controls.stuProfileId.setValue(data.stuProfileId || '');
      this.fullname = data.fullname;
      this.class_room = data.classRoom;
      this.stu_code = data.stuCode;
      this.smart_purse_id = data.smartPurseId;
      this.is_left = data.isleft;

      if (this.form.value.stuImage == null || this.form.value.stuImage === '' || this.form.value.stuImage == undefined) {
        this.urls = [constants.SERVICE_API + '/api/v1/attachment/view/user-no-img.jpg'];
      } else {
        this.urls = [constants.SERVICE_API + '/api/v1/attachment/view/' + this.form.value.stuImage];
      }

      this.ddlProvince()
      this.ddlPobDistrict()
      this.ddlPobSubDistrict()

    });
  }

  ddlProvince() {
    this.service.httpGet('/api/v1/0/province/ddl', null).then((res) => {
      this.provinceList = res || [];
    });
  }

  ddlPobDistrict() {
    this.service.httpGet('/api/v1/0/district/ddlByCondition', { 'province': this.form.value.pobProvince }).then((res) => {
      this.pobdistrictList = res || [];
    });
  }

  ddlPobSubDistrict() {
    this.service.httpGet('/api/v1/0/sub-district/ddlByCondition', { 'district': this.form.value.pobDistrict }).then((res) => {
      this.pobsubdistrictList = res || [];
    });
  }

  onCheckTitle() {
    let ele = this.titleList.filter((o) => {
      return o['name'] == this.form.value.title;
    })
    this.form.controls.genderId.setValue(ele[0]['gender'])

    let _ele = this.genderList.filter((o) => {
      return o['genderId'] == this.form.value.genderId;
    });
    this.form.controls.gender.setValue(_ele[0]['genderDescTh'])

    if (this.form.value.title == 'ด.ช.') {
      this.form.controls.titleEn.setValue('M.')
    }
    if (this.form.value.title == 'นาย') {
      this.form.controls.titleEn.setValue('MR.')
    }
    if (this.form.value.title == 'ด.ญ.' || this.form.value.title == 'น.ส.') {
      this.form.controls.titleEn.setValue('MS.')
    }
  }

  /* DROP DOWN คำนำหน้าชื่อ */
  ddlTitle() {
    let json = { 'langCode': 'th' };
    this.service.httpGet('/api/v1/0/title/ddl', json).then((res) => {
      this.titleList = res || [];
    });
  }

  /* DROP DOWN เพศ */
  ddlGender() {
    this.service.httpGet('/api/v1/0/gender/ddl', null).then((res) => {
      this.genderList = res || [];
    });
  }

  ddlStuStatus() {
    this.service.httpGet('/api/v1/0/stu-status/ddl', null).then((res) => {
      this.stustatusList = res || [];
    });
  }

  onBack() {
    this.router.navigate(['stu/stu005']);
  }

  onForm(id) {
    this.router.navigate(['/stu/stu005/form'], { queryParams: { id: id } });
  }

  countDob(dob) {
    let convert_dob = this._util.transform(dob);
    let mounth_dob = parseInt(convert_dob.substr(-7, 2));
    let year_dob = parseInt(convert_dob.substr(-4)) - 543;

    let date_now = new Date();
    let convert_date_now = this._util.transform(date_now);
    let mounth_date_now = parseInt(convert_date_now.substr(-7, 2));
    let year_date_now = parseInt(convert_date_now.substr(-4)) - 543;

    if (convert_dob.length == 9) {
      this.day_bob = parseInt(convert_dob.substr(-9, 1));
    } else {
      this.day_bob = parseInt(convert_dob.substr(-10, 2));
    }

    if (convert_date_now.length == 9) {
      this.day_date_now = parseInt(convert_date_now.substr(-9, 1));
    } else {
      this.day_date_now = parseInt(convert_date_now.substr(-10, 2));
    }

    let cal_mounth = ((year_date_now * 12) + mounth_date_now) - ((year_dob * 12) + mounth_dob);
    let cal_day = this.day_date_now - this.day_bob;
    let convert_cal_day = cal_day.toString();
    if (convert_cal_day.substr(0, 1) == '-') {
      this.count_dob = 'อายุ : ' + Math.floor((cal_mounth - 1) / 12) + '  ปี  ' + ((cal_mounth - 1) % 12) + '  เดือน';
    } else {
      this.count_dob = 'อายุ : ' + Math.floor(cal_mounth / 12) + '  ปี  ' + (cal_mounth % 12) + '  เดือน';
    }
  }

  onIsLeftDialog(stuProfileId): void {
    const dialogRef = this.dialog.open(Stu005IsleftDialogComponent, {
      width: '60%'
      , 'autoFocus': true
      , 'disableClose': true
      , data: {
        'stuProfileId': stuProfileId
      }
    });
    dialogRef.afterClosed().subscribe(result => {
      if ((result || '') == '') {
        this.onBack()
      }

    });
  }

  onCancelIsLeft() {
    Swal.fire({
      title: this.translate.instant('alert.isleft'),
      icon: 'info',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: this.translate.instant('psms.DL0008'),
      cancelButtonText: this.translate.instant('psms.DL0009')
    }).then((result) => {
      if (result.dismiss == 'cancel') {
        return;
      }
      let json = this.form.value
      this.service.httpPut('/api/v1/stu-profile/cancelisleft', { 'stuProfileId': this.form.value.stuProfileId }).then((res: IResponse) => {
        if ((res.responseCode || 500) != 200) {
          Swal.fire(
            this.translate.instant('alert.isleft_error'),
            res.responseMsg,
            'error'
          )
          return;
        }
        Swal.fire(
          this.translate.instant('alert.isleft_header'),
          this.translate.instant('message.isleft'),
          'success'
        ).then(() => { this.onBack(); })
      })
      return;
    });
  }


  onSave() {
    if (this.form.invalid) {
      Swal.fire(
        this.translate.instant('alert.error'),
        this.translate.instant('alert.validate'),
        'error'
      )
      return;
    }

    Swal.fire({
      title: this.translate.instant('psms.DL0001'),
      icon: 'info',
      showCancelButton: true,
      confirmButtonColor: '#2085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: this.translate.instant('psms.DL0008'),
      cancelButtonText: this.translate.instant('psms.DL0009')
    }).then((result) => {
      if (result.dismiss == 'cancel') {
        return;
      }
      this.form.value.scCode = 'PRAMANDA';
      let json = this.form.value;

      if (this.selectedFile == undefined) {
        this.service.httpPut('/api/v1/stu-profile/mini', json).then((res: IResponse) => {
          if ((res.responseCode || 500) != 200) {
            Swal.fire(
              this.translate.instant('message.edit_error'),
              res.responseMsg,
              'error'
            )
            return; 
          }
          Swal.fire(
            this.translate.instant('psms.DL0006'),
            this.translate.instant('message.edit'),
            'success'
          ).then(() => {
            this.onBack();
          })
        });
      } else {
        if (this.stu_code == null || this.stu_code === '' || this.stu_code == undefined) {
          Swal.fire(
            this.translate.instant('alert.no_stucode'),
            this.translate.instant('alert.no_upload'),
            'error'
          )
        } else {
          const uploadData = new FormData();
          uploadData.append('file', this.selectedFile, this.selectedFile.name);

          this.service.httpPost("/api/v1/attachment/upload/" + this.stu_code, uploadData).then((res: IResponse) => {
            if (res.responseCode != 200) {
              Swal.fire(
                this.translate.instant('alert.error_pic'),
                res.responseMsg,
                'error'
              )
              return;
            }

            let jsons = this.form.value;
            this.form.value.stuImage = res.responseData.attachmentCode + "/" + res.responseData.attachmentName;

            this.service.httpPut('/api/v1/stu-profile/mini', jsons).then((res: IResponse) => {
              if ((res.responseCode || 500) != 200) {
                Swal.fire(
                  this.translate.instant('message.edit_error'),
                  res.responseMsg,
                  'error'
                )
                return;
              }
              Swal.fire(
                this.translate.instant('psms.DL0006'),
                this.translate.instant('message.edit'),
                'success'
              ).then(() => {
                this.onBack();
              })
            });

          });
        }
      } //
    });
  }

  onCancel() {
    this.loadData();
  }

}
