import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FlexLayoutModule } from '@angular/flex-layout';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { TranslateModule } from '@ngx-translate/core';

/*** Material ***/
import {
  MatExpansionModule,
  MatButtonModule,
  MatIconModule,
  MatInputModule,
  MatTableModule,
  MatCardModule,
  MatFormFieldModule,
  MatPaginatorModule,
  MatDialogModule,
  MatSlideToggleModule,
  MatTabsModule,
  MatRadioModule,
} from '@angular/material';

/*** Routing ***/
import {RepRoutes} from './rep.routing'; 

/*** Page ***/
import { Rep001Component } from './rep001/rep001.component';
import { Rep002Component } from './rep002/rep002.component';
import { Rep004Component } from './rep004/rep004.component';
import { Rep005Component } from './rep005/rep005.component'; 
import { Rep006Component } from './rep006/rep006.component';

import { Rep001ReportComponent } from './rep001-report/rep001-report.component';
import { MatDatepickerModule} from '@angular/material/datepicker';

@NgModule({
  imports: [
    CommonModule,
    FlexLayoutModule,
    RouterModule.forChild(RepRoutes),
    MatExpansionModule,
    MatButtonModule,
    TranslateModule,
    MatDatepickerModule, 
    MatIconModule,
    MatInputModule,
    MatTableModule,
    MatCardModule,
    MatFormFieldModule,
    ReactiveFormsModule,
    MatPaginatorModule,
    MatDialogModule,
    MatSlideToggleModule,
    MatTabsModule,
    FormsModule,
    MatRadioModule
  ],
    providers: [],
    exports:[RouterModule],
  declarations: [
    Rep001Component, 
    Rep002Component, 
    Rep004Component, 
    Rep005Component, 
    Rep006Component,
    Rep001ReportComponent
  ],
  entryComponents:[
  ]

})
export class RepModule { }
