
import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormBuilder } from '@angular/forms';
import { ApiService, IResponse } from 'src/app/shared/service/api.service';
import { MatPaginator } from '@angular/material';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-rep001-report',
  templateUrl: './rep001-report.component.html',
  styleUrls: ['./rep001-report.component.scss']
})
export class Rep001ReportComponent implements OnInit {
  isProcess = false;

  // form search ภาคเรียนที่ 1 & จำนวนนักเรียน
  searchFormTerm1 = this.fb.group({
    yearGroupName: [''],
    classId: [''],
    classRoomId: [''],
    classRoomMemberId: [''],
    stuCode: ['']
  });

  // form search ภาคเรียนที่ 2 & จำนวนนักเรียน
  searchFormTerm2 = this.fb.group({
    yearGroupName: [''],
    classId: [''],
    classRoomId: [''],
    classRoomMemberId: [''],
    stuCode: [''],
  });

  // เก็บค่าจาก query
  dataSourceListTerm1: [];
  dataSourceListTerm2: [];
  dataSourceListTeacher: [];
  dataSourceMax: [];

  dataTerm1: any;
  dataTerm2: any;

  // เก็บค่าจาก rep001
  yearGroupName: any;
  fullName: any;
  classRoom: any;
  teacherName: any;

  // เก็บค่าจำนวนนักเรียน
  amountTerm1: number;
  amountTerm2: number;

  // เก็บค่ารวม
  fullScoreArray: any;
  scoreFinalArrayTerm1: any;
  scoreFinalArrayTerm2: any;

  totalFullScoreArray = 0;
  totalScoreFinalTerm1 = 0;
  totalScoreFinalTerm2 = 0;
  totalPercent = 0;


  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;

  constructor(
    private translate: TranslateService,
    ar: ActivatedRoute,
    private fb: FormBuilder,
    private service: ApiService,
    private router: Router,
  ) {
    this.isProcess = true;
    this.searchFormTerm1.value.yearGroupName = ar.snapshot.queryParamMap.get('yearGroupName') || '';
    this.searchFormTerm1.value.classId = ar.snapshot.queryParamMap.get('classId') || '';
    this.searchFormTerm1.value.classRoomId = ar.snapshot.queryParamMap.get('classRoomId') || '';
    this.searchFormTerm1.value.classRoomMemberId = ar.snapshot.queryParamMap.get('classRoomMemberId') || '';
    this.searchFormTerm1.value.stuCode = ar.snapshot.queryParamMap.get('stuCode') || '';

    this.searchFormTerm2.value.yearGroupName = ar.snapshot.queryParamMap.get('yearGroupName') || '';
    this.searchFormTerm2.value.classId = ar.snapshot.queryParamMap.get('classId') || '';
    this.searchFormTerm2.value.classRoomId = ar.snapshot.queryParamMap.get('classRoomId') || '';
    this.searchFormTerm2.value.classRoomMemberId = ar.snapshot.queryParamMap.get('classRoomMemberId') || '';
    this.searchFormTerm2.value.stuCode = ar.snapshot.queryParamMap.get('stuCode') || '';

  }

  ngOnInit() {
    this.onSearchTerm1();
    this.onSearchTerm2();
    this.onSearchTeacher();
    this.onSearchCountTerm1();
    this.onSearchCountTerm2();
  }

  // search ภาคเรียนที่ 1
  onSearchTerm1() {
    this.service.httpGet('/api/v1/classRoom-member/rep001/reportTerm1', this.searchFormTerm1.value).then((res: IResponse) => {
      this.dataSourceListTerm1 = res.responseData || [];
      this.yearGroupName = res.responseData[0] && res.responseData[0].year_group_name.substring(2, 8);
      this.fullName = res.responseData[0] && res.responseData[0].fullname;
      this.classRoom = res.responseData[0] && res.responseData[0].class_room.substring(2, 7);
      this.onSumFullScoreTerm1();

    });
  }

  // search ภาคเรียนที่ 2
  onSearchTerm2() {
    this.service.httpGet('/api/v1/classRoom-member/rep001/reportTerm2', this.searchFormTerm2.value).then((res: IResponse) => {
      this.dataSourceListTerm2 = res.responseData || [];
      this.onSumFullScoreTerm2();

    });
  }

  onSearchTeacher() {
    this.service.httpGet('/api/v1/classRoom-member/rep001/reportTeacher', this.searchFormTerm1.value).then((res: IResponse) => {
      this.dataSourceListTeacher = res.responseData || [];
      this.teacherName = res.responseData[0] && res.responseData[0].fullname;

    });

  }


  // รวม score ภาคเรียนที่ 1 & full score
  onSumFullScoreTerm1() {
    this.dataTerm1 = this.dataSourceListTerm1;
    this.scoreFinalArrayTerm1 = [];
    this.fullScoreArray = [];

    // tslint:disable-next-line:prefer-for-of
    for (let i = 0; i < this.dataTerm1.length; i++) { // วน for query ภาคเรียนที่ 1
      if (this.dataTerm1[i].subject_type === '1') {
        this.fullScoreArray.push(this.dataTerm1[i].full_score_final);
        this.totalFullScoreArray = this.fullScoreArray.reduce((acc, cur) => acc + cur, 0);

        this.scoreFinalArrayTerm1.push(this.dataTerm1[i].score_final);
        this.totalScoreFinalTerm1 = this.scoreFinalArrayTerm1.reduce((acc, cur) => acc + cur, 0);

      }

      // else if (this.data[i].subject_type === '2') {
      //   this.fullScoreData1 += this.data[i].full_score_final;
      //   this.scoreFinal1 += this.data[i].score_final;
      // }
    }
    this.onCalPercent();

  }

  // รวม score ภาคเรียนที่ 2
  onSumFullScoreTerm2() {
    this.dataTerm2 = this.dataSourceListTerm2;
    this.scoreFinalArrayTerm2 = [];


    // tslint:disable-next-line:prefer-for-of
    for (let i = 0; i < this.dataTerm2.length; i++) { // วน for query ภาคเรียนที่ 2
      if (this.dataTerm2[i].subject_type === '1') {
        this.scoreFinalArrayTerm2.push(this.dataTerm2[i].score_final);
        this.totalScoreFinalTerm2 = this.scoreFinalArrayTerm2.reduce((acc, cur) => acc + cur, 0);

      }
      // else if (this.data[i].subject_type === '2') {
      //   this.fullScoreData1 += this.data1[i].full_score_final;
      //   this.scoreFinal1 += this.data1[i].score_final;
      // }
    }
    this.onCalPercent();
  }


  // search จำนวนนักเรียน ภาคเรียนที่ 1
  onSearchCountTerm1() {
    this.service.httpGet('/api/v1/classRoom-member/rep001/reportCountTerm1', this.searchFormTerm1.value).then((res: IResponse) => {
      this.amountTerm1 = res.responseSize || 0;
    });
  }

  // search จำนวนนักเรียน ภาคเรียนที่ 2
  onSearchCountTerm2() {
    this.service.httpGet('/api/v1/classRoom-member/rep001/reportCountTerm2', this.searchFormTerm2.value).then((res: IResponse) => {
      this.amountTerm2 = res.responseSize || 0;
    });
  }

  onCalPercent() {
    this.totalPercent = (this.totalScoreFinalTerm1 + this.totalScoreFinalTerm2) / 2;
  }

  printComponent(cmpName) {
    const printContents = document.getElementById(cmpName).innerHTML;
    const originalContents = document.body.innerHTML;

    document.body.innerHTML = printContents;
    window.print();
    document.body.innerHTML = originalContents;
    window.close();
    window.location.reload(true);

  }

  onBackPage() {
    this.router.navigate(['/rep/rep001']);
  }
}
