import { Component, OnInit, ViewChild, Inject } from "@angular/core";
import { FormBuilder, Validators, FormGroup } from "@angular/forms";
import {
  MatPaginator,
  MatTableDataSource,
  MAT_DIALOG_DATA,
  MatDialog
} from "@angular/material";
import {
  ApiService,
  IResponse,
  EHttp
} from "../../../shared/service/api.service";
import { Router } from "@angular/router";
import Swal from "sweetalert2/dist/sweetalert2.js";
import "sweetalert2/src/sweetalert2.scss";
import { TabHeadingDirective } from "ngx-bootstrap";
import { TranslateService } from '@ngx-translate/core';
@Component({
  selector: "app-rep004",
  templateUrl: "./rep004.component.html",
  styleUrls: ["./rep004.component.scss"]
})
export class Rep004Component implements OnInit {
  Langcode:string;
  TermCode:string;
  Page:string;
  isProcess: boolean = false;
  searchForm = this.fb.group({
    yearGroupId: [""],
    p: [""],
    classId: [""],
    classRoomId: [""],
    langCode:[""],
    termCode:[""],
    result: [10]
  });
  termForm = this.fb.group({
    termCode:[""],
    page:[""]
  });
  report = this.fb.group({
    yearGroupId: [""],
    classId: [""],
    classRoomId: [""],
  });
  pageSize: number;
  displayedColumns: string[] = [
    "yearGroupName",
    "className",
    "classRoom",
    "yearStatus"
  ];
  dataSource = new MatTableDataSource();
  yearList: [];
  classList: [];
  classRoomList: [];
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  constructor(
    private translate: TranslateService,
    private fb: FormBuilder,
    private service: ApiService,
    public dialog: MatDialog,
    private router: Router
  ) {}
  ngOnInit() {
    this.searchForm.controls.langCode.setValue("1");
    this.termForm.controls.termCode.setValue("1");
    this.termForm.controls.page.setValue("1");
    this.ddlYear();
    this.ddlClass();
    this.ddlClassRoom();
    this.onSearch(0);
  }
  onSearch(e) {
    this.searchForm.value.p = (this.paginator.pageIndex = e.pageIndex || 0) + 1;
    this.service.httpGet("/api/v1/classRoom-member/rep004", this.searchForm.value).then((res: IResponse) => {this.dataSource.data = res.responseData || [];this.pageSize = res.responseSize || 0;});
  }
  onReport(yearGrooupId, classId, classRoomId) {
    this.isProcess = true;
    this.report.controls.yearGroupId.setValue(yearGrooupId);
    this.report.controls.classId.setValue(classId);
    this.report.controls.classRoomId.setValue(classRoomId);
    this.Langcode = this.searchForm.value.langCode;
    this.TermCode = this.termForm.value.termCode;
    this.Page = this.termForm.value.page;
    if(this.Langcode == "1"){//ไทย
      if(this.TermCode == "1"){//กลางภาค 
        if(this.Page == "1"){//A4
          this.service.httpPreviewPDF("/api/v1/rep004/report/pdfA41mid", this.report.value).then(res => {this.isProcess = false;window.location.href = res;}); 
          this.service.httpPreviewPDF("/api/v1/rep004/report/pdfA42mid", this.report.value).then(res => {this.isProcess = false;window.location.href = res;});
        }
        else if(this.Page == "2"){//A3
          this.service.httpPreviewPDF("/api/v1/rep004/report/pdfA3mid", this.report.value).then(res => {this.isProcess = false;window.location.href = res;});
        }
      } 
      else if(this.TermCode == "2"){//ปลายภาค
        if(this.Page == "1"){//A4
          this.service.httpPreviewPDF("/api/v1/rep004/report/pdfA41final", this.report.value).then(res => {this.isProcess = false;window.location.href = res;}); 
          this.service.httpPreviewPDF("/api/v1/rep004/report/pdfA42final", this.report.value).then(res => {this.isProcess = false;window.location.href = res;}); 

        }
        else if(this.Page == "2"){//A3
          this.service.httpPreviewPDF("/api/v1/rep004/report/pdfA3final", this.report.value).then(res => {this.isProcess = false;window.location.href = res;}); 
        } 
      }
      else if(this.TermCode == "3"){//รวมสองเทอม
        if(this.Page == "1"){//A4
          this.service.httpPreviewPDF("/api/v1/rep004eng/report/pdf", this.report.value).then(res => {this.isProcess = false;window.location.href = res;});//x
        }
        else if(this.Page == "2"){//A3
          this.service.httpPreviewPDF("/api/v1/rep004eng/report/pdf", this.report.value).then(res => {this.isProcess = false;window.location.href = res;});//x
        }
      }
    }
    else if(this.Langcode == "2"){//อังกฤษ
      if(this.TermCode == "1"){//กลางภาค
        if(this.Page == "1"){//A4
          this.service.httpPreviewPDF("/api/v1/rep004eng/report/pdf", this.report.value).then(res => {this.isProcess = false;window.location.href = res;});//x
        }
        else if(this.Page == "2"){//A3
          this.service.httpPreviewPDF("/api/v1/rep004eng/report/pdf", this.report.value).then(res => {this.isProcess = false;window.location.href = res;});//x
        }
      }
      else if(this.TermCode == "2"){//ปลายภาค
        if(this.Page == "1"){//A4
          this.service.httpPreviewPDF("/api/v1/rep004eng/report/pdf", this.report.value).then(res => {this.isProcess = false;window.location.href = res;});//x
        }
        else if(this.Page == "2"){//A3
          this.service.httpPreviewPDF("/api/v1/rep004eng/report/pdf", this.report.value).then(res => {this.isProcess = false;window.location.href = res;});//x
        }
      }
      else if(this.TermCode == "3"){//รวมสองเทอม
        if(this.Page == "1"){//A4
          this.service.httpPreviewPDF("/api/v1/rep004eng/report/pdf", this.report.value).then(res => {this.isProcess = false;window.location.href = res;});//x
        }
        else if(this.Page == "2"){//A3
          this.service.httpPreviewPDF("/api/v1/rep004eng/report/pdf", this.report.value).then(res => {this.isProcess = false;window.location.href = res;});//x
        }
      }
    }
  }
  ddlYear() {
    this.service.httpGet("/api/v1/0/year-group/ddl", null).then(res => {this.yearList = res || [];this.searchForm.controls.yearGroupId.setValue(res[0]['yearGroupId']);});
  }
  ddlClass() {
    this.service.httpGet("/api/v1/0/class/ddl", null).then(res => {this.classList = res || [];});
  }
  ddlClassRoom() {
    let json = {classId: this.searchForm.value.classId};
    this.service.httpGet("/api/v1/0/classroom-list/ddlFilter", json).then(res => {this.classRoomList = res || [];});
  }
}
