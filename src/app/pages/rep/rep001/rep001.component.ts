import { Component, OnInit, ViewChild, Inject } from '@angular/core';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { MatPaginator, MatTableDataSource, MAT_DIALOG_DATA, MatDialog } from '@angular/material';
import { ApiService, IResponse, EHttp } from '../../../shared/service/api.service';
import { Router } from '@angular/router';
import 'sweetalert2/src/sweetalert2.scss';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-rep001',
  templateUrl: './rep001.component.html',
  styleUrls: ['./rep001.component.scss']
})
export class Rep001Component implements OnInit {


  isProcess = false;
  searchForm = this.fb.group({ 
    yearGroupName: ['']
    , classId: ['']
    , classRoomId: ['']
    , p: ['']
    , result: [10]
  });


  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  pageSize: number;

  yearGroupName: any;
  selected: string;
  classId: any;
  classRoomId: any;

  displayedColumns: string[] = ['no', 'stuCode', 'fullname', 'classRoom', 'action'];
  dataSource = new MatTableDataSource();
  yearList: [];
  classList: [];
  roomList: [];


  constructor(
    private translate: TranslateService,
    private fb: FormBuilder,
    private service: ApiService,
    public dialog: MatDialog,
    private router: Router) {
  }

  ngOnInit() {
    this.ddlClassroom();
    this.ddlClass();
    this.ddlYear();
    this.onSearch(0);

  }

  // dropdown ปีการศึกษา
  ddlYear() {
    this.service.httpGet('/api/v1/0/year-group/ddl', null).then((res) => {
      this.yearList = res || [];
      this.searchForm.controls.yearGroupName.setValue(res[0].name);
    });


  }
  // dropdown ชั้นเรียน
  ddlClass() {
    this.service.httpGet('/api/v1/0/class/ddlCatclassrep001', null).then((res) => {
      this.classList = res || [];
    });
  }
  // dropdown ห้องเรียน
  ddlClassroom() {
    const json = { classId: this.searchForm.value.classId };
    this.service.httpGet('/api/v1/0/classroom-list/ddlFilter2', json).then((res) => {
      this.roomList = res || [];
    });
  }


  // ค้นหา
  onSearch(e) {
    this.searchForm.value.p = (this.paginator.pageIndex = e.pageIndex || 0) + 1;
    this.service.httpGet('/api/v1/classRoom-member/rep001', this.searchForm.value).then((res: IResponse) => {
      this.dataSource.data = res.responseData || [];
      this.pageSize = res.responseSize || 0;
    });
  }

  // ยกเลิก
  onCancel() {
    this.searchForm.controls.yearGroupName.setValue(this.ddlYear());
    this.searchForm.controls.classId.reset('');
    this.searchForm.controls.classRoomId.reset('');
    this.onSearch(0);
  }

  onReport(classRoomMemberId, stuCode, classId, classRoomId) {
    this.yearGroupName = this.searchForm.value.yearGroupName;
    this.classId = this.searchForm.value.classId;
    this.classRoomId = this.searchForm.value.classRoomId;
    this.router.navigate(['/rep/rep001-report'],
      {
        queryParams: {
          yearGroupName: this.yearGroupName,
          classId,
          classRoomId,
          classRoomMemberId,
          stuCode,
        }
      });
  }

}
