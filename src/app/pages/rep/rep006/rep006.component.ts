import { Component, OnInit, ViewChild, Inject } from '@angular/core';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { MatPaginator, MatTableDataSource, MAT_DIALOG_DATA, MatDialog } from '@angular/material';
import { ApiService,IResponse,EHttp } from '../../../shared/service/api.service';
import { Router } from '@angular/router';
import Swal from 'sweetalert2/dist/sweetalert2.js';
import 'sweetalert2/src/sweetalert2.scss';


@Component({
  selector: 'app-rep006',
  templateUrl: './rep006.component.html',
  styleUrls: ['./rep006.component.scss']
})
export class Rep006Component implements OnInit {

  isProcess:boolean = false;
  searchForm = this.fb.group({
    'yearGroupId':['']
    , 'classId':['']
    , 'classRoomId':['']
    , 'p':['']
    , 'result':[10]
  });

  report = this.fb.group({
    'stuProfileId':['']
  });

  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  public arr_stu = [];
  pageSize:number;
  selected:String;
  

  displayedColumns: string[] = ['no','stuCode', 'fullname','classRoom','action'];
  dataSource = new MatTableDataSource();
  yearList:[];
  classList:[];
  roomList:[];

  constructor(
    private fb: FormBuilder,
    private service:ApiService,
    public dialog: MatDialog,
    private router: Router
  ) { }

  ngOnInit() {
    this.ddlClass();
    this.ddlYear();
    this.onSearch(0);
  }
//checkbox
checked: boolean = true;

changeValue(value) {
    this.checked = !value;
}

//dropdown ปีการศึกษา
ddlYear(){
  this.service.httpGet('/api/v1/0/year-group/ddl',null).then((res)=>{
    this.yearList = res||[];
  });
}
//dropdown ชั้นเรียน
ddlClass(){
  this.service.httpGet('/api/v1/0/class/ddlCatclass',null).then((res)=>{
    this.classList = res||[];
  });
}
//dropdown ห้องเรียน
ddlClassroom(){
  let json = {'classId': this.searchForm.value.classId};
  this.service.httpGet('/api/v1/0/classroom-list/ddlFilter2',json).then((res) => {
    this.roomList = res || [];
  });
}

//search ค้นหา
onSearch(e){
  this.searchForm.value.p = (this.paginator.pageIndex = e.pageIndex||0)+1;
  this.service.httpGet('/api/v1/stu-profile/rep006',this.searchForm.value).then((res:IResponse)=>{
    this.dataSource.data = res.responseData||[];
    this.pageSize = res.responseSize||0;
  });
}

//cancel ยกเลิก
onCancel(){
  this.searchForm.controls.yearGroupId.reset('');
  this.searchForm.controls.classId.reset('');
  this.searchForm.controls.classRoomId.reset('');
  this.onSearch(0);
}
//คลิกรีพอร์ท 1 2 3
onReport1(stuProfileId){
  this.isProcess = true;
  this.report.controls.stuProfileId.setValue(stuProfileId);
  this.service.httpPreviewPDF('/api/v1/rep006/report/rep006_1/pdf',this.report.value).then((res)=>{
    this.isProcess = false;
    window.location.href=res;
  });
};
onReport2(stuProfileId){
  this.isProcess = true;
  this.report.controls.stuProfileId.setValue(stuProfileId);
  this.service.httpPreviewPDF('/api/v1/rep006/report/rep006_2/pdf',this.report.value).then((res)=>{
    this.isProcess = false;
    window.location.href=res;
  });
};
onReport3(stuProfileId){
  this.isProcess = true;
  this.report.controls.stuProfileId.setValue(stuProfileId);
  this.service.httpPreviewPDF('/api/v1/rep006/report/rep006_3/pdf',this.report.value).then((res)=>{
    this.isProcess = false;
    window.location.href=res;
  });
};

}
