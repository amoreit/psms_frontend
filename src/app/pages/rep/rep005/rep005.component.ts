
import { Component, OnInit, ViewChild, Inject } from '@angular/core';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { MatPaginator, MatTableDataSource, MAT_DIALOG_DATA, MatDialog } from '@angular/material';
import { ApiService, IResponse, EHttp } from '../../../shared/service/api.service';
import { Router } from '@angular/router';
import 'sweetalert2/src/sweetalert2.scss';
import { TranslateService } from '@ngx-translate/core';
import { UtilService } from 'src/app/_util/util.service';

@Component({
  selector: 'app-rep005',
  templateUrl: './rep005.component.html', 
  styleUrls: ['./rep005.component.scss']
})
export class Rep005Component implements OnInit {

  isProcess = false;
  searchForm = this.fb.group({
    yearGroupId: [''],
    classId: [''],
    classRoomId: [''],
    stuCode: [''], 
    sendDate: [''],
    firstnameTh: [''],
    lastnameTh: [''],
    p: [''],
    result: [10]
  });

  report = this.fb.group({
    stuProfileId: ['']
    , sendDate: ['']
    , yearGroupId: ['']
  });

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  pageSize: number;
  selected: string;

  displayedColumns: string[] = ['no', 'stuCode', 'fullname', 'report1', 'report2'];
  dataSource = new MatTableDataSource();
  yearList: [];
  classList: [];
  roomList: [];
 
  animal: string;
  name: string;

  constructor(
    private translate: TranslateService,
    private fb: FormBuilder,
    private service: ApiService,
    public dialog: MatDialog,
    private router: Router,
    private utilService: UtilService) {
  }

  ngOnInit() {
    this.searchForm.controls.sendDate.setValue(this.utilService.formatDateForSQL(new Date()));
    this.onSearch(0);
    this.ddlClassroom();
    this.ddlClass();
    this.ddlYear();
  }

  // dropdown ปีการศึกษา
  ddlYear() {
    this.service.httpGet('/api/v1/0/year-group/ddl', null).then((res) => {
      this.yearList = res || [];
      this.searchForm.controls.yearGroupId.setValue(res[0].yearGroupId);
    });
  }

  // dropdown ชั้นเรียน
  ddlClass() {
    // let json ={'class':this.searchForm.value.class};
    this.service.httpGet('/api/v1/0/class/ddlCatclass', null).then((res) => {
      this.classList = res || [];
    });
  }

  // dropdown ห้องเรียน
  ddlClassroom() {
    const json = { classId: this.searchForm.value.classId };
    this.service.httpGet('/api/v1/0/classroom-list/ddlFilterClassRoom', json).then((res) => {
      this.roomList = res || [];
    });
  }

  // ค้นหา
  onSearch(e) {
    if (this.searchForm.value.yearGroupId != '') {
      if (this.searchForm.value.sendDate == '') {
        this.searchForm.controls.sendDate.setValue(this.utilService.formatDateForSQL(new Date()));
        this.searchForm.value.p = (this.paginator.pageIndex = e.pageIndex || 0) + 1;
        this.service.httpGet('/api/v1/stu-profile/cer002', this.searchForm.value).then((res: IResponse) => {
          this.dataSource.data = res.responseData || [];
          this.pageSize = res.responseSize || 0;
        });
      } else if (this.searchForm.value.sendDate != '') {
        this.searchForm.controls.sendDate.setValue(this.utilService.formatDateForSQL(new Date(this.searchForm.value.sendDate)));
        this.searchForm.value.p = (this.paginator.pageIndex = e.pageIndex || 0) + 1;
        this.service.httpGet('/api/v1/stu-profile/cer002', this.searchForm.value).then((res: IResponse) => {
          this.dataSource.data = res.responseData || [];
          this.pageSize = res.responseSize || 0;
        });
      }
    } else { }

  }

  // ยกเลิก
  onCancel() {
    this.searchForm = this.fb.group({
      yearGroupId: [this.ddlYear()],
      classId: [''],
      classRoomId: [''],
      stuCode: [''],
      sendDate: [''],
      firstnameTh: [''],
      lastnameTh: [''], 
    });

    this.onSearch(0); 
  } 

  // คลิกรีพอร์ท 1 2
  onExportDoc1(stuProfileId) {
    this.isProcess = true;
    this.report.controls.stuProfileId.setValue(stuProfileId);
    this.report.controls.sendDate.setValue(this.utilService.transformThai(this.searchForm.value.sendDate, ''));
    this.report.controls.yearGroupId.setValue(this.searchForm.value.yearGroupId);
    this.service.httpReportDOCX('/api/v1/cer002/report/cer002/docx', 'ใบรับรองการศึกษา', this.report.value).then((res) => {
      this.isProcess = false;
    });
  }
  onExportDoc2(stuProfileId) {
    this.isProcess = true;
    this.report.controls.sendDate.setValue(this.utilService.transformEng(this.searchForm.value.sendDate, ''));
    this.report.controls.stuProfileId.setValue(stuProfileId);
    this.report.controls.yearGroupId.setValue(this.searchForm.value.yearGroupId);
    this.service.httpReportDOCX('/api/v1/cer002/report/cer002_2/docx', 'Certificate of education', this.report.value).then((res) => {
      this.isProcess = false;
    });
  }


}
