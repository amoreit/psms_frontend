import { classRoom } from './../../aca/aca004-sub01/aca004-sub01-form/aca004-sub01-form.component';
import { MatDialogModule, MatDialog } from '@angular/material/dialog';
import { Component, OnInit, ViewChild, Inject } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { MatPaginator, MatTableDataSource } from '@angular/material';
import { ApiService, IResponse } from '../../../shared/service/api.service';
import { Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-rep002',
  templateUrl: './rep002.component.html',
  styleUrls: ['./rep002.component.scss']
})
export class Rep002Component implements OnInit {

  isProcess = false;
  searchForm = this.fb.group({ 
    yearGroupName: [''],
    classId: [''],
    classRoomId: [''],
    chooseLang: [''], 
    p: [''],
    result: [10]
  });

  report = this.fb.group({
    yearGroupName: [''],
    stuCode: [''],
    fullname: [''],
    classRoom: ['']
  });

  pageSize: number;
  displayedColumns: string[] = ['no','classRoomNo','stuCode','firstNameTh','classRoom'];
  dataSource = new MatTableDataSource();
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;

  yearList: [];
  classList: [];
  classRoomList: [];
  classRoomMemberId: [];
  checkClass: any;
  chechchooseTerm = false;
  chooseTerm: string;
  checkLangCode: string;
  subyear: string;
  sub: string;
  language: string;

  headerSelected:Boolean;
  isSelected:any;
  checklist:any;
  selectlist = false;
  allChecklist:any;

  constructor(
    private translate: TranslateService,
    private fb: FormBuilder,
    private service: ApiService,
    private router: Router
  ) { }

  ngOnInit() { 
    this.chooseTerm = '1';
    this.ddlClass();
    this.ddlClassRoom();
    this.ddlYear();
  }

  onSearch(e) {
    this.chechchooseTerm = true;
    this.searchForm.value.p = (this.paginator.pageIndex = e.pageIndex || 0) + 1;
    this.service.httpGet('/api/v1/classRoom-member/rep002', this.searchForm.value).then((res: IResponse) => {
        this.dataSource.data = res.responseData || [];
        this.checklist = res.responseData || [];
        this.pageSize = res.responseSize || 0;
        this.checkClass = res.responseData; 
    });
  }

  checkAll() {
    for (var i = 0; i < this.checklist.length; i++) {
      this.checklist[i].isSelected = this.headerSelected;
    }
    this.getCheckedItemList();
  } 
 
  isAllSelected() {
    this.checklist.every(function(item:any) {
        return item.isSelected === true;
    })
    this.headerSelected = false;
    this.getCheckedItemList();
  }

  getCheckedItemList(){
    this.allChecklist = [];
    for (var i = 0; i < this.checklist.length; i++) {
      if(this.checklist[i].isSelected){ 
        let itemData = {
          'stuCode':this.checklist[i].stu_code,
          'fullname':this.checklist[i].fullname
        };
        this.allChecklist.push(itemData); 
      }
    } 
    if(this.allChecklist.length == 0){
      this.selectlist = false; 
    }else{
      this.selectlist = true;
    }
  }
 
  onReportPDF() {
    this.subyear = this.searchForm.value.yearGroupName;
    this.sub = this.subyear.substring(0, 1);
    this.language = this.searchForm.value.chooseLang;
    
    let stuCode = [];
    let fullname = [];
    for(var i = 0; i < this.allChecklist.length; i++){
      stuCode.push('\''+this.allChecklist[i].stuCode+'\'');
      fullname.push('\''+this.allChecklist[i].fullname+'\'');
    }
    this.report.controls.stuCode.setValue(stuCode);
    this.report.controls.fullname.setValue(fullname);
    this.report.controls.yearGroupName.setValue(this.searchForm.value.yearGroupName);
    this.report.controls.classRoom.setValue(this.searchForm.value.classRoom);

    if (this.sub == '1') {
      if (this.language == '' || this.language == '1') {
          if (this.chooseTerm == '1') {
            this.service.httpPreviewPDF('/api/v1/rep002/report/rec001/pdf', this.report.value).then(res => {
                this.isProcess = false;
                window.location.href = res;
            });
          }else if(this.chooseTerm == '2') {
            this.service.httpPreviewPDF('/api/v1/rep002/report/rec002/pdf', this.report.value).then(res => {
                this.isProcess = false;
                window.location.href = res;
            });
          }else{ 
            this.service.httpPreviewPDF('/api/v1/rep002/report/rec005/pdf', this.report.value).then(res => {
                this.isProcess = false;
                window.location.href = res;
            });
          }  
      }else if(this.language == '2') {
          if(this.chooseTerm == '1') {
            this.service.httpPreviewPDF('/api/v1/rep002/report/rec006/pdf', this.report.value).then(res => {
                this.isProcess = false;
                window.location.href = res;
              });
          }else if(this.chooseTerm == '2') {
            this.service.httpPreviewPDF('/api/v1/rep002/report/rec007/pdf', this.report.value).then(res => {
                this.isProcess = false;
                window.location.href = res;
              });
          }else{
            this.service.httpPreviewPDF('/api/v1/rep002/report/rec010/pdf', this.report.value).then(res => {
                this.isProcess = false;
                window.location.href = res;
            });
          }
      }
    }else{
      if(this.language == '' || this.language == '1') {
          if(this.chooseTerm == '1') {
            this.service.httpPreviewPDF('/api/v1/rep002/report/rec001/pdf', this.report.value).then(res => {
              this.isProcess = false;
                window.location.href = res;
              });
          }else if(this.chooseTerm == '2') {
            this.service.httpPreviewPDF('/api/v1/rep002/report/rec004/pdf', this.report.value).then(res => {
                this.isProcess = false;
                window.location.href = res;
            });
          }else{
            this.service.httpPreviewPDF('/api/v1/rep002/report/rec005/pdf', this.report.value).then(res => {
                this.isProcess = false;
                window.location.href = res;
            });
          }
      }else if(this.language == '2') {
          if(this.chooseTerm == '1') {
            this.service.httpPreviewPDF('/api/v1/rep002/report/rec006/pdf', this.report.value).then(res => {
                this.isProcess = false;
                window.location.href = res;
            });
          }else if(this.chooseTerm == '2') {
            this.service.httpPreviewPDF('/api/v1/rep002/report/rec009/pdf', this.report.value).then(res => {
                this.isProcess = false;
                window.location.href = res;
            });
          }else{
            this.service.httpPreviewPDF('/api/v1/rep002/report/rec010/pdf', this.report.value).then(res => {
                this.isProcess = false;
                window.location.href = res;
            });
          }
      }
    }
  }
 
  onReportXLS() {
    this.subyear = this.searchForm.value.yearGroupName;
    this.sub = this.subyear.substring(0, 1);
    this.language = this.searchForm.value.chooseLang;
    
    let stuCode = [];
    let fullname = [];
    for(var i = 0; i < this.allChecklist.length; i++){
      stuCode.push('\''+this.allChecklist[i].stuCode+'\'');
      fullname.push('\''+this.allChecklist[i].fullname+'\'');
    }
    this.report.controls.stuCode.setValue(stuCode);
    this.report.controls.fullname.setValue(fullname);
    this.report.controls.yearGroupName.setValue(this.searchForm.value.yearGroupName);
    this.report.controls.classRoom.setValue(this.searchForm.value.classRoom);

    if (this.sub == '1') {
      if (this.language == '' || this.language == '1') {
          if (this.chooseTerm == '1') {
            this.service.httpReportXLS('/api/v1/rep002/report/rec001/xls',"รายงานกลางภาค",this.report.value).then((res)=>{
              this.isProcess = false;
            });
          }else if(this.chooseTerm == '2') {
            this.service.httpReportXLS('/api/v1/rep002/report/rec002/xls',"รายงานปลายภาค",this.report.value).then((res)=>{
              this.isProcess = false;
            });
          }else{
            this.service.httpReportXLS('/api/v1/rep002/report/rec005/xls',"รายงาน 2 ภาคเรียน",this.report.value).then((res)=>{
              this.isProcess = false;
            });
          }
      }else if(this.language == '2') {
          if(this.chooseTerm == '1') {
            this.service.httpReportXLS('/api/v1/rep002/report/rec006/xls',"Midterm report",this.report.value).then((res)=>{
              this.isProcess = false;
            });
          }else if(this.chooseTerm == '2') {
            this.service.httpReportXLS('/api/v1/rep002/report/rec007/xls',"Final report",this.report.value).then((res)=>{
              this.isProcess = false;
            });
          }else{
            this.service.httpReportXLS('/api/v1/rep002/report/rec010/xls',"All term report",this.report.value).then((res)=>{
              this.isProcess = false;
            });
          }
      }
    }else{
      if(this.language == '' || this.language == '1') {
          if(this.chooseTerm == '1') {
            this.service.httpReportXLS('/api/v1/rep002/report/rec001/xls',"รายงานกลางภาค",this.report.value).then((res)=>{
              this.isProcess = false;
            });
          }else if(this.chooseTerm == '2') {
            this.service.httpReportXLS('/api/v1/rep002/report/rec004/xls',"รายงานปลายภาค",this.report.value).then((res)=>{
              this.isProcess = false;
            });
          }else{
            this.service.httpReportXLS('/api/v1/rep002/report/rec005/xls',"รายงาน 2 ภาคเรียน",this.report.value).then((res)=>{
              this.isProcess = false;
            });
          }
      }else if(this.language == '2') {
          if(this.chooseTerm == '1') {
            this.service.httpReportXLS('/api/v1/rep002/report/rec006/xls',"Midterm report",this.report.value).then((res)=>{
              this.isProcess = false;
            });
          }else if(this.chooseTerm == '2') {
            this.service.httpReportXLS('/api/v1/rep002/report/rec009/xls',"Final report",this.report.value).then((res)=>{
              this.isProcess = false;
            });
          }else{
            this.service.httpReportXLS('/api/v1/rep002/report/rec010/xls',"All term report",this.report.value).then((res)=>{
              this.isProcess = false;
            });
          }
      }
    }
  }
 
  onCancel() {
    this.searchForm.controls.classId.setValue(''); 
    this.searchForm.controls.classRoomId.setValue('');
    this.onSearch(0);
    this.chechchooseTerm = false;
  }

  /* DROP DOWN ปีการศึกษา */
  ddlYear() {
    this.service.httpGet('/api/v1/0/year-group/ddl', null).then(res => {
      this.yearList = res || [];
      this.searchForm.controls.yearGroupName.setValue(res[0].name);
    });
  }

  /* DROP DOWN ชั้นเรียน */
  ddlClass() {
    this.service.httpGet('/api/v1/0/class/ddlCatclass', null).then(res => {
      this.classList = res || [];
    });
  }

  /* DROP DOWN ห้องเรียน */
  ddlClassRoom() {
    const json = { classId: this.searchForm.value.classId };
    this.service.httpGet('/api/v1/0/classroom-list/ddlFilter', json).then(res => {
      this.classRoomList = res || [];
    });
  }

}
