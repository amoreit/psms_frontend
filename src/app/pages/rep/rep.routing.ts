import { Routes } from '@angular/router';
import { AuthGuard } from '../../shared/service/auth.guard';

/*** Page ***/
import { Rep001Component } from './rep001/rep001.component';
import { Rep002Component } from './rep002/rep002.component';
import { Rep004Component } from './rep004/rep004.component';
import { Rep005Component } from './rep005/rep005.component';
import { Rep006Component } from './rep006/rep006.component';
import { Rep001ReportComponent } from './rep001-report/rep001-report.component';


export const RepRoutes: Routes = [
  {
    path:'', 
    children:[
      {path: 'rep001',component: Rep001Component,canActivate:[AuthGuard]},
      {path: 'rep002',component: Rep002Component,canActivate:[AuthGuard]},
      {path: 'rep004',component: Rep004Component,canActivate:[AuthGuard]},
      {path: 'rep005',component: Rep005Component,canActivate:[AuthGuard]},
      {path: 'rep006',component: Rep006Component,canActivate:[AuthGuard]},
      {path: 'rep001-report', component: Rep001ReportComponent,canActivate:[AuthGuard]},
    ]
  }
];