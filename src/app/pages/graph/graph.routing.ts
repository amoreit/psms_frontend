import { Routes } from '@angular/router';
import { AuthGuard } from '../../shared/service/auth.guard';

/*** Page ***/
import { Graph001Component } from './graph001/graph001.component';
import { Graph002Component } from './graph002/graph002.component';
import { Graph003Component } from './graph003/graph003.component';
import { Graph004Component } from './graph004/graph004.component';
import { Graph005Component } from './graph005/graph005.component';
import { Graph006Component } from './graph006/graph006.component';
import { Graph007Component } from './graph007/graph007.component';
import { Graph008Component } from './graph008/graph008.component';
import { Graph009Component } from './graph009/graph009.component';
import { Graph010Component } from './graph010/graph010.component';

export const GraphRoutes: Routes = [
  {
    path:'', 
    children:[
      {path: 'graph001',component: Graph001Component,canActivate:[AuthGuard]},
      {path: 'graph002',component: Graph002Component,canActivate:[AuthGuard]},
      {path: 'graph003',component: Graph003Component,canActivate:[AuthGuard]},
      {path: 'graph004',component: Graph004Component,canActivate:[AuthGuard]},
      {path: 'graph005',component: Graph005Component,canActivate:[AuthGuard]},
      {path: 'graph006',component: Graph006Component,canActivate:[AuthGuard]},
      {path: 'graph007',component: Graph007Component,canActivate:[AuthGuard]},
      {path: 'graph008',component: Graph008Component,canActivate:[AuthGuard]},
      {path: 'graph009',component: Graph009Component,canActivate:[AuthGuard]},
      {path: 'graph010',component: Graph010Component,canActivate:[AuthGuard]},
    ]
  }
];