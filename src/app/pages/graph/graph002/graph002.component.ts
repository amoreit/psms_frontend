import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder } from '@angular/forms';
import { ApiService, IResponse } from 'src/app/shared/service/api.service';
import { MatDialog, MatTableDataSource, MatPaginator } from '@angular/material';

@Component({
  selector: 'app-graph002',
  templateUrl: './graph002.component.html',
  styleUrls: ['./graph002.component.scss']
})
export class Graph002Component implements OnInit {

  elements: any = [
    { id: 1, first: 'Mark', last: 'Otto', handle: '@mdo' },
    { id: 2, first: 'Jacob', last: 'Thornton', handle: '@fat' },
    { id: 3, first: 'Larry', last: 'the Bird', handle: '@twitter' },
  ];

  headElements = ['ID', 'First', 'Last', 'Handle'];

  title = 'Angular Charts';
  view: any[] = [1000, 500];
  // options for the chart
  showXAxis = true;
  yScaleMax = "10";
  maxYAxisTickLength = 10;
  showYAxis = true;
  gradient = false;
  showXAxisLabel = true;
  showYAxisLabel = true;
  barPadding = 50;

  showLegend = false;
  legendTitle = 'เพศ';
  xAxisLabel = 'ระดับชั้น';
  yAxisLabel = 'คะแนนเต็ม';
  timeline = true;
  colorScheme = {
    domain: ['#9370DB', '#87CEFA', '#FA8072', '#FF7F50', '#90EE90', '#9370DB']
  };
  //pie
  showLabels = true;
  // data goes here
  public data = [
    {
      "name": "ป.1",
      "value": 8.35

    },
    {
      "name": "ป.2",
      "value": 8.57

    },
    {
      "name": "ป.3",
      "value": 7.29

    },
    {
      "name": "ป.4",
      "value": 9.03

    },
    {
      "name": "ป.5",
      "value": 8.67
    },
    {
      "name": "ป.6",
      "value": 9.13
    }
  ];

  constructor(
    private router: Router
  ) { }

  ngOnInit() {
  }

  printComponent(cmpName) {
    let printContents = document.getElementById(cmpName).innerHTML;
    let originalContents = document.body.innerHTML;
    document.body.innerHTML = printContents;
    window.print();
    document.body.innerHTML = originalContents;
    location.reload();
  }

  onCancel(){}

}
