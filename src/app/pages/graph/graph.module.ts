import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FlexLayoutModule } from '@angular/flex-layout';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { NgxChartsModule } from '@swimlane/ngx-charts';

/*** Material ***/
import {
  MatExpansionModule,
  MatButtonModule,
  MatIconModule,
  MatInputModule,
  MatTableModule,
  MatCardModule,
  MatFormFieldModule,
  MatPaginatorModule,
  MatDialogModule,
  MatSlideToggleModule,
  MatTabsModule,
} from '@angular/material';

/*** Routing ***/
import { GraphRoutes } from './graph.routing';

/*** Page ***/
import { SharedModule } from 'src/app/shared/shared.module';
import { Graph001Component } from './graph001/graph001.component';
import { Graph002Component } from './graph002/graph002.component';
import { Graph003Component } from './graph003/graph003.component';
import { Graph004Component } from './graph004/graph004.component';
import { Graph005Component } from './graph005/graph005.component';
import { Graph006Component } from './graph006/graph006.component';
import { Graph007Component } from './graph007/graph007.component';
import { Graph008Component } from './graph008/graph008.component';
import { Graph009Component } from './graph009/graph009.component';
import { Graph010Component } from './graph010/graph010.component';

@NgModule({
  imports: [
    CommonModule,
    FlexLayoutModule,
    RouterModule.forChild(GraphRoutes),
    MatExpansionModule,
    MatButtonModule,
    MatIconModule,
    MatInputModule,
    MatTableModule,
    MatDatepickerModule,
    MatCardModule,
    MatFormFieldModule,
    ReactiveFormsModule,
    MatPaginatorModule,
    MatDialogModule,
    MatSlideToggleModule,
    MatTabsModule,
    CommonModule,
    NgxChartsModule,
    FormsModule,
    ReactiveFormsModule,
    SharedModule,
  ],
  providers: [],
  exports: [RouterModule],
  declarations: [
    Graph001Component,
    Graph002Component,
    Graph003Component,
    Graph004Component,
    Graph005Component,
    Graph006Component,
    Graph007Component,
    Graph008Component,
    Graph009Component,
    Graph010Component
  ],
  entryComponents: [

  ]
})
export class GraphModule { }
