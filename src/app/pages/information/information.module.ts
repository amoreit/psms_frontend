import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FlexLayoutModule } from '@angular/flex-layout';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';  
import { MatDatepickerModule} from '@angular/material/datepicker';

/*** Material ***/
import {
  MatExpansionModule,
  MatButtonModule,
  MatIconModule,
  MatInputModule,
  MatTableModule, 
  MatCardModule,
  MatFormFieldModule,
  MatPaginatorModule,
  MatDialogModule,
  MatSlideToggleModule,
  MatTabsModule
} from '@angular/material';

/*** Routing ***/
import {InformationRoutes} from './information.routing';

/*** Page ***/
import { SharedModule } from 'src/app/shared/shared.module';
import { Info001Component } from './info001/info001.component';

@NgModule({
  imports: [ 
    CommonModule,
    FlexLayoutModule,
    RouterModule.forChild(InformationRoutes),
    MatExpansionModule,
    MatButtonModule,
    MatIconModule,
    MatInputModule,
    MatTableModule,
    MatDatepickerModule,
    MatCardModule,
    MatFormFieldModule,
    ReactiveFormsModule,
    MatPaginatorModule,
    MatDialogModule,
    MatSlideToggleModule,
    MatTabsModule,
    CommonModule,

    FormsModule, 
    ReactiveFormsModule, 
    SharedModule,
  ],
    providers: [],
    exports:[RouterModule],
  declarations: [

  Info001Component],
  entryComponents:[

  ]
})
export class InformationModule { }
