import { Routes } from '@angular/router';
import { AuthGuard } from '../../shared/service/auth.guard';

/*** Page ***/
import { Info001Component } from './info001/info001.component';

export const InformationRoutes: Routes = [
  {
    path:'', 
    children:[
      {path: 'info001',component: Info001Component,canActivate:[AuthGuard]},
    ]
  }
];