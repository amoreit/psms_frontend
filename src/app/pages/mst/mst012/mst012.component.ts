import { Component, OnInit, ViewChild } from '@angular/core';
import { MatTableDataSource, MatPaginator, MatDialog } from '@angular/material';
import { FormBuilder } from '@angular/forms';
import { ApiService, IResponse } from 'src/app/shared/service/api.service';
import { Router } from '@angular/router';
import { UtilService } from 'src/app/_util/util.service';
import Swal from 'sweetalert2/dist/sweetalert2.js';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-mst012',
  templateUrl: './mst012.component.html',
  styleUrls: ['./mst012.component.scss']
})
export class Mst012Component implements OnInit {

  isProcess = false;
  searchForm = this.fb.group({
    'divisionNameTh': [''],
    'subDivisionNameTh': [''], 
    'subDivisionNameEn': [''],
    'reportType': [''],
    'p': [''],
    'result': [10] 
  });
 
  pageSize: number;
  displayedColumns: string[] = ['no', 'divisionNameTh', 'subDivisionNameTh', 'subDivisionNameEng', 'updatedUser', 'updatedDate', 'status'];
  dataSource = new MatTableDataSource();

  divisionList: [];

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  constructor(
    private translate: TranslateService,
    private fb: FormBuilder,
    private service: ApiService,
    public dialog: MatDialog,
    private router: Router,
    public util: UtilService
  ) { } 

  ngOnInit() {
    this.ddlDivision();
    this.onSearch(0);
  }

  onSearch(e) {
    this.isProcess = true;
    this.searchForm.value.p = (this.paginator.pageIndex = e.pageIndex || 0) + 1;
    this.service.httpGet('/api/v1/mst-subDivision', this.searchForm.value).then((res: IResponse) => {
      this.isProcess = false;
      this.dataSource.data = res.responseData || [];
      this.pageSize = res.responseSize || 0;
    });
  }

  onClear() {
    this.searchForm.controls.divisionNameTh.setValue('');
    this.searchForm.controls.subDivisionNameTh.setValue('');
    this.searchForm.controls.subDivisionNameEn.setValue('');
    this.onSearch(0);
  }

  ddlDivision() {
    this.service.httpGet('/api/v1/0/tbMstDivision/ddl', null).then((res) => {
      this.divisionList = res || [];
    });
  } 
 
  onForm(id) {
    this.router.navigate(['/mst/mst012/form'], { queryParams: { id } });
  }

  onReport() {
    if(this.searchForm.value.divisionNameTh == '' && this.searchForm.value.subDivisionNameTh == '' && this.searchForm.value.subDivisionNameEn == ''){
      this.searchForm.controls.reportType.setValue('0');
      this.service.httpPreviewPDF('/api/v1/mst012/report/pdf', this.searchForm.value).then((res) => {
        window.location.href = res;
      });
    }else if(this.searchForm.value.divisionNameTh != '' && this.searchForm.value.subDivisionNameTh == '' && this.searchForm.value.subDivisionNameEn == ''){
      this.searchForm.controls.reportType.setValue('1');
      this.service.httpPreviewPDF('/api/v1/mst012/report/pdf', this.searchForm.value).then((res) => {
        window.location.href = res;
      });
    }else if(this.searchForm.value.divisionNameTh == '' && this.searchForm.value.subDivisionNameTh != '' && this.searchForm.value.subDivisionNameEn == ''){
      this.searchForm.controls.reportType.setValue('2');
      this.service.httpPreviewPDF('/api/v1/mst012/report/pdf', this.searchForm.value).then((res) => {
        window.location.href = res;
      });
    }else if(this.searchForm.value.divisionNameTh == '' && this.searchForm.value.subDivisionNameTh == '' && this.searchForm.value.subDivisionNameEn != ''){
      this.searchForm.controls.reportType.setValue('3');
      this.service.httpPreviewPDF('/api/v1/mst012/report/pdf', this.searchForm.value).then((res) => {
        window.location.href = res;
      });
    }else if(this.searchForm.value.divisionNameTh == '' && this.searchForm.value.subDivisionNameTh != '' && this.searchForm.value.subDivisionNameEn != ''){
      this.searchForm.controls.reportType.setValue('4');
      this.service.httpPreviewPDF('/api/v1/mst012/report/pdf', this.searchForm.value).then((res) => {
        window.location.href = res;
      });
    }else if(this.searchForm.value.divisionNameTh != '' && this.searchForm.value.subDivisionNameTh == '' && this.searchForm.value.subDivisionNameEn != ''){
      this.searchForm.controls.reportType.setValue('5');
      this.service.httpPreviewPDF('/api/v1/mst012/report/pdf', this.searchForm.value).then((res) => {
        window.location.href = res;
      });
    }else if(this.searchForm.value.divisionNameTh != '' && this.searchForm.value.subDivisionNameTh != '' && this.searchForm.value.subDivisionNameEn == ''){
      this.searchForm.controls.reportType.setValue('6');
      this.service.httpPreviewPDF('/api/v1/mst012/report/pdf', this.searchForm.value).then((res) => {
        window.location.href = res;
      });
    }else{
      this.searchForm.controls.reportType.setValue('7');
      this.service.httpPreviewPDF('/api/v1/mst012/report/pdf', this.searchForm.value).then((res) => {
        window.location.href = res;
      });
    }
  }

  onExportPDF() {
    if(this.searchForm.value.divisionNameTh != '' && this.searchForm.value.subDivisionNameTh == '' && this.searchForm.value.subDivisionNameEn == ''){
      this.searchForm.controls.reportType.setValue('1');
      this.service.httpReportPDF('/api/v1/mst012/report/pdf', 'ข้อมูลฝ่าย', this.searchForm.value).then((res) => {
      });
    }else if(this.searchForm.value.divisionNameTh == '' && this.searchForm.value.subDivisionNameTh != '' && this.searchForm.value.subDivisionNameEn == ''){
      this.searchForm.controls.reportType.setValue('2');
      this.service.httpReportPDF('/api/v1/mst012/report/pdf', 'ข้อมูลฝ่าย', this.searchForm.value).then((res) => {
      });
    }else if(this.searchForm.value.divisionNameTh == '' && this.searchForm.value.subDivisionNameTh == '' && this.searchForm.value.subDivisionNameEn != ''){
      this.searchForm.controls.reportType.setValue('3');
      this.service.httpReportPDF('/api/v1/mst012/report/pdf', 'ข้อมูลฝ่าย', this.searchForm.value).then((res) => {
      });
    }else if(this.searchForm.value.divisionNameTh == '' && this.searchForm.value.subDivisionNameTh != '' && this.searchForm.value.subDivisionNameEn != ''){
      this.searchForm.controls.reportType.setValue('4');
      this.service.httpReportPDF('/api/v1/mst012/report/pdf', 'ข้อมูลฝ่าย', this.searchForm.value).then((res) => {
      });
    }else if(this.searchForm.value.divisionNameTh != '' && this.searchForm.value.subDivisionNameTh == '' && this.searchForm.value.subDivisionNameEn != ''){
      this.searchForm.controls.reportType.setValue('5');
      this.service.httpReportPDF('/api/v1/mst012/report/pdf', 'ข้อมูลฝ่าย', this.searchForm.value).then((res) => {
      });
    }else if(this.searchForm.value.divisionNameTh != '' && this.searchForm.value.subDivisionNameTh != '' && this.searchForm.value.subDivisionNameEn == ''){
      this.searchForm.controls.reportType.setValue('6');
      this.service.httpReportPDF('/api/v1/mst012/report/pdf', 'ข้อมูลฝ่าย', this.searchForm.value).then((res) => {
      });
    }else{
      this.searchForm.controls.reportType.setValue('7');
      this.service.httpReportPDF('/api/v1/mst012/report/pdf', 'ข้อมูลฝ่าย', this.searchForm.value).then((res) => {
      });
    }
  }

  onExportXls() {
    if(this.searchForm.value.divisionNameTh != '' && this.searchForm.value.subDivisionNameTh == '' && this.searchForm.value.subDivisionNameEn == ''){
      this.searchForm.controls.reportType.setValue('1');
      this.service.httpReportXLS('/api/v1/mst012/report/xls', 'ข้อมูลฝ่าย', this.searchForm.value).then((res) => {
      });
    }else if(this.searchForm.value.divisionNameTh == '' && this.searchForm.value.subDivisionNameTh != '' && this.searchForm.value.subDivisionNameEn == ''){
      this.searchForm.controls.reportType.setValue('2');
      this.service.httpReportXLS('/api/v1/mst012/report/xls', 'ข้อมูลฝ่าย', this.searchForm.value).then((res) => {
      });
    }else if(this.searchForm.value.divisionNameTh == '' && this.searchForm.value.subDivisionNameTh == '' && this.searchForm.value.subDivisionNameEn != ''){
      this.searchForm.controls.reportType.setValue('3');
      this.service.httpReportXLS('/api/v1/mst012/report/xls', 'ข้อมูลฝ่าย', this.searchForm.value).then((res) => {
      });
    }else if(this.searchForm.value.divisionNameTh == '' && this.searchForm.value.subDivisionNameTh != '' && this.searchForm.value.subDivisionNameEn != ''){
      this.searchForm.controls.reportType.setValue('4');
      this.service.httpReportXLS('/api/v1/mst012/report/xls', 'ข้อมูลฝ่าย', this.searchForm.value).then((res) => {
      });
    }else if(this.searchForm.value.divisionNameTh != '' && this.searchForm.value.subDivisionNameTh == '' && this.searchForm.value.subDivisionNameEn != ''){
      this.searchForm.controls.reportType.setValue('5');
      this.service.httpReportXLS('/api/v1/mst012/report/xls', 'ข้อมูลฝ่าย', this.searchForm.value).then((res) => {
      });
    }else if(this.searchForm.value.divisionNameTh != '' && this.searchForm.value.subDivisionNameTh != '' && this.searchForm.value.subDivisionNameEn == ''){
      this.searchForm.controls.reportType.setValue('6');
      this.service.httpReportXLS('/api/v1/mst012/report/xls', 'ข้อมูลฝ่าย', this.searchForm.value).then((res) => {
      });
    }else{
      this.searchForm.controls.reportType.setValue('7');
      this.service.httpReportXLS('/api/v1/mst012/report/xls', 'ข้อมูลฝ่าย', this.searchForm.value).then((res) => {
      });
    }
  }

  onDelete(e) {
    Swal.fire({
      title: this.translate.instant('alert.delete'),
      icon: 'info',
      showCancelButton: true,
      confirmButtonColor: '#2085d6',
      cancelButtonVolor: '#d33',
      confirmButtonText: this.translate.instant('psms.DL0008'),
      cancelButtonText: this.translate.instant('psms.DL0009')
    }).then((result) => {
      // tslint:disable-next-line:triple-equals
      if (result.dismiss == 'cancel') {
        return;
      }
      this.isProcess = true;
      this.service.httpDelete('/api/v1/mst-subDivision', { subDivisionId: e.sub_division_id }).then((res: IResponse) => {
        this.isProcess = false;
        // tslint:disable-next-line:triple-equals
        if ((res.responseCode || 500) != 200) {
          Swal.fire(
            this.translate.instant('message.delete_error'),
            res.responseMsg,
            'error'
          )
          return;
        }
        Swal.fire(
          this.translate.instant('alert.delete_header'),
          this.translate.instant('message.delete'),
          'success'
        );
        this.onSearch(0);
      });
    });
  }
}
