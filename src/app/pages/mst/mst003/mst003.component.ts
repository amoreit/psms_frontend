import { Component, OnInit, ViewChild } from '@angular/core';
import { MatTableDataSource, MatPaginator, MatDialog } from '@angular/material';
import { FormBuilder } from '@angular/forms';
import { ApiService, IResponse } from 'src/app/shared/service/api.service';
import { Router } from '@angular/router';

// sweetalert2
import Swal from 'sweetalert2/dist/sweetalert2.js';
import 'sweetalert2/src/sweetalert2.scss';
import { UtilService } from 'src/app/_util/util.service';
import { TranslateService } from '@ngx-translate/core';
// sweetalert2

@Component({
  selector: 'app-mst003',
  templateUrl: './mst003.component.html',
  styleUrls: ['./mst003.component.scss']
})
export class Mst003Component implements OnInit {

  isProcess: boolean = false;
  searchForm = this.fb.group({
    'departmentCode': ['']
    , 'lnameTh': ['']
    , 'name': ['']
    , 'p': ['']
    , 'result': [10] 
  });
  pageSize: number;
  displayedColumns: string[] = ['departmentCode', 'lnameTh', 'name', 'updatedUser', 'updatedDate', 'recordStatus'];
  dataSource = new MatTableDataSource();
  catClassList: [];

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  constructor(
    private fb: FormBuilder,
    private service: ApiService,
    public dialog: MatDialog,
    private router: Router,
    public util: UtilService,
    private translate: TranslateService, ) {
  }

  ngOnInit() {
    this.ddlTbMstCatClass();
    this.onSearch(0);
  }

  onSearch(e) {
    this.isProcess = true;
    this.searchForm.value.p = (this.paginator.pageIndex = e.pageIndex || 0) + 1;
    this.service.httpGet('/api/v1/department', this.searchForm.value).then((res: IResponse) => {
      this.dataSource.data = res.responseData || [];
      this.pageSize = res.responseSize || 0;
    });
  }

  onForm(id) {
    this.router.navigate(['/mst/mst003/form'], { queryParams: { id: id } });
  }

  onDelete(e) {
    Swal.fire({
      title: this.translate.instant('alert.delete'),
      icon: 'info',
      showCancelButton: true,
      confirmButtonColor: '#2085d6',
      cancelButtonVolor: '##DD3333',
      confirmButtonText: this.translate.instant('psms.DL0008'),
      cancelButtonText: this.translate.instant('psms.DL0009')
    }).then((result) => {
      if (result.dismiss == 'cancel') {
        return;
      }
      this.isProcess = true;
      this.service.httpDelete('/api/v1/department', { 'departmentId': e.departmentId }).then((res: IResponse) => {
        this.isProcess = false;
        if ((res.responseCode || 500) != 200) {
          alert(res.responseMsg);
          return;
        }
        Swal.fire(
          this.translate.instant('alert.delete_header'),
          this.translate.instant('message.delete'),
          'success'
        )
        this.onSearch(0);
      });
    });
  }

  ddlTbMstCatClass() {
    this.service.httpGet('/api/v1/0/tbMstCatClass/ddl', null).then((res) => {
      this.catClassList = res || [];
    });
  }

  onCancel() {
    this.searchForm.controls.departmentCode.setValue('');
    this.searchForm.controls.lnameTh.setValue('');
    this.searchForm.controls.name.setValue('');
    this.onSearch(0);
    this.ddlTbMstCatClass();
  }

  onReport() {
    this.isProcess = true;
    this.service.httpPreviewPDF('/api/v1/mst003/report/pdf', this.searchForm.value).then((res) => {
      this.isProcess = false;
      window.location.href = res;
    });
  }

  onReportPDF() {
    this.isProcess = true;
    this.service.httpReportPDF('/api/v1/mst003/report/pdf', "mst003", this.searchForm.value).then((res) => {
      this.isProcess = false;
    });
  }

  onReportXls() {
    this.isProcess = true;
    this.service.httpReportXLS('/api/v1/mst003/report/xls', "mst003", this.searchForm.value).then((res) => {
      this.isProcess = false;
    });
  }

}

