import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, Validators } from '@angular/forms';
import { ApiService, IResponse } from 'src/app/shared/service/api.service';

// sweetalert2
import Swal from 'sweetalert2/dist/sweetalert2.js';
import 'sweetalert2/src/sweetalert2.scss';
import { TranslateService } from '@ngx-translate/core';
import { UtilService } from 'src/app/_util/util.service';
// sweetalert2

@Component({
  selector: 'app-mst003-form',
  templateUrl: './mst003-form.component.html',
  styleUrls: ['./mst003-form.component.scss']
})
export class Mst003FormComponent implements OnInit {

  isProcess: boolean = false;
  form = this.fb.group({
    'departmentId': ['']
    , 'lnameTh': ['']
    , 'departmentCode': ['']
    , 'orders': ['']
    , 'catClass': ['', Validators.required]
    , 'scCode': ['']
    , 'name': ['', Validators.required]
    , 'nameShort': ['', Validators.required]
    , 'updatedUser': ['',]
    , 'updatedDate': ['',]
  });

  catClassList: [];
  modeType = '';
  result = '';
  id = '';
  updatedUser = '';
  updatedDate: Date;
  validateValue = false;
  headerType:any;

  constructor(private translate: TranslateService,private ar: ActivatedRoute, private fb: FormBuilder, private service: ApiService, private router: Router, public util: UtilService) { }

  ngOnInit() {
    this.ddlTbMstCatClass();
    this.id = this.ar.snapshot.queryParamMap.get('id') || '';
    if (this.id == '') { 
      this.modeType = 'เพิ่ม';  
      this.headerType = this.translate.instant('modeType.MT0007');
      this.translate.onLangChange.subscribe(() => {
        this.headerType = this.translate.instant('modeType.MT0007');
      });
    }
    else { 
      this.modeType = 'แก้ไข';  
      this.headerType = this.translate.instant('modeType.MT0008');
      this.translate.onLangChange.subscribe(() => { 
        this.headerType = this.translate.instant('modeType.MT0008');
      });
      this.onGetData();
    }
  }

  onBackPage() {
    this.router.navigate(['/mst/mst003']);
  }

  onGetData() {
    this.isProcess = true;
    this.service.httpGet('/api/v1/department/' + this.id, null).then((res: IResponse) => {
      this.isProcess = false;
      let data = res.responseData;
      this.form.controls.departmentId.setValue(data.departmentId || '');
      this.form.controls.lnameTh.setValue(data.lnameTh || '');
      this.form.controls.departmentCode.setValue(data.departmentCode || '');
      this.form.controls.catClass.setValue(data.cattclass.catClassId || '');
      this.form.controls.scCode.setValue(data.scCode || '');
      this.form.controls.name.setValue(data.name || '');
      this.form.controls.nameShort.setValue(data.nameShort || '');
      this.form.controls.orders.setValue(data.orders || '');
      this.updatedDate = data.updatedDate;
      this.form.controls.updatedUser.setValue(data.updatedUser || '');
    });
  }

  onSave() {
    if (this.form.invalid) {
      this.validateValue = true;
      Swal.fire(
        this.translate.instant('alert.error'),
        this.translate.instant('alert.validate'),
        'error'
      )
      return;
    } 
    Swal.fire({
      title: this.translate.instant('psms.DL0001'),
      icon: 'info',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      //cancelButtonColor: '', 
      confirmButtonText: this.translate.instant('psms.DL0008'),
      cancelButtonText: this.translate.instant('psms.DL0009')
    }).then((result) => {
      if (result.dismiss == 'cancel') {
        return;
      }
      this.isProcess = true;
      this.form.controls.orders.setValue(this.form.value.departmentCode);
      let json = this.form.value;
      if (json.departmentId == '') {
        this.service.httpPost('/api/v1/department', json).then((res: IResponse) => {
          this.isProcess = false;
          if ((res.responseCode || 500) != 200) {
            Swal.fire(
              this.translate.instant('message.add_error'),
              res.responseMsg,
              'error'
            )
            return;
          }

          Swal.fire(
            this.translate.instant('alert.add_header'),
            this.translate.instant('message.add'),
            'success',
          ).then(() => { this.onBackPage(); })

        });
        return;
      }
      this.service.httpPut('/api/v1/department', json).then((res: IResponse) => {
        this.isProcess = false;
        if ((res.responseCode || 500) != 200) {
          alert(res.responseMsg);
          return;
        }
        Swal.fire(
          this.translate.instant('psms.DL0006'),
          this.translate.instant('message.edit'),
          'success'
        ).then((result) => { this.onBackPage(); })
      });
    });
  }
 
  ddlTbMstCatClass() {
    this.service.httpGet('/api/v1/0/tbMstCatClass/ddl', null).then((res) => {
      this.catClassList = res || [];
    });
  }

  OnClear() {
    if (this.id == '') {
      this.form.controls.catClass.setValue('');
      this.form.controls.lnameTh.setValue('');
      this.form.controls.departmentCode.setValue('');
      this.form.controls.name.setValue(''); 
      this.form.controls.nameShort.setValue('');
    } else {
      this.onGetData();
    }
  }

}

