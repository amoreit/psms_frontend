import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder } from '@angular/forms';
import { ApiService, IResponse } from 'src/app/shared/service/api.service';
import { MatDialog, MatTableDataSource, MatPaginator } from '@angular/material';
import Swal from 'sweetalert2/dist/sweetalert2.js';
import 'sweetalert2/src/sweetalert2.scss';
import { UtilService } from 'src/app/_util/util.service';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-mst015',
  templateUrl: './mst015.component.html',
  styleUrls: ['./mst015.component.scss']
})
export class Mst015Component implements OnInit {

  isProcess: boolean = false;
  searchForm = this.fb.group({
    'activityGroup': ['']
    , 'p': [''] 
    , 'result': [10]
  });

  pageSize: number;
  displayedColumns: string[] = ['activityGroup', 'updatedUser', 'updatedDate', 'activityGroupStatus'];
  dataSource = new MatTableDataSource();

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  constructor(
    private translate: TranslateService,
    private fb: FormBuilder,
    private service: ApiService,
    public dialog: MatDialog,
    private router: Router,
    public _util: UtilService) {

  }

  ngOnInit() {
    this.onSearch(0);
  }

  onSearch(e) {
    this.searchForm.value.p = (this.paginator.pageIndex = e.pageIndex || 0) + 1;
    this.service.httpGet('/api/v1/activity-group', this.searchForm.value).then((res: IResponse) => {
      this.dataSource.data = res.responseData || [];
      this.pageSize = res.responseSize || 0;
    });
  }

  onDelete(e) {
    Swal.fire({
      title: this.translate.instant('alert.delete'),
      icon: 'info',
      showCancelButton: true,
      confirmButtonColor: '#2085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: this.translate.instant('psms.DL0008'),
      cancelButtonText: this.translate.instant('psms.DL0009')
    }).then((result) => {
      if (result.dismiss == 'cancel') {
        return;
      }
      this.isProcess = true;
      this.service.httpDelete('/api/v1/activity-group', { 'activityGroupId': e.activityGroupId }).then((res: IResponse) => {
        this.isProcess = false;
        if ((res.responseCode || 500) != 200) {
          Swal.fire(
            this.translate.instant('message.delete_error'),
            res.responseMsg,
            'error'
          )
          return;
        }
        Swal.fire(
          this.translate.instant('alert.delete_header'),
          this.translate.instant('message.delete'),
          'success'
        )
        this.onSearch(0);
      });
    });
  }

  onCancel() {
    this.searchForm = this.fb.group({
      'activityGroup': ['']
      , 'p': ['']
      , 'result': [10]
    });
    this.onSearch(0);

  }

  onForm(id) {
    this.router.navigate(['/mst/mst015/form'], { queryParams: { id: id } });
  }


  onPreviewPDF() {
    this.isProcess = true;
    this.service.httpPreviewPDF('/api/v1/activity-group/report/pdf', this.searchForm.value).then((res) => {
      this.isProcess = false;
      window.location.href = res;
    });
  }

  onExportPDF() {
    this.isProcess = true;
    this.service.httpReportPDF('/api/v1/activity-group/report/pdf', "ข้อมูลกลุ่มกิจกรรม", this.searchForm.value).then((res) => {
      this.isProcess = false;
    });
  }

  onExportExcel() {
    this.isProcess = true;
    this.service.httpReportXLS('/api/v1/activity-group/report/xls', "ข้อมูลกลุ่มกิจกรรม", this.searchForm.value).then((res) => {
      this.isProcess = false;
    });
  }


}
