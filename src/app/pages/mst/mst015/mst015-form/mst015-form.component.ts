import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, Validators } from '@angular/forms';
import { ApiService, IResponse } from 'src/app/shared/service/api.service';
import Swal from 'sweetalert2/dist/sweetalert2.js';
import 'sweetalert2/src/sweetalert2.scss';
import { UtilService } from 'src/app/_util/util.service';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-mst015-form',
  templateUrl: './mst015-form.component.html',
  styleUrls: ['./mst015-form.component.scss']
})
export class Mst015FormComponent implements OnInit {

  isProcess: boolean = false;

  form = this.fb.group({
    'activityGroupId': ['']
    , 'catClassId': ['']
    , 'activityGroup': ['', Validators.required]
    , 'scCode': ['']
  });

  mode = ''; 
  id = '';
  updatedDate: any;
  updatedDateTime: any; 
  updatedUser: any;
  catclassList: [];
  validateValue = false;
  headerType = '';

  constructor(
    private translate: TranslateService,
    private ar: ActivatedRoute,
    private fb: FormBuilder,
    private service: ApiService,
    private router: Router,
    public _util: UtilService) {
  }

  ngOnInit() {
    this.ddlCatClass();
    this.id = this.ar.snapshot.queryParamMap.get('id') || '';
    if (this.id == '') {
      this.mode = 'เพิ่มข้อมูล';
      this.headerType = this.translate.instant('modeType.MT0007');
      this.translate.onLangChange.subscribe(() => {
        this.headerType = this.translate.instant('modeType.MT0007');
      });
    } else {
      this.mode = 'แก้ไขข้อมูล';
      this.headerType = this.translate.instant('modeType.MT0008');
      this.translate.onLangChange.subscribe(() => {
        this.headerType = this.translate.instant('modeType.MT0008');
      });
      this.loadData();
    }
  }

  loadData() {
    this.isProcess = true;
    this.service.httpGet('/api/v1/activity-group/' + this.id, null).then((res: IResponse) => {
      this.isProcess = false;
      let data = res.responseData;

      this.form.controls.activityGroupId.setValue(data.activityGroupId || '')
      this.form.controls.activityGroup.setValue(data.activityGroup || '');
      this.form.controls.catClassId.setValue(data.catClassId || '');

      this.updatedUser = data.updatedUser || '';
      this.updatedDateTime = data.updatedDate || '';
      this.updatedDate = this._util.formatDateSQLToString(data.updatedDate || '', true);
    });
  }

  onSave() {
    if (this.form.invalid) {
      this.validateValue = true;
      Swal.fire(
        this.translate.instant('alert.error'),
        this.translate.instant('alert.validate'),
        'error'
      )
      return;
    }
    Swal.fire({
      title: this.translate.instant('psms.DL0001'),
      icon: 'info',
      showCancelButton: true,
      confirmButtonColor: '#2085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: this.translate.instant('psms.DL0008'),
      cancelButtonText: this.translate.instant('psms.DL0009')
    }).then((result) => {
      if (result.dismiss == 'cancel') {
        return;
      }
      this.form.value.scCode = 'PRAMANDA';
      let json = this.form.value;
      if (json.activityGroupId == '') {
        this.service.httpPost('/api/v1/activity-group', json).then((res: IResponse) => {
          if ((res.responseCode || 500) != 200) {
            Swal.fire(
              this.translate.instant('message.add_error'),
              res.responseMsg,
              'error'
            );
            return;
          }
          Swal.fire(
            this.translate.instant('alert.add_header'),
            this.translate.instant('message.add'),
            'success'
          ).then(() => {
            this.onBack();
          })
        });
        return;
      }
      this.service.httpPut('/api/v1/activity-group', json).then((res: IResponse) => {
        if ((res.responseCode || 500) != 200) {
          Swal.fire(
            this.translate.instant('message.edit_error'),
            res.responseMsg,
            'error'
          )
          return;
        }
        Swal.fire(
          this.translate.instant('psms.DL0006'),
          this.translate.instant('message.edit'),
          'success'
        ).then(() => {
          this.onBack();
        })
      });
    });
  }

  ddlCatClass() {
    this.service.httpGet('/api/v1/0/catclass/ddl', null).then((res) => {
      this.catclassList = res || [];
    });
  }

  onBack() {
    this.router.navigate(['mst/mst015']);
  }

  onCancel() {
    if (this.id != '') {
      this.loadData();
    } else {
      this.form.controls.activityGroupId.setValue('')
      this.form.controls.catClassId.setValue('')
      this.form.controls.activityGroup.setValue('')
      this.form.controls.scCode.setValue('')
    }
  }

}
