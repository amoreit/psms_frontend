import { Component, OnInit, ViewChild, Inject } from '@angular/core';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { MatPaginator, MatTableDataSource, MAT_DIALOG_DATA, MatDialog } from '@angular/material';
import { ApiService,IResponse,EHttp } from '../../../shared/service/api.service';
import { Router } from '@angular/router';
import { UtilService } from 'src/app/_util/util.service';
import Swal from 'sweetalert2/dist/sweetalert2.js';
import 'sweetalert2/src/sweetalert2.scss';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-mst005',
  templateUrl: './mst005.component.html',
  styleUrls: ['./mst005.component.scss']
})
export class Mst005Component implements OnInit {

  isProcess:boolean = false;
  searchForm = this.fb.group({
    'lnameTh':['']
    , 'p':['']
    , 'result':[10] 
  }); 
  pageSize:number;
  displayedColumns: string[] = ['classNameTh', 'updatedUser', 'updatedDate','classStatus'];
  dataSource = new MatTableDataSource();

  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  constructor(
    private fb: FormBuilder,
    private service:ApiService,
    public dialog: MatDialog,
    private router: Router,
    public utilService: UtilService,
    private translate: TranslateService,
    ) {
    
  }

  ngOnInit() {
    this.onSearch(0);
  }

  onSearch(e){
    this.searchForm.value.p = (this.paginator.pageIndex = e.pageIndex||0)+1;
    this.service.httpGet('/api/v1/catClass',this.searchForm.value).then((res:IResponse)=>{
      this.dataSource.data = res.responseData||[];
      this.pageSize = res.responseSize||0;
    });
  }

  onCancel(){
    this.searchForm.controls.lnameTh.setValue('');
    this.onSearch(0);
  }

  onDelete(e){
    Swal.fire({
      title: this.translate.instant('alert.delete'),
      icon: 'info',
      showCancelButton: true,
      confirmButtonColor: '#3085d6', 
      cancelButtonColor: '#DD3333',
      confirmButtonText: this.translate.instant('psms.DL0008'),
      cancelButtonText: this.translate.instant('psms.DL0009') 
    }).then((result) => {
      if(result.dismiss == 'cancel'){
          return;
      }
      this.isProcess = true;
      this.service.httpDelete('/api/v1/catClass',{'catClassId':e.catClassId}).then((res:IResponse)=>{
        this.isProcess = false;
        if((res.responseCode||500)!=200){
          Swal.fire(
            this.translate.instant('message.delete_error'),
            res.responseMsg,
            'error'
          )
          return;
        }
        Swal.fire(
          this.translate.instant('alert.delete_header'),
          this.translate.instant('message.delete'),
          'success'
        )
        this.onSearch(0);
      });
    });
  }

  onForm(id){
    this.router.navigate(['/mst/mst005/form'],{ queryParams: {id:id}});
  }

  onReport(){
    this.isProcess = true;
    this.service.httpPreviewPDF('/api/v1/mst005/report/pdf',this.searchForm.value).then((res)=>{
      this.isProcess = false;
      window.location.href = res;
    });
  }

  onReportPDF(){
    this.isProcess = true;
    this.service.httpReportPDF('/api/v1/mst005/report/pdf',"mst005",this.searchForm.value).then((res)=>{
      this.isProcess = false;
    });
  }

  onReportXls(){
    this.isProcess = true;
    this.service.httpReportXLS('/api/v1/mst005/report/xls',"mst005",this.searchForm.value).then((res)=>{
      this.isProcess = false;
    });
  }
}
