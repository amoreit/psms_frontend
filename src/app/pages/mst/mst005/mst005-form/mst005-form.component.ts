import { Component, OnInit, ViewChild, Inject } from '@angular/core';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { MatPaginator, MatTableDataSource, MAT_DIALOG_DATA, MatDialog } from '@angular/material';
import { ApiService, IResponse, EHttp } from '../../../../shared/service/api.service';
import { ActivatedRoute, Router } from '@angular/router';
import { UtilService } from 'src/app/_util/util.service';
import Swal from 'sweetalert2/dist/sweetalert2.js';
import 'sweetalert2/src/sweetalert2.scss';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-mst005-form',
  templateUrl: './mst005-form.component.html',
  styleUrls: ['./mst005-form.component.scss']
})
export class Mst005FormComponent implements OnInit {

  isProcess: boolean = false;
  form = this.fb.group({ 
    'catClassId': ['']
    , 'lnameTh': ['', Validators.required]
    , 'updatedUser': ['']
    , 'updatedDate': ['']
    , 'scCode': ['']
  }); 

  updatedUser = '';
  updatedDate = Date;
  Status = '';
  id = '';
  validateValue = false;
  headerType = '';

  constructor(private translate: TranslateService,ar: ActivatedRoute, private fb: FormBuilder, private service: ApiService, private router: Router, public utilService: UtilService) {
    this.id = ar.snapshot.queryParamMap.get('id') || '';
    if (this.id == '') {
      this.Status = 'เพิ่มข้อมูล';
      this.headerType = this.translate.instant('modeType.MT0007');
      this.translate.onLangChange.subscribe(() => {
        this.headerType = this.translate.instant('modeType.MT0007');
        });
    }
    else {
      this.Status = 'แก้ไขข้อมูล';
      this.headerType = this.translate.instant('modeType.MT0008');
      this.translate.onLangChange.subscribe(() => {
        this.headerType = this.translate.instant('modeType.MT0008');
        });
      this.loadData();
    }
  }

  ngOnInit() { }

  loadData() {
    this.isProcess = true;
    this.service.httpGet('/api/v1/catClass/' + this.id, null).then((res: IResponse) => {
      this.isProcess = false;
      let data = res.responseData;
      this.form.controls.catClassId.setValue(data.catClassId || '');
      this.form.controls.lnameTh.setValue(data.lnameTh || '');
      this.form.controls.updatedUser.setValue(data.updatedUser || '');
      this.updatedUser = data.updatedUser;
      this.updatedDate = data.updatedDate;
    });
  }

  onCancel() {
    if (this.id != '') {
      this.loadData();
    } else {
      this.form.controls.catClassId.setValue('');
      this.form.controls.lnameTh.setValue('');
      this.form.controls.lnameEn.setValue('');
      this.form.controls.updatedUser.setValue('');
      this.form.controls.updatedDate.setValue('');
      this.form.controls.scCode.setValue('');
    }
  }

  backPage() {
    this.router.navigate(['/mst/mst005']);
  }

  onSave() {
    if (this.form.invalid) {
      this.validateValue = true;
      Swal.fire(
        this.translate.instant('alert.error'),
        this.translate.instant('alert.validate'),
        'error'
      )
      return;
    }
    Swal.fire({
      title: this.translate.instant('psms.DL0001'),
      icon: 'info',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#DD3333',
      confirmButtonText: this.translate.instant('psms.DL0008'),
      cancelButtonText: this.translate.instant('psms.DL0009')
    }).then((result) => {
      if (result.dismiss == 'cancel') {
        return;
      }
      this.form.value.scCode = "PRAMANDA";
      this.isProcess = true;
      let json = this.form.value;
      if (json.catClassId == '') {
        this.service.httpPost('/api/v1/catClass', json).then((res: IResponse) => {
          this.isProcess = false;
          if ((res.responseCode || 500) != 200) {
            Swal.fire(
              this.translate.instant('message.add_error'),
              res.responseMsg,
              'error'
            );
            return;
          }
          Swal.fire(
            this.translate.instant('alert.add_header'),
            this.translate.instant('message.add'),
            'success'
          ).then(() => { this.backPage(); })
        });
        return;
      }
      this.service.httpPut('/api/v1/catClass', json).then((res: IResponse) => {
        this.isProcess = false;
        if ((res.responseCode || 500) != 200) {
          Swal.fire(
            this.translate.instant('message.edit_error'),
            res.responseMsg,
            'error'
          )
          return;
        }
        Swal.fire(
          this.translate.instant('psms.DL0006'),
          this.translate.instant('message.edit'),
          'success'
        ).then(() => { this.backPage(); })
      });
    });
  }

}
