import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, Validators } from '@angular/forms';
import { ApiService, IResponse } from 'src/app/shared/service/api.service';
import Swal from 'sweetalert2/dist/sweetalert2.js';
import 'sweetalert2/src/sweetalert2.scss';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-mst009-form',
  templateUrl: './mst009-form.component.html',
  styleUrls: ['./mst009-form.component.scss']
})
export class Mst009FormComponent implements OnInit {

  isProcess: boolean = false;
  paramForm = this.fb.group({
    'positionId': ['']
    , 'scCode': ['']
    , 'positionNameTh': ['', Validators.required]
    , 'positionNameEn': ['', Validators.required]

  });
  id = '';
  positionId = '';
  scCode: any;
  positionNameTh: any;
  positionNameEn: any;
  Head = ''; 
  validateValue = false;
  headerType = '';

  constructor(
    private translate: TranslateService,
    private fb: FormBuilder,
    private service: ApiService,
    private router: Router,
    private ar: ActivatedRoute,
  ) {
    this.id = ar.snapshot.queryParamMap.get('id') || '';
    if (this.id == '') {
      this.Head = 'เพิ่มข้อมูลตำแหน่งงาน';
      this.headerType = this.translate.instant('modeType.MT0007');
      this.translate.onLangChange.subscribe(() => {
        this.headerType = this.translate.instant('modeType.MT0007');
      });
    } else {
      this.Head = 'แก้ไขข้อมูลตำแหน่งงาน';
      this.headerType = this.translate.instant('modeType.MT0008');
      this.translate.onLangChange.subscribe(() => {
        this.headerType = this.translate.instant('modeType.MT0008');
      });
    }

  }

  ngOnInit() {
    this.positionNameTh = this.ar.snapshot.queryParamMap.get('nth') || '';
    this.positionNameEn = this.ar.snapshot.queryParamMap.get('nen') || '';
    this.scCode = this.ar.snapshot.queryParamMap.get('sc') || '';
    this.positionId = this.ar.snapshot.queryParamMap.get('id') || '';
    this.paramForm.controls.positionNameTh.setValue(this.positionNameTh);
    this.paramForm.controls.positionNameEn.setValue(this.positionNameEn);
    this.paramForm.controls.scCode.setValue(this.scCode);
    this.paramForm.controls.positionId.setValue(this.positionId);
  }

  onSave() {
    if (this.paramForm.invalid) {
      this.validateValue = true;
      Swal.fire(
        this.translate.instant('alert.error'),
        this.translate.instant('alert.validate'),
        'error'
      );
      return;

    }
    Swal.fire({
      title: this.translate.instant('psms.DL0001'),
      icon: 'info',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: this.translate.instant('psms.DL0008'),
      cancelButtonText: this.translate.instant('psms.DL0009')
    }).then((result) => {

      if (result.dismiss == 'cancel') {
        return;
      }
      this.isProcess = true;
      this.paramForm.controls.scCode.setValue("PRAMANDA")
      let json = this.paramForm.value;
      if (json.positionId == '') {
        this.service.httpPost('/api/v1/position', json).then((res: IResponse) => {
          this.isProcess = false;
          if ((res.responseCode || 500) != 200) {
            Swal.fire(
              this.translate.instant('message.add_error'),
              res.responseMsg,
              'error'
            )
            return;
          }
          Swal.fire( 
            this.translate.instant('alert.add_header'),
            this.translate.instant('message.add'),
            'success'
          )
        });
      } else {
        this.service.httpPut('/api/v1/position/update', json).then((res: IResponse) => {
          this.isProcess = true;
          if ((res.responseCode || 500) != 200) {
            Swal.fire(
              this.translate.instant('message.edit_error'),
              res.responseMsg,
              'error'
            )
            return;
          }
          Swal.fire(
            this.translate.instant('psms.DL0006'),
            this.translate.instant('message.edit'),
            'success'
          ).then(() => { this.onBack(); })

        });
      }
    });

  }

  close() {
    this.onBack();
  }

  onBack() {
    this.router.navigate(['/mst/mst009']);
  }

  onCancel() {
    if (this.id == ''){
      this.paramForm.controls.positionNameTh.setValue('');
      this.paramForm.controls.positionNameEn.setValue('');
    }else{
      this.ngOnInit();
    }
  }

}
