import { Component, OnInit, ViewChild, Inject } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { MatPaginator, MatTableDataSource } from '@angular/material';
import { ApiService, IResponse } from '../../../shared/service/api.service';
import { Router } from '@angular/router';
import Swal from 'sweetalert2/dist/sweetalert2.js';
import 'sweetalert2/src/sweetalert2.scss';
import { TranslateService } from '@ngx-translate/core';
@Component({
  selector: 'app-mst009',
  templateUrl: './mst009.component.html',
  styleUrls: ['./mst009.component.scss']
})
export class Mst009Component implements OnInit {

  isProcess: boolean = false;
  searchForm = this.fb.group({
    'positionId': ['']
    , 'positionNameTh': ['']
    , 'positionNameEn': ['']
    , 'p': ['']
    , 'result': ['']
  });
 
  pageSize: number;
  displayedColumns: string[] = ['no', 'positionNameTh', 'positionNameEn', 'createdUser', 'createdDate', 'Status'];
  dataSource = new MatTableDataSource();

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  constructor(
    private translate: TranslateService,
    private fb: FormBuilder,
    private service: ApiService,
    private router: Router,
  ) { }

  ngOnInit() {
    this.onSearch(0);
  }

  onForm(code, nth, nen, sc, id) {
    this.router.navigate(['/mst/mst009/form'], { queryParams: { nth: nth, nen: nen, sc: sc, id: id } });
  }


  clickForm() {
    this.router.navigate(['/mst/mst009/form']);
  }

  onDelete(e) {
    Swal.fire({
      title: this.translate.instant('alert.delete'),
      icon: 'info',
      showCancelButton: true,
      confirmButtonColor: '#2085d6',
      cancelButtonVolor: '#d33',
      confirmButtonText: this.translate.instant('psms.DL0008'),
      cancelButtonText: this.translate.instant('psms.DL0009')
    }).then((result) => {
      if (result.dismiss == 'cancel') {
        return;
      }
      this.isProcess = true;
      this.service.httpDelete('/api/v1/position/delete', { 'positionId': e.positionId }).then((res: IResponse) => {
        this.isProcess = false;
        if ((res.responseCode || 500) != 200) {
          alert(res.responseMsg);
          Swal.fire(
            this.translate.instant('message.delete_error'),
            res.responseMsg,
            'error'
          )
          return;
        }
        Swal.fire(
          this.translate.instant('alert.delete_header'),
          this.translate.instant('message.delete'),
          'success'
        )
        this.onSearch(0);
      });
    });
  }

  onSearch(e) {
    this.searchForm.value.p = (this.paginator.pageIndex = e.pageIndex || 0) + 1;
    this.service.httpGet('/api/v1/position', this.searchForm.value).then((res: IResponse) => {
      this.dataSource.data = res.responseData || [];
      this.pageSize = res.responseSize || 0;
    });
  }

  toLocaleDateString() {
    var date = new Date(Date.UTC(2012, 11, 20, 3, 0, 0));
    var options = { weekday: 'long', year: 'numeric', month: 'long', day: 'numeric' };

  }

  formatDateSQLToString(dateStr: string, formatBC: boolean, splitter: string = '/') {
    if (dateStr != null && dateStr != '') {
      dateStr = dateStr.substring(0, 10);
      if (dateStr.length == 10) {
        var arr = dateStr.split('-');
        if (arr.length == 3 && arr[0].length == 4 && arr[1].length == 2 && arr[2].length == 2) {
          var year = parseInt(arr[0]);
          if (!isNaN(parseInt(arr[2])) && !isNaN(parseInt(arr[1])) && !isNaN(year)) {
            if (formatBC) {
              if (year < 2362) {
                year += 543;
              }
            } else {
              if (year > 2219) {
                year -= 543;
              }
            }
            return this.twoDigit(arr[2]) + splitter + this.twoDigit(arr[1]) + splitter + year;
          }
        }
      }
    }

    return '';
  }

  twoDigit(digit: any) {
    digit = parseInt(digit);
    return digit < 10 ? '0' + digit : new String(digit);
  }

  onCancel() {
    this.searchForm.controls.positionId.setValue('');
    this.searchForm.controls.positionNameTh.setValue('');
    this.searchForm.controls.positionNameEn.setValue('');
    this.ngOnInit();
  }



}
