import { Component, OnInit, ViewChild } from '@angular/core';
import { MatTableDataSource, MatPaginator, MatDialog } from '@angular/material';
import { FormBuilder } from '@angular/forms';
import { ApiService, IResponse } from 'src/app/shared/service/api.service';
import { Router } from '@angular/router';
import { UtilService } from 'src/app/_util/util.service';
import Swal from 'sweetalert2/dist/sweetalert2.js';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-mst011',
  templateUrl: './mst011.component.html',
  styleUrls: ['./mst011.component.scss']
})
export class Mst011Component implements OnInit {

  isProcess = false;
  searchForm = this.fb.group({
    divisionNameTh: ['']
    , divisionNameEn: [''] 
    , p: ['']
    , result: [10]
  }); 

  pageSize: number;
  displayedColumns: string[] = ['no', 'nameTh', 'nameEng', 'updatedUser', 'updatedDate', 'status'];
  dataSource = new MatTableDataSource();


  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  constructor(
    private translate: TranslateService,
    private fb: FormBuilder,
    private service: ApiService,
    public dialog: MatDialog,
    private router: Router,
    public util: UtilService
  ) { }

  ngOnInit() {
    this.onSearch(0);
  }

  onSearch(e) {
    this.isProcess = true;
    this.searchForm.value.p = (this.paginator.pageIndex = e.pageIndex || 0) + 1;
    this.service.httpGet('/api/v1/mst-division', this.searchForm.value).then((res: IResponse) => {
      this.isProcess = false;
      this.dataSource.data = res.responseData || [];
      this.pageSize = res.responseSize || 0;
    });
  }

  onClear() {
    this.searchForm.controls.divisionNameTh.setValue('');
    this.searchForm.controls.divisionNameEn.setValue('');
    this.onSearch(0);
  }

  onForm(id) {
    this.router.navigate(['/mst/mst011/form'], { queryParams: { id } });
  }

  onReport() {
    this.isProcess = true;
    this.service.httpPreviewPDF('/api/v1/mst011/report/pdf', this.searchForm.value).then((res) => {
      this.isProcess = false;
      window.location.href = res;
    });
  }

  onExportPDF() {
    this.isProcess = true;
    this.service.httpReportPDF('/api/v1/mst011/report/pdf', 'ข้อมูลฝ่าย', this.searchForm.value).then((res) => {
      this.isProcess = false;
    });
  }

  onExportXls() {
    this.isProcess = true;
    this.service.httpReportXLS('/api/v1/mst011/report/xls', 'ข้อมูลฝ่าย', this.searchForm.value).then((res) => {
      this.isProcess = false;
    });
  }

  onDelete(e) {
    Swal.fire({
      title: this.translate.instant('alert.delete'),
      icon: 'info',
      showCancelButton: true,
      confirmButtonColor: '#2085d6',
      cancelButtonVolor: '#d33',
      confirmButtonText: this.translate.instant('psms.DL0008'),
      cancelButtonText: this.translate.instant('psms.DL0009')
    }).then((result) => {
      // tslint:disable-next-line:triple-equals
      if (result.dismiss == 'cancel') {
        return;
      }
      this.isProcess = true;
      this.service.httpDelete('/api/v1/mst-division', { divisionId: e.divisionId }).then((res: IResponse) => {
        this.isProcess = false;
        // tslint:disable-next-line:triple-equals
        if ((res.responseCode || 500) != 200) {
          Swal.fire(
            this.translate.instant('message.delete_error'),
            res.responseMsg,
            'error'
          )
          return;
        }
        Swal.fire(
          this.translate.instant('alert.delete_header'),
          this.translate.instant('message.delete'),
          'success'
        );
        this.onSearch(0);
      });
    });
  }
}

