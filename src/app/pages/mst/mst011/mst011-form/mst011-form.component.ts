import { Component, OnInit } from '@angular/core';
import { Validators, FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { ApiService, IResponse } from 'src/app/shared/service/api.service';
import { UtilService } from 'src/app/_util/util.service';
import Swal from 'sweetalert2/dist/sweetalert2.js';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-mst011-form',
  templateUrl: './mst011-form.component.html',
  styleUrls: ['./mst011-form.component.scss']
})
export class Mst011FormComponent implements OnInit {


  isProcess = false;
  form = this.fb.group({ 
    divisionId: ['']
    , scCode: ['']
    , divisionNameTh: ['', Validators.required]
    , divisionNameEn: ['', Validators.required]
  }); 

  updatedUser = '';
  updatedDate: Date;
  modeType = '';
  id: any;
  patternThai = /^[ก-๏\s]+$/;
  patternEng = /^[a-zA-Z\s]+$/;
  validateValue = false;
  headerType = '';

  constructor(
    private translate: TranslateService,
    ar: ActivatedRoute,
    private fb: FormBuilder,
    private service: ApiService,
    private router: Router,
    public util: UtilService
  ) {
    this.isProcess = true;
    this.id = ar.snapshot.queryParamMap.get('id') || '';
    /* **/
    // tslint:disable-next-line:triple-equals
    if (this.id == '') { 
      this.modeType = 'เพิ่ม'; 
      this.headerType = this.translate.instant('modeType.MT0007');
      this.translate.onLangChange.subscribe(() => {
        this.headerType = this.translate.instant('modeType.MT0007');
        });
    } else { 
      this.modeType = 'แก้ไข'; 
      this.headerType = this.translate.instant('modeType.MT0008');
      this.translate.onLangChange.subscribe(() => {
        this.headerType = this.translate.instant('modeType.MT0008');
        });
    }
    /* **/
    this.loadData();
  }

  ngOnInit(
  ) { }

  loadData() {
    this.service.httpGet('/api/v1/mst-division/' + this.id, null).then((res: IResponse) => {
      this.isProcess = false;
      const data = res.responseData;
      this.form.controls.divisionId.setValue(data.divisionId || '');
      this.form.controls.divisionNameTh.setValue(data.divisionNameTh || '');
      this.form.controls.divisionNameEn.setValue(data.divisionNameEn || '');
      this.updatedUser = localStorage.getItem('userName') || '';
      this.updatedDate = data.updatedDate;
    });
  }

  onClear() {
    this.form.controls.divisionNameTh.setValue('');
    this.form.controls.divisionNameEn.setValue('');
  }

  onBackPage() {
    this.router.navigate(['/mst/mst011']);
  }

  onSave() {

    // tslint:disable-next-line:triple-equals
    if (this.form.invalid) {
      this.validateValue = true;
      Swal.fire(
        this.translate.instant('alert.error'),
        this.translate.instant('alert.validate'),
        'error'
      );
      return;
    } else if (!this.form.value.divisionNameTh.match(this.patternThai) || !this.form.value.divisionNameEn.match(this.patternEng)) {
      Swal.fire(
        this.translate.instant('alert.error'),
        this.translate.instant('alert.validate'),
        'error'
      );
      return;
    }
    Swal.fire({
      title: this.translate.instant('psms.DL0001'),
      icon: 'info',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: this.translate.instant('psms.DL0008'),
      cancelButtonText: this.translate.instant('psms.DL0009')
    }).then((result) => {
      // tslint:disable-next-line:triple-equals
      if (result.dismiss == 'cancel') {
        return;
      }
      this.isProcess = true;
      const json = this.form.value;
      // tslint:disable-next-line:triple-equals
      if (json.divisionId == '') {
        this.service.httpPost('/api/v1/mst-division', json).then((res: IResponse) => {
          this.isProcess = false;
          // tslint:disable-next-line:triple-equals
          if ((res.responseCode || 500) != 200) {
            Swal.fire(
              this.translate.instant('message.add_error'),
              res.responseMsg,
              'error'
            );
            return;
          }
          Swal.fire(
            this.translate.instant('alert.add_header'),
            this.translate.instant('message.add'),
            'success'
          ).then(() => { this.onBackPage(); });
        });
        return;
      }
      this.service.httpPut('/api/v1/mst-division', json).then((res: IResponse) => {
        this.isProcess = false;
        // tslint:disable-next-line:triple-equals
        if ((res.responseCode || 500) != 200) {
          Swal.fire(
            this.translate.instant('message.edit_error'),
            res.responseMsg,
            'error'
          )
          return;
        }
        Swal.fire(
          this.translate.instant('psms.DL0006'),
          this.translate.instant('message.edit'),
          'success'
        ).then(() => { this.onBackPage(); });
      });
    });
  }

}
