import { Component, OnInit, ViewChild, Inject } from '@angular/core';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { MatPaginator, MatTableDataSource, MAT_DIALOG_DATA, MatDialog } from '@angular/material';
import { ApiService,IResponse,EHttp } from '../../../shared/service/api.service';
import { Router } from '@angular/router';
import Swal from 'sweetalert2/dist/sweetalert2.js';
import { TranslateService } from '@ngx-translate/core';
import 'sweetalert2/src/sweetalert2.scss';
import { UtilService } from 'src/app/_util/util.service';

@Component({ 
  selector: 'app-mst002',
  templateUrl: './mst002.component.html',
  styleUrls: ['./mst002.component.scss']
})
export class Mst002Component implements OnInit {  
 
  isProcess:boolean = false;
  searchForm = this.fb.group({
      'subjectTypeCode':['']
      , 'name':['']
      , 'p':[''] 
      , 'result':[10]
  }); 
 
  pageSize:number; 
  displayedColumns: string[] = ['subjectTypeCode', 'name', 'updatedUser', 'updatedDate', 'recordStatus'];
  dataSource = new MatTableDataSource();
  subjectTypeList:[];

  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  constructor(
      private translate: TranslateService,
      private fb: FormBuilder,
      private service:ApiService,
      public dialog: MatDialog,
      private router: Router,
      public util: UtilService  
  ) {}

  ngOnInit() { 
    this.ddlTbMstSujectType(); 
    this.onSearch(0);  
  } 

  onSearch(e){ 
    this.isProcess = true;
    this.searchForm.value.p = (this.paginator.pageIndex = e.pageIndex||0)+1;
    this.service.httpGet('/api/v1/tbMstSubjectType',this.searchForm.value).then((res:IResponse)=>{
        this.isProcess = false;
        this.dataSource.data = res.responseData||[];
        this.pageSize = res.responseSize||0;
    });
  }
 
  onClear(){
    this.searchForm.controls.subjectTypeCode.setValue('');
    this.searchForm.controls.name.setValue('');
    this.onSearch(0);  
  } 

  ddlTbMstSujectType(){
    this.service.httpGet('/api/v1/0/tbMstSubjectType/ddl',null).then((res)=>{
      this.subjectTypeList = res||[];
    }); 
  } 

  onForm(id){
    this.router.navigate(['/mst/mst002/form'],{ queryParams: {id:id}});
  }

  onReport(){
    this.isProcess = true;
    this.service.httpPreviewPDF('/api/v1/mst002/report/pdf',this.searchForm.value).then((res)=>{
      this.isProcess = false;
      window.location.href = res;
    });
  }

  onReportPDF(){
    this.isProcess = true;
    this.service.httpReportPDF('/api/v1/mst002/report/pdf',"ข้อมูลประเภทวิชา",this.searchForm.value).then((res)=>{
      this.isProcess = false;
    });
  }

  onReportXls(){
    this.isProcess = true;
    this.service.httpReportXLS('/api/v1/mst002/report/xls',"ข้อมูลประเภทวิชา",this.searchForm.value).then((res)=>{
      this.isProcess = false;
    });
  }

  onDelete(e){
      Swal.fire({
          title: this.translate.instant('alert.delete'),
          icon: 'info',
          showCancelButton: true,
          confirmButtonColor: '#2085d6',
          cancelButtonVolor: '##DD3333',
          confirmButtonText: this.translate.instant('psms.DL0008'),
          cancelButtonText: this.translate.instant('psms.DL0009')
      }).then((result) => {
        if(result.dismiss == 'cancel'){
            return;
        }
        this.isProcess = true;
        this.service.httpDelete('/api/v1/tbMstSubjectType',{'subjectTypeId':e.subjectTypeId}).then((res:IResponse)=>{
          this.isProcess = false;
          if((res.responseCode||500)!=200){
              alert(res.responseMsg);
              return;
          } Swal.fire(
              this.translate.instant('alert.delete_header'),
              this.translate.instant('message.delete'),
              'success'
          )
          this.onSearch(0);
        });
      });
  }
}
