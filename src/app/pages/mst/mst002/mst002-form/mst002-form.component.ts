import { Component, OnInit } from '@angular/core';
import { Validators, FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { ApiService, IResponse } from 'src/app/shared/service/api.service';

import { TranslateService } from '@ngx-translate/core';
import Swal from 'sweetalert2/dist/sweetalert2.js';
import 'sweetalert2/src/sweetalert2.scss';
import { UtilService } from 'src/app/_util/util.service';

@Component({
  selector: 'app-mst002-form',
  templateUrl: './mst002-form.component.html',
  styleUrls: ['./mst002-form.component.scss']
})
export class Mst002FormComponent implements OnInit {

  isProcess: boolean = false;
  form = this.fb.group({
    'subjectTypeId': ['']
    , 'subjectTypeCode': ['', Validators.required]
    , 'scCode': ['']
    , 'name': ['', Validators.required]
  });

  updatedUser = '';
  updatedDate: Date;
  modeType = '';
  id: any;
  validateValue = false;
  headerType = '';

  constructor(
    private translate: TranslateService,
    ar: ActivatedRoute,
    private fb: FormBuilder,
    private service: ApiService,
    private router: Router,
    public util: UtilService
  ) {
    this.isProcess = true;
    this.id = ar.snapshot.queryParamMap.get('id') || '';
    if (this.id == '') { 
      this.modeType = 'เพิ่ม'; 
      this.headerType = this.translate.instant('modeType.MT0007');
      this.translate.onLangChange.subscribe(() => {
        this.headerType = this.translate.instant('modeType.MT0007');
      });
    }
    else { 
      this.modeType = 'แก้ไข'; 
      this.headerType = this.translate.instant('modeType.MT0008');
      this.translate.onLangChange.subscribe(() => {
        this.headerType = this.translate.instant('modeType.MT0008');
      });
    }
    this.loadData();
  }

  ngOnInit() { }

  loadData() {
    this.service.httpGet('/api/v1/tbMstSubjectType/' + this.id, null).then((res: IResponse) => {
      this.isProcess = false;
      let data = res.responseData;
      this.form.controls.subjectTypeId.setValue(data.subjectTypeId || '');
      this.form.controls.subjectTypeCode.setValue(data.subjectTypeCode || '');
      this.form.controls.name.setValue(data.name || '');
      this.updatedUser = localStorage.getItem('userName') || '';
      this.updatedDate = data.updatedDate;
    });
  }

  onClear() {
    this.form.controls.name.setValue('');
    this.form.controls.subjectTypeCode.setValue(''); 
  }

  onBackPage() {
    this.router.navigate(['/mst/mst002']);
  }

  onSave() {
    if (this.form.invalid) {
      this.validateValue = true;
      Swal.fire(
        this.translate.instant('alert.error'),
        this.translate.instant('alert.validate'),
        'error'
      )
      return;
    }
    Swal.fire({
      title: this.translate.instant('psms.DL0001'),
      icon: 'info',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      //cancelButtonColor: '##DD3333',
      confirmButtonText: this.translate.instant('psms.DL0008'),
      cancelButtonText: this.translate.instant('psms.DL0009')
    }).then((result) => {
      if (result.dismiss == 'cancel') {
        return;
      }
      this.isProcess = true;
      let json = this.form.value;
      if (json.subjectTypeId == '') {
        this.service.httpPost('/api/v1/tbMstSubjectType', json).then((res: IResponse) => {
          this.isProcess = false;
          if ((res.responseCode || 500) != 200) {
            Swal.fire(
              this.translate.instant('message.add_error'),
              res.responseMsg,
              'error'
            ) 
            return;
          }
          Swal.fire(
            this.translate.instant('alert.add_header'),
            this.translate.instant('message.add'), 
            'success'
          ).then(() => { this.onBackPage(); })
        });
        return;
      }
      this.service.httpPut('/api/v1/tbMstSubjectType', json).then((res: IResponse) => {
        this.isProcess = false;
        if ((res.responseCode || 500) != 200) {
          alert(res.responseMsg);
          return;
        }
        Swal.fire(
          this.translate.instant('psms.DL0006'),
          this.translate.instant('message.edit'),
          'success'
        ).then(() => { this.onBackPage(); })
      });
    });
  }

}
