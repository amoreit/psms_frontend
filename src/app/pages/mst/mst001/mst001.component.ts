import { Component, OnInit, ViewChild, Inject } from '@angular/core';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { MatPaginator, MatTableDataSource, MAT_DIALOG_DATA, MatDialog } from '@angular/material';
import { ApiService, IResponse, EHttp } from '../../../shared/service/api.service';
import { Router } from '@angular/router';
import { UtilService } from 'src/app/_util/util.service';
import Swal from 'sweetalert2/dist/sweetalert2.js';
import { TranslateService } from '@ngx-translate/core';

import 'sweetalert2/src/sweetalert2.scss';
import { from } from 'rxjs';

@Component({
  selector: 'app-mst001',
  templateUrl: './mst001.component.html',
  styleUrls: ['./mst001.component.scss']
})
export class Mst001Component implements OnInit {

  isProcess: boolean = false;
  searchForm = this.fb.group({
    // 'name':[''] 
    'yearCode': ['']
    , 'p': ['']
    , 'result': [10]
  });
  pageSize: number;
  displayedColumns: string[] = ['year', 'firstday', 'lastday', 'yearCurrent', 'updatedUser', 'updatedDate', 'yearStatus'];
  dataSource = new MatTableDataSource();
  yearList: [];

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  constructor(
    private translate: TranslateService,
    private fb: FormBuilder,
    private service: ApiService,
    public dialog: MatDialog,
    private router: Router,
    public utilService: UtilService
  ) {}

  ngOnInit() {
    this.ddlYear();
    this.onSearch(0);
  }

  onSearch(e) {
    this.searchForm.value.p = (this.paginator.pageIndex = e.pageIndex || 0) + 1;
    this.service.httpGet('/api/v1/yeargroup', this.searchForm.value).then((res: IResponse) => {
      this.dataSource.data = res.responseData || [];
      this.pageSize = res.responseSize || 0;
    });
  }

  ddlYear() {
    this.service.httpGet('/api/v1/0/year/ddl', null).then((res) => {
      this.yearList = res || [];
    });
  }

  onCancel() {
    this.searchForm.controls.yearCode.setValue('');
    this.onSearch(0);
  }


  onDelete(e) {
    Swal.fire({
      title: this.translate.instant('alert.delete'), 
      icon: 'info',
      showCancelButton: true,
      allowOutsideClick: false,
      confirmButtonColor: '#2085d6',
      cancelButtonVolor: '##DD3333',
      confirmButtonText: this.translate.instant('psms.DL0008'),
      cancelButtonText: this.translate.instant('psms.DL0009')
    }).then((result) => {
      if (result.dismiss == 'cancel') {
        return;
      } 
      this.isProcess = true;
      this.service.httpDelete('/api/v1/yeargroup', { 'yearGroupId': e.yearGroupId }).then((res: IResponse) => {
        this.isProcess = false;
        if ((res.responseCode || 500) != 200) {
          alert(res.responseMsg);
          return;
        }
        Swal.fire(
          this.translate.instant('alert.delete_header'),
          this.translate.instant('message.delete'),
          'success'
        )
        this.onSearch(0);
      });
    });
  }

  onForm(id) {
    this.router.navigate(['/mst/mst001/form'], { queryParams: { id: id } });
  }

  onReport() {
    this.isProcess = true;
    this.service.httpPreviewPDF('/api/v1/mst001/report/pdf', this.searchForm.value).then((res) => {
      this.isProcess = false;
      window.location.href = res;
    });
  }

  onReportPDF() {
    this.isProcess = true;
    this.service.httpReportPDF('/api/v1/mst001/report/pdf', "mst001", this.searchForm.value).then((res) => {
      this.isProcess = false;
    });
  }

  onReportXls() {
    this.isProcess = true;
    this.service.httpReportXLS('/api/v1/mst001/report/xls', "mst001", this.searchForm.value).then((res) => {
      this.isProcess = false;
    });
  }

}
