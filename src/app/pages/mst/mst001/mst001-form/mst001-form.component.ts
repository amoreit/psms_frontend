import { Component, OnInit, ViewChild, Inject } from '@angular/core';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { MatPaginator, MatTableDataSource, MAT_DIALOG_DATA, MatDialog } from '@angular/material';
import { ApiService, IResponse, EHttp } from '../../../../shared/service/api.service';
import { ActivatedRoute, Router } from '@angular/router';
import { UtilService } from 'src/app/_util/util.service';
import Swal from 'sweetalert2/dist/sweetalert2.js';
import { TranslateService } from '@ngx-translate/core';
import 'sweetalert2/src/sweetalert2.scss';

@Component({
  selector: 'app-mst001-form',
  templateUrl: './mst001-form.component.html',
  styleUrls: ['./mst001-form.component.scss']
})
export class Mst001FormComponent implements OnInit {

  isProcess: boolean = false;
  form = this.fb.group({
    'yearGroupId': [''] 
    , 'yearTermId': ['', Validators.required]
    , 'yearId': ['', Validators.required]
    , 'scCode': ['']
    , 'firstday': ['']
    , 'lastday': ['']
    , 'updatedUser': ['']
    , 'name': ['']
    , 'iscurrent': ['']
  });

  updatedUser = '';
  updatedDate = Date;
  iscurrent: Boolean; 
  yearList: [];
  yearTermList: [];
  Status = '';
  id = '';
  validateValue = false;
  headerType = '';

  constructor(private translate: TranslateService, ar: ActivatedRoute, private fb: FormBuilder, private service: ApiService, private router: Router, public utilService: UtilService) {
    this.id = ar.snapshot.queryParamMap.get('id') || '';

    if (this.id == '') {
      this.Status = 'เพิ่มข้อมูล';
      this.headerType = this.translate.instant('modeType.MT0007');
      this.translate.onLangChange.subscribe(() => {
        this.headerType = this.translate.instant('modeType.MT0007');
      });
    } else {
      this.Status = 'แก้ไขข้อมูล';
      this.headerType = this.translate.instant('modeType.MT0008'); 
      this.translate.onLangChange.subscribe(() => {
        this.headerType = this.translate.instant('modeType.MT0008');
      });
      this.loadData();
    }
  }

  ngOnInit() {
    this.ddlYear();
    this.ddlYearTerm();
    this.form.controls.iscurrent.setValue(false);
  }

  loadData() {
    this.isProcess = true;
    this.service.httpGet('/api/v1/yeargroup/' + this.id, null).then((res: IResponse) => {
      this.isProcess = false;
      let data = res.responseData;
      this.form.controls.yearGroupId.setValue(data.yearGroupId || '');
      this.form.controls.yearTermId.setValue(data.yearTerm.yearTermId || '');
      this.form.controls.yearId.setValue(data.year.yearId || '');
      this.form.controls.firstday.setValue(data.firstday == null ? null : this.utilService.formatDateSQLToDate(data.firstday || ''));
      this.form.controls.lastday.setValue(data.lastday == null ? null : this.utilService.formatDateSQLToDate(data.lastday || ''));
      this.form.controls.iscurrent.setValue(data.iscurrent || '');
      this.form.controls.updatedUser.setValue(data.updatedUser || '');
      this.updatedUser = data.updatedUser;
      this.updatedDate = data.updatedDate;
    });
  }

  ddlYear() {
    this.service.httpGet('/api/v1/0/year/ddl', null).then((res) => {
      this.yearList = res || [];
    });
  }

  ddlYearTerm() {
    this.service.httpGet('/api/v1/0/yearTerm/ddl', null).then((res) => {
      this.yearTermList = res || [];
    });
  }

  backPage() {
    this.router.navigate(['/mst/mst001']);
  }
 
  onCancel() {
    if (this.id != '') {
      this.loadData();
    } else {
      this.form.controls.yearGroupId.setValue('');
      this.form.controls.yearTermId.setValue('');
      this.form.controls.yearId.setValue('');
      this.form.controls.scCode.setValue('');
      this.form.controls.firstday.setValue('');
      this.form.controls.lastday.setValue('');
      this.form.controls.updatedUser.setValue('');
      this.form.controls.name.setValue('');
      this.form.controls.iscurrent.setValue('');
    }
  }
 
  onSave() {
    if (this.form.invalid) {
      this.validateValue = true;
      Swal.fire(
        this.translate.instant('alert.error'),
        this.translate.instant('alert.validate'),
        'error'
      )
      return;
    }

    let jsonYearTerm = this.yearTermList.filter((o) => {
      return o['yearTermId'] == this.form.value.yearTermId;
    });
    let jsonYear = this.yearList.filter((o) => {
      return o['yearId'] == this.form.value.yearId;
    });
    this.form.value.name = jsonYearTerm[0]['yearTermCode'] + '/' + jsonYear[0]['yearCode'];

    Swal.fire({
      title: this.translate.instant('psms.DL0001'),
      icon: 'info',
      showCancelButton: true,
      allowOutsideClick: false,
      confirmButtonColor: '#3085d6', 
      cancelButtonColor: '#DD3333',
      confirmButtonText: this.translate.instant('psms.DL0008'),
      cancelButtonText: this.translate.instant('psms.DL0009')
    }).then((result) => {
      if (result.dismiss == 'cancel') {
        return;
      }
      
      this.form.value.scCode = "PRAMANDA";
      let json = this.form.value;
      if (json.yearGroupId == '' && json.iscurrent == false) {
        this.service.httpPost('/api/v1/yeargroup', json).then((res: IResponse) => {
          this.isProcess = false;
          if ((res.responseCode || 500) != 200) {
            Swal.fire(
              this.translate.instant('message.add_error'),
              res.responseMsg,
              'error'
            )
            return;
          }
          Swal.fire(
            this.translate.instant('alert.add_header'),
            this.translate.instant('message.add'),
            'success'
          ).then(() => { this.backPage(); })
        });
        return;
      }

      else if (json.yearGroupId == '' && json.iscurrent == true) {
        this.service.httpPost('/api/v1/yeargroup/insertIscurrent', json).then((res: IResponse) => {
          this.isProcess = false;
          if ((res.responseCode || 500) != 200) {
            alert(res.responseMsg);
            return;
          }
          Swal.fire(
            this.translate.instant('alert.add_header'),
            this.translate.instant('message.add'),
            'success'
          ).then(() => { this.backPage(); })
        });
        return;
      }

      else if (json.iscurrent == false) {
        this.service.httpPut('/api/v1/yeargroup', json).then((res: IResponse) => {
          this.isProcess = false;
          if ((res.responseCode || 500) != 200) {
            alert(res.responseMsg);
            return;
          }
          Swal.fire(
            this.translate.instant('psms.DL0006'),
            this.translate.instant('message.edit'),
            'success'
          ).then(() => { this.backPage(); })
        });
      }
      else if (json.iscurrent == true) {
        this.service.httpPut('/api/v1/yeargroup/updateIscurrent', json).then((res: IResponse) => {
          this.isProcess = false;
          if ((res.responseCode || 500) != 200) {
            alert(res.responseMsg);
            return;
          }
          else {
            this.onSave2();
          }
        });
      }
    });
  }

  onSave2() {
    let json = this.form.value;
    this.service.httpPut
      ('/api/v1/yeargroup', json).then((res: IResponse) => {
        if ((res.responseCode || 500) != 200) {
          alert(res.responseMsg);
          return;
        }
        Swal.fire(
          this.translate.instant('psms.DL0006'),
            this.translate.instant('message.edit'),
          'success'
        ).then(() => { this.backPage(); })
      });
  }

}