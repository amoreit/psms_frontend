import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, Validators } from '@angular/forms';
import { ApiService, IResponse } from 'src/app/shared/service/api.service';
import Swal from 'sweetalert2/dist/sweetalert2.js';
import 'sweetalert2/src/sweetalert2.scss';
import { UtilService } from 'src/app/_util/util.service';
import { TranslateService } from '@ngx-translate/core';


@Component({
  selector: 'app-mst014-form',
  templateUrl: './mst014-form.component.html',
  styleUrls: ['./mst014-form.component.scss']
})
export class Mst014FormComponent implements OnInit {

  isProcess: boolean = false;

  form = this.fb.group({
    'titleId': ['']
    , 'name': ['', Validators.required]
    , 'descTh': ['', Validators.required]
    , 'gender': ['', Validators.required]
  });
 
  genderList: [];
  mode = '';
  id = '';
  updatedDate: any;
  updatedDateTime: any;
  updatedUser: any;
  validateValue = false;
  headerType = '';

  constructor(private ar: ActivatedRoute,
    private fb: FormBuilder,
    private service: ApiService, 
    private router: Router,
    private translate: TranslateService,
    public _util: UtilService) {
  }

  ngOnInit() {
    this.ddlGender();
    this.id = this.ar.snapshot.queryParamMap.get('id') || '';
    if (this.id == '') {
      this.mode = 'เพิ่มข้อมูล';
      this.headerType = this.translate.instant('modeType.MT0007');
      this.translate.onLangChange.subscribe(() => {
        this.headerType = this.translate.instant('modeType.MT0007');
      });
    } else {
      this.mode = 'แก้ไขข้อมูล';
      this.headerType = this.translate.instant('modeType.MT0008');
      this.translate.onLangChange.subscribe(() => {
        this.headerType = this.translate.instant('modeType.MT0008');
      });
      this.loadData();
    }
  }

  loadData() {
    this.isProcess = true;
    this.service.httpGet('/api/v1/title/' + this.id, null).then((res: IResponse) => {
      this.isProcess = false;
      let data = res.responseData;

      this.form.controls.titleId.setValue(data.titleId || '')
      this.form.controls.name.setValue(data.name || '');
      this.form.controls.descTh.setValue(data.descTh || '');
      this.form.controls.gender.setValue(data.gender || '');

      this.updatedUser = data.updatedUser || '';
      this.updatedDateTime = data.updatedDate || '';
      this.updatedDate = this._util.formatDateSQLToString(data.updatedDate || '', true);
    });
  }

  onSave() {
    if (this.form.invalid) {
        this.validateValue = true;
        Swal.fire(
          this.translate.instant('alert.error'),
          this.translate.instant('alert.validate'),
          'error'
        )
      return;
    }
    Swal.fire({
      title: this.translate.instant('psms.DL0001'),
      icon: 'info',
      showCancelButton: true,
      confirmButtonColor: '#2085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: this.translate.instant('psms.DL0008'),
      cancelButtonText: this.translate.instant('psms.DL0009')
    }).then((result) => {
      if (result.dismiss == 'cancel') {
        return;
      }
      let json = this.form.value;
      if (json.titleId == '') {
        this.service.httpPost('/api/v1/title', json).then((res: IResponse) => {
          if ((res.responseCode || 500) != 200) {
            Swal.fire(
              this.translate.instant('message.add_error'),
              res.responseMsg,
              'error'
            );
            return;
          }
          Swal.fire(
            this.translate.instant('alert.add_header'),
            this.translate.instant('message.add'),
            'success'
          ).then(() => {
          this.onBack();
          })
        });
        return;
      }
      this.service.httpPut('/api/v1/title', json).then((res: IResponse) => {
        if ((res.responseCode || 500) != 200) {
          Swal.fire(
            this.translate.instant('message.delete_error'),
            res.responseMsg,
            'error'
          )
          return;
        }
        Swal.fire(
          this.translate.instant('alert.delete_header'),
          this.translate.instant('message.delete'),
          'success'
        ).then(() => {
          this.onBack();
        })
      });
    });
  }

  ddlGender() {
    this.service.httpGet('/api/v1/0/gender/ddl', null).then((res) => {
      this.genderList = res || [];
    });
  }

  onBack() {
    this.router.navigate(['mst/mst014']);
  }

  onCancel() {
    if (this.id != '') {
      this.loadData();
    } else {
      this.form.controls.name.setValue('');
      this.form.controls.descTh.setValue('');
      this.form.controls.gender.setValue('');
    }
  }

}
