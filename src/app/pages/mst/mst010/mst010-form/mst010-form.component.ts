import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, Validators } from '@angular/forms';
import { ApiService, IResponse } from 'src/app/shared/service/api.service';
import { MatDialogModule, MatDialog } from '@angular/material/dialog';
import Swal from 'sweetalert2/dist/sweetalert2.js';
import 'sweetalert2/src/sweetalert2.scss';
import { Mst010PopComponent } from '../mst010-pop/mst010-pop.component';
import { TranslateService } from '@ngx-translate/core';

type NewType = boolean;

@Component({
  selector: 'app-mst010-form',
  templateUrl: './mst010-form.component.html',
  styleUrls: ['./mst010-form.component.scss']
})
export class Mst010FormComponent implements OnInit {

  isProcess: boolean = false;
  paramForm = this.fb.group({
    'authorizedId': [''] 
    , 'scCode': ['']
    , 'fullnameTh': ['', Validators.required]
    , 'fullnameEn': ['']
    , 'position': ['', Validators.required]
    , 'langCode': ['']
    , 'iscurrent': ['']
    , 'signatureImagePath': ['']
    , '_sig': ['']

  });
  pList: [];
  AuList: [];
  id = '';
  authorizedId = '';
  authorized_id = '';
  position: any;
  langCode: any;
  scCode: any;
  fullnameTh: String;
  fullnameEn: String;
  fullname_th: String;
  fullname_en: String;
  signatureImagePath = '';
  Head = '';
  checked_auth = false;
  fullnameth: any;
  validateValue = false;
  headerType = '';

  constructor(
    private translate: TranslateService,
    private fb: FormBuilder,
    private service: ApiService,
    private router: Router,
    private ar: ActivatedRoute,
    public dialog: MatDialog,
  ) {
    this.id = ar.snapshot.queryParamMap.get('id') || '';
    if (this.id == '') {
      this.Head = 'เพิ่มข้อมูลผู้รับมอบอำนาจ';
      this.headerType = this.translate.instant('modeType.MT0007');
      this.translate.onLangChange.subscribe(() => {
        this.headerType = this.translate.instant('modeType.MT0007');
      });
    } else {
      this.Head = 'แก้ไขข้อมูลผู้รับมอบอำนาจ';
      this.headerType = this.translate.instant('modeType.MT0008');
      this.translate.onLangChange.subscribe(() => {
        this.headerType = this.translate.instant('modeType.MT0008');
      });
    }
  }

  ngOnInit() {
    this.ddlAuthor();
    this.ddlPosit();
    this.position = this.ar.snapshot.queryParamMap.get('po') || '';
    this.fullnameTh = this.ar.snapshot.queryParamMap.get('nth') || '';
    this.fullnameEn = this.ar.snapshot.queryParamMap.get('nen') || '';
    this.signatureImagePath = this.ar.snapshot.queryParamMap.get('sig') || '';
    this.scCode = this.ar.snapshot.queryParamMap.get('sc') || '';
    this.authorizedId = this.ar.snapshot.queryParamMap.get('id') || '';
    let iscurrent = this.ar.snapshot.queryParamMap.get('ic') || '';

    this.fullnameth = this.fullnameTh;
    this.paramForm.controls.position.setValue(this.position);
    this.paramForm.controls.fullnameTh.setValue(this.fullnameTh);
    this.paramForm.controls.fullnameEn.setValue(this.fullnameEn);
    this.paramForm.controls._sig.setValue(this.signatureImagePath.substr(this.signatureImagePath.indexOf("/") + 1));
    this.paramForm.controls.scCode.setValue(this.scCode);
    this.paramForm.controls.authorizedId.setValue(this.authorizedId);

    if (iscurrent == "true") { this.paramForm.controls.iscurrent.setValue(true); }
    else { this.paramForm.controls.iscurrent.setValue(false); }

  } 

  onFrom() {
    const dialogRef = this.dialog.open(Mst010PopComponent, {
      width: '60%'
      , 'autoFocus': true
      , 'disableClose': true
    });
    dialogRef.afterClosed().subscribe((result) => {
      if (result.selectedItem) {
        if(result.selectedItem.titleId == null || result.selectedItem.firstnameTh == null || result.selectedItem.lastnameTh == null || result.selectedItem.titleIdEn == null || result.selectedItem.firstnameEn == null || result.selectedItem.lastnameEn ==null){
          this.paramForm.controls.fullnameTh.setValue('');
          this.paramForm.controls.fullnameEn.setValue('');
        }else{
          this.paramForm.controls.fullnameTh.setValue(result.selectedItem.titleId + '' + result.selectedItem.firstnameTh + '  ' + result.selectedItem.lastnameTh);
          this.paramForm.controls.fullnameEn.setValue(result.selectedItem.titleIdEn + '.' + result.selectedItem.firstnameEn + '  ' + result.selectedItem.lastnameEn);
        }
      }
    });
  }

  onImage(event) {
    const selectedFile = event.target.files[0];
    const uploadData = new FormData();
    uploadData.append('file', selectedFile, selectedFile.name);
    this.service.httpPost("/api/v1/attachmentteacher/uploadsig/" + this.paramForm.value.fullnameTh, uploadData).then((res: IResponse) => {
      if (res.responseCode != 200) {
        alert(res.responseMsg);
        return;
      }
      let json = res.responseData;
      this.paramForm.controls._sig.setValue(json.attachmentName);
      this.paramForm.controls.signatureImagePath.setValue(json.attachmentCode + '/' + json.attachmentName);
    });
  }
  ddlPosit() {
    this.service.httpGet('/api/v1/0/ddlposit/ddl', null).then((res) => {
      this.pList = res || [];
    });
  }

  close() {
    this.onBack();
  }

  onBack() {
    this.router.navigate(['/mst/mst010']);
  }

  onCancle() {
    this.paramForm.controls.position.reset('');
    this.paramForm.controls.fullnameTh.reset('');
    this.paramForm.controls.fullnameEn.reset('');
    this.paramForm.controls.signatureImagePath.reset('');
    this.ngOnInit();
  }

  ddlAuthor() {
    let json = { 'position': this.position };
    this.service.httpGet('/api/v1/0/ddlAutho/ddlAuthoriz', json).then((res) => {
      this.AuList = res || [];
    });
  }

  onSave() {
    if (this.paramForm.invalid) {
      this.validateValue = true;
      Swal.fire(
        this.translate.instant('alert.error'),
        this.translate.instant('alert.validate'),
        'error'
      );
      return;

    }
    Swal.fire({
      title: this.translate.instant('psms.DL0001'),
      icon: 'info',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: this.translate.instant('psms.DL0008'),
      cancelButtonText: this.translate.instant('psms.DL0009')
    }).then((result) => {
      if (result.dismiss == 'cancel') {
        return;
      }
      this.isProcess = true;
      this.paramForm.controls.scCode.setValue("PRAMANDA")
      let json = this.paramForm.value;
      if (json.authorizedId == '' && json.iscurrent == false) {
        this.service.httpPost('/api/v1/Authori', json).then((res: IResponse) => {
          this.isProcess = false;
          if ((res.responseCode || 500) != 200) {
            Swal.fire(
              this.translate.instant('message.add_error'),
              res.responseMsg,
              'error'
            )
            return;
          }
          Swal.fire(
            this.translate.instant('alert.add_header'),
            this.translate.instant('message.add'),
            'success'
          ).then(() => { this.close(); })
        });
        return;
      }
      else if (json.authorizedId == '' && json.iscurrent == true) {
        this.service.httpPost('/api/v1/Authori/insertIscurrent', json).then((res: IResponse) => {
          this.isProcess = false;
          if ((res.responseCode || 500) != 200) {
            alert(res.responseMsg);
            return;
          }
          Swal.fire(
            this.translate.instant('alert.add_header'),
            this.translate.instant('message.add'),
            'success'
          ).then(() => { this.close(); })
        });
        return;
      }

      else if (json.iscurrent == false) {
        this.service.httpPut('/api/v1/Authori/update', json).then((res: IResponse) => {
          this.isProcess = false;
          if ((res.responseCode || 500) != 200) {
            Swal.fire(
              this.translate.instant('message.edit_error'),
              res.responseMsg,
              'error'
            )
            return;
          }
          Swal.fire(
            this.translate.instant('psms.DL0006'),
            this.translate.instant('message.edit'),
            'success'
          ).then(() => { this.close(); })
        });
      }
      else if (json.iscurrent == true) {
        this.service.httpPut('/api/v1/Authori/updateIscurrent', json).then((res: IResponse) => {
          this.isProcess = false;
          if ((res.responseCode || 500) != 200) {
            Swal.fire(
              this.translate.instant('message.edit_error'),
              res.responseMsg,
              'error'
            )
            return;
          }
          else {
            this.onSave2();
          }
        });
      }
    });
  }
  onSave2() {
    let json = this.paramForm.value;
    this.service.httpPut
      ('/api/v1/Authori/update', json).then((res: IResponse) => {
        if ((res.responseCode || 500) != 200) {
          Swal.fire(
            this.translate.instant('message.edit_error'),
            res.responseMsg,
            'error'
          )
          return;
        }
        Swal.fire(
          this.translate.instant('psms.DL0006'),
          this.translate.instant('message.edit'),
          'success'
        ).then(() => { this.close(); })
      });
  }

}
