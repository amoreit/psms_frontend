import { MatDialogModule, MatDialog } from '@angular/material/dialog';
import { Component, OnInit, ViewChild, Inject } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { MatPaginator, MatTableDataSource } from '@angular/material';;
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { Router, ActivatedRoute } from '@angular/router';
import Swal from 'sweetalert2/dist/sweetalert2.js';
import { UtilService } from 'src/app/_util/util.service';
import { IResponse, ApiService } from 'src/app/shared/service/api.service';
import { TranslateService } from '@ngx-translate/core';
@Component({
  selector: 'app-mst010-pop',
  templateUrl: './mst010-pop.component.html',
  styleUrls: ['./mst010-pop.component.scss']
})
export class Mst010PopComponent implements OnInit {

  isProcess: boolean = false;
  searchForm = this.fb.group({
    'teacherId': [''] 
    , 'firstnameTh': ['']
    , 'lastnameTh': ['']
    , 'firstnameEn': ['']
    , 'lastnameEn': ['']
    , 'fullname': ['']
    , 'fullnameEn': ['']
    , 'p': ['']
    , 'result': [5]
  });

  teacherList: [];
  id = '';

  pageSize: number;
  displayedColumns: string[] = ['no1', 'fullname', 'fullnameEn', 'Status'];
  dataSource = new MatTableDataSource();

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  constructor(
    private fb: FormBuilder,
    private service: ApiService,
    private router: Router,
    private ar: ActivatedRoute,
    private translate: TranslateService,
    public utilService: UtilService,
    @Inject(MAT_DIALOG_DATA) data: any,
    public dialogRef: MatDialogRef<Mst010PopComponent>,
  ) { }


  ngOnInit() {
    this.onSearch(0);
  } 

  onSearch(e) {
    this.searchForm.value.p = (this.paginator.pageIndex = e.pageIndex || 0) + 1;
    this.service.httpGet('/api/v1/tbTeacherProfile/search', this.searchForm.value).then((res: IResponse) => {
      this.dataSource.data = res.responseData || [];
      this.pageSize = res.responseSize || 0;
    });
  }

  onCancel() {
    this.searchForm.controls.teacherId.setValue('');
    this.searchForm.controls.firstnameTh.setValue('');
    this.searchForm.controls.lastnameTh.setValue('');
    this.searchForm.controls.firstnameEn.setValue('');
    this.searchForm.controls.lastnameEn.setValue('');
    this.searchForm.controls.fullname.setValue('');
    this.searchForm.controls.fullnameEn.setValue('');
    this.onSearch(0);
  }


  close() {
    this.dialogRef.close({ selectedItem: '' } );
    // this.onBack();
  }

  onSelect(item) {
    this.dialogRef.close({ selectedItem: item });
  }
}
