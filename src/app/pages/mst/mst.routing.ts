import { Routes } from '@angular/router';
import { AuthGuard } from '../../shared/service/auth.guard';

/*** Page ***/
import { Mst001Component } from './mst001/mst001.component';
import { Mst002Component } from './mst002/mst002.component';
import { Mst003Component } from './mst003/mst003.component'; 
import { Mst004Component } from './mst004/mst004.component';
import { Mst005Component } from './mst005/mst005.component';
import { Mst006Component } from './mst006/mst006.component';
import { Mst007Component } from './mst007/mst007.component';
import { Mst008Component } from './mst008/mst008.component';
import { Mst009Component } from './mst009/mst009.component';
import { Mst010Component } from './mst010/mst010.component';
import { Mst011Component } from './mst011/mst011.component';
import { Mst012Component } from './mst012/mst012.component';
import { Mst013Component } from './mst013/mst013.component';
import { Mst014Component } from './mst014/mst014.component';
import { Mst015Component } from './mst015/mst015.component';
import { Mst016Component } from './mst016/mst016.component';

import { Mst001FormComponent } from './mst001/mst001-form/mst001-form.component';
import { Mst002FormComponent } from './mst002/mst002-form/mst002-form.component';
import { Mst003FormComponent } from './mst003/mst003-form/mst003-form.component';
import { Mst004FormComponent } from './mst004/mst004-form/mst004-form.component';
import { Mst005FormComponent } from './mst005/mst005-form/mst005-form.component';
import { Mst006FormComponent } from './mst006/mst006-form/mst006-form.component';
import { Mst007FormComponent } from './mst007/mst007-form/mst007-form.component';
import { Mst008FormComponent } from './mst008/mst008-form/mst008-form.component';
import { Mst009FormComponent } from './mst009/mst009-form/mst009-form.component';
import { Mst010FormComponent } from './mst010/mst010-form/mst010-form.component';
import { Mst011FormComponent } from './mst011/mst011-form/mst011-form.component';
import { Mst012FormComponent } from './mst012/mst012-form/mst012-form.component';
import { Mst014FormComponent } from './mst014/mst014-form/mst014-form.component';
import { Mst015FormComponent } from './mst015/mst015-form/mst015-form.component';
import { Mst016FormComponent } from './mst016/mst016-form/mst016-form.component';

import { Mst010PopComponent } from './mst010/mst010-pop/mst010-pop.component';

export const MstRoutes: Routes = [
  {
    path:'', 
    children:[
      {path: 'mst001',component: Mst001Component},
      {path: 'mst002',component: Mst002Component},
      {path: 'mst003',component: Mst003Component},
      {path: 'mst004',component: Mst004Component},
      {path: 'mst005',component: Mst005Component},
      {path: 'mst006',component: Mst006Component},
      {path: 'mst007',component: Mst007Component},
      {path: 'mst008',component: Mst008Component},
      {path: 'mst009', component: Mst009Component},
      {path: 'mst010', component: Mst010Component},
      {path: 'mst011', component: Mst011Component},
      {path: 'mst012', component: Mst012Component},
      {path: 'mst013',component: Mst013Component},
      {path: 'mst014', component: Mst014Component},
      {path: 'mst015', component: Mst015Component},
      {path: 'mst016', component: Mst016Component},
      {path: 'mst001/form',component:  Mst001FormComponent,canActivate:[AuthGuard]},
      {path: 'mst002/form',component:  Mst002FormComponent,canActivate:[AuthGuard]},
      {path: 'mst003/form',component:  Mst003FormComponent,canActivate:[AuthGuard]},
      {path: 'mst004/form',component:  Mst004FormComponent,canActivate:[AuthGuard]},
      {path: 'mst005/form',component:  Mst005FormComponent,canActivate:[AuthGuard]},
      {path: 'mst006/form',component:  Mst006FormComponent,canActivate:[AuthGuard]},
      {path: 'mst007/form',component:  Mst007FormComponent,canActivate:[AuthGuard]},
      {path: 'mst008/form',component:  Mst008FormComponent,canActivate:[AuthGuard]},
      {path: 'mst009/form', component: Mst009FormComponent, canActivate:[AuthGuard]},
      {path: 'mst010/form', component: Mst010FormComponent, canActivate:[AuthGuard]},
      {path: 'mst011/form', component: Mst011FormComponent, canActivate:[AuthGuard]},
      {path: 'mst012/form', component: Mst012FormComponent, canActivate:[AuthGuard]},
      {path: 'mst014/form', component: Mst014FormComponent, canActivate:[AuthGuard]},
      {path: 'mst015/form', component: Mst015FormComponent, canActivate:[AuthGuard]},
      {path: 'mst016/form', component: Mst016FormComponent, canActivate:[AuthGuard]},
      {path: 'mst010/pop', component: Mst010PopComponent, canActivate: [AuthGuard]},

    ]
  }
];