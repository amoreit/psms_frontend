import { Component, OnInit } from '@angular/core';
import { Validators, FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { ApiService, IResponse } from 'src/app/shared/service/api.service';

import Swal from 'sweetalert2/dist/sweetalert2.js';
import 'sweetalert2/src/sweetalert2.scss';
import { UtilService } from 'src/app/_util/util.service';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-mst006-form',
  templateUrl: './mst006-form.component.html',
  styleUrls: ['./mst006-form.component.scss']
})
export class Mst006FormComponent implements OnInit {

  isProcess: boolean = false;
  form = this.fb.group({ 
    'classId': ['']
    , 'catClassId': ['', Validators.required]
    , 'scCode': ['PRAMANDA']
    , 'classLnameTh': ['', Validators.required]
    , 'classLnameEn': ['', Validators.required]
    , 'classSnameTh': ['', Validators.required]
    , 'classSnameEn': ['', Validators.required]
    , 'age': ['']
  });

  id: any;
  catClassList: [];
  modeType = '';
  updatedUser = '';
  updatedDate: Date;
  validateValue = false;
  headerType = '';

  constructor(
    private translate: TranslateService,
    ar: ActivatedRoute,
    private fb: FormBuilder,
    private service: ApiService,
    private router: Router,
    public util: UtilService
  ) {
    this.isProcess = true;
    this.id = ar.snapshot.queryParamMap.get('id') || '';
    if (this.id == '') { 
      this.modeType = 'เพิ่ม'; 
      this.headerType = this.translate.instant('modeType.MT0007');
      this.translate.onLangChange.subscribe(() => {
        this.headerType = this.translate.instant('modeType.MT0007');
      });
    }
    else { 
      this.modeType = 'แก้ไข'; 
      this.headerType = this.translate.instant('modeType.MT0008');
      this.translate.onLangChange.subscribe(() => {
        this.headerType = this.translate.instant('modeType.MT0008');
      });
      this.loadData();
    }
  }

  ngOnInit() {
    this.ddlTbMstCatClass();
  }

  loadData() {
    this.service.httpGet('/api/v1/tbMstClass/' + this.id, null).then((res: IResponse) => {
      this.isProcess = false;
      let data = res.responseData;
      this.form.controls.classId.setValue(data.classId || '');
      this.form.controls.catClassId.setValue(data.catclass.catClassId || '');
      this.form.controls.scCode.setValue(data.scCode || '');
      this.form.controls.classLnameTh.setValue(data.classLnameTh || '');
      this.form.controls.classLnameEn.setValue(data.classLnameEn || '');
      this.form.controls.classSnameTh.setValue(data.classSnameTh || '');
      this.form.controls.classSnameEn.setValue(data.classSnameEn || '');
      this.form.controls.age.setValue(data.age || '');
      this.updatedUser = localStorage.getItem('userName') || '';
      this.updatedDate = data.updatedDate;
    });
  }

  onBackPage() {
    this.router.navigate(['/mst/mst006']);
  }

  onClear() {
    this.form.controls.classId.setValue('');
    this.form.controls.catClassId.setValue('');
    this.form.controls.scCode.setValue('');
    this.form.controls.classLnameTh.setValue('');
    this.form.controls.classLnameEn.setValue('');
    this.form.controls.classSnameTh.setValue('');
    this.form.controls.classSnameEn.setValue('');
    this.form.controls.age.setValue('');
  }

  onSave() {
    if (this.form.invalid) {
      this.validateValue = true;
      Swal.fire(
        this.translate.instant('alert.error'),
        this.translate.instant('alert.validate'),
        'error'
      )
      return;
    }
    Swal.fire({
      title: this.translate.instant('psms.DL0001'),
      icon: 'info',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText:  this.translate.instant('psms.DL0008'),
      cancelButtonText:  this.translate.instant('psms.DL0009')
    }).then((result) => {
      if (result.dismiss == 'cancel') {
        return;
      }
      this.isProcess = true;
      let json = this.form.value;
      if (json.classId == '') {
        this.service.httpPost('/api/v1/tbMstClass', json).then((res: IResponse) => {
          this.isProcess = false;
          if ((res.responseCode || 500) != 200) {
            Swal.fire(
              this.translate.instant('message.add_error'),
              res.responseMsg, 
              'error'
            )
            return;
          }
          Swal.fire(
            this.translate.instant('alert.add_header'),
            this.translate.instant('message.add'),
            'success'
          ).then(() => { this.onBackPage(); })
        });
        return;
      }
      this.service.httpPut('/api/v1/tbMstClass', json).then((res: IResponse) => {
        this.isProcess = false;
        if ((res.responseCode || 500) != 200) {
          alert(res.responseMsg);
          Swal.fire(
            this.translate.instant('message.edit_error'),
            res.responseMsg,
            'error'
          )
          return;
        }
        Swal.fire(
          this.translate.instant('psms.DL0006'),
          this.translate.instant('message.edit'),
          'success'
        ).then(() => { this.onBackPage(); })
      });
    });
  }

  ddlTbMstCatClass() {
    this.service.httpGet('/api/v1/0/tbMstCatClass/ddl', null).then((res) => {
      this.catClassList = res || [];
    });
  }

}
