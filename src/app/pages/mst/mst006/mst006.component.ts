import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator, MatTableDataSource, MatDialog } from '@angular/material';
import { FormBuilder } from '@angular/forms';
import { ApiService, IResponse } from 'src/app/shared/service/api.service';
import { Router } from '@angular/router'; 
import Swal from 'sweetalert2/dist/sweetalert2.js';
import 'sweetalert2/src/sweetalert2.scss';
import { UtilService } from 'src/app/_util/util.service';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-mst006',
  templateUrl: './mst006.component.html',
  styleUrls: ['./mst006.component.scss']
}) 
export class Mst006Component implements OnInit {

  isProcess:boolean = false;
  searchForm = this.fb.group({
    'classLnameTh':['']
    , 'classId':['']
    , 'catClassId':['']
    , 'lnameTh':['']
    , 'classLnameEn':['']
    , 'age':['']
    , 'p':['']
    , 'result':[10]
  });

  pageSize:number; 
  displayedColumns: string[] = ['lnameTh', 'classLnameTh', 'classLnameEn', 'age', 'updatedUser', 'updatedDate', 'recordStatus'];
  dataSource = new MatTableDataSource();
  
  classList:[];
  catClassList:[];

  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  constructor(
    private translate: TranslateService,
    private fb: FormBuilder, 
    private service:ApiService,
    public dialog: MatDialog,
    private router: Router,
    public util: UtilService 
  ) {} 

  ngOnInit() { 
    this.ddlTbMstCatClass();  
    this.onSearch(0);  
  }

  onSearch(e){
    this.isProcess = true;
    this.searchForm.value.p = (this.paginator.pageIndex = e.pageIndex||0)+1;
    this.service.httpGet('/api/v1/tbMstClass',this.searchForm.value).then((res:IResponse)=>{
      this.isProcess = false;
      this.dataSource.data = res.responseData||[];
      this.pageSize = res.responseSize||0; 
    }); 
  }

  onClear(){
    this.searchForm.controls.classLnameTh.setValue('');
    this.searchForm.controls.lnameTh.setValue('');
    this.onSearch(0);
  }

  //DROP DOWN
  ddlTbMstClass(){
    this.searchForm.controls.classLnameTh.setValue('');
    let json = {'lnameTh':this.searchForm.value.lnameTh};
    this.service.httpGet('/api/v1/0/tbMstClass/ddlFilter',json).then((res)=>{
      this.classList = res||[];
    });
  }

  ddlTbMstCatClass(){
    this.service.httpGet('/api/v1/0/tbMstCatClass/ddl',null).then((res)=>{ 
      this.catClassList = res||[];
    });
  }

  onForm(id){
    this.router.navigate(['/mst/mst006/form'],{ queryParams: {id:id}});
  }

  onDelete(e){
      Swal.fire({
        title: this.translate.instant('alert.delete'),
        icon: 'info',
        showCancelButton: true,
        confirmButtonColor: '#2085d6',
        cancelButtonVolor: '#d33', 
        confirmButtonText:  this.translate.instant('psms.DL0008'),
        cancelButtonText:  this.translate.instant('psms.DL0009')
      }).then((result) => {
        if(result.dismiss == 'cancel'){
            return;
        }
        this.isProcess = true;
        this.service.httpDelete('/api/v1/tbMstClass',{'classId':e.classId}).then((res:IResponse)=>{
          this.isProcess = false;
          if((res.responseCode||500)!=200){
            alert(res.responseMsg);
            return;
          }Swal.fire(
            this.translate.instant('alert.delete_header'),
            this.translate.instant('message.delete'),
            'success'
          )
          this.onSearch(0);
        });
      });
  }

  onReport(){
    this.isProcess = true;
    this.service.httpPreviewPDF('/api/v1/mst006/report/pdf',this.searchForm.value).then((res)=>{
      this.isProcess = false;
      window.location.href = res;
    });
  }

  onReportPDF(){
    this.isProcess = true;
    this.service.httpReportPDF('/api/v1/mst006/report/pdf',"ข้อมูลระดับชั้นเรียน_ชั้นปี",this.searchForm.value).then((res)=>{
      this.isProcess = false;
    });
  }

  onReportXls(){
    this.isProcess = true;
    this.service.httpReportXLS('/api/v1/mst006/report/xls',"ข้อมูลระดับชั้นเรียน_ชั้นปี",this.searchForm.value).then((res)=>{
      this.isProcess = false;
    });
  }

}
