import { NgModule } from '@angular/core'; 
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FlexLayoutModule } from '@angular/flex-layout';
import { ReactiveFormsModule } from '@angular/forms';
import { TranslateModule } from '@ngx-translate/core';
import { MatDatepickerModule} from '@angular/material/datepicker';

/*** Material ***/
import {
  MatExpansionModule,
  MatButtonModule,
  MatIconModule,
  MatInputModule,
  MatTableModule,
  MatCardModule,
  MatFormFieldModule,
  MatPaginatorModule,
  MatDialogModule,
  MatSlideToggleModule,
  MatTabsModule
} from '@angular/material'; 

/*** Routing ***/
import {MstRoutes} from './mst.routing';

/*** Page ***/
import { Mst001Component } from './mst001/mst001.component';
import { Mst002Component } from './mst002/mst002.component';
import { Mst003Component } from './mst003/mst003.component';
import { Mst004Component } from './mst004/mst004.component';
import { Mst005Component } from './mst005/mst005.component';
import { Mst006Component } from './mst006/mst006.component';
import { Mst007Component } from './mst007/mst007.component';
import { Mst008Component } from './mst008/mst008.component';
import { Mst009Component } from './mst009/mst009.component';
import { Mst010Component } from './mst010/mst010.component';
import { Mst011Component } from './mst011/mst011.component';
import { Mst012Component } from './mst012/mst012.component';
import { Mst013Component } from './mst013/mst013.component';
import { Mst014Component } from './mst014/mst014.component';
import { Mst015Component } from './mst015/mst015.component';
import { Mst016Component } from './mst016/mst016.component';

import { Mst001FormComponent } from './mst001/mst001-form/mst001-form.component';
import { Mst002FormComponent } from './mst002/mst002-form/mst002-form.component';
import { Mst003FormComponent } from './mst003/mst003-form/mst003-form.component';
import { Mst004FormComponent } from './mst004/mst004-form/mst004-form.component';
import { Mst005FormComponent } from './mst005/mst005-form/mst005-form.component';
import { Mst006FormComponent } from './mst006/mst006-form/mst006-form.component';
import { Mst007FormComponent } from './mst007/mst007-form/mst007-form.component';
import { Mst008FormComponent } from './mst008/mst008-form/mst008-form.component';
import { Mst009FormComponent } from './mst009/mst009-form/mst009-form.component';
import { Mst010FormComponent } from './mst010/mst010-form/mst010-form.component';
import { Mst011FormComponent } from './mst011/mst011-form/mst011-form.component';
import { Mst012FormComponent } from './mst012/mst012-form/mst012-form.component';
import { Mst014FormComponent } from './mst014/mst014-form/mst014-form.component';
import { Mst015FormComponent } from './mst015/mst015-form/mst015-form.component';
import { Mst016FormComponent } from './mst016/mst016-form/mst016-form.component';

import { Mst010PopComponent } from './mst010/mst010-pop/mst010-pop.component';

import { HttpLoaderFactory } from 'src/app/app.module';
import { HttpClientModule, HttpClient } from '@angular/common/http';

 
@NgModule({
  imports: [
    CommonModule,
    TranslateModule,
    FlexLayoutModule,
    RouterModule.forChild(MstRoutes),
    MatExpansionModule,
    MatButtonModule,
    MatIconModule,
    MatInputModule,
    MatTableModule,
    MatDatepickerModule,
    MatCardModule, 
    MatFormFieldModule,
    ReactiveFormsModule,
    MatPaginatorModule,
    MatDialogModule,
    MatSlideToggleModule,
    MatTabsModule,
    HttpClientModule,
  ],
    providers: [],
    exports:[RouterModule],
  declarations: [
    Mst001Component,
    Mst002Component, 
    Mst003Component, 
    Mst004Component, 
    Mst005Component, 
    Mst006Component, 
    Mst007Component, 
    Mst008Component,  
    Mst009Component,
    Mst010Component,
    Mst011Component,
    Mst012Component,
    Mst013Component,
    Mst014Component,
    Mst015Component,
    Mst016Component,
    Mst001FormComponent,
    Mst002FormComponent, 
    Mst003FormComponent,
    Mst004FormComponent,
    Mst005FormComponent,
    Mst006FormComponent,
    Mst007FormComponent,
    Mst008FormComponent,
    Mst009FormComponent,
    Mst011FormComponent,
    Mst012FormComponent,
    Mst010FormComponent,
    Mst010PopComponent, 
    Mst014FormComponent,
    Mst015FormComponent,
    Mst016FormComponent, 
  ],
  entryComponents:[
  ]
})
export class MstModule { }
