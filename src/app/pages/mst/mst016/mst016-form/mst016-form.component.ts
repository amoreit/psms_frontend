import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, Validators } from '@angular/forms';
import { ApiService, IResponse } from 'src/app/shared/service/api.service';
import Swal from 'sweetalert2/dist/sweetalert2.js';
import 'sweetalert2/src/sweetalert2.scss';
import { UtilService } from 'src/app/_util/util.service';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-mst016-form',
  templateUrl: './mst016-form.component.html',
  styleUrls: ['./mst016-form.component.scss']
})
export class Mst016FormComponent implements OnInit {

  isProcess: boolean = false;

  form = this.fb.group({
    'scoreId': ['']
    , 'scoreName': ['', Validators.required]
    , 'fullScoreTotal': ['']
    , 'fullScore1': [''] 
    , 'fullScoreMidterm': ['']
    , 'fullScore2': [''] 
    , 'fullScoreFinal': ['', Validators.required]
    , 'iscatclass1': [false]
    , 'scCode': ['']
  });

  mode = '';
  id = '';
  updatedDate: any;
  updatedDateTime: any;
  updatedUser: any;
  catclassList: [];
  validateValue = false;
  headerType = '';

  constructor(
    private translate: TranslateService,
    private ar: ActivatedRoute,
    private fb: FormBuilder,
    private service: ApiService,
    private router: Router,
    public _util: UtilService) {
  }

  ngOnInit() {
    this.id = this.ar.snapshot.queryParamMap.get('id') || '';
    if (this.id == '') {
      this.mode = 'เพิ่มข้อมูล';
      this.headerType = this.translate.instant('modeType.MT0007');
      this.translate.onLangChange.subscribe(() => {
        this.headerType = this.translate.instant('modeType.MT0007');
      });
    } else {
      this.mode = 'แก้ไขข้อมูล';
      this.headerType = this.translate.instant('modeType.MT0008');
      this.translate.onLangChange.subscribe(() => {
        this.headerType = this.translate.instant('modeType.MT0008');
      });
      this.loadData();
    }
  }

  loadData() {
    this.isProcess = true;
    this.service.httpGet('/api/v1/score/' + this.id, null).then((res: IResponse) => {
      this.isProcess = false;
      let data = res.responseData;

      this.form.controls.scoreId.setValue(data.scoreId || '')
      this.form.controls.scoreName.setValue(data.scoreName || '');
      this.form.controls.fullScoreTotal.setValue(data.fullScoreTotal || '');
      this.form.controls.fullScore1.setValue(data.fullScore1 || '');
      this.form.controls.fullScoreMidterm.setValue(data.fullScoreMidterm || '');
      this.form.controls.fullScore2.setValue(data.fullScore2 || '');
      this.form.controls.fullScoreFinal.setValue(data.fullScoreFinal || '');
      this.form.controls.iscatclass1.setValue(data.iscatclass1);

      this.updatedUser = data.updatedUser || '';
      this.updatedDateTime = data.updatedDate || '';
      this.updatedDate = this._util.formatDateSQLToString(data.updatedDate || '', true);
    });
  }

  onSave() {
    if (this.form.invalid) {
      this.validateValue = true;
      Swal.fire(
        this.translate.instant('alert.error'),
        this.translate.instant('alert.validate'),
        'error'
      )
      return;
    }
    Swal.fire({
      title: this.translate.instant('psms.DL0001'),
      icon: 'info',
      showCancelButton: true,
      confirmButtonColor: '#2085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: this.translate.instant('psms.DL0008'),
      cancelButtonText: this.translate.instant('psms.DL0009')
    }).then((result) => {
      if (result.dismiss == 'cancel') {
        return;
      }
      this.form.value.iscatclass1 == false ? this.form.controls.fullScoreTotal.setValue(this.parseNum(this.form.value.fullScore1) +
        this.parseNum(this.form.value.fullScoreMidterm) +
        this.parseNum(this.form.value.fullScore2) +
        this.parseNum(this.form.value.fullScoreFinal)) : this.form.controls.fullScoreTotal.setValue(null)

      this.form.value.scCode = 'PRAMANDA';
      let json = this.form.value;
      if (json.scoreId == '') {
        this.service.httpPost('/api/v1/score', json).then((res: IResponse) => {
          if ((res.responseCode || 500) != 200) {
            Swal.fire(
              this.translate.instant('message.add_error'),
              res.responseMsg,
              'error'
            );
            return;
          }
          Swal.fire(
            this.translate.instant('alert.add_header'),
            this.translate.instant('message.add'),
            'success'
          ).then(() => {
            this.onBack();
          })
        });
        return;
      }
      this.service.httpPut('/api/v1/score', json).then((res: IResponse) => {
        if ((res.responseCode || 500) != 200) {
          Swal.fire(
            this.translate.instant('message.edit_error'),
            res.responseMsg,
            'error'
          )
          return;
        }
        Swal.fire(
          this.translate.instant('psms.DL0006'),
          this.translate.instant('message.edit'),
          'success'
        ).then(() => {
          this.onBack();
        })
      });
    });
  }

  onBack() {
    this.router.navigate(['mst/mst016']);
  }

  parseNum(a) {
    return Number(a);
  }

  onNumber(e): boolean {
    const chaCode = (e.which) ? e.which : e.keyCode;
    if (chaCode > 31 && (chaCode < 48 || chaCode > 57)) {
      return false;
    }
    return true;
  }

  changeScore(e) {
    if (e.target.checked == true) {
      this.form.controls.fullScore1.setValue(null)
      this.form.controls.fullScoreMidterm.setValue(null)
      this.form.controls.fullScore2.setValue(null)
    }
  }

  onCancel() {
    if (this.id != '') {
      this.loadData();
    } else {
      this.form.controls.scoreId.setValue('')
      this.form.controls.scoreName.setValue('');
      this.form.controls.fullScoreTotal.setValue('');
      this.form.controls.fullScore1.setValue('');
      this.form.controls.fullScoreMidterm.setValue('');
      this.form.controls.fullScore2.setValue('');
      this.form.controls.fullScoreFinal.setValue('');
      this.form.controls.iscatclass1.setValue(false);
      this.form.controls.scCode.setValue('');
    }
  }

}
