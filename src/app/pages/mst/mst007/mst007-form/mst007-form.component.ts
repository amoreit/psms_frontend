import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, Validators } from '@angular/forms';
import { ApiService, IResponse } from 'src/app/shared/service/api.service';

// sweetalert2
import Swal from 'sweetalert2/dist/sweetalert2.js';
import 'sweetalert2/src/sweetalert2.scss';
import { TranslateService } from '@ngx-translate/core';
// sweetalert2

@Component({
  selector: 'app-mst007-form',
  templateUrl: './mst007-form.component.html',
  styleUrls: ['./mst007-form.component.scss'] 
})
export class Mst007FormComponent implements OnInit {

  isProcess: boolean = false;
  form = this.fb.group({
    'activityId': ['']
    , 'catClass': ['', Validators.required]
    , 'classId': ['', Validators.required]
    , 'scCode': ['']
    , 'activityGroupId': ['', Validators.required]
    , 'activityName': ['', Validators.required]
    , 'location': ['']
    , 'isactive': ['']
    , 'updatedUser': ['']
    , 'updatedDate': ['']
  });
  catClassList: [];
  modeType = '';
  classList: [];
  activityGroupList: [];
  id = '';
  updatedUser = '';
  updatedDate: Date;
  validateValue = false;
  headerType = '';

  constructor(private translate: TranslateService,private ar: ActivatedRoute, private fb: FormBuilder, private service: ApiService, private router: Router) { }

  ngOnInit() {
    this.form.controls.isactive.setValue(true);
    this.ddlTbMstCatClass()
    this.ddlActivityGroup();
    this.id = this.ar.snapshot.queryParamMap.get('id') || '';
    if (this.id == '') { 
      this.modeType = 'เพิ่ม'; 
      this.headerType = this.translate.instant('modeType.MT0007');
      this.translate.onLangChange.subscribe(() => {
        this.headerType = this.translate.instant('modeType.MT0007');
      });
    }
    else { 
      this.modeType = 'แก้ไข'; 
      this.headerType = this.translate.instant('modeType.MT0008');
      this.translate.onLangChange.subscribe(() => {
        this.headerType = this.translate.instant('modeType.MT0008');
      });
      this.onGetData(); 
    }
  }

  onGetData() {
    this.isProcess = false;
    let id = this.ar.snapshot.queryParamMap.get('id') || '';
    this.service.httpGet('/api/v1/activity/' + id, null).then((res: IResponse) => {
      this.isProcess = false;
      let data = res.responseData;
      this.form.controls.activityId.setValue(data.activityId || '');
      this.form.controls.catClass.setValue(data.catClass || '');
      this.ddlTbMstClass();
      this.form.controls.classId.setValue(data.classId || '');
      this.form.controls.activityGroupId.setValue(data.activityGroupId || '');
      this.form.controls.activityName.setValue(data.activityName || '');
      this.form.controls.location.setValue(data.location || '');
      this.form.controls.updatedUser.setValue(localStorage.getItem("userName") || '');
      this.form.controls.isactive.setValue(data.isactive || '');
      this.updatedDate = data.updatedDate;
    });
  }

  onBackPage() {
    this.router.navigate(['/mst/mst007']);
  }

  onSave() {
    if (this.form.invalid) {
      this.validateValue = true;
      Swal.fire(
        this.translate.instant('alert.error'),
        this.translate.instant('alert.validate'),
        'error'
      )
      return;
    }
    Swal.fire({
      title: this.translate.instant('psms.DL0001'),
      icon: 'info',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: this.translate.instant('psms.DL0008'),
      cancelButtonText: this.translate.instant('psms.DL0009')
    }).then((result) => {
      if (result.dismiss == 'cancel') {
        return;
      }
      this.isProcess = true;
      let json = this.form.value;
      if (json.activityId == '' && json.isactive == false) {
        this.service.httpPost('/api/v1/activity', json).then((res: IResponse) => {
          this.isProcess = false;
          if ((res.responseCode || 500) != 200) {
            Swal.fire(
              this.translate.instant('message.add_error'),
              res.responseMsg,
              'error'
            )
            return;
          }
          Swal.fire(
            this.translate.instant('alert.add_header'),
            this.translate.instant('message.add'),
            'success'
          ).then(() => {
            this.onBackPage();
          })
        });
        return;
      }
      else if (json.activityId == '' && json.isactive == true) {
        this.service.httpPost('/api/v1/activity/insertIsactive', json).then((res: IResponse) => {
          this.isProcess = false;
          if ((res.responseCode || 500) != 200) {
            Swal.fire(
              this.translate.instant('message.add_error'),
              res.responseMsg,
              'error'
            )
            return;
          }
          Swal.fire(
            this.translate.instant('alert.add_header'),
            this.translate.instant('message.add'),
            'success'
          ).then(() => {
            this.onBackPage();
          })
        });
        return;
      }

      else if (json.isactive == false) {
        this.service.httpPut('/api/v1/activity/update', json).then((res: IResponse) => {
          this.isProcess = false;
          if ((res.responseCode || 500) != 200) {
            Swal.fire(
              this.translate.instant('message.edit_error'),
              res.responseMsg,
              'error'
            )
            return;
          }
          Swal.fire(
            this.translate.instant('psms.DL0006'),
            this.translate.instant('message.edit'),
            'success'
          ).then(() => {
            this.onBackPage();
          })
        });
      }
      else if (json.isactive == true) {
        this.service.httpPut('/api/v1/activity/updateIsactive', json).then((res: IResponse) => {
          this.isProcess = false;
          if ((res.responseCode || 500) != 200) {
            alert(res.responseMsg);
            return;
          }
          else {
            this.onSave2();
          }
        });
      }
    });
  }

  onSave2() {
    let json = this.form.value;
    this.service.httpPut
      ('/api/v1/activity/update', json).then((res: IResponse) => {
        if ((res.responseCode || 500) != 200) {
          Swal.fire(
            this.translate.instant('message.edit_error'),
            res.responseMsg,
            'error'
          )
          return;
        }
        Swal.fire(
          this.translate.instant('psms.DL0006'), 
          this.translate.instant('message.edit'),
          'success'
        ).then(() => {
          this.onBackPage();
        })
      });
  }

  ddlTbMstCatClass() {
    this.service.httpGet('/api/v1/0/tbMstCatClass/ddl', null).then((res) => {
      this.catClassList = res || [];
    });
  }

  ddlTbMstClass() {
    this.service.httpGet('/api/v1/0/class/ddlfilterbycatclass', { 'catClassId': this.form.value.catClass }).then((res) => {
      this.classList = res || [];
    });
  }

  ddlActivityGroup() {
    this.service.httpGet('/api/v1/0/activity-group/ddl', null).then((res) => {
      this.activityGroupList = res || [];
    });
  }

  OnClear() {
    if (this.id == '') {
      this.form.controls.classId.setValue('');
      this.form.controls.activityGroupId.setValue('');
      this.form.controls.activityName.setValue('');
      this.form.controls.location.setValue('');
    } else {
      this.onGetData();
    }
  }

}
