import { Component, OnInit, ViewChild } from '@angular/core';
import { MatTableDataSource, MatDialog, MatPaginator } from '@angular/material';
import { FormBuilder } from '@angular/forms';
import { ApiService, IResponse } from 'src/app/shared/service/api.service';
import { Router } from '@angular/router';

// sweetalert2 
import Swal from 'sweetalert2/dist/sweetalert2.js';
import 'sweetalert2/src/sweetalert2.scss';
import { UtilService } from 'src/app/_util/util.service';
import { TranslateService } from '@ngx-translate/core';
// sweetalert2

@Component({
  selector: 'app-mst007',
  templateUrl: './mst007.component.html',
  styleUrls: ['./mst007.component.scss']
})
export class Mst007Component implements OnInit {

  isProcess: boolean = false;
  searchForm = this.fb.group({
    'activityName': ['']
    , 'classId': [''] 
    , 'activityGroupId': ['']
    , 'location': ['']
    , 'p': ['']
    , 'result': [10]
  });

  report = this.fb.group({
    'activityGroupId': ['']
    , 'activityName': ['']
    , 'classId': ['']
  });

  reportClass = this.fb.group({
    'classId': ['']
  });

  reportClassActivityCode = this.fb.group({
    'activityGroupId': ['']
  });

  reportClassActivityName = this.fb.group({
    'activityName': ['']
  });

  reportlocation = this.fb.group({
    'location': ['']
  });

  reportClassActivityCodeActivityName = this.fb.group({

    'activityGroupId': ['']
    , 'activityName': ['']
    , 'classId': ['']
    , 'location': ['']

  });

  reportClasslocation = this.fb.group({
    'classId': ['']
    , 'location': ['']

  });

  reportactivityGroupIdlocation = this.fb.group({
    'activityGroupId': ['']
    , 'location': ['']

  });

  reportactivityNamelocation = this.fb.group({
    'activityName': ['']
    , 'location': ['']

  });

  pageSize: number;
  displayedColumns: string[] = ['activityGroupId', 'activityName', 'classLnameTh', 'location', 'updatedUser', 'updatedDate', 'recordStatus'];
  dataSource = new MatTableDataSource();
  pageGroupList: [];
  activityList: [];
  classList: [];
  activityGroupList: [];

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  constructor(
    private fb: FormBuilder,
    private service: ApiService,
    public dialog: MatDialog,
    private router: Router,
    public util: UtilService,
    private translate: TranslateService, ) { }

  ngOnInit() {
    this.onSearch(0);
    this.ddlTbMstClass();
    this.ddlActivityGroup();
  }

  onForm(id) {
    this.router.navigate(['/mst/mst007/form'], { queryParams: { id: id } });
  }

  onSearch(e) {
    this.searchForm.value.p = (this.paginator.pageIndex = e.pageIndex || 0) + 1;
    this.service.httpGet('/api/v1/activity', this.searchForm.value).then((res: IResponse) => {
      this.dataSource.data = res.responseData || [];
      this.pageSize = res.responseSize || 0;
    });
  }

  onDelete(e) {
    Swal.fire({
      title: this.translate.instant('alert.delete'),
      icon: 'info',
      showCancelButton: true,
      confirmButtonColor: '#2085d6',
      cancelButtonVolor: '#d33',
      confirmButtonText: this.translate.instant('psms.DL0008'),
      cancelButtonText: this.translate.instant('psms.DL0009')
    }).then((result) => {
      if (result.dismiss == 'cancel') {
        return;
      }
      this.isProcess = true;
      this.service.httpDelete('/api/v1/activity', { 'activityId': e.activity_id }).then((res: IResponse) => {
        this.isProcess = false;
        if ((res.responseCode || 500) != 200) {
          alert();
          Swal.fire(
            this.translate.instant('message.delete_error'),
            res.responseMsg,
            'error'
          )
          return;
        }
        Swal.fire(
          this.translate.instant('alert.delete_header'),
          this.translate.instant('message.delete'),
          'success'
        )
        this.onSearch(0);
      });
    });
  }

  ddlTbMstClass() {
    this.service.httpGet('/api/v1/0/tbMstClass/ddl', null).then((res) => {
      this.classList = res || [];
    });
  }

  ddlActivityGroup() {
    this.service.httpGet('/api/v1/0/activity-group/ddl', null).then((res) => {
      this.activityGroupList = res || [];
    });
  }

  onClear() {
    this.searchForm.controls.activityName.setValue("");
    this.searchForm.controls.classId.setValue("");
    this.searchForm.controls.activityGroupId.setValue("");
    this.searchForm.controls.location.setValue("");
    this.onSearch(0);
  }


  onReport() {
    if (this.searchForm.value.classId == '' && this.searchForm.value.activityGroupId == '' && this.searchForm.value.activityName == '' && this.searchForm.value.location == '') {
      this.service.httpPreviewPDF('/api/v1/mst007/report/pdf', this.searchForm.value).then((res) => {
        this.isProcess = false;
        window.location.href = res;
      });
    }

    else if (this.searchForm.value.classId != '' && this.searchForm.value.activityGroupId == '' && this.searchForm.value.activityName == '' && this.searchForm.value.location == '') {
      this.reportClass.controls.classId.setValue(this.searchForm.value.classId);
      this.service.httpPreviewPDF('/api/v1/mst007/report/pdfclass', this.reportClass.value).then((res) => {
        this.isProcess = false;
        window.location.href = res;
      });
    }

    else if (this.searchForm.value.classId == '' && this.searchForm.value.activityGroupId != '' && this.searchForm.value.activityName == '' && this.searchForm.value.location == '') {
      this.reportClassActivityCode.controls.activityGroupId.setValue(this.searchForm.value.activityGroupId);
      this.service.httpPreviewPDF('/api/v1/mst007/report/pdfclassActivityCode', this.reportClassActivityCode.value).then((res) => {
        this.isProcess = false;
        window.location.href = res;
      });
    }

    else if (this.searchForm.value.classId == '' && this.searchForm.value.activityGroupId == '' && this.searchForm.value.activityName != '' && this.searchForm.value.location == '') {
      this.reportClassActivityName.controls.activityName.setValue(this.searchForm.value.activityName);
      this.service.httpPreviewPDF('/api/v1/mst007/report/pdfclassActivityName', this.reportClassActivityName.value).then((res) => {
        this.isProcess = false;
        window.location.href = res;
      });
    }

    else if (this.searchForm.value.classId == '' && this.searchForm.value.activityGroupId == '' && this.searchForm.value.activityName == '' && this.searchForm.value.location != '') {
      this.reportlocation.controls.location.setValue(this.searchForm.value.location);
      this.service.httpPreviewPDF('/api/v1/mst007/report/pdflocation', this.reportlocation.value).then((res) => {
        this.isProcess = false;
        window.location.href = res;
      });
    }

    else if (this.searchForm.value.classId != '' && this.searchForm.value.activityGroupId != '' && this.searchForm.value.activityName != '') {
      this.reportClassActivityCodeActivityName.controls.activityGroupId.setValue(this.searchForm.value.activityGroupId);
      this.reportClassActivityCodeActivityName.controls.activityName.setValue(this.searchForm.value.activityName);
      this.reportClassActivityCodeActivityName.controls.classId.setValue(this.searchForm.value.classId);
      // this.reportClassActivityCodeActivityName.controls.location.setValue(this.searchForm.value.location);
      this.service.httpPreviewPDF('/api/v1/mst007/report/pdfclassActivityCodeActivityName', this.reportClassActivityCodeActivityName.value).then((res) => {
        this.isProcess = false;
        window.location.href = res;
      });
    }

    else if (this.searchForm.value.classId == '' && this.searchForm.value.activityGroupId != '' && this.searchForm.value.activityName != '') {
      this.reportClassActivityCodeActivityName.controls.activityGroupId.setValue(this.searchForm.value.activityGroupId);
      this.reportClassActivityCodeActivityName.controls.activityName.setValue(this.searchForm.value.activityName);
      this.service.httpPreviewPDF('/api/v1/mst007/report/pdfActivityCodeActivityName3', this.reportClassActivityCodeActivityName.value).then((res) => {
        this.isProcess = false;
        window.location.href = res;
      });
    }

    else if (this.searchForm.value.classId != '' && this.searchForm.value.activityGroupId == '' && this.searchForm.value.activityName != '') {
      this.reportClassActivityCodeActivityName.controls.classId.setValue(this.searchForm.value.classId);
      this.reportClassActivityCodeActivityName.controls.activityName.setValue(this.searchForm.value.activityName);
      this.service.httpPreviewPDF('/api/v1/mst007/report/pdfclassActivityName2', this.reportClassActivityCodeActivityName.value).then((res) => {
        this.isProcess = false;
        window.location.href = res;
      });
    }

    else if (this.searchForm.value.classId != '' && this.searchForm.value.activityGroupId != '' && this.searchForm.value.activityName == '') {
      this.reportClassActivityCodeActivityName.controls.classId.setValue(this.searchForm.value.classId);
      this.reportClassActivityCodeActivityName.controls.activityGroupId.setValue(this.searchForm.value.activityGroupId);
      this.reportClassActivityCodeActivityName.controls.activityName.setValue(this.searchForm.value.activityName);
      this.service.httpPreviewPDF('/api/v1/mst007/report/pdfclassActivityCodeActivityName', this.reportClassActivityCodeActivityName.value).then((res) => {
        this.isProcess = false;
        window.location.href = res;
      });
    }
    else if (this.searchForm.value.classId != '' && this.searchForm.value.activityGroupId == '' && this.searchForm.value.activityName == '' && this.searchForm.value.location != '') {
      this.reportClasslocation.controls.classId.setValue(this.searchForm.value.classId);
      this.reportClasslocation.controls.location.setValue(this.searchForm.value.location);
      this.service.httpPreviewPDF('/api/v1/mst007/report/pdfclasslocation', this.reportClasslocation.value).then((res) => {
        this.isProcess = false;
        window.location.href = res;
      });
    }
    else if (this.searchForm.value.classId == '' && this.searchForm.value.activityGroupId != '' && this.searchForm.value.activityName == '' && this.searchForm.value.location != '') {
      this.reportactivityGroupIdlocation.controls.activityGroupId.setValue(this.searchForm.value.activityGroupId);
      this.reportactivityGroupIdlocation.controls.location.setValue(this.searchForm.value.location);
      this.service.httpPreviewPDF('/api/v1/mst007/report/pdfactivitylocation', this.reportactivityGroupIdlocation.value).then((res) => {
        this.isProcess = false;
        window.location.href = res;
      });
    }
    else if (this.searchForm.value.classId == '' && this.searchForm.value.activityGroupId == '' && this.searchForm.value.activityName != '' && this.searchForm.value.location != '') {
      this.reportactivityNamelocation.controls.activityName.setValue(this.searchForm.value.activityName);
      this.reportactivityNamelocation.controls.location.setValue(this.searchForm.value.location);
      this.service.httpPreviewPDF('/api/v1/mst007/report/pdfactivitynamelocation', this.reportactivityNamelocation.value).then((res) => {
        this.isProcess = false;
        window.location.href = res;
      });
    }
  }

  onReportPDF() {
    if (this.searchForm.value.classId == '' && this.searchForm.value.activityGroupId == '' && this.searchForm.value.activityName == '' && this.searchForm.value.location == '') {
      this.service.httpReportPDF('/api/v1/mst007/report/pdf', "mst007", this.searchForm.value).then((res) => {
        this.isProcess = false;
      });
    }

    else if (this.searchForm.value.classId != '' && this.searchForm.value.activityGroupId == '' && this.searchForm.value.activityName == '' && this.searchForm.value.location == '') {
      this.reportClass.controls.classId.setValue(this.searchForm.value.classId);
      this.service.httpReportPDF('/api/v1/mst007/report/pdfclass', "mst007", this.reportClass.value).then((res) => {
        this.isProcess = false;
      });
    }

    else if (this.searchForm.value.classId == '' && this.searchForm.value.activityGroupId != '' && this.searchForm.value.activityName == '' && this.searchForm.value.location == '') {
      this.reportClassActivityCode.controls.activityGroupId.setValue(this.searchForm.value.activityGroupId);
      this.service.httpReportPDF('/api/v1/mst007/report/pdfclassActivityCode', "mst007", this.reportClassActivityCode.value).then((res) => {
        this.isProcess = false;
      });
    }

    else if (this.searchForm.value.classId == '' && this.searchForm.value.activityGroupId == '' && this.searchForm.value.activityName != '' && this.searchForm.value.location == '') {
      this.reportClassActivityName.controls.activityName.setValue(this.searchForm.value.activityName);
      this.service.httpReportPDF('/api/v1/mst007/report/pdfclassActivityName', "mst007", this.reportClassActivityName.value).then((res) => {
        this.isProcess = false;
      });
    }

    else if (this.searchForm.value.classId == '' && this.searchForm.value.activityGroupId == '' && this.searchForm.value.activityName == '' && this.searchForm.value.location != '') {
      this.reportlocation.controls.location.setValue(this.searchForm.value.location);
      this.service.httpReportPDF('/api/v1/mst007/report/pdflocation', "mst007", this.reportlocation.value).then((res) => {
        this.isProcess = false;
      });
    }

    else if (this.searchForm.value.classId != '' && this.searchForm.value.activityGroupId != '' && this.searchForm.value.activityName != '') {
      this.reportClassActivityCodeActivityName.controls.classId.setValue(this.searchForm.value.classId);
      this.reportClassActivityCodeActivityName.controls.activityGroupId.setValue(this.searchForm.value.activityGroupId);
      this.reportClassActivityCodeActivityName.controls.activityName.setValue(this.searchForm.value.activityName);
      this.service.httpReportPDF('/api/v1/mst007/report/pdfclassActivityCodeActivityName', "mst007", this.reportClassActivityCodeActivityName.value).then((res) => {
        this.isProcess = false;
      });
    }

    else if (this.searchForm.value.classId == '' && this.searchForm.value.activityGroupId != '' && this.searchForm.value.activityName != '') {
      this.reportClassActivityCodeActivityName.controls.activityGroupId.setValue(this.searchForm.value.activityGroupId);
      this.reportClassActivityCodeActivityName.controls.activityName.setValue(this.searchForm.value.activityName);
      this.service.httpReportPDF('/api/v1/mst007/report/pdfActivityCodeActivityName3', "mst007", this.reportClassActivityCodeActivityName.value).then((res) => {
        this.isProcess = false;
      });
    }

    else if (this.searchForm.value.classId != '' && this.searchForm.value.activityGroupId == '' && this.searchForm.value.activityName != '') {
      this.reportClassActivityCodeActivityName.controls.classId.setValue(this.searchForm.value.classId);
      this.reportClassActivityCodeActivityName.controls.activityName.setValue(this.searchForm.value.activityName);
      this.service.httpReportPDF('/api/v1/mst007/report/pdfclassActivityName2', "mst007", this.reportClassActivityCodeActivityName.value).then((res) => {
        this.isProcess = false;
      });
    }

    else if (this.searchForm.value.classId != '' && this.searchForm.value.activityGroupId != '' && this.searchForm.value.activityName == '') {
      this.reportClassActivityCodeActivityName.controls.classId.setValue(this.searchForm.value.classId);
      this.reportClassActivityCodeActivityName.controls.activityGroupId.setValue(this.searchForm.value.activityGroupId);
      this.reportClassActivityCodeActivityName.controls.activityName.setValue(this.searchForm.value.activityName);
      this.service.httpReportPDF('/api/v1/mst007/report/pdfclassActivityCodeActivityName', "mst007", this.reportClassActivityCodeActivityName.value).then((res) => {
        this.isProcess = false;
      });
    }
  }

  onReportXls() {
    if (this.searchForm.value.classId == '' && this.searchForm.value.activityGroupId == '' && this.searchForm.value.activityName == '' && this.searchForm.value.location == '') {
      this.service.httpReportXLS('/api/v1/mst007/report/xls', "mst007", this.searchForm.value).then((res) => {
        this.isProcess = false;
      });
    }

    else if (this.searchForm.value.classId != '' && this.searchForm.value.activityGroupId == '' && this.searchForm.value.activityName == '' && this.searchForm.value.location == '') {
      this.reportClass.controls.classId.setValue(this.searchForm.value.classId);
      this.service.httpReportXLS('/api/v1/mst007/report/xlsclass', "mst007", this.reportClass.value).then((res) => {
        this.isProcess = false;
      });
    }

    else if (this.searchForm.value.classId == '' && this.searchForm.value.activityGroupId != '' && this.searchForm.value.activityName == '' && this.searchForm.value.location == '') {
      this.reportClassActivityCode.controls.activityGroupId.setValue(this.searchForm.value.activityGroupId);
      this.service.httpReportXLS('/api/v1/mst007/report/xlsclassActivityCode', "mst007", this.reportClassActivityCode.value).then((res) => {
        this.isProcess = false;
      });
    }

    else if (this.searchForm.value.classId == '' && this.searchForm.value.activityGroupId == '' && this.searchForm.value.activityName != '' && this.searchForm.value.location == '') {
      this.reportClassActivityName.controls.activityName.setValue(this.searchForm.value.activityName);
      this.service.httpReportXLS('/api/v1/mst007/report/xlsclassActivityName', "mst007", this.reportClassActivityName.value).then((res) => {
        this.isProcess = false;
      });
    }

    else if (this.searchForm.value.classId == '' && this.searchForm.value.activityGroupId == '' && this.searchForm.value.activityName == '' && this.searchForm.value.location != '') {
      this.reportlocation.controls.location.setValue(this.searchForm.value.location);
      this.service.httpReportXLS('/api/v1/mst007/report/xlslocation', "mst007", this.reportlocation.value).then((res) => {
        this.isProcess = false;
      });
    }

    else if (this.searchForm.value.classId != '' && this.searchForm.value.activityGroupId != '' && this.searchForm.value.activityName != '') {
      this.reportClassActivityCodeActivityName.controls.classId.setValue(this.searchForm.value.classId);
      this.reportClassActivityCodeActivityName.controls.activityGroupId.setValue(this.searchForm.value.activityGroupId);
      this.reportClassActivityCodeActivityName.controls.activityName.setValue(this.searchForm.value.activityName);
      this.service.httpReportXLS('/api/v1/mst007/report/xlsclassActivityCodeActivityName', "mst007", this.reportClassActivityCodeActivityName.value).then((res) => {
        this.isProcess = false;
      });
    }

    else if (this.searchForm.value.classId == '' && this.searchForm.value.activityGroupId != '' && this.searchForm.value.activityName != '') {
      this.reportClassActivityCodeActivityName.controls.activityGroupId.setValue(this.searchForm.value.activityGroupId);
      this.reportClassActivityCodeActivityName.controls.activityName.setValue(this.searchForm.value.activityName);
      this.service.httpReportXLS('/api/v1/mst007/report/xlsclassActivityCodeActivityName', "mst007", this.reportClassActivityCodeActivityName.value).then((res) => {
        this.isProcess = false;
      });
    }

    else if (this.searchForm.value.classId != '' && this.searchForm.value.activityGroupId == '' && this.searchForm.value.activityName != '') {
      this.reportClassActivityCodeActivityName.controls.classId.setValue(this.searchForm.value.classId);
      this.reportClassActivityCodeActivityName.controls.activityName.setValue(this.searchForm.value.activityName);
      this.service.httpReportXLS('/api/v1/mst007/report/xlsclassActivityCodeActivityName', "mst007", this.reportClassActivityCodeActivityName.value).then((res) => {
        this.isProcess = false;
      });
    }

    else if (this.searchForm.value.classId != '' && this.searchForm.value.activityGroupId != '' && this.searchForm.value.activityName == '') {
      this.reportClassActivityCodeActivityName.controls.classId.setValue(this.searchForm.value.classId);
      this.reportClassActivityCodeActivityName.controls.activityGroupId.setValue(this.searchForm.value.activityGroupId);
      this.reportClassActivityCodeActivityName.controls.activityName.setValue(this.searchForm.value.activityName);
      this.service.httpReportXLS('/api/v1/mst007/report/xlsclassActivityCodeActivityName', "mst007", this.reportClassActivityCodeActivityName.value).then((res) => {
        this.isProcess = false;
      });
    }

  }

}
