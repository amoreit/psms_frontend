import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, Validators } from '@angular/forms';
import { ApiService, IResponse } from 'src/app/shared/service/api.service';
import Swal from 'sweetalert2/dist/sweetalert2.js';
import 'sweetalert2/src/sweetalert2.scss';
import { UtilService } from 'src/app/_util/util.service';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-mst008-form', 
  templateUrl: './mst008-form.component.html',
  styleUrls: ['./mst008-form.component.scss']
})
export class Mst008FormComponent implements OnInit {

  isProcess: boolean = false;

  form = this.fb.group({
    'classRoomId': ['']
    , 'nameClassId': ['', Validators.required]
    , 'classRoom': ['', Validators.required]
    , 'classRoomSnameTh': ['', Validators.required]
    , 'scCode': ['']
    , 'isEp': [false]
  });

  classList: [];
  teacherList: [];
  mode = '';
  id = '';
  updatedDate: any;
  updatedDateTime: any;
  updatedUser: any;
  validateValue = false;
  headerType = '';

  constructor(
    private translate: TranslateService,
    private ar: ActivatedRoute,
    private fb: FormBuilder,
    private service: ApiService,
    private router: Router,
    public _util: UtilService) {
  }

  ngOnInit() {
    this.ddlClass();
    this.id = this.ar.snapshot.queryParamMap.get('id') || '';
    if (this.id == '') {
      this.mode = 'เพิ่มข้อมูล';
      this.headerType = this.translate.instant('modeType.MT0007');
      this.translate.onLangChange.subscribe(() => {
        this.headerType = this.translate.instant('modeType.MT0007');
      });
    } else {
      this.mode = 'แก้ไขข้อมูล';
      this.headerType = this.translate.instant('modeType.MT0008');
      this.translate.onLangChange.subscribe(() => {
        this.headerType = this.translate.instant('modeType.MT0008');
      });
      this.loadData();
    }
  }

  loadData() {
    this.isProcess = true;
    this.service.httpGet('/api/v1/classroom/' + this.id, null).then((res: IResponse) => {
      this.isProcess = false;
      let data = res.responseData;

      this.form.controls.classRoomId.setValue(data.classRoomId || '');
      this.form.controls.nameClassId.setValue(data.classJoin.classId || '');
      this.form.controls.classRoom.setValue(data.classRoom || '');
      this.form.controls.classRoomSnameTh.setValue(data.classRoomSnameTh || '');
      this.form.controls.isEp.setValue(data.isEp || '');

      this.updatedUser = data.updatedUser || '';
      this.updatedDateTime = data.updatedDate || '';
      this.updatedDate = this._util.formatDateSQLToString(data.updatedDate || '', true);
    });
  }

  onSave() {
    if (this.form.invalid) {
      this.validateValue = true;
      Swal.fire(
        this.translate.instant('alert.error'),
        this.translate.instant('alert.validate'),
        'error'
      )
      return;
    }
    Swal.fire({
      title: this.translate.instant('psms.DL0001'),
      icon: 'info',
      showCancelButton: true,
      confirmButtonColor: '#2085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: this.translate.instant('psms.DL0008'),
      cancelButtonText: this.translate.instant('psms.DL0009')
    }).then((result) => {
      if (result.dismiss == 'cancel') {
        return;
      }
      this.form.value.scCode = 'PRAMANDA';
      let json = this.form.value;
      if (json.classRoomId == '') {
        this.service.httpPost('/api/v1/classroom', json).then((res: IResponse) => {
          if ((res.responseCode || 500) != 200) {
            Swal.fire(
              this.translate.instant('message.add_error'),
              res.responseMsg,
              'error'
            )
            return;
          }
          Swal.fire(
            this.translate.instant('alert.add_header'),
            this.translate.instant('message.add'),
            'success'
          ).then(() => {
            this.onBack();
          })
        });
        return;
      }
      this.service.httpPut('/api/v1/classroom', json).then((res: IResponse) => {
        if ((res.responseCode || 500) != 200) {
          Swal.fire(
            this.translate.instant('message.edit_error'),
            res.responseMsg,
            'error'
          )
          return;
        }
        Swal.fire(
          this.translate.instant('psms.DL0006'),
          this.translate.instant('message.edit'),
          'success'
        ).then(() => {
          this.onBack();
        })
      });
    });
  }

  ddlClass() {
    this.service.httpGet('/api/v1/0/class/ddl', null).then((res) => {
      this.classList = res || [];
    });
  }

  onBack() {
    this.router.navigate(['mst/mst008']);
  }

  onCancel() {
    if (this.id != '') {
      this.loadData();
    } else {
      this.form.controls.classRoomId.setValue('');
      this.form.controls.nameClassId.setValue('');
      this.form.controls.classRoom.setValue('');
      this.form.controls.classRoomSnameTh.setValue('');
      this.form.controls.scCode.setValue('');
    }
  }

}
