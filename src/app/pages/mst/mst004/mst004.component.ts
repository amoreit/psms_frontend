import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder } from '@angular/forms';
import { ApiService, IResponse } from 'src/app/shared/service/api.service';
import { MatDialog, MatTableDataSource, MatPaginator } from '@angular/material';
import Swal from 'sweetalert2/dist/sweetalert2.js';
import 'sweetalert2/src/sweetalert2.scss';
import { TranslateService } from '@ngx-translate/core';
import { UtilService } from 'src/app/_util/util.service';

@Component({
  selector: 'app-mst004',
  templateUrl: './mst004.component.html',
  styleUrls: ['./mst004.component.scss']
})
export class Mst004Component implements OnInit {
  isProcess: boolean = false;
  searchForm = this.fb.group({ 
    'catClassId': ['']
    , 'departmentId': [''] 
    , 'subjectCode': ['']
    , 'nameTh': [''] 
    , 'p': ['']
    , 'result': [10]
  });

  pageSize: number;
  displayedColumns: string[] = ['subjectCode', 'nameTh', 'nameDeparment', 'subjectType', 'credit_lesson', 'scale', 'updatedUser', 'updatedDate', 'subjectStatus'];
  dataSource = new MatTableDataSource();
  departmentList: [];
  catclassList: [];

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  constructor(private translate: TranslateService,private fb: FormBuilder, private service: ApiService, public dialog: MatDialog, private router: Router, public _util: UtilService) {

  }

  ngOnInit() {
    this.ddlDepartment();
    this.ddlCatClass();
    this.onSearch(0);
  }
 
  onSearch(e) {
    this.searchForm.value.p = (this.paginator.pageIndex = e.pageIndex || 0) + 1;
    this.service.httpGet('/api/v1/subject', this.searchForm.value).then((res: IResponse) => {
      this.dataSource.data = res.responseData || [];
      this.pageSize = res.responseSize || 0;
    });
  }

  onDelete(e) {
    Swal.fire({
      title: this.translate.instant('alert.delete'),
      icon: 'info',
      showCancelButton: true,
      confirmButtonColor: '#2085d6',
      cancelButtonColor: '#DD3333',
      confirmButtonText: this.translate.instant('psms.DL0008'),
      cancelButtonText: this.translate.instant('psms.DL0009')
    }).then((result) => {
      if (result.dismiss == 'cancel') {
        return;
      }
      this.isProcess = true;
      this.service.httpDelete('/api/v1/subject', { 'subjectId': e.subject_id }).then((res: IResponse) => {
        this.isProcess = false;
        if ((res.responseCode || 500) != 200) {
          Swal.fire(
            this.translate.instant('message.delete_error'),
            res.responseMsg,
            'error'
          )
          return;
        }
        Swal.fire(
          this.translate.instant('alert.delete_header'),
          this.translate.instant('message.delete'),
          'success'
        )
        this.onSearch(0);
      });
    });
  }

  onCancel() {
    this.searchForm.controls.catClassId.setValue('');
    this.searchForm.controls.departmentId.setValue('');
    this.searchForm.controls.subjectCode.setValue('');
    this.searchForm.controls.nameTh.setValue('');
    this.onSearch(0);
    this.ddlDepartment();

  }

  onForm(id) {
    this.router.navigate(['/mst/mst004/form'], { queryParams: { id: id } });
  }

  onPreviewPDF() {
    this.isProcess = true;
    if (this.searchForm.value.catClassId == '' && this.searchForm.value.departmentId == '') {
      this.service.httpPreviewPDF('/api/v1/subject/report/pdfall', this.searchForm.value).then((res) => {
        this.isProcess = false;
        window.location.href = res;
      });
    }
    if (this.searchForm.value.catClassId != '' && this.searchForm.value.departmentId == '') {
      this.service.httpPreviewPDF('/api/v1/subject/report/pdfcatclassid', this.searchForm.value).then((res) => {
        this.isProcess = false;
        window.location.href = res;
      });
    }
    if (this.searchForm.value.catClassId != '' && this.searchForm.value.departmentId != '') {
      this.service.httpPreviewPDF('/api/v1/subject/report/pdfcatclassidanddepartmentid', this.searchForm.value).then((res) => {
        this.isProcess = false;
        window.location.href = res;
      });
    }

  }

  onExportPDF() {
    this.isProcess = true;
    if (this.searchForm.value.catClassId == '' && this.searchForm.value.departmentId == '') {
      this.service.httpReportPDF('/api/v1/subject/report/pdfall', "ข้อมูลวิชา", this.searchForm.value).then((res) => {
        this.isProcess = false;
      });
    }
    if (this.searchForm.value.catClassId != '' && this.searchForm.value.departmentId == '') {
      this.service.httpReportPDF('/api/v1/subject/report/pdfcatclassid', "ข้อมูลวิชา", this.searchForm.value).then((res) => {
        this.isProcess = false;
      });
    }
    if (this.searchForm.value.catClassId != '' && this.searchForm.value.departmentId != '') {
      this.service.httpReportPDF('/api/v1/subject/report/pdfcatclassidanddepartmentid', "ข้อมูลวิชา", this.searchForm.value).then((res) => {
        this.isProcess = false;
      });
    }
  }

  onExportExcel() {
    this.isProcess = true;
    if (this.searchForm.value.catClassId == '' && this.searchForm.value.departmentId == '') {
      this.service.httpReportXLS('/api/v1/subject/report/xlsall', "ข้อมูลวิชา", this.searchForm.value).then((res) => {
        this.isProcess = false;
      });
    }
    if (this.searchForm.value.catClassId != '' && this.searchForm.value.departmentId == '') {
      this.service.httpReportXLS('/api/v1/subject/report/xlscatclassid', "ข้อมูลวิชา", this.searchForm.value).then((res) => {
        this.isProcess = false;
      });
    }
    if (this.searchForm.value.catClassId != '' && this.searchForm.value.departmentId != '') {
      this.service.httpReportXLS('/api/v1/subject/report/xlscatclassidanddepartmentid', "ข้อมูลวิชา", this.searchForm.value).then((res) => {
        this.isProcess = false;
      });
    }
  }

  ddlDepartment() {
    let json = { 'catClassId': this.searchForm.value.catClassId };
    this.service.httpGet('/api/v1/0/department/ddlfiltercatclass', json).then((res) => {
      this.departmentList = res || [];
    });
  }

  ddlCatClass() {
    this.service.httpGet('/api/v1/0/catclass/ddl', null).then((res) => {
      this.catclassList = res || [];
    });
  }


}
