import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, Validators } from '@angular/forms';
import { ApiService, IResponse } from 'src/app/shared/service/api.service';
import Swal from 'sweetalert2/dist/sweetalert2.js';
import 'sweetalert2/src/sweetalert2.scss';
import { UtilService } from 'src/app/_util/util.service';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-mst004-form',
  templateUrl: './mst004-form.component.html',
  styleUrls: ['./mst004-form.component.scss']
})

export class Mst004FormComponent implements OnInit {

  isProcess: boolean = false;
  form = this.fb.group({
    'subjectId': ['']
    , 'subjectCode': ['', Validators.required]
    , 'nameTh': ['', Validators.required]
    , 'credit': ['']
    , 'lesson': ['']
    , 'scale': ['']
    , 'scCode': ['']
    , 'catClassId': ['', Validators.required]
    , 'departmentId': ['', Validators.required]
    , 'subjectType': ['', Validators.required]
  }); 

  departmentList: [];
  subjecttypeList: [];
  scaleList: [];
  catclassList: [];
  mode = '';
  id = '';
  updatedDate: any;
  updatedDateTime: any;
  updatedUser: any;
  validateValue = false;
  headerType = '';

  constructor(private ar: ActivatedRoute,
    private translate: TranslateService,
    private fb: FormBuilder,
    private service: ApiService,
    private router: Router,
    public _util: UtilService) {
  }

  ngOnInit() {
    this.ddlCatClass();

    this.ddlSubjectType();
    this.ddlScale();

    this.id = this.ar.snapshot.queryParamMap.get('id') || '';
    if (this.id == '') {
      this.mode = 'เพิ่มข้อมูล';
      this.headerType = this.translate.instant('modeType.MT0007');
      this.translate.onLangChange.subscribe(() => {
        this.headerType = this.translate.instant('modeType.MT0007');
        });
    } else {
      this.mode = 'แก้ไขข้อมูล';
      this.headerType = this.translate.instant('modeType.MT0008');
      this.translate.onLangChange.subscribe(() => {
        this.headerType = this.translate.instant('modeType.MT0008');
        });
      this.loadData();
    }
  }

  loadData() {
    this.isProcess = true;
    this.service.httpGet('/api/v1/subject/subjectbyid', { 'subjectId': this.id }).then((res: IResponse) => {
      this.isProcess = false;
      let data = res.responseData;
      this.form.controls.subjectId.setValue(data[0].subject_id || '');
      this.form.controls.subjectCode.setValue(data[0].subject_code || '');
      this.form.controls.nameTh.setValue(data[0].name_th || '');
      this.form.controls.credit.setValue(this._util.formatFloat(data[0].credit, 1));
      this.form.controls.lesson.setValue(data[0].lesson || '');
      this.form.controls.scale.setValue(data[0].scale || '');
      this.form.controls.catClassId.setValue(data[0].cat_class || '');
      this.form.controls.departmentId.setValue(data[0].department_id || '');
      this.form.controls.subjectType.setValue(data[0].subject_type || '');
      this.updatedUser = data[0].updated_user || '';
      this.updatedDateTime = data[0].updated_date || '';
      this.updatedDate = this._util.formatDateSQLToString(data[0].updated_date || '', true);
      this.ddlDepartment();
    });
  }

  onSave() {
    if (this.form.invalid) {
      this.validateValue = true;
      Swal.fire(
        this.translate.instant('alert.error'),
        this.translate.instant('alert.validate'),
        'error'
      )
      return;
    }
    Swal.fire({
      title: this.translate.instant('psms.DL0001'),
      icon: 'info',
      showCancelButton: true,
      confirmButtonColor: '#2085d6',
      cancelButtonColor: '#DD3333',
      confirmButtonText: this.translate.instant('psms.DL0008'),
      cancelButtonText: this.translate.instant('psms.DL0009')
    }).then((result) => {
      if (result.dismiss == 'cancel') {
        return;
      }
      this.form.value.scCode = 'PRAMANDA';
      let json = this.form.value;
      if (json.subjectId == '') {
        this.service.httpPost('/api/v1/subject', json).then((res: IResponse) => {
          if ((res.responseCode || 500) != 200) {
            Swal.fire(
              this.translate.instant('message.add_error'),
              res.responseMsg,
              'error'
            )

            return;
          }
          Swal.fire(
            this.translate.instant('alert.add_header'),
            this.translate.instant('message.add'),
            'success'
          ).then(() => {
            this.onBack();
          })
        });
        return;
      }
      this.service.httpPut('/api/v1/subject', json).then((res: IResponse) => {
        if ((res.responseCode || 500) != 200) {
          Swal.fire(
            this.translate.instant('message.edit_error'),
            res.responseMsg,
            'error'
          )
          return;
        }
        Swal.fire(
          this.translate.instant('psms.DL0006'),
          this.translate.instant('message.edit'),
          'success'
        ).then(() => {
          this.onBack();
        })
      });
    })
  } 

  onNumber(e): boolean {
    const chaCode = (e.which) ? e.which : e.keyCode;
    if (chaCode > 31 && (chaCode < 48 || chaCode > 57) && chaCode != 46) {
      return false;
    }
    return true;
  }

  ddlDepartment() {
    let json = { 'catClassId': this.form.value.catClassId };
    this.service.httpGet('/api/v1/0/department/ddlfiltercatclass', json).then((res) => {
      this.departmentList = res || [];
    });
  }

  ddlSubjectType() {
    this.service.httpGet('/api/v1/0/subjecttype/ddl', null).then((res) => {
      this.subjecttypeList = res || [];
    });
  }

  ddlScale() {
    this.service.httpGet('/api/v1/0/scale/ddl', null).then((res) => {
      this.scaleList = res || [];
    });
  }

  ddlCatClass() {
    this.service.httpGet('/api/v1/0/catclass/ddl', null).then((res) => {
      this.catclassList = res || [];
    });
  }

  onBack() {
    this.router.navigate(['mst/mst004']);
  }

  onCancel() {
    if (this.id != '') {
      this.loadData();
    } else {
      this.form.controls.subjectId.setValue('');
      this.form.controls.subjectCode.setValue('');
      this.form.controls.nameTh.setValue('');
      this.form.controls.credit.setValue('');
      this.form.controls.lesson.setValue('');
      this.form.controls.scale.setValue('');
      this.form.controls.scCode.setValue('');
      this.form.controls.catClassId.setValue('');
      this.form.controls.departmentId.setValue('');
      this.form.controls.subjectType.setValue('');
    }
  }

}
