import { Component, OnInit } from '@angular/core';
import { Validators, FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { ApiService, IResponse } from 'src/app/shared/service/api.service';
import { TranslateService } from '@ngx-translate/core';

import Swal from 'sweetalert2/dist/sweetalert2.js';
import 'sweetalert2/src/sweetalert2.scss';
import { UtilService } from 'src/app/_util/util.service';

@Component({
  selector: 'app-mst013',
  templateUrl: './mst013.component.html',
  styleUrls: ['./mst013.component.scss']
})
export class Mst013Component implements OnInit {
  isProcess:boolean = false;
  Validata = false;
  countryList:[];
  provinceList:[];
  districtList:[];
  subDistrictList:[];
  form = this.fb.group({ 
    schoolProfileId:[""],
    scCode:["PRAMANDA"],
    scName:["",Validators.required],
    scNameEn:["",Validators.required],
    scType:["",Validators.required],
    scRegisterNo:[""], 
    scRegisterAuthorized:[""],
    scDoe:[""],
    scMotto	:[""],
    scVision:[""],
    scMission:[""],
    scAbout:[""],
    scLogo:[""],
    scAnnounce:[""],
    scAnnounceStaff:[""],
    scAnnounceStudent:[""],
    scBankAccount:[""],
    addrNo:["",Validators.required],
    village:[""],
    villageNo:["",Validators.required],
    alley:[""],
    road:["",Validators.required],
    subDistrict:["",Validators.required],
    district:["",Validators.required],
    province:["",Validators.required],
    country:["",Validators.required],
    postCode:["",Validators.required],
    tel:["",Validators.required],
    tel2:[""],
    tel3:[""],
    fax:[""],
    email:[""],
    email2:[""],
    email3:[""],
    website:[""],
    facebook:[""],
    twitter:[""]
});  

  constructor(ar:ActivatedRoute,
    private translate: TranslateService,
    private fb: FormBuilder, 
    private service:ApiService,
    private router: Router) { }

  ngOnInit() {
    this.loadData();
    this.ddlCountry();
    this.ddlProvince();
    this.ddlDistrict();
    this.ddlSubDistrict();
  }
  loadData() {
    this.isProcess = true;
    let json = {'scCode':this.form.value.scCode};
    this.service.httpGet("/api/v1/MstSchoolProfile",json).then((res: IResponse) => {
      this.isProcess = false;
      let data = res.responseData;
      this.form.controls.schoolProfileId.setValue(data[0].schoolProfileId||'');
      this.form.controls.scCode.setValue(data[0].scCode||'');
      this.form.controls.scName.setValue(data[0].scName||'');
      this.form.controls.scNameEn.setValue(data[0].scNameEn||'');
      this.form.controls.scType.setValue(data[0].scType||'');
      this.form.controls.scRegisterNo.setValue(data[0].scRegisterNo||'');
      this.form.controls.scRegisterAuthorized.setValue(data[0].scRegisterAuthorized||'');
      this.form.controls.scDoe.setValue(data[0].scDoe||'');
      this.form.controls.scMotto.setValue(data[0].scMotto||'');
      this.form.controls.scVision.setValue(data[0].scVision||'');
      this.form.controls.scMission.setValue(data[0].scMission||'');
      this.form.controls.scAbout.setValue(data[0].scAbout||'');
      this.form.controls.scLogo.setValue(data[0].scLogo||'');
      this.form.controls.scAnnounce.setValue(data[0].scAnnounce||'');
      this.form.controls.scAnnounceStaff.setValue(data[0].scAnnounceStaff||'');
      this.form.controls.scAnnounceStudent.setValue(data[0].scAnnounceStudent||'');
      this.form.controls.scBankAccount.setValue(data[0].scBankAccount||'');
      this.form.controls.addrNo.setValue(data[0].addrNo||'');
      this.form.controls.village.setValue(data[0].village||'');
      this.form.controls.villageNo.setValue(data[0].villageNo||'');
      this.form.controls.alley.setValue(data[0].alley||'');
      this.form.controls.road.setValue(data[0].road||'');
      this.form.controls.province.setValue(data[0].province||'');
      this.ddlDistrict()
      this.form.controls.district.setValue(data[0].district||'');
      this.ddlSubDistrict();
      this.form.controls.subDistrict.setValue(data[0].subDistrict||'');
      this.form.controls.country.setValue(data[0].country||'');
      this.form.controls.postCode.setValue(data[0].postCode||'');
      this.form.controls.tel.setValue(data[0].tel||'');
      this.form.controls.tel2.setValue(data[0].tel2||'');
      this.form.controls.tel3.setValue(data[0].tel3||'');
      this.form.controls.fax.setValue(data[0].fax||'');
      this.form.controls.email.setValue(data[0].email||'');
      this.form.controls.email2.setValue(data[0].email2||'');
      this.form.controls.email3.setValue(data[0].email3||'');
      this.form.controls.website.setValue(data[0].website||'');
      this.form.controls.facebook.setValue(data[0].facebook||'');
      this.form.controls.twitter.setValue(data[0].twitter||'');
    });
  }

  ddlCountry(){
    this.service.httpGet("/api/v1/0/country/ddl", null).then(res => {
        this.countryList = res || [];
    });
  }
  ddlProvince(){
    this.service.httpGet('/api/v1/0/province/ddl',null).then((res)=>{
        this.provinceList = res || [];
    });
  }
  ddlDistrict(){
    let json = {'province':this.form.value.province};
    this.service.httpGet('/api/v1/0/district/ddlByCondition',json).then((res)=>{
        this.districtList = res || [];
    });
  }
  ddlSubDistrict(){
    let json = {'district':this.form.value.district};
    this.service.httpGet("/api/v1/0/sub-district/ddlByCondition",json).then((res)=>{
        this.subDistrictList = res || [];
    }); 
  }
  onClear(){
    this.form.controls.scType.setValue('');
    this.form.controls.scRegisterNo.setValue('');
    this.form.controls.scRegisterAuthorized.setValue('');
    this.form.controls.scDoe.setValue('');
    this.form.controls.scMotto.setValue('');
    this.form.controls.scVision.setValue('');
    this.form.controls.scMission.setValue('');
    this.form.controls.scAbout.setValue('');
    this.form.controls.scLogo.setValue('');
    this.form.controls.scAnnounce.setValue('');
    this.form.controls.scAnnounceStaff.setValue('');
    this.form.controls.scAnnounceStudent.setValue('');
    this.form.controls.scBankAccount.setValue('');
    this.form.controls.addrNo.setValue('');
    this.form.controls.village.setValue('');
    this.form.controls.villageNo.setValue('');
    this.form.controls.alley.setValue('');
    this.form.controls.road.setValue('');
    this.form.controls.subDistrict.setValue('');
    this.form.controls.district.setValue('');
    this.form.controls.province.setValue('');
    this.form.controls.country.setValue('');
    this.form.controls.postCode.setValue('');
    this.form.controls.tel.setValue('');
    this.form.controls.tel2.setValue('');
    this.form.controls.tel3.setValue('');
    this.form.controls.fax.setValue('');
    this.form.controls.email.setValue('');
    this.form.controls.email2.setValue('');
    this.form.controls.email3.setValue('');
    this.form.controls.website.setValue('');
    this.form.controls.facebook.setValue('');
    this.form.controls.twitter.setValue('');
  }

  onSave(){
    if (this.form.invalid) {
        this.Validata = true;
        Swal.fire(
          this.translate.instant('message.add_error'),//เพิ่มข้อมูลผิดพลาด
          this.translate.instant('alert.validate'),//กรุณาระบุข้อมูล ให้ครบ.
          "error"
          );
        return;
    }
    else {
      Swal.fire({
        title: this.translate.instant('alert.save'),//ต้องการบันทึกข้อมูล ใช่หรือไม่
        icon : 'info',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: this.translate.instant('psms.DL0008'),//ใช่
        cancelButtonText: this.translate.instant('psms.DL0009')//ไม่
      }).then((result) => {
          if(result.dismiss == 'cancel'){
            return;
          } 
          let json = this.form.value;
          this.service.httpPut("/api/v1/MstSchoolProfile",json).then((res:IResponse)=>{
            this.isProcess = false;
            if((res.responseCode||500)!=200){
                Swal.fire(
                  this.translate.instant('message.edit_error'),//แก้ไขข้อมูลผิดพลาด
                  res.responseMsg,
                  'error' 
                )
              return;
            }
            Swal.fire(
              this.translate.instant('psms.DL0006'),//แก้ไขข้อมูล
              this.translate.instant('psms.DL0007'),//แก้ไขข้อมูลสำเร็จ
              'success'
            )
          });
        });
          
      }
    }
}
