import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder } from '@angular/forms';
import { ApiService, IResponse } from 'src/app/shared/service/api.service';
import { MatDialog, MatTableDataSource, MatPaginator } from '@angular/material';
import Swal from 'sweetalert2/dist/sweetalert2.js';
import 'sweetalert2/src/sweetalert2.scss';
import { TranslateService } from '@ngx-translate/core';

export interface result {
  competenciesResult: any;
}

@Component({
  selector: 'app-aca004-sub08',
  templateUrl: './aca004-sub08.component.html',
  styleUrls: ['./aca004-sub08.component.scss']
})
export class Aca004Sub08Component implements OnInit {

  isProcess: boolean = false;
  searchForm = this.fb.group({
    'yearGroupId': [''] 
    , 'classId': ['']
    , 'classRoomId': ['']
  });
  pageSize: number;
  displayedColumns: string[] = ['classRoomNo', 'stuCode', 'fullname', 'memberStatus'];
  dataSource = new MatTableDataSource();
  classList: [];
  classroomnoList: [];
  yearGroupList: [];
  checkdata = false;
  chooseAll: any;
  citizen_id: any;

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  constructor(private fb: FormBuilder, private service: ApiService, public dialog: MatDialog, private translate: TranslateService, private router: Router) {
  }

  ngOnInit() {
    this.citizen_id = localStorage.getItem('citizenId') || '';
    this.ddlClass();
    this.ddlYearGroup();
    this.onSearch(0);
  }

  onSearch(e) {
    this.service.httpGet('/api/v1/trn-classroom-member/stuforcompetencies', this.searchForm.value).then((res: IResponse) => {
      if (res.responseData.length === 0) {
        this.dataSource.data = []
        this.checkdata = true;
      } else {
        this.dataSource.data = res.responseData || [];
        this.chooseAll = undefined;
        this.checkdata = false;
      }
    });
  }

  onCancel() {
    this.searchForm = this.fb.group({
      'yearGroupId': ['']
      , 'classId': ['']
      , 'classRoomId': ['']
    });
    this.onSearch(0);
  }

  ddlClass() {
    this.service.httpGet('/api/v1/0/class/ddlbycitizenid', { 'citizenId': this.citizen_id }).then(res => {
      this.classList = res || [];
    });
  }

  ddlClassRoomNo() {
    const json = { 'citizenId': this.citizen_id, 'classId': this.searchForm.value.classId };
    this.service
      .httpGet('/api/v1/0/classroom-list/ddlfiltercitizenid', json)
      .then(res => {
        this.classroomnoList = res || [];
      });
  }

  ddlYearGroup() {
    this.service.httpGet('/api/v1/0/year-group/ddl', null).then((res) => {
      this.yearGroupList = res || [];
      let ele = this.yearGroupList.filter((o) => {
        return o['iscurrent'] == "true";
      });
      this.searchForm.controls.yearGroupId.setValue(ele[0]['yearGroupId'])
    });
  }

  onChoose(o) {
    this.dataSource.data.map(e => {
      e['competenciesResult'] = o.target.value;
      return e;
    });
  }

  onChooseSomeone() {
    this.chooseAll = undefined;
  }

  onSave2() {
    const myData = this.dataSource.data.map((row: result) => {
      return {
        competenciesResult: row.competenciesResult,
      }
    });

    Swal.fire({
      title: this.translate.instant('psms.DL0001'),
      icon: 'info',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: this.translate.instant('psms.DL0008'),
      cancelButtonText: this.translate.instant('psms.DL0009')
    }).then((result) => {
      if (result.dismiss == 'cancel') {
        return;
      }

      let json = this.dataSource.data;
      let user = localStorage.getItem('userName')
      if (json.length === 0 || this.searchForm.value.yearGroupId == '' || this.searchForm.value.classId == '' || this.searchForm.value.classRoomId == '') {
        Swal.fire(
          this.translate.instant('alert.error'),
          this.translate.instant('alert.validate'),
          'error'
        )
      } else {
        this.service.httpPut('/api/v1/trn-classroom-member/' + user, json).then((res: IResponse) => {
          if((res.responseCode||500)!=200){
            Swal.fire(
              this.translate.instant('message.save_error'),
              res.responseMsg,
              'error' 
            )
            return;
          }
          Swal.fire( 
            this.translate.instant('message.save_header'), 
            this.translate.instant('message.save'),
            'success'
          ).then(() => { })
        });
      }
    });

  }

}
