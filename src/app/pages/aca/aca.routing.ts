import { Routes } from '@angular/router';
import { AuthGuard } from '../../shared/service/auth.guard';

/*** Page ***/
import { Aca001Component } from './aca001/aca001.component';
import { Aca002Component } from './aca002/aca002.component';
import { Aca002Sub01Component } from './aca002-sub01/aca002-sub01.component';
import { Aca002Sub02Component } from './aca002-sub02/aca002-sub02.component';
import { Aca002Sub03Component } from './aca002-sub03/aca002-sub03.component';
import { Aca002SubjectComponent } from './aca002/aca002-subject/aca002-subject.component';
import { Aca004Sub01Component } from './aca004-sub01/aca004-sub01.component';
import { Aca004Sub01FormComponent } from './aca004-sub01/aca004-sub01-form/aca004-sub01-form.component'; 
import { Aca004Sub01Form2Component } from './aca004-sub01/aca004-sub01-form2/aca004-sub01-form2.component';
import { Aca004Sub02Component } from './aca004-sub02/aca004-sub02.component';
import { Aca004Sub03Component } from './aca004-sub03/aca004-sub03.component';
import { Aca004Sub03ScoreComponent } from './aca004-sub03/aca004-sub03-score/aca004-sub03-score.component';
import { Aca004Sub04Component } from './aca004-sub04/aca004-sub04.component';
import { Aca004Sub05Component } from './aca004-sub05/aca004-sub05.component';
import { Aca004Sub05FormComponent } from './aca004-sub05/aca004-sub05-form/aca004-sub05-form.component';
import { Aca004Sub06Component } from './aca004-sub06/aca004-sub06.component';
import { Aca004Sub06FormComponent } from './aca004-sub06/aca004-sub06-form/aca004-sub06-form.component'; 
import { Aca004Sub07Component } from './aca004-sub07/aca004-sub07.component';
import { Aca004Sub07FormComponent } from './aca004-sub07/aca004-sub07-form/aca004-sub07-form.component';
import { Aca004Sub08Component } from './aca004-sub08/aca004-sub08.component';
import { Aca004Sub09Component } from './aca004-sub09/aca004-sub09.component';
import { Aca004Sub09EvaluationComponent } from './aca004-sub09/aca004-sub09-evaluation/aca004-sub09-evaluation.component';
import { Aca006Component } from './aca006/aca006.component';
import { Aca006FormComponent } from './aca006/aca006-form/aca006-form.component';
import { Aca006Form2Component } from './aca006/aca006-form2/aca006-form2.component';
import { Aca008Component } from './aca008/aca008.component';
import { Aca008ReportComponent } from './aca008/aca008-report/aca008-report.component';
import { Aca009Component } from './aca009/aca009.component';
import { Aca011ReportComponent } from './aca011/aca011-report/aca011-report.component';
import { Aca012Component } from './aca012/aca012.component';
import { Aca011Component } from './aca011/aca011.component';
import { Aca011ReportEngComponent } from './aca011/aca011-report-eng/aca011-report-eng.component';
import { Aca004Sub06HtmlComponent } from './aca004-sub06/aca004-sub06-html/aca004-sub06-html.component';
import { Aca014Component } from './aca014/aca014.component';
import { Aca014HtmlComponent } from './aca014/aca014-html/aca014-html.component';
import { Aca013Component } from './aca013/aca013.component';
import { Aca013Form1Component } from './aca013/aca-form1/aca-form1.component';
import { Aca013Form2Component } from './aca013/aca-form2/aca-form2.component';
import { Aca015Component } from './aca015/aca015.component';
import { Aca015Form1Component } from './aca015/aca015-form1/aca015-form1.component';
import { Aca015Form2Component } from './aca015/aca015-form2/aca015-form2.component';
import { Aca015Form3Component } from './aca015/aca015-form3/aca015-form3.component';

export const AcaRoutes: Routes = [
  { 
    path:'',
    children:[
      {path: 'aca001',component: Aca001Component,canActivate:[AuthGuard]},
      {path: 'aca002',component: Aca002Component,canActivate:[AuthGuard]},
      {path: 'aca002-sub01',component: Aca002Sub01Component,canActivate:[AuthGuard]},
      {path: 'aca002-sub02',component: Aca002Sub02Component,canActivate:[AuthGuard]},
      {path: 'aca002-sub03',component: Aca002Sub03Component,canActivate:[AuthGuard]},
      {path: 'aca002/subject',component: Aca002SubjectComponent,canActivate:[AuthGuard]},
      {path: 'aca004-sub01',component: Aca004Sub01Component,canActivate:[AuthGuard]},
      {path: 'aca004-sub01/form',component: Aca004Sub01FormComponent,canActivate:[AuthGuard]},
      {path: 'aca004-sub01/form2',component: Aca004Sub01Form2Component,canActivate:[AuthGuard]},
      {path: 'aca004-sub02',component: Aca004Sub02Component,canActivate:[AuthGuard]},
      {path: 'aca004-sub03',component: Aca004Sub03Component,canActivate:[AuthGuard]},
      {path: 'aca004-sub03-score',component: Aca004Sub03ScoreComponent,canActivate:[AuthGuard]},
      {path: 'aca004-sub04',component: Aca004Sub04Component,canActivate:[AuthGuard]},
      {path: 'aca004-sub05',component: Aca004Sub05Component,canActivate:[AuthGuard]},
      {path: 'aca004-sub05/form',component: Aca004Sub05FormComponent,canActivate:[AuthGuard]},
      {path: 'aca004-sub06',component: Aca004Sub06Component,canActivate:[AuthGuard]},
      {path: 'aca004-sub06/form',component: Aca004Sub06FormComponent,canActivate:[AuthGuard]},
      {path: 'aca004-sub07',component: Aca004Sub07Component,canActivate:[AuthGuard]},
      {path: 'aca004-sub07/form',component: Aca004Sub07FormComponent,canActivate:[AuthGuard]},
      {path: 'aca004-sub08',component: Aca004Sub08Component,canActivate:[AuthGuard]},
      {path: 'aca004-sub09',component: Aca004Sub09Component,canActivate:[AuthGuard]},
      {path: 'aca004-sub09-evaluation',component: Aca004Sub09EvaluationComponent,canActivate:[AuthGuard]},
      {path: 'aca006',component: Aca006Component,canActivate:[AuthGuard]}, 
      {path: 'aca006/form',component: Aca006FormComponent,canActivate:[AuthGuard]}, 
      {path: 'aca006/form2',component: Aca006Form2Component,canActivate:[AuthGuard]},
      {path: 'aca008',component: Aca008Component,canActivate:[AuthGuard]},
      {path: 'aca008/report',component: Aca008ReportComponent,canActivate:[AuthGuard]},
      {path: 'aca009',component: Aca009Component,canActivate:[AuthGuard]},
      {path: 'aca011', component: Aca011Component,canActivate:[AuthGuard]},
      {path: 'aca011-report', component: Aca011ReportComponent,canActivate:[AuthGuard]},
      {path: 'aca011-report-eng', component: Aca011ReportEngComponent,canActivate:[AuthGuard]},
      {path: 'aca012', component: Aca012Component, canActivate: [AuthGuard]},
      {path: 'aca004-sub06/html',component: Aca004Sub06HtmlComponent,canActivate:[AuthGuard]},
      {path: 'aca014', component: Aca014Component,canActivate:[AuthGuard]},
      {path: 'aca014/html', component: Aca014HtmlComponent,canActivate:[AuthGuard]},
      {path: 'aca013',component: Aca013Component,canActivate:[AuthGuard]},
      {path: 'aca013/form1',component: Aca013Form1Component,canActivate:[AuthGuard]},
      {path: 'aca013/form2',component: Aca013Form2Component,canActivate:[AuthGuard]},
      {path: 'aca015',component: Aca015Component,canActivate:[AuthGuard]},
      {path: 'aca015/form1',component: Aca015Form1Component,canActivate:[AuthGuard]},
      {path: 'aca015/form2',component: Aca015Form2Component,canActivate:[AuthGuard]},
      {path: 'aca015/form3',component: Aca015Form3Component,canActivate:[AuthGuard]},
    ]
  }
];