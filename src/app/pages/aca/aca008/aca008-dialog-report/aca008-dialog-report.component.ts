import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { ApiService } from 'src/app/shared/service/api.service';
import { FormBuilder } from '@angular/forms';
import { UtilService } from 'src/app/_util/util.service';
import { Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';

@Component({ 
  selector: 'app-aca008-dialog-report',
  templateUrl: './aca008-dialog-report.component.html',
  styleUrls: ['./aca008-dialog-report.component.scss']
}) 
export class Aca008DialogReportComponent implements OnInit {

  searchForm = this.fb.group({
    'yearGroupId':['']
    , 'activityGroupId':[''] 
  });

  acgroupId1:any;
  acgroupId2:any;
  acgroupId3:any;
  acgroupId4:any;
  acgroupName1:any;
  acgroupName2:any;
  acgroupName3:any;
  acgroupName4:any;
  yearGroupName:any;

  yearGroupList:[];

  constructor(
    private translate: TranslateService,
    @Inject(MAT_DIALOG_DATA) data:any,
    private service:ApiService,
    public dialogRef: MatDialogRef<Aca008DialogReportComponent>, 
    private fb: FormBuilder, 
    public util: UtilService,
    private router: Router
  ) { }

  ngOnInit() {
    this.ddlYearGroup();  
  }

  ddlYearGroup(){
    this.service.httpGet('/api/v1/0/year-group/ddl',null).then((res)=>{
      this.yearGroupList = res||[];
      this.searchForm.controls.yearGroupId.setValue(res[0]['yearGroupId']);
    });
  }

  onPreview(){
    let ele = this.yearGroupList.filter((o)=>{
      return o['yearGroupId'] == this.searchForm.value.yearGroupId
    });
    this.yearGroupName = ele[0]['name'];

    localStorage.setItem('rp_yearGroupId',this.searchForm.value.yearGroupId);
    localStorage.setItem('rp_yearGroupName',this.yearGroupName);
    this.router.navigate(['/aca/aca008/report']);
    this.dialogRef.close();
  }

  close() {
    this.dialogRef.close(); 
  } 

}
