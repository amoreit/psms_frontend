import { Component, OnInit, ViewChild } from '@angular/core';
import { MatTableDataSource, MatDialog } from '@angular/material';
import { FormBuilder } from '@angular/forms';
import { ApiService, IResponse } from 'src/app/shared/service/api.service';
import { UtilService } from 'src/app/_util/util.service';
import { Aca008DialogComponent } from 'src/app/dialog/aca008-dialog/aca008-dialog.component';
import Swal from 'sweetalert2/dist/sweetalert2.js';
import 'sweetalert2/src/sweetalert2.scss';
import { Aca008TectherDialogComponent } from 'src/app/dialog/aca008-tecther-dialog/aca008-tecther-dialog.component';
import { Aca008DialogReportComponent } from './aca008-dialog-report/aca008-dialog-report.component';
import { TranslateService } from '@ngx-translate/core';

@Component({ 
  selector: 'app-aca008',
  templateUrl: './aca008.component.html',
  styleUrls: ['./aca008.component.scss']
})
export class Aca008Component implements OnInit {

  searchForm = this.fb.group({
    'yearGroupName':['']
    , 'activityGroupId':['']
    , 'classId':['']
  }); 

  _activityGroupId:any;
  userName:any;
  yearGroupId:any;
  classSnameTh:any;
  activityGroup:any;
  checkSearch = false;

  yearGroupList:[];
  classList:[]; 
  activityGroupList:[];
  dataSourceActivity:[]; 
  dataSourceActivityList:[];
  
  displayedColumns: string[] = ['yearGroupName','classRoom','classRoomNo','fullname','acName','acLesson','manage']; 
  displayedColumnsTea: string[] = ['no','fullname','manage']; 

  dataSource = new MatTableDataSource();
  dataSourceTea = new MatTableDataSource();

  constructor(
    private translate: TranslateService,
      private fb: FormBuilder,
      private service: ApiService,
      public util: UtilService,
      public dialog: MatDialog 
  ) { } 
  
  ngOnInit() { 
    this.userName = localStorage.getItem('userName')||'';
    this.ddlYearGroup();
    this.ddlClass(); 
  } 

  ddlYearGroup(){ 
    this.service.httpGet('/api/v1/0/year-group/ddl',null).then((res)=>{
      this.yearGroupList = res||[];
      this.searchForm.controls.yearGroupName.setValue(res[0]['name']);
    });
  }

  ddlClass(){ 
    this.service.httpGet('/api/v1/0/tbMstClass/ddl',null).then((res)=>{
      this.classList= res||[];
    });  
  }

  onfilterClass(){
    let ele = this.classList.filter((o)=>{ 
      return o['classId'] == this.searchForm.value.classId;
    });
    this.classSnameTh =  ele[0]['classSnameTh'];
  }

  ddlActivityGroup(){
    if(this.searchForm.value.classId == 1 || this.searchForm.value.classId == 2 || this.searchForm.value.classId == 3){
      this.service.httpGet('/api/v1/0/activity-grouplist/kindergarten',null).then((res)=>{
        this.activityGroupList= res||[];
      }); 
    }else{
      this.service.httpGet('/api/v1/0/activity-grouplist/juniorup',null).then((res)=>{
        this.activityGroupList= res||[];
      }); 
    }
  }

  onfilterActivityGroup(){
    let ele = this.activityGroupList.filter((o)=>{ 
      return o['activityGroupId'] == this.searchForm.value.activityGroupId;
    });
    this.activityGroup =  ele[0]['activityGroup'];
  }
   
  onSearch(){ 
    this.checkSearch = true;
    this.service.httpGet('/api/v1/_activity',this.searchForm.value).then((res:IResponse)=>{ 
      this.dataSourceActivity = res.responseData||[];
      for(let i=0; i <= this.dataSourceActivity.length-1; i++){
        this._activityGroupId = this.dataSourceActivity[i]['activityGroupId'];

        let jsonTea = {'activityName':this.dataSourceActivity[i]['activityName'],'activityGroupId':this.dataSourceActivity[i]['activityGroupId'],'yearGroupName':this.searchForm.value.yearGroupName,'classId':this.searchForm.value.classId};
        this.service.httpGet('/api/v1/_activityTeacher',jsonTea).then((res:IResponse)=>{
            this.dataSourceTea.data[i] = res.responseData||[];
        });

        let json = {'activityName':this.dataSourceActivity[i]['activityName'],'activityGroupId':this.dataSourceActivity[i]['activityGroupId'],'yearGroupName':this.searchForm.value.yearGroupName,'classId':this.searchForm.value.classId};
        this.service.httpGet('/api/v1/_trnClassRoomMember/activity-stu',json).then((res:IResponse)=>{
            this.dataSource.data[i] = res.responseData||[];
        });
      } 
    }); 
  } 

  onAddstudents(activityGroupId, activityId, activityName, activityLesson): void {
    localStorage.setItem('classId',this.searchForm.value.classId);
    const dialogRef = this.dialog.open(Aca008DialogComponent, {
      width: '60%'
      , 'autoFocus': true 
      , 'disableClose':true
      , data:{'yearGroupName':this.searchForm.value.yearGroupName,'activityGroupId':activityGroupId, 'activityId':activityId, 'activityName':activityName, 'activityLesson':activityLesson}
    });
    dialogRef.afterClosed().subscribe(result => {
      if((result||'') == ''){
        this.searchForm.controls.classId.setValue(localStorage.getItem('dialog_classId'));
        this.searchForm.controls.activityGroupId.setValue(localStorage.getItem('dialog_activityGroupId'));
        this.onSearch();
      }
    });
  } 

  onAddteachers(activityGroupId, activityId, activityName): void {
    localStorage.setItem('classId',this.searchForm.value.classId);
    const dialogRef = this.dialog.open(Aca008TectherDialogComponent, {
      width: '60%'
      , 'autoFocus': true
      , 'disableClose':true
      , data:{'yearGroupName':this.searchForm.value.yearGroupName,'activityGroupId':activityGroupId, 'activityId':activityId, 'activityName':activityName}
    });
    dialogRef.afterClosed().subscribe(result => {
      if((result||'') == ''){
        this.searchForm.controls.classId.setValue(localStorage.getItem('dialogTea_classId'));
        this.searchForm.controls.activityGroupId.setValue(localStorage.getItem('dialogTea_activityGroupId'));
        this.onSearch();
      }
    }); 
  }

  onDelete(acGroupId,id){
    Swal.fire({
        title: this.translate.instant('alert.delete'), //ต้องการลบข้อมูล ใช่หรือไม่
        icon : 'info',
        showCancelButton: true,
        confirmButtonColor: '#3085d6', 
        cancelButtonColor: '#d33',
        confirmButtonText: this.translate.instant('psms.DL0008'),//ใช่
        cancelButtonText: this.translate.instant('psms.DL0009')//ไม่
      }).then((result) => {
      if(result.dismiss == 'cancel'){
          return;
      } 
      let json = {'classRoomMemberId':id,'activityGroupId':acGroupId};
      this.service.httpPut('/api/v1/_trnClassRoomMember/delete/'+this.userName,json).then((res:IResponse)=>{
        if((res.responseCode||500)!=200){
            Swal.fire(
              this.translate.instant('message.delete_error'),//ลบข้อมูลครูผิดพลาด
              res.responseMsg,
              'error' 
            )
          return;
        }
        Swal.fire( 
          this.translate.instant('psms.B0024'),//ลบข้อมูล
          this.translate.instant('message.delete'),//ลบข้อมูลสำเร็จ
          'success'
        ).then(() => this.reload());
      });
    });
  }

  onDeleteTeacher(id){
    Swal.fire({
        title: this.translate.instant('alert.delete'),//ต้องการลบข้อมูล ใช่หรือไม่
        icon : 'info',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: this.translate.instant('psms.DL0008'),//ใช่
        cancelButtonText: this.translate.instant('psms.DL0009')//ไม่
      }).then((result) => {
      if(result.dismiss == 'cancel'){
          return;
      } 
      let json = {'activityTeacherId':id};
      this.service.httpDelete('/api/v1/_activityTeacher',json).then((res:IResponse)=>{
        if((res.responseCode||500)!=200){
            Swal.fire(
              this.translate.instant('message.delete_error'),//ลบข้อมูลผิดพลาด
              res.responseMsg,
              'error' 
            )
          return;
        }
        Swal.fire( 
          this.translate.instant('psms.B0024'),//ลบข้อมูล
          this.translate.instant('message.delete'),//ลบข้อมูลสำเร็จ
          'success'
        ).then(() => this.reload());
      });
    });
  }

  onDialogReport(): void {
    const dialogRef = this.dialog.open(Aca008DialogReportComponent, {
      width: '45%'
      , 'autoFocus': true
      , 'disableClose':true
    });
    dialogRef.afterClosed().subscribe(result => {
      if((result||'') == ''){
        this.searchForm.controls.classId.setValue(this.searchForm.value.classId);
        this.searchForm.controls.activityGroupId.setValue(this.searchForm.value.activityGroupId);
        this.onSearch();
      }
    });
  }

  reload(){
    this.searchForm.controls.classId.setValue(this.searchForm.value.classId);
    this.searchForm.controls.activityGroupId.setValue(this.searchForm.value.activityGroupId);
    this.onSearch();
  }

}
 