import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ApiService, IResponse } from 'src/app/shared/service/api.service';
import { MatTableDataSource } from '@angular/material';

@Component({
  selector: 'app-aca008-report',
  templateUrl: './aca008-report.component.html', 
  styleUrls: ['./aca008-report.component.scss']
})
export class Aca008ReportComponent implements OnInit {

  yearGroupId:any;
  yearGroupName:String;
  acgroupId1:any;
  acgroupId2:any;
  acgroupId3:any;
  acgroupId4:any;
  acgroupName1:any;
  acgroupName2:any; 
  acgroupName3:any;
  acgroupName4:any;

  dataSourceAc1:any;
  listAc1:[];
  dataSourceAc2:any;
  listAc2:[];
  dataSourceAc3:any;
  listAc3:[];
  dataSourceAc4:any;
  listAc4:[];
  activityGroupList:[];

  displayedColumns: string[] = ['yearGroupName','classRoom','classRoomNo','fullname','acName','acLesson']; 
  displayedColumnsTea: string[] = ['no','fullname','classNo']; 

  dataSourceAc1Stu = new MatTableDataSource();
  dataSourceAc2Stu = new MatTableDataSource();
  dataSourceAc3Stu = new MatTableDataSource();
  dataSourceAc4Stu = new MatTableDataSource();
  dataSourceAc1Tea = new MatTableDataSource();
  dataSourceAc2Tea = new MatTableDataSource();
  dataSourceAc3Tea = new MatTableDataSource();
  dataSourceAc4Tea = new MatTableDataSource();

  constructor(
    private service:ApiService,
    private router: Router
  ) { }

  ngOnInit() {
    this.yearGroupId = localStorage.getItem('rp_yearGroupId');
    this.yearGroupName = localStorage.getItem('rp_yearGroupName');
    this.service.httpGet('/api/v1/0/activity-grouplist/juniorup',null).then((res)=>{
        this.activityGroupList= res||[];
        this.acgroupId1 = res[0]['activityGroupId'];
        this.acgroupId2 = res[1]['activityGroupId'];
        this.acgroupId3 = res[2]['activityGroupId'];
        this.acgroupId4 = res[3]['activityGroupId']; 
        this.acgroupName1 = res[0]['activityGroup'];
        this.acgroupName2 = res[1]['activityGroup'];
        this.acgroupName3 = res[2]['activityGroup'];
        this.acgroupName4 = res[3]['activityGroup'];
    }).then(()=>this.onSearchAc1());
  }

  onSearchAc1(){
    let pushListAc1 = [];
    this.service.httpGet('/api/v1/_activity/activity-stu-report',{'activityGroupId':this.acgroupId1}).then((res:IResponse)=>{ 
        this.listAc1 = res.responseData||[];
        for(let i=0; i <  this.listAc1.length; i++){
          pushListAc1.push(this.listAc1[i]['activityName']);
        }
        this.dataSourceAc1 = [...new Set(pushListAc1)];
        for(let i=0; i < this.dataSourceAc1.length; i++){
            let jsonTea = {'activityName':this.dataSourceAc1[i],'activityGroupId':this.acgroupId1,'yearGroupName':this.yearGroupName};
            this.service.httpGet('/api/v1/_activityTeacher/activity-tea-report',jsonTea).then((res:IResponse)=>{
                this.dataSourceAc1Tea.data[i] = res.responseData||[];
            });
            let json = {'activityName':this.dataSourceAc1[i],'activityGroupId':this.acgroupId1,'yearGroupName':this.yearGroupName};
            this.service.httpGet('/api/v1/_trnClassRoomMember/activity-stu-report',json).then((res:IResponse)=>{
                this.dataSourceAc1Stu.data[i] = res.responseData||[];
            });
        } 
    }).then(()=>this.onSearchAc2());
  }

  onSearchAc2(){
    let pushListAc2 = [];
    this.service.httpGet('/api/v1/_activity/activity-stu-report',{'activityGroupId':this.acgroupId2}).then((res:IResponse)=>{ 
        this.listAc2 = res.responseData||[];
        for(let i=0; i <  this.listAc2.length; i++){
          pushListAc2.push( this.listAc2[i]['activityName'])
        }
        this.dataSourceAc2 = [...new Set(pushListAc2)];
        for(let i=0; i < this.dataSourceAc2.length; i++){
            let jsonTea = {'activityName':this.dataSourceAc2[i],'activityGroupId':this.acgroupId2,'yearGroupName':this.yearGroupName};
            this.service.httpGet('/api/v1/_activityTeacher/activity-tea-report',jsonTea).then((res:IResponse)=>{
                this.dataSourceAc2Tea.data[i] = res.responseData||[];
            });
            let json = {'activityName':this.dataSourceAc2[i],'activityGroupId':this.acgroupId2,'yearGroupName':this.yearGroupName};
            this.service.httpGet('/api/v1/_trnClassRoomMember/activity-stu-report',json).then((res:IResponse)=>{
                this.dataSourceAc2Stu.data[i] = res.responseData||[];
            });
        } 
    }).then(()=>this.onSearchAc3()); 
  }

  onSearchAc3(){
    let pushListAc3 = [];
      this.service.httpGet('/api/v1/_activity/activity-stu-report',{'activityGroupId':this.acgroupId3}).then((res:IResponse)=>{ 
          this.listAc3 = res.responseData||[];
          for(let i=0; i <  this.listAc3.length; i++){
            pushListAc3.push( this.listAc3[i]['activityName'])
          }
          this.dataSourceAc3 = [...new Set(pushListAc3)];
          for(let i=0; i < this.dataSourceAc3.length; i++){
              let jsonTea = {'activityName':this.dataSourceAc3[i],'activityGroupId':this.acgroupId3,'yearGroupName':this.yearGroupName};
              this.service.httpGet('/api/v1/_activityTeacher/activity-tea-report',jsonTea).then((res:IResponse)=>{
                  this.dataSourceAc3Tea.data[i] = res.responseData||[];
              });
              let json = {'activityName':this.dataSourceAc3[i],'activityGroupId':this.acgroupId3,'yearGroupName':this.yearGroupName};
              this.service.httpGet('/api/v1/_trnClassRoomMember/activity-stu-report',json).then((res:IResponse)=>{
                  this.dataSourceAc3Stu.data[i] = res.responseData||[];
              });
          } 
      }).then(()=>this.onSearchAc4()); 
  }
  
  onSearchAc4(){
    let pushListAc4 = [];
    this.service.httpGet('/api/v1/_activity/activity-stu-report',{'activityGroupId':this.acgroupId4}).then((res:IResponse)=>{ 
        this.listAc4 = res.responseData||[];
        for(let i=0; i <  this.listAc4.length; i++){
          pushListAc4.push( this.listAc4[i]['activityName'])
        }
        this.dataSourceAc4 = [...new Set(pushListAc4)];
        for(let i=0; i < this.dataSourceAc4.length; i++){
            let jsonTea = {'activityName':this.dataSourceAc4[i],'activityGroupId':this.acgroupId4,'yearGroupName':this.yearGroupName};
            this.service.httpGet('/api/v1/_activityTeacher/activity-tea-report',jsonTea).then((res:IResponse)=>{
                this.dataSourceAc4Tea.data[i] = res.responseData||[];
            });
            let json = {'activityName':this.dataSourceAc4[i],'activityGroupId':this.acgroupId4,'yearGroupName':this.yearGroupName};
            this.service.httpGet('/api/v1/_trnClassRoomMember/activity-stu-report',json).then((res:IResponse)=>{
                this.dataSourceAc4Stu.data[i] = res.responseData||[];
            });
        } 
    });
  }

  onPrint(id) {
    let printContents = document.getElementById(id).innerHTML;
    let originalContents = document.body.innerHTML;
    document.body.innerHTML = printContents;
    window.print();
    document.body.innerHTML = originalContents;
    location.reload();
  }

  onBack(){
    this.router.navigate(['/aca/aca008']);
  }

}
