import { Component, OnInit, ViewChild, Inject, ElementRef } from '@angular/core';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { MatPaginator, MatTableDataSource } from '@angular/material';
import { ApiService, IResponse } from '../../../shared/service/api.service';
import { Router } from '@angular/router';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import Swal from 'sweetalert2/dist/sweetalert2.js';
import 'sweetalert2/src/sweetalert2.scss';
import { UtilService } from 'src/app/_util/util.service';
import * as XLSX from 'xlsx';
import { ResourceLoader } from '@angular/compiler';
import { TranslateService } from '@ngx-translate/core';

type ex = any[][];
 
@Component({
  selector: 'app-aca009',
  templateUrl: './aca009.component.html',
  styleUrls: ['./aca009.component.scss']
})
export class Aca009Component implements OnInit {

  dataOnet: ex = [];
  wopts: XLSX.WritingOptions = { bookType: 'xlsx', type: 'array' };
  fileName: string = 'SheetJS.xlsx';

  objOnet:any;
  excelPath:any;
  checkOnetList:[];

  score1:any;
  score2:any;
  score3:any;
  score4:any;
  score5:any;
  score6:any;
  score7:any;
  score8:any;
  score9:any;
  score10:any;
  scoreAvg:any;

  @ViewChild(MatPaginator, { static: true}) paginator: MatPaginator;
  constructor(private fb: FormBuilder,
    private service: ApiService,
    private router: Router,
    public dialog: MatDialog,
    public util: UtilService,
    private translate: TranslateService
  ) { }
  
  ngOnInit() {}

  onFileChange(evt:any) {
    /* wire up file reader */
    const target: DataTransfer = <DataTransfer>(evt.target);
    let filename = target.files[0]['name'];
    let typefile = filename.slice(-4);
    if(typefile != 'xlsx' ){
      Swal.fire(
        this.translate.instant('alert.error_doc'),//อัพโหลดเอกสารผิดพลาด
        this.translate.instant('message.type_file'),//สามารถอัพโหลดเอกสาร เฉพาะสกุลไฟล์ .xlsx
        'error' 
      )
    }else{
      this.excelPath = target.files[0]['name'];
      if (target.files.length !== 1) throw new Error('Cannot use multiple files'); const reader: FileReader = new FileReader(); reader.onload = (e: any) => {
        /* read workbook */ 
        const bstr: string = e.target.result;
        const wb: XLSX.WorkBook = XLSX.read(bstr, { type: 'binary' });
    
        /* grab first sheet */
        const wsname: string = wb.SheetNames[0];
        const ws: XLSX.WorkSheet = wb.Sheets[wsname];
        
        /* save data */
        this.objOnet = [];
        this.dataOnet = <ex>(XLSX.utils.sheet_to_json(ws, { header: 1, range: 1}));
        for(let i =0; i < this.dataOnet.length; i++){
            let onetList = { 
                  "citizenId":this.dataOnet[i][2], 
                  "fullname":this.dataOnet[i][3], 
                  "subjectScore1":this.dataOnet[i][4],
                  "subjectScore2":this.dataOnet[i][5],
                  "subjectScore3":this.dataOnet[i][6],
                  "subjectScore4":this.dataOnet[i][7],
                  "subjectScore5":this.dataOnet[i][8],
                  "subjectScore6":this.dataOnet[i][9],
                  "subjectScore7":this.dataOnet[i][10],
                  "subjectScore8":this.dataOnet[i][11],
                  "subjectScore9":this.dataOnet[i][12],
                  "subjectScore10":this.dataOnet[i][13],
                  "subjectScoreAvg":this.dataOnet[i][14],
            };
            this.objOnet.push(onetList);
        }
      };
      reader.readAsBinaryString(target.files[0]);
    }
  }

  onSave(){
    Swal.fire({
      title: this.translate.instant('psms.DL0001'),//ต้องการบันทึกข้อมูล ใช่หรือไม่?
      icon : 'info',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: this.translate.instant('psms.DL0008'),
      cancelButtonText: this.translate.instant('psms.DL0009')
    }).then((result) => {
      if(result.dismiss == 'cancel'){
          return;
      } 
      for(let i =0; i < this.objOnet.length; i++){
        let json = { "citizenId":this.objOnet[i]['citizenId']};
        this.service.httpGet('/api/v1/0/_onet/ddl',json).then((res)=>{
          this.checkOnetList = res||[];
          if(this.checkOnetList.length == 0){
            this.service.httpPost('/api/v1/_onet',this.objOnet).then((res:IResponse)=>{
                if((res.responseCode||500)!=200){
                    Swal.fire(
                      this.translate.instant('message.save_error'),//บันทึกข้อมูลผิดพลาด
                      res.responseMsg,
                      'error' 
                    )
                  return;
                }
            });
          }else{
            let stuOnetScoreId = res[0]['stuOnetScoreId'];
            if(this.objOnet[i]['subjectScore1'] == '-'){this.score1 = 0;}else{this.score1 = this.objOnet[i]['subjectScore1'];}
            if(this.objOnet[i]['subjectScore2'] == '-'){this.score2 = 0;}else{this.score2 = this.objOnet[i]['subjectScore2'];}
            if(this.objOnet[i]['subjectScore3'] == '-'){this.score3 = 0;}else{this.score3 = this.objOnet[i]['subjectScore3'];}
            if(this.objOnet[i]['subjectScore4'] == '-'){this.score4 = 0;}else{this.score4 = this.objOnet[i]['subjectScore4'];}
            if(this.objOnet[i]['subjectScore5'] == '-'){this.score5 = 0;}else{this.score5 = this.objOnet[i]['subjectScore5'];}
            if(this.objOnet[i]['subjectScore6'] == '-'){this.score6 = 0;}else{this.score6 = this.objOnet[i]['subjectScore6'];}
            if(this.objOnet[i]['subjectScore7'] == '-'){this.score7 = 0;}else{this.score7 = this.objOnet[i]['subjectScore7'];}
            if(this.objOnet[i]['subjectScore8'] == '-'){this.score8 = 0;}else{this.score8 = this.objOnet[i]['subjectScore8'];}
            if(this.objOnet[i]['subjectScore9'] == '-'){this.score9 = 0;}else{this.score9 = this.objOnet[i]['subjectScore9'];}
            if(this.objOnet[i]['subjectScore10'] == '-'){this.score10 = 0;}else{this.score10 = this.objOnet[i]['subjectScore10'];}
            if(this.objOnet[i]['subjectScoreAvg'] == '-'){this.scoreAvg = 0;}else{this.scoreAvg = this.objOnet[i]['subjectScoreAvg'];}

            let json = {
                  "stuOnetScoreId":stuOnetScoreId,
                  "citizenId":this.objOnet[i]['citizenId'], 
                  "fullname":this.objOnet[i]['fullname'], 
                  "subjectScore1":this.score1,
                  "subjectScore2":this.score2,
                  "subjectScore3":this.score3,
                  "subjectScore4":this.score4,
                  "subjectScore5":this.score5,
                  "subjectScore6":this.score6,
                  "subjectScore7":this.score7,
                  "subjectScore8":this.score8,
                  "subjectScore9":this.score9,
                  "subjectScore10":this.score10,
                  "subjectScoreAvg":this.scoreAvg,
            };
            this.service.httpPut('/api/v1/_onet',json).then((res:IResponse)=>{
              if((res.responseCode||500)!=200){
                  Swal.fire(
                    this.translate.instant('message.edit_error'),//แก้ไขข้อมูลผิดพลาด
                    res.responseMsg,
                    'error' 
                  )
                return;
              }
            });
          }
        }); 
      }Swal.fire( 
        this.translate.instant('message.save_header'),//บันทึกข้อมูล
        this.translate.instant('message.save'),//บันทึกข้อมูลสำเร็จ
        'success' 
      );
    });
  }
  
  onCancel() {
    this.excelPath = '';
    location.reload();
  }

}
