import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ApiService, IResponse } from 'src/app/shared/service/api.service';
import { MatDialog, MatTableDataSource, MatPaginator } from '@angular/material';
import { UtilService } from 'src/app/_util/util.service';
import Swal from 'sweetalert2/dist/sweetalert2.js';
import 'sweetalert2/src/sweetalert2.scss';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-aca002-sub02',
  templateUrl: './aca002-sub02.component.html',
  styleUrls: ['./aca002-sub02.component.scss']
})
export class Aca002Sub02Component implements OnInit {

  isProcess: boolean = false;
  searchForm = this.fb.group({
    'yearGroupId': ['']
    , 'classId': [''] 
  });

  copyForm = this.fb.group({
    'yearGroupId': ['']
    , 'copyDataTeacher': [false]
  });

  pageSize: number;
  displayedColumns: string[] = ['subjectCode', 'subjectName', 'credit', 'choose'];
  dataSource = new MatTableDataSource();
  classList: [];
  classroomnoList: [];
  yearGroupList: [];
  yearGroupCopyList: [];
  arrSubject = [];
  _allChecklist = [];
  checkdata = false;
  headerSelected: boolean;
  year_group_name: any;
  year_group_search: any;

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  constructor(private fb: FormBuilder,
    private service: ApiService,
    public dialog: MatDialog,
    private translate: TranslateService,
    public _util: UtilService) {
  }

  ngOnInit() {
    this.ddlClass();
    this.ddlYearGroup();
    this.onSearch(0);

  }

  onSearch(e) {
    this.headerSelected = false;
    this.service.httpGet('/api/v1/trn-class-room-subject/subjectincopy', this.searchForm.value).then((res: IResponse) => {
      if (res.responseData.length === 0) {
        this.dataSource.data = []
        this.checkdata = true;
      } else {
        this.dataSource.data = res.responseData || [];
        this.checkdata = false;
      }
    });
    this.service.httpGet('/api/v1/trn-class-room-subject/subjectallinclassroomcopy', this.searchForm.value).then((res: IResponse) => {
      this.arrSubject = res.responseData || []
    });
  }

  onCancel() {
    this.searchForm = this.fb.group({
      'yearGroupId': ['']
      , 'classId': ['']
    });
    this.onSearch(0);
  }


  ddlClass() {
    this.service.httpGet('/api/v1/0/class/ddl', null).then((res) => {
      this.classList = res || [];
    });
  }

  ddlYearGroup() {
    this.service.httpGet('/api/v1/0/year-group/ddl', null).then((res) => {
      this.yearGroupList = res || [];
      let ele = this.yearGroupList.filter((o) => {
        return o['iscurrent'] == "true";
      });
      this.searchForm.controls.yearGroupId.setValue(ele[0]['yearGroupId'])
      this.checkYearGroup(this.searchForm.value.yearGroupId);
    });
  }
 
  checkYearGroup(txt) {
    let ele = this.yearGroupList.filter((o) => {
      return o['yearGroupId'] == txt;
    });
    this.year_group_name = ele[0]['name']
    this.year_group_search = this.year_group_name.substring(2) + this.year_group_name.substring(0, 1)

    this.service.httpGet('/api/v1/0/year-group/ddlupperyeargroup', { 'yearGroupSearch': this.year_group_search }).then((res) => {
      this.yearGroupCopyList = res || [];
    });
  }

  checkAll() {
    for (var i = 0; i < this.dataSource.data.length; i++) {
      this.dataSource.data[i]['isSelected'] = this.headerSelected;
    }
    this.getCheckedItemList();
  }

  isCancelCheck() {
    for (var i = 0; i < this.dataSource.data.length; i++) {
      this.dataSource.data[i]['isSelected'] = false;
    }
    this.headerSelected = false;
    this._allChecklist = [];
  }

  isAllSelected() {
    this.dataSource.data.every((item: any) => {
      return item.isSelected === true;
    })
    this.headerSelected = false;
    this.getCheckedItemList();
  }

  getCheckedItemList() {
    this._allChecklist = [];
    if (this.copyForm.value.copyDataTeacher == false) {
      for (var i = 0; i < this.arrSubject.length; i++) {
        for (var j = 0; j < this.dataSource.data.length; j++) {
          if (this.dataSource.data[j]['isSelected']) {
            if (this.dataSource.data[j]['subject_code'] == this.arrSubject[i]['subjectCode']) {
              let itemData = {
                'scCode': this.arrSubject[i]['scCode'], 'yearGroupId': '', 'classRoomId': this.arrSubject[i]['classRoomId'],
                'departmentId': this.arrSubject[i]['departmentId'], 'subjectCode': this.arrSubject[i]['subjectCode'],
                'subjectCode2': this.arrSubject[i]['subjectCode2'], 'subjectNameEn': this.arrSubject[i]['subjectNameEn'],
                'subjectNameTh': this.arrSubject[i]['subjectNameTh'], 'scoreId': this.arrSubject[i]['scoreId'],
                'orders': this.arrSubject[i]['orders'], 'credit': this.arrSubject[i]['credit'],
                'lesson': this.arrSubject[i]['lesson'], 'weight': this.arrSubject[i]['weight'],
                'teacher1': null, 'teacherFullname1': null,
                'teacher2': null, 'teacherFullname2': null
              }
              this._allChecklist.push(itemData);
            }
          }
        }
      }
    } else {
      for (var i = 0; i < this.arrSubject.length; i++) {
        for (var j = 0; j < this.dataSource.data.length; j++) {
          if (this.dataSource.data[j]['isSelected']) {
            if (this.dataSource.data[j]['subject_code'] == this.arrSubject[i]['subjectCode']) {
              let itemData = {
                'scCode': this.arrSubject[i]['scCode'], 'yearGroupId': '', 'classRoomId': this.arrSubject[i]['classRoomId'],
                'departmentId': this.arrSubject[i]['departmentId'], 'subjectCode': this.arrSubject[i]['subjectCode'],
                'subjectCode2': this.arrSubject[i]['subjectCode2'], 'subjectNameEn': this.arrSubject[i]['subjectNameEn'],
                'subjectNameTh': this.arrSubject[i]['subjectNameTh'], 'scoreId': this.arrSubject[i]['scoreId'],
                'orders': this.arrSubject[i]['orders'], 'credit': this.arrSubject[i]['credit'],
                'lesson': this.arrSubject[i]['lesson'], 'weight': this.arrSubject[i]['weight'],
                'teacher1': this.arrSubject[i]['teacher1'], 'teacherFullname1': this.arrSubject[i]['teacherFullname1'],
                'teacher2': this.arrSubject[i]['teacher2'], 'teacherFullname2': this.arrSubject[i]['teacherFullname2']
              }
              this._allChecklist.push(itemData);
            }
          } 
        }
      }
    }
  } 

  onSave2() {
    this._allChecklist.map(e => {
      e['yearGroupId'] = this.copyForm.value.yearGroupId;
      return e;
    });
    Swal.fire({
      title: this.translate.instant('alert.copy_subject'),//ต้องการอนุมัติการคัดลอกรายวิชา ใช่หรือไม่
      icon: 'info',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: this.translate.instant('psms.DL0008'),
      cancelButtonText: this.translate.instant('psms.DL0009')
    }).then((result) => {
      if (result.dismiss == 'cancel') {
        return;
      }
      let json = this._allChecklist;
      let user = localStorage.getItem('userName')
      if (json.length === 0 || this.copyForm.value.yearGroupId == '') {
        Swal.fire(
          this.translate.instant('alert.copy_subject_error'),//คัดลอกวิชาผิดพลาด
          this.translate.instant('alert.validate'),
          'error'
        )
      }
      else {
        this.service.httpPost('/api/v1/trn-class-room-subject/copy/' + user, json).then((res: IResponse) => {
          if ((res.responseCode || 500) != 200) {
            Swal.fire(
              this.translate.instant('alert.copy_subject_error'),
              res.responseMsg,
              'error'
            )
            return;
          }
          Swal.fire(
            this.translate.instant('alert.copy_subject_header'),//คัดลอกรายวิชา
            this.translate.instant('alert.copy_subject_success'),//คัดลอกรายวิชาสำเร็จ
            'success'
          ).then(() => { })
        });
      }
      return;
    });
  }

}
