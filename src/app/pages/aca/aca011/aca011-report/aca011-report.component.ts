import { Component, OnInit } from '@angular/core';
import { ApiService, IResponse } from 'src/app/shared/service/api.service';
import { FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
@Component({
  selector: 'app-aca011-report',
  templateUrl: './aca011-report.component.html',
  styleUrls: ['./aca011-report.component.scss']
})
export class Aca011ReportComponent implements OnInit {

  isProcess = false; 
  searchForm = this.fb.group({
    yearGroupName: [''],
    classId: [''],
    classRoomId: [''],
    result: [1000]
  });

  searchForm2 = this.fb.group({
    yearGroupName: [''],
    classId: [''],
    classRoomId: [''],
    result: [1000]
  });

  dataSourceTerm1: any;
  dataSourceTerm2: [];

  dataTerm1: any;
  dataTerm2: any;
  allChecklist: any;

  constructor(
    private translate: TranslateService,
    private service: ApiService,
    private fb: FormBuilder,
    private router: Router,
  ) {
    this.isProcess = true;
    this.allChecklist = [];
    this.allChecklist = JSON.parse(localStorage.getItem('allChecklist'));
  }

  ngOnInit() {
    this.onSearchTerm1();
    this.onSearchTerm2();
  }

  onSearchTerm1() {
    this.dataTerm1 = [];
    for (let i = 0; i < this.allChecklist.length; i++) {
      setTimeout(() => {
        this.searchForm.value.yearGroupName = this.allChecklist[i].yearGroupName;
        this.searchForm.value.classId = this.allChecklist[i].classId;
        this.searchForm.value.classRoomId = this.allChecklist[i].classRoomId;

        this.service.httpGet('/api/v1/stu-plan/term1', this.searchForm.value).then((res: IResponse) => {
          this.dataSourceTerm1 = res.responseData || [];
          this.dataTerm1.push(this.dataSourceTerm1);
        });
      }, i * 300);
    }
  }

  onSearchTerm2() {
    this.dataTerm2 = [];
    for (let i = 0; i < this.allChecklist.length; i++) {
      setTimeout(() => {
        this.searchForm2.value.yearGroupName = this.allChecklist[i].yearGroupName;
        this.searchForm2.value.classId = this.allChecklist[i].classId;
        this.searchForm2.value.classRoomId = this.allChecklist[i].classRoomId;

        this.service.httpGet('/api/v1/stu-plan/term2', this.searchForm2.value).then((res: IResponse) => {
          this.dataSourceTerm2 = res.responseData || [];
          this.dataTerm2.push(this.dataSourceTerm2);

        });
      }, i * 300);
    }
  }

  printComponent(cmpName) {
    const printContents = document.getElementById(cmpName).innerHTML;
    const originalContents = document.body.innerHTML;

    document.body.innerHTML = printContents;
    window.print();
    document.body.innerHTML = originalContents;
    window.close();
    window.location.reload(true);

  }

  onBackPage() {
    this.router.navigate(['/aca/aca011']);
  }

}
