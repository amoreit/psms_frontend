import { Component, OnInit, ViewChild } from '@angular/core';
import { ApiService, IResponse } from 'src/app/shared/service/api.service';
import { FormBuilder } from '@angular/forms';
import { MatTableDataSource, MatPaginator } from '@angular/material';
import { Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-aca011',
  templateUrl: './aca011.component.html',
  styleUrls: ['./aca011.component.scss']
})
export class Aca011Component implements OnInit {

  searchForm = this.fb.group({
    yearGroupName: [''],
    classId: [''],
    langCode: ['1'],
    result: [10],
    p: ['']
  });

  yearGroupList: [];
  classList: [];

  displayedColumns: string[] = ['check', 'no', 'class', 'classRoom'];
  dataSource = new MatTableDataSource();

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  pageSize: number;

  headerSelected: boolean;
  isSelected: any;
  checklist: any;
  allChecklist: any;

  checkChoose = false;

  constructor(
    private translate: TranslateService,
    private service: ApiService,
    private fb: FormBuilder,
    private router: Router
  ) { }

  ngOnInit() {
    this.ddlClass();
    this.ddlYearGroup();
    this.onSearch(0);
  }

  ddlYearGroup() {
    this.service.httpGet('/api/v1/0/year-group/ddl', null).then((res) => {
      this.yearGroupList = res || [];
      this.searchForm.controls.yearGroupName.setValue(res[0].name);
    });
  }

  ddlClass() {
    this.service.httpGet('/api/v1/0/class/ddl', null).then((res) => {
      this.classList = res || [];
      this.searchForm.controls.classId.setValue(undefined);
    });
  }


  onSearch(e) {
    this.searchForm.value.p = (this.paginator.pageIndex = e.pageIndex || 0) + 1;
    this.service.httpGet('/api/v1/stu-plan/search', this.searchForm.value).then((res: IResponse) => {
      this.dataSource.data = res.responseData || [];
      this.pageSize = res.responseSize || 0;
      this.checklist = this.dataSource.data;

    });

  }

  onCancel() {
    this.searchForm.controls.yearGroupName.setValue(this.ddlYearGroup());
    this.searchForm.controls.classId.setValue(this.ddlClass());
    this.onSearch(0);
  }

  checkAll() {
    // tslint:disable-next-line:forin
    for (const i in this.checklist) {
      this.checklist[i].isSelected = this.headerSelected;

    }
    this.getCheckedItemList();
  }

  isAllSelected() {
    this.checklist.every((item: any) => {
      return item.isSelected === true;
    });
    this.headerSelected = false;
    this.getCheckedItemList();
  }

  getCheckedItemList() {
    this.allChecklist = [];

    for (const i in this.checklist) {
      if (this.checklist[i].isSelected === true) {
        const itemData = {
          yearGroupName: this.checklist[i].year_group_name,
          classId: this.checklist[i].class_id,
          classRoomId: this.checklist[i].class_room_id,
          classRoomSnameTh: this.checklist[i].class_room_sname_th,
          classRoom: this.checklist[i].class_room,
          classLnameEn: this.checklist[i].class_lname_en
        };
        this.allChecklist.push(itemData);
        this.checkChoose = true;

      }
    }
  }

  onReport() {
    if (this.checkChoose === true && this.searchForm.value.langCode == '1') {
      localStorage.setItem('allChecklist', JSON.stringify(this.allChecklist));
      localStorage.setItem('langCode', this.searchForm.value.langCode);
      this.router.navigate(['/aca/aca011-report']);
    } else {
      localStorage.setItem('allChecklistEng', JSON.stringify(this.allChecklist));
      localStorage.setItem('langCode', this.searchForm.value.langCode);
      this.router.navigate(['/aca/aca011-report-eng']);
    }
  }

}
