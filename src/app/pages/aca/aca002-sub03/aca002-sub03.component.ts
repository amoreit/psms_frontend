import { Component, OnInit, ViewChild } from '@angular/core';
import { ApiService, IResponse } from 'src/app/shared/service/api.service';
import { FormBuilder } from '@angular/forms';
import { MatTableDataSource, MatPaginator, MatDialog } from '@angular/material';
import Swal from 'sweetalert2/dist/sweetalert2.js';
import 'sweetalert2/src/sweetalert2.scss';
import { Aca002Sub03AddDialogComponent } from 'src/app/dialog/aca002-sub03-add-dialog/aca002-sub03-add-dialog.component';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-aca002-sub03',
  templateUrl: './aca002-sub03.component.html',
  styleUrls: ['./aca002-sub03.component.scss']
})
export class Aca002Sub03Component implements OnInit {

  searchForm = this.fb.group({ 
    'classId': [''] 
    , 'yearGroupId': ['']
    , 'subjectCode': ['']
  });

  yearGroupList: [];
  classList = [];
  classRoomSubjectList: [];

  subject_name: any;
  checkdata = false;
  checkvalue1 = false;
  checkvalue2 = true;

  dataSource = new MatTableDataSource();
  displayedColumns: string[] = ['classRoom', 'teacher1', 'manage1', 'teacher2', 'manage2'];

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  constructor(private fb: FormBuilder, private service: ApiService, private translate: TranslateService, public dialog: MatDialog) { }

  ngOnInit() {
    this.ddlClass();
    this.ddlYearGroup();
    this.onSearch(0);
  }

  onSearch(e) {
    this.service.httpGet('/api/v1/trn-class-room-subject/classroomteacherinaca002sub03', this.searchForm.value).then((res: IResponse) => {
      if (res.responseData.length === 0) {
        this.dataSource.data = []
        this.checkdata = true;
      } else {
        this.dataSource.data = res.responseData || [];
        this.checkdata = false;
      }
    });
  }

  onCancel() {
    this.searchForm = this.fb.group({
      'classId': ['']
      , 'yearGroupId': ['']
      , 'subjectCode': ['']
    });
    this.onSearch(0);
  }


  ddlYearGroup() {
    this.service.httpGet('/api/v1/0/year-group/ddl', null).then((res) => {
      this.yearGroupList = res || [];
      let ele = this.yearGroupList.filter((o) => {
        return o['iscurrent'] == "true";
      });
      this.searchForm.controls.yearGroupId.setValue(ele[0]['yearGroupId'])
    });
  }

  ddlClass() {
    this.service.httpGet('/api/v1/0/class/ddl', null).then((res) => {
      this.classList = res || [];
    });
  }

  ddlClassRoomSubject() {
    let json = {
      'classId': this.searchForm.value.classId,
      'yearGroupId': this.searchForm.value.yearGroupId
    };
    this.service.httpGet('/api/v1/class-room-subject/ddlaca002sub03', json).then((res) => {
      this.classRoomSubjectList = res || [];
    });
  }

  onCheckClassRoomSubject() {
    let ele = this.classRoomSubjectList.filter((o) => {
      return o['subject_code'] == this.searchForm.value.subjectCode;
    });
    this.subject_name = ele[0]['subject_name_th']
  }

  onCheckClass() {
    let ele = this.classList.filter((o) => {
      return o['classId'] == this.searchForm.value.classId;
    });
    if (ele[0]['classId'] == 1 || ele[0]['classId'] == 2 || ele[0]['classId'] == 3) {
      this.checkvalue1 = true;
      this.checkvalue2 = false
    } else {
      this.checkvalue1 = false;
      this.checkvalue2 = true;
    }
  }

  onAddForm(e, type): void {
    const dialogRef = this.dialog.open(Aca002Sub03AddDialogComponent, {
      width: '60%'
      , 'autoFocus': true
      , 'disableClose': true
      , data: { 'departmentId': e.department_id, 'classRoomSubjectId': e.class_room_subject_id, 'teacherType': type }
    });
    dialogRef.afterClosed().subscribe(result => {
      if ((result || '') == '') {
        this.onSearch(0)
      }
    });
  }

  onDelete(e, type) { 
    Swal.fire({
      title: this.translate.instant('alert.teacher_delete'),//ต้องการลบครูผู้สอน ใช่หรือไม่
      icon: 'info',
      showCancelButton: true,
      confirmButtonColor: '#2085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: this.translate.instant('psms.DL0008'),
      cancelButtonText: this.translate.instant('psms.DL0009')
    }).then((result) => {
      if (result.dismiss == 'cancel') {
        return;
      }
      let user = localStorage.getItem('userName')
      if (type == 1) {
        this.service.httpPut('/api/v1/trn-class-room-subject/deleteteacher1/' + user, { 'classRoomSubjectId': e.class_room_subject_id }).then((res: IResponse) => {
          if ((res.responseCode || 500) != 200) {
            Swal.fire(
              this.translate.instant('message.delete_error'),//ลบข้อมูลผิดพลาด
              res.responseMsg,
              'error'
            )
            return;
          }
          Swal.fire(
            this.translate.instant('alert.delete_header')+'(1)',//ลบครูผู้สอน 1
            this.translate.instant('message.delete'),//ลบครูผู้สอน 1 สำเร็จ
            'success'
          ).then(() => {
            this.onSearch(0);
          })
        });
      } else {
        this.service.httpPut('/api/v1/trn-class-room-subject/deleteteacher2/' + user, { 'classRoomSubjectId': e.class_room_subject_id }).then((res: IResponse) => {
          if ((res.responseCode || 500) != 200) {
            Swal.fire(
              this.translate.instant('message.delete_error'),//ลบข้อมูลผิดพลาด
              res.responseMsg,
              'error'
            )
            return;
          }
          Swal.fire(
            this.translate.instant('alert.delete_header')+'(2)',//ลบครูผู้สอน 2
            this.translate.instant('message.delete'),//ลบครูผู้สอน 2 สำเร็จ
            'success'
          ).then(() => {
            this.onSearch(0);
          })
        });
      }

    });
  }

}
