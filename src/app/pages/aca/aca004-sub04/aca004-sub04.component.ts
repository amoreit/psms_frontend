import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator, MatDialog, MatTableDataSource } from '@angular/material';
import { FormBuilder } from '@angular/forms';
import { ApiService, IResponse } from 'src/app/shared/service/api.service';
import { Router } from '@angular/router';
import Swal from 'sweetalert2/dist/sweetalert2.js';
import 'sweetalert2/src/sweetalert2.scss';
import { TranslateService } from '@ngx-translate/core';

export interface classMember {
  ratwResult: any;
}

@Component({
  selector: 'app-aca004-sub04',
  templateUrl: './aca004-sub04.component.html',
  styleUrls: ['./aca004-sub04.component.scss']
})
export class Aca004Sub04Component implements OnInit {
 
  searchForm = this.fb.group({
    'classId': ['']
    , 'classRoomId': ['']
    , 'yearGroupId': ['']
  });

  headerAll: any;
  classList: [];
  classroomList: [];
  yearGroupList: [];
  userName: String;
  citizen_id: any;
  pageSize: number;
  displayedColumns: string[] = ['no.', 'yearGroupName', 'stuCode', 'fullName', 'classRoom', 'ratwResult'];
  dataSource = new MatTableDataSource();

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  constructor(private fb: FormBuilder, private service: ApiService, public dialog: MatDialog, private translate: TranslateService, private router: Router) { }

  ngOnInit() {
    this.userName = localStorage.getItem('userName') || '';
    this.citizen_id = localStorage.getItem('citizenId') || '';
    this.ddlYearGroup();
    this.ddlClass();
    this.onSearch(0);
  }

  onSearch(e) {
    this.service.httpGet('/api/v1/_trnClassRoomMember/multiSkill', this.searchForm.value).then((res: IResponse) => {
      this.dataSource.data = res.responseData || [];
    });
  }

  ddlYearGroup() {
    this.service.httpGet('/api/v1/0/year-group/ddl', null).then((res) => {
      this.yearGroupList = res || [];
      this.searchForm.controls.yearGroupId.setValue(res[0]['yearGroupId']);
    });
  }

  ddlClass() {
    this.service.httpGet('/api/v1/0/class/ddlbycitizenid', { 'citizenId': this.citizen_id }).then(res => {
      this.classList = res || [];
    });
  }

  ddlClassRoom() {
    const json = { 'citizenId': this.citizen_id, 'classId': this.searchForm.value.classId };
    this.service
      .httpGet('/api/v1/0/classroom-list/ddlfiltercitizenid', json)
      .then(res => {
        this.classroomList = res || [];
      });
  }

  onClear() {
    this.searchForm = this.fb.group({
      'classId': ['']
      , 'classRoomId': ['']
    });
    this.onSearch(0);
  }

  selectedAll(result) {
    this.dataSource.data.map(o => {
      return o['ratwResult'] = result;
    });
  }

  selectedHeader() {
    this.headerAll = undefined;
  }

  onSave() {
    this.dataSource.data.map((row: classMember) => {
      return { ratwResult: row.ratwResult }
    });

    Swal.fire({
      title: this.translate.instant('psms.DL0001'),
      icon: 'info',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: this.translate.instant('psms.DL0008'),
      cancelButtonText: this.translate.instant('psms.DL0009')
    }).then((result) => {
      if (result.dismiss == 'cancel') {
        return;
      }
      let json = this.dataSource.data;
      this.service.httpPut('/api/v1/_trnClassRoomMember/' + this.userName, json).then((res: IResponse) => {
        if((res.responseCode||500)!=200){
          Swal.fire(
            this.translate.instant('message.save_error'),
            res.responseMsg,
            'error' 
          )
          return;
        }
        Swal.fire( 
            this.translate.instant('message.save_header'), 
            this.translate.instant('message.save'),
            'success'
          ).then(() => this.searchForm.controls.classId.setValue(this.searchForm.value.classId), this.searchForm.controls.classRoomId.setValue(this.searchForm.value.classRoomId));
      });
    });
  }

}
