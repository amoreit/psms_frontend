import { Component, OnInit, ViewChild, Inject } from '@angular/core';
import { FormBuilder, Validators, FormGroup, FormArray } from '@angular/forms';
import { MatPaginator, MatTableDataSource, MAT_DIALOG_DATA, MatDialog } from '@angular/material';
import { ApiService, IResponse, EHttp } from '../../../../shared/service/api.service';
import { ActivatedRoute, Router } from '@angular/router';
import { UtilService } from 'src/app/_util/util.service';
import Swal from 'sweetalert2/dist/sweetalert2.js';
import 'sweetalert2/src/sweetalert2.scss';
import { TouchSequence } from 'selenium-webdriver';
import $ from "jquery";
import { TranslateService } from '@ngx-translate/core';

export interface classRoom {
  classRoomSubjectScoreId: string;
  scoreFinal: string;
}
 
@Component({
  selector: 'app-aca004-sub01-form2',
  templateUrl: './aca004-sub01-form2.component.html',
  styleUrls: ['./aca004-sub01-form2.component.scss']
})
export class Aca004Sub01Form2Component implements OnInit {

  isProcess: boolean = false;
  form = this.fb.group({
    'yearGroupId': ['']
    , 'classRoomSubjectId': ['']
    , 'classId': ['']
    , 'classRoomId': ['']
  });

  subjectCode: any;
  subjectName: any;
  credit: any;
  classRoomSnameTh: any;
  yearGroupName: any;
  term: any;
  yearGroupId: any;
  classId: any;
  classRoomId: any;
  fullScoreFinal = 0;

  displayedColumns: string[] = ['no', 'stuCode', 'fullname', 'scoreFinal'];
  dataSource = new MatTableDataSource();

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  constructor(
    ar: ActivatedRoute,
    private fb: FormBuilder,
    private service: ApiService,
    public dialog: MatDialog,
    private router: Router,
    private translate: TranslateService,
    public utilService: UtilService
  ) {
    this.form.controls.yearGroupId.setValue(ar.snapshot.queryParamMap.get('yearGroupId'));
    this.form.controls.classId.setValue(ar.snapshot.queryParamMap.get('classId'));
    this.form.controls.classRoomId.setValue(ar.snapshot.queryParamMap.get('classRoomId'));
    this.form.controls.classRoomSubjectId.setValue(ar.snapshot.queryParamMap.get('classRoomSubjectId'));
  }

  ngOnInit() {
    this.onSearch(0);
  }

  onSearch(e) {
    this.service.httpGet('/api/v1/classRoom-subjectScore', this.form.value).then((res: IResponse) => {
      this.dataSource.data = res.responseData || [];
      if (this.dataSource.data.length > 1) {
        this.subjectCode = res.responseData[0].subjectCode;
        this.subjectName = res.responseData[0].subjectName;
        this.credit = res.responseData[0].credit;
        this.classRoomSnameTh = res.responseData[0].classRoomSnameTh;
        let yearGroupName = res.responseData[0].yearGroupName;
        this.yearGroupName = yearGroupName.substring(2);
        let term = res.responseData[0].yearGroupName;
        this.term = term.substring(0, 1);
        this.fullScoreFinal = res.responseData[0].fullScoreFinal;
      }
      else {
      }
    });
  }

  controlkey() {
    var objSet = $("table.myTable").find("input"); // กำหนด ส่วน หรือขอบเขต การจัดการ    
    objSet.keyup(function (event) {

      var len_obj = objSet.length;
      var now_id = objSet.index($(this));

      var row_len = $(this).parents("tr").find("input").length;
      var navCode = event.keyCode;
      switch (navCode) {
        case 37:
          objSet.eq(now_id - 1).focus();
          break;
        case 38:
          if (now_id >= row_len) {
            objSet.eq(now_id - row_len).focus();
          }
          break;
        case 39:
          objSet.eq(now_id + 1).focus();
          break;
        case 40:
          objSet.eq(now_id + row_len).focus();
          break;
      }
    });
  }

  onSave() {

    const myData = this.dataSource.data.map((row: classRoom) => {
      return {
        classRoomSubjectScoreId: row.classRoomSubjectScoreId
        , scoreFinal: row.scoreFinal == '' || Number(row.scoreFinal) > Number(this.fullScoreFinal) ? 0 : row.scoreFinal
      }
    });

    Swal.fire({
      title: this.translate.instant('psms.DL0001'),//ต้องการบันทึกข้อมูล ใช่หรือไม่
      icon: 'info',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: this.translate.instant('psms.DL0008'),
      cancelButtonText: this.translate.instant('psms.DL0009')
    }).then((result) => {
      if (result.dismiss == 'cancel') {
        return;
      }
      let user = localStorage.getItem('userName');
      let json = this.dataSource.data;;
      this.service.httpPut('/api/v1/classRoom-subjectScore/save2/' + user, myData).then((res: IResponse) => {
        this.isProcess = false;
        if ((res.responseCode || 500) != 200) {
          Swal.fire(
            this.translate.instant('message.add_error'),
            res.responseMsg,
            'error'
          );
          return;
        }
        Swal.fire(
          this.translate.instant('alert.add_header'),
          this.translate.instant('message.add'),
          'success'
        )
      });
      return;
    });
  }

  parseNum(a) {
    return Number(a);
  }

  numberOnly(event): boolean {
    const charCode = (event.which) ? event.which : event.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      return false;
    }
    return true;
  }

  onBackPage(yearGroupId, classId, classRoomId) {
    this.router.navigate(['/aca/aca004-sub01'], {
      queryParams:
      {
        yearGroupId: this.form.value.yearGroupId
        , classId: this.form.value.classId
        , classRoomId: this.form.value.classRoomId
      }
    });
  }
}
