import { Component, OnInit, ViewChild, Inject} from '@angular/core';
import { FormBuilder, Validators, FormGroup, FormArray } from '@angular/forms';
import { MatPaginator, MatTableDataSource, MAT_DIALOG_DATA, MatDialog } from '@angular/material';
import { ApiService,IResponse,EHttp } from '../../../../shared/service/api.service';
import { ActivatedRoute,Router } from '@angular/router';
import { UtilService } from 'src/app/_util/util.service';
import Swal from 'sweetalert2/dist/sweetalert2.js';
import 'sweetalert2/src/sweetalert2.scss';

@Component({
  selector: 'app-aca004-sub06-html',
  templateUrl: './aca004-sub06-html.component.html',
  styleUrls: ['./aca004-sub06-html.component.scss']
})
export class Aca004Sub06HtmlComponent implements OnInit {

  isProcess:boolean = false;
  form = this.fb.group({
    'classRoomMemberId':['']
    ,'yearGroupId':['']
  });

  stuCode:any;
  firstnameTh:any;
  lastnameTh:any;
  yearGroupName:any;
  term:any;
  yearGroupId:any;
  classRoomNo:any;
  classRoom:any;

  keys: {};
  groups = [];
  groups2 = [];
  groupByName:any = {};
  ELEMENT_DATA:any;
  dataSource = new MatTableDataSource();

  constructor(
    private ar:ActivatedRoute,
    private fb: FormBuilder,
    private service:ApiService,
    public dialog: MatDialog, 
    private router: Router,
    public utilService: UtilService
  ) { }

  ngOnInit() {
    this.form.controls.classRoomMemberId.setValue(this.ar.snapshot.queryParamMap.get('classRoomMemberId'));
    this.form.controls.yearGroupId.setValue(this.ar.snapshot.queryParamMap.get('yearGroupId'));
    this.onSearch(0);
  }

  onSearch(e){
    this.service.httpGet('/api/v1/classRoomMemberDcs/searchAca004Sub06Form',this.form.value).then((res:IResponse)=>{
      this.dataSource.data = res.responseData||[];
      if(this.dataSource.data.length >= 1 ){
        this.stuCode = res.responseData[0].stuCode;
        this.firstnameTh = res.responseData[0].firstnameTh;
        this.lastnameTh = res.responseData[0].lastnameTh;
        this.yearGroupName = res.responseData[0].yearGroupName;
        this.classRoom = res.responseData[0].classRoom;
        this.classRoomNo = res.responseData[0].classRoomNo;
      }
      else{}
      let kkk = new Set;
         res.responseData.forEach(function (a) {
            kkk.add(a.itemDesc);
          });   
        this.keys = kkk;
        let groupByName = [];  
        res.responseData.map(function (a) {   
          groupByName [a.itemDesc] = groupByName [a.itemDesc] || [];
            groupByName [a.itemDesc].push(
            { 
              classRoomMemberDcsId: a.classRoomMemberDcsId,
              behavioursSkillsDetailId: a.behavioursSkillsDetailId,                  
              description: a.description,
              score1: a.score1,
              score2: a.score2,
              sumScore: a.sumScore                 
            });   
          });  
        this.groups = groupByName;       
    });  
  }

  printComponent(cmpName) {
    const printContents = document.getElementById(cmpName).innerHTML;
    const originalContents = document.body.innerHTML;
    document.body.innerHTML = printContents;
    window.print();
    window.close();
    document.body.innerHTML = originalContents;

    window.location.reload(true)

  }

}
