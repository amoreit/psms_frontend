import { Component, OnInit, ViewChild, Inject } from '@angular/core';
import { FormBuilder, Validators, FormGroup, FormArray } from '@angular/forms';
import { MatPaginator, MatTableDataSource, MAT_DIALOG_DATA, MatDialog } from '@angular/material';
import { ApiService, IResponse, EHttp } from '../../../../shared/service/api.service';
import { ActivatedRoute, Router } from '@angular/router';
import { UtilService } from 'src/app/_util/util.service';
import Swal from 'sweetalert2/dist/sweetalert2.js';
import 'sweetalert2/src/sweetalert2.scss';
import { TranslateService } from '@ngx-translate/core';

export interface classRoomDcs {
  classRoomMemberDcsId: string; 
  score1: string;
  score2: string;
}

@Component({
  selector: 'app-aca004-sub06-form',
  templateUrl: './aca004-sub06-form.component.html',
  styleUrls: ['./aca004-sub06-form.component.scss']
})
export class Aca004Sub06FormComponent implements OnInit {

  isProcess: boolean = false;
  form = this.fb.group({
    'classRoomMemberId': ['']
    , 'yearGroupId': ['']
  }); 

  stuCode: any;
  fullname: any;
  yearGroupName: any;
  term: any;
  yearGroupId: any;
  classId: any;
  classRoomId: any;

  keys: {};
  groups = [];
  groups2 = [];
  groupByName: any = {};
  ELEMENT_DATA: any;
  key = 0;

  displayedColumns = ['no', 'Head', 'desc', 'score1', 'score2', 'sumScore'];
  dataSource = new MatTableDataSource();

  constructor(
    private ar: ActivatedRoute,
    private fb: FormBuilder,
    private service: ApiService,
    public dialog: MatDialog,
    private router: Router,
    private translate: TranslateService,
    public utilService: UtilService
  ) { }

  ngOnInit() {
    this.key = 3
    this.form.controls.classRoomMemberId.setValue(this.ar.snapshot.queryParamMap.get('classRoomMemberId'));
    this.form.controls.yearGroupId.setValue(this.ar.snapshot.queryParamMap.get('yearGroupId'));
    this.onSearch(0);
  }

  onSearch(e) {
    this.service.httpGet('/api/v1/classRoomMemberDcs/searchAca004Sub06Form', this.form.value).then((res: IResponse) => {
      this.dataSource.data = res.responseData || [];
      if (this.dataSource.data.length >= 1) {
        this.stuCode = res.responseData[0].stuCode;
        this.fullname = res.responseData[0].fullName;
        let yearGroupName = res.responseData[0].yearGroupName;
        this.yearGroupName = yearGroupName.substring(2);
        let term = res.responseData[0].yearGroupName;
        this.term = term.substring(0, 1);
        this.yearGroupId = res.responseData[0].yearGroupId;
        this.classId = res.responseData[0].classId;
        this.classRoomId = res.responseData[0].classRoomId;
      }
      else { }
    });
  }

  parseNum(a) {
    if (Number(a) > 3) {
      return 0
    } else {
      return Number(a);
    }
  }

  ceilNum(a) {
    return Math.ceil(Number(a / 2))
  }

  onChoose(o) { 
    this.dataSource.data.map(e => {
      e['score1'] = o.target.value;
      e['score2'] = o.target.value;
      return e;
    });
  }

  onSave() {
    const myData = this.dataSource.data.map((row: classRoomDcs) => {
      return {
        classRoomMemberDcsId: row.classRoomMemberDcsId
        , score1: Number(row.score1) > 3 ? 0 : row.score1
        , score2: Number(row.score2) > 3 ? 0 : row.score2
      }
    });

    Swal.fire({
      title: this.translate.instant('psms.DL0001'),//ต้องการบันทึกข้อมูล ใช่หรือไม่?
      icon: 'info',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: this.translate.instant('psms.DL0008'),
      cancelButtonText: this.translate.instant('psms.DL0009')
    }).then((result) => {
      if (result.dismiss == 'cancel') {
        return;
      }
      let user = localStorage.getItem('userName');
      this.service.httpPut('/api/v1/classRoomMemberDcs/update/' + user, myData).then((res: IResponse) => {
        this.isProcess = false;
        if((res.responseCode||500)!=200){
          Swal.fire(
            this.translate.instant('message.save_error'),
            res.responseMsg,
            'error' 
          )
          return;
        }
        Swal.fire( 
          this.translate.instant('message.save_header'), 
          this.translate.instant('message.save'),
          'success'
        )
      });
      return;
    });
  }

  onBackPage(yearGroupId, classId, classRoomId) {
    this.router.navigate(['/aca/aca004-sub06'], {
      queryParams:
      {
        yearGroupId: this.yearGroupId
        , classId: this.classId
        , classRoomId: this.classRoomId
      }
    });
  }

  numberOnly(event): boolean {
    const charCode = (event.which) ? event.which : event.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      return false;
    }
    return true;
  }

}
