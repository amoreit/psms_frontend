import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder } from '@angular/forms';
import { ApiService, IResponse } from 'src/app/shared/service/api.service';
import { MatDialog, MatTableDataSource, MatPaginator } from '@angular/material';
import Swal from 'sweetalert2/dist/sweetalert2.js';
import 'sweetalert2/src/sweetalert2.scss';
import { TranslateService } from '@ngx-translate/core';

export interface numberStu {
  classRoomNo: any;
}

@Component({
  selector: 'app-aca001',
  templateUrl: './aca001.component.html',
  styleUrls: ['./aca001.component.scss']
})
export class Aca001Component implements OnInit {
  
  isProcess:boolean = false;
  searchForm = this.fb.group({
     'yearGroupId':['']
    ,'classId':['']
    , 'classRoomId':['']
    , 'orderBy':['']
  });

  pageSize:number;
  displayedColumns: string[] = ['noNew','noOld','stuCode','fullname'];
  dataSource = new MatTableDataSource();
  classList:[];
  classroomnoList:[];
  yearGroupList:[];
  checkdata = false;

  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  constructor(private translate: TranslateService,private fb: FormBuilder,private service:ApiService,public dialog: MatDialog, private router: Router) { 
  }

  ngOnInit() {
    this.ddlClass();
    this.ddlYearGroup();
    this.onSearch(0);
  }

  onSearch(e){
    this.service.httpGet('/api/v1/trn-classroom-member/stufornumber',this.searchForm.value).then((res:IResponse)=>{
        if(res.responseData.length === 0){
          this.dataSource.data = []
          this.checkdata = true;
        }else{
          this.dataSource.data = res.responseData||[]; 
          this.checkdata = false;
          this.onChoose()    
        }
    });
  }

  onCancel(){
     this.searchForm = this.fb.group({
       'yearGroupId':['']
    ,'classId':['']
    , 'classRoomId':['']
    , 'orderBy':['']
    , 'p':['']
    , 'result':[60]
    });
    this.onSearch(0);
  }


  ddlClass(){
    this.service.httpGet('/api/v1/0/class/ddl',null).then((res)=>{
      this.classList = res||[];
    });
  }

  ddlClassRoomNo(){
    let json = {'classId':this.searchForm.value.classId};
    this.searchForm.controls.classRoomId.setValue('');
    this.service.httpGet('/api/v1/0/classroom/ddlByClassId',json).then((res)=>{
      this.classroomnoList = res||[];
    });
  }

  ddlYearGroup(){
    this.service.httpGet('/api/v1/0/year-group/ddl',null).then((res)=>{
      this.yearGroupList = res||[];
          let ele = this.yearGroupList.filter((o) => {
            return o['iscurrent'] == "true";
          });
          this.searchForm.controls.yearGroupId.setValue(ele[0]['yearGroupId'])
    });
  }

  onChoose(){ 
    for(let i = 0;i<this.dataSource.data.length;i++){
      this.dataSource.data[i]['classRoomNo'] = i+1
    }
  }

  onSave2(){
    const myData = this.dataSource.data.map((row: numberStu) => {
      return {
        classRoomNo: row.classRoomNo,
      }
    });

    Swal.fire({
      title: this.translate.instant('alert.save_number'),//ต้องการบันทึกเลขที่ ใช่หรือไม่
      icon : 'info',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: this.translate.instant('psms.DL0008'),//ใช่
      cancelButtonText: this.translate.instant('psms.DL0009')//ไม่
  }).then((result) => {
    if(result.dismiss == 'cancel'){
        return;
    }
    let json = this.dataSource.data;
    if(json.length === 0 || this.searchForm.value.yearGroupId == '' || this.searchForm.value.classId == '' || this.searchForm.value.classRoomId == '' || this.searchForm.value.orderBy == ''){
      Swal.fire(
        this.translate.instant('alert.save_number_error'),//เพิ่มเลขที่ผิดพลาด
        this.translate.instant('alert.validate'),//กรุณาระบุข้อมูล ให้ครบ.
        'error' 
      )
    }else{
      this.service.httpPut('/api/v1/trn-classroom-member/updatenumber',json).then((res:IResponse)=>{
          this.isProcess = false;
          if((res.responseCode||500)!=200){
              Swal.fire(
                this.translate.instant('alert.save_number_error'),//เพิ่มเลขที่ผิดพลาด
                res.responseMsg,
                'error' 
              )
            return;
          }
          Swal.fire(
            this.translate.instant('alert.save_number_header'),//เพิ่มเลขที่
            this.translate.instant('alert.save_number_success'),//เพิ่มเลขที่สำเร็จ
            'success' 
          ).then(() => {this.onSearch(0)})
        });
      }
        return;
  });
    
  }

  onPreviewPDF(){
    this.isProcess = true;
    this.service.httpPreviewPDF('/api/v1/aca001/report/pdf',this.searchForm.value).then((res)=>{
      this.isProcess = false;
     window.location.href = res;
    });
  }

  onExportPDF(){
    this.isProcess = true;
    this.service.httpReportPDF('/api/v1/aca001/report/pdf',"เลขที่นักเรียน",this.searchForm.value).then((res)=>{
      this.isProcess = false;
    });
  }

  onExportExcel(){
    this.isProcess = true;
    this.service.httpReportXLS('/api/v1/aca001/report/xls',"เลขที่นักเรียน",this.searchForm.value).then((res)=>{
      this.isProcess = false;
    });
  }



}
