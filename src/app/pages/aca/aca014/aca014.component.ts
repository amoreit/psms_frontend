import { Component, OnInit, ViewChild, Inject } from '@angular/core';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { MatPaginator, MatTableDataSource, MAT_DIALOG_DATA, MatDialog } from '@angular/material';
import { ApiService,IResponse,EHttp } from '../../../shared/service/api.service';
import { Router } from '@angular/router';
import Swal from 'sweetalert2/dist/sweetalert2.js';
import 'sweetalert2/src/sweetalert2.scss';
import { UtilService } from 'src/app/_util/util.service';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-aca014',
  templateUrl: './aca014.component.html',
  styleUrls: ['./aca014.component.scss']
})
export class Aca014Component implements OnInit {

  isProcess:boolean = false;
  searchForm = this.fb.group({
      'stuCode':['']
      , 'firstnameTh':['']
      , 'lastnameTh':['']
      , 'classRoomId':['']
      , 'p':['']
      , 'result':[10] 
  }); 

  pageSize:number;
  displayedColumns: string[] = ['check','no', 'stuCode', 'fullName', 'status'];
  dataSource = new MatTableDataSource();

  headerSelected: boolean;
  isSelected: any;
  checklist: any;
  allChecklist: any;
  checkChoose = false;

  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  constructor(
    private translate: TranslateService,
    private fb: FormBuilder,
    private service:ApiService,
    public dialog: MatDialog,
    private router: Router,
    public util: UtilService 
  ) { }

  ngOnInit() {
  }

  onSearch(e){
    this.service.httpGet('/api/v1/classRoomMemberDcs/searchAca014',this.searchForm.value).then((res:IResponse)=>{
      this.dataSource.data = res.responseData||[];
      this.pageSize = res.responseSize || 0;
      this.checklist = this.dataSource.data;
    });
  }

  onCancel(){
    this.searchForm.controls.stuCode.setValue('');
    this.searchForm.controls.firstnameTh.setValue('');
    this.searchForm.controls.lastnameTh.setValue('');
    this.searchForm.controls.classRoomId.setValue('');
  }

  checkAll() {
    for (const i in this.checklist) {
      this.checklist[i].isSelected = this.headerSelected;

    }
    this.getCheckedItemList();
  }

  isAllSelected() {
    this.checklist.every((item: any) => {
      return item.isSelected === true;
    });
    this.headerSelected = false;
    this.getCheckedItemList();
  }

  getCheckedItemList() {
    this.allChecklist = [];

    for (const i in this.checklist) {
      if (this.checklist[i].isSelected === true) {
        const itemData = {
          stuProfileId: this.checklist[i].stu_profile_id,
        };
        this.allChecklist.push(itemData);
        this.checkChoose = true;
      }
    }
  }

  onReport() {
    if (this.checkChoose === true) {
      localStorage.setItem('allChecklist', JSON.stringify(this.allChecklist));
      this.router.navigate(['/aca/aca014/html']);
    }
  }
  onPrint(stuCode){
    localStorage.setItem('stuCode',stuCode);
    this.router.navigate(['/aca/aca014/html']);
  };

}
