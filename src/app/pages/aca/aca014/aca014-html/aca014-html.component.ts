import { Component, OnInit } from '@angular/core';
import { ApiService, IResponse } from 'src/app/shared/service/api.service';
import { FormBuilder } from '@angular/forms';
import { MatPaginator, MatTableDataSource, MAT_DIALOG_DATA, MatDialog } from '@angular/material';

@Component({
  selector: 'app-aca014-html',
  templateUrl: './aca014-html.component.html',
  styleUrls: ['./aca014-html.component.scss']
})
export class Aca014HtmlComponent implements OnInit {

  isProcess = false;
  form = this.fb.group({
    'stuCode':['']
  });

  dataSourceTerm1: any;
  dataSourceTerm2: any;

  dataTerm1: any;
  dataTerm2: any;

  allChecklist: any;

  stuCode:any;
  firstnameTh:any;
  lastnameTh:any;
  yearGroupName:any;
  term:any;
  yearGroupId:any;
  classRoomNo:any;
  classRoom:any;

  keys: {};
  groups = [];
  groups2 = [];
  groupByName:any = {};
  ELEMENT_DATA:any;
  dataSource = new MatTableDataSource();

  constructor(
    private service: ApiService,
    private fb: FormBuilder
  ) { 
    this.isProcess = true;
    this.allChecklist = [];
    this.allChecklist = JSON.parse(localStorage.getItem('allChecklist'));
    this.form.controls.stuCode.setValue(localStorage.getItem('stuCode'));
  }

  ngOnInit() {
    this.onSearch(0);
  }

  onSearch(e){
    console.log(this.form.value)
    this.service.httpGet('/api/v1/classRoomMemberDcs/searchAca014Html',this.form.value).then((res:IResponse)=>{
      this.dataSource.data = res.responseData||[];
      console.log(this.dataSource.data)
      if(this.dataSource.data.length >= 1 ){
        this.stuCode = res.responseData[0].stuCode;
        this.firstnameTh = res.responseData[0].firstnameTh;
        this.lastnameTh = res.responseData[0].lastnameTh;
        this.yearGroupName = res.responseData[0].yearGroupName;
        this.classRoom = res.responseData[0].classRoom;
        this.classRoomNo = res.responseData[0].classRoomNo;
      }
      else{}
      let kkk = new Set;
         res.responseData.forEach(function (a) {
            kkk.add(a.itemDesc);
          });   
        this.keys = kkk;
        let groupByName = [];  
        res.responseData.map(function (a) {   
          groupByName [a.itemDesc] = groupByName [a.itemDesc] || [];
            groupByName [a.itemDesc].push(
            { 
              classRoomMemberDcsId: a.classRoomMemberDcsId,
              behavioursSkillsDetailId: a.behavioursSkillsDetailId,                  
              description: a.description,
              score1: a.score1,
              score2: a.score2,
              sumScore: a.sumScore                 
            });   
          });  
        this.groups = groupByName;       
    });  
  }

}
