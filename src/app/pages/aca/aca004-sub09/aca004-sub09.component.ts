import { Component, OnInit, ViewChild } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder } from '@angular/forms';
import { ApiService, IResponse } from 'src/app/shared/service/api.service';
import { MatDialog, MatTableDataSource, MatPaginator } from '@angular/material';


@Component({
  selector: 'app-aca004-sub09',
  templateUrl: './aca004-sub09.component.html',
  styleUrls: ['./aca004-sub09.component.scss']
})
export class Aca004Sub09Component implements OnInit {

  isProcess: boolean = false;
  searchForm = this.fb.group({
    'yearGroupId': ['']
    , 'classId': ['']
    , 'classRoomId': ['']
  });
  pageSize: number;
  displayedColumns: string[] = ['classRoomNo', 'stuCode', 'fullname', 'memberStatus'];
  dataSource = new MatTableDataSource();
  classList: [];
  classroomnoList: [];
  yearGroupList: [];
  checkdata = false; 
  year_group_id: any;
  class_id: any;
  class_room_id: any;
  citizen_id: any;

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  constructor(private ar: ActivatedRoute, private fb: FormBuilder, private service: ApiService, public dialog: MatDialog, private router: Router) {

  }

  ngOnInit() {
    this.searchForm.controls.yearGroupId.setValue(this.ar.snapshot.queryParamMap.get('year_group_id') || '')
    this.searchForm.controls.classId.setValue(this.ar.snapshot.queryParamMap.get('class_id') || '')
    this.searchForm.controls.classRoomId.setValue(this.ar.snapshot.queryParamMap.get('class_room_id') || '')
    this.citizen_id = localStorage.getItem('citizenId') || ''
    this.ddlClass();
    this.ddlClassRoomNo();
    this.ddlYearGroup();
    this.onSearch(0);
  }

  onSearch(e) {
    this.service.httpGet('/api/v1/trn-classroom-member/stuforevaluation', this.searchForm.value).then((res: IResponse) => {
      if (res.responseData.length === 0) {
        this.dataSource.data = []
        this.checkdata = true;
      } else {
        this.dataSource.data = res.responseData || [];
        this.checkdata = false;
      }
    });
  }


  onCancel() {
    this.searchForm = this.fb.group({
      'yearGroupId': ['']
      , 'classId': ['']
      , 'classRoomId': ['']
    });
    this.onSearch(0);
  }

  onForm(id) {
    this.router.navigate(['/aca/aca004-sub09-evaluation'], { queryParams: { id: id } });
  }

  ddlClass() {
    this.service.httpGet('/api/v1/0/class/ddlbycitizenid', { 'citizenId': this.citizen_id }).then(res => {
      this.classList = res || [];
    });
  }

  ddlClassRoomNo() {
    const json = { 'citizenId': this.citizen_id, 'classId': this.searchForm.value.classId };
    this.service
      .httpGet('/api/v1/0/classroom-list/ddlfiltercitizenid', json)
      .then(res => {
        this.classroomnoList = res || [];
      });
  }

  ddlYearGroup() {
    this.service.httpGet('/api/v1/0/year-group/ddl', null).then((res) => {
      this.yearGroupList = res || [];
      if (this.ar.snapshot.queryParamMap.get('year_group_id') != null) {
        return;
      } else {
        let ele = this.yearGroupList.filter((o) => {
          return o['iscurrent'] == "true";
        });
        this.searchForm.controls.yearGroupId.setValue(ele[0]['yearGroupId'])
      }
    });
  }


}
