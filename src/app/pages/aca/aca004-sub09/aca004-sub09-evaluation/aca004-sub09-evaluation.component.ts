import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { MatTableDataSource, MatPaginator, MatDialog } from '@angular/material';
import { FormBuilder } from '@angular/forms';
import { ApiService, IResponse } from 'src/app/shared/service/api.service';
import Swal from 'sweetalert2/dist/sweetalert2.js';
import 'sweetalert2/src/sweetalert2.scss';
import { TranslateService } from '@ngx-translate/core';

export interface lifeskillScore {
  score: any;
}

@Component({
  selector: 'app-aca004-sub09-evaluation',
  templateUrl: './aca004-sub09-evaluation.component.html',
  styleUrls: ['./aca004-sub09-evaluation.component.scss']
})
export class Aca004Sub09EvaluationComponent implements OnInit {

  behavioursForm = this.fb.group({
    'classRoomMemberId':['']
    ,'classId':['']
  });

  dataSource = new MatTableDataSource(); 
  displayedColumns: string[] = ['no','behaviours','score'];
 
  dataSource2 = new MatTableDataSource();
  displayedColumns2: string[] = ['col1','col2','col3'];

  constructor(private ar:ActivatedRoute,private fb: FormBuilder,private service:ApiService,public dialog: MatDialog, private translate: TranslateService, private router: Router) { }

  id:any;
  stu_code:any
  fullname:any;
  class_room:any;
  year_group_name:any;
  year_group_id:any;
  class_id:any;
  class_room_id:any;
  

  ngOnInit() {
    this.id = this.ar.snapshot.queryParamMap.get('id')||'';
    this.loadData();
    this.loadRules();
  }

  onBack(year_group_id,class_id,class_room_id){
    this.router.navigate(['aca/aca004-sub09'],{ queryParams: {year_group_id:this.year_group_id,class_id:this.class_id,class_room_id:this.class_room_id}});
  }

  loadData(){
    this.service.httpGet('/api/v1/trn-classroom-member/'+this.id,null).then((res:IResponse)=>{
      let data = res.responseData;
      this.behavioursForm.controls.classRoomMemberId.setValue(data.classRoomMemberId||'');  
      this.behavioursForm.controls.classId.setValue(data.classId||'');               
      this.stu_code = data.stuCode;
      this.fullname = data.fullname;
      this.class_room = data.classRoom;
      this.year_group_name = data.yearGroupName;
      this.year_group_id = data.yearGroupId;
      this.class_id = data.classId;
      this.class_room_id = data.classRoomId;
      this.loadBehaviours();
    });
  }

  loadBehaviours(){
    this.service.httpGet('/api/v1/behaviours-skills',this.behavioursForm.value).then((res:IResponse)=>{
      this.dataSource.data = res.responseData||[];    
    });
  }

  loadRules(){

    this.dataSource2.data = [
      { 'col1': '3 = ปฏิบัติเป็นประจำ/มากที่สุด', 'col2': '2.50 - 3.00', 'col3': 'ดีมาก'},
      { 'col1': '2 = ปฏิบัติเป็นบางครั้ง/ปานกลาง', 'col2': '1.50 - 2.49', 'col3': 'ดี'},
      { 'col1': '1 = ปฏิบัติน้อย/ไม่ชัดเจน/น้อย', 'col2': '1.00 - 1.49', 'col3': 'พอใช้'}     
    ];
  }

  onChoose(o){
    this.dataSource.data.map(e=>{
      e['score'] = o.target.value;     
      return e;
    });
  }

  sumScore1() {
    return this.dataSource.data.reduce((sum_score:number, v:lifeskillScore) => parseInt(v.score) == 1 ? sum_score+1: sum_score +0,0) 
  }

  sumScore2() {
    return this.dataSource.data.reduce((sum_score:number, v:lifeskillScore) => parseInt(v.score) == 2 ? sum_score+1: sum_score +0,0) 
  }

  sumScore3() { 
    return this.dataSource.data.reduce((sum_score:number, v:lifeskillScore) => parseInt(v.score) == 3 ? sum_score+1: sum_score +0,0) 
  }

  sumScore() {
     return this.dataSource.data.reduce((sum_score:number, v:lifeskillScore) => sum_score += parseInt(v.score), 0) 
  }
  
  checkSumScoreTest(){
    if(this.sumScore1() > this.sumScore2() && this.sumScore1() > this.sumScore3()){
      return 1;
    }
    if(this.sumScore2() > this.sumScore1() && this.sumScore2() > this.sumScore3()){
      return 2;
    }
    if(this.sumScore3() > this.sumScore1() && this.sumScore3() > this.sumScore2()){
      return 3;
    }
    if(this.sumScore1() == this.sumScore2() && this.sumScore1() != this.sumScore3()){
      return (1+2)/2;
    }
    if(this.sumScore1() == this.sumScore3() && this.sumScore1() != this.sumScore2()){
      return (1+3)/2;
    }
    if(this.sumScore2() == this.sumScore3() && this.sumScore2() != this.sumScore1()){
      return (2+3)/2;
    }
    if(this.sumScore1() == this.sumScore2() && this.sumScore2() == this.sumScore3() && this.sumScore3() == this.sumScore1()){
      return (1+2+3)/3;
    }
  }

checkSumScore(){
  let sum = Number(this.checkSumScoreTest())
  if(sum>=1.00 && sum<=1.49){
    return "พอใช้"
  }else if(sum>=1.50 && sum<=2.49){
    return "ดี"
   }else{
    return "ดีมาก"
  }
}

  onSave2(){
    const myData = this.dataSource.data.map((row: lifeskillScore) => {
      return {
          score: row.score,
      }
    });
    
    Swal.fire({
      title: this.translate.instant('psms.DL0001'),
      icon : 'info',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: this.translate.instant('psms.DL0008'),
      cancelButtonText: this.translate.instant('psms.DL0009')
  }).then((result) => {
    if(result.dismiss == 'cancel'){
        return;
    }

    let json = this.dataSource.data;
    let user = localStorage.getItem('userName')
    if(this.dataSource.data[0]['classRoomLifeskillScoreId'] == null){
      this.service.httpPost('/api/v1/class-room-lifeskill-score/'+user,json).then((res:IResponse)=>{
          if((res.responseCode||500)!=200){
            Swal.fire(
              this.translate.instant('message.save_error'),
              res.responseMsg,
              'error' 
            )
            return;
          }
          Swal.fire( 
              this.translate.instant('message.save_header'), 
              this.translate.instant('message.save'),
              'success'
          ).then(() => {this.onBack(this.year_group_id,this.class_id,this.class_room_id)})
        });
        return;
    }
    this.service.httpPut('/api/v1/class-room-lifeskill-score/'+user,json).then((res:IResponse)=>{
      if ((res.responseCode || 500) != 200) {
        Swal.fire(
          this.translate.instant('message.edit_error'),
          res.responseMsg,
          'error'
        )
        return;
      }
      Swal.fire(
        this.translate.instant('psms.DL0006'),
        this.translate.instant('message.edit'),
        'success'
      ).then(() => {this.onBack(this.year_group_id,this.class_id,this.class_room_id)})
    });
  });
    
  }


}
