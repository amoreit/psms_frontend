import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router'; 
import { FlexLayoutModule } from '@angular/flex-layout';
import { ReactiveFormsModule,FormsModule } from '@angular/forms';
import { TranslateModule } from '@ngx-translate/core';

/*** Material ***/
import {
  MatExpansionModule,
  MatButtonModule, 
  MatIconModule,
  MatInputModule,
  MatTableModule,
  MatCardModule,
  MatFormFieldModule,
  MatPaginatorModule,
  MatDialogModule,
  MatSlideToggleModule,
  MatTabsModule,
  MatStepperModule
} from '@angular/material';

/*** Routing ***/
import {AcaRoutes} from './aca.routing';

/*** Page ***/
import { Aca001Component } from './aca001/aca001.component';
import { Aca002Component } from './aca002/aca002.component';
import { Aca002Sub01Component } from './aca002-sub01/aca002-sub01.component';
import { Aca002Sub02Component } from './aca002-sub02/aca002-sub02.component';
import { Aca002Sub03Component } from './aca002-sub03/aca002-sub03.component';
import { Aca002SubjectComponent } from './aca002/aca002-subject/aca002-subject.component';
import { Aca004Sub01Component } from './aca004-sub01/aca004-sub01.component';
import { Aca004Sub01FormComponent } from './aca004-sub01/aca004-sub01-form/aca004-sub01-form.component';
import { Aca004Sub01Form2Component } from './aca004-sub01/aca004-sub01-form2/aca004-sub01-form2.component';
import { Aca004Sub02Component } from './aca004-sub02/aca004-sub02.component';
import { Aca004Sub03Component } from './aca004-sub03/aca004-sub03.component';
import { Aca004Sub03ScoreComponent } from './aca004-sub03/aca004-sub03-score/aca004-sub03-score.component';
import { Aca004Sub04Component } from './aca004-sub04/aca004-sub04.component';
import { Aca004Sub05Component } from './aca004-sub05/aca004-sub05.component';
import { Aca004Sub05FormComponent } from './aca004-sub05/aca004-sub05-form/aca004-sub05-form.component';
import { Aca004Sub06Component } from './aca004-sub06/aca004-sub06.component';
import { Aca004Sub06HtmlComponent } from './aca004-sub06/aca004-sub06-html/aca004-sub06-html.component';
import { Aca004Sub07Component } from './aca004-sub07/aca004-sub07.component';
import { Aca004Sub07FormComponent } from './aca004-sub07/aca004-sub07-form/aca004-sub07-form.component';
import { Aca004Sub08Component } from './aca004-sub08/aca004-sub08.component';
import { Aca004Sub09Component } from './aca004-sub09/aca004-sub09.component';
import { Aca004Sub09EvaluationComponent } from './aca004-sub09/aca004-sub09-evaluation/aca004-sub09-evaluation.component';
import { Aca006Component } from './aca006/aca006.component';
import { Aca006Form2Component } from './aca006/aca006-form2/aca006-form2.component';
import { Aca006FormComponent } from './aca006/aca006-form/aca006-form.component';
import { Aca008Component } from './aca008/aca008.component';
import { Aca009Component } from './aca009/aca009.component';
import { Aca011ReportComponent } from './aca011/aca011-report/aca011-report.component';
import { Aca011Component } from './aca011/aca011.component';
import { Aca012Component } from './aca012/aca012.component';
import { Aca014Component } from './aca014/aca014.component';
import { Aca014HtmlComponent } from './aca014/aca014-html/aca014-html.component';
import { Aca013Component } from './aca013/aca013.component';
import { Aca013Form1Component } from './aca013/aca-form1/aca-form1.component';
import { Aca013Form2Component } from './aca013/aca-form2/aca-form2.component';
import { Aca015Component } from './aca015/aca015.component';
import { Aca015Form1Component } from './aca015/aca015-form1/aca015-form1.component';
import { Aca015Form2Component } from './aca015/aca015-form2/aca015-form2.component';
import { Aca015Form3Component } from './aca015/aca015-form3/aca015-form3.component';
import { Aca004Sub06FormComponent } from './aca004-sub06/aca004-sub06-form/aca004-sub06-form.component';
import { Aca008DialogReportComponent } from './aca008/aca008-dialog-report/aca008-dialog-report.component';
import { Aca008ReportComponent } from './aca008/aca008-report/aca008-report.component';
import { Aca011ReportEngComponent } from './aca011/aca011-report-eng/aca011-report-eng.component';

@NgModule({
  imports: [
    CommonModule,
    FlexLayoutModule,
    TranslateModule,
    RouterModule.forChild(AcaRoutes),
    MatExpansionModule,
    MatButtonModule,
    MatIconModule,
    MatInputModule,
    MatTableModule,
    MatCardModule,
    MatFormFieldModule,
    ReactiveFormsModule,
    MatPaginatorModule,
    MatDialogModule,
    MatSlideToggleModule,
    MatTabsModule,
    MatStepperModule, 
    FormsModule,
  ],
    providers: [],
    exports:[RouterModule],
  declarations: [
    Aca001Component, 
    Aca002Component, 
    Aca002Sub01Component,
    Aca002Sub02Component,
    Aca002Sub03Component,
    Aca002SubjectComponent,
    Aca004Sub01Component,
    Aca004Sub02Component,
    Aca004Sub03Component,
    Aca004Sub03ScoreComponent,
    Aca004Sub04Component,
    Aca004Sub06Component, 
    Aca004Sub01FormComponent,
    Aca004Sub01Form2Component,
    Aca004Sub05Component,
    Aca004Sub07Component,
    Aca004Sub07FormComponent,
    Aca004Sub05FormComponent,
    Aca004Sub06FormComponent,
    Aca004Sub06HtmlComponent,
    Aca004Sub08Component,
    Aca004Sub09Component,
    Aca004Sub09EvaluationComponent,
    Aca004Sub01FormComponent,
    Aca006Component,
    Aca006Form2Component,
    Aca006FormComponent,
    Aca008Component,
    Aca009Component, 
    Aca011Component,
    Aca011ReportComponent,
    Aca011ReportEngComponent,
    Aca012Component,
    Aca014Component,
    Aca014HtmlComponent,
    Aca013Component,
    Aca013Form1Component,
    Aca013Form2Component,
    Aca015Component,
    Aca015Form1Component,
    Aca015Form2Component,
    Aca015Form3Component,
    Aca008DialogReportComponent,
    Aca008ReportComponent, 
  ],
  entryComponents:[
    Aca008DialogReportComponent,
  ] 
})
export class AcaModule { }
