import { Component, OnInit, ViewChild } from '@angular/core';
import { ApiService, IResponse } from 'src/app/shared/service/api.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatTableDataSource, MatPaginator, MatDialog } from '@angular/material';
import { Router, ActivatedRoute } from '@angular/router';
import 'sweetalert2/src/sweetalert2.scss';
import { UtilService } from 'src/app/_util/util.service';
import { Aca002AddStuDialogComponent } from 'src/app/dialog/aca002-add-stu-dialog/aca002-add-stu-dialog.component';
import { Tec002DialogComponent } from 'src/app/dialog/tec002-dialog/tec002-dialog.component';
import Swal from 'sweetalert2/dist/sweetalert2.js';
import 'sweetalert2/src/sweetalert2.scss';
import { TranslateService } from '@ngx-translate/core';
import { Aca002ChangeClassroomDialogComponent } from 'src/app/dialog/aca002-change-classroom-dialog/aca002-change-classroom-dialog.component';

export interface noStudent {
  class_room_no: any;
}
 
@Component({
  selector: 'app-aca002',
  templateUrl: './aca002.component.html',
  styleUrls: ['./aca002.component.scss']
})
export class Aca002Component implements OnInit {
 
  searchForm = this.fb.group({
    'classRoom': ['']
    , 'yearGroupId': ['']
    , 'departmentId': ['']
  });

  classroomlistList: [];
  yearGroupList: [];
  dataDepartment = [];
  classData = [];
  arrData = [];
  catclass1 = [];

  _catClassId: any;
  year_group_name: any;
  class_room_name: any;
  show_year_group: any;
  show_class_room: any
  is_ep: any;
  role: any;
  class_id: any;
  class_sname_th: any;

  firstFormGroup: FormGroup;
  secondFormGroup: FormGroup;

  isLinear: boolean;

  checkdata = false;
  checkteacher = false;
  isProcess = false;

  dataSource = new MatTableDataSource();
  dataTeacher = new MatTableDataSource();
  dataSubject = new MatTableDataSource();
  displayedColumns: string[] = ['classRoomNo', 'stuCode', 'fullname', 'status'];
  displayedColumnsTeacher: string[] = ['fullname', 'status'];
  displayedColumnsSubject: string[] = ['subjectCode', 'subjectNameTh', 'credit'];
  displayedColumnsKid: string[] = ['subjectCode'];


  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  constructor(
    private translate: TranslateService,
    private ar: ActivatedRoute,
    private fb: FormBuilder,
    private service: ApiService,
    public dialog: MatDialog,
    private router: Router,
    public _util: UtilService) { }

  ngOnInit() {

    this.searchForm.controls.yearGroupId.setValue(this.ar.snapshot.queryParamMap.get('year_group_id') || '')
    this.searchForm.controls.classRoom.setValue(this.ar.snapshot.queryParamMap.get('class_room_id') || '')
    this.year_group_name = this.ar.snapshot.queryParamMap.get('year_group_name') || ''
    this.class_room_name = this.ar.snapshot.queryParamMap.get('class_room') || ''
    this.role = localStorage.getItem('role') || ''
    this.ddlYearGroup();
    this.ddlClassRoomList();
    this.onSearch(0);


    this.firstFormGroup = this.fb.group({
      firstCtrl: ['', Validators.required]
    });
    this.secondFormGroup = this.fb.group({
      secondCtrl: ['', Validators.required]
    });
  }

  onSearch(e) {
    this.isProcess = true;
    this.service.httpGet('/api/v1/trn-classroom-member/stu', this.searchForm.value).then((res: IResponse) => {
      if (res.responseData.length === 0) {
        this.dataSource.data = []
        this.checkdata = true;
      } else {
        this.dataSource.data = res.responseData || [];
        this.checkdata = false;
      }
    });

    this.service.httpGet('/api/v1/trn-classroom-member/teacher', this.searchForm.value).then((res: IResponse) => {
      if (res.responseData.length === 0) {
        this.dataTeacher.data = []
        this.checkteacher = true;
      } else {
        this.dataTeacher.data = res.responseData || [];
        this.checkteacher = false;
      }
    }); 

    this.service.httpGet('/api/v1/trn-classroom-member/department', this.searchForm.value).then((res: IResponse) => {
      this.dataDepartment = res.responseData || [];
      for (let i = 0; i <= this.dataDepartment.length - 1; i++) {
        this._catClassId = this.dataDepartment[i]['cat_class_id']
        let json = {
          'classRoom': this.searchForm.value.classRoom, 'yearGroupId': this.searchForm.value.yearGroupId,
          'departmentId': this.dataDepartment[i]['department_id']
        }
        this.service.httpGet('/api/v1/trn-classroom-member/subjectclassroom', json).then((res: IResponse) => {
          this.dataSubject.data[i] = res.responseData || [];
        });
      }
    });

    this.show_year_group = this.year_group_name;
    this.show_class_room = this.class_room_name;
  }

  ddlClassRoomList() { 
    this.service.httpGet('/api/v1/0/classroom-list/ddlisep', null).then((res) => {
      this.classroomlistList = res || [];
    });
  }

  ddlYearGroup() {
    this.service.httpGet('/api/v1/0/year-group/ddl', null).then((res) => {
      this.yearGroupList = res || [];
      if (this.ar.snapshot.queryParamMap.get('year_group_id') != null) {
        return;
      } else {
        let ele = this.yearGroupList.filter((o) => {
          return o['iscurrent'] == "true";
        });
        this.searchForm.controls.yearGroupId.setValue(ele[0]['yearGroupId'])
        this.onCheckYearGroup();
      }

    })
  }

  onDialogAddStudent(classRoomId, classRoomName, yearGroupId, yearGroupName): void {
    const dialogRef = this.dialog.open(Aca002AddStuDialogComponent, {
      width: '60%'
      , 'autoFocus': true
      , 'disableClose': true
      , data: {
        'classRoomId': this.searchForm.value.classRoom, 'classRoomName': this.show_class_room || '',
        'yearGroupId': this.searchForm.value.yearGroupId, 'yearGroupName': this.show_year_group,
        'isEp': this.is_ep
      }
    });
    dialogRef.afterClosed().subscribe(result => {
      if ((result || '') == '') {
        this.onSearch(0)
      }

    });
  }

  onDialogUpdateClassRoom(e): void {
    const dialogRef = this.dialog.open(Aca002ChangeClassroomDialogComponent, {
      width: '60%'
      , 'autoFocus': true
      , 'disableClose': true
      , data: { 'classRoomId': this.searchForm.value.classRoom, 'classRoomMemberId': e.class_room_member_id, 'role': e.role, 'memberId': e.member_id }
    });
    dialogRef.afterClosed().subscribe(result => {
      if ((result || '') == '') {
        this.onSearch(0)
      }

    });
  }


  onSearchteacher(): void {
    localStorage.setItem("tec002_classId", this.class_id);
    localStorage.setItem("tec002_className", this.class_sname_th);
    localStorage.setItem("tec002_yearGroupId", this.searchForm.value.yearGroupId);
    localStorage.setItem("tec002_yearGroupName", this.show_year_group);
    localStorage.setItem("tec002_classRoomId", this.searchForm.value.classRoom);
    localStorage.setItem("tec002_classRoom", this.show_class_room);

    const dialogRef = this.dialog.open(Tec002DialogComponent, {
      width: '55%'
      , 'autoFocus': true
      , 'disableClose': true
    }
    );
    dialogRef.afterClosed().subscribe(result => {
      if ((result || '') == '') {
        this.onSearch(0)
      }
    });
  }


  onForm(id) {
    this.router.navigate(['/aca/aca002/subject'], { queryParams: { id: id } });
  }

  onCheckYearGroup() {
    let ele = this.yearGroupList.filter((o) => {
      return o['yearGroupId'] == this.searchForm.value.yearGroupId;
    });
    this.year_group_name = ele[0]['name']
  }

  onCheckClassRoom() {
    let ele = this.classroomlistList.filter((o) => {
      return o['class_room_id'] == this.searchForm.value.classRoom;
    });
    this.class_room_name = ele[0]['class_room_sname_th']
    this.is_ep = ele[0]['is_ep'];
    this.class_id = ele[0]['class_id']
    this.class_sname_th = ele[0]['class_sname_th']
  }

  onDelete(e) {

    Swal.fire({
      title: this.translate.instant('alert.delete'),//ต้องการลบข้อมูล ใช่หรือไม่
      icon: 'info',
      showCancelButton: true,
      confirmButtonColor: '#2085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: this.translate.instant('psms.DL0008'),//ใช่
      cancelButtonText: this.translate.instant('psms.DL0009')//ไม่
    }).then((result) => {
      if (result.dismiss == 'cancel') {
        return;
      }
      if (e.role == 'student') {
        this.service.httpDelete('/api/v1/trn-classroom-member', { 'classRoomMemberId': e.class_room_member_id }).then((res: IResponse) => {
          if ((res.responseCode || 500) != 200) {
            alert(res.responseMsg);
            return;
          }
          this.service.httpDelete('/api/v1/trn-classroom-member/deleteclassroomstudent', { 'stuProfileId': e.member_id }).then((res: IResponse) => {
            if ((res.responseCode || 500) != 200) {
              alert(res.responseMsg);
              return;
            }
            Swal.fire(
              this.translate.instant('alert.student_delete_header'),//ลบข้อมูลนักเรียน
              this.translate.instant('alert.student_delete_success'),//ลบข้อมูลนักเรียนสำเร็จ
              'success'
            ).then(() => {
              this.onSearch(0);
            })
          });
        });
      } else {
        this.service.httpDelete('/api/v1/trn-classroom-member', { 'classRoomMemberId': e.class_room_member_id }).then((res: IResponse) => {
          if ((res.responseCode || 500) != 200) {
            alert(res.responseMsg);
            return;
          }
          this.service.httpDelete('/api/v1/trn-classroom-member/deleteclassroomteacher', { 'teacherId': e.member_id }).then((res: IResponse) => {
            if ((res.responseCode || 500) != 200) {
              alert(res.responseMsg);
              return;
            }
            Swal.fire(
              this.translate.instant('alert.teacher_delete_header'),//ลบข้อมูลคุณครู
              this.translate.instant('alert.teacher_delete_success'),//ลบข้อมูลคุณครูสำเร็จ
              'success'
            ).then(() => {
              this.onSearch(0);
            })
          });
        });
      }
    });
  }

  onSave() {
    this.dataSource.data.map((row: noStudent) => {
      return {
        class_room_no: row.class_room_no,
      }
    });
    this.arrData = []
    for (var i = 0; i < this.dataSource.data.length; i++) {
      let itemData = {
        'classRoomNo': this.dataSource.data[i]['class_room_no'],
        'classRoomMemberId': this.dataSource.data[i]['class_room_member_id']
      }
      this.arrData.push(itemData);
    }


    Swal.fire({
      title: this.translate.instant('alert.save_number'),//ต้องการบันทึกเลขที่ ใช่หรือไม่
      icon: 'info',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: this.translate.instant('psms.DL0008'),//ใช่
      cancelButtonText: this.translate.instant('psms.DL0009')//ไม่
    }).then((result) => {
      if (result.dismiss == 'cancel') {
        return;
      }
      let json = this.arrData;
      if (json.length === 0 || this.searchForm.value.yearGroupId == '' || this.searchForm.value.classRoom == '') {
        Swal.fire(
          this.translate.instant('alert.save_number_error'),//เพิ่มเลขที่ผิดพลาด
          this.translate.instant('alert.validate'),//กรุณาระบุข้อมูล ให้ครบ.
          'error'
        )
      } else {
        this.service.httpPut('/api/v1/trn-classroom-member/updatenumber', json).then((res: IResponse) => {
          if ((res.responseCode || 500) != 200) {
            Swal.fire(
              this.translate.instant('alert.save_number_error'),//เพิ่มเลขที่ผิดพลาด
              res.responseMsg,
              'error'
            )
            return;
          }
          Swal.fire(
            this.translate.instant('alert.save_number_header'),//เพิ่มเลขที่
            this.translate.instant('alert.save_number_success'),//เพิ่มเลขที่สำเร็จ
            'success'
          ).then(() => { this.onSearch(0) })
        });
      }
      return;
    });

  }


}
