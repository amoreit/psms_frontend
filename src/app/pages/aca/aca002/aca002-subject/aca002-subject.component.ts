import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { MatTableDataSource, MatDialog } from '@angular/material';
import { FormBuilder } from '@angular/forms';
import { ApiService, IResponse } from 'src/app/shared/service/api.service';
import { UtilService } from 'src/app/_util/util.service';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-aca002-subject',
  templateUrl: './aca002-subject.component.html',
  styleUrls: ['./aca002-subject.component.scss']
})
export class Aca002SubjectComponent implements OnInit {

  activityForm = this.fb.group({
    'classRoomMemberId':['']
  });

  subjectForm = this.fb.group({
    'stuCode':['']
    ,'yearGroupId':['']
    ,'classRoomId':['']
  });

  dataSource = new MatTableDataSource();
  displayedColumns: string[] = ['category','name','lesson'];

  dataSource2 = new MatTableDataSource();
  displayedColumns2: string[] = ['code','name','credit'];

  constructor(
    private translate: TranslateService,
    private ar:ActivatedRoute,
    private fb: FormBuilder,
    private service:ApiService,
    public dialog: MatDialog, 
    private router: Router,
    public _util: UtilService) { }

  id:any;
  stu_code:any
  fullname:any;
  class_room:any;
  year_group_id:any;
  class_room_id:any;
  year_group_name:any;
  checksubject = false;

  ngOnInit() {
    this.id = this.ar.snapshot.queryParamMap.get('id')||'';
    this.loadData();
  }

  onBack(year_group_id,year_group_name,class_room_id,class_room){
    this.router.navigate(['aca/aca002'],{ queryParams: {year_group_id:this.year_group_id,year_group_name:this.year_group_name,class_room_id:this.class_room_id,class_room:this.class_room}});
  }

  loadData(){
    this.service.httpGet('/api/v1/trn-classroom-member/'+this.id,null).then((res:IResponse)=>{
      let data = res.responseData; 
      this.activityForm.controls.classRoomMemberId.setValue(data.classRoomMemberId||'');   
      this.subjectForm.controls.stuCode.setValue(data.stuCode||'');  
      this.subjectForm.controls.yearGroupId.setValue(data.yearGroupId||'');  
      this.subjectForm.controls.classRoomId.setValue(data.classRoomId||'');     
      this.stu_code = data.stuCode;
      this.fullname = data.fullname;
      this.class_room = data.classRoom;
      this.year_group_id = data.yearGroupId;
      this.class_room_id = data.classRoomId;
      this.year_group_name = data.yearGroupName;
      this.loadActivity();
      this.loadSubject();
    });
  }

  loadActivity(){
    this.service.httpGet('/api/v1/trn-classroom-member/activity',this.activityForm.value).then((res:IResponse)=>{   
      this.dataSource.data = res.responseData||[];
    });
  }

  loadSubject(){
    this.service.httpGet('/api/v1/trn-class-room-subject/subjectinstu',this.subjectForm.value).then((res:IResponse)=>{
     if(res.responseData.length === 0){
      this.checksubject = true;
     }
     else {
      this.dataSource2.data = res.responseData||[];
      this.checksubject = false;
     }
     
    });
  }

}
