import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { MatTableDataSource, MatPaginator } from '@angular/material';
import { ApiService, IResponse } from 'src/app/shared/service/api.service';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-aca012',
  templateUrl: './aca012.component.html',
  styleUrls: ['./aca012.component.scss']
})
export class Aca012Component implements OnInit {

  searchForm = this.fb.group({
    yearGroupId: [''],
    classRoom: [''] 
  });

  reportForm = this.fb.group({
    yearGroupId: [''],
    classId: [''],
    classRoomId: [''],
    stuCode: ['']
  });

  classRoomList: [];
  yearGroupList: [];

  stuCode: any;
  catClassId: any;

  checkData = false;
  isProcess = false;

  headerSelected: boolean;
  isSelected: any;
  checklist: any;
  allChecklist: any;

  checkChoose = false;

  dataSource = new MatTableDataSource();
  displayedColumns: string[] = ['check', 'classRoomNo', 'stuCode', 'fullname'];


  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  constructor(
    private translate: TranslateService,
    private fb: FormBuilder,
    private service: ApiService,
  ) { }
 
  ngOnInit() {
    this.ddlYearGroup();
    this.ddlClassRoomList();
    this.onSearch(0); 
  }

  onSearch(e) {
    this.isProcess = true;
    this.service.httpGet('/api/v1/trn-classroom-member/stu', this.searchForm.value).then((res: IResponse) => {
      if (res.responseData.length === 0) {
        this.dataSource.data = [];
        this.checkData = true;
        this.checklist = this.dataSource.data;

      } else {
        this.dataSource.data = res.responseData || [];
        this.checkData = false; 
        this.checklist = this.dataSource.data;
      }
    });
  } 

  ddlClassRoomList() {
    this.service.httpGet('/api/v1/0/classroom-list/ddlisep', null).then((res) => {
      this.classRoomList = res || [];
    });
  }

  ddlYearGroup() {
    this.service.httpGet('/api/v1/0/year-group/ddl', null).then((res) => {
      this.yearGroupList = res || [];
      this.searchForm.controls.yearGroupId.setValue(res[0]['yearGroupId'.toString()]);
    });
  }


  checkAll() {
    for (const i in this.checklist) {
      this.checklist[i].isSelected = this.headerSelected;
    } 
    this.getCheckedItemList();
  }

  isAllSelected() {
    this.checklist.every((item: any) => {
      return item.isSelected === true;
    });
    this.headerSelected = false;
    this.getCheckedItemList();
  }

  getCheckedItemList() { 
    this.allChecklist = []; 
    for (const i in this.checklist) {
      if (this.checklist[i].isSelected === true) {
        const itemData = {
          yearGroupId: this.checklist[i].year_group_id,
          classId: this.checklist[i].class_id,
          classRoomId: this.checklist[i].class_room_id,
          stuCode: this.checklist[i].stu_code,
          catClassId: this.checklist[i].cat_class_id
        };
        this.allChecklist.push(itemData);
        this.checkChoose = true; 
      }
    }
  }

  onReport() {
    this.stuCode = [];
    if (this.checkChoose === true) {
      this.isProcess = true;
      for (const i in this.allChecklist) {
        this.reportForm.value.yearGroupId = this.allChecklist[0].yearGroupId;
        this.reportForm.value.classId = this.allChecklist[0].classId;
        this.reportForm.value.classRoomId = this.allChecklist[0].classRoomId;
        this.catClassId = this.allChecklist[0].catClassId;
        this.stuCode.push('\'' + this.allChecklist[i].stuCode + '\'');
      }
      this.reportForm.value.stuCode = this.stuCode;

      if (this.catClassId === 2 || this.catClassId === 3 || this.catClassId === 4) {
        this.service.httpPreviewPDF('/api/v1/aca012/report/pdf', this.reportForm.value).then((res) => {
          this.isProcess = false;
          window.location.href = res;

        });
      } else if (this.catClassId === 1) {
        this.service.httpPreviewPDF('/api/v1/aca012/report/pdf2', this.reportForm.value).then((res) => {
          this.isProcess = false;
          window.location.href = res;

        });
      }
    }
  }

}
