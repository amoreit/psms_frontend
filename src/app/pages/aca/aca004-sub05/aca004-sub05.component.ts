import { Component, OnInit, ViewChild, Inject } from '@angular/core';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { MatPaginator, MatTableDataSource, MAT_DIALOG_DATA, MatDialog } from '@angular/material';
import { ApiService,IResponse,EHttp } from '../../../shared/service/api.service';
import { ActivatedRoute,Router } from '@angular/router';
import { UtilService } from 'src/app/_util/util.service';
import Swal from 'sweetalert2/dist/sweetalert2.js';
import 'sweetalert2/src/sweetalert2.scss';
import { TouchSequence } from 'selenium-webdriver';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-aca004-sub05',
  templateUrl: './aca004-sub05.component.html',
  styleUrls: ['./aca004-sub05.component.scss']
})
export class Aca004Sub05Component implements OnInit {

  isProcess:boolean = false;
  searchForm = this.fb.group({
    'yearGroupId':['']
    ,'classId':['']
    ,'classRoomId':['']
    , 'p':['']
    // , 'result':[40]
  });

  form = this.fb.group({
    'yearGroupId':['']
    ,'classId':['']
    ,'classRoomId':['']
    ,'classRoomSubjectId': ['']
  });

  pageSize:number;
  displayedColumns: string[] = ['subjectCode', 'subjectName','subjectStatus'];
  dataSource = new MatTableDataSource();

  citizenId:any;
  yearGroupList:[];
  classList: [];
  classRoomList: [];

  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  constructor(
    private ar:ActivatedRoute,
    private fb: FormBuilder,
    private service:ApiService,
    public dialog: MatDialog,
    private router: Router,
    private translate: TranslateService,
    public utilService: UtilService
  ) { }

  ngOnInit() {
    this.citizenId = localStorage.getItem('citizenId') || '';
    this.searchForm.controls.yearGroupId.setValue(this.ar.snapshot.queryParamMap.get('yearGroupId')||'')
    this.searchForm.controls.classId.setValue(this.ar.snapshot.queryParamMap.get('classId')||'')
    this.searchForm.controls.classRoomId.setValue(this.ar.snapshot.queryParamMap.get('classRoomId')||'')
    this.ddlYearGroup();
    this.ddlClass();
    this.ddlClassRoom();
    this.onSearch(0); 
  }
  /* DROP DOWN ปีการศึกษา */
  ddlYearGroup() {
    this.service.httpGet('/api/v1/0/year-group/ddl', null).then((res) => {
      this.yearGroupList = res || [];
      let ele = this.yearGroupList.filter((o) => {
        return o['iscurrent'] == "true";
      });
      this.searchForm.controls.yearGroupId.setValue(ele[0]['yearGroupId'])
    });
  } 
  /* DROP DOWN ชั้นเรียน */
  ddlClass() {
    this.service.httpGet('/api/v1/0/class/ddlbycitizenid',{'citizenId': this.citizenId}).then((res) => {
      this.classList = res || [];
    });
  }
  /* DROP DOWN ห้องเรียน */
  ddlClassRoom() {
    let jsonClassRoom = {'citizenId': this.citizenId,'classId': this.searchForm.value.classId};
    this.service.httpGet('/api/v1/0/classroom-list/ddlfiltercitizenid', jsonClassRoom).then((res) => {
      this.classRoomList = res || [];
    });
  }
  onSearch(e){
    this.searchForm.value.p = (this.paginator.pageIndex = e.pageIndex||0)+1;
    this.service.httpGet('/api/v1/classRoom-subject',this.searchForm.value).then((res:IResponse)=>{
      this.dataSource.data = res.responseData||[];
      this.pageSize = res.responseSize||0;
    });
  } 

  onForm(yearGroupId,classRoomSubjectId){
    this.form.controls.yearGroupId.setValue(yearGroupId);
    this.form.controls.classId.setValue(this.searchForm.value.classId);
    this.form.controls.classRoomId.setValue(this.searchForm.value.classRoomId);
    this.form.controls.classRoomSubjectId.setValue(classRoomSubjectId);
    this.router.navigate(['/aca/aca004-sub05/form'],{ queryParams : this.form.value});
  };

  onCancel(){
    this.searchForm.controls.yearGroupId.setValue('');
    this.searchForm.controls.classId.setValue('');
    this.searchForm.controls.classRoomId.setValue('');
    this.onSearch(0);
  }
}
