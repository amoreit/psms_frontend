import { Component, OnInit, ViewChild, Inject } from '@angular/core';
import { FormBuilder, Validators, FormGroup, FormArray } from '@angular/forms';
import { MatPaginator, MatTableDataSource, MAT_DIALOG_DATA, MatDialog } from '@angular/material';
import { ApiService,IResponse,EHttp } from '../../../../shared/service/api.service';
import { ActivatedRoute,Router } from '@angular/router';
import { UtilService } from 'src/app/_util/util.service';
import Swal from 'sweetalert2/dist/sweetalert2.js';
import 'sweetalert2/src/sweetalert2.scss';
import { TouchSequence } from 'selenium-webdriver';
import { TranslateService } from '@ngx-translate/core';

export interface score {
  score1: string;
  score2: string; 
  score3: string;
  score4: string;
  score5: string;
  score6: string;
  score7: string; 
  score8: string;
  score9: string;
}

@Component({
  selector: 'app-aca004-sub05-form',
  templateUrl: './aca004-sub05-form.component.html',
  styleUrls: ['./aca004-sub05-form.component.scss']
})
export class Aca004Sub05FormComponent implements OnInit {

  isProcess:boolean = false;
  form = this.fb.group({
    'yearGroupId':['']
    ,'classId':['']
    ,'classRoomId':['']
    ,'classRoomSubjectId': ['']
    ,'p':['']
    ,'result':[50]
  });

  form2 = this.fb.group({
    'yearGroupId':['']
    ,'classRoomSubjectId': ['']
    ,'p':['']
    ,'result':[50]
  });

  pageSize:number;
  displayedColumns: string[] = ['no', 'stuCode', 'fullname','score1','score2','score3','score4','score5','score6','score7','score8','score9'];
  dataSource = new MatTableDataSource();

  subjectCode ='';
  subjectName ='';
  credit ='';
  classRoomSnameTh ='';
  yearGroupName ='';
  term ='';

  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  constructor(
    ar:ActivatedRoute,
    private fb: FormBuilder,
    private service:ApiService,
    public dialog: MatDialog, 
    private router: Router,
    private translate: TranslateService,
    public utilService: UtilService
  ) 
  {
    this.form.controls.yearGroupId.setValue(ar.snapshot.queryParamMap.get('yearGroupId'));
    this.form.controls.classId.setValue(ar.snapshot.queryParamMap.get('classId'));
    this.form.controls.classRoomId.setValue(ar.snapshot.queryParamMap.get('classRoomId'));
    this.form.controls.classRoomSubjectId.setValue(ar.snapshot.queryParamMap.get('classRoomSubjectId')); 
  }

  ngOnInit() {
    this.onSearch(0);
  }

  onSearch(e){
    // this.form.value.p = (this.paginator.pageIndex = e.pageIndex||0)+1;
    this.service.httpGet('/api/v1/classRoom-member/searchAca004Sub05Form',this.form.value).then((res:IResponse)=>{
      this.dataSource.data = res.responseData||[];
      // this.pageSize = res.responseSize||0;
      this.subjectCode = res.responseData[0].subjectCode;
      this.subjectName = res.responseData[0].subjectName;
      this.classRoomSnameTh = res.responseData[0].classRoomSnameTh;
      let yearGroupName = res.responseData[0].yearGroupName;
      this.yearGroupName = yearGroupName.substring(2);
      let term = res.responseData[0].yearGroupName;
      this.term = term.substring(0,1);
    });
  }

  onChoose(o){
    this.dataSource.data.map(e=>{
      e['score1'] = o.target.value;
      e['score2'] = o.target.value;
      e['score3'] = o.target.value;
      e['score4'] = o.target.value;
      e['score5'] = o.target.value;
      e['score6'] = o.target.value;
      e['score7'] = o.target.value;
      e['score8'] = o.target.value;
      e['score9'] = o.target.value;
      return e;
    });
  }

  onSave(){

    const myData = this.dataSource.data.map((row: score) => {
      return {
          score1: row.score1
        , score2: row.score2
        , score3: row.score3
        , score4: row.score4
        , score5: row.score5
        , score6: row.score6
        , score7: row.score7
        , score8: row.score8
        , score9: row.score9
      }
    });
    
    Swal.fire({
      title: this.translate.instant('psms.DL0001'),//ต้องการบันทึกข้อมูล ใช่หรือไม่?
      icon : 'info',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: this.translate.instant('psms.DL0008'),
      cancelButtonText: this.translate.instant('psms.DL0009')
  }).then((result) => {
    if(result.dismiss == 'cancel'){
        return;
    }
    let json = this.dataSource.data;
    let user = localStorage.getItem('userName');
    if(this.dataSource.data[0]['classRoomDesiredCharScoreId'] == null){
        this.service.httpPost('/api/v1/desiredCharScore/'+user,json).then((res:IResponse)=>{
            this.isProcess = false;
            if ((res.responseCode || 500) != 200) {
              Swal.fire(
                this.translate.instant('message.add_error'),//เพิ่มข้อมูลผิดพลาด
                res.responseMsg,
                'error'
              );
              return;
            }
            Swal.fire(
              this.translate.instant('alert.add_header'),//เพิ่มข้อมูล
              this.translate.instant('message.add'),//เพิ่มข้อมูลสำเร็จ
              'success'
            ).then(() => {this.onBackPage('','','');})
          });
          return;
        }
        this.service.httpPut('/api/v1/desiredCharScore/'+user,json).then((res:IResponse)=>{
          if ((res.responseCode || 500) != 200) {
            Swal.fire(
              this.translate.instant('message.edit_error'),//แก้ไขข้อมูลผิดพลาด
              res.responseMsg,
              'error'
            )
            return;
          }
          Swal.fire(
            this.translate.instant('psms.DL0006'),//แก้ไขข้อมูล
            this.translate.instant('message.edit'),//แก้ไขข้อมูลสำเร็จ
            'success'
          ).then(() => {this.onBackPage('','','')})
        });
    });
  }

  onBackPage(yearGroupId,className,classRoomId){
    this.router.navigate(['/aca/aca004-sub05'],{ queryParams: 
      {yearGroupId:this.form.value.yearGroupId
      ,classId:this.form.value.classId
      ,classRoomId:this.form.value.classRoomId}});
  }

}
