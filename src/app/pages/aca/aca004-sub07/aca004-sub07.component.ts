import { Component, OnInit, ViewChild } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder } from '@angular/forms';
import { ApiService, IResponse } from 'src/app/shared/service/api.service';
import { MatDialog, MatTableDataSource, MatPaginator } from '@angular/material';
import { UtilService } from 'src/app/_util/util.service';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-aca004-sub07',
  templateUrl: './aca004-sub07.component.html', 
  styleUrls: ['./aca004-sub07.component.scss']
})
export class Aca004Sub07Component implements OnInit {
  searchForm = this.fb.group({
    'yearGroupId': ['']
    , 'classId': ['']
    , 'classRoomId': ['']
    , 'citizenId': ['']
  });
  displayedColumns: string[] = ['subjectCode', 'subjectName', 'manage'];
  dataSource = new MatTableDataSource();
  classList: [];
  classroomnoList: [];
  yearGroupList: [];
  checkdata = false;

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  constructor(private ar: ActivatedRoute, private fb: FormBuilder, private service: ApiService, public dialog: MatDialog, private router: Router, private translate: TranslateService, public _util: UtilService) {
  }

  ngOnInit() {
    this.searchForm.controls.yearGroupId.setValue(this.ar.snapshot.queryParamMap.get('year_group_id') || '')
    this.searchForm.controls.classId.setValue(this.ar.snapshot.queryParamMap.get('class_id') || '')
    this.searchForm.controls.classRoomId.setValue(this.ar.snapshot.queryParamMap.get('class_room_id') || '')
    this.searchForm.controls.citizenId.setValue(localStorage.getItem('citizenId') || '')
    this.ddlClass();
    this.ddlClassRoomNo();
    this.ddlYearGroup();
    this.onSearch(0);
  }

  onSearch(e) {
    this.service.httpGet('/api/v1/trn-class-room-subject/subjectaca004sub07', this.searchForm.value).then((res: IResponse) => {
      if (res.responseData.length === 0) {
        this.dataSource.data = []
        this.checkdata = true;
      } else {
        this.dataSource.data = res.responseData || [];
        this.checkdata = false;
      }
    });
  }


  onCancel() {
    this.searchForm = this.fb.group({
      'yearGroupId': ['']
      , 'classId': ['']
      , 'classRoomId': ['']
    });
    this.onSearch(0);
  }

  onForm(id) {
    this.router.navigate(['/aca/aca004-sub07/form'], { queryParams: { id: id } });
  }

  ddlClass() {
    this.service.httpGet('/api/v1/0/class/ddlbycitizenid', { 'citizenId': this.searchForm.value.citizenId }).then(res => {
      this.classList = res || [];
    });
  }

  ddlClassRoomNo() {
    const json = { 'citizenId': this.searchForm.value.citizenId, 'classId': this.searchForm.value.classId };
    this.service
      .httpGet('/api/v1/0/classroom-list/ddlfiltercitizenid', json)
      .then(res => {
        this.classroomnoList = res || [];
      });
  }

  ddlYearGroup() {
    this.service.httpGet('/api/v1/0/year-group/ddl', null).then((res) => {
      this.yearGroupList = res || [];
      if (this.ar.snapshot.queryParamMap.get('year_group_id') != null) {
        return;
      } else {
        let ele = this.yearGroupList.filter((o) => {
          return o['iscurrent'] == "true";
        });
        this.searchForm.controls.yearGroupId.setValue(ele[0]['yearGroupId'])
      }
    });
  }



}
