import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { MatTableDataSource, MatDialog } from '@angular/material';
import { FormBuilder } from '@angular/forms';
import { ApiService, IResponse } from 'src/app/shared/service/api.service';
import Swal from 'sweetalert2/dist/sweetalert2.js';
import 'sweetalert2/src/sweetalert2.scss';
import { TranslateService } from '@ngx-translate/core';

export interface competenciesScore {
  score1: any; 
  score2: any;
  score3: any;
  score4: any;
  score5: any;
}

@Component({
  selector: 'app-aca004-sub07-form',
  templateUrl: './aca004-sub07-form.component.html',
  styleUrls: ['./aca004-sub07-form.component.scss']
})
export class Aca004Sub07FormComponent implements OnInit {

  subjectForm = this.fb.group({
    'classRoomSubjectId': ['']
  });

  memberForm = this.fb.group({
    'yearGroupId': ['']
    , 'classRoomId': ['']
    , 'classRoomSubjectId': ['']
  });

  dataSource = new MatTableDataSource();
  displayedColumns: string[] = ['no', 'stuCode', 'fullname', 'col1', 'col2', 'col3', 'col4', 'col5'];

  constructor(private ar: ActivatedRoute, private fb: FormBuilder, private service: ApiService, public dialog: MatDialog, private translate: TranslateService, private router: Router) { }

  id: any;
  subject_code: any;
  subject_name_th: any;
  year_group: any;
  year_group_id: any;
  class_id: any;
  class_room_id: any;
  chooseAll: any;
  class_room_name: any;


  ngOnInit() {
    this.id = this.ar.snapshot.queryParamMap.get('id') || '';
    this.subjectForm.controls.classRoomSubjectId.setValue(this.id);
    this.memberForm.controls.classRoomSubjectId.setValue(this.id);
    this.loadData();
  }

  onBack(year_group_id, class_id, class_room_id) {
    this.router.navigate(['aca/aca004-sub07'], { queryParams: { year_group_id: this.year_group_id, class_id: this.class_id, class_room_id: this.class_room_id } });
  }

  loadData() {
    this.service.httpGet('/api/v1/trn-class-room-subject/detailsubject', this.subjectForm.value).then((res: IResponse) => {
      let data = res.responseData;
      this.memberForm.controls.yearGroupId.setValue(data[0]['year_group_id']);
      this.memberForm.controls.classRoomId.setValue(data[0]['class_room_id']);
      this.year_group_id = data[0]['year_group_id'];
      this.class_id = data[0]['class_id']
      this.class_room_id = data[0]['class_room_id'];
      this.class_room_name = data[0]['class_room_sname_th'];
      this.subject_code = data[0]['subject_code'];
      this.subject_name_th = data[0]['subject_name_th'];
      this.year_group = data[0]['name'];
      this.loadMember();
    });
  }

  loadMember() {
    this.service.httpGet('/api/v1/trn-classroom-member/memberfromsubject', this.memberForm.value).then((res: IResponse) => {
      this.dataSource.data = res.responseData || [];
    });
  }

  onChoose(o) {
    this.dataSource.data.map(e => {
      e['score1'] = o.target.value;
      e['score2'] = o.target.value;
      e['score3'] = o.target.value;
      e['score4'] = o.target.value;
      e['score5'] = o.target.value;
      return e;
    });
  }

  onChooseSomeone() {
    this.chooseAll = undefined;
  }

  onSave2() {
    const myData = this.dataSource.data.map((row: competenciesScore) => {
      return {
        score1: row.score1,
        score2: row.score2,
        score3: row.score3,
        score4: row.score4,
        score5: row.score5,
      }
    });

    Swal.fire({
      title: this.translate.instant('psms.DL0001'),
      icon: 'info',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: this.translate.instant('psms.DL0008'),
      cancelButtonText: this.translate.instant('psms.DL0009')
    }).then((result) => {
      if (result.dismiss == 'cancel') {
        return;
      }
      let json = this.dataSource.data;
      let user = localStorage.getItem('userName')

      this.service.httpPut('/api/v1/class-room-competencies-score/' + user, json).then((res: IResponse) => {
        if ((res.responseCode || 500) != 200) {
          Swal.fire(
            this.translate.instant('message.edit_error'),
            res.responseMsg,
            'error'
          )
          return;
        }
        Swal.fire(
          this.translate.instant('psms.DL0006'),
          this.translate.instant('message.edit'),
          'success'
        ).then(() => { this.onBack(this.year_group_id, this.class_id, this.class_room_id) })
      });
    });
  }

}
