import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-aca015-form1',
  templateUrl: './aca015-form1.component.html',
  styleUrls: ['./aca015-form1.component.css']
})
export class Aca015Form1Component implements OnInit {

  constructor() { }

  ngOnInit() {
  }

  onPrint(printId,printId2){
    let printContents = document.getElementById(printId).innerHTML;
    let printContents2 = document.getElementById(printId2).innerHTML;
    let originalContents = document.body.innerHTML;
    document.body.innerHTML = printContents+printContents2;
    window.print();
    document.body.innerHTML = originalContents;
    location.reload();
  }
  
}
