import { Component, OnInit, ViewChild, Inject } from "@angular/core";
import { FormBuilder, Validators, FormGroup } from "@angular/forms";
import {MatDialog} from "@angular/material";
import {ApiService,IResponse,EHttp} from "../../../shared/service/api.service";
import { Router } from "@angular/router";
import Swal from "sweetalert2/dist/sweetalert2.js";
import "sweetalert2/src/sweetalert2.scss";
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-aca015',
  templateUrl: './aca015.component.html',
  styleUrls: ['./aca015.component.scss']
})
export class Aca015Component implements OnInit {

  searchForm = this.fb.group({
    yearGroupId: [""],
    type:[""],
  });
  classList=[""];
  yearList=[""];

  constructor(
    private translate: TranslateService,
    private fb: FormBuilder,
    private service: ApiService,
    public dialog: MatDialog,
    private router: Router) { }

    ngOnInit() {
      this.searchForm.controls.type.setValue("1");
      this.ddlClass();
      this.ddlYear();
    }
  
    ddlClass() {
      this.service.httpGet("/api/v1/0/class/ddl", null).then(res => {this.classList = res || [];});
    }
    ddlYear() {
      this.service.httpGet("/api/v1/0/year-group/ddl", null).then(res => {this.yearList = res || [];this.searchForm.controls.yearGroupId.setValue(res[0]['yearGroupId']);});
    }
    onCancel(){
      this.searchForm.controls.yearGroupId.setValue('');
      this.searchForm.controls.classId.setValue('');
      this.ngOnInit();
    }
    onForm(){
      if(this.searchForm.value.type=='1'){
        this.router.navigate(["/aca/aca015/form1"]);
      }
      else if(this.searchForm.value.type =='2'){
        this.router.navigate(["/aca/aca015/form2"]);
      }
      else{
        this.router.navigate(["/aca/aca015/form3"]);
      }
    }
  }
