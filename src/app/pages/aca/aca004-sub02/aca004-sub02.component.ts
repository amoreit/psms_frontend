import { Component, OnInit} from '@angular/core';
import { MatDialog, MatTableDataSource } from '@angular/material';
import { FormBuilder} from '@angular/forms';
import { ApiService, IResponse } from 'src/app/shared/service/api.service';
import { UtilService } from 'src/app/_util/util.service';
import Swal from 'sweetalert2/dist/sweetalert2.js';
import 'sweetalert2/src/sweetalert2.scss'; 
import { TranslateService } from '@ngx-translate/core';

export interface activity1 { 
  activity1result:any;
}

export interface activity2 {
  activity2result:any;
}

export interface activity3 {
  activity3result:any;
}

export interface activity4 { 
  activity4result:any;
} 

@Component({
  selector: 'app-aca004-sub02',
  templateUrl: './aca004-sub02.component.html',
  styleUrls: ['./aca004-sub02.component.scss'] 
})
export class Aca004Sub02Component implements OnInit {

  searchForm = this.fb.group({
    'yearGroupName':['']
    , 'activityGroupId':['']
    , 'classId':['']
  }); 

  activity1All:any;
  activity2All:any;
  activity3All:any;
  activity4All:any;

  _activityGroupId:any;
  userName:any;
  yearGroupId:any; 
  dataSourceLength:any;

  yearGroupList:[];
  classList:[];
  activityGroupList:[];
  dataSourceActivity:[];
  dataSourceActivityList:[];

  datalist:any;
  checkSearch = false;

  displayedColumns: string[] = ['yearGroupName','classRoom','classRoomNo','fullname','acName','manage','acLesson']; 
  displayedColumnsTea: string[] = ['no','fullname','manage']; 

  dataSource = new MatTableDataSource();
  dataSourceTea = new MatTableDataSource();

  constructor( 
      private fb: FormBuilder,
      private service: ApiService,
      public util: UtilService,
      private translate: TranslateService,
      public dialog: MatDialog
  ) { } 
 
  ngOnInit() {   
    this.userName = localStorage.getItem('userName')||'';
    this.ddlYearGroup();
    this.ddlClass();
  } 

  ddlYearGroup(){ 
    this.service.httpGet('/api/v1/0/year-group/ddl',null).then((res)=>{
      this.yearGroupList = res||[];
      this.searchForm.controls.yearGroupName.setValue(res[0]['name']);
    });
  }

  ddlClass(){ 
    this.service.httpGet('/api/v1/0/tbMstClass/ddl',null).then((res)=>{
      this.classList= res||[];
    });  
  }

  ddlActivityGroup(){
    if(this.searchForm.value.classId == 1 || this.searchForm.value.classId == 2 || this.searchForm.value.classId == 3){
      this.service.httpGet('/api/v1/0/activity-grouplist/kindergarten',null).then((res)=>{
        this.activityGroupList= res||[];
      }); 
    }else{
      this.service.httpGet('/api/v1/0/activity-grouplist/juniorup',null).then((res)=>{
        this.activityGroupList= res||[];
      }); 
    }
  }

  onSearch(){ 
    this.checkSearch = true;
    this.service.httpGet('/api/v1/_activity',this.searchForm.value).then((res:IResponse)=>{
      this.dataSourceActivity = res.responseData||[];
      let data3 = [];
      for(let i=0; i <= this.dataSourceActivity.length-1; i++){
        this._activityGroupId = this.dataSourceActivity[i]['activityGroupId'];
        let json = { 'activityName':this.dataSourceActivity[i]['activityName'],'activityGroupId':this.dataSourceActivity[i]['activityGroupId'],'yearGroupName':this.searchForm.value.yearGroupName,'classId':this.searchForm.value.classId};
        this.service.httpGet('/api/v1/_trnClassRoomMember/activity-stu-result',json).then((res:IResponse)=>{
            this.dataSource.data[i] = res.responseData||[];
        });
      } 
    });
  } 

  //---------------------- activity1
  selectedAc1All(result){
    for(let i=0; i <= this.dataSourceActivity.length-1; i++){
      this.datalist = this.dataSource.data[i];
      this.datalist.map(o => {
        return o['activity1result'] = result;
      });
    } 
  }

  selectedAc1Header(){
    this.activity1All = undefined;
  }

  //---------------------- activity2
  selectedAc2All(result){
    for(let i=0; i <= this.dataSourceActivity.length-1; i++){
      this.datalist = this.dataSource.data[i];
      this.datalist.map(o => {
        return o['activity2result'] = result;
      });
    } 
  }

  selectedAc2Header(){
    this.activity2All = undefined;
  }

  //---------------------- activity3
  selectedAc3All(result){
    for(let i=0; i <= this.dataSourceActivity.length-1; i++){
      this.datalist = this.dataSource.data[i];
      this.datalist.map(o => {
        return o['activity3result'] = result;
      });
    } 
  }

  selectedAc3Header(){
    this.activity3All = undefined;
  }

  //---------------------- activity4
  selectedAc4All(result){
    for(let i=0; i <= this.dataSourceActivity.length-1; i++){
      this.datalist = this.dataSource.data[i];
      this.datalist.map(o => {
        return o['activity4result'] = result;
      });
    } 
  }

  selectedAc4Header(){
    this.activity4All = undefined;
  }

  onSave(){
    Swal.fire({
      title: this.translate.instant('psms.DL0001'),//ต้องการบันทึกข้อมูล ใช่หรือไม่?
      icon : 'info',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: this.translate.instant('psms.DL0008'),
      cancelButtonText: this.translate.instant('psms.DL0009')
    }).then((result) => {
      if(result.dismiss == 'cancel'){
          return;
      }
      //--------------------------------- update activity result_1
      if(this.searchForm.value.activityGroupId == 1 || this.searchForm.value.activityGroupId == 5){
        for(let i=0; i <= this.dataSourceActivity.length-1; i++){
          this.datalist = this.dataSource.data[i];
          this.datalist.map((row: activity1) => {
            return { activity1result: row.activity1result }
          });
          let jsonAc1 = this.datalist;
          this.service.httpPut('/api/v1/_trnClassRoomMember/activity1/'+this.userName,jsonAc1).then((res:IResponse)=>{
            if((res.responseCode||500)!=200){
              Swal.fire(
                this.translate.instant('message.save_error'),//บันทึกข้อมูลผิดพลาด
                res.responseMsg,
                'error' 
              )
              return;
            }
          });
        }Swal.fire(
          this.translate.instant('message.save_header'),//บันทึกข้อมูล
          this.translate.instant('message.save'),//บันทึกข้อมูลสำเร็จ
          'success'
        ).then(() => {this.reload();})
      //--------------------------------- update activity result_2
      }else if(this.searchForm.value.activityGroupId == 2 || this.searchForm.value.activityGroupId == 6){
        for(let i=0; i <= this.dataSourceActivity.length-1; i++){
          this.datalist = this.dataSource.data[i];
          this.datalist.map((row: activity2) => {
            return { activity2result: row.activity2result }
          });
          let jsonAc2 = this.datalist;
          this.service.httpPut('/api/v1/_trnClassRoomMember/activity2/'+this.userName,jsonAc2).then((res:IResponse)=>{
            if((res.responseCode||500)!=200){
              Swal.fire(
                this.translate.instant('message.save_error'),//บันทึกข้อมูลผิดพลาด
                res.responseMsg,
                'error' 
              )
              return;
            }
          });
        }Swal.fire(
          this.translate.instant('message.save_header'),//บันทึกข้อมูล
          this.translate.instant('message.save'),//บันทึกข้อมูลสำเร็จ
          'success'
        ).then(() => {this.reload();})
      //--------------------------------- update activity result_3
      }else if(this.searchForm.value.activityGroupId == 3 || this.searchForm.value.activityGroupId == 7){
        for(let i=0; i <= this.dataSourceActivity.length-1; i++){
          this.datalist = this.dataSource.data[i];
          this.datalist.map((row: activity3) => {
            return { activity3result: row.activity3result}
          });
          let jsonAc3 = this.datalist;
          this.service.httpPut('/api/v1/_trnClassRoomMember/activity3/'+this.userName,jsonAc3).then((res:IResponse)=>{
            if((res.responseCode||500)!=200){
              Swal.fire(
                this.translate.instant('message.save_error'),//บันทึกข้อมูลผิดพลาด
                res.responseMsg,
                'error' 
              )
              return;
            }
          });
        }Swal.fire(
          this.translate.instant('message.save_header'),//บันทึกข้อมูล
          this.translate.instant('message.save'),//บันทึกข้อมูลสำเร็จ
          'success'
        ).then(() => {this.reload();})
      //--------------------------------- update activity result_4
      }else{
        for(let i=0; i <= this.dataSourceActivity.length-1; i++){
          this.datalist = this.dataSource.data[i];
          this.datalist.map((row: activity4) => {
            return { activity4result: row.activity4result }
          });
          let jsonAc4 = this.datalist;
          this.service.httpPut('/api/v1/_trnClassRoomMember/activity4/'+this.userName,jsonAc4).then((res:IResponse)=>{
            if((res.responseCode||500)!=200){
              Swal.fire(
                this.translate.instant('message.save_error'),//บันทึกข้อมูลผิดพลาด
                res.responseMsg,
                'error' 
              )
              return;
            }
          });
        }Swal.fire(
          this.translate.instant('message.save_header'),//บันทึกข้อมูล
          this.translate.instant('message.save'),//บันทึกข้อมูลสำเร็จ
          'success'
        ).then(() => {this.reload();})
      }
    });
  }

  reload(){
    this.searchForm.controls.classId.setValue(this.searchForm.value.classId);
    this.searchForm.controls.activityGroupId.setValue(this.searchForm.value.activityGroupId);
    this.onSearch();
  }

}
