import { Component, OnInit, ViewChild, Inject } from "@angular/core";
import { FormBuilder, Validators, FormGroup } from "@angular/forms";
import {MatPaginator,MatTableDataSource,MAT_DIALOG_DATA,MatDialog} from "@angular/material";
import {ApiService,IResponse,EHttp} from "../../../shared/service/api.service";
import { Router } from "@angular/router";
import Swal from "sweetalert2/dist/sweetalert2.js";
import "sweetalert2/src/sweetalert2.scss";
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-aca013',
  templateUrl: './aca013.component.html',
  styleUrls: ['./aca013.component.scss']
})
export class Aca013Component implements OnInit {

  searchForm = this.fb.group({
    yearGroupId: [""],
    classId:[""],
    type:[""],
    result: [10],
    p: [""],
  });
  yearList: [];
  classList: [];
  pageSize: number;
  dataSource = new MatTableDataSource(); 
  displayedColumns: string[] = [
    'check','no', 'class','btn'
  ];

  constructor(
    private translate: TranslateService,
    private fb: FormBuilder,
    private service: ApiService,
    public dialog: MatDialog,
    private router: Router) { }

  ngOnInit() {
    this.searchForm.controls.type.setValue("1");
    this.ddlClass();
    this.ddlYear();
    this.onSearch(0);
  }
  onSearch(e) {

  }
  ddlClass() {
    this.service.httpGet("/api/v1/0/class/ddl", null).then(res => {this.classList = res || [];});
  }
  ddlYear() {
    this.service.httpGet("/api/v1/0/year-group/ddl", null).then(res => {this.yearList = res || [];this.searchForm.controls.yearGroupId.setValue(res[0]['yearGroupId']);});
  }
  onCancel(){
    this.searchForm.controls.yearGroupId.setValue('');
    this.searchForm.controls.classId.setValue('');
    this.ngOnInit();
  }
  onForm(){
    if(this.searchForm.value.type=='1'){
      this.router.navigate(["/aca/aca013/form1"]);
    }
    else{
      this.router.navigate(["/aca/aca013/form2"]);
    }
  }
}
