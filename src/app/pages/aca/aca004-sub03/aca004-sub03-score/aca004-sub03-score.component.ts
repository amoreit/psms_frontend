import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator, MatTableDataSource } from '@angular/material';
import { IResponse, ApiService } from 'src/app/shared/service/api.service'; 
import { ActivatedRoute, Router } from '@angular/router';
import { FormBuilder } from '@angular/forms';
import { UtilService } from 'src/app/_util/util.service';
import Swal from 'sweetalert2/dist/sweetalert2.js';
import 'sweetalert2/src/sweetalert2.scss'; 
import { TranslateService } from '@ngx-translate/core';
 
export interface classRatw {
  score1: string;
  score2: string;
  score3: string; 
  score4: string;
  score5: string;
}
 
@Component({  
  selector: 'app-aca004-sub03-score',
  templateUrl: './aca004-sub03-score.component.html', 
  styleUrls: ['./aca004-sub03-score.component.scss']
})

export class Aca004Sub03ScoreComponent implements OnInit { 

  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  searchMember = this.fb.group({ 
    'classRoomSubjectId':[''] 
      , 'yearGroupName':[''] 
      , 'classRoom':['']
  }); 
  
  classRoomSubjectId:any;
  subjectCode:String;
  subjectNameTh:String;
  yearGroupName:String;
  classRoomSnameTh:String;
  userName:String;
  ratwScoreList:[];
  ratw_core_length:any;
  headerAll:any;

  displayedColumns: string[] = ['no', 'stuCode', 'fullname', 'score1', 'score2', 'score3', 'score4', 'score5'];
  dataSource = new MatTableDataSource();
  
  constructor( 
      ar:ActivatedRoute,
      private fb: FormBuilder, 
      private service:ApiService,
      private router: Router,
      private translate: TranslateService,
      public util: UtilService 
  ) {
      this.classRoomSubjectId = ar.snapshot.queryParamMap.get('classRoomSubjectId')||'';
      this.subjectCode = ar.snapshot.queryParamMap.get('subjectCode')||'';
      this.subjectNameTh = ar.snapshot.queryParamMap.get('subjectNameTh')||'';
      this.yearGroupName = ar.snapshot.queryParamMap.get('yearGroupName')||'';
      this.classRoomSnameTh = ar.snapshot.queryParamMap.get('classRoomSnameTh')||'';
  }

  ngOnInit(){
      this.searchMember.controls.classRoomSubjectId.setValue(this.classRoomSubjectId);
      this.searchMember.controls.yearGroupName.setValue(this.yearGroupName);
      this.searchMember.controls.classRoom.setValue(this.classRoomSnameTh);
      this.userName = localStorage.getItem('userName')||'';
      this.onSearch(0);
  }

  onSearch(e){
      this.service.httpGet('/api/v1/_trnClassRoomMember/member',this.searchMember.value).then((res:IResponse)=>{
          this.dataSource.data = res.responseData||[];
      })
  }

  selectedAll(result){
    this.dataSource.data.map(o => {
      o['score1'] = result;
      o['score2'] = result;
      o['score3'] = result;
      o['score4'] = result;
      o['score5'] = result;
      return o;
    });
  }
 
  selectedHeader(){
    this.headerAll = undefined;
  }

  onSave(){
      const dataList = this.dataSource.data.map((row: classRatw) => {
        return {
            score1: row.score1
          , score2: row.score2
          , score3: row.score3
          , score4: row.score4
          , score5: row.score5
        }
      });

      Swal.fire({
        title: this.translate.instant('psms.DL0001'),
        icon : 'info',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: this.translate.instant('psms.DL0008'),
        cancelButtonText: this.translate.instant('psms.DL0009')
      }).then((result) => {
        if(result.dismiss == 'cancel'){
            return;
        }
        let json = this.dataSource.data;
        this.service.httpPut('/api/v1/_trnClassRoomRatwScore/'+this.userName,json).then((res:IResponse)=>{
          if((res.responseCode||500)!=200){
            Swal.fire(
              this.translate.instant('message.save_error'),
              res.responseMsg,
              'error' 
            )
            return;
          }
          Swal.fire( 
              this.translate.instant('message.save_header'), 
              this.translate.instant('message.save'),
              'success'
            ).then(() => {this.onBackPage();})
        });
      });
  }

  onBackPage(){
    this.router.navigate(['/aca/aca004-sub03']);
  }

}
  