import { Component, OnInit, ViewChild } from '@angular/core';
import { MatTableDataSource, MatPaginator, MatDialog } from '@angular/material';
import { FormBuilder } from '@angular/forms';
import { ApiService, IResponse } from 'src/app/shared/service/api.service';
import { Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-aca004-sub03',
  templateUrl: './aca004-sub03.component.html',
  styleUrls: ['./aca004-sub03.component.scss']
})
export class Aca004Sub03Component implements OnInit {

  searchForm = this.fb.group({
    'classId':['']
    , 'classRoomId':[''] 
    , 'yearGroupId':['']
    , 'p':['']
    , 'result':[10] 
  }); 

  citizenId:any;
  classList:[]; 
  classroomList:[]; 
  yearGroupList:[];
  
  pageSize:number;
  displayedColumns: string[] = ['no.','yearGroupName', 'departmentName', 'subjectCode', 'subjectName', 'classRoomLnameTh', 'status'];
  dataSource = new MatTableDataSource();
  
  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  constructor(private fb: FormBuilder,private service:ApiService,public dialog: MatDialog, private translate: TranslateService, private router: Router) { }

  ngOnInit() {
    this.citizenId = localStorage.getItem('citizenId') || '';
    console.log(this.citizenId );
    this.ddlYearGroup();
    this.ddlClass();
    this.ddlClassRoom();  
  }

  onSearch(e){
    this.searchForm.value.p = (this.paginator.pageIndex = e.pageIndex||0)+1;
    this.service.httpGet('/api/v1/_trnClassRoomSubject',this.searchForm.value).then((res:IResponse)=>{
      this.dataSource.data = res.responseData||[];
      this.pageSize = res.responseSize||0;
    });
  }

  ddlYearGroup(){ 
    this.service.httpGet('/api/v1/0/year-group/ddl',null).then((res)=>{
      this.yearGroupList = res||[];
      this.searchForm.controls.yearGroupId.setValue(res[0]['yearGroupId']);
    }); 
  }

  ddlClass(){
    this.service.httpGet('/api/v1/0/class/ddlbycitizenid',{'citizenId': this.citizenId}).then((res)=>{
      this.classList= res||[];
    }); 
  } 

  ddlClassRoom(){
    let json = {'citizenId': this.citizenId,'classId':this.searchForm.value.classId};
    this.service.httpGet('/api/v1/0/classroom-list/ddlfiltercitizenid',json).then((res)=>{
      this.classroomList = res||[];
    }); 
  }

  onClear(){
    this.searchForm.controls.classId.setValue('');
    this.searchForm.controls.classRoomId.setValue('');
    this.onSearch(0);
  }

  onFormScore(classRoomSubjectId, subjectCode, subjectNameTh, yearGroupName, classRoomSnameTh){ 
    this.router.navigate(['/aca/aca004-sub03-score'],{ queryParams: 
      { classRoomSubjectId: classRoomSubjectId, 
        subjectCode: subjectCode,
        subjectNameTh: subjectNameTh, 
        yearGroupName: yearGroupName,
        classRoomSnameTh: classRoomSnameTh,
      } 
    });
  }

}
