import { Component, OnInit, ViewChild, Inject } from '@angular/core';
import { FormBuilder, Validators, FormGroup, FormArray } from '@angular/forms';
import { MatPaginator, MatTableDataSource, MAT_DIALOG_DATA, MatDialog } from '@angular/material';
import { ApiService,IResponse,EHttp } from '../../../shared/service/api.service';
import { ActivatedRoute,Router } from '@angular/router';
import { UtilService } from 'src/app/_util/util.service';
import Swal from 'sweetalert2/dist/sweetalert2.js';
import 'sweetalert2/src/sweetalert2.scss';
import { TouchSequence } from 'selenium-webdriver';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-aca006',
  templateUrl: './aca006.component.html',
  styleUrls: ['./aca006.component.scss']
})
export class Aca006Component implements OnInit {

  isProcess:boolean = false;
  searchForm = this.fb.group({
    'yearGroupId':['']
    ,'classId':['']
    ,'classRoomId':['']
  }); 

  form = this.fb.group({
    'yearGroupId':['']
    ,'classId':['']
    ,'classRoomId':['']
    ,'classRoomMemberId':['']
  });

  pageSize:number;
  displayedColumns: string[] = ['classRoomNo', 'stuCode', 'fullName', 'status'];
  dataSource = new MatTableDataSource();

  yearGroupList:[];
  classList: [];
  classRoomList: [];
 
  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  constructor(
    private translate: TranslateService,
    private ar:ActivatedRoute,
    private fb: FormBuilder,
    private service:ApiService,
    public dialog: MatDialog, 
    private router: Router,
    public utilService: UtilService
  ) { 
    
   }

  ngOnInit() {
    this.searchForm.controls.yearGroupId.setValue(this.ar.snapshot.queryParamMap.get('yearGroupId')||'')
    this.searchForm.controls.classId.setValue(this.ar.snapshot.queryParamMap.get('classId')||'')
    this.searchForm.controls.classRoomId.setValue(this.ar.snapshot.queryParamMap.get('classRoomId')||'')
    this.ddlYearGroup();
    this.ddlClass();
    this.ddlClassRoom();
    this.onSearch(0);
  }

  /* DROP DOWN ปีการศึกษา */
  ddlYearGroup() {
    this.service.httpGet('/api/v1/0/year-group/ddl', null).then((res) => {
      this.yearGroupList = res || [];
      let ele = this.yearGroupList.filter((o) => {
        return o['iscurrent'] == "true";
      });
      this.searchForm.controls.yearGroupId.setValue(ele[0]['yearGroupId'])
    });
  }
  /* DROP DOWN ชั้นเรียน */
  ddlClass() {
    this.service.httpGet('/api/v1/0/tbMstClass/ddl', null).then((res) => {
      this.classList = res || [];
    });
  }
  /* DROP DOWN ห้องเรียน */
  ddlClassRoom() {
    let jsonClassRoom = {
      'classId': this.searchForm.value.classId
    };
    this.service.httpGet('/api/v1/0/classroom-list/ddlFilter2', jsonClassRoom).then((res) => {
      this.classRoomList = res || [];
    });
  }

  onSearch(e){
    this.service.httpGet('/api/v1/classRoom-member/searchAca006',this.searchForm.value).then((res:IResponse)=>{
      this.dataSource.data = res.responseData||[];
    });
  }

  onForm(classRoomMemberId,catClassId){
    this.form.controls.yearGroupId.setValue(this.searchForm.value.yearGroupId);
    this.form.controls.classId.setValue(this.searchForm.value.classId);
    this.form.controls.classRoomId.setValue(this.searchForm.value.classRoomId);
    this.form.controls.classRoomMemberId.setValue(classRoomMemberId);
    if(catClassId == 1){
      this.router.navigate(['/aca/aca006/form2'],{ queryParams : this.form.value});
    }
    else{
    this.router.navigate(['/aca/aca006/form'],{ queryParams : this.form.value});
    }
  };

  onCancel(){
    this.searchForm.controls.yearGroupId.setValue('');
    this.searchForm.controls.classId.setValue('');
    this.searchForm.controls.classRoomId.setValue('');
    this.onSearch(0);
  }
}
