import { Component, OnInit, ViewChild, Inject } from '@angular/core';
import { FormBuilder, Validators, FormGroup, FormArray } from '@angular/forms';
import { MatPaginator, MatTableDataSource, MAT_DIALOG_DATA, MatDialog } from '@angular/material';
import { ApiService,IResponse,EHttp } from '../../../../shared/service/api.service';
import { ActivatedRoute,Router } from '@angular/router';
import { UtilService } from 'src/app/_util/util.service';
import Swal from 'sweetalert2/dist/sweetalert2.js';
import 'sweetalert2/src/sweetalert2.scss';
import { TouchSequence } from 'selenium-webdriver';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-aca006-form',
  templateUrl: './aca006-form.component.html',
  styleUrls: ['./aca006-form.component.scss']
})
export class Aca006FormComponent implements OnInit {

  isProcess:boolean = false;
  form = this.fb.group({
    'classRoomMemberId':['']
    ,'p':['']
    ,'result':[50]
  });

  pageSize:number;
  displayedColumns: string[] = ['subjectName', 'score1', 'score2', 'scoreMid', 'scoreFinal', 'fullScore','grade'];
  dataSource = new MatTableDataSource();

  stuCode = "";
  fullname = "";
  year = "";
  classRoom = "";
  yearGroupId:any;
  classId:any;
  classRoomId:any;

  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  constructor(
    private translate: TranslateService,
    ar:ActivatedRoute,
    private fb: FormBuilder,
    private service:ApiService,
    public dialog: MatDialog, 
    private router: Router,
    public utilService: UtilService
  )  { 
    this.form.controls.classRoomMemberId.setValue(ar.snapshot.queryParamMap.get('classRoomMemberId'));
    this.yearGroupId = ar.snapshot.queryParamMap.get('yearGroupId');
    this.classId = ar.snapshot.queryParamMap.get('classId');
    this.classRoomId = ar.snapshot.queryParamMap.get('classRoomId');
  }

  ngOnInit() {
    this.onSearch(0);
  }

  onSearch(e){
    this.service.httpGet('/api/v1/classRoom-subjectScore/searchAca006Form',this.form.value).then((res:IResponse)=>{
      this.dataSource.data = res.responseData||[];
      this.stuCode = res.responseData[0].stu_code;
      this.fullname = res.responseData[0].fullname;
      this.year = res.responseData[0].year_group_name;
      this.classRoom = res.responseData[0].class_room;
    });
  }

  onBackPage(){
    this.router.navigate(['/aca/aca006'],{queryParams: 
      { yearGroupId:this.yearGroupId
      ,classId:this.classId
      ,classRoomId:this.classRoomId}
    });  
  }
}
