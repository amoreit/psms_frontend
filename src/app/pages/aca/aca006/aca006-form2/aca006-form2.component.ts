import { Component, OnInit, ViewChild, Inject } from '@angular/core';
import { FormBuilder, Validators, FormGroup, FormArray } from '@angular/forms';
import { MatPaginator, MatTableDataSource, MAT_DIALOG_DATA, MatDialog } from '@angular/material';
import { ApiService,IResponse,EHttp } from '../../../../shared/service/api.service';
import { ActivatedRoute,Router } from '@angular/router';
import { UtilService } from 'src/app/_util/util.service';
import Swal from 'sweetalert2/dist/sweetalert2.js';
import 'sweetalert2/src/sweetalert2.scss';
import { TranslateService } from '@ngx-translate/core';
import { TouchSequence } from 'selenium-webdriver';

@Component({
  selector: 'app-aca006-form2',
  templateUrl: './aca006-form2.component.html',
  styleUrls: ['./aca006-form2.component.scss']
})
export class Aca006Form2Component implements OnInit {

  isProcess:boolean = false;
  form = this.fb.group({
    'yearGroupId':['']
    ,'classId':['']
    ,'classRoomId':['']
    ,'classRoomMemberId':['']
    ,'p':['']
    ,'result':[50]
  });

  displayedColumns: string[] = ['subjectName', 'scoreFinal'];
  dataSource = new MatTableDataSource();

  stuCode = "";
  fullname = "";
  year = "";
  classRoom = "";

  constructor(
    private translate: TranslateService,
    ar:ActivatedRoute,
    private fb: FormBuilder,
    private service:ApiService,
    public dialog: MatDialog, 
    private router: Router,
    public utilService: UtilService
  ) {
    this.form.controls.classRoomMemberId.setValue(ar.snapshot.queryParamMap.get('classRoomMemberId'));
    this.form.controls.yearGroupId.setValue(ar.snapshot.queryParamMap.get('yearGroupId'));
    this.form.controls.classId.setValue(ar.snapshot.queryParamMap.get('classId'));
    this.form.controls.classRoomId.setValue(ar.snapshot.queryParamMap.get('classRoomId'));
   }

  ngOnInit() {
    this.onSearch(0);
  }

  onSearch(e){
      this.service.httpGet('/api/v1/classRoom-subjectScore/searchAca006Form',this.form.value).then((res:IResponse)=>{
      this.dataSource.data = res.responseData||[];
      this.stuCode = res.responseData[0].stu_code;
      this.fullname = res.responseData[0].fullname;
      this.year = res.responseData[0].year_group_name;
      this.classRoom = res.responseData[0].class_room;
    });
  }

  onBackPage(yearGroupId,className,classRoomId){
    this.router.navigate(['/aca/aca006']
      ,{ queryParams: 
      {yearGroupId:this.form.value.yearGroupId
      ,classId:this.form.value.classId
      ,classRoomId:this.form.value.classRoomId}});
  }
}

