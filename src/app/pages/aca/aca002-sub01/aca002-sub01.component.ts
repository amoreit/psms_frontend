import { Component, OnInit, ViewChild } from '@angular/core';
import { ApiService, IResponse } from 'src/app/shared/service/api.service';
import { FormBuilder } from '@angular/forms';
import { MatTableDataSource, MatPaginator, MatDialog } from '@angular/material';
import Swal from 'sweetalert2/dist/sweetalert2.js';
import 'sweetalert2/src/sweetalert2.scss';
import { Aca002DialogComponent } from 'src/app/dialog/aca002-dialog/aca002-dialog.component';
import { Aca002UpdateDialogComponent } from 'src/app/dialog/aca002-update-dialog/aca002-update-dialog.component';
import { Aca002Catclass1DialogComponent } from 'src/app/dialog/aca002-catclass1-dialog/aca002-catclass1-dialog.component';
import { Aca002Catclass1UpdateDialogComponent } from 'src/app/dialog/aca002-catclass1-update-dialog/aca002-catclass1-update-dialog.component';
import { UtilService } from 'src/app/_util/util.service';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-aca002-sub01', 
  templateUrl: './aca002-sub01.component.html',
  styleUrls: ['./aca002-sub01.component.scss']
})
export class Aca002Sub01Component implements OnInit {

  searchForm = this.fb.group({
    'classId': ['']
    , 'yearGroupId': ['']
    , 'departmentId': ['']
  });
 
  yearGroupList: [];
  classList: [];
  dataDepartment = [];
  year_group_name: any;
  class_name: any;
  show_year_group: any; 
  show_class: any
  lang_code: any
  _catClassId: any;

  dataSource = new MatTableDataSource();
  displayedColumns: string[] = ['subjectCode', 'subjectNameTh', 'credit', 'lesson', 'manage'];
  displayedColumnsKid: string[] = ['subjectCode', 'manage'];

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  constructor(private fb: FormBuilder, private service: ApiService, public dialog: MatDialog, private translate: TranslateService, public _util: UtilService) { }

  ngOnInit() {
    this.ddlClass();
    this.ddlYearGroup();
  }

  onSearch(e) {
    this.service.httpGet('/api/v1/trn-classroom-member/departmentfromclass', this.searchForm.value).then((res: IResponse) => {
      this.dataDepartment = res.responseData || [];
      for (let i = 0; i <= this.dataDepartment.length - 1; i++) {
        this._catClassId = this.dataDepartment[i]['cat_class_id']
        let json = {
          'classId': this.searchForm.value.classId, 'yearGroupId': this.searchForm.value.yearGroupId,
          'departmentId': this.dataDepartment[i]['department_id']
        }
        this.service.httpGet('/api/v1/trn-classroom-member/subjectfromclass', json).then((res: IResponse) => {
          this.dataSource.data[i] = res.responseData || [];
        });
      }
    });

    this.show_year_group = this.year_group_name;
    this.show_class = this.class_name;

  }

  ddlYearGroup() {
    this.service.httpGet('/api/v1/0/year-group/ddl', null).then((res) => {
      this.yearGroupList = res || [];
      let ele = this.yearGroupList.filter((o) => {
        return o['iscurrent'] == "true";
      });
      this.searchForm.controls.yearGroupId.setValue(ele[0]['yearGroupId'])
      this.onCheckYearGroup();
    });
  }

  ddlClass() {
    this.service.httpGet('/api/v1/0/class/ddl', null).then((res) => {
      this.classList = res || [];
    });
  }

  onFormUpdateCatClass1Dialog(e): void {
    const dialogRef = this.dialog.open(Aca002Catclass1UpdateDialogComponent, {
      width: '60%'
      , 'autoFocus': true
      , 'disableClose': true
      , data: { 'classId': e.class_id, 'yearGroupId': e.year_group_id, 'subjectCode': e.subject_code, 'departmentId': e.department_id }
    });
    dialogRef.afterClosed().subscribe(result => {
      if ((result || '') == '') {
        this.onSearch(0)
      }
    });
  }

  onFormUpdateDialog(e): void {
    const dialogRef = this.dialog.open(Aca002UpdateDialogComponent, {
      width: '60%'
      , 'autoFocus': true
      , 'disableClose': true
      , data: { 'classId': e.class_id, 'yearGroupId': e.year_group_id, 'subjectCode': e.subject_code, 'departmentId': e.department_id }
    });
    dialogRef.afterClosed().subscribe(result => {
      if ((result || '') == '') {
        this.onSearch(0)
      }
    });
  }

  onDelete(e) {
    Swal.fire({
      title: this.translate.instant('alert.delete'),//ต้องการลบข้อมูล ใช่หรือไม่
      icon: 'info',
      showCancelButton: true,
      confirmButtonColor: '#2085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: this.translate.instant('psms.DL0008'),
      cancelButtonText: this.translate.instant('psms.DL0009')
    }).then((result) => {
      if (result.dismiss == 'cancel') {
        return;
      }
      this.service.httpDelete('/api/v1/trn-class-room-subject', { 'yearGroupId': e.year_group_id, 'classId': e.class_id, 'subjectCode': e.subject_code }).then((res: IResponse) => {
        if ((res.responseCode || 500) != 200) {
          Swal.fire(
            this.translate.instant('message.delete_error'),
            res.responseMsg,
            'error'
          )
          return;
        }
        Swal.fire(
          this.translate.instant('alert.delete_header'),
          this.translate.instant('message.delete'),
          'success'
        ).then(() => {
          this.onSearch(0);
        })
      });
    });
  }

  onDialogAddSubjectKid(classId, departmentId, yearGroupId, langCode): void {
    const dialogRef = this.dialog.open(Aca002Catclass1DialogComponent, {
      width: '60%'
      , 'autoFocus': true
      , 'disableClose': true
      , data: { 'classId': this.searchForm.value.classId || '', 'departmentId': departmentId, 'yearGroupId': this.searchForm.value.yearGroupId }
    });
    dialogRef.afterClosed().subscribe(result => {
      if ((result || '') == '') {
        this.onSearch(0)
      }
    });
  }

  onDialogAddSubject(classId, departmentId, yearGroupId, langCode): void {
    const dialogRef = this.dialog.open(Aca002DialogComponent, {
      width: '60%'
      , 'autoFocus': true
      , 'disableClose': true
      , data: { 'classId': this.searchForm.value.classId || '', 'departmentId': departmentId, 'yearGroupId': this.searchForm.value.yearGroupId }
    });
    dialogRef.afterClosed().subscribe(result => {
      if ((result || '') == '') {
        this.onSearch(0)
      }

    });
  }
 
  onCheckYearGroup() {
    let ele = this.yearGroupList.filter((o) => {
      return o['yearGroupId'] == this.searchForm.value.yearGroupId;
    });
    this.year_group_name = ele[0]['name']
  }

  onCheckClass() {
    let ele = this.classList.filter((o) => {
      return o['classId'] == this.searchForm.value.classId;
    });
    this.class_name = ele[0]['classSnameTh']
    this.lang_code = ele[0]['langCode']
  }

}
