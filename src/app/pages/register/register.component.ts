import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, Validators} from '@angular/forms';
import { ApiService,IResponse,EHttp } from '../../shared/service/api.service';
import Swal from 'sweetalert2/dist/sweetalert2.js';
import 'sweetalert2/src/sweetalert2.scss';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html', 
  styleUrls: ['./register.component.css']
})
export class RegisterComponent {
  public form = this.fb.group({
    'citizenId': ['',Validators.required],
    'title': ['',Validators.required],
    'firstName': ['',Validators.required],
    'lastName': ['',Validators.required],
    'email': ['', Validators.required],
     'usrName':['', Validators.required],
    'password': ['', Validators.required],
    'phoneNo': ['', Validators.required],
    'confirmPassword': ['', Validators.required],
    'userType': ['', Validators.required],
    'scCode': ['']
  });

  citizen_id:any
  check_citizen_id: Boolean;

  constructor(public fb: FormBuilder, public router:Router,private service:ApiService){

  }

  onSubmit(){
      this.form.controls.usrName.setValue(this.form.value.email)
    if (!this.form.valid) {
      Swal.fire(
        'กรุณากรอกข้อมูลให้ครบถ้วน',
        '',
        'error'  
      )
      return;
    }
    Swal.fire({
      title: 'คุณต้องการลงทะเบียน ใช่หรือไม่',
      icon: 'info',
      showCancelButton: true,
      confirmButtonColor: '#2085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'ใช่',
      cancelButtonText: 'ไม่'
    }).then((result) => {
      if(result.dismiss == 'cancel'){
        return;
      }
    this.form.value.scCode = 'PRAMANDA';
    let data = this.form.value;
    if(data.password !== data.confirmPassword){
      Swal.fire(
        'ยืนยันรหัสผ่าน ไม่ถูกต้อง',
        '',
        'error'  
      )
      return;
    }
    this.service.register(data).then((res:IResponse)=>{
      if ((res.responseCode || 500) != 200) {
        Swal.fire(
          'ลงทะเบียนผิดพลาด',
         'อีเมล์นี้มีอยู่ในระบบแล้ว',
          'error'
        )
        return;
      }
      Swal.fire(
        'ลงทะเบียนสำเร็จ',
        '',
        'success'
      ).then(() => {
        this.onBackPage();
      })
    });
  });
  }

  onBackPage(){
    this.router.navigate(['/login']);
  }

  onCancel(){
    this.form = this.fb.group({
      'citizenId': [''],
      'title': [''],
      'firstName': [''],
      'lastName': [''],
      'email': [''],
      'usrName':[''],
      'password': [''],
      'phoneNo': [''],
      'confirmPassword': [''],
      'userType': [''],
      'scCode': ['']
    });
  }

  checkCitizenId(){ 
    if(this.citizen_id.length == 13){
      let index12 = parseFloat(this.citizen_id[12]);
      let data_index12 = index12.toString();
      let sum = (parseFloat(this.citizen_id[0])*13)+(parseFloat(this.citizen_id[1])*12)+(parseFloat(this.citizen_id[2])*11)+
                (parseFloat(this.citizen_id[3])*10)+(parseFloat(this.citizen_id[4])*9)+(parseFloat(this.citizen_id[5])*8)+
                (parseFloat(this.citizen_id[6])*7)+(parseFloat(this.citizen_id[7])*6)+(parseFloat(this.citizen_id[8])*5)+
                (parseFloat(this.citizen_id[9])*4)+(parseFloat(this.citizen_id[10])*3)+(parseFloat(this.citizen_id[11])*2);
      let modSum = sum%11; 
      let checkDigit = 11-modSum;
      let data_checkDigit = checkDigit.toString();
      if(data_checkDigit.length == 2){
        if(data_index12 == data_checkDigit[1]){
            this.check_citizen_id = true;
        }else{
            this.check_citizen_id = false;
            this.citizen_id = '';
        }
      }else{
        if(data_index12 == data_checkDigit){
          this.check_citizen_id = true;
        }else{
            this.check_citizen_id = false;
            this.citizen_id = '';
        }
      }
    }
}
}