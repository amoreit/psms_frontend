import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from '../../shared/shared.module';
import { RegisterComponent } from './register.component';

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild([{ path: '', component: RegisterComponent, pathMatch: 'full' }]),
    FormsModule, 
    ReactiveFormsModule,
    SharedModule
  ],
  declarations: [
    RegisterComponent
  ]
})
export class RegisterModule { }