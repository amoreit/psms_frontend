import { Routes } from '@angular/router';
import { AuthGuard } from '../../shared/service/auth.guard';
import { Gra001Component } from './gra001/gra001.component';
import { Gra001DialogComponent } from './gra001/gra001-dialog/gra001-dialog.component';
import { Gra002Component } from './gra002/gra002.component';
import { Gra002PopComponent } from './gra002/gra002-pop/gra002-pop.component';
import { Gra002PrintComponent } from './gra002/gra002-print/gra002-print.component';
import { Gra003Component } from './gra003/gra003.component';
import { Gra004Component } from './gra004/gra004.component';
import { Gra005Component } from './gra005/gra005.component'; 
import { Gra005ReportComponent } from './gra005/gra005-report/gra005-report.component';
import { PrimaryComponent } from './gra001/primary/primary.component';
import { JuniorComponent } from './gra001/junior/junior.component';
import { SeniorComponent } from './gra001/senior/senior.component';
import { TranscriptComponent } from './gra001/transcript/transcript.component';

/*** Page ***/

export const GraRoutes: Routes = [ 
  {
    path: '',
    children: [
      { path: 'gra001', component: Gra001Component, canActivate: [AuthGuard] },
      { path: 'gra001/gra001-pop', component: Gra001DialogComponent, canActivate: [AuthGuard] },
      { path: 'gra001/Primary', component: PrimaryComponent, canActivate: [AuthGuard] },
      { path: 'gra001/Junior', component: JuniorComponent, canActivate: [AuthGuard] },
      { path: 'gra001/Senior', component: SeniorComponent, canActivate: [AuthGuard] },
      { path: 'gra001/Transcript', component: TranscriptComponent, canActivate: [AuthGuard] },
      { path: 'gra002', component: Gra002Component, canActivate: [AuthGuard] },
	    { path: 'gra002/gra002-pop', component: Gra002PopComponent, canActivate: [AuthGuard] },
      { path: 'gra002/gra002-print', component: Gra002PrintComponent, canActivate: [AuthGuard] },
      { path: 'gra003', component: Gra003Component, canActivate: [AuthGuard] },
      { path: 'gra004', component: Gra004Component, canActivate: [AuthGuard] },
      { path: 'gra005', component: Gra005Component, canActivate: [AuthGuard] }, 
      { path: 'gra005/html', component: Gra005ReportComponent, canActivate: [AuthGuard] }
    ]
  }
];