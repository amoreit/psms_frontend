import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FlexLayoutModule } from '@angular/flex-layout';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { TranslateModule } from '@ngx-translate/core';
//import { FileUploadModule } from 'ng2-file-upload';

/*** Material ***/
import {
  MatExpansionModule,
  MatButtonModule,
  MatIconModule,
  MatInputModule,
  MatTableModule,
  MatCardModule, 
  MatFormFieldModule,
  MatPaginatorModule,
  MatDialogModule,
  MatSlideToggleModule,
  MatTabsModule
} from '@angular/material';

/*** Routing ***/
import { GraRoutes } from './gra.routing'
import { SharedModule } from 'src/app/shared/shared.module';
import { Gra001Component } from './gra001/gra001.component';
import { Gra001DialogComponent } from './gra001/gra001-dialog/gra001-dialog.component';
import { Gra001DialogENComponent } from './gra001/gra001-dialog-en/gra001-dialog-en.component';
import { Gra002Component } from './gra002/gra002.component';
import { Gra002PopComponent } from './gra002/gra002-pop/gra002-pop.component';
import { Gra002PrintComponent } from './gra002/gra002-print/gra002-print.component';
import { Gra003Component } from './gra003/gra003.component';
import { Gra004Component } from './gra004/gra004.component';
import { Gra005Component } from './gra005/gra005.component';
import { Gra005ReportComponent } from './gra005/gra005-report/gra005-report.component';
import { PrimaryComponent } from './gra001/primary/primary.component';
import { JuniorComponent } from './gra001/junior/junior.component';
import { SeniorComponent } from './gra001/senior/senior.component';
import { TranscriptComponent } from './gra001/transcript/transcript.component';

/*** Page ***/


@NgModule({
  imports: [
    CommonModule,
    FlexLayoutModule,
    TranslateModule,
    RouterModule.forChild(GraRoutes),
    MatExpansionModule,
    MatButtonModule,
    MatIconModule,
    MatInputModule,
    MatTableModule,
    MatCardModule,
    MatFormFieldModule,
    ReactiveFormsModule,
    MatPaginatorModule,
    MatDialogModule,
    MatSlideToggleModule,
    MatTabsModule,
    CommonModule,

    FormsModule,
    ReactiveFormsModule,
    SharedModule,

    //FileUploadModule
  ],
  providers: [],
  exports: [RouterModule],
  declarations: [
    Gra001Component,
    Gra002Component,
    Gra003Component,
    Gra004Component,
    Gra005Component,
    Gra005ReportComponent,
    Gra001DialogComponent,
    Gra001DialogENComponent,
	  Gra002PopComponent,
    Gra002PrintComponent,
    PrimaryComponent,
    JuniorComponent,
    SeniorComponent,
    TranscriptComponent,
  ],
  entryComponents: [
    Gra001DialogComponent,
    Gra001DialogENComponent,
	  Gra002PopComponent,
    Gra002PrintComponent,
  ]
})
export class GraModule { }
