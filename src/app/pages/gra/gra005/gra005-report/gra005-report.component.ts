import { Component, OnInit, ViewChild, Inject } from "@angular/core";
import { FormBuilder, Validators, FormGroup } from "@angular/forms";
import {ApiService,IResponse,EHttp} from "../../../../shared/service/api.service";
import { Router, ActivatedRoute } from '@angular/router';
import { UtilService } from 'src/app/_util/util.service';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-gra005-report',
  templateUrl: './gra005-report.component.html',
  styleUrls: ['./gra005-report.component.scss']
})
export class Gra005ReportComponent implements OnInit {

  form = this.fb.group({
    'yearGroupId':['']
    , 'classId':[''] 
  });

  term:any;
  year:any;
  schoolName:any; 
  subDistrict:any;
  district:any;
  province:any;
  fatherName:any;
  motherName:any;
  birthDayDate:any;
  dataList = [];
  constructor(
    private translate: TranslateService,
    private ar:ActivatedRoute,
    private fb: FormBuilder,
    private service: ApiService,
    public util: UtilService
  ) { }

  ngOnInit() {
    this.form.value.yearGroupId = this.ar.snapshot.queryParamMap.get('yearGroupId')||'';
    this.form.value.classId = this.ar.snapshot.queryParamMap.get('classId')||'';
    this.loadData();
  }

  loadData(){
    this.service.httpGet("/api/v1/stu-profile/gra005", this.form.value)
    .then((res: IResponse) => {
      this.dataList = res.responseData;
      let yearGroupName = res.responseData[0].year_group_name;
      this.year = yearGroupName.substring(2);
      let term = res.responseData[0].year_group_name;
      this.term = term.substring(0,1);
      this.schoolName = res.responseData[0].sc_name;
      this.subDistrict = res.responseData[0].sub_district;
      this.district = res.responseData[0].district;
      this.province = res.responseData[0].province;
    });
  }
}

