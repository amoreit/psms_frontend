import { Component, OnInit, ViewChild, Inject } from "@angular/core";
import { FormBuilder, Validators, FormGroup } from "@angular/forms";
import {MatPaginator,MatTableDataSource,MAT_DIALOG_DATA,MatDialog} from "@angular/material";
import {ApiService,IResponse,EHttp} from "../../../shared/service/api.service";
import { Router } from "@angular/router";
import Swal from "sweetalert2/dist/sweetalert2.js";
import "sweetalert2/src/sweetalert2.scss";
import { Gra005DialogComponent } from 'src/app/dialog/gra005-dialog/gra005-dialog.component';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-gra005',
  templateUrl: './gra005.component.html',
  styleUrls: ['./gra005.component.scss']
})
export class Gra005Component implements OnInit {

  isProcess: boolean = false;
  searchForm = this.fb.group({
    'yearGroupId':['']
    , 'classId':[''] 
  });
  report = this.fb.group({
    'stuProfileId':[''] 
  });

  yearGroupId:any;
  classId:any;
  pageSize: number;
  displayedColumns: string[] = [ "no","stuCode","fullname"];
  dataSource = new MatTableDataSource();
  yearList: [];
  classList: [];
  roomList: [];
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  constructor(
    private translate: TranslateService,
    private fb: FormBuilder,
    private service: ApiService,
    public dialog: MatDialog,
    private router: Router
  ) {}
  ngOnInit() {
    this.ddlYear();
    this.ddlClass();
    // this.onSearch(0);
  }
 //dropdown ปีการศึกษา
ddlYear(){
  this.service.httpGet('/api/v1/0/year-group/ddl',null).then((res)=>{
    this.yearList = res||[];
  });
}
//dropdown ชั้นเรียน
ddlClass(){
  this.service.httpGet('/api/v1/0/class/ddlCatclass',null).then((res)=>{
    this.classList = res||[];
  });
}

//search ค้นหา
onSearch(e) {
  // this.yearGroupId = this.searchForm.value.yearGroupId;
  // this.classId = this.searchForm.value.classId; 
  // this.router.navigate(['gra/gra005/html'],{ queryParams: {yearGroupId:this.yearGroupId,classId:this.classId}});
  this.service.httpGet("/api/v1/stu-profile/gra005", this.searchForm.value)
    .then((res: IResponse) => {
      this.dataSource.data = res.responseData || [];
    });
}
//ยกเลิก
onCancel(){
  this.searchForm.controls.yearGroupId.reset('');
  this.searchForm.controls.classId.reset('');   
  this.onSearch(0);
  }

//คลิกรีพอร์ท 1 2 3
onReport1(stuProfileId){
  this.isProcess = true;
  this.report.controls.stuProfileId.setValue(stuProfileId);
  this.service.httpPreviewPDF('/api/v1/gra005/report/rep006_1/pdf',this.report.value).then((res)=>{
    this.isProcess = false;
    window.location.href=res;
  });
};

onPrint(){
  const dialogRef = this.dialog.open(Gra005DialogComponent, {
    width: '60%'
    , 'autoFocus': true
    , 'disableClose': true
    , data: { 'classId': this.searchForm.value.classId, 'yearGroupId': this.searchForm.value.yearGroupId}   
  });
  dialogRef.afterClosed().subscribe(result => {
    if ((result || '') == '') {
      this.onSearch(0)
    }
  });
}

}
