import { MatDialogModule, MatDialog } from '@angular/material/dialog';
import { Component, OnInit, ViewChild, Inject } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { MatPaginator, MatTableDataSource } from '@angular/material';;
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { Router, ActivatedRoute } from '@angular/router';
import Swal from 'sweetalert2/dist/sweetalert2.js';
import { UtilService } from 'src/app/_util/util.service';
import { IResponse, ApiService } from 'src/app/shared/service/api.service';
import { TranslateService } from '@ngx-translate/core';
@Component({
  selector: 'app-gra002-pop',
  templateUrl: './gra002-pop.component.html',
  styleUrls: ['./gra002-pop.component.scss']
})
export class Gra002PopComponent implements OnInit {

  paramFrom = this.fb.group({
    'classRoomMemberId': ['']
    , 'yearGroupId': ['']
    , 'yearGroupName': ['']
    , 'doctypeId': ['']
    , 'setNo': ['']
    , 'scCode': ['']
    , 'dob': ['']
    , 'fullname': ['']
    , 'classId': ['']
    , 'updatedDate': ['']
    , 'transcriptDate': ['']
    , 'className': ['']
    , 'stuProfileId': ['']
    , 'createdUser': ['']
    , 'p': ['']
    , 'result': ['']
    , 'stuTranscriptLogId': ['']
  });
  isProcess: boolean = false;
  id = '';
  fullname: any;
  setNo: any;
  updatedDate: any;
  transcriptDate: any;
  class_room = '';
  stu_code = '';
  stu: any;
  stu_transcript_log_id = '';
  stu_profile_id= '';
  dob = '';

  date = new Date();
  today = Date.now()

  constructor(
    private translate: TranslateService,
    private fb: FormBuilder,
    private service: ApiService,
    private router: Router,
    private ar: ActivatedRoute,
    public utilService: UtilService,
    @Inject(MAT_DIALOG_DATA) data: any,
    public dialogRef: MatDialogRef<Gra002PopComponent>,
  ) {


  }

  ngOnInit() {
    this.id = this.ar.snapshot.queryParamMap.get('id') || '';
    this.stu_transcript_log_id = this.ar.snapshot.queryParamMap.get('trid') || '';
    this.setNo = this.ar.snapshot.queryParamMap.get('setNo') || '';
    this.transcriptDate = this.ar.snapshot.queryParamMap.get('date') || '';
    this.dob = this.ar.snapshot.queryParamMap.get("dobb") || "";
    this.stu_profile_id = this.ar.snapshot.queryParamMap.get("stu") || "";
    // this.paramFrom.controls.transcript_date.setValue(this.ar.snapshot.queryParamMap.get('date') || '');
    this.paramFrom.controls.transcriptDate.setValue(this.transcriptDate);
    this.paramFrom.controls.setNo.setValue(this.setNo);
    this.loadData();
  }

  onBack() {
    this.router.navigate(['/gra/gra002']);
  }


  onCancle() {
    this.paramFrom.controls.setNo.reset('');
    this.ngOnInit();
  }

  loadData() {
    this.isProcess = true;
    this.service.httpGet('/api/v1/classRoom-member/' + this.id, null).then((res: IResponse) => {
      this.isProcess = false;
      let data = res.responseData;
      this.paramFrom.controls.yearGroupId.setValue(data.yearGroupId || '');
      this.paramFrom.controls.classRoomMemberId.setValue(data.classRoomMemberId || '');
      this.paramFrom.controls.scCode.setValue(data.scCode || '');
      this.paramFrom.controls.fullname.setValue(data.fullname || '');
      this.paramFrom.controls.classId.setValue(data.classId || '');
      this.paramFrom.controls.className.setValue(data.className || '');
      this.paramFrom.controls.createdUser.setValue(data.createdUser || '');
      this.paramFrom.controls.yearGroupName.setValue(data.yearGroupName || '');
      // this.paramFrom.controls.stuTranscriptLogId.setValue(data.stuTranscriptLogId || '');
      this.stu_code = data.stuCode;
      this.fullname = data.fullname;
      this.class_room = data.classRoom;

    });
  }


  onSave() {
    this.paramFrom.controls.stuTranscriptLogId.setValue(this.stu_transcript_log_id);
    this.paramFrom.controls.stuProfileId.setValue( this.stu_profile_id);
    this.paramFrom.controls.dob.setValue( this.dob);
    this.paramFrom.controls.doctypeId.setValue("2")
    if (this.paramFrom.value.setNo == '' && this.paramFrom.value.transcriptDate == '') {
      Swal.fire(
        this.translate.instant('message.add_data'),//เพิ่มข้อมูลไม่ครบ
        this.translate.instant('message.check_dataAll'), //กรุณาระบุข้อมูล วันที่,เลขที่ 
        "error");
      return;
    } else if (this.paramFrom.value.setNo == '') {
      Swal.fire(
        this.translate.instant('message.add_data'),//เพิ่มข้อมูลไม่ครบ
        this.translate.instant('message.check_dataNo'), //กรุณาระบุข้อมูล เลขที่
        "error");
      return;
    } else if (this.paramFrom.value.transcriptDate == '') {
      Swal.fire(
        this.translate.instant('message.add_data'),//เพิ่มข้อมูลไม่ครบ
        this.translate.instant('message.check_dataDate') , //กรุณาระบุข้อมูล วันที่ 
        "error");
      return;
    }
    Swal.fire({
      title: this.translate.instant('alert.save'), //ต้องการบันทึกข้อมูล ใช่หรือไม่
      icon: 'info',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: this.translate.instant('psms.DL0008'),//ใช่
      cancelButtonText: this.translate.instant('psms.DL0009')//ไม่
    }).then((result) => {

      if (result.dismiss == 'cancel') {
        return;
      }
      this.isProcess = true;
      let json = this.paramFrom.value;
      if (json.stuTranscriptLogId == '') {
        this.service.httpPost('/api/v1/TranscriptLog', json).then((res: IResponse) => {
          this.isProcess = false;
          if ((res.responseCode || 500) != 200) {
            Swal.fire(
              this.translate.instant('message.add_error'),//เพิ่มข้อมูลผิดพลาด
              res.responseMsg,
              'error'
            )
            return;
          }
          Swal.fire(
            this.translate.instant('alert.add_header'),//เพิ่มข้อมูล
            this.translate.instant('message.add'),//เพิ่มข้อมูลสำเร็จ
            'success'
          ).then(() => { this.close(); })
        });

      } else {

        this.service.httpPut('/api/v1/TranscriptLog/update', json).then((res: IResponse) => {
          this.isProcess = true;
          if ((res.responseCode || 500) != 200) {
            alert(res.responseMsg);
            return;
          }
          Swal.fire(
            this.translate.instant('psms.DL0006'),//แก้ไขข้อมูล
          this.translate.instant('psms.DL0007'),//แก้ไขข้อมูลสำเร็จ
            'success'
          ).then(() => { this.close(); })

        });
      }
    });

  }

  close() {
    this.dialogRef.close();
  }

  OnClear() {
    if (this.id == '') {
      this.paramFrom.controls.setNo.setValue('');
      this.paramFrom.controls.lnameTh.setValue('');
    } else {
      this.loadData();
    }
  }

  formatDateSQLToDate(sqlDate: string) {
    return new Date(sqlDate);
  }

  formatDateForSQL(date: Date) {
    return date.getFullYear() + "-" + this.twoDigit(date.getMonth() + 1) + "-" + this.twoDigit(date.getDate());
  }

  dateyearplus(dateStr: string, formatBC: boolean, splitter: string = '/', num: number) {
    if (dateStr != null && dateStr != '') {
      dateStr = dateStr.substring(0, 10);
      if (dateStr.length == 10) {
        var arr = dateStr.split('-');
        if (arr.length == 3 && arr[0].length == 4 && arr[1].length == 2 && arr[2].length == 2) {
          var year = parseInt(arr[0]);
          if (!isNaN(parseInt(arr[2])) && !isNaN(parseInt(arr[1])) && !isNaN(year)) {
            if (formatBC) {
              // if (year < 2362) {
              //   year += 543;
              // }
            } else {
              if (year > 2219) {
                year -= 543;
              }
            }
            year += 5;
            return year + splitter + this.twoDigit(arr[1]) + splitter + this.twoDigit(arr[2]);
          }
        }
      }
    }
    return '';
  }

  twoDigit(digit: any) {
    digit = parseInt(digit);
    return digit < 10 ? '0' + digit : new String(digit);
  }

  formCheck = this.fb.group({
    yearGroupId:["",Validators.required],
    doctypeId:["2"],
    setNo:["",Validators.required],
  });

  formCheckUpdate = this.fb.group({
    yearGroupId:["",Validators.required],
    doctypeId:["2"],
    setNo:["",Validators.required],
    stuTranscriptLogId:[""]
  });


  onCheck(){
    this.formCheck.controls.yearGroupId.setValue( this.paramFrom.value.yearGroupId);
    this.formCheck.controls.doctypeId.setValue(2);
    this.formCheck.controls.setNo.setValue(this.paramFrom.value.setNo);
    this.formCheckUpdate.controls.yearGroupId.setValue( this.paramFrom.value.yearGroupId);
    this.formCheckUpdate.controls.doctypeId.setValue(2);
    this.formCheckUpdate.controls.setNo.setValue(this.paramFrom.value.setNo);
    this.formCheckUpdate.controls.stuTranscriptLogId.setValue( this.stu_transcript_log_id);
    if(this.stu_transcript_log_id == '' || this.stu_transcript_log_id == null){
      let json = this.formCheck.value;
      this.service.httpGet("/api/v1/TranscriptLog/searchCheck",json).then(res => {
        this.isProcess = false;
        let dataCheck = res.responseData;
        if(dataCheck.length>=1){
          Swal.fire(
            'ข้อมูลผิดพลาด',//ข้อมูลผิดพลาด
            'ข้อมูล เลขที่ ใบปพ.2 '//ข้อมูล เลขที่ ใบปพ.2 
            +this.formCheckUpdate.value.setNo+ 
            this.translate.instant('psms.DT0015')//ปีการศึกษา
            +this.paramFrom.value.yearGroupName+
            ' นี้มีอยู่ในระบบแล้ว', //นี้มีอยู่ในระบบแล้ว
            'error');
          return this.paramFrom.controls.setNo.setValue("");
        }
      });
    }else{
      let json = this.formCheckUpdate.value;
      this.service.httpGet("/api/v1/TranscriptLog/UpdateCheck",json).then(res => {
        this.isProcess = false;
        let dataCheck = res.responseData;
        if(dataCheck.length >=1){
          Swal.fire(
            'ข้อมูลผิดพลาด',//ข้อมูลผิดพลาด
            'ข้อมูล เลขที่ ใบปพ.2 '//ข้อมูล เลขที่ ใบปพ.2
            +this.formCheckUpdate.value.setNo+ 
            this.translate.instant('psms.DT0015')//ปีการศึกษา
            +this.paramFrom.value.yearGroupName+
            ' นี้มีอยู่ในระบบแล้ว', //นี้มีอยู่ในระบบแล้ว
            'error');
          return this.paramFrom.controls.setNo.setValue(this.setNo);
        }
      });
    }
}


}

