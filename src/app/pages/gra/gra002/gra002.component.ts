import { MatDialogModule, MatDialog } from '@angular/material/dialog';
import { Component, OnInit, ViewChild, Inject } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { MatPaginator, MatTableDataSource } from '@angular/material';
import { ApiService, IResponse } from '../../../shared/service/api.service';
import { Router } from '@angular/router';
import { UtilService } from 'src/app/_util/util.service';
import { Gra002PopComponent } from './gra002-pop/gra002-pop.component';
import { Gra002PrintComponent } from './gra002-print/gra002-print.component';
import { ThrowStmt } from '@angular/compiler';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-gra002',
  templateUrl: './gra002.component.html',
  styleUrls: ['./gra002.component.scss']
})
export class Gra002Component implements OnInit {

  isProcess: boolean = false;
  searchForm = this.fb.group({
    'yearGroupId': ['']
    , 'classId': [''],
    'classRoomId': ['']
    , 'p': ['']
    , 'result': ['100']
    , 'stuCode': ['']
    , 'setNo': ['']
  });


  pageSize: number;
  displayedColumns: string[] = ['no', 'stuCode', 'fullname', 'classRoom', 'setNo', 'Status',];
  dataSource = new MatTableDataSource();
  yearList: [];
  classList: [];
  classRoomList: [];
  classRoomMemberId: [];
  setNo1: [];

  chooseTerm: string;
  checkLangCode: string;
  subyear: string;
  sub: string;
  language: string;
  fullname: string;
  setNo: string;

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  constructor(
    private translate: TranslateService,
    private fb: FormBuilder,
    private service: ApiService,
    private router: Router,
    public dialog: MatDialog,
  ) { }

  ngOnInit() {
    this.onSearch(0);
    this.searchForm.value.chooseLang = '1';
    this.chooseTerm = '1';
    this.ddlClass();
    this.ddlClassRoom();
    this.ddlYear();
  }

  onSearch(e) {
    this.searchForm.value.p = (this.paginator.pageIndex = e.pageIndex || 0) + 1;
    this.service.httpGet('/api/v1/kornClassRoom-member/tran', this.searchForm.value).then((res: IResponse) => {
      this.dataSource.data = res.responseData || [];
      this.pageSize = res.responseSize || 0;
    });
  }

  onFrom(id, trid, setNo, date,stu,dobb) {
    const dialogRef = this.dialog.open(Gra002PopComponent, {
      width: '70%'
      , 'disableClose': true
      // , 'id': id || {}

    });
    dialogRef.afterClosed().subscribe(result => {
      if ((result || '') == '') {
        this.onSearch(0);
        return;
      }
    });

    this.router.navigate(['/gra/gra002'], { queryParams: { id: id, trid: trid, setNo: setNo, date: date,stu:stu,dobb:dobb } });
  }


  onReport1(id, sn, datep, tran) {
    const dialogRef = this.dialog.open(Gra002PrintComponent, {
      width: '70%'
      , 'autoFocus': true
      , 'disableClose': true

    });
    dialogRef.afterClosed().subscribe(result => {
    });

    this.router.navigate(['/gra/gra002'], { queryParams: { id: id, sn: sn, datep: datep, tran: tran } });
  }

  onCancel() {
    this.searchForm.controls.yearGroupId.reset('');
    this.searchForm.controls.classId.reset('');
    this.searchForm.controls.classRoomId.reset('');
    this.onSearch(0);
  }

  ddlClass() {
    this.service.httpGet('/api/v1/0/class/ddl', null).then(res => {
      this.classList = res || [];
    });
  }

  ddlClassRoom() {
    const json = { classId: this.searchForm.value.classId };
    this.service
      .httpGet('/api/v1/0/classroom-list/ddlFilter', json)
      .then(res => {
        this.classRoomList = res || [];
      });
  }

  ddlYear() {
    this.service.httpGet('/api/v1/0/year-group/ddl', null).then(res => {
      this.yearList = res || [];
      this.searchForm.controls.yearGroupId.setValue(res[0]['yearGroupId']);
    });
  }

  // onReport() {
  //   this.isProcess = true;
  //   this.service.httpPreviewPDF('/api/v1/gra002/report/pdf', this.report.value).then((res) => {
  //     this.isProcess = false;
  //     window.location.href = res;
  //   });

  // }

}

