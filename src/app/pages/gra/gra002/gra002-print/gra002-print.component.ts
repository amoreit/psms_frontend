import { MatDialogModule, MatDialog } from '@angular/material/dialog';
import { Component, OnInit, ViewChild, Inject } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { MatPaginator, MatTableDataSource } from '@angular/material';;
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { Router, ActivatedRoute } from '@angular/router';
import Swal from 'sweetalert2/dist/sweetalert2.js';
import { UtilService } from 'src/app/_util/util.service';
import { IResponse, ApiService } from 'src/app/shared/service/api.service';
import { TranslateService } from '@ngx-translate/core';

import { Pipe, PipeTransform } from '@angular/core'


@Pipe({
  name: 'formatDate'
})

@Component({
  selector: 'app-gra002-print',
  templateUrl: './gra002-print.component.html',
  styleUrls: ['./gra002-print.component.scss']
})
export class Gra002PrintComponent implements OnInit {
  paramFrom = this.fb.group({ 
    'classRoomMemberId': ['']
    , 'yearGroupId': ['']
    , 'doctypeId': ['']
    , 'scCode': ['']
    , 'setNo': ['']
    , 'transcriptDate': ['']
    , 'p': ['']
    , 'result': ['']
    , 'stuTranscriptLogId': ['']
  });
  isProcess: boolean = false;
  id = '';
  fullname: any;
  class_room = '';
  stu_code = '';
  set_no: any;
  transcriptDate: any;
  stu_transcript_log_id: any;

  report = this.fb.group({
    'stuTranscriptLogId': [''],
  });


  constructor(
    private translate: TranslateService,
    private fb: FormBuilder,
    private service: ApiService,
    private router: Router,
    private ar: ActivatedRoute,
    public utilService: UtilService,
    @Inject(MAT_DIALOG_DATA) data: any,
    public dialogRef: MatDialogRef<Gra002PrintComponent>,
  ) { }


  ngOnInit() {
    this.id = this.ar.snapshot.queryParamMap.get('id') || '';
    this.set_no = this.ar.snapshot.queryParamMap.get('sn') || '';
    this.transcriptDate = this.ar.snapshot.queryParamMap.get('datep') || '';
    this.stu_transcript_log_id = this.ar.snapshot.queryParamMap.get('tran') || '';

    this.paramFrom.controls.transcriptDate.setValue(this.transcriptDate);
    this.paramFrom.controls.stuTranscriptLogId.setValue(this.stu_transcript_log_id);
    this.loadData();
  }


  onReport(id) {
    if(id == 1){
      this.report.controls.stuTranscriptLogId.setValue(this.stu_transcript_log_id);
      this.service.httpPreviewPDF('/api/v1/gra002/report/pdf', this.report.value).then((res) => {
        window.location.href = res;
      }); 
    }else{
      this.report.controls.stuTranscriptLogId.setValue(this.stu_transcript_log_id);
      this.service.httpPreviewPDF('/api/v1/gra002/report/pdfPrint', this.report.value).then((res) => {
        window.location.href = res;
      });
    }
  }

  onBack() {
    this.router.navigate(['../../pages/gra/gra002']);
  }


  close() {
    this.dialogRef.close();
    // this.onBack();
  }

  loadData() {
    this.isProcess = true;
    this.service.httpGet('/api/v1/classRoom-member/' + this.id, null).then((res: IResponse) => {
      this.isProcess = false;
      let data = res.responseData;
      this.paramFrom.controls.setNo.setValue(data.setNo || '');
      // this.set_no = data.setNo;
      this.stu_code = data.stuCode;
      this.fullname = data.fullname;
      this.class_room = data.classRoom;

    });
  }

  toLocaleDateString() {
    var date = new Date(Date.UTC(2012, 11, 20, 3, 0, 0));

    // request a weekday along with a long date
    var options = { weekday: 'long', year: 'numeric', month: 'long', day: 'numeric' };
  }

  formatDateSQLToString(dateStr: string, formatBC: boolean, splitter: string = '/') {
    if (dateStr != null && dateStr != '') {
      dateStr = dateStr.substring(0, 10);
      if (dateStr.length == 10) {
        var arr = dateStr.split('-');
        if (arr.length == 3 && arr[0].length == 4 && arr[1].length == 2 && arr[2].length == 2) {
          var year = parseInt(arr[0]);
          if (!isNaN(parseInt(arr[2])) && !isNaN(parseInt(arr[1])) && !isNaN(year)) {
            if (formatBC) {
              if (year < 2362) {
                year += 543;
              }
            } else {
              if (year > 2219) {
                year -= 543;
              }
            }
            return this.twoDigit(arr[2]) + splitter + this.twoDigit(arr[1]) + splitter + year;
          }
        }
      }
    }

    return '';
  }

  twoDigit(digit: any) {
    digit = parseInt(digit);
    return digit < 10 ? '0' + digit : new String(digit);
  }


}
