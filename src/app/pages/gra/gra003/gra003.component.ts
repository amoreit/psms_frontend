import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ApiService, IResponse } from 'src/app/shared/service/api.service';
import { Router } from '@angular/router';
import { MatDialog, MatPaginator, MatTableDataSource } from '@angular/material';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-gra003',
  templateUrl: './gra003.component.html',
  styleUrls: ['./gra003.component.scss']
})
export class Gra003Component implements OnInit { 

  isProcess: boolean = false;
  searchForm = this.fb.group({
      'yearId': ['']
    , 'classId': ['']
    , 'p': ['']
    , 'result':[10]
  });

  yearReport = this.fb.group({
    'yearId': ['']
    ,'manTotal': ['']
    ,'womanTotal': ['']
    ,'genderTotal': ['']
  }); 

  classReport = this.fb.group({
    'classId': ['']
    ,'manTotal': ['']
    ,'womanTotal': ['']
    ,'genderTotal': ['']
  });

  yearClassReport = this.fb.group({
    'yearId': ['']
  ,'classId': ['']
    ,'manTotal': ['']
    ,'womanTotal': ['']
    ,'genderTotal': ['']
  });

  pageSize: number;
  displayedColumns: string[] = ['no', 'stuCode', 'fullname', 'classRoom', 'setNo', 'remark',];
  dataSource = new MatTableDataSource();

  yearList: [];
  classList:[];

  manTotal = 0;
  womanTotal = 0;
  genderTotal = '';

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  constructor(
    private translate: TranslateService,
    private fb: FormBuilder,
    private service: ApiService,
    private router: Router,
    public dialog: MatDialog,
  ) {}

  ngOnInit() {
    this.ddlYear();
    this.ddlClass();
  }

  onSearch(e) {
    this.searchForm.value.p = (this.paginator.pageIndex = e.pageIndex || 0) + 1;
    this.service.httpGet('/api/v1/paitranscriptlog', this.searchForm.value).then((res: IResponse) => {
      this.dataSource.data = res.responseData || [];
      this.pageSize = res.responseSize || 0;
      for(let i = 0 ; i < this.dataSource.data.length ;i++){
        if(this.dataSource.data[i]['gender'] == 'ชาย'){
          this.manTotal = this.manTotal + 1;
        }else{
          this.womanTotal = this.womanTotal + 1;
        }
      }
    });
  }

  ddlYear() {
    this.service.httpGet('/api/v1/0/year/ddl', null).then(res => {
      this.yearList = res || [];
    });
  }

  ddlClass() {
    this.service.httpGet('/api/v1/0/class/ddl', null).then(res => {
      this.classList = res || [];
    });
  }

  onCancel(){
    this.searchForm.controls.yearId.setValue("");
    this.searchForm.controls.classId.setValue("");
    this.manTotal = 0;
    this.womanTotal = 0;
    this.genderTotal = '';
    this.onSearch(0);
  }

  onReport(){
    if(this.searchForm.value.yearId != '' && this.searchForm.value.classId == ''){
      this.yearReport.controls.yearId.setValue(this.searchForm.value.yearId);
      this.yearReport.controls.manTotal.setValue(this.manTotal);
      this.yearReport.controls.womanTotal.setValue(this.womanTotal);
      this.yearReport.controls.genderTotal.setValue(this.manTotal+this.womanTotal);
      this.service.httpPreviewPDF('/api/v1/gra003/report/pdf',this.yearReport.value).then((res)=>{
        this.isProcess = false;
        window.location.href = res;
      });
    }
    if(this.searchForm.value.yearId == '' && this.searchForm.value.classId != ''){
      this.classReport.controls.classId.setValue(this.searchForm.value.classId);
      this.classReport.controls.manTotal.setValue(this.manTotal);
      this.classReport.controls.womanTotal.setValue(this.womanTotal);
      this.classReport.controls.genderTotal.setValue(this.manTotal+this.womanTotal);
      this.service.httpPreviewPDF('/api/v1/gra003/report/pdfclass',this.classReport.value).then((res)=>{
        this.isProcess = false;
        window.location.href = res;
      });
    }
    if(this.searchForm.value.yearId != '' && this.searchForm.value.classId != ''){
      this.yearClassReport.controls.yearId.setValue(this.searchForm.value.yearId);
      this.yearClassReport.controls.classId.setValue(this.searchForm.value.classId);
      this.yearClassReport.controls.manTotal.setValue(this.manTotal);
      this.yearClassReport.controls.womanTotal.setValue(this.womanTotal);
      this.yearClassReport.controls.genderTotal.setValue(this.manTotal+this.womanTotal);
      this.service.httpPreviewPDF('/api/v1/gra003/report/pdfyearclass',this.yearClassReport.value).then((res)=>{
        this.isProcess = false;
        window.location.href = res;
      });
    }
  }

  onReportPDF(){
    if(this.searchForm.value.yearId != '' && this.searchForm.value.classId == ''){
      this.yearReport.controls.yearId.setValue(this.searchForm.value.yearId);
      this.yearReport.controls.manTotal.setValue(this.manTotal);
      this.yearReport.controls.womanTotal.setValue(this.womanTotal);
      this.yearReport.controls.genderTotal.setValue(this.manTotal+this.womanTotal);
      this.service.httpReportPDF('/api/v1/gra003/report/pdf',"gra003",this.yearReport.value).then((res)=>{
        this.isProcess = false;
      });
    }
    if(this.searchForm.value.yearId == '' && this.searchForm.value.classId != ''){
      this.classReport.controls.classId.setValue(this.searchForm.value.classId);
      this.classReport.controls.manTotal.setValue(this.manTotal);
      this.classReport.controls.womanTotal.setValue(this.womanTotal);
      this.classReport.controls.genderTotal.setValue(this.manTotal+this.womanTotal);
      this.service.httpReportPDF('/api/v1/gra003/report/pdfclass',"gra003",this.classReport.value).then((res)=>{
        this.isProcess = false;
      });
    }
    if(this.searchForm.value.yearId != '' && this.searchForm.value.classId != ''){
      this.yearClassReport.controls.yearId.setValue(this.searchForm.value.yearId);
      this.yearClassReport.controls.classId.setValue(this.searchForm.value.classId);
      this.yearClassReport.controls.manTotal.setValue(this.manTotal);
      this.yearClassReport.controls.womanTotal.setValue(this.womanTotal);
      this.yearClassReport.controls.genderTotal.setValue(this.manTotal+this.womanTotal);
      this.service.httpReportPDF('/api/v1/gra003/report/pdfyearclass',"gra003",this.yearClassReport.value).then((res)=>{
        this.isProcess = false;
      });
    }
  }

  onReportXls(){
    if(this.searchForm.value.yearId != '' && this.searchForm.value.classId == ''){
      this.yearReport.controls.yearId.setValue(this.searchForm.value.yearId);
      this.yearReport.controls.manTotal.setValue(this.manTotal);
      this.yearReport.controls.womanTotal.setValue(this.womanTotal);
      this.yearReport.controls.genderTotal.setValue(this.manTotal+this.womanTotal);
      this.service.httpReportXLS('/api/v1/gra003/report/xls',"gra003",this.yearReport.value).then((res)=>{
        this.isProcess = false;
      });
    }
    if(this.searchForm.value.yearId == '' && this.searchForm.value.classId != ''){
      this.classReport.controls.classId.setValue(this.searchForm.value.classId);
      this.classReport.controls.manTotal.setValue(this.manTotal);
      this.classReport.controls.womanTotal.setValue(this.womanTotal);
      this.classReport.controls.genderTotal.setValue(this.manTotal+this.womanTotal);
      this.service.httpReportXLS('/api/v1/gra003/report/classxls',"gra003",this.classReport.value).then((res)=>{
        this.isProcess = false;
      });
    }
    if(this.searchForm.value.yearId != '' && this.searchForm.value.classId != ''){
      this.yearClassReport.controls.yearId.setValue(this.searchForm.value.yearId);
      this.yearClassReport.controls.classId.setValue(this.searchForm.value.classId);
      this.yearClassReport.controls.manTotal.setValue(this.manTotal);
      this.yearClassReport.controls.womanTotal.setValue(this.womanTotal);
      this.yearClassReport.controls.genderTotal.setValue(this.manTotal+this.womanTotal);
      this.service.httpReportXLS('/api/v1/gra003/report/yearclassxls',"gra003",this.yearClassReport.value).then((res)=>{
        this.isProcess = false;
      });
    }
  }

}
