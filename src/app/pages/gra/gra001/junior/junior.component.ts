import { Component, OnInit, ViewChild, Inject } from "@angular/core";
import { FormBuilder, Validators, FormGroup } from "@angular/forms";
import {
  MatPaginator,
  MatTableDataSource,
  MAT_DIALOG_DATA,
  MatDialog
} from "@angular/material";
import { ApiService, IResponse } from 'src/app/shared/service/api.service';
import { Router, ActivatedRoute } from '@angular/router';
import "sweetalert2/src/sweetalert2.scss";

@Component({
  selector: 'app-junior',
  templateUrl: './junior.component.html',
  styleUrls: ['./junior.component.css']
})
export class JuniorComponent implements OnInit {
  secondary1=[];secondary1_2=[];activity1=[];activity1_2=[];
  secondary2=[];secondary2_2=[];activity2=[];activity2_2=[];
  secondary3=[];secondary3_2=[];activity3=[];activity3_2=[];
  secondary4=[];secondary4_2=[];activity4=[];activity4_2=[];
  secondary5=[];secondary5_2=[];activity5=[];activity5_2=[];
  secondary6=[];secondary6_2=[];activity6=[];activity6_2=[];
  term_year1='';term_year1_2='';
  term_year2='';term_year2_2='';
  term_year3='';term_year3_2='';
  term_year4='';term_year4_2='';
  term_year5='';term_year5_2='';
  term_year6='';term_year6_2='';
  scoreAver=[];
  creditSum:number;
  gradeSum:number;
  grade70='';
  ratwResult='';
  desiredCharResult='';
  onetScore1='';
  onetScore3='';
  onetScore4='';
  onetScore5='';
  registrar='';
  Director='';
  stuTranscriptLogId='';
  tranDate='';
  date=[];
  day='';
  month='';
  year='';
  stuCode='';
  setNo='';
  seqNo='';
  citizenId='';
  firstnameTh='';
  lastnameTh='';
  gender='';
  citizenship='';
  religion='';
  dob='';
  fatherTitle='';
  fatherFirstnameTh='';
  fatherLastnameTh='';
  motherTitle='';
  motherFirstnameTh='';
  motherLastnameTh='';
 
 @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
 constructor( 
   private fb: FormBuilder,
   private service: ApiService,
   public dialog: MatDialog,
   private ar: ActivatedRoute,
   private router: Router
 ) {}

 ngOnInit() {
   this.stuCode = this.ar.snapshot.queryParamMap.get("stuCode") || "";
   this.setNo = this.ar.snapshot.queryParamMap.get("setNo") || "";
   this.seqNo = this.ar.snapshot.queryParamMap.get("seqNo") || "";
   this.stuTranscriptLogId = this.ar.snapshot.queryParamMap.get("idTs")||"";
   this.onSecondary();
   this.onStu();
   this.onActicity();
   this.onSumScore();
   this.onSumGpa();
   this.onGetOnet();
   this.onPosition();
   this.onTranDate();
 }
 onStu(){
   let stuCode = {'stuCode':this.stuCode};
 this.service.httpGet("/api/v1/stu-profile/stp", stuCode).then((res: IResponse) => {
     let data = res.responseData;
     this.stuCode = data[0].stu_code;
     this.citizenId = data[0].citizen_id;
     this.firstnameTh = data[0].firstname_th;
     this.lastnameTh = data[0].lastname_th;
     this.gender = data[0].gender;
     this.fatherTitle = data[0].father_title;
     this.fatherFirstnameTh = data[0].father_firstname_th;
     this.fatherLastnameTh = data[0].father_lastname_th;
     this.motherTitle = data[0].mother_title;
     this.motherFirstnameTh = data[0].mother_firstname_th;
     this.motherLastnameTh = data[0].mother_lastname_th;
     this.citizenship = this.onCitizenship(data[0].citizenship);
     this.religion = data[0].religion;
     this.dob = this.transform(data[0].dob,'');
     this.day = this.date[0][0];
     this.month = this.date[0][1];
     this.year = this.date[0][2];
  });
 }
 onTranDate(){
  let stuTranscriptLogId = {'stuTranscriptLogId':this.stuTranscriptLogId};
  this.service.httpGet("/api/v1/TranscriptLog/TranDate",stuTranscriptLogId).then((res:IResponse)=>{
   this.tranDate = this.transform(res.responseData[0].transcript_date,'');
  });
}
 onSecondary(){
  let stuCode = {'stuCode':this.stuCode};
   this.service.httpGet("/api/v1/classRoom-subjectScore/getSecondarySubjectScore", stuCode).then((res: IResponse) => {
     let data = res.responseData;
     for(let i = 0 ;i<data.length;i++){
       if(data[i].class_lname_th =="มัธยมศึกษาปีที่ 1"){
         if(data[i].year_group_name.split("/",1)=='1'){
         this.secondary1.push(data[i]);
         this.term_year1 = data[i].year_group_name;
        }
        else {
          this.secondary1_2.push(data[i]);
          this.term_year1_2 = data[i].year_group_name;
        }
       }
       else if(data[i].class_lname_th =="มัธยมศึกษาปีที่ 2"){
        if(data[i].year_group_name.split("/",1)=='1'){
          this.term_year2 = data[i].year_group_name;
          this.secondary2.push(data[i]);
         }
         else {
          this.term_year2_2 = data[i].year_group_name;
          this.secondary2_2.push(data[i]);
          }
       }
       else if(data[i].class_lname_th=="มัธยมศึกษาปีที่ 3"){
        if(data[i].year_group_name.split("/",1)=='1'){
          this.term_year3 = data[i].year_group_name;
          this.secondary3.push(data[i]);
         }
         else {
          this.term_year3_2 = data[i].year_group_name;
          this.secondary3_2.push(data[i]);
          }
       }
       else if(data[i].class_lname_th=="มัธยมศึกษาปีที่ 4"){
        if(data[i].year_group_name.split("/",1)=='1'){
          this.term_year4 = data[i].year_group_name;
          this.secondary4.push(data[i]);
         }
         else {
          this.term_year4_2 = data[i].year_group_name;
          this.secondary4_2.push(data[i]);
          }
       }
       else if(data[i].class_lname_th =="มัธยมศึกษาปีที่ 5"){
        if(data[i].year_group_name.split("/",1)=='1'){
          this.term_year5 = data[i].year_group_name;
          this.secondary5.push(data[i]);
         }
         else {
          this.term_year5_2 = data[i].year_group_name;
          this.secondary5_2.push(data[i]);
        }
       }
       else if(data[i].class_lname_th =="มัธยมศึกษาปีที่ 6"){
        if(data[i].year_group_name.split("/",1)=='1'){
          this.term_year6 = data[i].year_group_name;
          this.secondary6.push(data[i]);
         }
         else {
          this.term_year6 = data[i].year_group_name;
          this.secondary6_2.push(data[i]);
        }
       }
     }           
   });
 }

 onActicity(){
  let stuCode = {'stuCode':this.stuCode};
   this.service.httpGet("/api/v1/classRoom-member/getActivityScore", stuCode).then((res: IResponse) => {
     let data = res.responseData;
     for (let i = 0; i < data.length; i++) {
       if(data[i].class_lname_th == "มัธยมศึกษาปีที่ 1"&& data[i].result == true){
          if(data[i].year_group_name.split("/",1)=='1'){
          data[i].result ='ผ';
          this.activity1.push(data[i]);
          }
          else{
          data[i].result ='ผ';
          this.activity1_2.push(data[i]);
          }
         }
         else if(data[i].class_lname_th == "มัธยมศึกษาปีที่ 2"&& data[i].result == true){
          if(data[i].year_group_name.split("/",1)=='1'){
            data[i].result ='ผ';
            this.activity2.push(data[i]);
            }
            else{
            data[i].result ='ผ';
            this.activity2_2.push(data[i]);
            }
         }
         else if(data[i].class_lname_th == "มัธยมศึกษาปีที่ 3"&& data[i].result == true){
          if(data[i].year_group_name.split("/",1)=='1'){
            data[i].result ='ผ';
            this.activity3.push(data[i]);
            }
            else{
            data[i].result ='ผ';
            this.activity3_2.push(data[i]);
            }
         }
         else if(data[i].class_lname_th == "มัธยมศึกษาปีที่ 4"&& data[i].result == true){
          if(data[i].year_group_name.split("/",1)=='1'){
            data[i].result ='ผ';
            this.activity4.push(data[i]);
            }
            else{
            data[i].result ='ผ';
            this.activity4_2.push(data[i]);
            }
         }
         else if(data[i].class_lname_th == "มัธยมศึกษาปีที่ 5"&& data[i].result == true){
          if(data[i].year_group_name.split("/",1)=='1'){
            data[i].result ='ผ';
            this.activity5.push(data[i]);
            }
            else{
            data[i].result ='ผ';
            this.activity5_2.push(data[i]);
            }
         }
         else if(data[i].class_lname_th == "มัธยมศึกษาปีที่ 6"&& data[i].result == true){
          if(data[i].year_group_name.split("/",1)=='1'){
            data[i].result ='ผ';
            this.activity6.push(data[i]);
            }
            else{
            data[i].result ='ผ';
            this.activity6_2.push(data[i]);
            }
         }
     }
   });
 }

 onSumScore(){
  let stuCode = {'stuCode':this.stuCode};
   this.service.httpGet("/api/v1/classRoom-subjectScore/getSumScore", stuCode).then((res:IResponse)=>{
     this.scoreAver = res.responseData;
   })
 }

 onSumGpa(){
  let stuCode = {'stuCode':this.stuCode};
  this.service.httpGet("/api/v1/classRoom-subjectScore/getSumGpa", stuCode).then((res:IResponse)=>{
    this.gradeSum = res.responseData[0].gpa;
    this.creditSum = res.responseData[0].sum;
    this.grade70 = (this.gradeSum*0.7).toFixed(2);
  })
 }

 onGetOnet(){
  let json = {'stuCode':this.stuCode};
  this.service.httpGet("/api/v1/OnetScore/getOnetScore", json).then((res:IResponse)=>{
    let data = res.responseData;
    this.onetScore1 = data[0].subject_score1;
    this.onetScore3 = data[0].subject_score3;
    this.onetScore4 = data[0].subject_score4;
    this.onetScore5 = data[0].subject_score5;
  })
}

onGetratwResult(){
  let json = {'stuCode':this.stuCode};
  this.service.httpGet("/api/v1/classRoom-member/ratwResult",json).then((res:IResponse)=>{
  this.ratwResult = res.responseData[0].case;
  });
}

onGetdesiredCharResult(){
  let json = {'stuCode':this.stuCode};
  this.service.httpGet("/api/v1/classRoom-member/desiredCharResult",json).then((res:IResponse)=>{
  this.desiredCharResult = res.responseData[0].case;
  });
}

onPosition(){
  this.service.httpGet("/api/v1/Authori/getPosition", null).then((res:IResponse)=>{
    let data = res.responseData;
    for (let i = 0; i < data.length; i++) {
      if(data[i].position=="3"){/* data[i].position=="นายทะเบียน"&&data[i].iscurrent=="true" */
        this.registrar = data[i].fullname_th;
      }
      else if(data[i].position=="ผู้อำนวยการ"&&data[i].iscurrent=="true"){
        this.Director = data[i].fullname_th;
      }
    }
  })
}


 printComponent(cmpName) {
   let printContents = document.getElementById(cmpName).innerHTML;
   let originalContents = document.body.innerHTML;
   document.body.innerHTML = printContents;
   window.print();
   document.body.innerHTML = originalContents;
   location.reload();
 }
 twoDigit(digit: any) {
   digit = parseInt(digit);
   return digit < 10 ? '0' + digit : new String(digit);
 }

 transform(date: Date, format: string): string {
     let ThaiDay = ['อาทิตย์','จันทร์','อังคาร','พุธ','พฤหัสบดี','ศุกร์','เสาร์']
     let shortThaiMonth = [
       '01','02','03','04','05','06',
       '07','08','09','10','11','12'
         ];  
     let longThaiMonth = [
         'มกราคม','กุมภาพันธ์','มีนาคม','เมษายน','พฤษภาคม','มิถุนายน',
         'กรกฎาคม','สิงหาคม','กันยายน','ตุลาคม','พฤศจิกายน','ธันวาคม'
         ];   
   
     let inputDate=new Date(date);
     let dataDate = [
         inputDate.getDay(),inputDate.getDate(),inputDate.getMonth(),inputDate.getFullYear()
         ];
     let outputDateMedium = [
         dataDate[1],
         longThaiMonth[dataDate[2]],
         dataDate[3]+543
     ]; 
     this.date.push(outputDateMedium);
     let returnDate:string;
     returnDate = outputDateMedium.join(" ");
     return returnDate;
   }

   onCitizenship(Citizenship:any){
     let citizen;
     if (Citizenship=="Thailand") {
       citizen = "ไทย";
     }
     return citizen;
   }
}
