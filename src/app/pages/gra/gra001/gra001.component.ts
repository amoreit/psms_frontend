import { Component, OnInit, ViewChild, Inject } from "@angular/core";
import { FormBuilder, Validators, FormGroup } from "@angular/forms";
import { TranslateService } from '@ngx-translate/core';
import {
  MatPaginator,
  MatTableDataSource,
  MAT_DIALOG_DATA,
  MatDialog
} from "@angular/material";
import {
  ApiService,
  IResponse,
  EHttp
} from "../../../shared/service/api.service";
import { Router } from "@angular/router";
import "sweetalert2/src/sweetalert2.scss";
import { Gra001DialogComponent } from './gra001-dialog/gra001-dialog.component';
import { Gra001DialogENComponent } from './gra001-dialog-en/gra001-dialog-en.component';

@Component({
  selector: 'app-gra001',
  templateUrl: './gra001.component.html',
  styleUrls: ['./gra001.component.scss']
})
export class Gra001Component implements OnInit {
  isProcess: boolean = false;
  searchForm = this.fb.group({
    yearGroupId: [""],
    p: [""],
    classId:[""],
    classRoomId:[""],
    stuCode:[""],
    stuProfileId:[""],
    langCode:[""],
    result: [10]
  });
  pageSize: number;
  level='';
  displayedColumns: string[] = [
    "stuNo",
    "stuCode",
    "fullname",
    "classRoom",
    "set_no",
    "seq_no",
    "status",
    "yearStatus"
  ];
  dataSource = new MatTableDataSource();
  yearList: [];
  classList: [];
  classRoomList: [];
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  constructor(
    private translate: TranslateService,
    private fb: FormBuilder,
    private service: ApiService,
    public dialog: MatDialog,
    private router: Router
  ) {}
  ngOnInit() {
    this.searchForm.controls.langCode.setValue("1");
    this.ddlYear();
    this.ddlClass();
    this.ddlClassRoom();
    this.onSearch(0);
  }
  onForm(id,idTs,setNo,seqNo,stuid): void {
    if(this.searchForm.value.langCode=='1'){
      const dialogRef = this.dialog.open(Gra001DialogComponent, {width: '60%','disableClose':true});
      dialogRef.afterClosed().subscribe(result => {
      if((result||'') == ''){
        this.onSearch(0);
        return;
        }
      });this.router.navigate(["gra/gra001"], { queryParams: { id: id, idTs:idTs, setNo:setNo, seqNo:seqNo, stuid:stuid} });
    }
    else{
      const dialogRef = this.dialog.open(Gra001DialogENComponent, {width: '60%','disableClose':true});
      dialogRef.afterClosed().subscribe(result => {
      if((result||'') == ''){
        this.onSearch(0);
        return;
        }
      });this.router.navigate(["gra/gra001"], { queryParams: { id: id, idTs:idTs, stuid:stuid} });
    }
  }
  onFormPrint(stuCode,setNo,seqNo,level,idTs): void {
    if(level=="ประถมศึกษาปีที่ 1"||level=="ประถมศึกษาปีที่ 2"||level=="ประถมศึกษาปีที่ 3"||level=="ประถมศึกษาปีที่ 4"||level=="ประถมศึกษาปีที่ 5"||level=="ประถมศึกษาปีที่ 6"){
      this.router.navigate(["gra/gra001/Primary"], { queryParams: { stuCode:stuCode, setNo:setNo, seqNo:seqNo,idTs:idTs} });
    }
    else if(level=="มัธยมศึกษาปีที่ 1"||level=="มัธยมศึกษาปีที่ 2"||level=="มัธยมศึกษาปีที่ 3"){
      this.router.navigate(["gra/gra001/Junior"], { queryParams: { stuCode:stuCode, setNo:setNo, seqNo:seqNo,idTs:idTs} });
    }
    else{
      this.router.navigate(["gra/gra001/Senior"], { queryParams: { stuCode:stuCode, setNo:setNo, seqNo:seqNo,idTs:idTs} });
    }
  }
  onCancel(){
    this.searchForm.controls.yearGroupId.setValue('');
    this.searchForm.controls.classId.setValue('');
    this.searchForm.controls.classRoomId.setValue('');
    this.searchForm.controls.stuCode.setValue('');
    this.ngOnInit();
  }
  onSearch(e) {
    this.searchForm.value.p = (this.paginator.pageIndex = e.pageIndex || 0) + 1;
    this.service.httpGet("/api/v1/classRoom-member/rep003", this.searchForm.value).then((res: IResponse) => {
      this.dataSource.data = res.responseData || [];
      this.pageSize = res.responseSize || 0;
    });
  }
  ddlClass() {
    this.service.httpGet("/api/v1/0/class/ddl", null).then(res => {this.classList = res || [];});
  }
  ddlYear() {
    this.service.httpGet("/api/v1/0/year-group/ddl", null).then(res => {this.yearList = res || [];this.searchForm.controls.yearGroupId.setValue(res[0]['yearGroupId']);});
  }
  ddlClassRoom() {
    let json = {classId: this.searchForm.value.classId};
    this.service.httpGet("/api/v1/0/classroom-list/ddlFilter", json).then(res => {this.classRoomList = res || [];});
  }
}