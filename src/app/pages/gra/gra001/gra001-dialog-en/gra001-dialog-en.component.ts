import { Component, OnInit, Inject } from '@angular/core';
import { FormBuilder, Validators} from '@angular/forms';
import { ApiService, IResponse } from 'src/app/shared/service/api.service';
import { MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import Swal from 'sweetalert2/dist/sweetalert2.js';
import 'sweetalert2/src/sweetalert2.scss';

@Component({
  selector: 'app-gra001-dialog-en',
  templateUrl: './gra001-dialog-en.component.html',
  styleUrls: ['./gra001-dialog-en.component.css']
})
export class Gra001DialogENComponent implements OnInit {

  stu_code:any;
  fullname:any;
  fullnameEn:any;
  class_room:any;
  stu_transcript_log_id="";
  stu_profile_id ="";
  id="";

  formCriteria = this.fb.group({
    
  });
  formStuTranscript = this.fb.group({
    stuTranscriptLogId:[""],
    stu_code:[""],
    yearGroupId:[""],
    yearGroupName:[""],
    classId:[""],
    className:[""],
    fullname:[""],
    fullnameEn:[""],
    stuProfileId: [""],
    scCode:[""],
    doctypeId:["1"],
    transcriptDate:Date
  });

  constructor(private service:ApiService,
    @Inject(MAT_DIALOG_DATA) data:any,
    public dialogRef: MatDialogRef<Gra001DialogENComponent>, 
    private fb: FormBuilder,
    private ar: ActivatedRoute,
    private router: Router) { }


    
  ngOnInit() {
    this.id = this.ar.snapshot.queryParamMap.get("id") || "";
    this.stu_transcript_log_id = this.ar.snapshot.queryParamMap.get("idTs") || "";
    this.stu_profile_id = this.ar.snapshot.queryParamMap.get("stuid") || "";
    this.loadData();
  }
  loadData() {
    this.service.httpGet("/api/v1/classRoom-member/"+this.id, null).then((res: IResponse) => { 
        let data = res.responseData;
        this.formStuTranscript.controls.yearGroupId.setValue(data.yearGroupId ||'');
        this.formStuTranscript.controls.yearGroupName.setValue(data.yearGroupName ||'');
        this.formStuTranscript.controls.fullname.setValue(data.fullname ||'');
        this.formStuTranscript.controls.fullnameEn.setValue(data.fullnameEn ||'');
        this.formStuTranscript.controls.classId.setValue(data.classId ||'');
        this.formStuTranscript.controls.scCode.setValue(data.scCode ||'');
        this.formStuTranscript.controls.className.setValue(data.className ||'');
        this.stu_code = data.stuCode||'';
        this.fullname = data.fullname||'';
        this.class_room = data.classRoom||'';
        this.fullnameEn = data.fullnameEn||'';
      });
  }
  onBack() {
    this.router.navigate(["gra/gra001"]);
  }
  close() { 
    this.dialogRef.close(); 
  }
}
