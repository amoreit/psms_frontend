import { Component, OnInit, Inject } from '@angular/core';
import { FormBuilder, Validators} from '@angular/forms';
import { ApiService, IResponse } from 'src/app/shared/service/api.service';
import { MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import Swal from 'sweetalert2/dist/sweetalert2.js';
import 'sweetalert2/src/sweetalert2.scss';
import { Http2SecureServer } from 'http2';

@Component({
  selector: 'app-gra001-dialog',
  templateUrl: './gra001-dialog.component.html',
  styleUrls: ['./gra001-dialog.component.css']
})
export class Gra001DialogComponent implements OnInit {

  stu_code:any;
  fullname:any;
  class_room:any;
  stu_transcript_log_id="";
  stu_profile_id ="";
  setNo:any;
  seqNo:any;

  formStuTranscript = this.fb.group({
    stuTranscriptLogId:[""],
    stu_code:[""],
    yearGroupId:[""],
    yearGroupName:[""],
    classId:[""],
    className:[""],
    fullname:[""],
    fullnameEn:[""],
    stuProfileId: [""],
    scCode:[""],
    doctypeId:["1"],
    setNo:["",Validators.required],
    seqNo:["",Validators.required],
    transcriptDate:Date,
  });
  formONET = this.fb.group({
    thaiScore:[""]
  });
  formScore = this.fb.group({
    10070:[""],
    10030:[""]
  });
  formCriteria = this.fb.group({

  });
  formAnnotation = this.fb.group({

  });

  formCheck = this.fb.group({
    doctypeId:["1"],
    setNo:["",Validators.required],
    seqNo:["",Validators.required]
  });

  formCheckUpdate = this.fb.group({
    doctypeId:["1"],
    setNo:["",Validators.required],
    seqNo:["",Validators.required],
    stuTranscriptLogId:[""]
  });

  constructor(
    private service:ApiService,
    @Inject(MAT_DIALOG_DATA) data:any,
    public dialogRef: MatDialogRef<Gra001DialogComponent>, 
    private fb: FormBuilder,
    private ar: ActivatedRoute,
    private router: Router
    ) {}
  isProcess: boolean = false;
  id="";
  ngOnInit() {
    this.id = this.ar.snapshot.queryParamMap.get("id") || "";
    this.stu_transcript_log_id = this.ar.snapshot.queryParamMap.get("idTs") || "";
    this.stu_profile_id = this.ar.snapshot.queryParamMap.get("stuid") || "";
    this.setNo = this.ar.snapshot.queryParamMap.get("setNo") || "";
    this.seqNo = this.ar.snapshot.queryParamMap.get("seqNo") || "";
    this.formStuTranscript.controls.setNo.setValue(this.setNo);
    this.formCheck.controls.setNo.setValue(this.setNo);
    this.formCheckUpdate.controls.setNo.setValue(this.setNo);
    this.formStuTranscript.controls.seqNo.setValue(this.seqNo);
    this.formCheck.controls.seqNo.setValue(this.seqNo);
    this.formCheckUpdate.controls.seqNo.setValue(this.seqNo);
    this.loadData();
  }
  loadData() {
    this.isProcess = true;
    this.service.httpGet("/api/v1/classRoom-member/"+this.id, null).then((res: IResponse) => { 
        this.isProcess = false;
        let data = res.responseData;
        this.formStuTranscript.controls.yearGroupId.setValue(data.yearGroupId ||'');
        this.formStuTranscript.controls.yearGroupName.setValue(data.yearGroupName ||'');
        this.formStuTranscript.controls.fullname.setValue(data.fullname ||'');
        this.formStuTranscript.controls.fullnameEn.setValue(data.fullnameEn ||'');
        this.formStuTranscript.controls.classId.setValue(data.classId ||'');
        this.formStuTranscript.controls.scCode.setValue(data.scCode ||'');
        this.formStuTranscript.controls.className.setValue(data.className ||'');
        this.stu_code = data.stuCode||'';
        this.fullname = data.fullname||'';
        this.class_room = data.classRoom||'';
      });
  }
  onBack() {
    this.router.navigate(["gra/gra001"]);
  }
  close() { 
    this.dialogRef.close(); 
  }
  onSave_StuTranscript(){ 
    this.formStuTranscript.controls.stuTranscriptLogId.setValue( this.stu_transcript_log_id);
    this.formStuTranscript.controls.stuProfileId.setValue( this.stu_profile_id);
    this.formStuTranscript.controls.doctypeId.setValue("1");
    if (this.formStuTranscript.value.setNo =='' && this.formStuTranscript.value.seqNo =='') {
      Swal.fire("เพิ่มข้อมูลไม่ครบ", "กรุณาระบุข้อมูล ชุดที่,เลขที่ ", "error");
      return;
    }else if(this.formStuTranscript.value.setNo =='' ){
      Swal.fire("เพิ่มข้อมูลไม่ครบ", "กรุณาระบุข้อมูล ชุดที่", "error");
      return;
    }else if(this.formStuTranscript.value.seqNo ==''){
      Swal.fire("เพิ่มข้อมูลไม่ครบ", "กรุณาระบุข้อมูล เลขที่ ", "error");
      return;
    }else{
        Swal.fire({
          title: 'ต้องการบันทึกข้อมูล ใช่หรือไม่?',
          icon : 'info',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          confirmButtonText: 'ใช่',
          cancelButtonText: 'ไม่'
        }).then((result) => {
          if(result.dismiss == 'cancel'){
              return;
          }
          let json = this.formStuTranscript.value;
          if(json.stuTranscriptLogId == ''){
            this.service.httpPost("/api/v1/TranscriptLog",json).then((res:IResponse)=>{
              this.isProcess = false;
              if((res.responseCode||500)!=200){Swal.fire('เพิ่มข้อมูลผิดพลาด',res.responseMsg,'error' )
                return;
              }
              Swal.fire('บันทึกข้อมูล','บันทึกข้อมูลสำเร็จ','success').then(() => {
                this.close();
              });
            });
          }
          else{
            this.service.httpPut("/api/v1/TranscriptLog/update",json).then((res:IResponse)=>{
              this.isProcess = false;
              if((res.responseCode||500)!=200){
                  Swal.fire('แก้ไขข้อมูลผิดพลาด',res.responseMsg,'error' )
                return;
              }
              Swal.fire('แก้ไขข้อมูล','แก้ไขข้อมูลสำเร็จ','success').then(() => {
                this.close();
              });
            });
          }
        });
    }
  }
  onCheck(){
      this.formCheck.controls.doctypeId.setValue(1);
      this.formCheck.controls.setNo.setValue(this.formStuTranscript.value.setNo);
      this.formCheck.controls.seqNo.setValue(this.formStuTranscript.value.seqNo);
      this.formCheckUpdate.controls.doctypeId.setValue(1);
      this.formCheckUpdate.controls.setNo.setValue(this.formStuTranscript.value.setNo);
      this.formCheckUpdate.controls.seqNo.setValue(this.formStuTranscript.value.seqNo);
      this.formCheckUpdate.controls.stuTranscriptLogId.setValue( this.stu_transcript_log_id);
      if(this.stu_transcript_log_id == '' || this.stu_transcript_log_id == null){
        let json = this.formCheck.value;
        this.service.httpGet("/api/v1/TranscriptLog",json).then(res => {
          this.isProcess = false;
          let dataCheck = res.responseData;
          if(dataCheck.length>=1){
            Swal.fire('ข้อมูลผิดพลาด','ข้อมูล ชุดที่ '+this.formCheckUpdate.value.setNo+ ' และ เลขที่ '+this.formCheckUpdate.value.seqNo+' นี้มีอยู่ในระบบแล้ว', 'error');
            return this.formStuTranscript.controls.setNo.setValue(""), this.formStuTranscript.controls.seqNo.setValue("");
          }
        });
      }else{
        let json = this.formCheckUpdate.value;
        this.service.httpGet("/api/v1/TranscriptLog/Update",json).then(res => {
          this.isProcess = false;
          let dataCheck = res.responseData;
          if(dataCheck.length >=1){
            Swal.fire('ข้อมูลผิดพลาด','ข้อมูล ชุดที่ '+this.formCheckUpdate.value.setNo+ ' และ เลขที่ '+this.formCheckUpdate.value.seqNo+' นี้มีอยู่ในระบบแล้ว', 'error');
            return this.formStuTranscript.controls.setNo.setValue(this.setNo), this.formStuTranscript.controls.seqNo.setValue(this.seqNo);
          }
        });
      }
  }
}
