import { Routes } from '@angular/router';
import { AuthGuard } from '../../shared/service/auth.guard';
import { Admrp001Sub1Component } from './admrp001-sub1/admrp001-sub1.component';
import { Admrp001Sub2Component } from './admrp001-sub2/admrp001-sub2.component';
import { Admrp001Sub3Component } from './admrp001-sub3/admrp001-sub3.component';
import { Admrp001Sub4Component } from './admrp001-sub4/admrp001-sub4.component';
import { Admrp002Sub1Component } from './admrp002-sub1/admrp002-sub1.component';
import { Admrp002Sub2Component } from './admrp002-sub2/admrp002-sub2.component';
import { Admrp003Sub1Component } from './admrp003-sub1/admrp003-sub1.component';
import { Admrp004Sub1Component } from './admrp004-sub1/admrp004-sub1.component';
import { Admrp005Sub1Component } from './admrp005-sub1/admrp005-sub1.component';
import { Admrp001Sub5Component } from './admrp001-sub5/admrp001-sub5.component';

/*** Page ***/
export const AdmrpRoutes: Routes = [
  {
    path: '',
    children: [
      { path: 'admrp001-sub1', component: Admrp001Sub1Component, canActivate: [AuthGuard] },
      { path: 'admrp001-sub2', component: Admrp001Sub2Component, canActivate: [AuthGuard] },
      { path: 'admrp001-sub3', component: Admrp001Sub3Component, canActivate: [AuthGuard] },
      { path: 'admrp001-sub4', component: Admrp001Sub4Component, canActivate: [AuthGuard] },
      { path: 'admrp001-sub5', component: Admrp001Sub5Component, canActivate: [AuthGuard] },
      { path: 'admrp002-sub1', component: Admrp002Sub1Component, canActivate: [AuthGuard] },
      { path: 'admrp002-sub2', component: Admrp002Sub2Component, canActivate: [AuthGuard] },
      { path: 'admrp003-sub1', component: Admrp003Sub1Component, canActivate: [AuthGuard] },
      { path: 'admrp004-sub1', component: Admrp004Sub1Component, canActivate: [AuthGuard] },
      { path: 'admrp005-sub1', component: Admrp005Sub1Component, canActivate: [AuthGuard] }
    ]
  }

];