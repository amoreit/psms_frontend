import { Component, OnInit} from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ApiService, IResponse } from 'src/app/shared/service/api.service';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-admrp002-sub1',
  templateUrl: './admrp002-sub1.component.html',
  styleUrls: ['./admrp002-sub1.component.scss']
})
export class Admrp002Sub1Component implements OnInit {

  searchForm = this.fb.group({
    'departmentId': ['']
    , 'catClassId': ['']
    , 'section': ['1']
  });

  catclassList: [];
  departmentList: [];
  checkeducation = true; 
  checkage = false;
  view: any[] = [900,500];
  view2: any[] = [900, 500];

  showXAxis = true;
  showYAxis = true;
  gradient = false;
  showLegend = true;
  legendTitle = 'ประเภทคุณครู'
  showXAxisLabel = true;
  xAxisLabel = 'วุฒิการศึกษา';
  _xAxisLabel = 'ช่วงอายุ';
  showYAxisLabel = true;
  yAxisLabel = 'จำนวนคุณครู';
  timeline = true;
  explodeSlices = '';

  colorScheme = {
    domain: ['#9370DB', '#87CEFA', '#FA8072', '#FF7F50', '#90EE90', '#9370DB']
  };
  showLabels = true;
  numberTeacher = [];
  _numberTeacher = [];
  cat_class_name:any;
  show_cat_class:any;
  section_name:any;
  department_name:any;
  show_department:any;

  constructor(private translate: TranslateService,private fb: FormBuilder, private service: ApiService) {

  }

  multi: any = []
  _multi: any = []

  ngOnInit() {
    this.ddlCatClass();
    this.onSearch(0);
  }

  onSearch(e) {
    if(this.searchForm.value.section == "1"){
      this.checkeducation = true;
      this.checkage = false;
      this.section_name = 'วุฒิการศึกษา';
    this.service.httpGet('/api/v1/trn-classroom-member/admrp002sub1', this.searchForm.value).then((res: IResponse) => {
      if (res.responseData.length === 0) {
        this.numberTeacher = []
      } else {
        this.numberTeacher = res.responseData || [];
        for (let i = 0; i < this.numberTeacher.length; i++) {
          this.multi.push({
            name: this.numberTeacher[i].education,
            series: [{ name: "คุณครูประจำ", value: this.numberTeacher[i].permanent},
            { name: "คุณครูพิเศษ", value: this.numberTeacher[i].not_permanent}]
          })
        }
        this.multi = [...this.multi];
      }
    });
    this.multi = [];
  }
  if(this.searchForm.value.section == "2"){
    this.checkeducation = false;
    this.checkage = true;
    this.service.httpGet('/api/v1/trn-classroom-member/admrp002sub1', this.searchForm.value).then((res: IResponse) => {
      if (res.responseData.length === 0) {
        this._numberTeacher = []
      } else {
        this._numberTeacher = res.responseData || [];
        for (let i = 0; i < this._numberTeacher.length; i++) {
          this.multi.push({
            name: this._numberTeacher[i].education,
            series: [{ name: "คุณครูประจำ", value: this._numberTeacher[i].permanent},
            { name: "คุณครูพิเศษ", value: this._numberTeacher[i].not_permanent}]
          })
        }
        this._multi = [...this._multi];
      }
    });
    this._multi = [];
  }

   this.show_cat_class = this.cat_class_name;
   this.show_department = this.department_name;
  }


  ddlCatClass(){
    this.service.httpGet('/api/v1/0/catclass/ddl',null).then((res)=>{
      this.catclassList = res||[];
    });
  }

  ddlDepartment(){
    let json = {'catClassId':this.searchForm.value.catClassId}
    this.service.httpGet('/api/v1/0/department/ddlfiltercatclass',json).then((res)=>{
      this.departmentList = res||[];
    });
  }


  onCheckCatClass(){
    let ele = this.catclassList.filter((o)=>{ 
      return o['catClassId'] == this.searchForm.value.catClassId;
    });
    this.cat_class_name = ele[0]['lnameTh']
  }

  onCheckDepartment(){
    let ele = this.departmentList.filter((o)=>{ 
      return o['department_id'] == this.searchForm.value.departmentId;
    });
    this.department_name = ele[0]['name']
  }

  onPreviewPDF(){
    
  }

  onCancel(){
    this.searchForm = this.fb.group({
      'departmentId': ['']
      , 'catClassId': ['']
      , 'section': ['1']
  });
    this.onSearch(0);
  }

}
