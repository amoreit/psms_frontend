import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FlexLayoutModule } from '@angular/flex-layout';
import { ReactiveFormsModule } from '@angular/forms';
import { NgxChartsModule } from '@swimlane/ngx-charts';

/*** Material ***/
import {
  MatExpansionModule,
  MatButtonModule,
  MatIconModule,
  MatInputModule,
  MatTableModule,
  MatCardModule,
  MatFormFieldModule,
  MatPaginatorModule,
  MatDialogModule,
  MatSlideToggleModule,
  MatProgressSpinnerModule
} from '@angular/material';

/*** Routing ***/
import { AdmrpRoutes } from './admrp.routing';

/*** Page ***/
import { TranslateModule } from '@ngx-translate/core';

import { Admrp001Sub1Component } from './admrp001-sub1/admrp001-sub1.component';
import { Admrp001Sub2Component } from './admrp001-sub2/admrp001-sub2.component';
import { Admrp001Sub3Component } from './admrp001-sub3/admrp001-sub3.component';
import { Admrp001Sub4Component } from './admrp001-sub4/admrp001-sub4.component';
import { Admrp001Sub5Component } from './admrp001-sub5/admrp001-sub5.component';
import { Admrp002Sub1Component } from './admrp002-sub1/admrp002-sub1.component';
import { Admrp002Sub2Component } from './admrp002-sub2/admrp002-sub2.component';
import { Admrp003Sub1Component } from './admrp003-sub1/admrp003-sub1.component';
import { Admrp004Sub1Component } from './admrp004-sub1/admrp004-sub1.component';
import { Admrp005Sub1Component } from './admrp005-sub1/admrp005-sub1.component';



@NgModule({
  imports: [
    CommonModule,
    FlexLayoutModule,
    TranslateModule,
    RouterModule.forChild(AdmrpRoutes),
    MatExpansionModule,
    MatButtonModule,
    MatIconModule,
    MatInputModule,
    MatTableModule,
    MatCardModule,
    MatFormFieldModule,
    ReactiveFormsModule,
    MatPaginatorModule,
    MatDialogModule,
    MatSlideToggleModule,
    MatProgressSpinnerModule,
    NgxChartsModule
  ],
  providers: [],
  exports: [RouterModule],
  declarations: [
    /*** Page ***/
    Admrp001Sub1Component,
    Admrp001Sub2Component,
    Admrp001Sub3Component,
    Admrp001Sub4Component,
    Admrp001Sub5Component,
    Admrp002Sub1Component,
    Admrp002Sub2Component,
    Admrp003Sub1Component,
    Admrp004Sub1Component,
    Admrp005Sub1Component,


  ],
  entryComponents: [
  ]
})
export class AdmrpModule { }
