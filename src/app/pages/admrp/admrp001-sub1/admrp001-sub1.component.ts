import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ApiService, IResponse } from 'src/app/shared/service/api.service';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-admrp001-sub1',
  templateUrl: './admrp001-sub1.component.html',
  styleUrls: ['./admrp001-sub1.component.scss']
})
export class Admrp001Sub1Component implements OnInit {
  searchForm = this.fb.group({
    'yearGroupId': ['']
    , 'catClassId': ['']
  });

  yearGroupList: [];
  catclassList: [];
  checkdata = true;
  view: any[] = [1300, 500];

  gradient = false;
  label = 'นักเรียน';

  colorScheme = {
    domain: ['#9370DB', '#87CEFA']
  };
  showLabels = true;
  numberStuPie = [];
  year_group_name: any;
  cat_class_name: any;
  show_cat_class: any;
  show_year_group: any;

  constructor(private translate: TranslateService,private fb: FormBuilder, private service: ApiService) {

  }

  single: any = [];

  ngOnInit() {
    this.ddlYearGroup();
    this.ddlCatClass();

  }

  onSearch(e) {
    this.service.httpGet('/api/v1/trn-classroom-member/admrp001sub01', this.searchForm.value).then((res: IResponse) => {
      if (res.responseData.length === 0) {
        this.numberStuPie = []
        this.checkdata = false;
      } else {
        this.numberStuPie = res.responseData || [];
        this.checkdata = true;
        for (let i = 0; i < this.numberStuPie.length; i++) {
          this.single.push(
            { name: "ชาย", value: this.numberStuPie[i].male },
            { name: "หญิง", value: this.numberStuPie[i].female }
          )
        }
        this.single = [...this.single];
      }

    });
    this.single = [];

    this.show_cat_class = this.cat_class_name;
    this.show_year_group = this.year_group_name
  }


  ddlYearGroup() {
    this.service.httpGet('/api/v1/0/year-group/ddl', null).then((res) => {
      this.yearGroupList = res || [];
      let ele = this.yearGroupList.filter((o) => {
        return o['iscurrent'] == "true";
      });
      this.searchForm.controls.yearGroupId.setValue(ele[0]['yearGroupId'])
      this.onCheckYearGroup();
      this.onSearch(0);
    });
  }

  ddlCatClass() {
    this.service.httpGet('/api/v1/0/catclass/ddl', null).then((res) => {
      this.catclassList = res || [];
    });
  }

  onCheckYearGroup() {
    let ele = this.yearGroupList.filter((o) => {
      return o['yearGroupId'] == this.searchForm.value.yearGroupId;
    });
    this.year_group_name = ele[0]['name']
  }

  onCheckCatClass() {
    let ele = this.catclassList.filter((o) => {
      return o['catClassId'] == this.searchForm.value.catClassId;
    });
    this.cat_class_name = ele[0]['lnameTh']
  }

  onPreviewPDF() {

  }

  onCancel() {
    this.searchForm = this.fb.group({
      'yearGroupId': ['']
      , 'catClassId': ['']
    });
    this.ddlYearGroup()
    this.cat_class_name = '';
  }
  printComponent(cmpName) {
    let printContents = document.getElementById(cmpName).innerHTML;
    let originalContents = document.body.innerHTML;
    document.body.innerHTML = printContents;
    window.print();
    document.body.innerHTML = originalContents;
    location.reload();
  }
}
