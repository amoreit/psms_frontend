import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder } from '@angular/forms';
import { ApiService, IResponse } from 'src/app/shared/service/api.service';
import { MatDialog, MatTableDataSource, MatPaginator } from '@angular/material';
import { TranslateService } from '@ngx-translate/core';


@Component({
  selector: 'app-admrp001-sub4',
  templateUrl: './admrp001-sub4.component.html',
  styleUrls: ['./admrp001-sub4.component.scss']
})
export class Admrp001Sub4Component implements OnInit {



  isProcess: boolean = false;
  searchForm = this.fb.group({
    'startYearId': ['']
    , 'endYearId': ['']
  });

  yearList: [];
  checkdata = true;
  view2: any[] = [1100, 500];

  showXAxis = true;
  showYAxis = true;
  gradient = false;
  showLegend = true;
  legendTitle = ''
  label = 'นักเรียน';
  showXAxisLabel = true;
  xAxisLabel = 'ปีที่จบการศึกษา';
  showYAxisLabel = true;
  yAxisLabel = 'จำนวนนักเรียน';
  timeline = true;
  explodeSlices = '';

  colorScheme = {
    domain: ['#9370DB', '#87CEFA']
  };
  showLabels = true;
  numberStu = [];
  start_year: any;
  end_year: any;
  show_start_year: any;
  show_end_year: any;

  constructor(private translate: TranslateService,private fb: FormBuilder, private service: ApiService, public dialog: MatDialog, private router: Router) {

  }

  multi: any = []

  ngOnInit() {
    this.ddlYear();
    this.onSearch(0);
  }

  onSearch(e) {
    this.service.httpGet('/api/v1/trn-classroom-member/numberstu', this.searchForm.value).then((res: IResponse) => {
      if (res.responseData.length === 0) {
        this.numberStu = []
        this.checkdata = false;
      } else {
        this.numberStu = res.responseData || [];
        this.checkdata = true;
        for (let i = 0; i < this.numberStu.length; i++) {
          this.multi.push({
            name: this.numberStu[i].name,
            series: [{ name: this.numberStu[i].number, value: this.numberStu[i].value }]
          })
        }
        this.multi = [...this.multi];
      }
    });
    this.multi = [];

    this.show_start_year = this.start_year;
    this.show_end_year = this.end_year;
  }


  ddlYear() {
    this.service.httpGet('/api/v1/0/year/ddl', null).then((res) => {
      this.yearList = res || [];
    });
  }

  onCheckStartYear() {
    let ele = this.yearList.filter((o) => {
      return o['yearId'] == this.searchForm.value.startYearId;
    });
    this.start_year = ele[0]['yearCode']
  }

  onCheckEndYear() {
    let ele = this.yearList.filter((o) => {
      return o['yearId'] == this.searchForm.value.endYearId;
    });
    this.end_year = ele[0]['yearCode']
  }

  onPreviewPDF() {

  }

  onCancel() {
    this.searchForm = this.fb.group({
      'startYearId': ['']
      , 'endYearId': ['']
    });
    this.start_year = ''
    this.end_year = ''
    this.onSearch(0);

  }
  printComponent(cmpName) {
    let printContents = document.getElementById(cmpName).innerHTML;
    let originalContents = document.body.innerHTML;
    document.body.innerHTML = printContents;
    window.print();
    document.body.innerHTML = originalContents;
    location.reload();
  }

}
