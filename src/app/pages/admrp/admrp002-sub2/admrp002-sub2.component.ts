import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ApiService, IResponse } from 'src/app/shared/service/api.service';
import { MatDialog, MatTableDataSource, MatPaginator } from '@angular/material';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-admrp002-sub2',
  templateUrl: './admrp002-sub2.component.html',
  styleUrls: ['./admrp002-sub2.component.scss']
})
export class Admrp002Sub2Component implements OnInit {

  searchForm = this.fb.group({
    'departmentId': ['']
    , 'catClassId': ['']
  });

  catclassList: [];
  departmentList: [];
  view: any[] = [900, 500];
  view2: any[] = [900, 500];

  showXAxis = true;
  showYAxis = true;
  gradient = false;
  showLegend = true;
  legendTitle = 'ประเภทคุณครู'
  showXAxisLabel = true;
  xAxisLabel = 'เพศ';
  showYAxisLabel = true;
  yAxisLabel = 'จำนวนคุณครู';
  timeline = true;
  explodeSlices = '';

  colorScheme = {
    domain: ['#9370DB', '#87CEFA', '#FA8072', '#FF7F50', '#90EE90', '#9370DB']
  };
  showLabels = true;
  numberTeacher = [];
  cat_class_name: any;
  show_cat_class: any;
  department_name: any;
  show_department: any;

  constructor(private translate: TranslateService,private fb: FormBuilder, private service: ApiService, public dialog: MatDialog) {

  }

  multi: any = []

  ngOnInit() {
    this.ddlCatClass();
    this.onSearch(0);
  }

  onSearch(e) {
      this.service.httpGet('/api/v1/trn-classroom-member/admrp002sub2', this.searchForm.value).then((res: IResponse) => {
        if (res.responseData.length === 0) {
          this.numberTeacher = []
        } else {
          this.numberTeacher = res.responseData || [];
          for (let i = 0; i < this.numberTeacher.length; i++) {
            this.multi.push({
              name: this.numberTeacher[i].gender,
              series: [{ name: "คุณครูประจำ", value: this.numberTeacher[i].permanent },
              { name: "คุณครูพิเศษ", value: this.numberTeacher[i].not_permanent }]
            })
          }
          this.multi = [...this.multi];
        }
      });
      this.multi = [];

    this.show_cat_class = this.cat_class_name;
    this.show_department = this.department_name;
  }


  ddlCatClass() {
    this.service.httpGet('/api/v1/0/catclass/ddl', null).then((res) => {
      this.catclassList = res || [];
    });
  }

  ddlDepartment() {
    let json = { 'catClassId': this.searchForm.value.catClassId }
    this.service.httpGet('/api/v1/0/department/ddlfiltercatclass', json).then((res) => {
      this.departmentList = res || [];
    });
  }


  onCheckCatClass() {
    let ele = this.catclassList.filter((o) => {
      return o['catClassId'] == this.searchForm.value.catClassId;
    });
    this.cat_class_name = ele[0]['lnameTh']
  }

  onCheckDepartment() {
    let ele = this.departmentList.filter((o) => {
      return o['department_id'] == this.searchForm.value.departmentId;
    });
    this.department_name = ele[0]['name']
  }

  onPreviewPDF() {

  }

  onCancel(){
    this.searchForm = this.fb.group({
      'departmentId': ['']
      , 'catClassId': ['']
  });
    this.onSearch(0);
  }
}
