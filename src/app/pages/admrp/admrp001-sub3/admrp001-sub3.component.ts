import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ApiService, IResponse } from 'src/app/shared/service/api.service';


@Component({
  selector: 'app-admrp001-sub3',
  templateUrl: './admrp001-sub3.component.html',
  styleUrls: ['./admrp001-sub3.component.scss']
})
export class Admrp001Sub3Component implements OnInit {


  isProcess: boolean = false;
  searchForm = this.fb.group({
    'yearGroupId': ['']
    , 'catClassId': ['']
  });

  yearGroupList: [];
  catclassList: [];
  checkdata = true;
  view: any[] = [1400, 500];
  view2: any[] = [1400, 500];

  showXAxis = true;
  showYAxis = true;
  gradient = false;
  showLegend = true;
  legendTitle = ''
  label = 'นักเรียน';
  showXAxisLabel = true;
  xAxisLabel = 'ชั้นเรียน';
  showYAxisLabel = true;
  yAxisLabel = 'จำนวนนักเรียน';
  timeline = true;
  explodeSlices = '';

  colorScheme = {
    domain: ['#9370DB', '#87CEFA']
  };
  showLabels = true;
  numberStu = [];
  numberStuPie = [];
  year_group_name: any;
  cat_class_name: any;
  show_cat_class: any;
  show_year_group: any;

  constructor(private fb: FormBuilder, private service: ApiService) { }

  multi: any = []
  single: any = [];

  ngOnInit() {
    this.ddlYearGroup();
    this.ddlCatClass();

  }

  onSearch(e) {
    this.service.httpGet('/api/v1/trn-classroom-member/numberstuforcatclass', this.searchForm.value).then((res: IResponse) => {
      if (res.responseData.length === 0) {
        this.numberStu = []
        this.checkdata = false;
      } else {
        this.numberStu = res.responseData || [];
        this.checkdata = true;
        for (let i = 0; i < this.numberStu.length; i++) {
          this.multi.push({
            name: this.numberStu[i].class_name,
            series: [{ name: "ชาย", value: this.numberStu[i].male },
            { name: "หญิง", value: this.numberStu[i].female }]
          })
        }
        this.multi = [...this.multi];
      }
    });
    this.multi = [];

    //pie
    this.service.httpGet('/api/v1/trn-classroom-member/numberstuforcatclasspie', this.searchForm.value).then((res: IResponse) => {
      if (res.responseData.length === 0) {
        this.numberStuPie = []
        this.checkdata = false;
      } else {
        this.numberStuPie = res.responseData || [];
        this.checkdata = true;
        for (let i = 0; i < this.numberStuPie.length; i++) {
          this.single.push(
            { name: "ชาย", value: this.numberStuPie[i].male },
            { name: "หญิง", value: this.numberStuPie[i].female }
          )
        }
        this.single = [...this.single];
      }

    });
    this.single = [];

    this.show_cat_class = this.cat_class_name;
    this.show_year_group = this.year_group_name
  }


  ddlYearGroup() {
    this.service.httpGet('/api/v1/0/year-group/ddl', null).then((res) => {
      this.yearGroupList = res || [];
      let ele = this.yearGroupList.filter((o) => {
        return o['iscurrent'] == "true";
      });
      this.searchForm.controls.yearGroupId.setValue(ele[0]['yearGroupId'])
      this.onCheckYearGroup();
      this.onSearch(0);
    });
  }

  ddlCatClass() {
    this.service.httpGet('/api/v1/0/catclass/ddl', null).then((res) => {
      this.catclassList = res || [];
    });
  }

  onCheckYearGroup() {
    let ele = this.yearGroupList.filter((o) => {
      return o['yearGroupId'] == this.searchForm.value.yearGroupId;
    });
    this.year_group_name = ele[0]['name']
  }

  onCheckCatClass() {
    let ele = this.catclassList.filter((o) => {
      return o['catClassId'] == this.searchForm.value.catClassId;
    });
    this.cat_class_name = ele[0]['lnameTh']
  }

  onPreviewPDF() {

  }

  onCancel() {
    this.searchForm = this.fb.group({
      'yearGroupId': ['']
      , 'catClassId': ['']
    });
    this.ddlYearGroup()
    this.cat_class_name = '';
  }
  printComponent(cmpName) {
    let printContents = document.getElementById(cmpName).innerHTML;
    let originalContents = document.body.innerHTML;
    document.body.innerHTML = printContents;
    window.print();
    document.body.innerHTML = originalContents;
    location.reload();
  }

}
