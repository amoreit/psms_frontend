import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder } from '@angular/forms';
import { ApiService, IResponse } from 'src/app/shared/service/api.service';
import { MatDialog, MatTableDataSource, MatPaginator } from '@angular/material';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-admrp004-sub1',
  templateUrl: './admrp004-sub1.component.html',
  styleUrls: ['./admrp004-sub1.component.scss']
})
export class Admrp004Sub1Component implements OnInit {

  elements: any = [
    { id: 1, first: 'Mark', last: 'Otto', handle: '@mdo' },
    { id: 2, first: 'Jacob', last: 'Thornton', handle: '@fat' },
    { id: 3, first: 'Larry', last: 'the Bird', handle: '@twitter' },
  ];

  headElements = ['ID', 'First', 'Last', 'Handle'];

  title = 'Angular Charts';
  view: any[] = [1000, 500]; 
  // options for the chart
  showXAxis = true;
  yScaleMax = "20";
  maxYAxisTickLength = 10; 
  showYAxis = true;
  gradient = false;
  showXAxisLabel = true;
  showYAxisLabel = true;
  barPadding = 50;

  showLegend = false;
  legendTitle = 'เพศ';
  xAxisLabel = 'รายวิชา';
  yAxisLabel = 'คะแนนเต็ม';
  timeline = true;
  colorScheme = {
    domain: ['#9370DB', '#87CEFA', '#FA8072', '#FF7F50', '#90EE90', '#9370DB']
  };
  //pie
  showLabels = true;
  // data goes here
  public data = [
    {
      "name": "ไทย",
      "value": 16.38

    },
    {
      "name": "คณิต",
      "value": 16.67

    },
    {
      "name": "วิทย์",
      "value": 15.72

    },
    {
      "name": "สังคม",
      "value": 13.73

    },
    {
      "name": "ประวัติศาสตร์",
      "value": 14.04
    },
    {
      "name": "ภาษาอังกฤษ",
      "value": 11.50
    }
  ];

  constructor(
    private translate: TranslateService,
    private router: Router
  ) { }

  ngOnInit() {
  }

  printComponent(cmpName) {
    let printContents = document.getElementById(cmpName).innerHTML;
    let originalContents = document.body.innerHTML;
    document.body.innerHTML = printContents;
    window.print();
    document.body.innerHTML = originalContents;
    location.reload();
  }

  onCancel(){}

}
