import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { ApiService, IResponse } from 'src/app/shared/service/api.service';
import { ActivatedRoute, Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';

// sweetalert2
import Swal from 'sweetalert2/dist/sweetalert2.js';
import 'sweetalert2/src/sweetalert2.scss';
import { UtilService } from 'src/app/_util/util.service';
// sweetalert2

@Component({
  selector: 'app-adm001-form', 
  templateUrl: './adm001-form.component.html',
  styleUrls: ['./adm001-form.component.scss']
})
export class Adm001FormComponent implements OnInit {
 
  isProcess:boolean = false;
  form = this.fb.group({
    'roleId':['']
    , 'roleCode':['',Validators.required]
    , 'roleName':['',Validators.required] 
  });

  id = '';
  validateValue = false;
  headerType = '';

  constructor(private translate: TranslateService,ar:ActivatedRoute,private fb: FormBuilder,private service:ApiService,private router: Router) {
    this.id = ar.snapshot.queryParamMap.get('id')||'';
    if(this.id == ''){
      this.headerType = this.translate.instant('modeType.MT0007');
      this.translate.onLangChange.subscribe(() => {
        this.headerType = this.translate.instant('modeType.MT0007');
      });
    }else{
      this.headerType = this.translate.instant('modeType.MT0008');
      this.translate.onLangChange.subscribe(() => {
        this.headerType = this.translate.instant('modeType.MT0008');
      });
      this.loadData();
    }
  }

  ngOnInit() {

  }

  loadData(){
    this.isProcess = true;
    this.service.httpGet('/api/v1/role/'+this.id,null).then((res:IResponse)=>{
      this.isProcess = false;
      let data = res.responseData;
      this.form.controls.roleId.setValue(data.roleId||'');
      this.form.controls.roleCode.setValue(data.roleCode||'');
      this.form.controls.roleName.setValue(data.roleName||'');
    });
  }

  onBackPage(){
    this.router.navigate(['/adm/adm001']);
  }

  onSave(){
    if (this.form.invalid) {
      this.validateValue = true;
      Swal.fire(
        this.translate.instant('message.add_error'),//เพิ่มข้อมูลผิดพลาด
        this.translate.instant('alert.validate'),//กรุณาระบุข้อมูล ให้ครบ.
        'error'
      )
      return;
    }

    Swal.fire({
      title: this.translate.instant('alert.save'),//ต้องการบันทึกข้อมูล ใช่หรือไม่
      icon : 'info',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: this.translate.instant('psms.DL0008'),//ใช่
      cancelButtonText: this.translate.instant('psms.DL0009')//ไม่
    }).then((result) => {
      if(result.dismiss == 'cancel'){
        return;
      }
      this.isProcess = true;
      let json = this.form.value;
      if(json.roleId == ''){
        this.service.httpPost('/api/v1/role',json).then((res:IResponse)=>{
          this.isProcess = false;
          if((res.responseCode||500)!=200){
            Swal.fire(
              this.translate.instant('message.add_error'),//เพิ่มข้อมูลผิดพลาด
              res.responseMsg,
              'error' 
            )
            return;
          }
          Swal.fire(
            this.translate.instant('alert.add_header'),//เพิ่มข้อมูล
            this.translate.instant('message.add'),//เพิ่มข้อมูลสำเร็จ
            'success',
          ).then(() => {this.onBackPage();})
        });
        return;
      }
      this.service.httpPut('/api/v1/role',json).then((res:IResponse)=>{
        this.isProcess = false;
        if((res.responseCode||500)!=200){
          Swal.fire(
            this.translate.instant('message.edit_error'),//แก้ไขข้อมูลผิดพลาด
            res.responseMsg,
            'error' 
          )
          return;
        }
        Swal.fire(
          this.translate.instant('psms.DL0006'),//แก้ไขข้อมูล
          this.translate.instant('psms.DL0007'),//แก้ไขข้อมูลสำเร็จ
          'success'
        ).then((result) => {this.onBackPage();})      
      });
      });
    
  }

  onCancel(){
    this.form.controls.roleCode.setValue('');
    this.form.controls.roleName.setValue('');
    this.loadData();
  }
}
