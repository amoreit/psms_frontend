import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { MatPaginator, MatTableDataSource, MatDialog } from '@angular/material';
import { ApiService, IResponse } from '../../../shared/service/api.service';
import { Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';

// sweetalert2
import Swal from 'sweetalert2/dist/sweetalert2.js';
import 'sweetalert2/src/sweetalert2.scss';
import { UtilService } from 'src/app/_util/util.service';
// sweetalert2

@Component({
  selector: 'app-adm001',
  templateUrl: './adm001.component.html',
  styleUrls: ['./adm001.component.scss']
})
export class Adm001Component implements OnInit {

  isProcess: boolean = false;
  searchForm = this.fb.group({
    'roleCode': ['']
    , 'roleName': ['']
    , 'p': ['']
    , 'result': [10]
  });
  pageSize: number;
  displayedColumns: string[] = ['roleIndex', 'roleCode', 'roleName', 'roleStatus'];
  dataSource = new MatTableDataSource();

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  constructor(private translate: TranslateService, private fb: FormBuilder, private service: ApiService, public dialog: MatDialog, private router: Router) {

  }

  ngOnInit() {
    this.onSearch(0);
  }

  onSearch(e) {
    this.isProcess = true;
    this.searchForm.value.p = (this.paginator.pageIndex = e.pageIndex || 0) + 1;
    this.service.httpGet('/api/v1/role', this.searchForm.value).then((res: IResponse) => {
      this.isProcess = false;
      this.dataSource.data = res.responseData || [];
      this.pageSize = res.responseSize || 0;
    });
  }

  onCancel() {
    this.searchForm.controls.roleCode.setValue('');
    this.searchForm.controls.roleName.setValue('');
    this.onSearch(0);
  }

  onDelete(e) {
    Swal.fire({
      title: this.translate.instant('alert.delete'),//ต้องการลบข้อมูล ใช่หรือไม่
      icon: 'info',
      showCancelButton: true,
      confirmButtonColor: '#2085d6',
      cancelButtonVolor: '#d33',
      confirmButtonText: this.translate.instant('psms.DL0008'),//ใช่
      cancelButtonText: this.translate.instant('psms.DL0009')//ไม่
    }).then((result) => {
      if (result.dismiss == 'cancel') {
        return;
      }
      this.isProcess = true;
      this.service.httpDelete('/api/v1/role', { 'roleId': e.roleId }).then((res: IResponse) => {
        this.isProcess = false;
        if ((res.responseCode || 500) != 200) {
          Swal.fire(
            this.translate.instant('message.delete_error'),//ลบข้อมูลผิดพลาด
            res.responseMsg,
            'error' 
          )
          return;
        }
        Swal.fire(
          this.translate.instant('psms.B0024'),//ลบข้อมูล
          this.translate.instant('message.delete'),//ลบข้อมูลสำเร็จ
          'success'
        )
        this.onSearch(0);
      });
    });

  }

  onForm(id) {
    this.router.navigate(['/adm/adm001/form'], { queryParams: { id: id } });
  }

  onReport() {
    this.isProcess = true;
    this.service.httpPreviewPDF('/api/v1/role-group/report/pdf', this.searchForm.value).then((res) => {
      this.isProcess = false;
      window.location.href = res;
    });
  }

  onReportPDF() {
    this.isProcess = true;
    this.service.httpReportPDF('/api/v1/role-group/report/pdf', "adm001", this.searchForm.value).then((res) => {
      this.isProcess = false;
    });
  }

  onReportXls() {
    this.isProcess = true;
    this.service.httpReportXLS('/api/v1/role-group/report/xls', "adm001", this.searchForm.value).then((res) => {
      this.isProcess = false;
    });
  }
}