import { Component, OnInit,ViewChild } from '@angular/core';
import { MatPaginator, MatTableDataSource, MatDialog } from '@angular/material';
import { Router } from '@angular/router';
import Swal from 'sweetalert2/dist/sweetalert2.js';
import 'sweetalert2/src/sweetalert2.scss';
import { FormBuilder } from '@angular/forms';
import { ApiService, IResponse } from 'src/app/shared/service/api.service';
import { TranslateService } from '@ngx-translate/core';


@Component({
  selector: 'app-adm006',
  templateUrl: './adm006.component.html',
  styleUrls: ['./adm006.component.scss'] 
})
export class Adm006Component implements OnInit {

  searchForm = this.fb.group({
      'userType':['']
      , 'firstName':['']
      , 'lastName':[''] 
      , 'activatedCheck':['']
      , 'p':['']
      , 'result':[10]
  }); 
  
  headerSelected:Boolean;
  isSelected:any; 
  checklist:any;
  _allChecklist:any;
  chechUser = false;

  roleList:[];

  pageSize:number; 
  displayedColumns: string[] = ['no', 'usrName', 'firstName', 'lastName', 'email', 'phoneNo', 'roleName', 'activated'];
  dataSource = new MatTableDataSource();

  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  constructor(
    private translate: TranslateService,
    private fb: FormBuilder,
    private service:ApiService,
    public dialog: MatDialog,
    private router: Router 
  ) {} 

  ngOnInit() {
    this.onSearch(0);  
    this.ddlRoleGroup(); 
  }

  onSearch(e){
    this.searchForm.value.p = (this.paginator.pageIndex = e.pageIndex||0)+1;
    this.service.httpGet('/api/v1/admUser',this.searchForm.value).then((res:IResponse)=>{
        this.dataSource.data = res.responseData||[];
        this.checklist = this.dataSource.data;
        this.pageSize = res.responseSize||0;
    });
  }
 
  ddlRoleGroup(){
    this.service.httpGet('/api/v1/0/adm-role/ddlAll',null).then((res)=>{
        this.roleList = res||[];
    });
  }
 
  onClear(){
    this.searchForm = this.fb.group({
      'userType':['']
      , 'firstName':['']
      , 'lastName':[''] 
      , 'phoneNo':[''] 
      , 'activatedCheck':[''] 
    }); 
    this.onSearch(0);
  }
 
  onFormAdd(){
    this.router.navigate(['/adm/adm006/form'],{ queryParams: {modeType:'registerlistAdd'}});
  }

  onFormEdit(id){
    this.router.navigate(['/adm/adm006/form'],{ queryParams: {id:id, modeType:'registerlistEdit'}});
  }

  checkAll() { 
    for (var i = 0; i < this.checklist.length; i++) {
      this.checklist[i].isSelected = this.headerSelected;
    }
    this.getCheckedItemList();
  }
  
  isAllSelected() {
    this.checklist.every(function(item:any) {
        return item.isSelected == true;
    })
    this.headerSelected = false;
    this.getCheckedItemList(); 
  }

  getCheckedItemList(){
    this._allChecklist = [];
    for (var i = 0; i < this.checklist.length; i++) {
      if(this.checklist[i].isSelected){
        let itemData = {
            'userId':this.checklist[i].user_id, 
            'firstName':this.checklist[i].first_name, 
            'lastName':this.checklist[i].last_name,
            'usrName':this.checklist[i].usr_name,
            'email':this.checklist[i].email,
            'activated':this.checklist[i].activated
        };
        this._allChecklist.push(itemData);  
      }
    }
    if(this._allChecklist.length == 0){
      this.chechUser = false;
    }else{
      this.chechUser = true;
    }
  }

  /** UPDATE ALL **/
  onConfirmAll(){
    Swal.fire({
      title: this.translate.instant('psms.DL0016'),//ต้องการดำเนินการที่เลือก ใช่หรือไม่
      icon: 'info',
      showCancelButton: true,
      confirmButtonColor: '#2085d6',
      cancelButtonVolor: '#d33',
      confirmButtonText:  this.translate.instant('psms.DL0008'),
      cancelButtonText:  this.translate.instant('psms.DL0009')
    }).then((result) => {
      if(result.dismiss == 'cancel'){
          return; 
      }

      let json = this._allChecklist;
      this.service.httpPut('/api/v1/admUser/updatedCheckAll',json).then((res:IResponse)=>{
        if((res.responseCode||500)!=200){
          Swal.fire(
            this.translate.instant('psms.DL0017'),//ดำเนินการที่เลือกผิดพลาด
            res.responseMsg,
            'error'
          )
            return;
        }Swal.fire(
          this.translate.instant('psms.DL0018'),//ดำเนินการที่เลือก
          this.translate.instant('psms.DL0019'),//ดำเนินการที่เลือกสำเร็จ
          'success'
        )
        this.onSearch(0);
      });
    }); 
  }

  /** UPDATE TRUE **/
  onConfirmTrue(e){
    Swal.fire({
        title: this.translate.instant('psms.DL0020'),//ต้องการอนุมัติ ใช่หรือไม่
        icon: 'info',
        showCancelButton: true,
        confirmButtonColor: '#2085d6',
        cancelButtonVolor: '#d33',
        confirmButtonText:  this.translate.instant('psms.DL0008'),
        cancelButtonText:  this.translate.instant('psms.DL0009')
    }).then((result) => {
      if(result.dismiss == 'cancel'){
          return; 
      }
      this.service.httpPut('/api/v1/admUser',{'userId':e.user_id}).then((res:IResponse)=>{
        if((res.responseCode||500)!=200){
            Swal.fire(
              this.translate.instant('psms.DL0021'),//อนุมัติผิดพลาด
              res.responseMsg,
              'error'
            )
            return;
        }Swal.fire(
          this.translate.instant('psms.DL0022'),//อนุมัติ
          this.translate.instant('psms.DL0023'),//อนุมัติสำเร็จ
          'success'
        )
        this.onSearch(0);
      });
    }); 
  }

  /** UPDATE FALSE **/
  onConfirmFalse(e){
    Swal.fire({
        title: this.translate.instant('psms.DL0024'),//ต้องการยกเลิกการอนุมัติ ใช่หรือไม่
        icon: 'info',
        showCancelButton: true,
        confirmButtonColor: '#2085d6',
        cancelButtonVolor: '#d33',
        confirmButtonText:  this.translate.instant('psms.DL0008'),
        cancelButtonText:  this.translate.instant('psms.DL0009')
    }).then((result) => {
      if(result.dismiss == 'cancel'){
          return; 
      }
      this.service.httpPut('/api/v1/admUser/updateFalse',{'userId':e.user_id}).then((res:IResponse)=>{
        if((res.responseCode||500)!=200){
            alert(res.responseMsg);
            return;
        }Swal.fire(
          this.translate.instant('psms.DL0025'),//ยกเลิกอนุมัติ
          this.translate.instant('psms.DL0026'),//ยกเลิกอนุมัติสำเร็จ
          'success'
        ) 
        this.onSearch(0);
      });
    }); 
  }

  /** SEND E-MAIL **/
  onSend(e){
    Swal.fire({
        title: this.translate.instant('psms.DL0010'),//ต้องการส่งอีเมล์ ใช่หรือไม่
        icon: 'info',
        showCancelButton: true,
        confirmButtonColor: '#2085d6',
        cancelButtonVolor: '#d33',
        confirmButtonText:  this.translate.instant('psms.DL0008'),
        cancelButtonText:  this.translate.instant('psms.DL0009')
    }).then((result) => {
      if(result.dismiss == 'cancel'){
          return;
      }
      this.service.httpPut('/api/v1/admUser/sendMail',{'userId':e.user_id}).then((res:IResponse)=>{
        if((res.responseCode||500)!=200){
          Swal.fire(
            this.translate.instant('psms.DL0027'),//ส่งอีเมล์ผิดพลาด
            res.responseMsg,
            'error'
          )
          return;
        }Swal.fire(
          this.translate.instant('psms.DL0028'),//ส่งอีเมล์
          this.translate.instant('psms.DL0029'),//ส่งอีเมล์สำเร็จ
          'success'
        )
        this.onSearch(0);
      });
    }); 
  }

}
