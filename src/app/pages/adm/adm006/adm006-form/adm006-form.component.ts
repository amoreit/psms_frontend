import { Component, OnInit } from '@angular/core';
import Swal from 'sweetalert2/dist/sweetalert2.js';
import 'sweetalert2/src/sweetalert2.scss';
import { Validators, FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { UtilService } from 'src/app/_util/util.service';
import { ApiService, IResponse } from 'src/app/shared/service/api.service';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-adm006-form',
  templateUrl: './adm006-form.component.html', 
  styleUrls: ['./adm006-form.component.scss']
})
export class Adm006FormComponent implements OnInit {
 
  form = this.fb.group({ 
    'userId':['']   
    , 'email':['',Validators.required]  
    , 'citizenId':['',Validators.required]  
    , 'title':['',Validators.required]  
    , 'firstName':['',Validators.required]  
    , 'lastName':['',Validators.required]  
    , 'phoneNo':['']  
    , 'roleId':['',Validators.required]   
    , 'newpw':['']    
    , 'confirmpw':['']  
    , 'userType':['']  
    , 'usrName':['']  
    , 'groupUser':['']  
  });  

  formInsert = this.fb.group({ 
    'email':['',Validators.required]
    , 'citizenId':[''] 
    , 'title':['']  
    , 'firstName':['']   
    , 'lastName':['']  
    , 'phoneNo':['']  
    , 'roleId':['',Validators.required]  
    , 'userType':['',Validators.required]  
    , 'usrName':['',Validators.required]  
    , 'password':['',Validators.required]  
  });  

  formEditPW = this.fb.group({ 
    'userId':['']  
    , 'email':['']
    , 'citizenId':[''] 
    , 'title':['']  
    , 'firstName':['']   
    , 'lastName':['']  
    , 'phoneNo':['']  
    , 'roleId':['']  
    , 'userType':['']  
    , 'usrName':['']  
    , 'password':['']  
  });  
  
  updatedUser = '';
  updatedDate: Date;
  modeType = ''; 
  modeTypeShow = '';
  id:any;
  email:any;
  validateValue = false;
  titleList:[];
  roleList:[];
  roleListEdit:[];
  teacherList:[];

  checkGetTeacher = false;
  checkValidateInsertPW = false;
  headerType = '';

  constructor( 
    private translate: TranslateService,
    ar:ActivatedRoute,
    private fb: FormBuilder, 
    private service:ApiService, 
    private router: Router, 
    public util: UtilService 
  ) {
    this.modeType = ar.snapshot.queryParamMap.get('modeType')||'';
    if(this.modeType == 'registerlistEdit'){
      this.id = ar.snapshot.queryParamMap.get('id')||'';
      this.modeTypeShow = 'แก้ไขข้อมูลรายชื่อผู้ลงทะเบียน';
      this.headerType = this.translate.instant('modeType.MT0008');
      this.translate.onLangChange.subscribe(() => {
        this.headerType = this.translate.instant('modeType.MT0008');
      });
      this.form.controls.citizenId.disable(); 
      this.form.controls.email.disable();
      this.loadData();
    }else{
      this.form.controls.groupUser.setValue("EXT");
      this.modeTypeShow = 'เพิ่มข้อมูลรายชื่อผู้ลงทะเบียน';
      this.headerType = this.translate.instant('modeType.MT0007');
      this.translate.onLangChange.subscribe(() => {
        this.headerType = this.translate.instant('modeType.MT0007');
      });
    }
    this.ddlTbMstTitle();
    this.ddlRoleGroup();
    this.ddlRoleEdit(); 
  }

  ngOnInit(){} 

  loadData(){
    this.service.httpGet('/api/v1/admUser/'+this.id,null).then((res:IResponse)=>{
      let data = res.responseData;
      this.form.controls.userId.setValue(data.userId||'');
      this.form.controls.email.setValue(data.email||'');
      this.form.controls.citizenId.setValue(data.citizenId||'');
      this.form.controls.title.setValue(data.title||'');
      this.form.controls.firstName.setValue(data.firstName||'');
      this.form.controls.lastName.setValue(data.lastName||'');
      this.form.controls.phoneNo.setValue(data.phoneNo||'');
      this.form.controls.roleId.setValue(data.roleId||'');
      this.form.controls.userType.setValue(data.userType||'');
      this.form.controls.usrName.setValue(data.usrName||'');
      this.updatedUser = localStorage.getItem('userName')||''; 
      this.updatedDate = data.userUpdatedDate;
      this.form.controls.newpw.setValue('');
      this.form.controls.confirmpw.setValue('');
    });
  } 

  onBackPage(){
    this.router.navigate(['/adm/adm006']);
  }
 
  ddlTbMstTitle(){
    this.service.httpGet('/api/v1/0/tbMstTitle/ddl',null).then((res)=>{
        this.titleList = res||[];
    });
  }

  ddlRoleGroup(){
    let json = {'roleType':this.form.value.groupUser};
    this.service.httpGet('/api/v1/0/adm-role/ddl',json).then((res)=>{
        this.roleList = res||[];
    });
  }

  ddlRoleEdit(){
    this.service.httpGet('/api/v1/0/adm-role/ddlAll',null).then((res)=>{
        this.roleListEdit = res||[];
    });
  }

  roleCode(){
    let ele = this.roleList.filter((o)=>{ 
      return o['roleId'] == this.form.value.roleId;
    });
    this.form.controls.userType.setValue(ele[0]['roleCode']);
  }

  roleCodeEdit(){
    let ele = this.roleListEdit.filter((o)=>{ 
      return o['roleId'] == this.form.value.roleId;
    });
    this.form.controls.userType.setValue(ele[0]['roleCode']);
  }

  numberOnly(event): boolean {
    const charCode = (event.which) ? event.which : event.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      return false;
    }
    return true;
  } 

  getTeacher(){
    if(this.form.value.usrName.length == 8){
      let json = {'teacherCardId':this.form.value.usrName};
      this.service.httpGet('/api/v1/0/tbTeacherProfile/register',json).then((res)=>{
          this.teacherList = res||[];
          if(this.teacherList.length == 0){ 
            this.checkGetTeacher = false;
            Swal.fire(
              this.translate.instant('message.teccard_error'), 
              this.translate.instant('message.teccard_error_detail1')+' '+'<b>'+this.form.value.usrName+'</b>'+' '+this.translate.instant('message.teccard_error_detail2'),
              'warning'
            ).then(()=>
                this.form.controls.usrName.setValue(''),
                this.form.controls.citizenId.setValue(''),
                this.form.controls.title.setValue(''),
                this.form.controls.firstName.setValue(''),
                this.form.controls.lastName.setValue(''),
                this.form.controls.email.setValue('')
            )
          }else{
            this.checkGetTeacher = true;
            this.form.controls.citizenId.setValue(res[0]['citizenId']);
            this.form.controls.title.setValue(res[0]['titleId']);
            this.form.controls.firstName.setValue(res[0]['firstnameTh']);
            this.form.controls.lastName.setValue(res[0]['lastnameTh']);
            this.form.controls.phoneNo.setValue(res[0]['tel1']);
            this.form.controls.email.setValue(res[0]['email']);
            this.form.controls.userType.setValue('');
          }
      });
    }else{
      this.form.controls.usrName.setValue('');
      this.form.controls.citizenId.setValue('');
      this.form.controls.title.setValue('');
      this.form.controls.firstName.setValue('');
      this.form.controls.lastName.setValue('');
      this.form.controls.email.setValue('');
    }
  }

  onCheckpw(){
    if(this.form.value.newpw.length == 8 && this.form.value.confirmpw.length){
      if(this.form.value.newpw != this.form.value.confirmpw){
        Swal.fire(
          this.translate.instant('message.pw_error'),
          this.translate.instant('message.pw_notmatch'),
          'warning'
        )
        this.form.get('newpw').setValue('');
        this.form.get('confirmpw').setValue('');
      }else{
        return true;
      }
    }
  }
  
  onSave(){
    if(this.modeType == 'registerlistEdit'){ 
      if (this.form.invalid) {
        this.validateValue = true;
        Swal.fire(
          this.translate.instant('alert.error'),
          this.translate.instant('alert.validate'),
          'error'
        )
        return;
      }Swal.fire({
          title: this.translate.instant('psms.DL0001'),
          icon : 'info',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          confirmButtonText: this.translate.instant('psms.DL0008'),
          cancelButtonText: this.translate.instant('psms.DL0009')
      }).then((result) => {
        if(result.dismiss == 'cancel'){
            return;
        }
        if(this.form.value.newpw == '' && this.form.value.confirmpw == ''){
          let json = this.form.value;
          this.service.httpPut('/api/v1/admUser/updated',json).then((res:IResponse)=>{
            if ((res.responseCode || 500) != 200) {
              Swal.fire(
                this.translate.instant('message.edit_error'),
                res.responseMsg,
                'error'
              )
              return;
            }
            Swal.fire(
              this.translate.instant('psms.DL0006'),
              this.translate.instant('message.edit'),
              'success'
            ).then(() => {this.onBackPage();})
          });
        }else{
          this.formEditPW.get('userId').setValue(this.form.value.userId);
          this.formEditPW.get('email').setValue(this.form.value.email);
          this.formEditPW.get('citizenId').setValue(this.form.value.citizenId);
          this.formEditPW.get('title').setValue(this.form.value.title);
          this.formEditPW.get('firstName').setValue(this.form.value.firstName);
          this.formEditPW.get('lastName').setValue(this.form.value.lastName);
          this.formEditPW.get('phoneNo').setValue(this.form.value.phoneNo);
          this.formEditPW.get('roleId').setValue(this.form.value.roleId);
          this.formEditPW.get('userType').setValue(this.form.value.userType);
          this.formEditPW.get('password').setValue(this.form.value.confirmpw);
          this.formEditPW.get('usrName').setValue(this.form.value.usrName);

          let json = this.formEditPW.value;
          this.service.httpPut('/api/v1/admUser/updatedPW',json).then((res:IResponse)=>{
            if ((res.responseCode || 500) != 200) {
              Swal.fire(
                this.translate.instant('message.edit_error'),
                res.responseMsg,
                'error'
              )
              return;
            }
            Swal.fire(
              this.translate.instant('psms.DL0006'),
              this.translate.instant('message.edit'),
              'success'
            ).then(() => {this.onBackPage();})
          });
        }
      });
    }else if(this.modeType == 'registerlistAdd'){
      if(this.form.value.newpw.length != 8 && this.form.value.confirmpw.length != 8){
        this.checkValidateInsertPW = true;
        Swal.fire(
          this.translate.instant('message.pw_error'),
          this.translate.instant('message.pw_notmatch'),
          'warning'
        )
      }else{
        this.formInsert.get('email').setValue(this.form.value.email);
        this.formInsert.get('citizenId').setValue(this.form.value.citizenId);
        this.formInsert.get('title').setValue(this.form.value.title);
        this.formInsert.get('firstName').setValue(this.form.value.firstName);
        this.formInsert.get('lastName').setValue(this.form.value.lastName);
        this.formInsert.get('phoneNo').setValue(this.form.value.phoneNo);
        this.formInsert.get('roleId').setValue(this.form.value.roleId);
        this.formInsert.get('userType').setValue(this.form.value.userType);
        this.formInsert.get('password').setValue(this.form.value.confirmpw);
        this.formInsert.get('usrName').setValue(this.form.value.usrName);

        if (this.formInsert.invalid){
          this.validateValue = true;
          Swal.fire(
            this.translate.instant('alert.error'),
            this.translate.instant('alert.validate'),
            'error'
          )
          return;
        }
        Swal.fire({
          title: this.translate.instant('psms.DL0001'),
          icon : 'info',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          confirmButtonText: this.translate.instant('psms.DL0008'),
          cancelButtonText: this.translate.instant('psms.DL0009')
        }).then((result) => {
          if(result.dismiss == 'cancel'){
              return;
          } 
          let json = this.formInsert.value;
          this.service.httpPost('/api/v1/admUser',json).then((res:IResponse)=>{
            if ((res.responseCode || 500) != 200) {
              Swal.fire(
                this.translate.instant('message.add_error'),
                res.responseMsg,
                'error'
              );
              return;
            }
            Swal.fire(
              this.translate.instant('alert.add_header'),
              this.translate.instant('message.add'),
              'success'
            ).then(() => {this.onBackPage();})
          });
        });
      }
    }
  }

  onCancel(){
    this.form.controls.email.setValue('');
    this.form.controls.citizenId.setValue('');
    this.form.controls.title.setValue('');
    this.form.controls.firstName.setValue('');
    this.form.controls.lastName.setValue('');
    this.form.controls.phoneNo.setValue('');
    this.form.controls.newpw.setValue('');
    this.form.controls.confirmpw.setValue('');
    this.form.controls.usrName.setValue('');
    this.validateValue = false;
  }

}
 