import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ApiService, IResponse } from 'src/app/shared/service/api.service';
import { FormBuilder, Validators } from '@angular/forms';
import { PageGroupDdlComponent } from 'src/app/dialog/page-group-ddl/page-group-ddl.component';
import { MatDialog } from '@angular/material';
import { TranslateService } from '@ngx-translate/core';
import Swal from 'sweetalert2/dist/sweetalert2.js';
import 'sweetalert2/src/sweetalert2.scss';

@Component({
  selector: 'app-adm003-form',
  templateUrl: './adm003-form.component.html',
  styleUrls: ['./adm003-form.component.scss']
})
export class Adm003FormComponent implements OnInit {

  isProcess:boolean = false;
  form = this.fb.group({
    'pageId':[''] 
    , 'pageIcon':[''] 
    , 'pageGroupCode':['',Validators.required]
    , 'pageGroupName':['',Validators.required]
    , 'pageCode':['',Validators.required]
    , 'pageName':['',Validators.required] 
    , 'pageNameEn':['',Validators.required]
    , 'pageUrl':['',Validators.required]
  });

    id = '';
    validateValue = false;
    headerType = '';

    constructor(private translate: TranslateService,ar:ActivatedRoute,public dialog: MatDialog,private fb: FormBuilder,private service:ApiService,private router: Router) { 
      this.isProcess = true;
      this.id = ar.snapshot.queryParamMap.get('id')||'';
      if(this.id == ''){
        this.headerType = this.translate.instant('modeType.MT0007');
        this.translate.onLangChange.subscribe(() => {
          this.headerType = this.translate.instant('modeType.MT0007');
        });
      }else{
        this.headerType = this.translate.instant('modeType.MT0008');
        this.translate.onLangChange.subscribe(() => {
          this.headerType = this.translate.instant('modeType.MT0008');
        });
        this.loadData()
      }
  }

  ngOnInit() {
  }

  loadData(){
    this.isProcess = true;
    this.service.httpGet('/api/v1/page/'+this.id,null).then((res:IResponse)=>{
      this.isProcess = false;
      let data = res.responseData;

      this.form.controls.pageId.setValue(data.pageId||'');
      this.form.controls.pageCode.setValue(data.pageCode||'');
      this.form.controls.pageIcon.setValue(data.pageIcon||'');
      this.form.controls.pageName.setValue(data.pageName||'');
      this.form.controls.pageNameEn.setValue(data.pageNameEn||'');
      this.form.controls.pageUrl.setValue(data.pageUrl||'');
      
      this.form.controls.pageGroupCode.setValue(data.pageGroup.pageGroupCode||'');
      this.form.controls.pageGroupName.setValue(data.pageGroup.pageGroupName||'');
    });
  }

  onClearPageGroup(){
    this.form.controls.pageGroupCode.setValue('');
    this.form.controls.pageGroupName.setValue('');
  }

  ddlPageGroup(): void {
    const dialogRef = this.dialog.open(PageGroupDdlComponent, {
      width: '60%'
      , 'autoFocus': true
      , 'disableClose':true
      , data:{'pageGroupCode':this.form.value.pageGroupCode||''}
    });
    dialogRef.afterClosed().subscribe(result => {
      if((result||'') == ''){
        return;
      }
      this.form.controls.pageGroupCode.setValue(result.pageGroupCode||'');
      this.form.controls.pageGroupName.setValue(result.pageGroupName||'');
    });
  }

  onBackPage(){
    this.router.navigate(['/adm/adm003']);
  }

  onSave(){ 
    if (this.form.invalid) {
      this.validateValue = true;
      Swal.fire(
        this.translate.instant('message.add_error'),//เพิ่มข้อมูลผิดพลาด
        this.translate.instant('alert.validate'),//กรุณาระบุข้อมูล ให้ครบ. 
        'error'
      )
      return;
    }

    Swal.fire({
      title: this.translate.instant('alert.save'),//ต้องการบันทึกข้อมูล ใช่หรือไม่
      icon : 'info',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: this.translate.instant('psms.DL0008'),//ใช่
      cancelButtonText: this.translate.instant('psms.DL0009')//ไม่
    }).then((result) => {

      if(result.dismiss == 'cancel'){
        return;
      }
      this.isProcess = true;
      let json = this.form.value;
      if((json.pageId||'') == ''){
        this.service.httpPost('/api/v1/page',json).then((res:IResponse)=>{
          this.isProcess = false;
          if((res.responseCode||500)!=200){
            Swal.fire(
              this.translate.instant('message.add_error'),//เพิ่มข้อมูลผิดพลาด
              res.responseMsg,
              'error' 
            )
            return;
          }Swal.fire(
            this.translate.instant('alert.add_header'),//เพิ่มข้อมูล
            this.translate.instant('message.add'),//เพิ่มข้อมูลสำเร็จ
            'success',
          ).then(() => {this.onBackPage();})
        });
        return;
      }
      this.service.httpPut('/api/v1/page',json).then((res:IResponse)=>{
        this.isProcess = false;
        if((res.responseCode||500)!=200){
          Swal.fire(
            this.translate.instant('message.edit_error'),//แก้ไขข้อมูลผิดพลาด
            res.responseMsg,
            'error' 
          )
          return;
        }Swal.fire(
          this.translate.instant('psms.DL0006'),//แก้ไขข้อมูล
          this.translate.instant('psms.DL0007'),//แก้ไขข้อมูลสำเร็จ
          'success',
        ).then(() => {this.onBackPage();})
      });
    });
  }

  onCancel(){
    this.form.controls.pageIcon.setValue('');
    this.form.controls.pageGroupName.setValue('');
    this.form.controls.pageGroupCode.setValue('');
    this.form.controls.pageCode.setValue('');
    this.form.controls.pageName.setValue('');
    this.form.controls.pageNameEn.setValue('');
    this.form.controls.pageUrl.setValue('');
    this.loadData();
  }
}
