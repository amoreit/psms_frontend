import { Component, OnInit, ViewChild} from '@angular/core';
import { FormBuilder} from '@angular/forms';
import { MatPaginator, MatTableDataSource, MatDialog } from '@angular/material';
import { ApiService,IResponse} from '../../../shared/service/api.service';
import { Router } from '@angular/router';
import { PageGroupDdlComponent } from 'src/app/dialog/page-group-ddl/page-group-ddl.component';
import { TranslateService } from '@ngx-translate/core';
import Swal from 'sweetalert2/dist/sweetalert2.js';
import 'sweetalert2/src/sweetalert2.scss';

@Component({
  selector: 'app-adm003',
  templateUrl: './adm003.component.html',
  styleUrls: ['./adm003.component.scss']
})
export class Adm003Component implements OnInit {

  isProcess:boolean = false;

  searchForm = this.fb.group({
      'pageCode':['']
      , 'pageGroupCode':['']
    , 'pageGroupName':['']
    , 'pageName':['']
    , 'p':['']
    , 'result':[10]
  });

  pageSize:number;
  displayedColumns: string[] = ['pageCode','pageName','pageGroupName', 'pageStatus'];
  dataSource = new MatTableDataSource();

  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  constructor(private translate: TranslateService,private fb: FormBuilder,private service:ApiService,public dialog: MatDialog,private router: Router) {
    
  }

  ngOnInit() {
    this.onSearch(0);
  }

  onSearch(e){
    this.isProcess = true;
    this.searchForm.value.p = (this.paginator.pageIndex = e.pageIndex||0)+1;
    this.service.httpGet('/api/v1/page',this.searchForm.value).then((res:IResponse)=>{
      this.isProcess = false;
      this.dataSource.data = res.responseData||[];
      this.pageSize = res.responseSize||0;
    });
  }

  onCancel(){ 
    this.searchForm.controls.pageCode.setValue('');
    this.searchForm.controls.pageGroupCode.setValue('');
    this.searchForm.controls.pageGroupName.setValue('');
    this.searchForm.controls.pageName.setValue('');
    this.onSearch(0);
  }

  onClearPageGroup(){
    this.searchForm.controls.pageGroupCode.setValue('');
    this.searchForm.controls.pageGroupName.setValue('');
  }

  ddlPageGroup(): void {
    const dialogRef = this.dialog.open(PageGroupDdlComponent, {
      width: '60%'
      , 'autoFocus': true
      , 'disableClose':true
      , data:{'pageGroupCode':this.searchForm.value.pageGroupCode||''}
    });
    dialogRef.afterClosed().subscribe(result => {
      if((result||'') == ''){
        return;
      }
      this.searchForm.controls.pageGroupCode.setValue(result.pageGroupCode||'');
      this.searchForm.controls.pageGroupName.setValue(result.pageGroupName||'');
    });
  }

  onDelete(e){
    Swal.fire({
        title: this.translate.instant('alert.delete'),//ต้องการลบข้อมูล ใช่หรือไม่
        icon : 'info',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: this.translate.instant('psms.DL0008'),//ใช่
        cancelButtonText: this.translate.instant('psms.DL0009')//ไม่
      }).then((result) => {
      if(result.dismiss == 'cancel'){
        return; 
      }
      this.isProcess = true;
      this.service.httpDelete('/api/v1/page',{'pageId':e.pageId}).then((res:IResponse)=>{
        this.isProcess = false;
        if((res.responseCode||500)!=200){
          Swal.fire(
            this.translate.instant('message.delete_error'),//ลบข้อมูลผิดพลาด
            res.responseMsg,
            'error' 
          )
          return;
        }
        Swal.fire(
          this.translate.instant('psms.B0024'),//ลบข้อมูล
          this.translate.instant('message.delete'),//ลบข้อมูลสำเร็จ
          'success' 
      )});
    });
  }
  onReport(){
      this.service.httpPreviewPDF('/api/v1/page/report/pdf',this.searchForm.value).then((res)=>{
      this.isProcess = false;
      window.location.href = res;
    });
  
  }

  onReportPDF(){
    this.isProcess = true;
    this.service.httpReportPDF('/api/v1/page/report/pdf',"adm003",this.searchForm.value).then((res)=>{
      this.isProcess = false;
    });
  }

  onReportXls(){
    this.isProcess = true;
    this.service.httpReportXLS('/api/v1/page/report/xls',"adm003",this.searchForm.value).then((res)=>{
      this.isProcess = false;
    });
  }

  onForm(id){
    this.router.navigate(['/adm/adm003/form'],{ queryParams: {id:id}});
  }

}