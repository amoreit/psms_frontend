import { Component, OnInit, ViewChild, Inject } from '@angular/core';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { MatPaginator, MatTableDataSource, MAT_DIALOG_DATA, MatDialog } from '@angular/material';
import { ApiService, IResponse, EHttp } from '../../../shared/service/api.service';
import { TranslateService } from '@ngx-translate/core';

import Swal from 'sweetalert2/dist/sweetalert2.js';
import 'sweetalert2/src/sweetalert2.scss';
import { UtilService } from 'src/app/_util/util.service';
import { isArray } from 'util';
import { filter } from 'rxjs/operators';


@Component({
  selector: 'app-adm004',
  templateUrl: './adm004.component.html',
  styleUrls: ['./adm004.component.scss']
})
export class Adm004Component implements OnInit {
 
  isProcess: boolean = false;

  searchForm = this.fb.group({
    'roleId': ['']
  });
  displayedColumns: string[] = ['pageName','pageNameEn', 'pageGroupName','pageGroupNameEn','isView'];
  dataSource = new MatTableDataSource();
  roleList: [];
  labelTabs=[];
  pageSize: any;

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  constructor(public translate: TranslateService, private fb: FormBuilder, private service: ApiService, public dialog: MatDialog) {
  }

  ngOnInit() {
    this.ddlRole();
    this.onSearch();
    this.dataSource.filter = this.labelTabs[0];
    this.translate.onLangChange.subscribe(() => {this.onlabel()})
  }

  onSearch() {
    let id = this.searchForm.value.roleId || 0;
    if (id == '') {
      this.pageSize = 0;
      return;
    }
    this.isProcess = true;
    this.service.httpGet('/api/v1/manage-page/' + id, null).then((res: IResponse) => {
      this.isProcess = false;
      let data = res.responseData || [];
      this.dataSource.data = data;
      this.onlabel();
      this.pageSize = data.length;
    });
  }

  ddlRole() {
    this.service.httpGet('/api/v1/0/role/ddl', null).then((res) => {
      this.roleList = res || [];
    });
  }

  onCancel() {
    this.searchForm = this.fb.group({
      'roleId': ['']
    });
    this.dataSource.data = []
    this.pageSize = 0; 
  }
  onlabel(){
    let x = []
    for (let index = 0; index < this.dataSource.data.length; index++) {
      if(this.translate.currentLang == "th"){
        x.push(this.dataSource.data[index]['pageGroupName'])
      }
      else if (this.translate.currentLang == "en"){
      
        x.push(this.dataSource.data[index]['pageGroupNameEn'])
      }
    let data = [...new Set(x)]
    this.labelTabs = data
    }
  }
  onfilter(click){
    for (let index = 0; index < this.labelTabs.length; index++) {
        if(click.index==index){
          this.dataSource.filter = this.labelTabs[index]
        }
    }
  }
  onActiveAll(o) {
    this.dataSource.data.map(e => {
      e['isView'] = o;
      return e;
    });
  }

  onSave() {
    let arr = this.dataSource.data;
    if (arr.length == 0) {
      Swal.fire(
        this.translate.instant('alert.error'),
        this.translate.instant('alert.validate'),
        'error'
      );
      return;
    }

    Swal.fire({
      title: this.translate.instant('psms.DL0001'),
      icon: 'info',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: this.translate.instant('psms.DL0008'),
      cancelButtonText: this.translate.instant('psms.DL0009')
    }).then((result) => {

      if (result.dismiss == 'cancel') {
        return;
      }
      this.isProcess = true;
      let id = this.searchForm.value.roleId || '';
      this.service.httpPost('/api/v1/manage-page/' + id, arr).then((res: IResponse) => {
        this.isProcess = false;
        if ((res.responseCode || 500) != 200) {
          Swal.fire(
            this.translate.instant('message.add_error'),
            res.responseMsg,
            'error'
          );
          return;
        }
        Swal.fire(
          this.translate.instant('alert.add_header'),
          this.translate.instant('message.add'),
          'success'
        )
      });
    });
  }

  onReport() {

  }
  onReportPDF() {

  }
  onReportXls() {

  }

  onActive(data, ele, name) {
    data[name] = (ele.checked) ? 'Y' : 'N';
  }

}
