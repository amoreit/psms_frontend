import { Routes } from '@angular/router';
import { AuthGuard } from '../../shared/service/auth.guard';

/*** Page ***/
import {Adm001Component} from './adm001/adm001.component';
import {Adm001FormComponent} from './adm001/adm001-form/adm001-form.component';

import {Adm002Component} from './adm002/adm002.component';
import {Adm002FormComponent} from './adm002/adm002-form/adm002-form.component';

import {Adm003Component} from './adm003/adm003.component';
import {Adm003FormComponent} from './adm003/adm003-form/adm003-form.component';

import {Adm004Component} from './adm004/adm004.component';
import { Adm006Component } from './adm006/adm006.component';
import { Adm006FormComponent } from './adm006/adm006-form/adm006-form.component';
import { Adm007Component } from './adm007/adm007.component';
import { ProfileUserComponent } from './profile-user/profile-user.component';


export const AdmRoutes: Routes = [
  {
    path:'',
    children:[
      {path: 'adm001',component: Adm001Component,canActivate:[AuthGuard]},
      {path: 'adm001/form',component: Adm001FormComponent,canActivate:[AuthGuard]},
      {path: 'adm002',component: Adm002Component,canActivate:[AuthGuard]},
      {path: 'adm002/form',component: Adm002FormComponent,canActivate:[AuthGuard]},
      {path: 'adm003',component: Adm003Component,canActivate:[AuthGuard]},
      {path: 'adm003/form',component: Adm003FormComponent,canActivate:[AuthGuard]},
      {path: 'adm004',component: Adm004Component,canActivate:[AuthGuard]},
      {path: 'adm006',component: Adm006Component,canActivate:[AuthGuard]},
      {path: 'adm006/form',component: Adm006FormComponent,canActivate:[AuthGuard]},
      {path: 'adm007',component: Adm007Component,canActivate:[AuthGuard]},
      {path: 'profile-user',component: ProfileUserComponent,canActivate:[AuthGuard]},
    ]
  }
];