import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FlexLayoutModule } from '@angular/flex-layout';
import { ReactiveFormsModule,FormsModule } from '@angular/forms';
import { MatTabsModule } from '@angular/material/tabs';

/*** Material ***/
import {
  MatExpansionModule,
  MatButtonModule,
  MatIconModule,
  MatInputModule,
  MatTableModule,
  MatCardModule,
  MatFormFieldModule,
  MatPaginatorModule,
  MatDialogModule,
  MatSlideToggleModule,
  MatProgressSpinnerModule,
} from '@angular/material';

/*** Routing ***/
import {AdmRoutes} from './adm.routing';

/*** Page ***/
import {Adm001Component} from './adm001/adm001.component';
import {Adm002Component} from './adm002/adm002.component';
import {Adm003Component} from './adm003/adm003.component';
import {Adm004Component} from './adm004/adm004.component';
import { Adm001FormComponent } from './adm001/adm001-form/adm001-form.component';
import { Adm002FormComponent } from './adm002/adm002-form/adm002-form.component';
import { Adm003FormComponent } from './adm003/adm003-form/adm003-form.component';
import { Adm006Component } from './adm006/adm006.component';
import { Adm006FormComponent } from './adm006/adm006-form/adm006-form.component';
import { Adm007Component } from './adm007/adm007.component';
import { TranslateModule } from '@ngx-translate/core';
import { ProfileUserComponent } from './profile-user/profile-user.component';

@NgModule({
    imports: [
      CommonModule,
      FlexLayoutModule,
      TranslateModule,
      RouterModule.forChild(AdmRoutes), 
      MatExpansionModule,
      MatButtonModule,
      MatIconModule,
      MatInputModule,
      MatTableModule,
      MatCardModule,
      MatFormFieldModule,
      ReactiveFormsModule,
      FormsModule,
      MatPaginatorModule,
      MatDialogModule,
      MatSlideToggleModule,
      MatProgressSpinnerModule,
      MatTabsModule
    ],
    providers: [],
    exports:[RouterModule],
    declarations: [
      /*** Page ***/
      Adm001Component,
      Adm002Component,
      Adm003Component,
      Adm004Component,
      Adm006Component,

      /*** Dialog ***/
      Adm001FormComponent,
      Adm002FormComponent,
      Adm003FormComponent,
      Adm006FormComponent,
      ProfileUserComponent,
      Adm007Component,
    ],
    entryComponents:[
    ]
  })
export class AdmModule {}
