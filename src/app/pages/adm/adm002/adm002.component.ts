import { Component, OnInit, ViewChild, Inject } from '@angular/core';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { MatPaginator, MatTableDataSource, MAT_DIALOG_DATA, MatDialog } from '@angular/material';
import { ApiService,IResponse,EHttp } from '../../../shared/service/api.service';
import { Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';

// sweetalert2
import Swal from 'sweetalert2/dist/sweetalert2.js';
import 'sweetalert2/src/sweetalert2.scss';
import { UtilService } from 'src/app/_util/util.service';
// sweetalert2

@Component({
  selector: 'app-adm002',
  templateUrl: './adm002.component.html',
  styleUrls: ['./adm002.component.scss']
})
export class Adm002Component implements OnInit {

  isProcess:boolean = false;
  searchForm = this.fb.group({
    'pageGroupCode':['']
    , 'pageGroupName':['']
    , 'p':['']
    , 'result':[10]
  });
  pageSize:number;
  displayedColumns: string[] = ['pageGroupIndex','pageGroupCode','pageGroupName', 'pageGroupStatus'];
  dataSource = new MatTableDataSource();

  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  constructor(private translate: TranslateService,private fb: FormBuilder,private service:ApiService,public dialog: MatDialog,private router: Router) {
    
  }

  ngOnInit() {
    this.onSearch(0);
  }

  onSearch(e){
    this.isProcess = true;
    this.searchForm.value.p = (this.paginator.pageIndex = e.pageIndex||0)+1;
    this.service.httpGet('/api/v1/page-group',this.searchForm.value).then((res:IResponse)=>{
      this.isProcess = false;
      this.dataSource.data = res.responseData||[];
      this.pageSize = res.responseSize||0;
    });
  }

  onCancel(){
    this.searchForm.controls.pageGroupCode.setValue('');
    this.searchForm.controls.pageGroupName.setValue('');
    this.onSearch(0);
  }

  onDelete(e){
    Swal.fire({
      title: this.translate.instant('alert.delete'),//ต้องการลบข้อมูล ใช่หรือไม่
      icon: 'info',
      showCancelButton: true,
      confirmButtonColor: '#2085d6',
      cancelButtonVolor: '#d33',
      confirmButtonText: this.translate.instant('psms.DL0008'),//ใช่
      cancelButtonText: this.translate.instant('psms.DL0009')//ไม่
    }).then((result) => {
      if(result.dismiss == 'cancel'){
        return;
      }
      this.isProcess = true;
      this.service.httpDelete('/api/v1/page-group',{'pageGroupId':e.pageGroupId}).then((res:IResponse)=>{
        this.isProcess = false;
        if((res.responseCode||500)!=200){
          alert(res.responseMsg);
          return; 
        }
        Swal.fire(
          this.translate.instant('psms.B0024'),//ลบข้อมูล
          this.translate.instant('message.delete'),//ลบข้อมูลสำเร็จ
          'success'
        )
        this.onSearch(0);
      });
    });
    
  }

  // onRevert(e){
  //   if(confirm(this.translate.instant('alert.revert'))) {
  //     this.isProcess = true;
  //     this.service.httpPut('/api/v1/page-group',{'pageGroupId':e.pageGroupId}).then((res:IResponse)=>{
  //       this.isProcess = false;
  //       if((res.responseCode||500)!=200){
  //         Swal.fire(
  //           this.translate.instant('message.delete_error'),//ลบข้อมูลผิดพลาด
  //           res.responseMsg,
  //           'error' 
  //         )
  //         return;
  //       }
  //       alert(this.translate.instant('message.revert'));
  //       this.onSearch(0);
  //     });
  //   }
  // }

  onReport(){
    this.isProcess = true;
    this.service.httpPreviewPDF('/api/v1/page-group/report/pdf',this.searchForm.value).then((res)=>{
      this.isProcess = false;
      window.location.href = res;
    });
  }

  onReportPDF(){
    this.isProcess = true;
    this.service.httpReportPDF('/api/v1/page-group/report/pdf',"adm002",this.searchForm.value).then((res)=>{
      this.isProcess = false;
    });
  }

  onReportXls(){
    this.isProcess = true;
    this.service.httpReportXLS('/api/v1/page-group/report/xls',"adm002",this.searchForm.value).then((res)=>{
      this.isProcess = false;
    });
  }

  onForm(id){
    this.router.navigate(['/adm/adm002/form'],{ queryParams: {id:id}});
  }
}