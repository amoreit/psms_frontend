import { Component, OnInit } from '@angular/core';
import Swal from 'sweetalert2/dist/sweetalert2.js';
import 'sweetalert2/src/sweetalert2.scss';
import { ActivatedRoute, Router } from '@angular/router';
import { FormBuilder, Validators } from '@angular/forms';
import { ApiService, IResponse } from 'src/app/shared/service/api.service';
import { UtilService } from 'src/app/_util/util.service';
import * as constants from '../../../constants';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-profile-user', 
  templateUrl: './profile-user.component.html',
  styleUrls: ['./profile-user.component.scss']
})
export class ProfileUserComponent implements OnInit {
 
  form = this.fb.group({  
    'userId':['']  
    , 'email':['']
    , 'citizenId':[''] 
    , 'title':['']  
    , 'firstName':['']  
    , 'lastName':['']  
    , 'phoneNo':['']  
    , 'newpw':['']   
    , 'confirmpw':['']  
    , 'usrName':[''] 
    , 'photoPath':[''] 
  });

  formPW = this.fb.group({ 
    'userId':['']  
    , 'email':['']
    , 'citizenId':[''] 
    , 'title':['']  
    , 'firstName':['']  
    , 'lastName':['']  
    , 'phoneNo':['']  
    , 'password':['']  
    , 'usrName':['']
    , 'photoPath':[''] 
  });

  updatedUser = '';
  updatedDate: Date;
  id:any;
  email:any; 
  usrName:any;
  titleList:[];
  urls = new Array<string>();
  show_image:any;

  constructor(
    private translate: TranslateService,
    ar:ActivatedRoute,
    private fb: FormBuilder, 
    private service:ApiService,
    public util: UtilService 
  ) { }

  ngOnInit() {
    this.urls = ['../assets/img/profile/user-no-img.jpg'];
    this.email = localStorage.getItem('email')||''; 
    this.usrName = localStorage.getItem('usrName')||''; 
    this.ddlTbMstTitle();
    this.loadUser(); 
  }

  loadUser(){ 
    let profileuser = {'usrName':this.usrName};
    this.service.httpGet('/api/v1/admUser/usermenu',profileuser).then((res:IResponse)=>{
      let data = res.responseData;
      this.form.controls.userId.setValue(data[0]['userId']);
      this.form.controls.email.setValue(data[0]['email']);
      this.form.controls.citizenId.setValue(data[0]['citizenId']);
      this.form.controls.title.setValue(data[0]['title']);
      this.form.controls.firstName.setValue(data[0]['firstName']);
      this.form.controls.lastName.setValue(data[0]['lastName']);
      this.form.controls.phoneNo.setValue(data[0]['phoneNo']);
      this.form.controls.usrName.setValue(data[0]['usrName']);
      this.form.controls.photoPath.setValue(data[0]['photoPath']);
      this.updatedUser = localStorage.getItem('userName')||''; 
      this.updatedDate = new Date();
      this.form.controls.newpw.setValue('');
      this.form.controls.confirmpw.setValue('');
 
      if(this.form.value.photoPath == null || this.form.value.photoPath == '' || this.form.value.photoPath == undefined){
        this.urls = ['../assets/img/profile/user-no-img.jpg'];
      }else {
        this.urls = [constants.SERVICE_API + '/api/v1/attachmentteacher/viewUser/' + this.form.value.photoPath];
        this.show_image = this.form.value.photoPath.substring(14);
      }
    });
  }

  onImage(event){
    const selectedFile = event.target.files[0];
    this.show_image = selectedFile.name;
    const uploadData = new FormData();
    uploadData.append('file',selectedFile,selectedFile.name);

    this.service.httpPost("/api/v1/attachmentteacher/uploadUser/"+this.form.value.citizenId,uploadData).then((res:IResponse)=>{ 
      if(res.responseCode != 200){
        Swal.fire(
          this.translate.instant('alert.error_pic'),
          res.responseMsg,
          'error'
        )
        return;
      }
      let json = res.responseData;
      this.form.controls.photoPath.setValue(json.attachmentCode+'/'+json.attachmentName);
      this.urls = [constants.SERVICE_API + '/api/v1/attachmentteacher/viewUser/' + this.form.value.photoPath]
    }); 
  }

  numberOnly(event): boolean {
    const charCode = (event.which) ? event.which : event.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      return false;
    }
    return true;
  }
 
  onCheckpw(){
    if(this.form.value.newpw.length == 8 && this.form.value.confirmpw.length){
      if(this.form.value.newpw != this.form.value.confirmpw){
        Swal.fire(
          this.translate.instant('message.pw_error'),
          this.translate.instant('message.pw_notmatch'),
          'warning'
        )
        this.form.get('newpw').setValue('');
        this.form.get('confirmpw').setValue('');
      }else{
        return true;
      }
    }
  }

  ddlTbMstTitle(){
    this.service.httpGet('/api/v1/0/tbMstTitle/ddl',null).then((res)=>{
        this.titleList = res||[];
    });
  }

  onSave(){
    if(this.form.value.newpw == '' && this.form.value.confirmpw == ''){
      Swal.fire({
          title: this.translate.instant('alert.save'),//ต้องการบันทึกข้อมูล ใช่หรือไม่
          icon : 'info',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          confirmButtonText: this.translate.instant('psms.DL0008'),//ใช่
          cancelButtonText: this.translate.instant('psms.DL0009')//ไม่
      }).then((result) => {
        if(result.dismiss == 'cancel'){
            return;
        }
        let json = this.form.value;
        this.service.httpPut('/api/v1/admUser/updated',json).then((res:IResponse)=>{
          if((res.responseCode||500)!=200){
            alert(res.responseMsg);
            return;
          } 
          Swal.fire(
            this.translate.instant('psms.DL0006'),//แก้ไขข้อมูล
            this.translate.instant('psms.DL0007'),//แก้ไขข้อมูลสำเร็จ
            'success'
          ).then(() => {location.reload();})
        });
      });
    }else{
      if(this.form.value.newpw.length != 8 && this.form.value.confirmpw.length != 8){
        Swal.fire(
          this.translate.instant('message.pw_error'),
          this.translate.instant('message.pw_notmatch'),
          'warning'
        )
      }else{
        Swal.fire({
            title: this.translate.instant('alert.save'),//ต้องการบันทึกข้อมูล ใช่หรือไม่
            icon : 'info',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: this.translate.instant('psms.DL0008'),//ใช่
            cancelButtonText: this.translate.instant('psms.DL0009')//ไม่
        }).then((result) => {
          if(result.dismiss == 'cancel'){
              return;
          }
          this.formPW.get('userId').setValue(this.form.value.userId);
          this.formPW.get('email').setValue(this.form.value.email);
          this.formPW.get('citizenId').setValue(this.form.value.citizenId);
          this.formPW.get('title').setValue(this.form.value.title);
          this.formPW.get('firstName').setValue(this.form.value.firstName);
          this.formPW.get('lastName').setValue(this.form.value.lastName);
          this.formPW.get('phoneNo').setValue(this.form.value.phoneNo);
          this.formPW.get('password').setValue(this.form.value.confirmpw);
          this.formPW.get('usrName').setValue(this.form.value.usrName);
          this.formPW.get('photoPath').setValue(this.form.value.photoPath); 

          let json = this.formPW.value;
          this.service.httpPut('/api/v1/admUser/updatedPW',json).then((res:IResponse)=>{
            if((res.responseCode||500)!=200){
              Swal.fire(
                this.translate.instant('message.edit_error'),//แก้ไขข้อมูลผิดพลาด
                res.responseMsg,
                'error'
                );
              return;
            } 
            Swal.fire(
              this.translate.instant('psms.DL0006'),//แก้ไขข้อมูล
              this.translate.instant('psms.DL0007'),//แก้ไขข้อมูลสำเร็จ
              'success'
            ).then(() => {location.reload();})
          });
        });
      }
    }
  }
 
}
