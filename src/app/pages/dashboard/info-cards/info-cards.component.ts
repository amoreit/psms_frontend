import { Component, OnInit, ElementRef, ViewChild } from '@angular/core';
import { AppSettings } from '../../../app.settings';
import { Settings } from '../../../app.settings.model';
import { ApiService, IResponse } from 'src/app/shared/service/api.service';

@Component({
  selector: 'app-info-cards',
  templateUrl: './info-cards.component.html',
  styleUrls: ['./info-cards.component.scss']
})
export class InfoCardsComponent implements OnInit { 
  numberStu = [];
  numberTea =[];
  public colorScheme = {
    domain: ['rgba(255,255,255,0.8)']
  }; 
  public autoScale = true;
  number_stu:any;
  number_male_stu:any;
  number_female_stu:any;
  number_tea:any;
  number_male_tea:any;
  number_female_tea:any;
  @ViewChild('resizedDiv', { static: true }) resizedDiv:ElementRef;
  public previousWidthOfResizedDiv:number = 0; 
  public settings: Settings;
  constructor(public appSettings:AppSettings,private service:ApiService){
    this.settings = this.appSettings.settings; 
  }

  ngOnInit(){
    this.loadNumberStudent()
    this.loadNumberTeacher()
  }

  loadNumberStudent(){
    this.service.httpGet('/api/v1/trn-classroom-member/allnumstuandgender','').then((res:IResponse)=>{   
      this.numberStu = res.responseData||[];
      this.number_stu = this.numberStu[0].num_stu
      this.number_male_stu = this.numberStu[0].male
      this.number_female_stu = this.numberStu[0].female
    });
  }

  loadNumberTeacher(){
    this.service.httpGet('/api/v1/trn-classroom-member/allnumteacherandgender','').then((res:IResponse)=>{   
      this.numberTea = res.responseData||[];
      this.number_tea = this.numberTea[0].num_tea
      this.number_male_tea = this.numberTea[0].male
      this.number_female_tea = this.numberTea[0].female
    });
  }

  
  public onSelect(event) {
  }

}