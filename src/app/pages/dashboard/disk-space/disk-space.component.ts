import { Component, OnInit, ElementRef, ViewChild } from '@angular/core';
import { ApiService, IResponse } from 'src/app/shared/service/api.service';

@Component({
  selector: 'app-disk-space',
  templateUrl: './disk-space.component.html'
})
export class DiskSpaceComponent implements OnInit {
  public showLegend = false;
  public gradient = true;
  public colorScheme = {
    domain: ['#188ae2', '#4bd396', '#3ac9d6', '#f9c851']
  };
  public showLabels = false;
  public explodeSlices = false;
  public doughnut = false;
  numberStu = [];
  single = [];

  constructor(private service: ApiService) { }

  ngOnInit() {
    this.service.httpGet('/api/v1/trn-classroom-member/numstuallcatclasspie', '').then((res: IResponse) => {
      if (res.responseData.length === 0) {
        this.numberStu = []
      } else {
        this.numberStu = res.responseData || [];
        for (let i = 0; i < this.numberStu.length; i++) {
          this.single.push(
            { name: this.numberStu[i].lname_th, value: this.numberStu[i].student }
          )
        }
        this.single = [...this.single];
      }

    });
  }


}