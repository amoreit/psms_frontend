import { Component, OnInit } from '@angular/core';
import { ApiService, IResponse } from 'src/app/shared/service/api.service';

@Component({
  selector: 'app-todo',
  templateUrl: './todo.component.html',
  styleUrls: ['./todo.component.scss']
})
export class TodoComponent implements OnInit {
  numberStu = [];
  year_group_name:any;

  constructor(private service: ApiService) {}

  ngOnInit() {
    this.service.httpGet('/api/v1/trn-classroom-member/numstuallcatclasspie', '').then((res: IResponse) => {
        this.numberStu = res.responseData || [];
        this.year_group_name = this.numberStu[0].name

    });
  }

}