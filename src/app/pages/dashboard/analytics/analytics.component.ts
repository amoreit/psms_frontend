import { Component, OnInit, ElementRef, ViewChild } from '@angular/core';
import { ApiService, IResponse } from 'src/app/shared/service/api.service';

@Component({
  selector: 'app-analytics',
  templateUrl: './analytics.component.html'
})
export class AnalyticsComponent implements OnInit {

  public showXAxis = true;
  public legendTitle = '';
  public showYAxis = true;
  public gradient = false;
  public showLegend = true;
  public showXAxisLabel = true;
  public xAxisLabel = 'ปีการศึกษา';
  public showYAxisLabel = true;
  public yAxisLabel = 'จำนวนนักเรียน';
  public colorScheme = {
    domain: ['#3ac9d6', '#f9c851']
  };
  public autoScale = true;
  public roundDomains = true;
  numberStu = [];
  multi = [];
  male_detail = []
  female_detail = []
  @ViewChild('resizedDiv', { static: true }) resizedDiv: ElementRef;
  public previousWidthOfResizedDiv: number = 0;

  constructor(private service: ApiService) { }

  ngOnInit() {
    this.service.httpGet('/api/v1/trn-classroom-member/graphallnumstubyyear', '').then((res: IResponse) => {
      if (res.responseData.length === 0) {
        this.numberStu = []
      } else {
        this.numberStu = res.responseData || [];
        for (let i = 0; i < this.numberStu.length; i++) {
          let json_male = { name: this.numberStu[i].year_code, value: this.numberStu[i].male }
          this.male_detail.push(json_male)
          let json_female = { name: this.numberStu[i].year_code, value: this.numberStu[i].female }
          this.female_detail.push(json_female)
        }
        this.multi = [{
          name: 'ภาคปกติ ชาย',
          series: this.male_detail
        },
        {
          name: 'ภาคปกติ หญิง',
          series: this.female_detail
        }
        ]
      }
    });
  }

}