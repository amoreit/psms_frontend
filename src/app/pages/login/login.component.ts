import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router'; 
import { ApiService, IResponse } from 'src/app/shared/service/api.service';
import { FormBuilder } from '@angular/forms'; 
import * as constants from '../../constants';
import Swal from 'sweetalert2/dist/sweetalert2.js';
import 'sweetalert2/src/sweetalert2.scss';

@Component({ 
  selector: 'app-login',
  templateUrl: './login.component.html', 
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  public form = this.fb.group({
    'usrName': [''],
    'password': ['']
  });

  constructor(public fb: FormBuilder,public router:Router,private service:ApiService) {
  }

  ngOnInit() {
      localStorage.clear();
  }

  onLogin(){
    this.service.login(this.form.value).then((res:IResponse)=>{
      if((res.responseCode||500) != 200){
        Swal.fire(
          'ล็อคอินผิดพลาด',
          res.responseMsg,
          'error'  
        )
        return;
      }
      let data = res.responseData||{};
      localStorage.setItem("tokenNo",data.tokenNo);
      localStorage.setItem("userName",data.userName);
      localStorage.setItem("usrName",data.user);
      localStorage.setItem("email",data.email);
      localStorage.setItem("title",data.title);
      localStorage.setItem("citizenId",data.citizenId);
      localStorage.setItem("menu",data.menu);
      localStorage.setItem("role", data.role);
      document.location.href = constants.BASE_PATH;
    });
  }

  onRegister(){
    this.router.navigate(['/register']);
  }

  onForgotpassword(){
    this.router.navigate(['/forgot-password']);
  }

}
