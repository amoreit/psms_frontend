import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormBuilder } from '@angular/forms';
import { ApiService, IResponse } from 'src/app/shared/service/api.service';
import { UtilService } from 'src/app/_util/util.service';
import * as constants from '../../../constants';
import Swal from 'sweetalert2/dist/sweetalert2.js'; 
import 'sweetalert2/src/sweetalert2.scss';


@Component({
  selector: 'app-tec003',
  templateUrl: './tec003.component.html',
  styleUrls: ['./tec003.component.scss']
})
export class Tec003Component implements OnInit {

  isProcess:boolean = false;
  FormGroup = this.fb.group({
     'teacherId':['']
     ,'scCode':['']
     ,'fullName':['']
     ,'yearGroupId':['']
     ,'doorName':['']
     ,'checkTime':['']
    , 'teacherCardId':['']
    ,'cardId':[''] 
    ,'smartPurseId':[''] 
  });

  date = new Date();
  time = new Date();
  timer;
  today = new Date();

  teacherList:[];
  fullname ='';
  teacher_image:any;
  checkTime ='';
  doorName = '';
  yearGroupId = '';
  teacherCardId ='';
  teacher_card_id='';
  tbTeaTimeAttendanceList:[];
  yearGruopList:[];
  dataSucess = false;
  check_checkTime : boolean;
  urls = new Array<string>();

  constructor(private ar:ActivatedRoute,
    private router: Router ,
    private _formBuilder: FormBuilder,
    private service:ApiService,
    public util:UtilService,
    private fb: FormBuilder,) { }

  ngOnInit() {
    this.timer = setInterval(() => {
      this.time = new Date();
    }, 1000);
    document.getElementById("teacher_id").focus();
    this.urls = ['../assets/img/profile/user-no-img.jpg'];
  }

  detectFiles(event) {
    this.urls = [];
    let files = event.target.files;
    if (files) {
      for (let file of files) {
        let reader = new FileReader();
        reader.onload = (e: any) => {
          this.urls.push(e.target.result);
        }
        reader.readAsDataURL(file);
      }
    }
  }

  ddlTbMstTeacher(){
    if(this.FormGroup.value.smartPurseId.length == 8){
      let json = {'smartPurseId':this.FormGroup.value.smartPurseId};
      this.service.httpGet('/api/v1/0/teacher/ddlSm',json).then((res)=>{
        this.teacherList = res||[];
        for(let data of this.teacherList){
          this.fullname =  data['fullname'];
          this.teacher_image = data['teacherImage']
          this.teacher_card_id = data['teacherCardId']
          if(this.teacher_image == null || this.teacher_image == ''){
            this.urls = [constants.SERVICE_API + '/api/v1/attachmentteacher/view/user-no-img.jpg'];
          }else {
            this.urls = [constants.SERVICE_API + '/api/v1/attachmentteacher/view/' + this.teacher_image];
          }
        }
        this.onYear();
      }); 
    }
  }

  onYear(){
    this.service.httpGet('/api/v1/0/year-group/ddlYear',null).then((res)=>{
      this.yearGruopList = res||[];
      for(let data of this.yearGruopList){
        this.yearGroupId =  data['yearGroupId'];  
      }
      this.onSave();
      setTimeout(() => this.onClearData(),3000);
    });
  }

  onClearData(){
    this.FormGroup.controls.teacherCardId.setValue('');
    this.dataSucess = false;
  }

  onSave(){
    if(this.fullname != ''){
      this.FormGroup.controls.cardId.setValue(this.FormGroup.value.smartPurseId.toUpperCase());
      this.FormGroup.controls.fullName.setValue(this.fullname);
      this.FormGroup.controls.teacherCardId.setValue(this.teacher_card_id);
      this.FormGroup.controls.yearGroupId.setValue(this.yearGroupId);
      this.FormGroup.value.doorName ='1';
      this.service.httpPost('/api/v1/tbteatime-attendance',this.FormGroup.value).then((res:IResponse)=>{ 
            this.ddlTbTeaTimeAttendance();  
      });
    }else {
      Swal.fire(
        'เกิดข้อผิดพลาด',
        'เนื่องจากไม่พบข้อมูลของท่านในระบบ',
        'error'
      )
    }
  }

  ddlTbTeaTimeAttendance(){
    let json = {'cardId': this.FormGroup.value.cardId};
    this.service.httpGet('/api/v1/0/teacher-attendance/ddl',json).then((res)=>{
      this.tbTeaTimeAttendanceList = res||[];
      for(let data of this.tbTeaTimeAttendanceList){
        this.checkTime =  data['checkTime'];
        this.doorName =  data['doorName'];
      }
      this.dataSucess = true;
    });
  }

}

  
  


