import { Routes } from '@angular/router';
import { AuthGuard } from '../../shared/service/auth.guard';

/*** Page ***/
import { Tec001Component } from './tec001/tec001.component';
import { Tec001FormComponent } from './tec001/tec001-form/tec001-form.component';
import { Tec002Component } from './tec002/tec002.component';
import { Tec003Component } from './tec003/tec003.component';
import { Tec004Component } from './tec004/tec004.component';
import { Tec005Component } from './tec005/tec005.component';
import { Tec006Component } from './tec006/tec006.component';
import { Tec007Component } from './tec007/tec007.component';
import { Tec008Component } from './tec008/tec008.component';
import { Tec009Component } from './tec009/tec009.component';
import { Tec010Component } from './tec010/tec010.component';
import { Tec011Component } from './tec011/tec011.component'; 
import { Tec011CardComponent } from './tec011//tec011-card/tec011-card.component';

export const TecRoutes: Routes = [
  {
    path:'',
    children:[
      {path: 'tec001',component: Tec001Component,canActivate:[AuthGuard]},
	  {path: 'tec001/form',component:  Tec001FormComponent,canActivate:[AuthGuard]},
      {path: 'tec002',component: Tec002Component,canActivate:[AuthGuard]},
      {path: 'tec003',component: Tec003Component,canActivate:[AuthGuard]},
      {path: 'tec004',component: Tec004Component,canActivate:[AuthGuard]},
      {path: 'tec005',component: Tec005Component,canActivate:[AuthGuard]},
      {path: 'tec006',component: Tec006Component,canActivate:[AuthGuard]},
      {path: 'tec007',component: Tec007Component,canActivate:[AuthGuard]},
      {path: 'tec008',component: Tec008Component,canActivate:[AuthGuard]},
      {path: 'tec009',component: Tec009Component,canActivate:[AuthGuard]},
      {path: 'tec010',component: Tec010Component,canActivate:[AuthGuard]},
      {path: 'tec011',component: Tec011Component,canActivate:[AuthGuard]},
      {path: 'tec011/card',component: Tec011CardComponent,canActivate:[AuthGuard]}
    ]
  }
];