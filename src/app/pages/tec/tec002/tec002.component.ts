import { Component, OnInit, ViewChild } from '@angular/core';
import { MatTableDataSource, MatPaginator, MatDialog } from '@angular/material';
import { FormBuilder } from '@angular/forms';
import { ApiService, IResponse } from 'src/app/shared/service/api.service';
import { Router } from '@angular/router';
import { UtilService } from 'src/app/_util/util.service';
import { SelectionModel } from '@angular/cdk/collections';

import Swal from 'sweetalert2/dist/sweetalert2.js';
import 'sweetalert2/src/sweetalert2.scss';
import { Tec002DialogComponent } from 'src/app/dialog/tec002-dialog/tec002-dialog.component';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-tec002',
  templateUrl: './tec002.component.html',
  styleUrls: ['./tec002.component.scss']
}) 
export class Tec002Component implements OnInit {

  searchForm = this.fb.group({ 
    'yearGroupName':['']
    , 'className':['']
    , 'classRoom':['']
    , 'p':['']
    , 'result':[10]
  });
 
  classId:any;
  classLnameTh:any;
  yearGroupId:any;
  classRoomId:any;
  yearGroupList:[]; 
  classList:[]; 
  classRoomList:[];  
  userName:any;

  pageSize:number; 
  displayedColumns: string[] = ['yearGroupName','stuCode', 'fullname','classRoom','manage'];
  dataSource = new MatTableDataSource();

  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  constructor(
      private fb: FormBuilder,
      private service: ApiService,
      public dialog: MatDialog, 
      private translate: TranslateService,
      public util: UtilService
  ) { } 
 
  ngOnInit() { 
    this.userName = localStorage.getItem('userName')||'';
    this.ddlYearGroup(); 
    this.ddlClass(); 
    this.ddlClassRoom();
  }

  ddlYearGroup(){ 
    this.service.httpGet('/api/v1/0/year-group/ddl',null).then((res)=>{
      this.yearGroupList = res||[];
      this.searchForm.controls.yearGroupName.setValue(res[0]['name']);
      this.onSearch(0);
    }); 
  }

  ddlClass(){
    this.service.httpGet('/api/v1/0/tbMstClass/ddl',null).then((res)=>{
      this.classList= res||[];
    }); 
  }

  ddlClassRoom(){
    let json = {'classSnameTh':this.searchForm.value.className};
    this.service.httpGet('/api/v1/0/classroom-list/className',json).then((res)=>{
      this.classRoomList = res||[];
    }); 
  }
 
  onSearch(e){
    this.searchForm.value.p = (this.paginator.pageIndex = e.pageIndex||0)+1;
    this.service.httpGet('/api/v1/_teacherClassRoomMember',this.searchForm.value).then((res:IResponse)=>{
      this.dataSource.data = res.responseData||[];
      this.pageSize = res.responseSize||0;
    });
  }
 
  onSearchteacher(): void {
    let eleYear = this.yearGroupList.filter((o)=>{ 
      return o['name'] == this.searchForm.value.yearGroupName;
    });
    this.yearGroupId =  eleYear[0]['yearGroupId'];

    let eleClass = this.classList.filter((o)=>{ 
      return o['classSnameTh'] == this.searchForm.value.className;
    });
    this.classId =  eleClass[0]['classId'];
    this.classLnameTh = eleClass[0]['classLnameTh'];

    let eleClassroom = this.classRoomList.filter((o)=>{ 
      return o['classRoomSnameTh'] == this.searchForm.value.classRoom;
    });
    this.classRoomId =  eleClassroom[0]['classRoomId'];

    localStorage.setItem("tec002_yearGroupId",this.yearGroupId);
    localStorage.setItem("tec002_yearGroupName",this.searchForm.value.yearGroupName);
    localStorage.setItem("tec002_classId",this.classId);
    localStorage.setItem("tec002_className",this.searchForm.value.className);
    localStorage.setItem("tec002_classLnameTh",this.classLnameTh)
    localStorage.setItem("tec002_classRoomId",this.classRoomId);
    localStorage.setItem("tec002_classRoom",this.searchForm.value.classRoom);
    const dialogRef = this.dialog.open(Tec002DialogComponent, { 
      width: '55%' 
      , 'autoFocus': true 
      , 'disableClose':true}
    ); 
    dialogRef.afterClosed().subscribe(result => {
      this.searchForm.controls.yearGroupName.setValue(localStorage.getItem("tec002_yearGroupName"));
      this.searchForm.controls.className.setValue(localStorage.getItem("tec002_className"));
      this.searchForm.controls.classRoom.setValue(localStorage.getItem("tec002_classRoom"));
      this.onSearch(0); 
    });
  }

  onDelete(classRoomMemberId, stuCode){
    Swal.fire({
        title: this.translate.instant('alert.delete'), 
        icon : 'info',
        showCancelButton: true, 
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: this.translate.instant('psms.DL0008'),
        cancelButtonText: this.translate.instant('psms.DL0009') 
      }).then((result) => { 
      if(result.dismiss == 'cancel'){
          return;
      } 
      let json = {'classRoomMemberId':classRoomMemberId};
      this.service.httpDelete('/api/v1/_teacherClassRoomMember',json).then((res:IResponse)=>{
        if((res.responseCode||500)!=200){
          Swal.fire(
            this.translate.instant('message.delete_error'),
            res.responseMsg,
            'error'
          )
          return;
        }
        this.onDeleteTeacher(stuCode);
      });
    });
  }

  onDeleteTeacher(stuCode){
    let json = {'teacherCardId':stuCode};
    this.service.httpPut('/api/v1/tbTeacherProfile/classroom/delete/'+this.userName,json).then((res:IResponse)=>{
      if((res.responseCode||500)!=200){
        Swal.fire(
          this.translate.instant('message.delete_error'),
          res.responseMsg,
          'error'
        )
        return;
      }
      Swal.fire(
        this.translate.instant('alert.delete_header'),
        this.translate.instant('message.delete'),
        'success'
      ).then(()=>this.reload());
    });
  }

  reload(){
    this.searchForm.controls.yearGroupName.setValue(this.searchForm.value.yearGroupName);
    this.searchForm.controls.className.setValue(this.searchForm.value.className);
    this.searchForm.controls.classRoom.setValue(this.searchForm.value.classRoom);
    this.onSearch(0);
  }

  onCancel(){
    this.searchForm.controls.yearGroupName.setValue(this.searchForm.value.yearGroupName);
    this.searchForm.controls.className.setValue(""); 
    this.searchForm.controls.classRoom.setValue(""); 
    this.onSearch(0);
  }

}
