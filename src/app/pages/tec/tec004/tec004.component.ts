import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator, MatTableDataSource } from '@angular/material';
import { FormBuilder } from '@angular/forms';
import { ApiService, IResponse } from 'src/app/shared/service/api.service';
import { Router } from '@angular/router';
import { UtilService } from 'src/app/_util/util.service';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-tec004',
  templateUrl: './tec004.component.html',
  styleUrls: ['./tec004.component.scss']
})
export class Tec004Component implements OnInit {

  isProcess = false;
  searchForm = this.fb.group({
    yearGroupId: [''],
    yearName: [''],
    classId: [''],
    className: [''],
    classRoomId: [''],
    roomName: [''],
    condition: [''],
    startDate: [''],
    endDate: [''],
    fullName: [''],
    fullName1: [''],
    
    p: [''],
    result: [10]
  });

  formfullName = this.fb.group({
    'fullName':['']
  });
  formYear = this.fb.group({
  'yearGroupId':['']
  });

//ดูทุก transection
  pageSize: number;
  displayedColumns: string[] = [
    'no1',
    'teacherCardId1',
    'fullName1',
    'doorNo',
    'time1'
  ];

  //ดูเฉพาะ first/ Last
  pageSizeAll: number;
  displayedColumnsAll: string[] = [
    'no',
    'teacherCardId',
    'fullName',
    'doorNo',
    'firstTime',
    'lastTime'
  ];

  dataSource = new MatTableDataSource();
  dataSourceAll = new MatTableDataSource();

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;

  yearList: [];
  classList: [];
  classRoomList: [];
  classRoomMemberId: [];

  constructor(
    private translate: TranslateService,
    private fb: FormBuilder,
    private service: ApiService,
    private router: Router,
    public utilService: UtilService,

  ) { }

  ngOnInit() {
    this.searchForm.controls.condition.setValue("2");
    this.onSearch(0);
    this.ddlYear();
    this.ddlClass();
    this.ddlClassRoom();
  }

  onSearch(e) {

    if (this.searchForm.value.startDate == '' && this.searchForm.value.endDate == '') {
      this.searchForm.controls.startDate.setValue(this.utilService.formatDateForSQL(new Date()));
      this.searchForm.controls.endDate.setValue(this.utilService.formatDateForSQL(new Date()));
      this.searchForm.value.p = (this.paginator.pageIndex = e.pageIndex || 0) + 1;
      this.service
        .httpGet('/api/v1/tbteatime-attendance', this.searchForm.value)
        .then((res: IResponse) => {
          this.dataSource.data = res.responseData || [];
          this.pageSize = res.responseSize || 0;
          
        });

      this.service
        .httpGet('/api/v1/tbteatime-attendance', this.searchForm.value)
        .then((res: IResponse) => {
          this.dataSourceAll.data = res.responseData || [];
          this.pageSizeAll = res.responseSize || 0;
          
        });

    } else if (this.searchForm.value.startDate != '' && this.searchForm.value.endDate != '') {
      this.searchForm.controls.startDate.setValue(this.utilService.formatDateForSQL(new Date(this.searchForm.value.startDate)));
      this.searchForm.controls.endDate.setValue(this.utilService.formatDateForSQL(new Date(this.searchForm.value.endDate)));
      this.searchForm.value.p = (this.paginator.pageIndex = e.pageIndex || 0) + 1;
      this.service
        .httpGet('/api/v1/tbteatime-attendance', this.searchForm.value)
        .then((res: IResponse) => {
          this.dataSource.data = res.responseData || [];
          this.pageSize = res.responseSize || 0;
        });

      this.service
        .httpGet('/api/v1/tbteatime-attendance', this.searchForm.value)
        .then((res: IResponse) => {
          this.dataSourceAll.data = res.responseData || [];
          this.pageSizeAll = res.responseSize || 0;

        });

    }
  }

  /* DROP DOWN ปีการศึกษา */
  ddlYear() {
    this.service.httpGet('/api/v1/0/year-group/ddl', null).then(res => {
      this.yearList = res || [];   
      this.searchForm.controls.yearGroupId.setValue(res[0]['yearGroupId'])
    });
  }
  

  /* DROP DOWN ชั้นเรียน */
  ddlClass() {
    this.service.httpGet('/api/v1/0/class/classddl', null).then(res => {
      this.classList = res || [];
    });
  }

  /* DROP DOWN ห้องเรียน */
  ddlClassRoom() {
    const json = { classId: this.searchForm.value.classId };
    this.service
      .httpGet('/api/v1/0/classroom-list/ddlFilter', json)
      .then(res => {
        this.classRoomList = res || [];
      });
  }

  onCancel() {
    this.searchForm = this.fb.group({
      
      yearGroupId: [''],
      fullName: [''],
      condition: [''],
      startDate: [new Date()],
      endDate: [new Date()],
      p: [''],
      result: [10]
    });
    this.onSearch(0);

  }

  onReport() {
    this.isProcess = true;
    if (this.searchForm.value.condition == '' || this.searchForm.value.condition == '2') {

      if (this.searchForm.value.yearGroupId == '') {
        if (this.searchForm.value.fullName == '') {
          if (this.searchForm.value.classRoomId == '') {

          } else if (this.searchForm.value.classRoomId != '') {
            const jsonRoom = this.classRoomList.filter((o) => {
              return o['classRoomId'] == this.searchForm.value.classRoomId;
            });
            this.searchForm.value.roomName = jsonRoom[0]['classRoomSnameTh'];

          }
        } else if (this.searchForm.value.fullName != '') {
          if (this.searchForm.value.classRoomId == '') {
            const jsonClass = this.classList.filter((o) => {
              
            });
            

          } else if (this.searchForm.value.classRoomId != '') {
            const jsonClass = this.classList.filter((o) => {
              
            });
            


            const jsonRoom = this.classRoomList.filter((o) => {
              return o['classRoomId'] == this.searchForm.value.classRoomId;
            });
            this.searchForm.value.roomName = jsonRoom[0]['classRoomSnameTh'];

          }
        }
      } else if (this.searchForm.value.yearGroupId != '') {
        if (this.searchForm.value.fullName == '') {
          if (this.searchForm.value.classRoomId == '') {
            const jsonYear = this.yearList.filter((o) => {
              return o['yearGroupId'] == this.searchForm.value.yearGroupId;
            });
            this.searchForm.value.yearName = jsonYear[0]['name'];

          } else if (this.searchForm.value.classRoomId != '') {
            const jsonYear = this.yearList.filter((o) => {
              return o['yearGroupId'] == this.searchForm.value.yearGroupId;
            });
            this.searchForm.value.yearName = jsonYear[0]['name'];

            const jsonRoom = this.classRoomList.filter((o) => {
              return o['classRoomId'] == this.searchForm.value.classRoomId;
            });
            this.searchForm.value.roomName = jsonRoom[0]['classRoomSnameTh'];

          }
        } else if (this.searchForm.value.fullName != '') {
          if (this.searchForm.value.classRoomId == '') {
            const jsonYear = this.yearList.filter((o) => {
              return o['yearGroupId'] == this.searchForm.value.yearGroupId;
            });
            this.searchForm.value.yearName = jsonYear[0]['name'];


            const jsonClass = this.classList.filter((o) => {
              
            });
        

          } else if (this.searchForm.value.classRoomId != '') {
            const jsonYear = this.yearList.filter((o) => {
              return o['yearGroupId'] == this.searchForm.value.yearGroupId;
            });
            this.searchForm.value.yearName = jsonYear[0]['name'];


            const jsonClass = this.classList.filter((o) => {
              
            });
           


            const jsonRoom = this.classRoomList.filter((o) => {
              return o['classRoomId'] == this.searchForm.value.classRoomId;
            });
            this.searchForm.value.roomName = jsonRoom[0]['classRoomSnameTh'];

          }
        }
      }

      this.service
        .httpPreviewPDF('/api/v1/tec004/report/tec004_2/pdf', this.searchForm.value)
        .then(res => {
          this.isProcess = false;
          window.location.href = res;
        });

    } else if (this.searchForm.value.condition == '1') {
      if (this.searchForm.value.yearGroupId == '') {
        if (this.searchForm.value.fullName == '') {
          if (this.searchForm.value.classRoomId == '') {

          } else if (this.searchForm.value.classRoomId != '') {
            const jsonRoom = this.classRoomList.filter((o) => {
              return o['classRoomId'] == this.searchForm.value.classRoomId;
            });
            this.searchForm.value.roomName = jsonRoom[0]['classRoomSnameTh'];

          }
        } else if (this.searchForm.value.fullName != '') {
          if (this.searchForm.value.classRoomId == '') {
            const jsonClass = this.classList.filter((o) => {

            });
       

          } else if (this.searchForm.value.classRoomId != '') {
            const jsonClass = this.classList.filter((o) => {
           
            });
           


            const jsonRoom = this.classRoomList.filter((o) => {
              return o['classRoomId'] == this.searchForm.value.classRoomId;
            });
            this.searchForm.value.roomName = jsonRoom[0]['classRoomSnameTh'];

          }
        }
      } else if (this.searchForm.value.yearGroupId != '') {
        if (this.searchForm.value.fullName == '') {
          if (this.searchForm.value.classRoomId == '') {
            const jsonYear = this.yearList.filter((o) => {
              return o['yearGroupId'] == this.searchForm.value.yearGroupId;
            });
            this.searchForm.value.yearName = jsonYear[0]['name'];

          } else if (this.searchForm.value.classRoomId != '') {
            const jsonYear = this.yearList.filter((o) => {
              return o['yearGroupId'] == this.searchForm.value.yearGroupId;
            });
            this.searchForm.value.yearName = jsonYear[0]['name'];

            const jsonRoom = this.classRoomList.filter((o) => {
              return o['classRoomId'] == this.searchForm.value.classRoomId;
            });
            this.searchForm.value.roomName = jsonRoom[0]['classRoomSnameTh'];

          }
        } else if (this.searchForm.value.fullName != '') {
          if (this.searchForm.value.classRoomId == '') {
            const jsonYear = this.yearList.filter((o) => {
              return o['yearGroupId'] == this.searchForm.value.yearGroupId;
            });
            this.searchForm.value.yearName = jsonYear[0]['name'];


            const jsonClass = this.classList.filter((o) => {
             
            });
            

          } else if (this.searchForm.value.classRoomId != '') {
            const jsonYear = this.yearList.filter((o) => {
              return o['yearGroupId'] == this.searchForm.value.yearGroupId;
            });
            this.searchForm.value.yearName = jsonYear[0]['name'];


            const jsonClass = this.classList.filter((o) => {
           
            });
            


            const jsonRoom = this.classRoomList.filter((o) => {
              return o['classRoomId'] == this.searchForm.value.classRoomId;
            });
            this.searchForm.value.roomName = jsonRoom[0]['classRoomSnameTh'];

          }
        }
      }

      

      this.service
        .httpPreviewPDF('/api/v1/tec004/report/tec004_1/pdf', this.searchForm.value)
        .then(res => {
          this.isProcess = false;
          window.location.href = res;
        });
    }


  }

  onExportPDF() {
    this.isProcess = true;

    if (this.searchForm.value.condition == '' || this.searchForm.value.condition == '2') {

      if (this.searchForm.value.yearGroupId == '') {
        if (this.searchForm.value.fullName == '') {
          if (this.searchForm.value.classRoomId == '') {

          } else if (this.searchForm.value.classRoomId != '') {
            const jsonRoom = this.classRoomList.filter((o) => {
              return o['classRoomId'] == this.searchForm.value.classRoomId;
            });
            this.searchForm.value.roomName = jsonRoom[0]['classRoomSnameTh'];

          }
        } else if (this.searchForm.value.fullName != '') {
          if (this.searchForm.value.classRoomId == '') {
            const jsonClass = this.classList.filter((o) => {
              
            });
            

          } else if (this.searchForm.value.classRoomId != '') {
            const jsonClass = this.classList.filter((o) => {
            
            });
           


            const jsonRoom = this.classRoomList.filter((o) => {
              return o['classRoomId'] == this.searchForm.value.classRoomId;
            });
            this.searchForm.value.roomName = jsonRoom[0]['classRoomSnameTh'];

          }
        }
      } else if (this.searchForm.value.yearGroupId != '') {
        if (this.searchForm.value.fullName == '') {
          if (this.searchForm.value.classRoomId == '') {
            const jsonYear = this.yearList.filter((o) => {
              return o['yearGroupId'] == this.searchForm.value.yearGroupId;
            });
            this.searchForm.value.yearName = jsonYear[0]['name'];

          } else if (this.searchForm.value.classRoomId != '') {
            const jsonYear = this.yearList.filter((o) => {
              return o['yearGroupId'] == this.searchForm.value.yearGroupId;
            });
            this.searchForm.value.yearName = jsonYear[0]['name'];

            const jsonRoom = this.classRoomList.filter((o) => {
              return o['classRoomId'] == this.searchForm.value.classRoomId;
            });
            this.searchForm.value.roomName = jsonRoom[0]['classRoomSnameTh'];

          }
        } else if (this.searchForm.value.fullName != '') {
          if (this.searchForm.value.classRoomId == '') {
            const jsonYear = this.yearList.filter((o) => {
              return o['yearGroupId'] == this.searchForm.value.yearGroupId;
            });
            this.searchForm.value.yearName = jsonYear[0]['name'];


            const jsonClass = this.classList.filter((o) => {
              
            });
           

          } else if (this.searchForm.value.classRoomId != '') {
            const jsonYear = this.yearList.filter((o) => {
              return o['yearGroupId'] == this.searchForm.value.yearGroupId;
            });
            this.searchForm.value.yearName = jsonYear[0]['name'];


            const jsonClass = this.classList.filter((o) => {
             
            });
            


            const jsonRoom = this.classRoomList.filter((o) => {
              return o['classRoomId'] == this.searchForm.value.classRoomId;
            });
            this.searchForm.value.roomName = jsonRoom[0]['classRoomSnameTh'];

          }
        }
      }

      this.service
        .httpReportPDF('/api/v1/tec004/report/tec004_2/pdf', 'รายงานเวลาเข้า-ออก โรงเรียนนักเรียน เฉพาะ First/Last', this.searchForm.value)
        .then(res => {
          this.isProcess = false;

        });

    } else if (this.searchForm.value.condition == '1') {
      if (this.searchForm.value.yearGroupId == '') {
        if (this.searchForm.value.fullName == '') {
          if (this.searchForm.value.classRoomId == '') {

          } else if (this.searchForm.value.classRoomId != '') {
            const jsonRoom = this.classRoomList.filter((o) => {
              return o['classRoomId'] == this.searchForm.value.classRoomId;
            });
            this.searchForm.value.roomName = jsonRoom[0]['classRoomSnameTh'];

          }
        } else if (this.searchForm.value.fullName != '') {
          if (this.searchForm.value.classRoomId == '') {
            const jsonClass = this.classList.filter((o) => {
              
            });
            

          } else if (this.searchForm.value.classRoomId != '') {
            const jsonClass = this.classList.filter((o) => {
           
            });
            


            const jsonRoom = this.classRoomList.filter((o) => {
              return o['classRoomId'] == this.searchForm.value.classRoomId;
            });
            this.searchForm.value.roomName = jsonRoom[0]['classRoomSnameTh'];

          }
        }
      } else if (this.searchForm.value.yearGroupId != '') {
        if (this.searchForm.value.fullName == '') {
          if (this.searchForm.value.classRoomId == '') {
            const jsonYear = this.yearList.filter((o) => {
              return o['yearGroupId'] == this.searchForm.value.yearGroupId;
            });
            this.searchForm.value.yearName = jsonYear[0]['name'];

          } else if (this.searchForm.value.classRoomId != '') {
            const jsonYear = this.yearList.filter((o) => {
              return o['yearGroupId'] == this.searchForm.value.yearGroupId;
            });
            this.searchForm.value.yearName = jsonYear[0]['name'];

            const jsonRoom = this.classRoomList.filter((o) => {
              return o['classRoomId'] == this.searchForm.value.classRoomId;
            });
            this.searchForm.value.roomName = jsonRoom[0]['classRoomSnameTh'];

          }
        } else if (this.searchForm.value.fullName != '') {
          if (this.searchForm.value.classRoomId == '') {
            const jsonYear = this.yearList.filter((o) => {
              return o['yearGroupId'] == this.searchForm.value.yearGroupId;
            });
            this.searchForm.value.yearName = jsonYear[0]['name'];


            const jsonClass = this.classList.filter((o) => {
              
            });
           

          } else if (this.searchForm.value.classRoomId != '') {
            const jsonYear = this.yearList.filter((o) => {
              return o['yearGroupId'] == this.searchForm.value.yearGroupId;
            });
            this.searchForm.value.yearName = jsonYear[0]['name'];


            const jsonClass = this.classList.filter((o) => {
              
            });
            


            const jsonRoom = this.classRoomList.filter((o) => {
              return o['classRoomId'] == this.searchForm.value.classRoomId;
            });
            this.searchForm.value.roomName = jsonRoom[0]['classRoomSnameTh'];

          }
        }
      }

      this.service
        .httpReportPDF('/api/v1/tec004/report/tec004_1/pdf', 'รายงานเวลาเข้า-ออก โรงเรียนนักเรียน ทุก Transection', this.searchForm.value)
        .then(res => {
          this.isProcess = false;
          window.location.href = res;
        });
    }

  }

  onExportXLS() {
    this.isProcess = true;

    if (this.searchForm.value.condition == '' || this.searchForm.value.condition == '2') {

      if (this.searchForm.value.yearGroupId == '') {
        if (this.searchForm.value.fullName == '') {
          if (this.searchForm.value.classRoomId == '') {

          } else if (this.searchForm.value.classRoomId != '') {
            const jsonRoom = this.classRoomList.filter((o) => {
              return o['classRoomId'] == this.searchForm.value.classRoomId;
            });
            this.searchForm.value.roomName = jsonRoom[0]['classRoomSnameTh'];

          }
        } else if (this.searchForm.value.fullName != '') {
          if (this.searchForm.value.classRoomId == '') {
            const jsonClass = this.classList.filter((o) => {
              
            });
         

          } else if (this.searchForm.value.classRoomId != '') {
            const jsonClass = this.classList.filter((o) => {
            
            });
          


            const jsonRoom = this.classRoomList.filter((o) => {
              return o['classRoomId'] == this.searchForm.value.classRoomId;
            });
            this.searchForm.value.roomName = jsonRoom[0]['classRoomSnameTh'];

          }
        }
      } else if (this.searchForm.value.yearGroupId != '') {
        if (this.searchForm.value.fullName == '') {
          if (this.searchForm.value.classRoomId == '') {
            const jsonYear = this.yearList.filter((o) => {
              return o['yearGroupId'] == this.searchForm.value.yearGroupId;
            });
            this.searchForm.value.yearName = jsonYear[0]['name'];

          } else if (this.searchForm.value.classRoomId != '') {
            const jsonYear = this.yearList.filter((o) => {
              return o['yearGroupId'] == this.searchForm.value.yearGroupId;
            });
            this.searchForm.value.yearName = jsonYear[0]['name'];

            const jsonRoom = this.classRoomList.filter((o) => {
              return o['classRoomId'] == this.searchForm.value.classRoomId;
            });
            this.searchForm.value.roomName = jsonRoom[0]['classRoomSnameTh'];

          }
        } else if (this.searchForm.value.fullName != '') {
          if (this.searchForm.value.classRoomId == '') {
            const jsonYear = this.yearList.filter((o) => {
              return o['yearGroupId'] == this.searchForm.value.yearGroupId;
            });
            this.searchForm.value.yearName = jsonYear[0]['name'];


            const jsonClass = this.classList.filter((o) => {
            
            });
           

          } else if (this.searchForm.value.classRoomId != '') {
            const jsonYear = this.yearList.filter((o) => {
              return o['yearGroupId'] == this.searchForm.value.yearGroupId;
            });
            this.searchForm.value.yearName = jsonYear[0]['name'];


            const jsonClass = this.classList.filter((o) => {
            
            });
           


            const jsonRoom = this.classRoomList.filter((o) => {
              return o['classRoomId'] == this.searchForm.value.classRoomId;
            });
            this.searchForm.value.roomName = jsonRoom[0]['classRoomSnameTh'];

          }
        }
      }

      this.service
        .httpReportXLS('/api/v1/tec004/report/tec004_2/xls', 'รายงานเวลาเข้า-ออก โรงเรียนนักเรียน เฉพาะ First/Last', this.searchForm.value)
        .then(res => {
          this.isProcess = false;

        });

    } else if (this.searchForm.value.condition == '1') {
      if (this.searchForm.value.yearGroupId == '') {
        if (this.searchForm.value.fullName == '') {
          if (this.searchForm.value.classRoomId == '') {

          } else if (this.searchForm.value.classRoomId != '') {
            const jsonRoom = this.classRoomList.filter((o) => {
              return o['classRoomId'] == this.searchForm.value.classRoomId;
            });
            this.searchForm.value.roomName = jsonRoom[0]['classRoomSnameTh'];

          }
        } else if (this.searchForm.value.fullName != '') {
          if (this.searchForm.value.classRoomId == '') {
            const jsonClass = this.classList.filter((o) => {
           
            });
            

          } else if (this.searchForm.value.classRoomId != '') {
            const jsonClass = this.classList.filter((o) => {
             
            });
          


            const jsonRoom = this.classRoomList.filter((o) => {
              return o['classRoomId'] == this.searchForm.value.classRoomId;
            });
            this.searchForm.value.roomName = jsonRoom[0]['classRoomSnameTh'];

          }
        }
      } else if (this.searchForm.value.yearGroupId != '') {
        if (this.searchForm.value.fullName == '') {
          if (this.searchForm.value.classRoomId == '') {
            const jsonYear = this.yearList.filter((o) => {
              return o['yearGroupId'] == this.searchForm.value.yearGroupId;
            });
            this.searchForm.value.yearName = jsonYear[0]['name'];

          } else if (this.searchForm.value.classRoomId != '') {
            const jsonYear = this.yearList.filter((o) => {
              return o['yearGroupId'] == this.searchForm.value.yearGroupId;
            });
            this.searchForm.value.yearName = jsonYear[0]['name'];

            const jsonRoom = this.classRoomList.filter((o) => {
              return o['classRoomId'] == this.searchForm.value.classRoomId;
            });
            this.searchForm.value.roomName = jsonRoom[0]['classRoomSnameTh'];

          }
        } else if (this.searchForm.value.fullName != '') {
          if (this.searchForm.value.classRoomId == '') {
            const jsonYear = this.yearList.filter((o) => {
              return o['yearGroupId'] == this.searchForm.value.yearGroupId;
            });
            this.searchForm.value.yearName = jsonYear[0]['name'];


            const jsonClass = this.classList.filter((o) => {
              
            });
            

          } else if (this.searchForm.value.classRoomId != '') {
            const jsonYear = this.yearList.filter((o) => {
              return o['yearGroupId'] == this.searchForm.value.yearGroupId;
            });
            this.searchForm.value.yearName = jsonYear[0]['name'];


            const jsonClass = this.classList.filter((o) => {
             
            });
            this.searchForm.value.fullName = jsonClass[0]['classSnameTh'];


            const jsonRoom = this.classRoomList.filter((o) => {
              return o['classRoomId'] == this.searchForm.value.classRoomId;
            });
            this.searchForm.value.roomName = jsonRoom[0]['classRoomSnameTh'];

          }
        }
      }

      this.service
        .httpReportXLS('/api/v1/tec004/report/tec004_1/xls', 'รายงานเวลาเข้า-ออก โรงเรียนนักเรียน ทุก Transection', this.searchForm.value)
        .then(res => {
          this.isProcess = false;

        });
    }

  }
  


}
