import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder } from '@angular/forms';
import { ApiService, IResponse } from 'src/app/shared/service/api.service';
import { MatDialog, MatTableDataSource, MatPaginator } from '@angular/material';
import { UtilService } from 'src/app/_util/util.service';
import Swal from 'sweetalert2/dist/sweetalert2.js';
import 'sweetalert2/src/sweetalert2.scss';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-tec008',
  templateUrl: './tec008.component.html',
  styleUrls: ['./tec008.component.scss']
})
export class Tec008Component implements OnInit {

  searchForm = this.fb.group({
    'yearGroupId': ['']
    , 'catClassId': ['']
  });

  copyForm = this.fb.group({
    'yearGroupId': ['']
  });

  displayedColumns: string[] = ['yearGroup', 'fullname', 'department', 'leader', 'choose'];
  dataSource = new MatTableDataSource();
  yearGroupList: [];
  yearGroupCopyList: [];
  catclassList: [];
  _allChecklist = [];
  checkdata = false;
  headerSelected: boolean;
  year_group_name: any;
  year_group_search: any;
  year_group_name_copy: any;

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  constructor(private translate: TranslateService,private fb: FormBuilder, private service: ApiService, public dialog: MatDialog, private router: Router, public _util: UtilService) {

  }

  ngOnInit() {
    this.ddlCatClass();
    this.ddlYearGroup();
    this.onSearch(0);

  }

  onSearch(e) {
    this.headerSelected = false;
    this.service.httpGet('/api/v1/trn-teacher-department/teacher', this.searchForm.value).then((res: IResponse) => {
      if (res.responseData.length === 0) {
        this.dataSource.data = []
        this.checkdata = true;
      } else {
        this.dataSource.data = res.responseData || [];
        this.checkdata = false;
      }
    });
  }


  onCancel() {
    this.searchForm = this.fb.group({
      'yearGroupId': ['']
      , 'catClassId': ['']
    });
    this.copyForm = this.fb.group({
      'yearGroupId': ['']
    });
    this.onSearch(0);
  }

  ddlCatClass() {
    this.service.httpGet('/api/v1/0/catclass/ddl', null).then((res) => {
      this.catclassList = res || [];
    });
  }

  ddlYearGroup() {
    this.service.httpGet('/api/v1/0/year-group/ddl', null).then((res) => {
      this.yearGroupList = res || [];
      let ele = this.yearGroupList.filter((o) => {
        return o['iscurrent'] == "true";
      });
      this.searchForm.controls.yearGroupId.setValue(ele[0]['yearGroupId'])
      this.checkYearGroup(this.searchForm.value.yearGroupId);
    });
  }

  checkYearGroup(txt) {
    let ele = this.yearGroupList.filter((o) => {
      return o['yearGroupId'] == txt;
    });
    this.year_group_name = ele[0]['name']
    this.year_group_search = this.year_group_name.substring(2) + this.year_group_name.substring(0, 1)

    this.service.httpGet('/api/v1/0/year-group/ddlupperyeargroup', { 'yearGroupSearch': this.year_group_search }).then((res) => {
      this.yearGroupCopyList = res || [];
    });
  }


  onCheckYearGroupCopy() {
    let ele = this.yearGroupCopyList.filter((o) => {
      return o['year_group_id'] == this.copyForm.value.yearGroupId;
    });
    this.year_group_name_copy = ele[0]['name']
  }

  checkAll() {
    for (var i = 0; i < this.dataSource.data.length; i++) {
      this.dataSource.data[i]['isSelected'] = this.headerSelected;
    }
    this.getCheckedItemList();
  }

  isAllSelected() {
    this.dataSource.data.every((item: any) => {
      return item.isSelected === true;
    })
    this.headerSelected = false;
    this.getCheckedItemList();
  }

  getCheckedItemList() {
    this._allChecklist = [];
    for (var i = 0; i < this.dataSource.data.length; i++) {
      if (this.dataSource.data[i]['isSelected']) {
        let itemData = {
          'scCode': this.dataSource.data[i]['sc_code'], 'yearGroupId': '',
          'yearGroupName': '', 'departmentId': this.dataSource.data[i]['department_id'], 'departmentName': this.dataSource.data[i]['department_name'],
          'catClassId': this.dataSource.data[i]['cat_class_id'], 'teacherId': this.dataSource.data[i]['teacher_id'], 'fullname': this.dataSource.data[i]['fullname'], 'fullnameEn': this.dataSource.data[i]['fullname_en'],
          'leader': this.dataSource.data[i]['leader'], 'remark': this.dataSource.data[i]['remark']
        };
        this._allChecklist.push(itemData);
      }
    }
  }

  onSave2() {
    this._allChecklist.map(e => {
      e['yearGroupId'] = this.copyForm.value.yearGroupId;
      e['yearGroupName'] = this.year_group_name_copy;
      return e;
    });
    Swal.fire({
      title: this.translate.instant('alert.approve_classCopy_tecdep'),//ต้องการอนุมัติการคัดลอกกลุ่มคุณครูตามกลุ่มสาระ ใช่หรือไม่?
      icon: 'info',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: this.translate.instant('psms.DL0008'),//ใช่
      cancelButtonText: this.translate.instant('psms.DL0009')//ไม่
    }).then((result) => {
      if (result.dismiss == 'cancel') {
        return;
      }
      let json = this._allChecklist;
      let user = localStorage.getItem('userName')
      if (json.length === 0 || this.copyForm.value.yearGroupId == '') {
        Swal.fire(
          this.translate.instant('alert.approve_classCopy_tecdep_error'),//คัดลอกกลุ่มคุณครูตามกลุ่มสาระผิดพลาด
          this.translate.instant('alert.validate'),//กรุณาระบุข้อมูล ให้ครบ.
          'error'
        )
      } else {
        this.service.httpPost('/api/v1/trn-teacher-department/copyteacherdepartment/' + user, json).then((res: IResponse) => {
          if ((res.responseCode || 500) != 200) {
            Swal.fire(
              this.translate.instant('alert.approve_classCopy_tecdep_error'),//คัดลอกกลุ่มคุณครูตามกลุ่มสาระผิดพลาด
              res.responseMsg,
              'error'
            )
            return;
          }
          Swal.fire(
            this.translate.instant('alert.approve_classCopy_tecdep_header'),//คัดลอกกลุ่มคุณครูตามกลุ่มสาระ
            this.translate.instant('alert.approve_classCopy_tecdep_success'),//คัดลอกกลุ่มคุณครูตามกลุ่มสาระสำเร็จ
            'success'
          ).then(() => { })
        });
      }
      return;
    });
  }


}
