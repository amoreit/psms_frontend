import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FlexLayoutModule } from '@angular/flex-layout';
import { TranslateModule } from '@ngx-translate/core';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';  

/*** Material ***/
import {
  MatExpansionModule,
  MatButtonModule,
  MatIconModule,
  MatInputModule,
  MatTableModule,
  MatCardModule,
  MatFormFieldModule,
  MatPaginatorModule,
  MatDialogModule, 
  MatSlideToggleModule,
  MatTabsModule,
  MatDatepickerModule,
  MatNativeDateModule,
  MatCheckboxModule,
  MatRadioModule,
  MatStepperModule,
  
} from '@angular/material';

/*** Routing ***/
import {TecRoutes} from './tec.routing';

/*** Page ***/
import { Tec001Component } from './tec001/tec001.component';
import { Tec001FormComponent } from './tec001/tec001-form/tec001-form.component';
import { Tec002Component } from './tec002/tec002.component';
import { Tec003Component } from './tec003/tec003.component';
import { Tec004Component } from './tec004/tec004.component';
import { Tec005Component } from './tec005/tec005.component';
import { Tec006Component } from './tec006/tec006.component';
import { Tec007Component } from './tec007/tec007.component';
import { Tec008Component } from './tec008/tec008.component';
import { Tec009Component } from './tec009/tec009.component';
import { Tec010Component } from './tec010/tec010.component';
import { Tec011Component } from './tec011/tec011.component'; 
import { Tec011CardComponent } from './tec011//tec011-card/tec011-card.component';

@NgModule({
  imports: [ 
    CommonModule,
    FlexLayoutModule,
    TranslateModule,
    RouterModule.forChild(TecRoutes),
    MatExpansionModule,
    MatButtonModule,
    MatIconModule,
    MatInputModule,
    MatTableModule,
    MatCardModule,
    MatFormFieldModule,
    FormsModule, 
    ReactiveFormsModule,
    MatPaginatorModule,
    MatDialogModule,
    MatSlideToggleModule,
    MatTabsModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatCheckboxModule,
    MatRadioModule,
    MatStepperModule, 
  ],
    providers: [],
    exports:[RouterModule],
  declarations: [
    Tec001Component, 
	  Tec001FormComponent,
    Tec002Component, 
    Tec003Component, 
    Tec004Component, 
    Tec005Component, 
    Tec006Component, 
    Tec007Component,
    Tec008Component,
    Tec009Component,
    Tec010Component,
    Tec011Component,
    Tec011CardComponent
  ],
  entryComponents:[
    
  ]

})
export class TecModule { }
