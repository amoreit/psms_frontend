import { Component, OnInit, ViewChild } from '@angular/core';
import { SelectionModel } from '@angular/cdk/collections';
import { MatTableDataSource, MatPaginator, MatDialog } from '@angular/material';
import { FormBuilder } from '@angular/forms';
import { ApiService, IResponse } from 'src/app/shared/service/api.service';
import { Router } from '@angular/router';
import { UtilService } from 'src/app/_util/util.service';
import { Tec005DialogComponent } from 'src/app/dialog/tec005-dialog/tec005-dialog.component';
import { Tec005LeaderDialogComponent } from 'src/app/dialog/tec005-leader-dialog/tec005-leader-dialog.component';
import Swal from 'sweetalert2/dist/sweetalert2.js';
import 'sweetalert2/src/sweetalert2.scss';
import { TranslateService } from '@ngx-translate/core';

@Component({ 
  selector: 'app-tec005',
  templateUrl: './tec005.component.html',
  styleUrls: ['./tec005.component.scss']
})
export class Tec005Component implements OnInit {

  searchForm = this.fb.group({ 
    'yearGroupName':['']
    , 'catClassId':[''] 
    , 'departmentName':['']
  });

  yearGroupId:any;
  addTeacher = false;
  yearGroupList:[]; 
  catClassList:[];
  departmentList:[]; 
  departmentIdList:[];
  userName:any;
  
  _catClassName:any;
  _departmantId:any;
  _departmentName:any;

  displayedColumns: string[] = ['yearGroupName','fullname','leader','manage'];
  dataSource = new MatTableDataSource(); 

  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  constructor(
      private fb: FormBuilder,
      private service: ApiService,
      public dialog: MatDialog, 
      private translate: TranslateService,
      public util: UtilService
  ) { }  

  ngOnInit(){
    this.userName = localStorage.getItem('userName')||'';
    this.ddlYearGroup();
    this.ddlCatClass();
  } 

  onSearch(){
    this.addTeacher = true;
    let json = {'catClass':this.searchForm.value.catClassId};
    this.service.httpGet('/api/v1/_department',json).then((res:IResponse)=>{
      this.departmentList = res.responseData||[];
      for(let i=0; i <= this.departmentList.length-1; i++){
        let jsonSearch = {'departmentName':this.departmentList[i]['name'], 'catClassId':this.searchForm.value.catClassId, 'yearGroupName':this.searchForm.value.yearGroupName};
        this.service.httpGet('/api/v1/_teacherDepartment',jsonSearch).then((res:IResponse)=>{
          this.dataSource.data[i] = res.responseData||[];
        });
      }
    });
  }

  ddlYearGroup(){ 
    this.service.httpGet('/api/v1/0/year-group/ddl',null).then((res)=>{
      this.yearGroupList = res||[];
      this.searchForm.controls.yearGroupName.setValue(res[0]['name']);
    }); 
  }

  ddlCatClass(){
    this.service.httpGet('/api/v1/0/tbMstCatClass/junior',null).then((res)=>{
      this.catClassList = res||[];
    }); 
  }

  catClassName(){
    if(this.searchForm.value.catClassId == 2){this._catClassName = "ช่วงชั้นประถมศึกษา";}
    else if(this.searchForm.value.catClassId == 3){this._catClassName = "ช่วงชั้นมัธยมศึกษาตอนต้น";}
    else if(this.searchForm.value.catClassId == 4){this._catClassName = "ช่วงชั้นมัธยมศึกษาตอนปลาย";}
  }

  onSearchteacher(name):void{
    let ele = this.yearGroupList.filter((o)=>{ 
      return o['name'] == this.searchForm.value.yearGroupName;
    });
    this.yearGroupId =  ele[0]['yearGroupId'];

    let jsonDepartment= {'catClass':this.searchForm.value.catClassId, 'name':name};
    this.service.httpGet('/api/v1/0/check-departmentId/ddl',jsonDepartment).then((res)=>{
      this.departmentIdList = res||[];
      this._departmantId = res[0]['departmentId'];

      localStorage.setItem("yearGroupId",this.yearGroupId);
      localStorage.setItem("yearGroupName",this.searchForm.value.yearGroupName);
      localStorage.setItem("catClassId",this.searchForm.value.catClassId);
      localStorage.setItem("departmentId",this._departmantId);
      localStorage.setItem("name",name);
      const dialogRef = this.dialog.open(Tec005DialogComponent, { 
        width: '55%' 
        , 'autoFocus': true 
        , 'disableClose':true}
      );
      dialogRef.afterClosed().subscribe(result => {
        this.searchForm.controls.yearGroupName.setValue(localStorage.getItem("yearGroupName"));
        this.searchForm.controls.catClassId.setValue(localStorage.getItem("catClassId"));
        this.onSearch();
      });
    }); 
  } 

  onSearchdepartment(departmentLeader):void{
      localStorage.setItem("_yearGroupName",this.searchForm.value.yearGroupName);
      localStorage.setItem("_catClassId",this.searchForm.value.catClassId);
      localStorage.setItem("_departmentName",departmentLeader);
      const dialogRef = this.dialog.open(Tec005LeaderDialogComponent, { 
        width: '55%' 
        , 'autoFocus': true 
        , 'disableClose':true}
      );
      dialogRef.afterClosed().subscribe(result => {
        this.searchForm.controls.yearGroupName.setValue(localStorage.getItem("_yearGroupName"));
        this.searchForm.controls.catClassId.setValue(localStorage.getItem("_catClassId"));
        this.onSearch();
      });
  }

  onDelete(teacherDepartmentId, fullname){
      Swal.fire({
          title: this.translate.instant('alert.delete'), 
          icon : 'info',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          confirmButtonText: this.translate.instant('psms.DL0008'),
          cancelButtonText: this.translate.instant('psms.DL0009')
        }).then((result) => {
        if(result.dismiss == 'cancel'){
            return;
        } 
        let json = {'teacherDepartmentId':teacherDepartmentId};
        this.service.httpDelete('/api/v1/_teacherDepartment',json).then((res:IResponse)=>{
          if((res.responseCode||500)!=200){
            Swal.fire(
              this.translate.instant('message.delete_error'),
              res.responseMsg,
              'error'
            )
            return;
          }
          this.onDeleteTeacher(fullname);
        });
      });
  }

  onDeleteTeacher(fullname){
    let json = {'fullname':fullname};
    this.service.httpPut('/api/v1/tbTeacherProfile/department/delete/'+this.userName,json).then((res:IResponse)=>{
      if ((res.responseCode || 500) != 200) {
        Swal.fire(
          this.translate.instant('message.delete_error'),
          res.responseMsg,
          'error'
        )
        return;
      }
      Swal.fire(
        this.translate.instant('alert.delete_header'),
        this.translate.instant('message.delete'),
        'success'
      ).then(()=>this.reload());
    });
  }

  reload(){
    this.searchForm.controls.yearGroupName.setValue(this.searchForm.value.yearGroupName);
    this.searchForm.controls.catClassId.setValue(this.searchForm.value.catClassId);
    this.onSearch();
  }
  
}
