import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ApiService, IResponse } from 'src/app/shared/service/api.service';
import { MatDialog, MatTableDataSource, MatPaginator } from '@angular/material';
import { Router } from '@angular/router';
import {SelectionModel} from '@angular/cdk/collections';
  
// sweetalert2
import Swal from 'sweetalert2/dist/sweetalert2.js';
import 'sweetalert2/src/sweetalert2.scss';
import { UtilService } from 'src/app/_util/util.service';
import { TranslateService } from '@ngx-translate/core';
// sweetalert2 

@Component({
  selector: 'app-tec001',
  templateUrl: './tec001.component.html',
  styleUrls: ['./tec001.component.scss']
})
export class Tec001Component implements OnInit {  

  isProcess:boolean = false;
  searchForm = this.fb.group({
   'teacherCardId':['']
    , 'scCode':[''] //พระมารดา
    ,'firstnameTh':['']//ชื่อ ไทย
    , 'lastnameTh':['']//นามสกุล ไทย
    , 'departmentId':['']
    , 'classId':['']
    , 'p':['']
    , 'result':[10] 
  });
 
  form = this.fb.group({
    'teacherId':['']
  });

  formClass = this.fb.group({
    'classId':['']
  });

  formDepartment = this.fb.group({
  'departmentId':['']
  });

  formDepartmentclass = this.fb.group({
    'departmentId':['']
   ,'classId':['']
  });

  formDepartmentclassfirst = this.fb.group({
    'departmentId':['']
    ,'classId':['']
    ,'firstnameTh':['']
  });

  formFirst = this.fb.group({
    'firstnameTh':['']
    , 'departmentId':['']
  });

  formFirstClass = this.fb.group({
    'firstnameTh':['']
    , 'classId':['']
  });

  formFirstLastDepartmentClass = this.fb.group({
    'firstnameTh':['']
    , 'classId':['']
    , 'departmentId':['']
    , 'lastnameTh':['']
  });

  formFirstLastDepartment = this.fb.group({
    'firstnameTh':['']
    , 'departmentId':['']
    , 'lastnameTh':['']
  });

  formLastClassId = this.fb.group({
     'lastnameTh':['']
     , 'classId':['']
  });
 
  formLastdepartment = this.fb.group({
    'lastnameTh':['']
    , 'departmentId':['']
 });

 formLastdepartmentclass = this.fb.group({
  'lastnameTh':['']
  , 'departmentId':['']
  , 'classId':['']
 });

  classList: [];
  departmentList: [];
  selectedNo: number;

  day_str:any;
  day_date_now:any;

  selection = new SelectionModel(true, []);
  pageSize:number;
  displayedColumns: string[] = ['no','teacherCardId','fullname','departmentName','classRoomId','updatedUser','updatedDate','licenseNo','licenseExpired','startWorkDate','recordStatus'];
  dataSource = new MatTableDataSource();

  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  constructor(private fb: FormBuilder,
    private service:ApiService,
    public dialog: MatDialog, 
    private router: Router,
    private translate: TranslateService, 
    public util: UtilService
  ) { }

  ngOnInit() {
    this.ddlTbMstClass();
    this.ddlTbMstDepartment();
    this.onSearch(0);
  }

  onSearch(e){
    this.isProcess = true;
    this.searchForm.value.p = (this.paginator.pageIndex = e.pageIndex||0)+1;
    this.service.httpGet('/api/v1/teacherProfile',this.searchForm.value).then((res:IResponse)=>{
      this.dataSource.data = res.responseData||[];
      this.pageSize = res.responseSize||0;
    });
  }

  countDob(str){
    let convert_str = this.util.transform(str); 
    let mounth_str= parseInt(convert_str.substr(-7,2));
    let year_str = parseInt(convert_str.substr(-4))-543;

    let date_now = new Date();
    let convert_date_now = this.util.transform(date_now);
    let mounth_date_now  = parseInt(convert_date_now.substr(-7,2));
    let year_date_now  = parseInt(convert_date_now.substr(-4))-543;

    if(convert_str.length == 9){
        this.day_str = parseInt(convert_str.substr(-9,1));
    }else{
        this.day_str = parseInt(convert_str.substr(-10,2));
    }
    
    if(convert_date_now.length == 9){
        this.day_date_now = parseInt(convert_date_now.substr(-9,1));
    }else{
        this.day_date_now = parseInt(convert_date_now.substr(-10,2));
    }
    
    let cal_mounth = ((year_date_now*12)+mounth_date_now)-((year_str*12)+mounth_str);
    let cal_day = this.day_date_now-this.day_str;
    let convert_cal_day = cal_day.toString();
    if(convert_cal_day.substr(0,1) == '-'){
        return Math.floor((cal_mounth-1)/12)+'  ปี  '+((cal_mounth-1)%12)+'  เดือน'
    }else{
        return Math.floor(cal_mounth/12)+'  ปี  '+(cal_mounth%12)+'  เดือน'
    }
}

  onForm(id,mode){
    this.router.navigate(['/tec/tec001/form'],{ queryParams: {id:id,modeType:mode}});
  }

  onPrint(){
    window.open("assets/img/Doc1 (1).pdf");
  }

  onCancle(){
    this.searchForm.controls.firstnameTh.setValue('');
    this.searchForm.controls.lastnameTh.setValue('');
    this.searchForm.controls.departmentId.setValue('');
    this.searchForm.controls.classId.setValue('');
    this.onSearch(0);
  }

  onDelete(e){
    Swal.fire({
      title: this.translate.instant('alert.delete'),
      icon: 'info',
      showCancelButton: true,
      confirmButtonColor: '#2085d6',
      cancelButtonVolor: '#d33',
      confirmButtonText: this.translate.instant('psms.DL0008'),
      cancelButtonText: this.translate.instant('psms.DL0009')
    }).then((result) => {
      if(result.dismiss == 'cancel'){
        return;
      }
    this.isProcess = true;
    this.service.httpDelete('/api/v1/teacherProfile',{'teacherId':e.teacher_id}).then((res:IResponse)=>{
      this.isProcess = false;
      if ((res.responseCode || 500) != 200) {
        Swal.fire(
          this.translate.instant('message.delete_error'),
          res.responseMsg,
          'error'
        )
        return;
      }
      Swal.fire(
        this.translate.instant('alert.delete_header'),
        this.translate.instant('message.delete'),
        'success'
      )
      this.onSearch(0);
    });
  });
  }

  ddlTbMstClass(){
    this.service.httpGet('/api/v1/0/tbMstClass/ddl',null).then((res)=>{
      this.classList = res||[];
    });
  }

  ddlTbMstDepartment(){
    this.service.httpGet('/api/v1/0/department/ddl',null).then((res)=>{
      this.departmentList = res||[];
    });
  }

  onReport(id){
    this.isProcess = true;
    this.form.controls.teacherId.setValue(id);
    this.service.httpReportPDF('/api/v1/tec/report/pdf','ทะเบียนประวัติคุณครู',this.form.value).then((res)=>{
      this.isProcess = false;
    });
  }

  onReportPreview(){
    if(this.searchForm.value.firstnameTh == '' && this.searchForm.value.lastnameTh == '' && this.searchForm.value.classId == '' && this.searchForm.value.departmentId == ''){
      this.service.httpPreviewPDF('/api/v1/teacherreport/report/pdf',this.searchForm.value).then((res)=>{
        this.isProcess = false;
        window.location.href = res;
      });
    }
    if(this.searchForm.value.firstnameTh != '' && this.searchForm.value.lastnameTh == '' && this.searchForm.value.classId == '' && this.searchForm.value.departmentId == ''){
      this.service.httpPreviewPDF('/api/v1/teacherreport/report/pdf',this.searchForm.value).then((res)=>{
        this.isProcess = false;
        window.location.href = res;
      });
    }
    if(this.searchForm.value.firstnameTh == '' && this.searchForm.value.lastnameTh != '' && this.searchForm.value.classId == '' && this.searchForm.value.departmentId == ''){
      this.service.httpPreviewPDF('/api/v1/teacherreport/report/pdf',this.searchForm.value).then((res)=>{
        this.isProcess = false;
        window.location.href = res;
      });
    }
    if(this.searchForm.value.firstnameTh != '' && this.searchForm.value.lastnameTh != '' && this.searchForm.value.classId == '' && this.searchForm.value.departmentId == ''){
      this.service.httpPreviewPDF('/api/v1/teacherreport/report/pdf',this.searchForm.value).then((res)=>{
        this.isProcess = false;
        window.location.href = res;
      });
    } 
    if ( this.searchForm.value.classId != '' && this.searchForm.value.departmentId == '' && this.searchForm.value.firstnameTh == '' && this.searchForm.value.lastnameTh == '' ){
      this.formClass.controls.classId.setValue(this.searchForm.value.classId)
      this.service.httpPreviewPDF('/api/v1/teacherreport/report/pdfclass',this.formClass.value).then((res)=>{
        this.isProcess = false;
        window.location.href = res;
      });
    }
    if (this.searchForm.value.departmentId != '' && this.searchForm.value.classId == '' && this.searchForm.value.firstnameTh == '' && this.searchForm.value.lastnameTh == ''){
      this.formDepartment.controls.departmentId.setValue(this.searchForm.value.departmentId)
      this.service.httpPreviewPDF('/api/v1/teacherreport/report/pdfdepartment',this.formDepartment.value).then((res)=>{
        this.isProcess = false;
        window.location.href = res;
      });
    }
    if (this.searchForm.value.departmentId != '' &&  this.searchForm.value.classId != '' && this.searchForm.value.firstnameTh == '' && this.searchForm.value.lastnameTh == '' ){
      this.formDepartmentclass.controls.departmentId.setValue(this.searchForm.value.departmentId)
      this.formDepartmentclass.controls.classId.setValue(this.searchForm.value.classId)    
      this.service.httpPreviewPDF('/api/v1/teacherreport/report/pdfdepartmentclass',this.formDepartmentclass.value).then((res)=>{
        this.isProcess = false;
        window.location.href = res;
      });
    }
    if (this.searchForm.value.departmentId != '' &&  this.searchForm.value.classId != '' && this.searchForm.value.firstnameTh != '' && this.searchForm.value.lastnameTh == ''){
      this.formDepartmentclassfirst.controls.departmentId.setValue(this.searchForm.value.departmentId)
      this.formDepartmentclassfirst.controls.classId.setValue(this.searchForm.value.classId)
      this.formDepartmentclassfirst.controls.firstnameTh.setValue(this.searchForm.value.firstnameTh)  
      this.service.httpPreviewPDF('/api/v1/teacherreport/report/pdfdepartmentclassfrist',this.formDepartmentclassfirst.value).then((res)=>{
        this.isProcess = false;
        window.location.href = res;
      });
    }

    if (this.searchForm.value.departmentId != '' && this.searchForm.value.firstnameTh != '' && this.searchForm.value.classId ==   '' && this.searchForm.value.lastnameTh == ''){
      this.formFirst.controls.departmentId.setValue(this.searchForm.value.departmentId)
      this.formFirst.controls.firstnameTh.setValue(this.searchForm.value.firstnameTh)  
      this.service.httpPreviewPDF('/api/v1/teacherreport/report/pdffirst',this.formFirst.value).then((res)=>{
        this.isProcess = false;
        window.location.href = res;
      });
    }
    if (this.searchForm.value.departmentId == '' && this.searchForm.value.firstnameTh != '' && this.searchForm.value.classId != '' && this.searchForm.value.lastnameTh == ''){
      this.formFirstClass.controls.classId.setValue(this.searchForm.value.classId)
      this.formFirstClass.controls.firstnameTh.setValue(this.searchForm.value.firstnameTh)  
      this.service.httpPreviewPDF('/api/v1/teacherreport/report/pdffirstclass',this.formFirstClass.value).then((res)=>{
        this.isProcess = false;
        window.location.href = res;
      });
    }

    if (this.searchForm.value.departmentId != '' && this.searchForm.value.firstnameTh != '' && this.searchForm.value.classId != '' && this.searchForm.value.lastnameTh != ''){
      this.formFirstLastDepartmentClass.controls.classId.setValue(this.searchForm.value.classId)
      this.formFirstLastDepartmentClass.controls.firstnameTh.setValue(this.searchForm.value.firstnameTh)  
      this.formFirstLastDepartmentClass.controls.departmentId.setValue(this.searchForm.value.departmentId)  
      this.formFirstLastDepartmentClass.controls.lastnameTh.setValue(this.searchForm.value.lastnameTh)  
      this.service.httpPreviewPDF('/api/v1/teacherreport/report/pdffirstlastdepartmentclass',this.formFirstLastDepartmentClass.value).then((res)=>{
        this.isProcess = false;
        window.location.href = res;
      });
    }

    if (this.searchForm.value.departmentId != '' && this.searchForm.value.firstnameTh != '' && this.searchForm.value.classId == '' && this.searchForm.value.lastnameTh != ''){
      this.formFirstLastDepartment.controls.firstnameTh.setValue(this.searchForm.value.firstnameTh)  
      this.formFirstLastDepartment.controls.departmentId.setValue(this.searchForm.value.departmentId)  
      this.formFirstLastDepartment.controls.lastnameTh.setValue(this.searchForm.value.lastnameTh)  
      this.service.httpPreviewPDF('/api/v1/teacherreport/report/pdffirstlastdepartment',this.formFirstLastDepartment.value).then((res)=>{
        this.isProcess = false;
        window.location.href = res;
      });
    }

    if (this.searchForm.value.classId != '' && this.searchForm.value.lastnameTh != '' && this.searchForm.value.departmentId == '' && this.searchForm.value.firstnameTh == '' ){
      this.formLastClassId.controls.lastnameTh.setValue(this.searchForm.value.lastnameTh)  
      this.formLastClassId.controls.classId.setValue(this.searchForm.value.classId)
      this.service.httpPreviewPDF('/api/v1/teacherreport/report/pdflastClass',this.formLastClassId.value).then((res)=>{
        this.isProcess = false;
        window.location.href = res;
      });
    }

    if (this.searchForm.value.classId == '' && this.searchForm.value.lastnameTh != '' && this.searchForm.value.departmentId != '' && this.searchForm.value.firstnameTh == '' ){
      this.formLastdepartment.controls.lastnameTh.setValue(this.searchForm.value.lastnameTh)  
      this.formLastdepartment.controls.departmentId.setValue(this.searchForm.value.departmentId)
      this.service.httpPreviewPDF('/api/v1/teacherreport/report/pdflastdepartment',this.formLastdepartment.value).then((res)=>{
        this.isProcess = false;
        window.location.href = res;
      });
    }
    if (this.searchForm.value.classId != '' && this.searchForm.value.lastnameTh != '' && this.searchForm.value.departmentId != '' && this.searchForm.value.firstnameTh == '' ){
      this.formLastdepartmentclass.controls.lastnameTh.setValue(this.searchForm.value.lastnameTh)  
      this.formLastdepartmentclass.controls.departmentId.setValue(this.searchForm.value.departmentId)
      this.formLastdepartmentclass.controls.classId.setValue(this.searchForm.value.classId)
      this.service.httpPreviewPDF('/api/v1/teacherreport/report/pdflastdepartmentclass',this.formLastdepartmentclass.value).then((res)=>{
        this.isProcess = false;
      });
    }    
  }

  onReportPDF(){
    if(this.searchForm.value.firstnameTh == '' && this.searchForm.value.lastnameTh == '' && this.searchForm.value.classId == '' && this.searchForm.value.departmentId == ''){
      this.service.httpReportPDF('/api/v1/teacherreport/report/pdf',"tec001",this.searchForm.value).then((res)=>{
        this.isProcess = false;
      });
    }

    if(this.searchForm.value.firstnameTh != '' && this.searchForm.value.lastnameTh == '' && this.searchForm.value.classId == '' && this.searchForm.value.departmentId == ''){
      this.service.httpReportPDF('/api/v1/teacherreport/report/pdf',"tec001",this.searchForm.value).then((res)=>{
        this.isProcess = false;
      
      });
    }

    if(this.searchForm.value.firstnameTh == '' && this.searchForm.value.lastnameTh != '' && this.searchForm.value.classId == '' && this.searchForm.value.departmentId == ''){
      this.service.httpReportPDF('/api/v1/teacherreport/report/pdf',"tec001",this.searchForm.value).then((res)=>{
        this.isProcess = false;
     
      });
    }

    if(this.searchForm.value.firstnameTh != '' && this.searchForm.value.lastnameTh != '' && this.searchForm.value.classId == '' && this.searchForm.value.departmentId == ''){
      this.service.httpReportPDF('/api/v1/teacherreport/report/pdf',"tec001",this.searchForm.value).then((res)=>{
        this.isProcess = false;
       
      });
    } 
  
    if ( this.searchForm.value.classId != '' && this.searchForm.value.departmentId == '' && this.searchForm.value.firstnameTh == '' && this.searchForm.value.lastnameTh == '' ){
      this.formClass.controls.classId.setValue(this.searchForm.value.classId)
      this.service.httpReportPDF('/api/v1/teacherreport/report/pdfclass',"tec001",this.formClass.value).then((res)=>{
        this.isProcess = false;
        
      });
    }

    if (this.searchForm.value.departmentId != '' && this.searchForm.value.classId == '' && this.searchForm.value.firstnameTh == '' && this.searchForm.value.lastnameTh == ''){
      this.formDepartment.controls.departmentId.setValue(this.searchForm.value.departmentId)
      this.service.httpReportPDF('/api/v1/teacherreport/report/pdfdepartment',"tec001",this.formDepartment.value).then((res)=>{
        this.isProcess = false;
      });
    }

    if (this.searchForm.value.departmentId != '' &&  this.searchForm.value.classId != '' && this.searchForm.value.firstnameTh == '' && this.searchForm.value.lastnameTh == '' ){
      this.formDepartmentclass.controls.departmentId.setValue(this.searchForm.value.departmentId)
      this.formDepartmentclass.controls.classId.setValue(this.searchForm.value.classId)   
      this.service.httpReportPDF('/api/v1/teacherreport/report/pdfdepartmentclass',"tec001",this.formDepartmentclass.value).then((res)=>{
        this.isProcess = false;
      });
    }

    if (this.searchForm.value.departmentId != '' &&  this.searchForm.value.classId != '' && this.searchForm.value.firstnameTh != '' && this.searchForm.value.lastnameTh == ''){
      this.formDepartmentclassfirst.controls.departmentId.setValue(this.searchForm.value.departmentId)
      this.formDepartmentclassfirst.controls.classId.setValue(this.searchForm.value.classId)
      this.formDepartmentclassfirst.controls.firstnameTh.setValue(this.searchForm.value.firstnameTh)  
      this.service.httpReportPDF('/api/v1/teacherreport/report/pdfdepartmentclassfrist',"tec001",this.formDepartmentclassfirst.value).then((res)=>{
        this.isProcess = false;
      });
    }

    if (this.searchForm.value.departmentId != '' && this.searchForm.value.firstnameTh != '' && this.searchForm.value.classId ==   '' && this.searchForm.value.lastnameTh == ''){
      this.formFirst.controls.departmentId.setValue(this.searchForm.value.departmentId)
      this.formFirst.controls.firstnameTh.setValue(this.searchForm.value.firstnameTh)  
      this.service.httpReportPDF('/api/v1/teacherreport/report/pdffirst',"tec001",this.formFirst.value).then((res)=>{
        this.isProcess = false;
      });
    }

    if (this.searchForm.value.departmentId == '' && this.searchForm.value.firstnameTh != '' && this.searchForm.value.classId != '' && this.searchForm.value.lastnameTh == ''){
      this.formFirstClass.controls.classId.setValue(this.searchForm.value.classId)
      this.formFirstClass.controls.firstnameTh.setValue(this.searchForm.value.firstnameTh)  
      this.service.httpReportPDF('/api/v1/teacherreport/report/pdffirstclass',"tec001",this.formFirstClass.value).then((res)=>{
        this.isProcess = false;
      });
    }

    if (this.searchForm.value.departmentId != '' && this.searchForm.value.firstnameTh != '' && this.searchForm.value.classId != '' && this.searchForm.value.lastnameTh != ''){
      this.formFirstLastDepartmentClass.controls.classId.setValue(this.searchForm.value.classId)
      this.formFirstLastDepartmentClass.controls.firstnameTh.setValue(this.searchForm.value.firstnameTh)  
      this.formFirstLastDepartmentClass.controls.departmentId.setValue(this.searchForm.value.departmentId)  
      this.formFirstLastDepartmentClass.controls.lastnameTh.setValue(this.searchForm.value.lastnameTh)  
      this.service.httpReportPDF('/api/v1/teacherreport/report/pdffirstlastdepartmentclass',"tec001",this.formFirstLastDepartmentClass.value).then((res)=>{
        this.isProcess = false;
      });
    }

    if (this.searchForm.value.departmentId != '' && this.searchForm.value.firstnameTh != '' && this.searchForm.value.classId == '' && this.searchForm.value.lastnameTh != ''){
      this.formFirstLastDepartment.controls.firstnameTh.setValue(this.searchForm.value.firstnameTh)  
      this.formFirstLastDepartment.controls.departmentId.setValue(this.searchForm.value.departmentId)  
      this.formFirstLastDepartment.controls.lastnameTh.setValue(this.searchForm.value.lastnameTh)  
      this.service.httpReportPDF('/api/v1/teacherreport/report/pdffirstlastdepartment',"tec001",this.formFirstLastDepartment.value).then((res)=>{
        this.isProcess = false;
      });
    }

    if (this.searchForm.value.departmentId == '' && this.searchForm.value.firstnameTh == '' && this.searchForm.value.classId != '' && this.searchForm.value.lastnameTh != ''){
      this.formLastClassId.controls.lastnameTh.setValue(this.searchForm.value.lastnameTh)  
      this.formLastClassId.controls.classId.setValue(this.searchForm.value.classId)
      this.service.httpReportPDF('/api/v1/teacherreport/report/pdflastClass',"tec001",this.formLastClassId.value).then((res)=>{
        this.isProcess = false;
      });
    }

    if (this.searchForm.value.classId == '' && this.searchForm.value.lastnameTh != '' && this.searchForm.value.departmentId != '' && this.searchForm.value.firstnameTh == '' ){
      this.formLastdepartment.controls.lastnameTh.setValue(this.searchForm.value.lastnameTh)  
      this.formLastdepartment.controls.departmentId.setValue(this.searchForm.value.departmentId)
      this.service.httpReportPDF('/api/v1/teacherreport/report/pdflastdepartment',"tec001",this.formLastdepartment.value).then((res)=>{
        this.isProcess = false;
      });
    }

    if (this.searchForm.value.classId != '' && this.searchForm.value.lastnameTh != '' && this.searchForm.value.departmentId != '' && this.searchForm.value.firstnameTh == '' ){
      this.formLastdepartmentclass.controls.lastnameTh.setValue(this.searchForm.value.lastnameTh)  
      this.formLastdepartmentclass.controls.departmentId.setValue(this.searchForm.value.departmentId)
      this.formLastdepartmentclass.controls.classId.setValue(this.searchForm.value.classId)
      this.service.httpReportPDF('/api/v1/teacherreport/report/pdflastdepartmentclass',"tec001",this.formLastdepartmentclass.value).then((res)=>{
        this.isProcess = false;
      });
    }
  }

  onReportXls(){
    if(this.searchForm.value.firstnameTh == '' && this.searchForm.value.lastnameTh == '' && this.searchForm.value.classId == '' && this.searchForm.value.departmentId == ''){
      this.service.httpReportXLS('/api/v1/teacherreport/report/xls',"tec001",this.searchForm.value).then((res)=>{
        this.isProcess = false;
      });
    }

    if(this.searchForm.value.firstnameTh != '' && this.searchForm.value.lastnameTh == '' && this.searchForm.value.classId == '' && this.searchForm.value.departmentId == ''){
      this.service.httpReportXLS('/api/v1/teacherreport/report/xls',"tec001",this.searchForm.value).then((res)=>{
        this.isProcess = false;
     
      });
    }

    if(this.searchForm.value.firstnameTh == '' && this.searchForm.value.lastnameTh != '' && this.searchForm.value.classId == '' && this.searchForm.value.departmentId == ''){
      this.service.httpReportXLS('/api/v1/teacherreport/report/xls',"tec001",this.searchForm.value).then((res)=>{
        this.isProcess = false;
      });
    }

    if(this.searchForm.value.firstnameTh != '' && this.searchForm.value.lastnameTh != '' && this.searchForm.value.classId == '' && this.searchForm.value.departmentId == ''){
      this.service.httpReportXLS('/api/v1/teacherreport/report/xls',"tec001",this.searchForm.value).then((res)=>{
        this.isProcess = false;
      });
    }
  
    if ( this.searchForm.value.classId != '' && this.searchForm.value.departmentId == '' && this.searchForm.value.firstnameTh == '' && this.searchForm.value.lastnameTh == '' ){
      this.formClass.controls.classId.setValue(this.searchForm.value.classId)
      this.service.httpReportXLS('/api/v1/teacherreport/report/xlsclass',"tec001",this.formClass.value).then((res)=>{
        this.isProcess = false;
      });
    }

    if (this.searchForm.value.departmentId != '' && this.searchForm.value.classId == '' && this.searchForm.value.firstnameTh == '' && this.searchForm.value.lastnameTh == ''){
      this.formDepartment.controls.departmentId.setValue(this.searchForm.value.departmentId)
      this.service.httpReportXLS('/api/v1/teacherreport/report/xlsdepartment',"tec001",this.formDepartment.value).then((res)=>{
        this.isProcess = false;
      });
    }

    if (this.searchForm.value.departmentId != '' &&  this.searchForm.value.classId != '' && this.searchForm.value.firstnameTh == '' && this.searchForm.value.lastnameTh == '' ){
      this.formDepartmentclass.controls.departmentId.setValue(this.searchForm.value.departmentId)
      this.formDepartmentclass.controls.classId.setValue(this.searchForm.value.classId)    
      this.service.httpReportXLS('/api/v1/teacherreport/report/xlsdepartmentclass',"tec001",this.formDepartmentclass.value).then((res)=>{
        this.isProcess = false;
      });
    }

    if (this.searchForm.value.departmentId != '' &&  this.searchForm.value.classId != '' && this.searchForm.value.firstnameTh != '' && this.searchForm.value.lastnameTh == ''){
      this.formDepartmentclassfirst.controls.departmentId.setValue(this.searchForm.value.departmentId)
      this.formDepartmentclassfirst.controls.classId.setValue(this.searchForm.value.classId)
      this.formDepartmentclassfirst.controls.firstnameTh.setValue(this.searchForm.value.firstnameTh)  
      this.service.httpReportXLS('/api/v1/teacherreport/report/xlsdepartmentclassfirst',"tec001",this.formDepartmentclassfirst.value).then((res)=>{
        this.isProcess = false;
      });
    }

    if (this.searchForm.value.departmentId != '' && this.searchForm.value.firstnameTh != '' && this.searchForm.value.classId ==   '' && this.searchForm.value.lastnameTh == ''){
      this.formFirst.controls.departmentId.setValue(this.searchForm.value.departmentId)
      this.formFirst.controls.firstnameTh.setValue(this.searchForm.value.firstnameTh)  
      this.service.httpReportXLS('/api/v1/teacherreport/report/xlsfirst',"tec001",this.formFirst.value).then((res)=>{
        this.isProcess = false;
      });
    }

    if (this.searchForm.value.departmentId == '' && this.searchForm.value.firstnameTh != '' && this.searchForm.value.classId != '' && this.searchForm.value.lastnameTh == ''){
      this.formFirstClass.controls.classId.setValue(this.searchForm.value.classId)
      this.formFirstClass.controls.firstnameTh.setValue(this.searchForm.value.firstnameTh)  
      this.service.httpReportXLS('/api/v1/teacherreport/report/xlsfirstclass',"tec001",this.formFirstClass.value).then((res)=>{
        this.isProcess = false;
      });
    }

    if (this.searchForm.value.departmentId != '' && this.searchForm.value.firstnameTh != '' && this.searchForm.value.classId != '' && this.searchForm.value.lastnameTh != ''){
      this.formFirstLastDepartmentClass.controls.classId.setValue(this.searchForm.value.classId)
      this.formFirstLastDepartmentClass.controls.firstnameTh.setValue(this.searchForm.value.firstnameTh)  
      this.formFirstLastDepartmentClass.controls.departmentId.setValue(this.searchForm.value.departmentId)  
      this.formFirstLastDepartmentClass.controls.lastnameTh.setValue(this.searchForm.value.lastnameTh)  
      this.service.httpReportXLS('/api/v1/teacherreport/report/xlsfirstlastdepartmentclass',"tec001",this.formFirstLastDepartmentClass.value).then((res)=>{
        this.isProcess = false;
      });
    }

    if (this.searchForm.value.departmentId != '' && this.searchForm.value.firstnameTh != '' && this.searchForm.value.classId == '' && this.searchForm.value.lastnameTh != ''){
      this.formFirstLastDepartment.controls.firstnameTh.setValue(this.searchForm.value.firstnameTh)  
      this.formFirstLastDepartment.controls.departmentId.setValue(this.searchForm.value.departmentId)  
      this.formFirstLastDepartment.controls.lastnameTh.setValue(this.searchForm.value.lastnameTh)  
      this.service.httpReportXLS('/api/v1/teacherreport/report/xlsfirstlastdepartment',"tec001",this.formFirstLastDepartment.value).then((res)=>{
        this.isProcess = false;
      });
    }

    if (this.searchForm.value.departmentId == '' && this.searchForm.value.firstnameTh == '' && this.searchForm.value.classId != '' && this.searchForm.value.lastnameTh != ''){
      this.formLastClassId.controls.lastnameTh.setValue(this.searchForm.value.lastnameTh)  
      this.formLastClassId.controls.classId.setValue(this.searchForm.value.classId)
      this.service.httpReportXLS('/api/v1/teacherreport/report/xlslastclasss',"tec001",this.formLastClassId.value).then((res)=>{
        this.isProcess = false;
      });
    }

    if (this.searchForm.value.classId == '' && this.searchForm.value.lastnameTh != '' && this.searchForm.value.departmentId != '' && this.searchForm.value.firstnameTh == '' ){
      this.formLastdepartment.controls.lastnameTh.setValue(this.searchForm.value.lastnameTh)  
      this.formLastdepartment.controls.departmentId.setValue(this.searchForm.value.departmentId)
      this.service.httpReportXLS('/api/v1/teacherreport/report/xlslastdepartment',"tec001",this.formLastdepartment.value).then((res)=>{
        this.isProcess = false;
      });
    }

    if (this.searchForm.value.classId != '' && this.searchForm.value.lastnameTh != '' && this.searchForm.value.departmentId != '' && this.searchForm.value.firstnameTh == '' ){
      this.formLastdepartmentclass.controls.lastnameTh.setValue(this.searchForm.value.lastnameTh)  
      this.formLastdepartmentclass.controls.departmentId.setValue(this.searchForm.value.departmentId)
      this.formLastdepartmentclass.controls.classId.setValue(this.searchForm.value.classId)
      this.service.httpReportXLS('/api/v1/teacherreport/report/xlslastdepartmentclass',"tec001",this.formLastdepartmentclass.value).then((res)=>{
        this.isProcess = false;
      });
    }
  }

}
