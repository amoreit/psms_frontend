import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ApiService, IResponse } from 'src/app/shared/service/api.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import Swal from 'sweetalert2/dist/sweetalert2.js';
import 'sweetalert2/src/sweetalert2.scss';
import { UtilService } from 'src/app/_util/util.service';
import { TranslateService } from '@ngx-translate/core';

 
@Component({
  selector: 'app-tec001-form',
  templateUrl: './tec001-form.component.html',
  styleUrls: ['./tec001-form.component.scss']
})
export class Tec001FormComponent implements OnInit {

  isProcess:boolean = false; 
  FormGroup = this.fb.group({ 
      'teacherId':['']//primary key
    , 'citizenId':['',Validators.required] //เลขที่บัตรประชาชน
    , 'titleId':['',Validators.required] //คำนำหน้าชื่อไทย
    , 'titleIdEn':['',Validators.required] //คำนำหน้าชื่อ eng
    , 'scCode':[''] //พระมารดา
    , 'firstnameTh':['',Validators.required]//ชื่อ ไทย
    , 'lastnameTh':['',Validators.required]//นามสกุล ไทย
    , 'firstnameEn':['',Validators.required]//ชื่อ ไทย
    , 'lastnameEn':['',Validators.required]//นามสกุล ไทย
    , 'nickname':['']//ชื่อเล่น
    , 'fullname':['']//ชื่อเต็ม
    , 'fullnameEn':['']//ชื่อ ไทย
    , 'teacherCardId':['',Validators.required]
    , 'gender':['',Validators.required]//เพศ
    , 'ethnicity':['',Validators.required]//เชื้อชาติ
    , 'citizenship':['',Validators.required]//สัญชาติ
    , 'religion':['',Validators.required]//ศาสนา
    , 'saint':[''] //ชื่อนักบุญ
    , 'dob':['',Validators.required] //วันเกิด
    , 'data1':[''] 
    , 'teacherImage':['']
    
    //ที่อยู่ตามทะเบียนบ้าน
    , 'regSubDistrict':['',Validators.required]//ตำบล
    , 'regDistrict':['',Validators.required]//อำเภอ
    , 'regProvince':['',Validators.required]//จังหวัด
    , 'regCountry':['',Validators.required]//ประเทศ.
    , 'regPostCode':['',Validators.required]//ไปรษณีย์
    , 'regHouseNo':['',Validators.required]//เลขรหัสประจำบ้าน
    , 'regAddrNo':['',Validators.required]//ที่อยู่ตามทะเบียนบ้าน
    , 'regVillage':['']//หมู่บ้าน
    , 'regVillageNo':['',Validators.required]//หมู่.
    , 'regRoad':['']//ถนน
    , 'regAlley':['']//ซอย

    //ที่อยู่ปัจจุบัน
    , 'curAddrNo':['',Validators.required]//ที่อยู่ปัจจุบัน
    , 'curVillage':['']//หมู่บ้าน
    , 'curVillageNo':['',Validators.required]//หมู่
    , 'curAlley':['']//ซอย
    , 'curRoad':['']//ถนน
    , 'curSubDistrict':['',Validators.required]//ตำบล
    , 'curDistrict':['',Validators.required]//อำเภอ
    , 'curProvince':['',Validators.required]//จังหวัด
    , 'curCountry':['',Validators.required]//ประเทศ.
    , 'curPostCode':['',Validators.required]//ไปรษณีย์

    //ข้อมูลบิดา
    , 'fatherTitle':['']//คำนำหน้าชื่อบิดา
    , 'fatherFirstnameTh':['']//ชื่อบิดา
    , 'fatherLastnameTh':['']//นามสกุลบิดา

    //ข้อมูลมารดา
    , 'motherTitle':['']//คำนำหน้าชื่อมารดา
    , 'motherFirstnameTh':['']//ชื่อบิมารดา
    , 'motherLastnameTh':['']//นามสกุลมารดา 

    //ข้อมูลคู่สมรส
    , 'spouseTitle':['']//ที่อยู่ปัจจุบัน
    , 'spouseFirstname':['']//หมู่บ้าน
    , 'spouseLastname':['']//หมู่
    , 'spouseReligion':['']//ถนน
    , 'spouseSaint':['']//ตำบล
    , 'spouseChurch':['']//อำเภอ
    , 'spouseOffice':['']//จังหวัด
    , 'spouseTel1':['']//ประเทศ.
    , 'spouseTel2':['']//ไปรษณีย์

    , 'noOfChild':['']

    //ข้อมูลบุตรคนที่1
    , 'childTitle1':['']//คำนำหน้าชื่อบุตร1
    , 'childFirstname1':['']//ชื่อบุตร1
    , 'childLastname1':['']//นามสกุลบุตร1
    , 'childGender1':['']//เพศบุตร1
    , 'childDob1':['']//วันเกิดบุตร1
    , 'childClass1':['']//ชั้นเรียนบุตร1

     //ข้อมูลบุตรคนที่2
     , 'childTitle2':['']//คำนำหน้าชื่อบุตร2
     , 'childFirstname2':['']//ชื่อบุตร2
     , 'childLastname2':['']//นามสกุลบุตร2
     , 'childGender2':['']//เพศบุตร2
     , 'childDob2':['']//วันเกิดบุตร2
     , 'childClass2':['']//ชั้นเรียนบุตร2

      //ข้อมูลบุตรคนที่3
    , 'childTitle3':['']//คำนำหน้าชื่อบุตร3
    , 'childFirstname3':['']//ชื่อบุตร3
    , 'childLastname3':['']//นามสกุลบุตร3
    , 'childGender3':['']//เพศบุตร3
    , 'childDob3':['']//วันเกิดบุตร3
    , 'childClass3':['']//ชั้นเรียนบุตร3
  
    //ข้อมูลบรรจุ
    , 'startWorkDate':['',Validators.required]//วันที่เข้าทำงานวันแรก
    , 'cal_swd':[''] //อายุงาน
    , 'registeredDate':['',Validators.required]//วันที่ได้รับบรรจุ
    , 'licenseNo':['',Validators.required]
    , 'licenseStart':['',Validators.required]
    , 'licenseExpired':['',Validators.required]
    , 'positionName':['',Validators.required]
    , 'sectionName':['']
    , 'divisionName':['']
    , 'subDivisionName':['']
    , 'ispermanent':['']
    , 'isinstructor':['']
    , 'departmentName':['']
    , 'familyStatus':['']//สถานะ
    , 'education':['',Validators.required]//การศึกษา
    , 'educationDesc':['']//การศึกษา
    , 'scoutRank':['']//การศึกษา
    , 'email':['',Validators.required]//อีเมล
    , 'tel1':['']//เบอร์ที่ 1
    , 'tel2':['']//เบอร์ที่ 2
  });

  FormDepartment = this.fb.group({
    'departmentId':['']
  });

  FormClass = this.fb.group({
    'classId':['']
  });

  FormClassRoom = this.fb.group({
    'classRoomId':['']
  });

  firstFormGroup: FormGroup;
  secondFormGroup: FormGroup;
  thirdFormGroup: FormGroup;
  fourthFormGroup: FormGroup; 
  fifthFormGroup: FormGroup; 
  sixFormGroup: FormGroup; 

  checked_address = false; // checkbox ที่อยู่

  /* ที่อยู่ตามทะเบียนบ้านนักเรียน */
  reg_village: String; //ชื่อหมู่บ้าน (ตามทะเบัยนบ้าน)
  reg_house_no: String; //เลขประจำบ้าน (ตามทะเบัยนบ้าน)
  reg_addr_no: String; //เลขที่ (ตามทะเบัยนบ้าน)
  reg_village_no: String; //หมู่ที่ (ตามทะเบัยนบ้าน)
  reg_road: String; //ถนน (ตามทะเบัยนบ้าน)
  reg_country: String; //ประเทศ
  reg_province: String; //จังหวัด  (ตามทะเบัยนบ้าน)
  reg_district: String; //อำเภอ (ตามทะเบัยนบ้าน)
  reg_sub_district: String; //ตำบล/แขวง (ตามทะเบัยนบ้าน)
  reg_post_code: String; //รหัสไปรษณีย์ (ตามทะเบัยนบ้าน)

  /* ที่อยู่ปัจจุบันนักเรียน */
  cur_village: String; //ชื่อหมู่บ้าน (ตามที่อยู่ปัจจุบัน)
  cur_addr_no: String; //เลขที่ (ตามที่อยู่ปัจจุบัน)
  cur_village_no: String; //หมู่ที่ (ตามที่อยู่ปัจจุบัน)
  cur_road: String; //ถนน (ตามที่อยู่ปัจจุบัน)
  cur_country: String; //ประเทศ
  cur_province: String; //จังหวัด  (ตามที่อยู่ปัจจุบัน)
  cur_district: String; //อำเภอ (ตามที่อยู่ปัจจุบัน)
  cur_sub_district: String; //ตำบล/แขวง (ตามที่อยู่ปัจจุบัน)
  cur_post_code: String; //รหัสไปรษณีย์ (ตามที่อยู่ปัจจุบัน)
  

  // childDob1: Date; //วัน/เดือน/ปีเกิด
  // childDob2: Date; //วัน/เดือน/ปีเกิด
  // childDob3: Date; //วัน/เดือน/ปีเกิด

  titleList: [];
  Title2List:[];
  religionList: [];
  provinceList: [];
  districtList: [];
  subDistrictList: [];
  countryCodeList: [];
  GenderList: [];
  GenderChild1List: [];
  GenderChild2List: [];
  GenderChild3List: [];
  educationList: [];
  familyStatusList: [];
  TbMstChildTitleList:[];
  IspermanentList :[];
  sectionNameList:[];
  divisionNameList:[];
  subDivisionNameList:[];
  departmentNameList:[];
  ClassNameList:[];
  ClassRoomNameList:[];
  postCodeList:[];
  postCodeCurList:[];
  titleFatherList:[];
  titleMotherList:[];
  family_statusList: [];
  teacherCheckCardIdList:[];

  //ที่อยู่ตามทะเบียนบ้าน
  provinceRegList:[];
  districtRegList:[];
  subDistrictRegList:[];

  //ที่อยู่ปัจจุบัน
  provinceCurList:[];
  districtCurList:[];
  subDistrictCurList:[];

  citizenshipList:[];

  //ชั้นเรียนบุตร
  classList: [];

  //ชั้นเรียนบุตร
  positionList: [];
  startWorkDate: Date;
  licenseExpired: Date;
  registeredDate: Date;
  licenseStart: Date;
  fullname:String;
  fullnameEn:String;
  childDob1: Date;
  childDob2: Date;
  childDob3: Date;
  dob: Date;
  validateValue = false;  
  id = '';
  updatedUser = '';
  updatedDate :Date;
  modeType = '';
  teacher_card_id:any;
  check_citizenId : boolean;
  reg_district1: String ='';
  data1:String;
  show:any;
  showclass:any;
  showclassRoom:any;
  titleId:string;
  firstnameTh:string;
  lastnameTh:string;
  titleIdEn:string;
  firstnameEn:string;
  lastnameEn:string;
  titleEn:string;
  gender:string;
  citizenId:string;
  religion:string;
  ethnicity:string;
  citizenship:string;
  education:string;
  familyStatus:string;
  email:string;
  teacherCardId:string;
  position:string;
  regHouseNo:string;
  regAddrNo:string;
  regVillageNo:string;
  regCountry:string;
  regProvince:string;
  regDistrict:string;
  regSubDistrict:string;
  regPostCode:string;
  curVillage:string;
  curAddrNo:string;
  curVillageNo:string;
  curCountry:string;
  curProvince:string;
  curDistrict:string;
  curSubDistrict:string;
  curPostCode:string;
  mode :string;
  radio_ispermanent:any;
  radio_isInstructor:any;

  check:boolean;
  sectionName:string;
  divisionName:string;
  subDivisionName:string;
  classId:String;
  classRoomId:String;
  departmentName:String;

  day_str:any;
  day_date_now:any;
  headerType = '';

  constructor( private ar:ActivatedRoute,
               private router: Router ,
               private _formBuilder: FormBuilder,
               private service:ApiService,
               private fb: FormBuilder, 
               private translate: TranslateService,
               public util: UtilService
  ) {
    
    this.ddlTbMstTitle();
    this.ddlTbMstFatherTitle();
    this.ddlTbMstMotherTitle();
  }

  ngOnInit() {
    this.id = this.ar.snapshot.queryParamMap.get('id')||'';
    this.mode = this.ar.snapshot.queryParamMap.get('modeType')||'';
      if(this.id == ''){
        this.check = false;
        this.modeType = 'เพิ่ม';
        this.headerType = this.translate.instant('modeType.MT0007');
        this.translate.onLangChange.subscribe(() => {
          this.headerType = this.translate.instant('modeType.MT0007');
        });
        this.ddlTbMstTitle2();
        this.ddlTbMstReligion();
        this.ddlCitizenship();
        this.ddlTbMstClass();
        this.ddlTbMstPosition();
        this.ddlTbMstCountryCode();
        this.ddlTbMstEducationList();
        this.ddlTbMstFamilyStatusList();
        this.ddlRegTbMstProvince();
        this.ddlRegTbMstDistrict();
        this.ddlRegTbMstSubDistrict();
        this.ddlCurTbMstProvince();
        this.ddlCurTbMstDistrict();
        this.ddlCurTbMstSubDistrict();
        this.ddlTbMstChildTitle();
        this.ddlTbMstSectionName();
        this.ddlTbMstDivisionName();
      }else{
        this.check = true;
        this.modeType = 'แก้ไข';
        this.headerType = this.translate.instant('modeType.MT0008');
        this.translate.onLangChange.subscribe(() => {
          this.headerType = this.translate.instant('modeType.MT0008');
        });
        this.loadData();
    }
    this.firstFormGroup = this._formBuilder.group({
      firstCtrl: ['', Validators.required]
    });
    this.secondFormGroup = this._formBuilder.group({
      secondCtrl: ['', Validators.required]
    });
    this.thirdFormGroup = this._formBuilder.group({
      thirdCtrl: ['', Validators.required]
    });
    this.fourthFormGroup = this._formBuilder.group({
      fourthCtrl: ['', Validators.required]
    });
    this.fifthFormGroup = this._formBuilder.group({
      fifthCtrl: ['', Validators.required]
    });
    this.sixFormGroup = this._formBuilder.group({
      sixCtrl: ['', Validators.required]
    });
  }

  formatDateSQLToDate(sqlDate: string) {
    if(sqlDate == ''){
      return sqlDate;
    }else{
    return new Date(sqlDate);
    }
  }

  loadData(){
    this.service.httpGet('/api/v1/teacherProfile/'+this.id,null).then((res:IResponse)=>{
      let data = res.responseData;
      this.FormGroup.controls.teacherId.setValue(data.teacherId||'');
      this.FormGroup.controls.teacherCardId.setValue(data.teacherCardId||'');
      this.teacher_card_id = data.teacherCardId;
      this.FormGroup.controls.citizenId.setValue(data.citizenId||'');
      this.FormGroup.controls.titleId.setValue(data.titleId||'');
      this.FormGroup.controls.titleIdEn.setValue(data.titleIdEn||'');
      this.FormGroup.controls.citizenId.setValue(data.citizenId||'');
      this.FormGroup.controls.scCode.setValue(data.scCode||'');
      this.FormGroup.controls.firstnameTh.setValue(data.firstnameTh||'');
      this.FormGroup.controls.lastnameTh.setValue(data.lastnameTh||'');
      this.FormGroup.controls.firstnameEn.setValue(data.firstnameEn||'');
      this.FormGroup.controls.lastnameEn.setValue(data.lastnameEn||'');
      this.FormGroup.controls.nickname.setValue(data.nickname||'');
      this.FormGroup.controls.fullname.setValue(this.FormGroup.value.titleId + this.FormGroup.value.firstnameTh +"  "+ this.FormGroup.value.lastnameTh);
      this.FormGroup.controls.fullnameEn.setValue(this.FormGroup.value.titleIdEn + this.FormGroup.value.firstnameEn +"  "+ this.FormGroup.value.lastnameEn);
      this.FormGroup.controls.gender.setValue(data.gender||'');
      this.FormGroup.controls.ethnicity.setValue(data.ethnicity||'');
      this.FormGroup.controls.familyStatus.setValue(data.familyStatus||''); 
      this.FormGroup.controls.education.setValue(data.education||'');
      this.FormGroup.controls.saint.setValue(data.saint||'');
      this.FormGroup.controls.religion.setValue(data.religion||'');
      this.FormGroup.controls.citizenship.setValue(data.citizenship||'');
      this.FormGroup.controls.email.setValue(data.email||'');
      this.FormGroup.controls.educationDesc.setValue(data.educationDesc||'');
      this.FormGroup.controls.scoutRank.setValue(data.scoutRank||'');
      this.FormGroup.controls.dob.setValue(this.formatDateSQLToDate(data.dob||''));
      //ที่อยู่ตามทะเบียนบ้าน
      this.FormGroup.controls.regHouseNo.setValue(data.regHouseNo||'');
      this.FormGroup.controls.regAddrNo.setValue(data.regAddrNo||'');
      this.FormGroup.controls.regVillage.setValue(data.regVillage||'');
      this.FormGroup.controls.regVillageNo.setValue(data.regVillageNo||'');
      this.FormGroup.controls.regAlley.setValue(data.regAlley||'');
      this.FormGroup.controls.regRoad.setValue(data.regRoad||'');
      this.FormGroup.controls.regSubDistrict.setValue(data.regSubDistrict||'');
      this.FormGroup.controls.regDistrict.setValue(data.regDistrict||'');
      this.FormGroup.controls.regProvince.setValue(data.regProvince||'');
      this.FormGroup.controls.regCountry.setValue(data.regCountry||'');
      this.FormGroup.controls.regPostCode.setValue(data.regPostCode||'');
      //ที่อยู่ปัจจุบัน
      this.FormGroup.controls.curAddrNo.setValue(data.curAddrNo||'');
      this.FormGroup.controls.curVillage.setValue(data.curVillage||'');
      this.FormGroup.controls.curVillageNo.setValue(data.curVillageNo||'');
      this.FormGroup.controls.curRoad.setValue(data.curRoad||'');
      this.FormGroup.controls.curAlley.setValue(data.curAlley||'');
      this.FormGroup.controls.curSubDistrict.setValue(data.curSubDistrict||'');
      this.FormGroup.controls.curDistrict.setValue(data.curDistrict||'');
      this.FormGroup.controls.curProvince.setValue(data.curProvince||'');
      this.FormGroup.controls.curCountry.setValue(data.curCountry||'');
      this.FormGroup.controls.curPostCode.setValue(data.curPostCode||'');
      //ข้อมูลบิดา
      this.FormGroup.controls.fatherTitle.setValue(data.fatherTitle||'');
      this.FormGroup.controls.fatherFirstnameTh.setValue(data.fatherFirstnameTh||'');
      this.FormGroup.controls.fatherLastnameTh.setValue(data.fatherLastnameTh||'');
      //ข้อมูลมารดา
      this.FormGroup.controls.motherTitle.setValue(data.motherTitle||'');
      this.FormGroup.controls.motherFirstnameTh.setValue(data.motherFirstnameTh||'');
      this.FormGroup.controls.motherLastnameTh.setValue(data.motherLastnameTh||'');
      //ข้อมูลคู่สมรส
      this.FormGroup.controls.spouseTitle.setValue(data.spouseTitle||'');
      this.FormGroup.controls.spouseFirstname.setValue(data.spouseFirstname||'');
      this.FormGroup.controls.spouseLastname.setValue(data.spouseLastname||'');
      this.FormGroup.controls.spouseReligion.setValue(data.spouseReligion||'');
      this.FormGroup.controls.spouseSaint.setValue(data.spouseSaint||'');
      this.FormGroup.controls.spouseChurch.setValue(data.spouseChurch||'');
      this.FormGroup.controls.spouseOffice.setValue(data.spouseOffice||'');
      this.FormGroup.controls.spouseTel1.setValue(data.spouseTel1||'');
      this.FormGroup.controls.spouseTel2.setValue(data.spouseTel2||'');
      //จำนวนบุตร
      this.FormGroup.controls.noOfChild.setValue(data.noOfChild||'');
      //ข้อมูลบุตรคนที่1
      this.FormGroup.controls.childTitle1.setValue(data.childTitle1||'');
      this.FormGroup.controls.childFirstname1.setValue(data.childFirstname1||'');
      this.FormGroup.controls.childLastname1.setValue(data.childLastname1||'');
      this.ddlTbMstGenderChildList()
      this.FormGroup.controls.childGender1.setValue(data.childGender1||'');
      this.FormGroup.controls.childDob1.setValue(this.formatDateSQLToDate(data.childDob1||''));
      this.FormGroup.controls.childClass1.setValue(data.childClass1||'');
      //ข้อมูลบุตรคนที่2
      this.FormGroup.controls.childTitle2.setValue(data.childTitle2||'');
      this.FormGroup.controls.childFirstname2.setValue(data.childFirstname2||'');
      this.FormGroup.controls.childLastname2.setValue(data.childLastname2||'');
      this.ddlTbMstGenderChildList2()
      this.FormGroup.controls.childGender2.setValue(data.childGender2||'');
      this.FormGroup.controls.childDob2.setValue(this.formatDateSQLToDate(data.childDob2||''));
      this.FormGroup.controls.childClass2.setValue(data.childClass2||'');
      //ข้อมูลบุตรคนที่3
      this.FormGroup.controls.childTitle3.setValue(data.childTitle3||'');
      this.FormGroup.controls.childFirstname3.setValue(data.childFirstname3||'');
      this.FormGroup.controls.childLastname3.setValue(data.childLastname3||'');
      this.ddlTbMstGenderChildList3()
      this.FormGroup.controls.childGender3.setValue(data.childGender3||'');
      this.FormGroup.controls.childDob3.setValue(this.formatDateSQLToDate(data.childDob3||''));
      this.FormGroup.controls.childClass3.setValue(data.childClass3||'');
      //ข้อมูลบรรจุ
      this.FormGroup.controls.startWorkDate.setValue(this.formatDateSQLToDate(data.startWorkDate));
      this.calStwDate(this.FormGroup.value.startWorkDate);
      this.FormGroup.controls.registeredDate.setValue(this.formatDateSQLToDate(data.registeredDate));
      this.FormGroup.controls.licenseStart.setValue(this.formatDateSQLToDate(data.licenseStart));
      this.FormGroup.controls.licenseExpired.setValue(this.formatDateSQLToDate(data.licenseExpired));
      this.FormGroup.controls.licenseNo.setValue(data.licenseNo||'');
      this.FormGroup.controls.licenseNo.setValue(data.licenseNo||'');
      this.FormGroup.controls.positionName.setValue(data.positionName||'');
      this.FormGroup.controls.teacherImage.setValue(data.teacherImage||'');
      this.FormGroup.controls.data1.setValue(data.teacherImage||'');
      this.FormGroup.controls.sectionName.setValue(data.sectionName||'');
      this.FormGroup.controls.divisionName.setValue(data.divisionName||'');
      this.FormGroup.controls.subDivisionName.setValue(data.subDivisionName||'');
      this.FormGroup.controls.ispermanent.setValue(data.ispermanent||'');
      this.FormGroup.controls.isinstructor.setValue(data.isinstructor||'');
      this.radio_ispermanent = data.ispermanent;
      this.radio_isInstructor = data.isinstructor;
      this.departmentName = data.departmentName;
      this.classId = data.classId;
      this.classRoomId = data.classRoomId;
      this.FormGroup.controls.departmentName.setValue(data.departmentName||'');
      this.FormDepartment.controls.departmentId.setValue(data.departmentId||'');
      this.FormClass.controls.classId.setValue(data.classId||'');
      this.FormClassRoom.controls.classRoomId.setValue(data.classRoomId||'');  
      this.updatedUser =  data.updatedUser;//username
      this.updatedDate =  data.updatedDate;//time 
      this.Department();
      this.Class();
      this.ClassRoom();
  }).then(()=>this.ddlTbMstTitle()).then(()=>this.ddlTbMstTitle2()).then(()=>this.ddlTbMstReligion()).then(()=>this.ddlCitizenship()).then(()=>this.ddlTbMstClass()).then(()=>this.ddlTbMstPosition()).then(()=>this.ddlTbMstChildTitle()).then(()=>this.ddlTbMstSectionName())
    .then(()=>this.ddlTbMstCountryCode()).then(()=>this.ddlTbMstGenderList()).then(()=>this.ddlTbMstEducationList()).then(()=>this.ddlTbMstFamilyStatusList()).then(()=>this.ddlRegTbMstProvince()).then(()=>this.ddlRegTbMstDistrict()).then(()=>this.ddlTbMstIspermanent())
    .then(()=>this.ddlRegTbMstSubDistrict()).then(()=>this.ddlCurTbMstProvince()).then(()=>this.ddlCurTbMstDistrict()).then(()=>this.ddlCurTbMstSubDistrict()).then(()=>this.ddlTeacherCheckTeacherCardId()).then(()=>this.ddlTbMstDivisionName()).then(()=>this.ddlTbMstSubDivitionName());
  }

  onBackPage(){
    this.router.navigate(['/tec/tec001']);
  }

  /******* START STEP BUTTON *********/
  moveToSelectedTab(tabName: string) {

    for (let i =0; i< document.querySelectorAll('.mat-tab-label-content').length; i++) {
    if ((<HTMLElement>document.querySelectorAll('.mat-tab-label-content')[i]).innerText == tabName) 
      {
        (<HTMLElement>document.querySelectorAll('.mat-tab-label')[i]).click();
      }
    }
  }
  /******* END STEP BUTTON *********/

  onSave(){
    if (this.FormGroup.invalid) { 
        this.validateValue = true;
        Swal.fire(
          this.translate.instant('alert.error'),
          this.translate.instant('alert.validate'),
          'error'
        )
      return;
    }
    Swal.fire({
      title: this.translate.instant('psms.DL0001'),

      icon : 'info',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: this.translate.instant('psms.DL0008'),
      cancelButtonText: this.translate.instant('psms.DL0009')
    }).then((result) => {

      if(result.dismiss == 'cancel'){
        return;
      }
      this.isProcess = true;
      this.FormGroup.controls.fullname.setValue(this.FormGroup.value.titleId + this.FormGroup.value.firstnameTh +"  "+ this.FormGroup.value.lastnameTh);
      this.FormGroup.controls.fullnameEn.setValue(this.FormGroup.value.titleIdEn + this.FormGroup.value.firstnameEn +"  "+ this.FormGroup.value.lastnameEn);
      this.FormGroup.value.scCode = 'PRAMANDA';
     
      let json = this.FormGroup.value;
      if(json.teacherId == ''){
        this.service.httpPost('/api/v1/teacherProfile',json).then((res:IResponse)=>{
          this.isProcess = false;
          if ((res.responseCode || 500) != 200) {
            Swal.fire(
              this.translate.instant('message.add_error'),
              res.responseMsg,
              'error'
            );
            return;
          }
          Swal.fire(
            this.translate.instant('alert.add_header'),
            this.translate.instant('message.add'),
            'success'
          ).then(() => {this.onBackPage();})
        });
        return;
      }
      this.service.httpPut('/api/v1/teacherProfile',json).then((res:IResponse)=>{
        this.isProcess = false;
        if ((res.responseCode || 500) != 200) {
          Swal.fire(
            this.translate.instant('message.edit_error'),
            res.responseMsg,
            'error'
          )
          return;
        }
        Swal.fire(
          this.translate.instant('psms.DL0006'),
          this.translate.instant('message.edit'),
          'success'
        ).then((result) => {this.onBackPage();})
      });
    });
    
  }

  /* CHEK เลขบัตรประชาชน */
  checkCitizenId(){ 
    if(this.FormGroup.value.citizenId.length == 13){
      let index0 = parseFloat(this.FormGroup.value.citizenId[0]);
      let index1 = parseFloat(this.FormGroup.value.citizenId[1]);
      let index2 = parseFloat(this.FormGroup.value.citizenId[2]);
      let index3 = parseFloat(this.FormGroup.value.citizenId[3]);
      let index4= parseFloat(this.FormGroup.value.citizenId[4]);
      let index5 = parseFloat(this.FormGroup.value.citizenId[5]);
      let index6= parseFloat(this.FormGroup.value.citizenId[6]);
      let index7 = parseFloat(this.FormGroup.value.citizenId[7]);
      let index8= parseFloat(this.FormGroup.value.citizenId[8]);
      let index9 = parseFloat(this.FormGroup.value.citizenId[9]);
      let index10 = parseFloat(this.FormGroup.value.citizenId[10]);
      let index11 = parseFloat(this.FormGroup.value.citizenId[11]);
      let index12 = parseFloat(this.FormGroup.value.citizenId[12]);
      let data_index12 = index12.toString();
      let sum = (index0*13)+(index1*12)+(index2*11)+(index3*10)+(index4*9)+(index5*8)+(index6*7)+(index7*6)+(index8*5)+(index9*4)+(index10*3)+(index11*2);
      let modSum = sum%11; 
      let checkDigit = 11-modSum;
      let data_checkDigit = checkDigit.toString();
      if(data_checkDigit.length == 2){
        if(data_index12 == data_checkDigit[1]){
            this.check_citizenId = true;
        }else{
            this.check_citizenId = false;
            this.FormGroup.controls.citizenId.setValue("");
        }
      }else{
        if(data_index12 == data_checkDigit){
            this.check_citizenId = true;
        }else{
            this.check_citizenId = false;
            this.FormGroup.controls.citizenId.setValue("");
        }
      }
    }
 }

  dataAddressReg(e){
    this.checked_address = e.target.checked;
    if(this.checked_address == true){
        this.FormGroup.controls.curVillage.setValue(this.FormGroup.value.regVillage);
        this.FormGroup.controls.curAddrNo.setValue(this.FormGroup.value.regAddrNo);
        this.FormGroup.controls.curVillageNo.setValue(this.FormGroup.value.regVillageNo);
        this.FormGroup.controls.curAlley.setValue(this.FormGroup.value.regAlley);
        this.FormGroup.controls.curRoad.setValue(this.FormGroup.value.regRoad);
        this.FormGroup.controls.curCountry.setValue(this.FormGroup.value.regCountry);
        this.FormGroup.controls.curProvince.setValue(this.FormGroup.value.regProvince);
        this.districtCurList = this.districtRegList;
        this.subDistrictCurList = this.subDistrictRegList;
        this.FormGroup.controls.curDistrict.setValue(this.FormGroup.value.regDistrict);
        this.FormGroup.controls.curSubDistrict.setValue(this.FormGroup.value.regSubDistrict);
        this.FormGroup.controls.curPostCode.setValue(this.FormGroup.value.regPostCode);
    }else{ 
        this.clearDataAddressReg(); 
    }
  }

  clearDataAddressReg(){
      this.FormGroup.controls.curVillage.reset();
      this.FormGroup.controls.curAddrNo.reset();
      this.FormGroup.controls.curVillageNo.reset();
      this.FormGroup.controls.curAlley.reset();
      this.FormGroup.controls.curRoad.reset();
      this.FormGroup.controls.curCountry.reset();
      this.FormGroup.controls.curProvince.reset();
      this.FormGroup.controls.curDistrict.reset();
      this.FormGroup.controls.curSubDistrict.reset();
      this.FormGroup.controls.curPostCode.reset();
  }

  /* DROP DOWN คำนำหน้าชื่อ ไทย */
  ddlTbMstTitle(){
    this.service.httpGet('/api/v1/0/tbMstTitle/ddlParent',null).then((res)=>{
        this.titleList = res||[];
    });
  }

  ddlTbMstFatherTitle(){
    this.service.httpGet('/api/v1/0/tbMstTitle/ddlFather',null).then((res)=>{
      this.titleFatherList = res||[];
    });
  }

  ddlTbMstMotherTitle(){
    this.service.httpGet('/api/v1/0/tbMstTitle/ddlMother',null).then((res)=>{
      this.titleMotherList = res||[];
    });
  }

  // /* DROP DOWN ศาสนา */
  ddlTbMstReligion(){
    this.service.httpGet('/api/v1/0/tbMstReligion/ddl',null).then((res)=>{
        this.religionList = res||[];
    });
  }

  // /* DROP DOWN สัญชาติ */
  ddlTbMstCountryCode(){
    this.service.httpGet('/api/v1/0/tbMstCountry/ddl',null).then((res)=>{
      this.countryCodeList = res||[];
    });
  }

  /* DROP DOWN คำนำหน้าชื่อ ENG */
  ddlTbMstTitle2(){
    if(this.FormGroup.value.titleId == 'นาย'){
      this.service.httpGet('/api/v1/0/tbMstTitle/ddlEn',null).then((res)=>{
        this.Title2List = res||[];
        this.FormGroup.controls.titleIdEn.setValue(res[2]['descTh']);
      });
    }else if(this.FormGroup.value.titleId == 'น.ส.'){
      this.service.httpGet('/api/v1/0/tbMstTitle/ddlEn',null).then((res)=>{
        this.Title2List = res||[];
        this.FormGroup.controls.titleIdEn.setValue(res[1]['descTh']);
      });
    }else if(this.FormGroup.value.titleId == 'นาง'){
      this.service.httpGet('/api/v1/0/tbMstTitle/ddlEn',null).then((res)=>{
        this.Title2List = res||[];
        this.FormGroup.controls.titleIdEn.setValue(res[3]['descTh']);
      });
    }else{
      this.Title2List = [];
    }
  }

  /* DROP DOWN เพศ*/
  ddlTbMstGenderList(){
    let ele = this.titleList.filter((o)=>{ 
      return o['name'] == this.FormGroup.value.titleId;
    });
    let gender =  ele[0]['gender'];
    
    if(gender == 1){
      this.service.httpGet('/api/v1/0/tbMstGender/titleBoy',null).then((res)=>{
          this.GenderList = res||[];
          this.FormGroup.controls.gender.setValue(res[0]['genderDescTh'])
      });
    }else{
      this.service.httpGet('/api/v1/0/tbMstGender/titleGirl',null).then((res)=>{
          this.GenderList = res||['genderDescTh'];
          this.FormGroup.controls.gender.setValue(res[0]['genderDescTh'])
      });
    }
  }

  ddlTbMstGenderChildList(){
    if(this.FormGroup.value.childTitle1 == 'ด.ช.' || this.FormGroup.value.childTitle1 == 'นาย'){
      this.service.httpGet('/api/v1/0/tbMstGender/titleBoy',null).then((res)=>{
          this.GenderChild1List = res||[];
          this.FormGroup.controls.childGender1.setValue(res[0]['genderDescTh'])
      });
    }else{
      this.service.httpGet('/api/v1/0/tbMstGender/titleGirl',null).then((res)=>{
          this.GenderChild1List = res||['genderDescTh'];
          this.FormGroup.controls.childGender1.setValue(res[0]['genderDescTh'])
      });
    }

  }

  ddlTbMstGenderChildList2(){
    if(this.FormGroup.value.childTitle2 == 'ด.ช.' || this.FormGroup.value.childTitle2 == 'นาย'){
      this.service.httpGet('/api/v1/0/tbMstGender/titleBoy',null).then((res)=>{
          this.GenderChild2List = res||[];
          this.FormGroup.controls.childGender2.setValue(res[0]['genderDescTh'])
      });
    }else{
      this.service.httpGet('/api/v1/0/tbMstGender/titleGirl',null).then((res)=>{
          this.GenderChild2List = res||['genderDescTh'];
          this.FormGroup.controls.childGender2.setValue(res[0]['genderDescTh'])
      });
    }

  }

  ddlTbMstGenderChildList3(){
    if(this.FormGroup.value.childTitle3 == 'ด.ช.' || this.FormGroup.value.childTitle3 == 'นาย'){
      this.service.httpGet('/api/v1/0/tbMstGender/titleBoy',null).then((res)=>{
          this.GenderChild3List = res||[];
          this.FormGroup.controls.childGender3.setValue(res[0]['genderDescTh'])
      });
    }else{
      this.service.httpGet('/api/v1/0/tbMstGender/titleGirl',null).then((res)=>{
          this.GenderChild3List = res||['genderDescTh'];
          this.FormGroup.controls.childGender3.setValue(res[0]['genderDescTh'])
      });
    }
  }

  /* DROP DOWN การศึกษา*/
  ddlTbMstEducationList(){
    this.service.httpGet('/api/v1/0/tbMstEducation/ddl',null).then((res)=>{
      this.educationList = res||[];
    });
  }

  /* DROP DOWN สถานะ*/
  ddlTbMstFamilyStatusList(){
    this.service.httpGet('/api/v1/0/tbMstFamilyStatus/ddl',null).then((res)=>{
      this.familyStatusList = res||[];
    });
  }

  /* DROP DOWN จังหวัด อำเภอ ตำบล ตามทะเบียนบ้าน */
  ddlRegTbMstProvince(){
    this.service.httpGet('/api/v1/0/tbMstProvince/ddlReg',null).then((res)=>{
        this.provinceRegList = res||[];
    });
  }

  ddlRegTbMstDistrict(){
    let json = {'provinceThReg':this.FormGroup.value.regProvince};
    this.service.httpGet('/api/v1/0/tbMstDistrict/ddlReg',json).then((res)=>{
        this.districtRegList = res||[];
    });
  }

  ddlRegTbMstSubDistrict(){
    let json = {'districtThReg':this.FormGroup.value.regDistrict}
    this.service.httpGet('/api/v1/0/tbMstSubDistrict/ddlReg',json).then((res)=>{
        this.subDistrictRegList = res||[];
    });
  } 

  changeRegPostCode(){
    let json = {'districtThReg':this.FormGroup.value.regDistrict,'subdistrictReg':this.FormGroup.value.regSubDistrict}
    this.service.httpGet('/api/v1/0/tbMstSubDistrict/ddlRegPostcode',json).then((res)=>{
        this.postCodeList = res||[];
        for(let ele of this.postCodeList){
      this.FormGroup.controls.regPostCode.setValue (ele['postCode']);
    }
    });
  }

  /* DROP DOWN จังหวัด อำเภอ ตำบล ตามที่อยู่ปัจจุบัน */
  ddlCurTbMstProvince(){
    this.service.httpGet('/api/v1/0/tbMstProvince/ddlCur',null).then((res)=>{
        this.provinceCurList = res||[];
    });
  }

  ddlCurTbMstDistrict(){
    let json = {'provinceThCur':this.FormGroup.value.curProvince};
    this.service.httpGet('/api/v1/0/tbMstDistrict/ddlCur',json).then((res)=>{
        this.districtCurList = res||[];
    });
  }

  ddlCurTbMstSubDistrict(){
    let json = {'districtThCur':this.FormGroup.value.curDistrict};
    this.service.httpGet('/api/v1/0/tbMstSubDistrict/ddlCur',json).then((res)=>{
        this.subDistrictCurList = res||[];
    });
  }

  changeCurPostCode(){
    let json = {'districtThCur':this.FormGroup.value.curDistrict,'subdistrictCur':this.FormGroup.value.curSubDistrict};
    this.service.httpGet('/api/v1/0/tbMstSubDistrict/ddlCurPostcode',json).then((res)=>{
        this.postCodeCurList = res||[];
    for(let ele of this.postCodeCurList){
      this.FormGroup.controls.curPostCode.setValue (ele['postCode']);
    }
  });
  }

  /* DROP DOWN สัญชาติ */
  ddlCitizenship(){
    this.service.httpGet('/api/v1/0/tbMstCountry/ddl',null).then((res)=>{
        this.citizenshipList = res||[];
    });
  }
  /* DROP DOWN ชั้นเรียนบุตร */
  ddlTbMstClass(){
    this.service.httpGet('/api/v1/0/tbMstClass/ddl',null).then((res)=>{
      this.classList = res||[];
    });
  }

  /* DROP DOWN position */
  ddlTbMstPosition(){
    this.service.httpGet('/api/v1/0/ddlposit/ddl',null).then((res)=>{ 
      this.positionList = res||[];
    });
  }

  /* DROP DOWN Ispermanent */
  ddlTbMstIspermanent(){
    this.service.httpGet('/api/v1/0/ddlposit/ddl',null).then((res)=>{ 
      this.IspermanentList = res||[];
    });
  }

  /* DROP DOWN sectionName */
  ddlTbMstSectionName(){
    this.service.httpGet('/api/v1/0/ddlsectionname/ddl',null).then((res)=>{ 
      this.sectionNameList = res||[];
    });
  }

  /* DROP DOWN divitionName */
  ddlTbMstDivisionName(){
    this.service.httpGet('/api/v1/0/ddldivisionname/ddl',null).then((res)=>{ 
      this.divisionNameList = res||[];
    });
  }

  /* DROP DOWN subDivitionName */
  ddlTbMstSubDivitionName(){
    let json = {'divisionNameTh':this.FormGroup.value.divisionName};

    this.service.httpGet('/api/v1/0/ddlsubDivitionName/ddl',json).then((res)=>{ 

      this.subDivisionNameList = res||[];
    });
  } 

  /* DROP DOWN CheckteacherCardid */
  ddlTeacherCheckTeacherCardId(){
    if(this.mode == 'add'){
        if(this.FormGroup.value.teacherCardId.length == 8){ 
          let json = {'teacherCardId':this.FormGroup.value.teacherCardId};
          this.service.httpGet('/api/v1/0/teachercheckteachercardid/ddl',json).then((res)=>{
              this.teacherCheckCardIdList = res||[];
              if(this.teacherCheckCardIdList.length != 0){
                // alert("ไม่สามารถเพิ่มได้");
                Swal.fire(
                  this.translate.instant('alert.error'),
                  this.translate.instant('message.code_error'),
                  'error'
                )
                this.FormGroup.controls.teacherCardId.setValue(this.FormGroup.value.teacherCardId ='')
              }else{
                return;
              }
          });
        }
    }
  }

  ddlTbMstChildTitle(){
    this.service.httpGet('/api/v1/0/childTitle/ddl',null).then((res)=>{
        this.TbMstChildTitleList = res||[];
    });
  }

  Department(){
    this.service.httpGet('/api/v1/teacherProfile/department',this.FormDepartment.value).then((res)=>{
        this.departmentNameList = res.responseData||[];
        for(let i of this.departmentNameList){
          this.show = i['lname_th']
        }
    });
  }

  Class(){
    this.service.httpGet('/api/v1/teacherProfile/class',this.FormClass.value).then((res)=>{
        this.ClassNameList = res.responseData||[];
        for(let i of this.ClassNameList){
          this.showclass = i['class_sname_th']
        }
    });
  } 

  ClassRoom(){
    this.service.httpGet('/api/v1/teacherProfile/classromm',this.FormClassRoom.value).then((res)=>{
        this.ClassRoomNameList = res.responseData||[];
        
        for(let i of this.ClassRoomNameList){
          this.showclassRoom = i['class_room_sname_th']
        }
    });
  }

  onFile(event) {
    const selectedFile = event.target.files[0];
    const uploadData = new FormData();
    uploadData.append('file',selectedFile,selectedFile.name);
    this.service.httpPost("/api/v1/attachmentteacher/upload/"+this.teacher_card_id,uploadData).then((res:IResponse)=>{
      if(res.responseCode != 200){
        alert(res.responseMsg);
        return;
      }
      let json = res.responseData;
    this.FormGroup.controls.data1.setValue(selectedFile.name);
    this.FormGroup.controls.teacherImage.setValue(json.attachmentCode+'/'+selectedFile.name);
    });
  }

  onClear(){
    this.FormGroup = this.fb.group({
    'teacherId':['']//primary key
    , 'citizenId':[''] //เลขที่บัตรประชาชน
    , 'titleId':[''] //คำนำหน้าชื่อไทย
    , 'titleIdEn':[''] //คำนำหน้าชื่อ eng
    , 'scCode':[''] //พระมารดา
    , 'firstnameTh':['']//ชื่อ ไทย
    , 'lastnameTh':['']//นามสกุล ไทย
    , 'firstnameEn':['']//ชื่อ ไทย
    , 'lastnameEn':['']//นามสกุล ไทย
    , 'nickname':['']//ชื่อเล่น
    , 'fullname':['']//ชื่อเต็ม
    , 'fullnameEn':['']//ชื่อ ไทย
    , 'teacherCardId':['']
    , 'gender':['']//เพศ
    , 'ethnicity':['']//เชื้อชาติ
    , 'citizenship':['']//สัญชาติ
    , 'religion':['']//ศาสนา
    , 'saint':[''] //ชื่อนักบุญ
    , 'data1':[''] 
    , 'teacherImage':['']
    //ที่อยู่ตามทะเบียนบ้าน
    , 'regSubDistrict':['']//ตำบล
    , 'regDistrict':['']//อำเภอ
    , 'regProvince':['']//จังหวัด
    , 'regCountry':['']//ประเทศ.
    , 'regPostCode':['']//ไปรษณีย์
    , 'regHouseNo':['']//เลขรหัสประจำบ้าน
    , 'regAddrNo':['']//ที่อยู่ตามทะเบียนบ้าน
    , 'regVillage':['']//หมู่บ้าน
    , 'regVillageNo':['']//หมู่.
    , 'regRoad':['']//ถนน
    , 'regAlley':['']//ซอย

    //ที่อยู่ปัจจุบัน
    , 'curAddrNo':['']//ที่อยู่ปัจจุบัน
    , 'curVillage':['']//หมู่บ้าน
    , 'curVillageNo':['']//หมู่
    , 'curAlley':['']//ซอย
    , 'curRoad':['']//ถนน
    , 'curSubDistrict':['']//ตำบล
    , 'curDistrict':['']//อำเภอ
    , 'curProvince':['']//จังหวัด
    , 'curCountry':['']//ประเทศ.
    , 'curPostCode':['']//ไปรษณีย์

    //ข้อมูลบิดา
    , 'fatherTitle':['']//คำนำหน้าชื่อบิดา
    , 'fatherFirstnameTh':['']//ชื่อบิดา
    , 'fatherLastnameTh':['']//นามสกุลบิดา

    //ข้อมูลมารดา
    , 'motherTitle':['']//คำนำหน้าชื่อมารดา
    , 'motherFirstnameTh':['']//ชื่อบิมารดา
    , 'motherLastnameTh':['']//นามสกุลมารดา 

    //ข้อมูลคู่สมรส
    , 'spouseTitle':['']//ที่อยู่ปัจจุบัน
    , 'spouseFirstname':['']//หมู่บ้าน
    , 'spouseLastname':['']//หมู่
    , 'spouseReligion':['']//ถนน
    , 'spouseSaint':['']//ตำบล
    , 'spouseChurch':['']//อำเภอ
    , 'spouseOffice':['']//จังหวัด
    , 'spouseTel1':['']//ประเทศ.
    , 'spouseTel2':['']//ไปรษณีย์

    , 'noOfChild':['']

    //ข้อมูลบุตรคนที่1
    , 'childTitle1':['']//คำนำหน้าชื่อบุตร1
    , 'childFirstname1':['']//ชื่อบุตร1
    , 'childLastname1':['']//นามสกุลบุตร1
    , 'childGender1':['']//เพศบุตร1
    , 'childDob1':['']//วันเกิดบุตร1
    , 'childClass1':['']//ชั้นเรียนบุตร1

    //ข้อมูลบุตรคนที่2
    , 'childTitle2':['']//คำนำหน้าชื่อบุตร2
    , 'childFirstname2':['']//ชื่อบุตร2 
    , 'childLastname2':['']//นามสกุลบุตร2
    , 'childGender2':['']//เพศบุตร2
    , 'childDob2':['']//วันเกิดบุตร2
    , 'childClass2':['']//ชั้นเรียนบุตร2

      //ข้อมูลบุตรคนที่3
    , 'childTitle3':['']//คำนำหน้าชื่อบุตร3
    , 'childFirstname3':['']//ชื่อบุตร3
    , 'childLastname3':['']//นามสกุลบุตร3
    , 'childGender3':['']//เพศบุตร3
    , 'childDob3':['']//วันเกิดบุตร3
    , 'childClass3':['']//ชั้นเรียนบุตร3

    //ข้อมูลบรรจุ
    , 'startWorkDate':['']//วันที่เข้าทำงานวันแรก
    , 'cal_swd':[''] //อายุงาน
    , 'registeredDate':['']//วันที่ได้รับบรรจุ
    , 'licenseNo':['']
    , 'licenseStart':['']
    , 'licenseExpired':['']
    , 'positionName':['']

    , 'familyStatus':['']//สถานะ
    , 'education':['']//การศึกษา
    , 'email':['']//อีเมล
    , 'tel1':['']//เบอร์ที่ 1
    , 'tel2':['']//เบอร์ที่ 2
    });  
  }

  calStwDate(str){
    console.log("str : "+str);
    let convert_str = this.util.transform(str); 
    let mounth_str= parseInt(convert_str.substr(-7,2));
    let year_str = parseInt(convert_str.substr(-4))-543;

    let date_now = new Date();
    let convert_date_now = this.util.transform(date_now);
    let mounth_date_now  = parseInt(convert_date_now.substr(-7,2));
    let year_date_now  = parseInt(convert_date_now.substr(-4))-543;

    if(convert_str.length == 9){
        this.day_str = parseInt(convert_str.substr(-9,1));
    }else{
        this.day_str = parseInt(convert_str.substr(-10,2));
    }
    
    if(convert_date_now.length == 9){
        this.day_date_now = parseInt(convert_date_now.substr(-9,1));
    }else{
        this.day_date_now = parseInt(convert_date_now.substr(-10,2));
    }
    
    let cal_mounth = ((year_date_now*12)+mounth_date_now)-((year_str*12)+mounth_str);
    let cal_day = this.day_date_now-this.day_str;
    let convert_cal_day = cal_day.toString();
    if(convert_cal_day.substr(0,1) == '-'){
        this.FormGroup.controls.cal_swd.setValue('อายุงาน : '+Math.floor((cal_mounth-1)/12)+'  ปี  '+((cal_mounth-1)%12)+'  เดือน');
    }else{
        this.FormGroup.controls.cal_swd.setValue('อายุงาน : '+Math.floor(cal_mounth/12)+'  ปี  '+(cal_mounth%12)+'  เดือน');
    }

  }

 //------ numberOnly ------//
  numberOnly(event): boolean {
    this.FormGroup.value.teacherCardId = (event.which) ? event.which : event.keyCode;
    if (this.FormGroup.value.teacherCardId > 31 && (this.FormGroup.value.teacherCardId < 48 || this.FormGroup.value.teacherCardId > 57)) {
      return false;
    }
    return true;
  }

  numberOnly1(event): boolean {
    this.FormGroup.value.noOfChild = (event.which) ? event.which : event.keyCode;
    if (this.FormGroup.value.noOfChild > 31 && (this.FormGroup.value.noOfChild < 48 || this.FormGroup.value.noOfChild > 57)) {
      return false;
    }
    return true;
  }

  numberOnly2(event): boolean {
    this.FormGroup.value.regPostCode = (event.which) ? event.which : event.keyCode;
    if (this.FormGroup.value.regPostCode > 31 && (this.FormGroup.value.regPostCode < 48 || this.FormGroup.value.regPostCode > 57)) {
      return false;
    }
    return true;
  }

  numberOnly3(event): boolean {
    this.FormGroup.value.curPostCode = (event.which) ? event.which : event.keyCode;
    if (this.FormGroup.value.curPostCode > 31 && (this.FormGroup.value.curPostCode < 48 || this.FormGroup.value.curPostCode > 57)) {
      return false;
    }
    return true;
  }

  numberOnly4(event): boolean {
    this.FormGroup.value.licenseNo = (event.which) ? event.which : event.keyCode;
    if (this.FormGroup.value.licenseNo > 31 && (this.FormGroup.value.licenseNo < 48 || this.FormGroup.value.licenseNo > 57)) {
      return false;
    }
    return true;
  }

}
