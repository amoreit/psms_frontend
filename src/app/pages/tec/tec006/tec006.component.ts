import { Component, OnInit, ViewChild } from '@angular/core';
import { MatTableDataSource, MatPaginator, MatDialog } from '@angular/material';
import { FormBuilder } from '@angular/forms';
import { ApiService, IResponse } from 'src/app/shared/service/api.service';
import { Router } from '@angular/router';
import { UtilService } from 'src/app/_util/util.service';
import { Tec006DialogComponent } from 'src/app/dialog/tec006-dialog/tec006-dialog.component'; 
import { Tec006LeaderDialogComponent } from 'src/app/dialog/tec006-leader-dialog/tec006-leader-dialog.component';
import Swal from 'sweetalert2/dist/sweetalert2.js';
import 'sweetalert2/src/sweetalert2.scss';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-tec006',
  templateUrl: './tec006.component.html',
  styleUrls: ['./tec006.component.scss']
})
export class Tec006Component implements OnInit {

  isProcess:boolean = false;
  searchForm = this.fb.group({
    'catClassId':['']
    , 'yearGroupName':['']
    , 'className':['']
    , 'p':['']
    , 'result':[10]
  }); 

  yearGroupList:[];
  catClassList:[];
  classList: [];
  userName:any;

  yearGroupId:any;
  pageSize:number;
  displayedColumns: string[] = ['yearGroupName','fullname','className','leader','updatedUser','updatedDate','recordStatus'];
  dataSource = new MatTableDataSource();

  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;

  constructor(
      private fb: FormBuilder,
      private service: ApiService,
      public dialog: MatDialog, 
      private translate: TranslateService,
      public util: UtilService
  ) { } 
 
  ngOnInit() { 
    this.userName = localStorage.getItem('userName')||'';
    this.ddlYearGroup();
    this.ddlCatClass();
    this.ddlClass();
  } 

  ddlYearGroup(){ 
    this.service.httpGet('/api/v1/0/year-group/ddl',null).then((res)=>{
      this.yearGroupList = res||[];
      this.searchForm.controls.yearGroupName.setValue(res[0]['name']);
    }).then(()=>this.onSearch(0));
  }

  ddlCatClass(){
    this.service.httpGet('/api/v1/0/tbMstCatClass/ddl',null).then((res)=>{
      this.catClassList= res||[];
    });  
  }

  ddlClass(){
    let json = {'catClassId':this.searchForm.value.catClassId};
    this.service.httpGet('/api/v1/0/tbMstClass/ddlteacherClass',json).then((res)=>{
      this.classList= res||[];
    }); 
  }
 
  onSearch(e){
    this.isProcess = true;
    this.searchForm.value.p = (this.paginator.pageIndex = e.pageIndex||0)+1;
    this.service.httpGet('/api/v1/_teacherClass',this.searchForm.value).then((res:IResponse)=>{
      this.dataSource.data = res.responseData||[];
      this.pageSize = res.responseSize||0;
    });
  }
 
  onSearchteacher():void{ 
    localStorage.setItem("tec006_yearGroupName",this.searchForm.value.yearGroupName);
    const dialogRef = this.dialog.open(Tec006DialogComponent, { 
      width: '55%' 
      , 'autoFocus': true 
      , 'disableClose':true} 
    ); 
    dialogRef.afterClosed().subscribe(result => {
      this.searchForm.controls.yearGroupName.setValue(localStorage.getItem("tec_yearGroupName"));
      this.searchForm.controls.catClassId.setValue(localStorage.getItem("tec_catClassId"));
      this.ddlClass();
      this.searchForm.controls.className.setValue(localStorage.getItem("tec_className"));
      this.onSearch(0);
    }); 
  } 

  onSearchclass():void{ 
    const dialogRef = this.dialog.open(Tec006LeaderDialogComponent, { 
      width: '55%' 
      , 'autoFocus': true 
      , 'disableClose':true}
    );
    dialogRef.afterClosed().subscribe(result => {
      let tecclass_yearGroupName = localStorage.getItem("tecclass_yearGroupName");
      if(tecclass_yearGroupName == undefined || tecclass_yearGroupName == '' || tecclass_yearGroupName == null){
        this.ddlYearGroup();
      }else{
        this.searchForm.controls.yearGroupName.setValue(localStorage.getItem("tecclass_yearGroupName"));
        this.searchForm.controls.catClassId.setValue(localStorage.getItem("tecclass_catClassId"));
        this.ddlClass();
        this.searchForm.controls.className.setValue(localStorage.getItem("tecclass_className"));
        this.onSearch(0);
      }
    });
  }

  onDelete(teacherClassId,fullname){
    Swal.fire({
        title: this.translate.instant('alert.delete'),//ต้องการลบข้อมูล ใช่หรือไม่
        icon : 'info',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33', 
        confirmButtonText: 'ใช่',
        cancelButtonText: 'ไม่'
      }).then((result) => {
      if(result.dismiss == 'cancel'){
          return;
      } 
      let jsonDelete = {'teacherClassId':teacherClassId};
      this.service.httpDelete('/api/v1/_teacherClass',jsonDelete).then((res:IResponse)=>{
        if((res.responseCode||500)!=200){
          Swal.fire(
            this.translate.instant('message.delete_error'),
            res.responseMsg,
            'error'
          )
          return;
        }
        this.onDeleteTeacher(fullname);
      });
    });
  }

  onDeleteTeacher(fullname){
    let json = {'fullname':fullname};
    this.service.httpPut('/api/v1/tbTeacherProfile/class/delete/'+this.userName,json).then((res:IResponse)=>{
      if ((res.responseCode || 500) != 200) {
        Swal.fire(
          this.translate.instant('message.delete_error'),
          res.responseMsg,
          'error'
        )
        return;
      }
      Swal.fire(
        this.translate.instant('alert.delete_header'),
        this.translate.instant('message.delete'),
        'success'
      ).then(()=>this.reload());
    });
  }

  reload(){ 
    this.onSearch(0);
  }

  onCancel(){
    this.searchForm.controls.catClassId.setValue("");
    this.searchForm.controls.className.setValue("");
    this.onSearch(0);
  }

}
