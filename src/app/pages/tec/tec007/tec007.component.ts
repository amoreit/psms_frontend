import { Component, OnInit, ViewChild, Inject } from '@angular/core';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { ApiService, IResponse } from 'src/app/shared/service/api.service';
import { MAT_DIALOG_DATA, MatDialogRef, MatTableDataSource, MatPaginator } from '@angular/material';
import { Router, ActivatedRoute } from '@angular/router';
import { ControlPosition } from '@agm/core/services/google-maps-types';
import Swal from 'sweetalert2/dist/sweetalert2.js';
import 'sweetalert2/src/sweetalert2.scss';
import { UtilService } from 'src/app/_util/util.service';
import { TranslateService } from '@ngx-translate/core';
import { AnyARecord } from 'dns';

export interface SubjectAttendance {
  reason: string;
  remark: string;
}

@Component({
  selector: 'app-tec007',
  templateUrl: './tec007.component.html',
  styleUrls: ['./tec007.component.scss']
})
export class Tec007Component implements OnInit {

  isProcess: boolean = false; 
  searchForm = this.fb.group({
    'yearGroupId': ['']
    , 'classId': ['']
    , 'classRoomId': ['']
    , 'classRoomSubjectId': ['']
    , 'searchDate': ['']
    , 'checkDate': ['']

  });

  reportTec007 = this.fb.group({
    'classId': ['']
    , 'classRoomId': ['']
    , 'classRoomSubjectId': ['']
    , 'searchDate': ['']

  });
  pageSize: number;
  displayedColumns: string[] = ['no', 'stuCode', 'fullname', 'dateTaken', 'status', 'remark'];
  dataSource = new MatTableDataSource();



  classRoomList: [];
  classList: [];
  yearList: [];
  subjectList: [];
  userName: any;
  citizenId:any;
  class_room_sname_th: any;
  AttendanceId: any;
  subjectCode: any;
  stuCode: [];
 
  date = new Date();
  today = Date.now()

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  constructor(
    private translate: TranslateService,
    private service: ApiService,
    private fb: FormBuilder,
    public util: UtilService,
    private ar: ActivatedRoute,
    private router: Router,
  ) { }

  ngOnInit() {
    this.userName = localStorage.getItem('userName') || '';
    this.citizenId = localStorage.getItem('citizenId') || '';
    this.ddlClass();
    this.ddlClassRoom();
    this.ddlYear();
    //this.ddlSubject();
  }

  onCancel() {
    this.searchForm.controls.classRoomSubjectId.reset('');
    this.searchForm.controls.classId.reset('');
    this.searchForm.controls.classRoomId.reset('');
    this.searchForm.controls.checkDate.setValue(false);
    this.searchForm.controls.searchDate.setValue(null);
    this.onSearch(0);
  }

  onPreviewPDF() {
    this.reportTec007.controls.classId.setValue(this.searchForm.value.classId);
    this.reportTec007.controls.classRoomId.setValue(this.searchForm.value.classRoomId);
    this.reportTec007.controls.classRoomSubjectId.setValue(this.searchForm.value.classRoomSubjectId);
    this.reportTec007.controls.searchDate.setValue(this.searchForm.value.searchDate);
    this.service.httpPreviewPDF('/api/v1/tec007/report/pdf', this.reportTec007.value).then((res) => {
      this.isProcess = false;
      window.location.href = res;
    });
  }

  onExportPDF() {
    this.isProcess = true;
    this.service.httpReportPDF('/api/v1/tec007/report/pdf', "ข้อมูลเช็คชื่อนักเรียน", this.searchForm.value).then((res) => {
      this.isProcess = false;
    });
  }

  onSearch(e) {
    if (this.searchForm.value.checkDate == true && this.searchForm.value.searchDate != null) {
      this.searchForm.controls.searchDate.setValue(this.formatDateForSQL(this.searchForm.value.searchDate));
      this.service.httpGet('/api/v1/kornClassRoom-member-sub', this.searchForm.value).then((res: IResponse) => {
        this.dataSource.data = res.responseData || [];
      });
    } else {
      this.searchForm.controls.searchDate.setValue(null);
      this.service.httpGet('/api/v1/kornClassRoom-member-sub', this.searchForm.value).then((res: IResponse) => {
        let dataList = res.responseData || [];
        dataList.map(e => {
          e['classRoomMemberSubjectAttendanceId'] = null;
          e['dateTaken'] = null;
          e['reason'] = null;
          e['remark'] = null;
          return e;
        });
        this.dataSource.data = dataList;
      });
    }
  }

  clearSearchDate() { 
    this.searchForm.controls.searchDate.setValue(null)
  }

  onChooseReason(o) {
    this.dataSource.data.map(e => {
      e['reason'] = o.target.value;
      return e;
    });
  }

  ddlClass() {
    this.service.httpGet('/api/v1/0/class/ddlbycitizenid', {'citizenId': this.citizenId}).then(res => {
      this.classList = res || [];
    });
  }

  ddlClassRoom() {
    const json = { 'citizenId': this.citizenId,'classId': this.searchForm.value.classId };
    this.service
      .httpGet('/api/v1/0/classroom-list/ddlfiltercitizenid', json)
      .then(res => {
        this.classRoomList = res || [];
      });
  }

  ddlYear() {
    this.service.httpGet('/api/v1/0/year-group/ddl', null).then(res => {
      this.yearList = res || [];
      this.searchForm.controls.yearGroupId.setValue(res[0]['yearGroupId']);
    });
  }

  ddlSubject() { 
    let json = { 'citizenId': this.citizenId,'classRoomId': this.searchForm.value.classRoomId,'yearGroupId':this.searchForm.value.yearGroupId};
    this.service.httpGet('/api/v1/0/KsubjectType/Dedd2', json).then((res) => {
      this.subjectList = res || [];
    });
  }

  onSave() {
    this.dataSource.data.map((e: SubjectAttendance) => {
      return {
        reason: e.reason,
        remark: e.remark
      }
    });
    Swal.fire({
      title: this.translate.instant('alert.save'),//ต้องการบันทึกข้อมูล ใช่หรือไม่
      icon: 'info',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: this.translate.instant('psms.DL0008'),//ใช่
      cancelButtonText: this.translate.instant('psms.DL0009')//ไม่
    }).then((result) => {
      if (result.dismiss == 'cancel') {
        return;
      }

      this.service.httpPut('/api/v1/kornClassRoom-member-sub/manageSubjectAttendance/' + this.userName, this.dataSource.data).then((res: IResponse) => {
        if ((res.responseCode || 500) != 200) {
          Swal.fire(
            this.translate.instant('alert.status_error'),//เลือกสถานะผิดพลาด
            res.responseMsg,
            'error'
          )
          return;
        }
        Swal.fire(
          this.translate.instant('psms.LB0129'),//เลือกสถานะ
          this.translate.instant('alert.status_success'),//เลือกสถานะสำเร็จ
          'success'
        ).then(()=>this.onReload())
      });
    });
  }

  onReload(){
    this.searchForm.controls.yearGroupId.setValue(this.searchForm.value.yearGroupId)
    this.searchForm.controls.searchDate.setValue(new Date() ) 
    this.searchForm.controls.checkDate.setValue(true)
    this.searchForm.controls.classId.setValue(this.searchForm.value.classId)
    this.searchForm.controls.classRoomSubjectId.setValue(this.searchForm.value.classRoomSubjectId)
    this.onSearch(0)
  }

  formatDateSQLToDate(sqlDate: string) {
    return new Date(sqlDate);
  }

  formatDateForSQL(date: Date) {
    return date.getFullYear() + "-" + this.twoDigit(date.getMonth() + 1) + "-" + this.twoDigit(date.getDate());
  }

  dateyearplus(dateStr: string, formatBC: boolean, splitter: string = '/', num: number) {
    if (dateStr != null && dateStr != '') {
      dateStr = dateStr.substring(0, 10);
      if (dateStr.length == 10) {
        var arr = dateStr.split('-');
        if (arr.length == 3 && arr[0].length == 4 && arr[1].length == 2 && arr[2].length == 2) {
          var year = parseInt(arr[0]);
          if (!isNaN(parseInt(arr[2])) && !isNaN(parseInt(arr[1])) && !isNaN(year)) {
            if (formatBC) {
              // if (year < 2362) {
              //   year += 543;
              // }
            } else {
              if (year > 2219) {
                year -= 543;
              }
            }
            year += 5;
            return year + splitter + this.twoDigit(arr[1]) + splitter + this.twoDigit(arr[2]);
          }
        }
      }
    }
    return '';
  }

  twoDigit(digit: any) {
    digit = parseInt(digit);
    return digit < 10 ? '0' + digit : new String(digit);
  }

  onExportExcel(){
    this.isProcess = true;
    this.service.httpReportXLS('/api/v1/tec007/report/xls',"ข้อมูลเช็คชื่อนักเรียน",this.searchForm.value).then((res)=>{
      this.isProcess = false;
    });
  }

}
