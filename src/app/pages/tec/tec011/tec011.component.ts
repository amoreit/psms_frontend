import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ApiService, IResponse } from 'src/app/shared/service/api.service';
import { MatDialog, MatTableDataSource, MatPaginator } from '@angular/material';
import { Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';

// sweetalert2
import 'sweetalert2/src/sweetalert2.scss';
import { UtilService } from 'src/app/_util/util.service';
// sweetalert2
@Component({
  selector: 'app-tec011',
  templateUrl: './tec011.component.html',
  styleUrls: ['./tec011.component.scss']
})
export class Tec011Component implements OnInit {
  constructor(private translate: TranslateService,private fb: FormBuilder,private service:ApiService,public dialog: MatDialog, private router: Router, public util: UtilService,) { }
  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  pageSize:number;
  dataSource = new MatTableDataSource();
  displayedColumns: string[] = ['no','teacherCardId','fullname','departmentName','classRoomId','recordStatus'];
  classList:[];
  departmentList:[];

  searchForm = this.fb.group({
     teacherCardId:['']
    , scCode:['']
     , firstnameTh:[''] 
    , lastnameTh:['']
    , departmentId:['']
    , classId:['']
    , p:['']
    , result:[10]
    });

  ngOnInit() {
    this.onSearch(0);
    this.ddlTbMstClass();
    this.ddlTbMstDepartment();
  }

  onSearch(e){
    this.searchForm.value.p = (this.paginator.pageIndex = e.pageIndex||0)+1;
    this.service.httpGet('/api/v1/teacherProfile',this.searchForm.value).then((res:IResponse)=>{
      this.dataSource.data = res.responseData||[];
      this.pageSize = res.responseSize||0;
    });
  }
  ddlTbMstClass(){
    this.service.httpGet('/api/v1/0/tbMstClass/ddl',null).then((res)=>{
      this.classList = res||[];
    });
  }

  ddlTbMstDepartment(){
    this.service.httpGet('/api/v1/0/department/ddl',null).then((res)=>{
      this.departmentList = res||[];
    });
  }
  onForm(id,code) {
    this.router.navigate(["/tec/tec011/card"], { queryParams: { id: id,code:code }});
  }
}