import { Component, OnInit } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { FormBuilder, Validators } from "@angular/forms";
import { ApiService, IResponse } from "src/app/shared/service/api.service";
import Swal from "sweetalert2/dist/sweetalert2.js";
import "sweetalert2/src/sweetalert2.scss";
import * as constants from "../../../../constants";
import { TranslateService } from '@ngx-translate/core';
@Component({
  selector: 'app-tec011-card',
  templateUrl: './tec011-card.component.html',
  styleUrls: ['./tec011-card.component.scss']
})
export class Tec011CardComponent implements OnInit {

  date_start = new Date();
  exit_card: any;
  open_card: any;
  firstname_th:String;
  lastname_th:String;
  teacher_card_id:String;
  dob:String; 
  citizen_id:String;
  id="";//TeacherId
  code="";
  item_no:any;
  tecCode1List:[];
  tec_image:any;
  urls:any;
  formTecProfile = this.fb.group({
    'smartPurseId': ['',Validators.required],
    'teacherId':[''],
    'teacherCardId':[''],
    'startDate': [''],
    'expiredDate': ['']
  });

  formTecProfilePrint = this.fb.group({
    'teacherId':[''],
    'scCode': [''],
    'teacherCardId':[''],
    'itemno':[''],
    'startDate': [''],
    'expiredDate': [''],
    'smartPurseId': [''],
  });
  report = this.fb.group({
    'teacherId':['']
  });
  constructor(
    private translate: TranslateService,
    private fb: FormBuilder,
    public router: Router,
    private service: ApiService,
    private ar: ActivatedRoute
  ) {}

  ngOnInit() {
    this.id = this.ar.snapshot.queryParamMap.get("id") || "";
    this.code = this.ar.snapshot.queryParamMap.get("code") || "";
    this.open_card = this.formatDateForSQL(this.date_start);
    this.formTecProfile.controls.expiredDate.setValue(this.formatDateSQLToDate(this.dateyearplus(this.open_card, true, "/", 5)));
    this. loadData();
  } 

  loadData() {
    this.service.httpGet("/api/v1/teacherProfile/" + this.id, null).then((res: IResponse) => {
        let data = res.responseData;

        this.firstname_th = data.firstnameTh;
        this.lastname_th = data.lastnameTh;
        this.teacher_card_id = data.teacherCardId;
        this.dob = data.dob;
        this.citizen_id = data.citizenId;
        this.formTecProfile.controls.smartPurseId.setValue(data.smartPurseId);
        this.formTecProfile.controls.teacherId.setValue(data.teacherId);
        this.formTecProfile.controls.teacherCardId.setValue(data.teacherCardId);
        this.tec_image = data.teacherImage;
        if (this.tec_image == null || this.tec_image == "") {
          this.urls = [
            constants.SERVICE_API + "/api/v1/attachmentteacher/view/user-no-img.jpg"
          ];
        } else {
          this.urls = [
            constants.SERVICE_API + "/api/v1/attachmentteacher/view/" + this.tec_image
          ];
        }
      });
  }
  onBack() {
    this.router.navigate(["tec/tec011"]);
  }
  onSaveSmartPurseId(){
    if (this.formTecProfile.invalid) {
      Swal.fire(
        this.translate.instant('message.add_error'),//เพิ่มข้อมูลผิดพลาด
        this.translate.instant('alert.validate'),//กรุณาระบุข้อมูล ให้ครบ.
        "error");
      return;
    }
    Swal.fire({
      title: this.translate.instant('alert.save'),//ต้องการบันทึกข้อมูล ใช่หรือไม่
      icon: "info",
      showCancelButton: true,
      confirmButtonColor: "#3085d6",
      cancelButtonColor: "#d33",
      confirmButtonText: this.translate.instant('psms.DL0008'),//ใช่
      cancelButtonText: this.translate.instant('psms.DL0009')//ไม่
      }).then(result => {
      if (result.dismiss == "cancel") {
        return;
      }
    let json = this.formTecProfile.value;
    this.service.httpPut("/api/v1/tbTeacherProfile",json).then((res:IResponse)=>{
      if ((res.responseCode || 500) != 200) {
        Swal.fire(
          this.translate.instant('message.add_error'),//เพิ่มข้อมูลผิดพลาด
          res.responseMsg, 
          "error");
        return;
        }
        Swal.fire(
          this.translate.instant('alert.add_header'),//เพิ่มข้อมูล
          this.translate.instant('message.add'),//เพิ่มข้อมูลสำเร็จ 
          "success")
          .then(() => {
          // this.onBack();
        });
      });
    });
  }
  tecCodeCheck(){
    let teacherCardId = {'teacherCardId':this.teacher_card_id};
    this.service.httpGet('/api/v1/0/stu-printcardlog/TecCode-check',teacherCardId).then((res)=>{
        this.tecCode1List = res||[];
        this.item_no = this.tecCode1List.length;
    }).then(()=> this.onSave_tecPrintLog());
  }
  onSave_tecPrintLog() {
    this.formTecProfilePrint.controls.teacherId.setValue(this.id);
    this.formTecProfilePrint.controls.teacherCardId.setValue(this.teacher_card_id);
    this.formTecProfilePrint.controls.itemno.setValue(this.item_no+1);
    this.formTecProfilePrint.controls.startDate.setValue(this.date_start);
    this.formTecProfilePrint.controls.expiredDate.setValue(this.formTecProfile.value.expiredDate);
    this.formTecProfilePrint.controls.smartPurseId.setValue(this.formTecProfile.value.smartPurseId);
    let printCard = this.formTecProfilePrint.value;
    this.service.httpPost("/api/v1/TecPrintCardLog", printCard).then((res: IResponse) => {
        if ((res.responseCode || 500) != 200) {
          Swal.fire(
            this.translate.instant('message.add_error'),//เพิ่มข้อมูลผิดพลาด
             res.responseMsg, 
             "error");
          return;
        }
        this.onReport();
      });
    return; 
  }
  onReport(){
    this.report.controls.teacherId.setValue(this.id);
    if(this.code == '1'){
      console.log("CODE : "+this.code);
      console.log(this.report.value); 
      this.service.httpPreviewPDF("/api/v1/TecCard/report/pdf", this.report.value).then((res)=>{
        window.location.href = res;
      });
    }else{
      this.service.httpPreviewPDF("/api/v1/TecCard/report/ENpdf", this.report.value).then((res)=>{
        window.location.href = res;
      });
    }
  }

  onReportPDF(){
    this.report.controls.teacherId.setValue(this.id);
    if(this.code == '1'){
      this.service.httpReportPDF("/api/v1/TecCard/report/pdf","คุณครู "+this.firstname_th, this.report.value);
    }else{
      this.service.httpReportPDF("/api/v1/TecCard/report/ENpdf","คุณครู "+this.firstname_th, this.report.value);
    }
  }
  formatDateSQLToDate(sqlDate: string) {
    return new Date(sqlDate);
  }
  formatDateForSQL(date: Date) {
    return (
      date.getFullYear() +
      "-" +
      this.twoDigit(date.getMonth() + 1) +
      "-" +
      this.twoDigit(date.getDate())
    );
  }
  formatDateSQLToString(
    dateStr: string,
    formatBC: boolean,
    splitter: string = "/"
  ) {
    if (dateStr != null && dateStr != "") {
      dateStr = dateStr.substring(0, 10);
      if (dateStr.length == 10) {
        var arr = dateStr.split("-");
        if (
          arr.length == 3 &&
          arr[0].length == 4 &&
          arr[1].length == 2 &&
          arr[2].length == 2
        ) {
          var year = parseInt(arr[0]);
          if (
            !isNaN(parseInt(arr[2])) &&
            !isNaN(parseInt(arr[1])) &&
            !isNaN(year)
          ) {
            if (formatBC) {
              if (year < 2362) {
                year += 543;
              }
            } else {
              if (year > 2219) {
                year -= 543;
              }
            }
            return (
              this.twoDigit(arr[2]) +
              splitter +
              this.twoDigit(arr[1]) +
              splitter +
              year
            );
          }
        }
      }
    }
    return "";
  }
  dateyearplus(
    dateStr: string,
    formatBC: boolean,
    splitter: string = "/",
    num: number
  ) {
    if (dateStr != null && dateStr != "") {
      dateStr = dateStr.substring(0, 10);
      if (dateStr.length == 10) {
        var arr = dateStr.split("-");
        if (
          arr.length == 3 &&
          arr[0].length == 4 &&
          arr[1].length == 2 &&
          arr[2].length == 2
        ) {
          var year = parseInt(arr[0]);
          if (
            !isNaN(parseInt(arr[2])) &&
            !isNaN(parseInt(arr[1])) &&
            !isNaN(year)
          ) {
            if (formatBC) {
              // if (year < 2362) {
              //   year += 543;
              // }
            } else {
              if (year > 2219) {
                year -= 543;
              }
            }
            year += 5;
            return (
              year +
              splitter +
              this.twoDigit(arr[1]) +
              splitter +
              this.twoDigit(arr[2])
            );
          }
        }
      }
    }
    return "";
  }
  twoDigit(digit: any) {
    digit = parseInt(digit);
    return digit < 10 ? "0" + digit : new String(digit);
  }
}