import { RouterModule } from "@angular/router";
import { NgModule } from "@angular/core";
import{ ForgotPasswordComponent} from './forgot-password.component';
import { ReactiveFormsModule, FormsModule } from "@angular/forms";
import { CommonModule } from "@angular/common";
import { SharedModule } from "src/app/shared/shared.module";
 
@NgModule({
    imports: [
        CommonModule,
        RouterModule.forChild([{path: '**',component: ForgotPasswordComponent, pathMatch: 'full' }]),
        FormsModule, 
        ReactiveFormsModule,
        SharedModule
    ],
    providers: [],
    exports:[RouterModule],
    declarations: [ForgotPasswordComponent]
})
export class ForgotPasswordModule {}