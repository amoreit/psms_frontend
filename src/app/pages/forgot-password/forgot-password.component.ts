import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';
import { ApiService, IResponse } from 'src/app/shared/service/api.service';
import Swal from 'sweetalert2/dist/sweetalert2.js';
import 'sweetalert2/src/sweetalert2.scss';

@Component({
  selector: 'app-forgot-password',
  templateUrl: './forgot-password.component.html', 
  styleUrls: ['./forgot-password.component.css']
})
export class ForgotPasswordComponent implements OnInit {

  form = this.fb.group({
    'usrName': ['']
    ,'citizenId': ['']
  });
 
  formUpdate = this.fb.group({
    'userId': ['']
    ,'email': ['']
    ,'firstName': ['']
    ,'lastName': ['']
    ,'password': ['']
  });

  forgotPwcheckList:[];

  constructor(
    public fb: FormBuilder,
    public router:Router,
    private service:ApiService
  ) { }
 
  ngOnInit() {}

  //-------------------- random password
  randomString(length: number): string {
    const charset = "PRAMANDA0123456789";
    const values = new Uint32Array(length);
    window.crypto.getRandomValues(values);

    let result = "";
    for (let i = 0; i < length; i++) {
      result += charset[values[i] % charset.length];
    }
    return result;
  }

  onConfirm(){
    if(this.form.value.citizenId != '' && this.form.value.usrName != '' ){
      let jsonCheck = this.form.value;
      this.service.forgotpasswordcheck(jsonCheck).then((res:IResponse)=>{
        this.forgotPwcheckList = res.responseData||[];

        if(this.forgotPwcheckList.length == 0){
          Swal.fire(
            'ข้อมูลผิดพลาด',
            'กรุณาตรวจสอบเลขบัตร'+'  '+'<b>'+this.form.value.citizenId+'</b>'+'  '+'และ'+'<br>'+'ชื่อผู้ใช้งาน'+'  '+'<b>'+this.form.value.usrName+'</b>'+'<br>'+'เนื่องจากไม่พบข้อมูลในระบบ',
            'error'  
          ).then(() => this.form.controls.citizenId.setValue(''),this.form.controls.usrName.setValue(''))
        }else{
          Swal.fire({
            title: 'ยืนยันการแจ้งลืมรหัสผ่าน ใช่หรือไม่?', 
            icon : 'info',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'ใช่',
            cancelButtonText: 'ไม่'
          }).then((result) => {
            if(result.dismiss == 'cancel'){
                return;
            } 
            let userId =  res.responseData[0]['userId'];
            let firstName =  res.responseData[0]['firstName'];
            let lastName =  res.responseData[0]['lastName'];
            let email =  res.responseData[0]['email'];
            this.formUpdate.controls.userId.setValue(userId);
            this.formUpdate.controls.email.setValue(email);
            this.formUpdate.controls.firstName.setValue(firstName);
            this.formUpdate.controls.lastName.setValue(lastName);
            this.formUpdate.controls.password.setValue(this.randomString(8));

            let jsonUpdate = this.formUpdate.value;
            this.service.forgotpassword(jsonUpdate).then((res:IResponse)=>{
              if((res.responseCode||500)!=200){
                  Swal.fire(
                    'ยืนยันการแจ้งลืมรหัสผ่านผิดพลาด',
                    res.responseMsg,
                    'error' 
                  )
                return;
              }
              Swal.fire(
                'ยืนยันการแจ้งลืมรหัสผ่าน',
                'ยืนยันการแจ้งลืมรหัสผ่านสำเร็จ',
                'success' 
              )
            });
          });
        }
      });
    }else{
      Swal.fire(
        'ยืนยันข้อมูลผิดพลาด',
        'กรุณากรอกข้อมูลให้ครบ',
        'warning' 
      )
    }
  }

  onNumber(event): boolean {
    const chaCode = (event.which) ? event.which : event.keyCode;
    if(chaCode > 31 && (chaCode < 48 || chaCode > 57)){
      return false;
    }
    return true;
  }

  onLogin(){
    this.router.navigate(['/login']);
  }

}
