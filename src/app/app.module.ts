import { BrowserModule } from '@angular/platform-browser';
import { NgModule, LOCALE_ID, Injectable } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { OverlayContainer } from '@angular/cdk/overlay';
import { CustomOverlayContainer } from './theme/utils/custom-overlay-container';
import { HttpClientModule, HttpClient } from '@angular/common/http';

import { AgmCoreModule } from '@agm/core';
import { PerfectScrollbarModule } from 'ngx-perfect-scrollbar';
import { PERFECT_SCROLLBAR_CONFIG } from 'ngx-perfect-scrollbar';
import { PerfectScrollbarConfigInterface } from 'ngx-perfect-scrollbar';
const DEFAULT_PERFECT_SCROLLBAR_CONFIG: PerfectScrollbarConfigInterface = {
  wheelPropagation: true,
  suppressScrollX: true
};

import { CalendarModule, DateAdapter } from 'angular-calendar';
import { adapterFactory } from 'angular-calendar/date-adapters/date-fns';
import { PipesModule } from './theme/pipes/pipes.module';
import { AppRoutingModule } from './app.routing';

import { AppSettings } from './app.settings';
import { AppComponent } from './app.component';

import { TopInfoContentComponent } from './theme/components/top-info-content/top-info-content.component';
import { SidenavComponent } from './theme/components/sidenav/sidenav.component';
import { VerticalMenuComponent } from './theme/components/menu/vertical-menu/vertical-menu.component';
import { HorizontalMenuComponent } from './theme/components/menu/horizontal-menu/horizontal-menu.component';
import { FlagsMenuComponent } from './theme/components/flags-menu/flags-menu.component';
import { FullScreenComponent } from './theme/components/fullscreen/fullscreen.component';
import { UserMenuComponent } from './theme/components/user-menu/user-menu.component';
import { LayoutComponent } from './layout/layout.component';

import { AlertModule } from 'ngx-bootstrap';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { RouterModule } from '@angular/router';
import { SharedModule } from './shared/shared.module';

import { AuthGuard } from './shared/service/auth.guard';

/*** translate ***/
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';

/*** DDL ***/
import { PageGroupDdlComponent } from './dialog/page-group-ddl/page-group-ddl.component';
import { HashLocationStrategy, LocationStrategy, NgLocalization, NgLocaleLocalization } from '@angular/common';
/*** DIALOG ***/
import { Stu004DialogComponent } from './dialog/stu004-dialog/stu004-dialog.component'; 
import { Aca002DialogComponent } from './dialog/aca002-dialog/aca002-dialog.component';
import { Aca002Sub03AddDialogComponent } from './dialog/aca002-sub03-add-dialog/aca002-sub03-add-dialog.component';
import { Aca002UpdateDialogComponent } from './dialog/aca002-update-dialog/aca002-update-dialog.component';
import { Aca002Catclass1DialogComponent } from './dialog/aca002-catclass1-dialog/aca002-catclass1-dialog.component';
import { Aca002Catclass1UpdateDialogComponent } from './dialog/aca002-catclass1-update-dialog/aca002-catclass1-update-dialog.component';
import { Tec002DialogComponent } from './dialog/tec002-dialog/tec002-dialog.component';
import { Tec005DialogComponent } from './dialog/tec005-dialog/tec005-dialog.component';
import { Tec006DialogComponent } from './dialog/tec006-dialog/tec006-dialog.component';

import { Tec005LeaderDialogComponent } from './dialog/tec005-leader-dialog/tec005-leader-dialog.component';
import { Tec006LeaderDialogComponent } from './dialog/tec006-leader-dialog/tec006-leader-dialog.component';
import { Aca008DialogComponent } from './dialog/aca008-dialog/aca008-dialog.component';
import { Aca008TectherDialogComponent } from './dialog/aca008-tecther-dialog/aca008-tecther-dialog.component';
import { Gra005DialogComponent } from './dialog/gra005-dialog/gra005-dialog.component';
import { Aca002AddStuDialogComponent } from './dialog/aca002-add-stu-dialog/aca002-add-stu-dialog.component';
import { Aca002ChangeClassroomDialogComponent } from './dialog/aca002-change-classroom-dialog/aca002-change-classroom-dialog.component';
import { Stu005IsleftDialogComponent } from './dialog/stu005-isleft-dialog/stu005-isleft-dialog.component';

export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http, './assets/i18n/', '.json');
}
  
@NgModule({
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    FormsModule,
    ReactiveFormsModule, 
    SharedModule,
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyAAYi6itRZ0rPgI76O3I83BhhzZHIgMwPg'
    }),
    PerfectScrollbarModule,
    CalendarModule.forRoot({
      provide: DateAdapter,
      useFactory: adapterFactory
    }),
    PipesModule,
    RouterModule.forRoot(AppRoutingModule),
    AlertModule.forRoot(),
    BsDatepickerModule.forRoot(),
    HttpClientModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient]
      }
    })
  ],
  declarations: [
    AppComponent,
    TopInfoContentComponent,
    SidenavComponent,
    VerticalMenuComponent,
    HorizontalMenuComponent,
    FlagsMenuComponent,
    FullScreenComponent,
    UserMenuComponent,
    LayoutComponent,
    PageGroupDdlComponent,
    Stu004DialogComponent,
    Aca002DialogComponent,
    Aca002UpdateDialogComponent,
    Aca002Catclass1DialogComponent,
    Aca002Catclass1UpdateDialogComponent,
    Tec002DialogComponent,
    Tec005DialogComponent,
    Tec006DialogComponent,
    Tec005LeaderDialogComponent,
    Tec006LeaderDialogComponent,
    Aca002Sub03AddDialogComponent,
    Aca002AddStuDialogComponent,
    Aca002ChangeClassroomDialogComponent,
    Aca008DialogComponent,
    Aca008TectherDialogComponent,
    Gra005DialogComponent,
    Stu005IsleftDialogComponent,

  ],
  entryComponents: [
    VerticalMenuComponent,

    /*** DDL ***/
    PageGroupDdlComponent,

    /*** DIALOG ***/
    Stu004DialogComponent,
    Aca002DialogComponent,
    Aca002Catclass1DialogComponent,
    Aca002Catclass1UpdateDialogComponent,
    Aca002AddStuDialogComponent,
    Aca002ChangeClassroomDialogComponent,
    Tec002DialogComponent,
    Tec005DialogComponent,
    Tec006DialogComponent,
    Tec005LeaderDialogComponent,
    Tec006LeaderDialogComponent,
    Aca002UpdateDialogComponent,
    Aca002Sub03AddDialogComponent,
    Aca008DialogComponent,
    Aca008TectherDialogComponent,
    Gra005DialogComponent,
    Stu005IsleftDialogComponent,
  ],
  providers: [
    AuthGuard,
    AppSettings,
    { provide: LOCALE_ID, useValue: "th-TH" },
    { provide: PERFECT_SCROLLBAR_CONFIG, useValue: DEFAULT_PERFECT_SCROLLBAR_CONFIG },
    { provide: OverlayContainer, useClass: CustomOverlayContainer },
    { provide: LocationStrategy, useClass: HashLocationStrategy },
    { provide: NgLocalization, useClass: NgLocaleLocalization }
  ],
  bootstrap: [
    AppComponent
  ]
})
export class AppModule { }
