import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ApiService, IResponse } from 'src/app/shared/service/api.service';
import { MatDialogRef, MatTableDataSource, MatPaginator} from '@angular/material';
import Swal from 'sweetalert2/dist/sweetalert2.js';
import 'sweetalert2/src/sweetalert2.scss';
import { UtilService } from 'src/app/_util/util.service';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-tec006-dialog',
  templateUrl: './tec006-dialog.component.html',
  styleUrls: ['./tec006-dialog.component.scss']
})
export class Tec006DialogComponent implements OnInit {

  isProcess:boolean = false;
  searchForm = this.fb.group({
    'teacherId':['']
    , 'scCode':['']
    , 'firstnameTh':['']
    , 'lastnameTh':['']
    , 'classId':['']
    , 'classRoomId':[''] 
    , 'p':['']
    , 'result':[10]
  });
 
  headerSelected:Boolean;
  isSelected:any;
  checklist:any;
  _allChecklist:any;

  cat_class_id:any; 
  class_name:any;
  class_id:any;
  userName:String;
  yearGroupId:any;
  yearGroupName:any;

  yearGroupList:[];
  catClassList:[];
  classList:[];
  teacherClassList:[];
  selectTeacher = false;

  pageSize:number;
  displayedColumns: string[] = ['no','titleId','firstnameTh','lastnameTh'];
  dataSource = new MatTableDataSource();

  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  constructor(
    private service:ApiService,
    public dialogRef: MatDialogRef<Tec006DialogComponent>, 
    private fb: FormBuilder, 
    private translate: TranslateService,
    public util: UtilService
  ) { }
  
  ngOnInit() {  
    this.userName = localStorage.getItem('userName')||'';
    this.yearGroupName = localStorage.getItem("tec006_yearGroupName");
    let jsonYearGroupName = {'yearGroupName':this.yearGroupName};
    this.service.httpGet('/api/v1/0/year-group/searchId',jsonYearGroupName).then((res)=>{
      this.yearGroupList = res||[];
      this.yearGroupId = res[0]['yearGroupId'];
    });
    this.ddlCatClass();
    this.onSearch(0);
  }
 
  onSearch(e){
    this.isProcess = true;
    this.headerSelected = false;
    this.searchForm.value.p = (this.paginator.pageIndex = e.pageIndex||0)+1;
    this.service.httpGet('/api/v1/tbTeacherProfile',this.searchForm.value).then((res:IResponse)=>{
      this.dataSource.data = res.responseData||[];
      this.checklist = this.dataSource.data;
      this.pageSize = res.responseSize||0; 
    });
  }

  ddlCatClass(){
    this.service.httpGet('/api/v1/0/tbMstCatClass/ddl',null).then((res)=>{
      this.catClassList= res||[];
    }); 
  }

  ddlClass(){
    let json = {'catClassId':this.cat_class_id};
    this.service.httpGet('/api/v1/0/tbMstClass/ddlteacherClass',json).then((res)=>{
      this.classList= res||[];
    }); 
  }

  checkAll() {
    for (var i = 0; i < this.checklist.length; i++) {
      this.checklist[i].isSelected = this.headerSelected;
    }
    this.getCheckedItemList();
  }
 
  isAllSelected() {
    this.checklist.every(function(item:any) {
        return item.isSelected == true;
    })
    this.headerSelected = false;
    this.selectTeacher = true;
    this.getCheckedItemList();
  }

  getCheckedItemList(){
    let ele = this.classList.filter((o)=>{ 
      return o['classLnameTh'] == this.class_name;
    });
    this.class_id =  ele[0]['classId']; 

    this._allChecklist = [];
    for (var i = 0; i < this.checklist.length; i++) {
      if(this.checklist[i].isSelected){
        let itemData = {
          'teacherId':this.checklist[i].teacherId, 
          'fullname':this.checklist[i].fullname, 
          'fullnameEn':this.checklist[i].fullnameEn,
          'classId':this.class_id
        };
        this._allChecklist.push(itemData); 
      }
    }
    if(this._allChecklist.length == 0){
      this.selectTeacher = false;
    }else{ 
      this.selectTeacher = true;
    }
  }

  onSave(){
    Swal.fire({
        title: this.translate.instant('psms.DL0001'), 
        icon : 'info',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: this.translate.instant('psms.DL0008'),
        cancelButtonText: this.translate.instant('psms.DL0009')
      }).then((result) => {
      if(result.dismiss == 'cancel'){
          return;
      } 
      let json = this._allChecklist;
      this.service.httpPut('/api/v1/tbTeacherProfile/class/'+this.userName,json).then((res:IResponse)=>{
        if((res.responseCode||500)!=200){
            Swal.fire(
              this.translate.instant('message.save_tec_error'),//บันทึกข้อมูลครูผิดพลาด
              res.responseMsg,
              'error' 
            )
          return;
        }
        this.onTeacherClass();
      });
    });
  } 

  onTeacherClass(){  
    for (var i = 0; i < this.checklist.length; i++) {
      if(this.checklist[i].isSelected){
        let jsonInsert = {
            'teacherId':this.checklist[i].teacherId, 
            'fullname':this.checklist[i].fullname, 
            'fullnameEn':this.checklist[i].fullnameEn, 
            'yearGroupId':this.yearGroupId, 
            'yearGroupName':this.yearGroupName, 
            'catClassId':this.cat_class_id,
            'classId':this.class_id,
            'className':this.class_name,
            'leader':false
        };
        this.service.httpGet('/api/v1/0/teacher-class/ddl',jsonInsert).then((res)=>{
            this.teacherClassList = res||[];
            if(this.teacherClassList.length == 0){
              // insert
              this.service.httpPost('/api/v1/_teacherClass',jsonInsert).then((res:IResponse)=>{
                  if((res.responseCode||500)!=200){
                    Swal.fire(
                      this.translate.instant('message.save_error'),
                      res.responseMsg,
                      'error' 
                    )
                    return;
                  }
              });
            }else{
              // update
              let teacherClassId = res[0]['teacherClassId'];
              let teacherId = res[0]['teacherId'];
              let fullname = res[0]['fullname'];
              let jsonUpdate = {
                  'teacherClassId':teacherClassId, 
                  'teacherId':teacherId, 
                  'fullname':fullname, 
                  'yearGroupId':this.yearGroupId, 
                  'yearGroupName':this.yearGroupName, 
                  'catClassId':this.cat_class_id,
                  'classId':this.class_id,
                  'className':this.class_name,
                  'leader':false
              };
              this.service.httpPut('/api/v1/_teacherClass',jsonUpdate).then((res:IResponse)=>{
                  if((res.responseCode||500)!=200){
                    Swal.fire(
                      this.translate.instant('message.edit_error'),
                      res.responseMsg,
                      'error'
                    )
                    return;
                  }
              });
            }
        }); 
      }
    }Swal.fire( 
		  this.translate.instant('message.save_header'), 
		  this.translate.instant('message.save'),
		  'success'
		).then(() => this.close());
  }

  onCancel(){
    this.searchForm = this.fb.group({
      'firstnameTh':['']
      , 'lastnameTh':['']
    });
    this.onSearch(0);
  }
  
  close(){
    if(this.cat_class_id == undefined || this.class_name == undefined){
      localStorage.setItem("tec_yearGroupName",this.yearGroupName);
      localStorage.setItem("tec_catClassId","");
      localStorage.setItem("tec_className","");
      this.dialogRef.close(); 
    }else{
      localStorage.setItem("tec_yearGroupName",this.yearGroupName);
      localStorage.setItem("tec_catClassId",this.cat_class_id);
      localStorage.setItem("tec_className",this.class_name);
      this.dialogRef.close(); 
    }
  } 

}
