import { Component, OnInit, Inject } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ApiService, IResponse } from 'src/app/shared/service/api.service';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { Router } from '@angular/router';
import Swal from 'sweetalert2/dist/sweetalert2.js';
import 'sweetalert2/src/sweetalert2.scss';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-stu004-dialog',
  templateUrl: './stu004-dialog.component.html',
  styleUrls: ['./stu004-dialog.component.scss']
})
export class Stu004DialogComponent implements OnInit {
  form = this.fb.group({
    'stuProfileId': ['']
    , 'stuCode': ['']
  });

  constructor( 
    private service: ApiService,
    @Inject(MAT_DIALOG_DATA) data: any,
    public dialogRef: MatDialogRef<Stu004DialogComponent>,
    private fb: FormBuilder,
    private translate: TranslateService,
    private router: Router
  ) {
    if ((data.stuProfileId || '') != '') {
      this.form.controls.stuProfileId.setValue(data.stuProfileId)
      this.form.controls.stuCode.setValue((data.stuCode2.max + 1 || ''));
    }
  }

  ngOnInit() {
  }

  onBack() {
    this.router.navigate(['../../pages/stu/stu004']);
  }

  close() {
    this.dialogRef.close();
  }

  onSave() {

    Swal.fire({
      title: this.translate.instant('psms.DL0001'),
      icon: 'info',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: this.translate.instant('psms.DL0008'),
      cancelButtonText: this.translate.instant('psms.DL0009')
    }).then((result) => {
      if (result.dismiss == 'cancel') {
        return;
      }
      let json = this.form.value;
      let user = localStorage.getItem('userName');
      this.service.httpPut('/api/v1/tbStuProfile/updateCode/' + user, json).then((res: IResponse) => {
        if ((res.responseCode || 500) != 200) {
          alert(res.responseMsg);
          Swal.fire(
            this.translate.instant('message.edit_error'),
            res.responseMsg,
            'error'
          )
          return;
        }
        Swal.fire(
          this.translate.instant('psms.DL0006'),
          this.translate.instant('message.edit'),
          'success'
        ).then(() => { this.close(); })
      });
    });
  }
}
