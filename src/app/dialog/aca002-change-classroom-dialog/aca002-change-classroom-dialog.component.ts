import { Component, OnInit, Inject } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ApiService, IResponse } from 'src/app/shared/service/api.service';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import Swal from 'sweetalert2/dist/sweetalert2.js';
import 'sweetalert2/src/sweetalert2.scss';
import { TranslateService } from '@ngx-translate/core';


@Component({
  selector: 'app-aca002-change-classroom-dialog',
  templateUrl: './aca002-change-classroom-dialog.component.html',
  styleUrls: ['./aca002-change-classroom-dialog.component.scss']
})
export class Aca002ChangeClassroomDialogComponent implements OnInit {
  form = this.fb.group({
    'classRoomMemberId': ['']
    , 'classRoomId': ['']
    , 'classRoom': ['']
    , 'isEp': ['']
  });

  classRoomList: [];
  class_room_id: any;
  role: any;
  member_id: any;

  constructor(
    private translate: TranslateService,
    private service: ApiService,
    @Inject(MAT_DIALOG_DATA) data: any,
    public dialogRef: MatDialogRef<Aca002ChangeClassroomDialogComponent>,
    private fb: FormBuilder
  ) {
    this.class_room_id = data.classRoomId;
    this.form.controls.classRoomMemberId.setValue(data.classRoomMemberId || '')
    this.role = data.role;
    this.member_id = data.memberId;
  }

  ngOnInit() {
    this.ddlClassRoom();
  }

  close() {
    this.dialogRef.close();
  }

  ddlClassRoom() {
    let json = {
      'classRoomId': this.class_room_id,
    };
    this.service.httpGet('/api/v1/0/classroom/ddlfindroombyclassroomid', json).then((res) => {
      this.classRoomList = res || [];
    });
  }

  onCheckClassRoom() {
    let ele = this.classRoomList.filter((o) => {
      return o['class_room_id'] == this.form.value.classRoomId;
    });
    this.form.controls.classRoom.setValue(ele[0]['class_room_sname_th']);
    this.form.controls.isEp.setValue(ele[0]['is_ep'])
  }

  changeTeacherProfile() {
    this.service.httpPut('/api/v1/trn-classroom-member/updateclassroomteacher', { 'classRoomId': this.form.value.classRoomId, 'teacherId': this.member_id }).then((res: IResponse) => {
      if ((res.responseCode || 500) != 200) {
        alert(res.responseMsg);
        return;
      }
      Swal.fire(
        this.translate.instant('alert.save_move_classroom_header'),//บันทึกข้อมูลย้ายห้องเรียน
        this.translate.instant('alert.save_move_classroom_success'),//บันทึกข้อมูลย้ายห้องเรียนสำเร็จ
        'success'
      ).then(() => {
        this.close();
      })
    });
  }

  changeStuProfile() {
    this.service.httpPut('/api/v1/trn-classroom-member/updateclassroomstu', { 'classRoomId': this.form.value.classRoomId, 'classRoom': this.form.value.classRoom, 'stuProfileId': this.member_id }).then((res: IResponse) => {
      if ((res.responseCode || 500) != 200) {
        alert(res.responseMsg);
        return;
      }
      Swal.fire(
        this.translate.instant('alert.save_move_classroom_header'),//บันทึกข้อมูลย้ายห้องเรียน
        this.translate.instant('alert.save_move_classroom_success'),//บันทึกข้อมูลย้ายห้องเรียนสำเร็จ
        'success'
      ).then(() => {
        this.close();
      })
    });
  }

  onSave2() {
    Swal.fire({
      title: this.translate.instant('alert.save'), //ต้องการบันทึกข้อมูล ใช่หรือไม่
      icon: 'info',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: this.translate.instant('psms.DL0008'),//ใช่
      cancelButtonText: this.translate.instant('psms.DL0009')//ไม่
    }).then((result) => {
      if (result.dismiss == 'cancel') {
        return;
      }
      let json = this.form.value
      if (this.form.value.classRoomMemberId == '' || this.form.value.classRoomId == '' || this.form.value.classRoom == '') {
        Swal.fire(
          this.translate.instant('alert.save_move_classroom_error'),//ย้ายห้องเรียนผิดพลาด
          this.translate.instant('alert.validate'),//กรุณาระบุข้อมูล ให้ครบ.
          'error'
        )
      } else {
        if (this.role == 'student') {
          this.service.httpPut('/api/v1/trn-classroom-member/updateclassroom', json).then((res: IResponse) => {
            if ((res.responseCode || 500) != 200) {
              Swal.fire(
                this.translate.instant('alert.save_move_classroom_error'),//ย้ายห้องเรียนผิดพลาด
                res.responseMsg,
                'error'
              )
              return;
            }
            this.changeStuProfile();
          })
        } else {
          this.service.httpPut('/api/v1/trn-classroom-member/updateclassroom', json).then((res: IResponse) => {
            if ((res.responseCode || 500) != 200) {
              Swal.fire(
                this.translate.instant('alert.save_move_classroom_error'),//ย้ายห้องเรียนผิดพลาด
                res.responseMsg,
                'error'
              )
              return;
            }
            this.changeTeacherProfile();
          })
        }
      }
      return;
    });
  } 

}
