import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ApiService, IResponse } from 'src/app/shared/service/api.service';
import { MatDialogRef, MatTableDataSource, MatPaginator} from '@angular/material';
import Swal from 'sweetalert2/dist/sweetalert2.js';
import 'sweetalert2/src/sweetalert2.scss';
import { UtilService } from 'src/app/_util/util.service';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-tec005-dialog',
  templateUrl: './tec005-dialog.component.html', 
  styleUrls: ['./tec005-dialog.component.scss']
})
export class Tec005DialogComponent implements OnInit {
 
  isProcess:boolean = false;
  searchForm = this.fb.group({ 
    'teacherId':['']
    , 'scCode':[''] 
    , 'firstnameTh':['']
    , 'lastnameTh':['']
    , 'classId':['']
    , 'classRoomId':['']
    , 'p':['']
    , 'result':[10]
  });

  headerSelected:Boolean;
  isSelected:any;
  checklist:any;
  _allChecklist:any;
  _allChecklist2:any;
 
  userName:String;
  yearGroupId:any;
  yearGroupName:any;
  departmentId:any;
  departmentName:any;
  catClassId:any;

  teacherDepartmentList:[];
  selectTeacher = false;

  pageSize:number;
  displayedColumns: string[] = ['no','titleId','firstnameTh','lastnameTh'];
  dataSource = new MatTableDataSource();

  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  constructor(
    private service:ApiService,
    public dialogRef: MatDialogRef<Tec005DialogComponent>, 
    private fb: FormBuilder, 
    public util: UtilService,
    private translate: TranslateService
  ) { }
  
  ngOnInit() {  
    this.userName = localStorage.getItem('userName')||'';
    this.yearGroupId = localStorage.getItem("yearGroupId");
    this.yearGroupName = localStorage.getItem("yearGroupName");
    this.departmentId = localStorage.getItem("departmentId");
    this.departmentName = localStorage.getItem("name");
    this.catClassId = localStorage.getItem("catClassId");
    this.onSearch(0);
  }
 
  onSearch(e){
    this.isProcess = true;
    this.headerSelected = false;
    this.searchForm.value.p = (this.paginator.pageIndex = e.pageIndex||0)+1;
    this.service.httpGet('/api/v1/tbTeacherProfile',this.searchForm.value).then((res:IResponse)=>{
      this.dataSource.data = res.responseData||[];
      this.checklist = this.dataSource.data;
      this.pageSize = res.responseSize||0; 
    });
  }

  checkAll() {
    for (var i = 0; i < this.checklist.length; i++) {
      this.checklist[i].isSelected = this.headerSelected;
    }
    this.getCheckedItemList();
  } 
 
  isAllSelected() {
    this.checklist.every(function(item:any) {
        return item.isSelected === true;
    })
    this.headerSelected = false;
    this.getCheckedItemList();
  }

  getCheckedItemList(){
    this._allChecklist = [];
    for (var i = 0; i < this.checklist.length; i++) {
      if(this.checklist[i].isSelected){ 
        let itemData = {
            'teacherId':this.checklist[i].teacherId, 
            'fullname':this.checklist[i].fullname, 
            'fullnameEn':this.checklist[i].fullnameEn, 
            'yearGroupId':this.yearGroupId, 
            'yearGroupName':this.yearGroupName, 
            'departmentId':this.departmentId, 
            'departmentName':this.departmentName
        };
        this._allChecklist.push(itemData); 
      }
    } 
    if(this._allChecklist.length == 0){
      this.selectTeacher = false;
    }else{
      this.selectTeacher = true;
    }
  }

  onSave(){
    Swal.fire({
        title: this.translate.instant('psms.DL0001'), 
        icon : 'info',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: this.translate.instant('psms.DL0008'),
        cancelButtonText: this.translate.instant('psms.DL0009')
      }).then((result) => {
      if(result.dismiss == 'cancel'){
          return;
      } 
      let json = this._allChecklist;
      this.service.httpPut('/api/v1/tbTeacherProfile/department/'+this.userName,json).then((res:IResponse)=>{
        if((res.responseCode||500)!=200){
            Swal.fire(
              this.translate.instant('message.save_tec_error'),//บันทึกข้อมูลครูผิดพลาด
              res.responseMsg,
              'error' 
            )
          return;
        }
        this.onTeacherDepartment();
      });
    });
  } 

  onTeacherDepartment(){  
    this._allChecklist2 = [];
    for (var i = 0; i < this.checklist.length; i++) {
      if(this.checklist[i].isSelected){
        let jsonInsert = {
            'teacherId':this.checklist[i].teacherId, 
            'fullname':this.checklist[i].fullname, 
            'fullnameEn':this.checklist[i].fullnameEn, 
            'yearGroupId':this.yearGroupId, 
            'yearGroupName':this.yearGroupName, 
            'departmentId':this.departmentId, 
            'departmentName':this.departmentName, 
            'catClassId':this.catClassId,
            'leader':false
        };
        this.service.httpGet('/api/v1/0/teacher-department/ddl',jsonInsert).then((res)=>{
            this.teacherDepartmentList = res||[];
            if(this.teacherDepartmentList.length == 0){
              //insert
              this.service.httpPost('/api/v1/_teacherDepartment',jsonInsert).then((res:IResponse)=>{
                  if((res.responseCode||500)!=200){
                      Swal.fire(
                        this.translate.instant('message.save_dep_error'),//บันทึกข้อมูลกลุ่มสาระผิดพลาด
                        res.responseMsg,
                        'error' 
                      )
                    return;
                  }
              });
            }else{
              //update
              let _teacherDepartmentId = res[0]['teacherDepartmentId'];
              let _teacherId = res[0]['teacherId'];
              let _fullname = res[0]['fullname'];
              let jsonUpdate = {
                    'teacherDepartmentId':_teacherDepartmentId, 
                    'teacherId':_teacherId, 
                    'fullname':_fullname, 
                    'yearGroupId':this.yearGroupId, 
                    'yearGroupName':this.yearGroupName, 
                    'departmentId':this.departmentId, 
                    'departmentName':this.departmentName, 
                    'catClassId':this.catClassId,
                    'leader':false
              };
              this.service.httpPut('/api/v1/_teacherDepartment',jsonUpdate).then((res:IResponse)=>{
                  if((res.responseCode||500)!=200){
                    Swal.fire(
                      this.translate.instant('message.edit_error'),
                      res.responseMsg,
                      'error'
                    )
                    return;
                  }
              });
            }
        }); 
      }
    }Swal.fire( 
      this.translate.instant('message.save_header'), 
		  this.translate.instant('message.save'),
		  'success'
    ).then(() => this.close());
  }

  onCancel(){
    this.searchForm = this.fb.group({
      'firstnameTh':['']
      , 'lastnameTh':['']
    });
    this.onSearch(0);
  }
  
  close() {
    this.dialogRef.close(); 
  } 

}
