import { Component, OnInit, Inject, ViewChild } from '@angular/core';
import { ApiService, IResponse } from 'src/app/shared/service/api.service';
import { MAT_DIALOG_DATA, MatDialogRef, MatTableDataSource, MatPaginator } from '@angular/material';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-page-group-ddl',
  templateUrl: './page-group-ddl.component.html',
  styleUrls: ['./page-group-ddl.component.scss'] 
})
export class PageGroupDdlComponent implements OnInit {

  displayedColumns: string[] = ['roleGroupCode', 'roleGroupName', 'roleGroupButton'];
  dataSource = new MatTableDataSource();
  pageSize:number; 
  isProcess:boolean = false;
  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  pageGroupCode:any;  
  inputText:any;

  constructor(private service:ApiService,@Inject(MAT_DIALOG_DATA) data:any,
    public dialogRef: MatDialogRef<PageGroupDdlComponent>,
    private translate: TranslateService,
    ) { 
    this.pageGroupCode = data.pageGroupCode||'';
  }

  onSearch(e){
    this.isProcess = true;
    let json = {'p':(this.paginator.pageIndex = (e||{})['pageIndex']||0)+1};
    json['text'] = this.inputText||'';

    this.service.httpGet('/api/v1/0/page-group/ddl',json).then((res:IResponse)=>{
      this.isProcess = false;
      if(res.responseCode != 200){
        alert(res.responseMsg);
        return;
      }
      this.dataSource.data = res.responseData;
      this.pageSize = res.responseSize;
    });

  }

  ngOnInit() {
    this.onSearch(null);
  }

  onSel(data){
    this.dialogRef.close(data);
  }

  onClose(){
    this.dialogRef.close();
  }

}
