import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ApiService, IResponse } from 'src/app/shared/service/api.service';
import { MatDialogRef, MatTableDataSource, MatPaginator} from '@angular/material';
import Swal from 'sweetalert2/dist/sweetalert2.js';
import 'sweetalert2/src/sweetalert2.scss';
import { UtilService } from 'src/app/_util/util.service';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-tec002-dialog',
  templateUrl: './tec002-dialog.component.html',
  styleUrls: ['./tec002-dialog.component.scss'] 
})
export class Tec002DialogComponent implements OnInit {

  isProcess:boolean = false;
  searchForm = this.fb.group({
    'classId':['']
    , 'p':['']
    , 'result':[10]
  });
 
  headerSelected:Boolean;
  isSelected:any;
  checklist:any;
  _allChecklist:any;

  userName:String;
  yearGroupId:any;
  yearGroupName:any;
  className:any;
  classLnameTh:any;
  classRoomId:any;
  classRoom:any;
 
  teacherClassRoomList:[]; 
  selectTeacher = false;

  pageSize:number;
  displayedColumns: string[] = ['no','titleId','firstnameTh','lastnameTh'];
  dataSource = new MatTableDataSource();

  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  constructor(
    private service:ApiService,
    public dialogRef: MatDialogRef<Tec002DialogComponent>, 
    private fb: FormBuilder, 
    private translate: TranslateService,
    public util: UtilService
  ) { }
  
  ngOnInit() {  
    this.userName = localStorage.getItem('userName')||'';
    this.yearGroupId = localStorage.getItem("tec002_yearGroupId");
    this.yearGroupName = localStorage.getItem("tec002_yearGroupName");
    this.searchForm.controls.classId.setValue(localStorage.getItem("tec002_classId"));
    this.className = localStorage.getItem("tec002_className");
    this.classLnameTh = localStorage.getItem("tec002_classLnameTh");
    this.classRoomId = localStorage.getItem("tec002_classRoomId");
    this.classRoom = localStorage.getItem("tec002_classRoom");
    this.onSearch(0);
  }
 
  onSearch(e){
    this.isProcess = true; 
    this.headerSelected = false;
    this.searchForm.value.p = (this.paginator.pageIndex = e.pageIndex||0)+1;
    this.service.httpGet('/api/v1/tbTeacherProfile/classroom',this.searchForm.value).then((res:IResponse)=>{
      this.dataSource.data = res.responseData||[];
      this.checklist = this.dataSource.data;
      this.pageSize = res.responseSize||0; 
    });
  }

  checkAll() { 
    for (var i = 0; i < this.checklist.length; i++) {
      this.checklist[i].isSelected = this.headerSelected;
    }
    this.getCheckedItemList();
  }
 
  isAllSelected() {
    this.checklist.every(function(item:any) {
        return item.isSelected == true;
    }) 
    this.headerSelected = false;
    this.getCheckedItemList();
  }

  getCheckedItemList(){
    this._allChecklist = [];
    for (var i = 0; i < this.checklist.length; i++) {
      if(this.checklist[i].isSelected){
        let itemData = {
            'teacherId':this.checklist[i].teacherId, 
            'fullname':this.checklist[i].fullname, 
            'fullnameEn':this.checklist[i].fullnameEn,
            'stuCode':this.checklist[i].teacherCardId,
            'classId':this.searchForm.value.classId,
            'classRoomId':this.classRoomId
        };
        this._allChecklist.push(itemData);  
      }
    }
    if(this._allChecklist.length == 0){
      this.selectTeacher = false;
    }else{
      this.selectTeacher = true;
    }
  }

  onSave(){
    Swal.fire({
        title: this.translate.instant('psms.DL0001'), 
        icon : 'info',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: this.translate.instant('psms.DL0008'),
        cancelButtonText: this.translate.instant('psms.DL0009')
      }).then((result) => {
      if(result.dismiss == 'cancel'){
          return;
      } 
      let json = this._allChecklist;
      this.service.httpPut('/api/v1/tbTeacherProfile/classroom/'+this.userName,json).then((res:IResponse)=>{
        if((res.responseCode||500)!=200){
            Swal.fire(
              this.translate.instant('message.save_error'),
              res.responseMsg,
              'error' 
            )
          return;
        }
         this.onTeacherClassRoom();
      });
    });
  } 

  onTeacherClassRoom(){  
    for (var i = 0; i < this.checklist.length; i++) { 
      if(this.checklist[i].isSelected){
        let jsonInsert = {
            'memberId':this.checklist[i].teacherId, 
            'fullname':this.checklist[i].fullname, 
            'fullnameEn':this.checklist[i].fullnameEn, 
            'stuCode':this.checklist[i].teacherCardId,
            'yearGroupId':this.yearGroupId, 
            'yearGroupName':this.yearGroupName, 
            'classId':this.searchForm.value.classId,
            'className':this.className,
            'classRoomId':this.classRoomId,
            'classRoom':this.classRoom,
            'role':'teacher'
        };
        this.service.httpGet('/api/v1/0/teacher-classRoomMember/ddl',jsonInsert).then((res)=>{
            this.teacherClassRoomList = res||[];
            if(this.teacherClassRoomList.length == 0){
              this.service.httpPost('/api/v1/_teacherClassRoomMember',jsonInsert).then((res:IResponse)=>{
                  if((res.responseCode||500)!=200){
                    Swal.fire(
                      this.translate.instant('message.save_error'),
                      res.responseMsg,
                      'error' 
                    )
                    return;
                  }
              });
            }else{
              let classRoomMemberId = res[0]['classRoomMemberId'];
              let class_teacherId = res[0]['memberId'];
              let class_fullname = res[0]['fullname'];
              let class_fullnameEn = res[0]['fullnameEn'];
              let class_stuCode = res[0]['stuCode'];
              let jsonUpdate = {
                  'classRoomMemberId':classRoomMemberId, 
                  'memberId':class_teacherId, 
                  'fullname':class_fullname, 
                  'fullnameEn':class_fullnameEn, 
                  'stuCode':class_stuCode,
                  'yearGroupId':this.yearGroupId, 
                  'yearGroupName':this.yearGroupName, 
                  'classId':this.searchForm.value.classId,
                  'className':this.className,
                  'classRoomId':this.classRoomId,
                  'classRoom':this.classRoom,
                  'role':'teacher'
              };
              this.service.httpPut('/api/v1/_teacherClassRoomMember',jsonUpdate).then((res:IResponse)=>{
                  if((res.responseCode||500)!=200){
                    Swal.fire(
                      this.translate.instant('message.edit_error'),
                      res.responseMsg,
                      'error'
                    )
                    return;
                  }
              });
            } 
        }); 
      }
    }
    Swal.fire( 
      this.translate.instant('message.save_header'), 
      this.translate.instant('message.save'),
      'success'
    ).then(() => this.close());
  }

  onCancel(){
    this.searchForm = this.fb.group({
      'firstnameTh':['']
      , 'lastnameTh':['']
    });
    this.onSearch(0);
  }
  
  close(){
    this.dialogRef.close(); 
  } 

}
