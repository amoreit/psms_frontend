import { Component, OnInit, Inject } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ApiService, IResponse } from 'src/app/shared/service/api.service';
import { MAT_DIALOG_DATA, MatDialogRef, MatTableDataSource } from '@angular/material';
import 'sweetalert2/src/sweetalert2.scss';

@Component({
  selector: 'app-gra005-dialog',
  templateUrl: './gra005-dialog.component.html',
  styleUrls: ['./gra005-dialog.component.scss']
})
export class Gra005DialogComponent implements OnInit {

  form = this.fb.group({
    'classId':['']
    ,'yearGroupId':['']

  });

  displayedColumns: string[] = [ "no","num1","num2"];
  dataSource = new MatTableDataSource();

  constructor(
    private service:ApiService,
    @Inject(MAT_DIALOG_DATA) data:any,
    public dialogRef: MatDialogRef<Gra005DialogComponent>,
    private fb: FormBuilder,
  ) { 
    this.form.controls.yearGroupId.setValue(data.yearGroupId||'');
    this.form.controls.classId.setValue(data.classId||'');
  }

  ngOnInit() {
    this.onSearch(0)
  }

  //search ค้นหา
  onSearch(e) {
    this.service.httpGet("/api/v1/stu-profile/gra005", this.form.value)
      .then((res: IResponse) => {
        this.dataSource.data = res.responseData || [];
    });
  }

  close() { 
    this.dialogRef.close(); 
  }

}
