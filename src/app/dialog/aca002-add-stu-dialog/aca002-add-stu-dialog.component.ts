import { Component, OnInit, ViewChild, Inject } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ApiService, IResponse } from 'src/app/shared/service/api.service';
import { MAT_DIALOG_DATA, MatDialogRef, MatTableDataSource, MatPaginator } from '@angular/material';
import { Router } from '@angular/router';
import Swal from 'sweetalert2/dist/sweetalert2.js';
import 'sweetalert2/src/sweetalert2.scss';
import { UtilService } from 'src/app/_util/util.service';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-aca002-add-stu-dialog',
  templateUrl: './aca002-add-stu-dialog.component.html',
  styleUrls: ['./aca002-add-stu-dialog.component.scss']
})
export class Aca002AddStuDialogComponent implements OnInit {

  isProcess: boolean = false;
  searchForm = this.fb.group({ 
    'stuCode': ['']
    , 'firstnameTh': ['']
    , 'lastnameTh': ['']
    , 'p': ['']
    , 'result': [10]
  });

  headerSelected: Boolean;
  isSelected: any;
  checklist: any;
  _allChecklist: any;
  _allChecklist2: any;

  userName: String;
  yearGroupId: any;
  yearGroupName: any;
  departmentId: any;
  departmentName: any;
  catClassId: any;
  year_group_id: any;
  year_group_name: any;
  class_room_id: any;
  class_room_name: any
  class_id: any
  class_name: any; 
  is_ep: any;

  checkStu = false;

  teacherDepartmentList: [];
  classData = [];

  pageSize: number;
  displayedColumns: string[] = ['no', 'stuCode', 'fullname'];
  dataSource = new MatTableDataSource();

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  constructor(
    private translate: TranslateService, 
    private service: ApiService,
    public dialogRef: MatDialogRef<Aca002AddStuDialogComponent>,
    private fb: FormBuilder,
    public util: UtilService,
    @Inject(MAT_DIALOG_DATA) data: any,
  ) {
    this.year_group_id = data.yearGroupId;
    this.year_group_name = data.yearGroupName
    this.class_room_id = data.classRoomId;
    this.class_room_name = data.classRoomName
    this.is_ep = data.isEp;
  }

  ngOnInit() {
    this.userName = localStorage.getItem('userName') || '';
    this.onSearch(0);
    this.findClassIdAndName();
  }

  onSearch(e) {
    this.isProcess = true;
    this.headerSelected = false;
    this.searchForm.value.p = (this.paginator.pageIndex = e.pageIndex || 0) + 1;
    this.service.httpGet('/api/v1/trn-classroom-member/stufordialog', this.searchForm.value).then((res: IResponse) => {
      this.dataSource.data = res.responseData || [];
      this.checklist = this.dataSource.data;
      this.pageSize = res.responseSize || 0;
    });
  }

  findClassIdAndName() {
    this.service.httpGet('/api/v1/trn-classroom-member/classidandclassnamebyclassroomid', { 'classRoomId': this.class_room_id }).then((res: IResponse) => {
      this.classData = res.responseData || [];
      this.class_id = this.classData[0].class_id;
      this.class_name = this.classData[0].class_sname_th;
    });
  }

  checkAll() {
    for (var i = 0; i < this.checklist.length; i++) {
      this.checklist[i].isSelected = this.headerSelected;
    }
    this.getCheckedItemList();
  }

  isAllSelected() {
    this.checklist.every(function (item: any) {
      return item.isSelected === true;
    })
    this.headerSelected = false;
    this.getCheckedItemList();
  }

  getCheckedItemList() {
    this._allChecklist = [];
    for (var i = 0; i < this.checklist.length; i++) {
      if (this.checklist[i].isSelected) {
        let itemData = {
          'scCode': 'PRAMANDA',
          'yearGroupId': this.year_group_id,
          'yearGroupName': this.year_group_name,
          'memberId': this.checklist[i].stu_profile_id,
          'stuCode': this.checklist[i].stu_code,
          'fullname': this.checklist[i].fullname,
          'fullnameEn': this.checklist[i].fullname_en,
          'classId': this.class_id,
          'className': this.class_name,
          'classRoomId': this.class_room_id[0],
          'classRoom': this.class_room_name,
          'role': 'student',
          'isEp': this.is_ep
        };
        this._allChecklist.push(itemData);
      }
    }
    if(this._allChecklist.length == 0){
      this.checkStu = false;
    }else{
      this.checkStu = true;
    }
  }

  onSave() {
    Swal.fire({
      title: this.translate.instant('alert.save'), //ต้องการบันทึกข้อมูล ใช่หรือไม่
      icon: 'info',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: this.translate.instant('psms.DL0008'),//ใช่
      cancelButtonText: this.translate.instant('psms.DL0009')//ไม่
    }).then((result) => {
      if (result.dismiss == 'cancel') {
        return;
      }
      let json = this._allChecklist;
      this.service.httpPost('/api/v1/trn-classroom-member/stufordialog/' + this.userName, json).then((res: IResponse) => {
        if ((res.responseCode || 500) != 200) {
          Swal.fire(
            this.translate.instant('alert.save_student_error'),//บันทึกข้อมูลนักเรียนผิดพลาด
            res.responseMsg,
            'error'
          )
          return;
        }
        Swal.fire(
          this.translate.instant('alert.save_student_header'),//บันทึกข้อมูลนักเรียน
          this.translate.instant('alert.save_student_success'),//บันทึกข้อมูลนักเรียนสำเร็จ
          'success'
        ).then(() => { this.close(); })
      });
    });
  }

  onCancel() {
    this.searchForm = this.fb.group({
      'stuCode': ['']
      , 'firstnameTh': ['']
      , 'lastnameTh': ['']
    });
    this.onSearch(0);
  }

  close() {
    this.dialogRef.close();
  }

}
