import { Component, OnInit, Inject } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ApiService, IResponse } from 'src/app/shared/service/api.service';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import Swal from 'sweetalert2/dist/sweetalert2.js';
import 'sweetalert2/src/sweetalert2.scss';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-stu005-isleft-dialog',
  templateUrl: './stu005-isleft-dialog.component.html',
  styleUrls: ['./stu005-isleft-dialog.component.scss']
})
export class Stu005IsleftDialogComponent implements OnInit {
  form = this.fb.group({
    'stuProfileId': ['']
    , 'leftReason': ['']
    , 'isleft': [true]
  });
 
  classRoomList: [];
  class_room_id: any;
  role: any;
  member_id: any;
  stu_profile_id: any;

  constructor(
    private service: ApiService,
    @Inject(MAT_DIALOG_DATA) data: any,
    public dialogRef: MatDialogRef<Stu005IsleftDialogComponent>,
    private translate: TranslateService,
    private fb: FormBuilder
  ) {
    this.form.controls.stuProfileId.setValue(data.stuProfileId);
  }

  ngOnInit() {

  }

  close() {
    this.dialogRef.close();
  }

  onSave2() {
    Swal.fire({
      title: this.translate.instant('alert.isleft_save'),
      icon: 'info',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: this.translate.instant('psms.DL0008'),
      cancelButtonText: this.translate.instant('psms.DL0009')
    }).then((result) => {
      if (result.dismiss == 'cancel') {
        return;
      }
      let json = this.form.value
      if (this.form.value.leftReason == '') {
        Swal.fire(
          this.translate.instant('alert.isleft_error'),
          this.translate.instant('alert.validate'),
          'error'
        )
      } else
        this.service.httpPut('/api/v1/stu-profile/updateisleft', json).then((res: IResponse) => {
          if ((res.responseCode || 500) != 200) {
            Swal.fire(
              this.translate.instant('alert.isleft_error'),
              res.responseMsg,
              'error'
            )
            return;
          }
          Swal.fire(
            this.translate.instant('message.isleft_save'),
            this.translate.instant('message.isleft_save_success'),
            'success'
          ).then(() => { this.close(); })
        })
      return;
    });
  }
}
