import { Component, OnInit, ViewChild, Inject } from '@angular/core';
import { IResponse, ApiService } from 'src/app/shared/service/api.service';
import Swal from 'sweetalert2/dist/sweetalert2.js';
import 'sweetalert2/src/sweetalert2.scss';
import { UtilService } from 'src/app/_util/util.service';
import { MatTableDataSource, MatPaginator, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { FormBuilder } from '@angular/forms';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-aca008-tecther-dialog',
  templateUrl: './aca008-tecther-dialog.component.html',
  styleUrls: ['./aca008-tecther-dialog.component.scss']
})
export class Aca008TectherDialogComponent implements OnInit {

  searchForm = this.fb.group({
    'teacherId':['']
    , 'scCode':['']
    , 'firstnameTh':['']
    , 'lastnameTh':['']
    , 'classId':['']
    , 'classRoomId':['']
    , 'p':['']
    , 'result':[10]
  });

  headerSelected:Boolean;
  isSelected:any;
  checklist:any;
  _allChecklist:any;
  _yearGroupId:any;
  _yearGroupName:any;
  _activityGroupId:any;
  _activityId:any;
  _activityName:any;
  _classId:any;
  _className:any;

  checkTeacher = false;

  yearGroupList:[];
  classList:[];

  pageSize:number;
  displayedColumns: string[] = ['no','titleId','firstnameTh','lastnameTh'];
  dataSource = new MatTableDataSource();

  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  constructor(
    private translate: TranslateService,
    @Inject(MAT_DIALOG_DATA) data:any,
    private service:ApiService,
    public dialogRef: MatDialogRef<Aca008TectherDialogComponent>, 
    private fb: FormBuilder, 
    public util: UtilService
  ) { 
    this._yearGroupName = data.yearGroupName; 
    let jsonYear = {'yearGroupName':this._yearGroupName};
    this.service.httpGet('/api/v1/0/year-group/searchId',jsonYear).then((res)=>{
      this.yearGroupList = res||[];
      this._yearGroupId = res[0]['yearGroupId'];
    });
    this._activityGroupId = data.activityGroupId; 
    this._activityId = data.activityId; 
    this._activityName = data.activityName; 
  }
  
  ngOnInit() {  
    this._classId = parseInt(localStorage.getItem('classId')); 
    let jsonClass = {'classId':this._classId};
    this.service.httpGet('/api/v1/0/tbMstClass/ddlAca008',jsonClass).then((res)=>{
      this.classList = res||[];
      this._className = res[0]['classSnameTh'];
    });
    this.onSearch(0);
  }
 
  onSearch(e){
    this.headerSelected = false;
    this.searchForm.value.p = (this.paginator.pageIndex = e.pageIndex||0)+1;
    this.service.httpGet('/api/v1/tbTeacherProfile',this.searchForm.value).then((res:IResponse)=>{
      this.dataSource.data = res.responseData||[];
      this.checklist = this.dataSource.data;
      this.pageSize = res.responseSize||0; 
    });
  }

  checkAll() {
    for (var i = 0; i < this.checklist.length; i++) {
      this.checklist[i].isSelected = this.headerSelected;
    }
    this.getCheckedItemList();
  }
 
  isAllSelected() {
    this.checklist.every(function(item:any) {
        return item.isSelected === true;
    })
    this.headerSelected = false;
    this.getCheckedItemList();
  }

  getCheckedItemList(){
    this._allChecklist = [];
    for (var i = 0; i < this.checklist.length; i++) {
      if(this.checklist[i].isSelected){
        let itemData = {'teacherId':this.checklist[i].teacherId, 
                        'fullname':this.checklist[i].fullname, 
                       };
        this._allChecklist.push(itemData); 
      }
    }
    if(this._allChecklist.length == 0){
      this.checkTeacher = false;
    }else{
      this.checkTeacher = true;
    }
  }

  onSave(){
    Swal.fire({
        title: this.translate.instant('alert.save'), //ต้องการบันทึกข้อมูล ใช่หรือไม่
        icon : 'info',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: this.translate.instant('psms.DL0008'),//ใช่
        cancelButtonText: this.translate.instant('psms.DL0009')//ไม่
      }).then((result) => {
      if(result.dismiss == 'cancel'){
          return;
      } 
      for (var i = 0; i < this.checklist.length; i++) {
        if(this.checklist[i].isSelected){
          let jsonInsert = {
              'teacherId':this.checklist[i].teacherId, 
              'fullname':this.checklist[i].fullname, 
              'yearGroupId':this._yearGroupId, 
              'yearGroupName':this._yearGroupName, 
              'classId':this._classId, 
              'className':this._className, 
              'activityGroupId':this._activityGroupId, 
              'activityId':this._activityId, 
              'activityName':this._activityName, 
          };
          this.service.httpPost('/api/v1/_activityTeacher',jsonInsert).then((res:IResponse)=>{
            if((res.responseCode||500)!=200){
                Swal.fire(
                  this.translate.instant('message.save_tec_error'),//บันทึกข้อมูลครูผิดพลาด
                  res.responseMsg,
                  'error' 
                )
              return;
            }
            Swal.fire( 
              this.translate.instant('message.save_header'),//บันทึกข้อมูล
              this.translate.instant('message.save'),//บันทึกข้อมูลสำเร็จ
              'success'
            ).then(() => this.close());
          });
        }
      }
    });
  } 

  onCancel(){
    this.searchForm = this.fb.group({
      'firstnameTh':['']
      , 'lastnameTh':[''] 
    });
    this.onSearch(0);
  }
  
  close() {
    localStorage.setItem('dialogTea_classId',this._classId);
    localStorage.setItem('dialogTea_yearGroupName',this._yearGroupName);
    localStorage.setItem('dialogTea_activityGroupId',this._activityGroupId);
    this.dialogRef.close(); 
  } 

}
