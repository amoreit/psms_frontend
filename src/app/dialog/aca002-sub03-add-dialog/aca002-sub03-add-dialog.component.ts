import { Component, OnInit, Inject } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ApiService, IResponse } from 'src/app/shared/service/api.service';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import Swal from 'sweetalert2/dist/sweetalert2.js';
import 'sweetalert2/src/sweetalert2.scss';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-aca002-sub03-add-dialog',
  templateUrl: './aca002-sub03-add-dialog.component.html',
  styleUrls: ['./aca002-sub03-add-dialog.component.scss']
})
export class Aca002Sub03AddDialogComponent implements OnInit {
  form = this.fb.group({
    'teacher1': ['']
    , 'teacher2': ['']
    , 'teacherFullname1': ['']
    , 'teacherFullname2': ['']
    , 'classRoomSubjectId': ['']
  });

  teacherProfileList: [];
  department_id: any; 
  teacher_type: any;

  constructor(
    private service: ApiService,
    @Inject(MAT_DIALOG_DATA) data: any,
    public dialogRef: MatDialogRef<Aca002Sub03AddDialogComponent>,
    private translate: TranslateService,
    private fb: FormBuilder
  ) {
    this.department_id = data.departmentId;
    console.log(" this.department_id : "+this.department_id);
    this.form.controls.classRoomSubjectId.setValue(data.classRoomSubjectId || '');
    this.teacher_type = data.teacherType;

  }

  ngOnInit() {
    this.ddlTeacherProfile();

  } 
 
  close() {
    this.dialogRef.close();
  } 

  ddlTeacherProfile() {
    let json = { 'departmentId': this.department_id };
    this.service.httpGet('/api/v1/teacher-profile/ddlaca002sub03', json).then((res) => {
      this.teacherProfileList = res || [];
    });
  }

  onCheckTeacherProfile() {
    let ele = this.teacherProfileList.filter((o) => {
      return o['teacher_id'] == this.form.value.teacher1;
    });
    this.form.controls.teacherFullname1.setValue(ele[0]['fullname']);
  }

  onCheckTeacher2Profile() {
    let ele = this.teacherProfileList.filter((o) => {
      return o['teacher_id'] == this.form.value.teacher2;
    });
    this.form.controls.teacherFullname2.setValue(ele[0]['fullname']);
  }

  onSave2() {
    Swal.fire({
      title: this.translate.instant('alert.teacher_add'),//ต้องการเพิ่มครูผู้สอน ใช่หรือไม่?
      icon: 'info',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: this.translate.instant('psms.DL0008'),
      cancelButtonText: this.translate.instant('psms.DL0009')
    }).then((result) => {
      if (result.dismiss == 'cancel') {
        return;
      }
      let json = this.form.value
      let user = localStorage.getItem('userName')
      if (this.teacher_type == 1) {
        /******************************************** */
        if (this.form.value.teacher1 == '' || this.form.value.teacherFullname1 == '') {
          Swal.fire(
            this.translate.instant('alert.teacher_add_error'),//เพิ่มครูผู้สอนผิดพลาด
            this.translate.instant('alert.teacher_selected'),//กรุณาเลือกครูผู้สอน
            'error'
          )
        } else {
          this.service.httpPut('/api/v1/trn-class-room-subject/addteacher1/' + user, {
            'teacher1': this.form.value.teacher1, 'teacherFullname1': this.form.value.teacherFullname1,
            'classRoomSubjectId': this.form.value.classRoomSubjectId
          }).then((res: IResponse) => {
            if ((res.responseCode || 500) != 200) {
              Swal.fire(
                this.translate.instant('message.save_error'),//บันทึกข้อมูลผิดพลาด
                res.responseMsg,
                'error'
              )
              return;
            }
            Swal.fire(
              this.translate.instant('alert.teacher_add_header'),//เพิ่มครูผู้สอน
              this.translate.instant('alert.teacher_add_success'),//เพิ่มครูผู้สอนสำเร็จ
              'success'
            ).then(() => { this.close(); })
          })
        }
      } else {
        if (this.form.value.teacher2 == '' || this.form.value.teacherFullname2 == '') {
          Swal.fire(
            this.translate.instant('alert.teacher_add_error'),//เพิ่มครูผู้สอนผิดพลาด
            this.translate.instant('alert.teacher_selected'),//กรุณาเลือกครูผู้สอน
            'error'
          )
        } else {
          this.service.httpPut('/api/v1/trn-class-room-subject/addteacher2/' + user, {
            'teacher2': this.form.value.teacher2, 'teacherFullname2': this.form.value.teacherFullname2,
            'classRoomSubjectId': this.form.value.classRoomSubjectId
          }).then((res: IResponse) => {
            if ((res.responseCode || 500) != 200) {
              Swal.fire(
                this.translate.instant('message.save_error'),//บันทึกข้อมูลผิดพลาด
                res.responseMsg,
                'error'
              )
              return;
            }
            Swal.fire(
              this.translate.instant('alert.teacher_add_header'),//เพิ่มครูผู้สอน
              this.translate.instant('alert.teacher_add_success'),//เพิ่มครูผู้สอนสำเร็จ
              'success'
            ).then(() => { this.close(); })
          })
        }
      }
      /************************************** */

      return;
    });
  }


}
