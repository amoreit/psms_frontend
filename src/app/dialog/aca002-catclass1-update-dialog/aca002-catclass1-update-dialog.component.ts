import { Component, OnInit, Inject } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ApiService, IResponse } from 'src/app/shared/service/api.service';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { Router } from '@angular/router';
import Swal from 'sweetalert2/dist/sweetalert2.js';
import 'sweetalert2/src/sweetalert2.scss';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-aca002-catclass1-update-dialog',
  templateUrl: './aca002-catclass1-update-dialog.component.html',
  styleUrls: ['./aca002-catclass1-update-dialog.component.scss']
})
export class Aca002Catclass1UpdateDialogComponent implements OnInit {

  updateSubjectForm = this.fb.group({
    'yearGroupId': ['']
    , 'classId': ['']
    , 'subjectCode': ['']
  }); 

  form = this.fb.group({
    'subjectCode': ['']
    , 'classId': ['']
    , 'scoreId': ['']
    , 'yearGroupId': ['']
    , 'teacher1': ['']
    , 'teacherFullname1': ['']
  });

  classRoomSubjectList: [];
  teacherProfileList: [];
  scoreList: [];
  dataSubject = [];
  department_id: any;
  class_id: any;

  constructor(
    private service: ApiService,
    @Inject(MAT_DIALOG_DATA) data: any,
    public dialogRef: MatDialogRef<Aca002Catclass1UpdateDialogComponent>,
    private fb: FormBuilder,
    private translate: TranslateService,
    private router: Router
  ) {
    this.updateSubjectForm.controls.yearGroupId.setValue(data.yearGroupId || '');
    this.updateSubjectForm.controls.classId.setValue(data.classId || '');
    this.updateSubjectForm.controls.subjectCode.setValue(data.subjectCode || '');
    this.form.controls.yearGroupId.setValue(data.yearGroupId || '');
    this.form.controls.classId.setValue(data.classId || '');
    this.department_id = data.departmentId;
    this.class_id = data.classId || '';
  }

  ngOnInit() {
    this.loadData();
    this.ddlClassRoomSubject();
    this.ddlTeacherProfile();
    this.ddlScore();
  }

  close() {
    this.dialogRef.close();
  }

  loadData() {
    this.service.httpGet('/api/v1/trn-class-room-subject/subjectacac002update', this.updateSubjectForm.value).then((res: IResponse) => {
      let data = res.responseData || [];
      this.dataSubject = res.responseData || [];
      this.form.controls.subjectCode.setValue(data[0].subjectCode)
      this.form.controls.teacher1.setValue(data[0].teacher1 || '')
      this.form.controls.teacherFullname1.setValue(data[0].teacherFullname1)
      this.form.controls.scoreId.setValue(data[0].scoreId)
    });
  }

  ddlClassRoomSubject() {
    let json = { 'departmentId': this.department_id };
    this.service.httpGet('/api/v1/class-room-subject/ddl', json).then((res) => {
      this.classRoomSubjectList = res || [];
    });
  }

  ddlTeacherProfile() {
    let json = { 'departmentId': this.department_id };
    this.service.httpGet('/api/v1/teacher-profile/ddl', json).then((res) => {
      this.teacherProfileList = res || [];
    });
  }


  onCheckTeacherProfile() {
    let ele = this.teacherProfileList.filter((o) => {
      return o['teacher_id'] == this.form.value.teacher1;
    });
    this.form.controls.teacherFullname1.setValue(ele[0]['fullname']);
  }

  ddlScore() {
    this.service.httpGet('/api/v1/0/score/ddl', { 'iscatclass1': 'true' }).then((res) => {
      this.scoreList = res || [];
    });
  }

  onSave2() {
    this.dataSubject.map(e => {
      e['yearGroupId'] = this.form.value.yearGroupId;
      e['classId'] = this.form.value.classId;
      e['scoreId'] = this.form.value.scoreId;
      e['teacher1'] = this.form.value.teacher1;
      e['teacherFullname1'] = this.form.value.teacherFullname1;
      return e;
    });

    Swal.fire({
      title: this.translate.instant('psms.DL0001'),//ต้องการบันทึกข้อมูล ใช่หรือไม่?
      icon: 'info',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: this.translate.instant('psms.DL0008'),
      cancelButtonText: this.translate.instant('psms.DL0009')
    }).then((result) => {
      if (result.dismiss == 'cancel') {
        return;
      }
      let json = this.dataSubject
      let user = localStorage.getItem('userName')
      if (this.form.value.scoreId == '') {
        Swal.fire(
          this.translate.instant('message.edit_error'),//แก้ไขข้อมูลผิดพลาด 
          this.translate.instant('alert.validate'),//กรุณาเลือกข้อมูลให้ครบ
          'error'
        )
      } else {
        this.service.httpPut('/api/v1/trn-class-room-subject/catclass1/' + user, json).then((res: IResponse) => {
          if ((res.responseCode || 500) != 200) {
            Swal.fire(
              this.translate.instant('message.edit_error'),//แก้ไขข้อมูลผิดพลาด
              res.responseMsg,
              'error'
            )
            return;
          }
          Swal.fire(
            this.translate.instant('psms.DL0006'),//แก้ไขข้อมูล
            this.translate.instant('message.edit'),//แก้ไขข้อมูลสำเร็จ
            'success'
          )
        }).then(() => { this.close() })
      }
      return;
    });
  }
}
