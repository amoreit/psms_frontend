import { Component, OnInit } from '@angular/core';
import { IResponse, ApiService } from 'src/app/shared/service/api.service';
import { MatTableDataSource, MatDialogRef } from '@angular/material';
import { FormBuilder } from '@angular/forms';
import { UtilService } from 'src/app/_util/util.service';
import Swal from 'sweetalert2/dist/sweetalert2.js';
import 'sweetalert2/src/sweetalert2.scss';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-tec006-leader-dialog',
  templateUrl: './tec006-leader-dialog.component.html',
  styleUrls: ['./tec006-leader-dialog.component.scss']
})
export class  Tec006LeaderDialogComponent implements OnInit {

  searchForm = this.fb.group({
    'catClassId':[''] 
    , 'className':['']
    , 'yearGroupName':[''] 
  }); 
 
  _teacherClassId:any;
  userName:String;
  yearGroupList:[];
  catClassList:[];
  classList:[];

  displayedColumns: string[] = ['yearGroupName','fullname','className','leader','manage'];
  dataSource = new MatTableDataSource();

  constructor(
      private fb: FormBuilder,
      private service: ApiService,
      public dialogRef: MatDialogRef<Tec006LeaderDialogComponent>,
      private translate: TranslateService,
      public util: UtilService
  ) { } 

  ngOnInit() { 
    this.userName = localStorage.getItem('userName')||'';
    this.ddlYearGroup();
    this.ddlCatClass();
  }  

  ddlYearGroup(){ 
    this.service.httpGet('/api/v1/0/year-group/ddl',null).then((res)=>{
      this.yearGroupList = res||[];
      this.searchForm.controls.yearGroupName.setValue(res[0]['name']);
    });
  }

  ddlCatClass(){
    this.service.httpGet('/api/v1/0/tbMstCatClass/ddl',null).then((res)=>{
      this.catClassList= res||[];
    }); 
  }
 
  ddlClass(){
    let json = {'catClassId':this.searchForm.value.catClassId};
    this.service.httpGet('/api/v1/0/tbMstClass/ddlteacherClass',json).then((res)=>{
      this.classList= res||[];
    }); 
  }

  onSearch(){
    this.service.httpGet('/api/v1/_teacherClass/leader',this.searchForm.value).then((res:IResponse)=>{
      this.dataSource.data = res.responseData||[];
    });
  }

  onCancel(){
    this.searchForm.controls.catClassId.setValue("");
    this.searchForm.controls.className.setValue("");
    this.searchForm.controls.yearGroupName.setValue("");
    this.onSearch();
  }

  onSave(_id){
    this._teacherClassId = _id; 
    Swal.fire({
        title: this.translate.instant('alert.leader'),//ต้องการเลือกท่านนี้ ใช่หรือไม่? 
        icon : 'info',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: this.translate.instant('psms.DL0008'),
        cancelButtonText: this.translate.instant('psms.DL0009')
      }).then((result) => {
      if(result.dismiss == 'cancel'){
          return;
      } 
      this.dataSource.data.map((e) => {
        e['leader'] = false;
        return e;
      });
      this.service.httpPut('/api/v1/_teacherClass/updateLeader/'+ this.userName,this.dataSource.data).then((res:IResponse)=>{
        if((res.responseCode||500)!=200){
            Swal.fire(
              this.translate.instant('alert.leader_classLevel_error'),//เลือกหัวหน้าระดับชั้นผิดพลาด
              res.responseMsg,
              'error' 
            )
          return;
        }
         this.leaderTrue();
      });
    });
  } 

  leaderTrue(){
    let _updateLeader = {'teacherClassId':this._teacherClassId,'leader':true};
    this.service.httpPut('/api/v1/_teacherClass/updateTrue',_updateLeader).then((res:IResponse)=>{
      if((res.responseCode||500)!=200){
          Swal.fire(
            this.translate.instant('alert.leader_classLevel_error_true'),//เลือกหัวหน้าระดับ true ชั้นผิดพลาด
            res.responseMsg,
            'error' 
          )
        return;
      }
      Swal.fire(
        this.translate.instant('alert.leader_classLevel_header'),//เลือกหัวหน้าระดับชั้น
        this.translate.instant('alert.leader_classLevel_success'),//เลือกหัวหน้าระดับชั้นสำเร็จ
        'success'
      ).then(() => this.close());
    });
  }

  close() {
    localStorage.setItem("tecclass_yearGroupName",this.searchForm.value.yearGroupName);
    localStorage.setItem("tecclass_catClassId",this.searchForm.value.catClassId);
    localStorage.setItem("tecclass_className",this.searchForm.value.className);
    this.dialogRef.close(); 
  } 
}
   