import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA, MatTableDataSource } from '@angular/material';
import { ApiService, IResponse } from 'src/app/shared/service/api.service';
import { FormBuilder } from '@angular/forms';
import { UtilService } from 'src/app/_util/util.service';
import Swal from 'sweetalert2/dist/sweetalert2.js';
import 'sweetalert2/src/sweetalert2.scss';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-aca008-dialog',
  templateUrl: './aca008-dialog.component.html', 
  styleUrls: ['./aca008-dialog.component.scss']
})
export class Aca008DialogComponent implements OnInit { 

  searchForm = this.fb.group({
    'yearGroupName':[''] 
    , 'activityGroupId':[''] 
    , 'classId':['']
  });
  headerSelected:Boolean; 
  isSelected:any;
  checklist:any;
  _allChecklist:any;
  _activityId:any;
  _activityName:any;
  _activityLesson:any;
  userName:any;
  yearGroupList:[];

  checkStu = false;

  displayedColumns: string[] = ['no','classRoomNo','fullname','classRoom']; 
  dataSource = new MatTableDataSource();
  
  constructor(
    private translate: TranslateService,
    @Inject(MAT_DIALOG_DATA) data:any,
    private service:ApiService,
    public dialogRef: MatDialogRef<Aca008DialogComponent>, 
    private fb: FormBuilder, 
    public util: UtilService,
  ) { 
    this.searchForm.controls.yearGroupName.setValue(data.yearGroupName); 
    this.searchForm.controls.activityGroupId.setValue(data.activityGroupId); 
    this.searchForm.controls.classId.setValue(parseInt(localStorage.getItem('classId'))); 
    this._activityId = data.activityId; 
    this._activityName = data.activityName; 
    this._activityLesson = data.activityLesson; 
    this.onSearch();
  }

  ngOnInit() {
    this.userName = localStorage.getItem('userName')||'';
  }

  onSearch(){
    this.headerSelected = false;
    let json = this.searchForm.value;
    this.service.httpGet('/api/v1/_trnClassRoomMember/dialog/activity-stu',json).then((res:IResponse)=>{
      this.dataSource.data = res.responseData||[];
      this.checklist = this.dataSource.data;
    });
  }

  checkAll() { 
    for (var i = 0; i < this.checklist.length; i++) {
      this.checklist[i].isSelected = this.headerSelected;
    }
    this.getCheckedItemList();
  }
 
  isAllSelected() {
    this.checklist.every(function(item:any) {
        return item.isSelected == true;
    })
    this.headerSelected = false;
    this.getCheckedItemList();
  }

  getCheckedItemList(){
    this._allChecklist = [];
    if(this.searchForm.value.activityGroupId == 1 || this.searchForm.value.activityGroupId == 5){
      for (var i = 0; i < this.checklist.length; i++) {
        if(this.checklist[i].isSelected){
          let itemData = {
              'classRoomMemberId':this.checklist[i].class_room_member_id, 
              'fullname':this.checklist[i].fullname, 
              'activityGroupId':this.searchForm.value.activityGroupId, 
              'activity1id':this._activityId, 
              'activity1name':this._activityName, 
              'activity1lesson':this._activityLesson
          };
          this._allChecklist.push(itemData); 
        }
      }
    }else if(this.searchForm.value.activityGroupId == 2 || this.searchForm.value.activityGroupId == 6){
      for (var i = 0; i < this.checklist.length; i++) {
        if(this.checklist[i].isSelected){
          let itemData = {
              'classRoomMemberId':this.checklist[i].class_room_member_id, 
              'fullname':this.checklist[i].fullname, 
              'activityGroupId':this.searchForm.value.activityGroupId, 
              'activity2id':this._activityId, 
              'activity2name':this._activityName, 
              'activity2lesson':this._activityLesson
          };
          this._allChecklist.push(itemData); 
        }
      }
    }else if(this.searchForm.value.activityGroupId == 3 || this.searchForm.value.activityGroupId == 7){
      for (var i = 0; i < this.checklist.length; i++) {
        if(this.checklist[i].isSelected){
          let itemData = {
              'classRoomMemberId':this.checklist[i].class_room_member_id, 
              'fullname':this.checklist[i].fullname, 
              'activityGroupId':this.searchForm.value.activityGroupId, 
              'activity3id':this._activityId, 
              'activity3name':this._activityName, 
              'activity3lesson':this._activityLesson
          };
          this._allChecklist.push(itemData); 
        }
      }
    }else{
      for (var i = 0; i < this.checklist.length; i++) {
        if(this.checklist[i].isSelected){
          let itemData = {
              'classRoomMemberId':this.checklist[i].class_room_member_id, 
              'fullname':this.checklist[i].fullname, 
              'activityGroupId':this.searchForm.value.activityGroupId, 
              'activity4id':this._activityId, 
              'activity4name':this._activityName, 
              'activity4lesson':this._activityLesson
          };
          this._allChecklist.push(itemData); 
        }
      }
    }

    if(this._allChecklist.length == 0){
      this.checkStu = false;
    }else{
      this.checkStu = true;
    }
  }

  onSave(){
    Swal.fire({
        title: this.translate.instant('alert.save'), //ต้องการบันทึกข้อมูล ใช่หรือไม่
        icon : 'info',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: this.translate.instant('psms.DL0008'),//ใช่
        cancelButtonText: this.translate.instant('psms.DL0009')//ไม่
      }).then((result) => {
      if(result.dismiss == 'cancel'){
          return;
      } 
      let json = this._allChecklist;
      this.service.httpPut('/api/v1/_trnClassRoomMember/dialog/update/'+this.userName,json).then((res:IResponse)=>{
        if((res.responseCode||500)!=200){
            Swal.fire(
              this.translate.instant('message.save_tec_error'),//บันทึกข้อมูลครูผิดพลาด
              res.responseMsg,
              'error' 
            )
          return;
        }
        Swal.fire( 
          this.translate.instant('message.save_header'),//บันทึกข้อมูล
          this.translate.instant('message.save'),//บันทึกข้อมูลสำเร็จ
          'success'
        ).then(() => this.close());
      });
    });
  } 
  
  close(){
    localStorage.setItem('dialog_classId',this.searchForm.value.classId);
    localStorage.setItem('dialog_yearGroupName',this.searchForm.value.yearGroupName);
    localStorage.setItem('dialog_activityGroupId',this.searchForm.value.activityGroupId);
    this.dialogRef.close(); 
  } 

}
