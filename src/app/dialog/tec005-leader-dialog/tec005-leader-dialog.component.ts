import { Component, OnInit } from '@angular/core';
import Swal from 'sweetalert2/dist/sweetalert2.js';
import 'sweetalert2/src/sweetalert2.scss';
import { MatTableDataSource, MatDialogRef } from '@angular/material';
import { ApiService, IResponse } from 'src/app/shared/service/api.service';
import { UtilService } from 'src/app/_util/util.service';
import { FormBuilder } from '@angular/forms';
import { TranslateService } from '@ngx-translate/core';

@Component({ 
  selector: 'app-tec005-leader-dialog',
  templateUrl: './tec005-leader-dialog.component.html',
  styleUrls: ['./tec005-leader-dialog.component.scss']
})
export class Tec005LeaderDialogComponent implements OnInit {

  isProcess:boolean = false;
  searchForm = this.fb.group({
     'fullname':['']
    , 'yearGroupName':['']
    , 'departmentName':['']
    , 'catClassId':['']
  });

  _yearGroupName:any; 
  _departmentName:any;
  _catClassId:any;
  _teacherDepartmentId:any;
  userName:any;

  teacherDepartmentList:[];

  pageSize:number;
  displayedColumns: string[] = ['yearGroupName','fullname','leader','catClassId','manage'];
  dataSource = new MatTableDataSource();

  constructor(
    private service:ApiService,
    public dialogRef: MatDialogRef<Tec005LeaderDialogComponent>, 
    private fb: FormBuilder, 
    public util: UtilService,
    private translate: TranslateService
  ) { }
  
  ngOnInit() { 
    this.userName = localStorage.getItem('userName')||'';
    this._yearGroupName = localStorage.getItem("_yearGroupName");
    this._departmentName = localStorage.getItem("_departmentName");
    this._catClassId = localStorage.getItem("_catClassId");
    this.onSearch();
  }
 
  onSearch(){
    this.isProcess = true;
    this.searchForm.controls.yearGroupName.setValue(this._yearGroupName);
    this.searchForm.controls.departmentName.setValue(this._departmentName);
    this.searchForm.controls.catClassId.setValue(this._catClassId);
    this.service.httpGet('/api/v1/_teacherDepartment/leader',this.searchForm.value).then((res:IResponse)=>{
      this.dataSource.data = res.responseData||[];
    });
  }

  onSave(_id){ 
    this._teacherDepartmentId = _id;
    Swal.fire({
        title: this.translate.instant('alert.leader'), //ต้องการเลือกท่านนี้ ใช่หรือไม่
        icon : 'info',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: this.translate.instant('psms.DL0008'),
        cancelButtonText: this.translate.instant('psms.DL0009')
      }).then((result) => {
      if(result.dismiss == 'cancel'){
          return;
      } 
      this.dataSource.data.map((e) => {
        e['leader'] = false;
        
        return e;
      });
      this.service.httpPut('/api/v1/_teacherDepartment/updateLeader/'+ this.userName,this.dataSource.data).then((res:IResponse)=>{
        if((res.responseCode||500)!=200){
            Swal.fire(
              this.translate.instant('alert.leader_error'),//เลือกหัวหน้ากลุ่มสาระผิดพลาด
              res.responseMsg,
              'error' 
            )
          return; 
        }
         this.leaderTrue();
      });
    });
  } 

  leaderTrue(){
    let _updateLeader = {'teacherDepartmentId':this._teacherDepartmentId,'leader':true};
    this.service.httpPut('/api/v1/_teacherDepartment/updateTrue',_updateLeader).then((res:IResponse)=>{
      if((res.responseCode||500)!=200){
          Swal.fire(
            this.translate.instant('alert.leader_error_true'),//เลือกหัวหน้ากลุ่มสาระ true ผิดพลาด
            res.responseMsg,
            'error' 
          )
        return;
      }
      Swal.fire(
        this.translate.instant('alert.leader_header'),//เลือกหัวหน้ากลุ่มสาระ
        this.translate.instant('alert.leader_success'),//เลือกหัวหน้ากลุ่มสาระสำเร็จ
        'success'
      ).then(() => this.close());
    });
  }
  
  onCancel(){
    this.searchForm.controls.fullname.setValue("");
    this.onSearch();
  }
  
  close() {
    this.dialogRef.close(); 
  } 

}
