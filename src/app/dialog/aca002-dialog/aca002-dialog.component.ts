import { Component, OnInit, Inject } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ApiService, IResponse } from 'src/app/shared/service/api.service';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import Swal from 'sweetalert2/dist/sweetalert2.js';
import 'sweetalert2/src/sweetalert2.scss';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-aca002-dialog',
  templateUrl: './aca002-dialog.component.html',
  styleUrls: ['./aca002-dialog.component.scss']
})
export class Aca002DialogComponent implements OnInit {
  form = this.fb.group({
    'subjectNameTh': ['']
    , 'subjectNameEn': ['']
    , 'scoreId': ['']
    , 'departmentId': ['']
    , 'subjectCode': ['']
    , 'subjectCode2': [''] 
    , 'classId': ['']
    , 'yearGroupId': ['']
    , 'orders': ['']
    , 'subjectType': ['']
    , 'credit': ['']
    , 'lesson': ['']
    , 'weight': ['']
    , 'teacher1': ['']
    , 'teacherFullname1': ['']
  });

  subjectForm = this.fb.group({
    'classId': ['']
  });

  classRoomSubjectList: [];
  teacherProfileList: [];
  scoreList: [];
  dataSubject = [];
  classId: any;
  nameDepartment: any;

  constructor(
    private service: ApiService,
    @Inject(MAT_DIALOG_DATA) data: any,
    public dialogRef: MatDialogRef<Aca002DialogComponent>,
    private translate: TranslateService,
    private fb: FormBuilder
  ) {
    this.classId = data.classId || '';
    this.subjectForm.controls.classId.setValue(data.classId || '');
    this.form.controls.yearGroupId.setValue(data.yearGroupId || '');
    this.form.controls.departmentId.setValue(data.departmentId || '');
  }

  ngOnInit() {
    this.ddlClassRoomSubject();
    this.ddlTeacherProfile();
    this.ddlScore();
    this.loadSubject();
  }

  close() {
    this.dialogRef.close();
  }

  ddlClassRoomSubject() {
    let json = {
      'departmentId': this.form.value.departmentId
    };
    this.service.httpGet('/api/v1/class-room-subject/ddl', json).then((res) => {
      this.classRoomSubjectList = res || [];
    });
  }

  ddlTeacherProfile() {
    let json = { 'departmentId': this.form.value.departmentId };
    this.service.httpGet('/api/v1/teacher-profile/ddl', json).then((res) => {
      this.teacherProfileList = res || [];
    });
  }

  onCheckClassRoomSubject() {
    let ele = this.classRoomSubjectList.filter((o) => {
      return o['name_th'] == this.form.value.subjectNameTh;
    });
    this.form.controls.subjectCode.setValue(ele[0]['subject_code']);
    this.form.controls.subjectCode2.setValue(ele[0]['subject_code2'])
    this.form.controls.subjectNameEn.setValue(ele[0]['name_en'])
    this.form.controls.subjectType.setValue(ele[0]['subject_type'])
    this.form.controls.orders.setValue(ele[0]['orders']);
    this.form.controls.credit.setValue(ele[0]['credit']);
    this.form.controls.lesson.setValue(ele[0]['lesson']);
    this.form.controls.weight.setValue(ele[0]['weight']);
  }

  onCheckTeacherProfile() {
    let ele = this.teacherProfileList.filter((o) => {
      return o['teacher_id'] == this.form.value.teacher1;
    });
    this.form.controls.teacherFullname1.setValue(ele[0]['fullname']);
  }

  ddlScore() {
    this.service.httpGet('/api/v1/0/score/ddl', { 'iscatclass1': 'false' }).then((res) => {
      this.scoreList = res || [];
    });
  }

  loadSubject() {
    this.service.httpGet('/api/v1/trn-class-room-subject/classroomindialog', this.subjectForm.value).then((res: IResponse) => {
      this.dataSubject = res.responseData || [];
    });
  }

  onSave2() {
    this.dataSubject.map(e => {
      e['yearGroupId'] = this.form.value.yearGroupId;
      e['departmentId'] = this.form.value.departmentId;
      e['subjectCode'] = this.form.value.subjectCode;
      e['subjectCode2'] = this.form.value.subjectCode2;
      e['subjectNameTh'] = this.form.value.subjectNameTh;
      e['subjectNameEn'] = this.form.value.subjectNameEn;
      e['subjectType'] = this.form.value.subjectType;
      e['scoreId'] = this.form.value.scoreId;
      e['credit'] = this.form.value.credit;
      e['lesson'] = this.form.value.lesson;
      e['orders'] = this.form.value.orders;
      e['weight'] = this.form.value.weight;
      e['teacher1'] = this.form.value.teacher1;
      e['teacherFullname1'] = this.form.value.teacherFullname1;
      return e;
    });

    Swal.fire({
      title: this.translate.instant('psms.DL0001'),
      icon: 'info',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: this.translate.instant('psms.DL0008'),
      cancelButtonText: this.translate.instant('psms.DL0009')
    }).then((result) => {
      if (result.dismiss == 'cancel') {
        return;
      }
      let json = this.dataSubject
      let user = localStorage.getItem('userName')
      if (this.form.value.subjectNameTh == '' || this.form.value.scoreId == '') {
        Swal.fire(
          this.translate.instant('alert.error'),
          this.translate.instant('alert.validate'),
          'error'
        ) 
      } else {
        this.service.httpPost('/api/v1/trn-class-room-subject/' + user, json).then((res: IResponse) => {
          if((res.responseCode||500)!=200){
            Swal.fire(
              this.translate.instant('message.save_error'),
              res.responseMsg,
              'error' 
            )
            return;
          }
          Swal.fire( 
              this.translate.instant('message.save_header'), 
              this.translate.instant('message.save'),
              'success'
            ).then(() => { this.close(); })
        })
      }
      return;
    });
  }

}
