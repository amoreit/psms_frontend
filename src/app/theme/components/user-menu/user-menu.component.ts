import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { Router } from '@angular/router';
import { UtilService } from 'src/app/_util/util.service'; 
import { ApiService, IResponse } from 'src/app/shared/service/api.service';
import * as constants from '../../../constants';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-user-menu',
  templateUrl: './user-menu.component.html',
  styleUrls: ['./user-menu.component.scss'],
  encapsulation: ViewEncapsulation.None, 
})
export class UserMenuComponent implements OnInit {
  public userImage = "assets/img/users/user.jpg"; 
  constructor(private router: Router,private service:ApiService,private translate: TranslateService,public util: UtilService ) { } 
  
  username = '';
  email = '';
  usrName = '';
  title = '';
  urls = new Array<string>();
  photoPath='';

  ngOnInit() {
    this.username = localStorage.getItem('userName')||'';   
    this.email = localStorage.getItem('email')||'';    
    this.title = localStorage.getItem('title')||'';    
    this.usrName = localStorage.getItem('usrName')||'';  
    this.loadUser();
  } 

  loadUser(){
    let profileuser = {'usrName':this.usrName};
    this.service.httpGet('/api/v1/admUser/usermenu',profileuser).then((res:IResponse)=>{
      let data = res.responseData;
      this.photoPath = data[0]['photoPath'];
      if(this.photoPath == null || this.photoPath == '' || this.photoPath == undefined){
        this.urls = ['../assets/img/app/logo_Pramanda.gif'];
      }else {
        this.urls = [constants.SERVICE_API + '/api/v1/attachmentteacher/viewUser/' + this.photoPath];
      }
    });
  }

  onProfile(){
    this.router.navigate(['/adm/profile-user']);
  }

  onDocHelp(){
    this.router.navigate(['/adm/adm007']);
  }
}

