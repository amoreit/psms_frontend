import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { PerfectScrollbarConfigInterface } from 'ngx-perfect-scrollbar';
import { AppSettings } from '../../../app.settings';
import { Settings } from '../../../app.settings.model';
import { MenuService } from '../menu/menu.service';
import { ApiService, IResponse } from 'src/app/shared/service/api.service';
import { UtilService } from 'src/app/_util/util.service';
import * as constants from '../../../constants';

@Component({
  selector: 'app-sidenav',
  templateUrl: './sidenav.component.html',
  styleUrls: ['./sidenav.component.scss'],
  encapsulation: ViewEncapsulation.None,
  providers: [ MenuService ]
})
export class SidenavComponent implements OnInit {
  public psConfig: PerfectScrollbarConfigInterface = {
    wheelPropagation:true
  };

  public menuItems:Array<any>;
  public settings: Settings;
  constructor(
    public appSettings:AppSettings, 
    public menuService:MenuService,
    private service:ApiService,
    public util: UtilService 
  ){
    this.settings = this.appSettings.settings; 
  }

  username = '';
  email = '';
  usrName='';
  title ='';
  urls = new Array<string>();
  photoPath='';

  ngOnInit() {
    this.menuItems = this.menuService.getVerticalMenuItems(); 
    this.username = localStorage.getItem('userName')||'';   
    this.email = localStorage.getItem('email')||'';  
    this.title = localStorage.getItem('title')||'';  
    this.usrName = localStorage.getItem('usrName')||'';  
    this.loadUser();
  }

  loadUser(){
    let profileuser = {'usrName':this.usrName};
    this.service.httpGet('/api/v1/admUser/usermenu',profileuser).then((res:IResponse)=>{
      let data = res.responseData;
      this.photoPath = data[0]['photoPath'];
      if(this.photoPath == null || this.photoPath == '' || this.photoPath == undefined){
        this.urls = ['../assets/img/app/logo_Pramanda.gif'];
      }else {
        this.urls = [constants.SERVICE_API + '/api/v1/attachmentteacher/viewUser/' + this.photoPath];
      }
    });
  }

  ngDoCheck(){
    if(this.settings.fixedSidenav){
      if(this.psConfig.wheelPropagation == true){
        this.psConfig.wheelPropagation = false;
      }      
    }
    else{
      if(this.psConfig.wheelPropagation == false){
        this.psConfig.wheelPropagation = true;
      }  
    }
  }

  public closeSubMenus(){
    let menu = document.getElementById("vertical-menu");
    if(menu){
      for (let i = 0; i < menu.children[0].children.length; i++) {
        let child = menu.children[0].children[i];
        if(child){
          if(child.children[0].classList.contains('expanded')){
              child.children[0].classList.remove('expanded');
              child.children[1].classList.remove('show');
          }
        }
      }
    }
  }

}
