import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { AppSettings } from '../../../app.settings';
import { Settings } from '../../../app.settings.model';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-flags-menu',
  templateUrl: './flags-menu.component.html',
  styleUrls: ['./flags-menu.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class FlagsMenuComponent implements OnInit {

  public settings: Settings;
  translate:TranslateService;
 
  constructor(public appSettings:AppSettings,translate:TranslateService){
      this.settings = this.appSettings.settings; 
      this.translate = translate;
  } 
  langcode = localStorage.getItem("lan")||'th';
  language(lan:string){
    localStorage.setItem("lan",lan);
    this.translate.use(localStorage.getItem("lan")||'en');
    this.langcode =localStorage.getItem("lan")||'th';
  }

  ngOnInit() {
    this.translate.use(localStorage.getItem("lan")||'th');
  }

}
